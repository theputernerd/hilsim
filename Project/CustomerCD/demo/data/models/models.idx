MODEL_LIST(VERSION 2.0)


!** SEE END OF FILE FOR DEFINITIONS **


!**************************************************
! This is a model definition file; it is responsible
! for configuring and registering 3D models to be used 
! by VSB. All models must be registered via a model
! definition
!
! The syntax consists of two sections, the template
! definition section and the model definition section
!
! Templates define common parameters that may be used
! by model definitions. Template parameters are a
! subset of model parameters, as some parameters must
! be unique to each model definition.
!
! Note, the structure of the model directory is 
! critical. There is a system model directory that
! ie either;
!
!  <executable directory>/data/models
!
! or
!
!  <VIS3D_ROOT environment variable>/data/models
!
! depending on whether the environment variable exists.
!
! All paths specified within the model definition file
! are relative to the model directory, regardless of
! whether the model definition file is in the models
! directory, or a subdirectory there of.
!
! RULES:
!
! There is a sequence by which model definition files
! are parsed. The models.idx file in the models directory
! is parsed first. For this reason, it is a good idea
! to put all global templates in this index file.
!
! Subsequently, vsb recurses subdirectories of the
! models directory and parses all files named models.idx
! in the subdirectories.
!
! The reason for this is so that the model definition
! for a model is part of the model set of files to enable
! Models to easily be included and removed from a model
! set.
! 
! Directories containing individual models and their model
! definiton file can be added or removed, without having 
! to edit one global model definition file. 
!
! This is particularly useful for having a repository of
! configured models which users can easily just add to 
! their model set as required, aleviating the need to
! distribute every available model to every user. 
!
! NOTES:
! coordinates and euler angles are in vsb's coordinate
! frame which is different to the coordinate frame
! of the data which is in right hand, Z minus up, +X forward
!
! VSB coordinates are right hand, but Z positive up, and
! Y forward. 
!
! Template syntax:
! 
!   TEMPLATE(<template name>)
!   {
!      <template parameter>
!      ...
!      <template parameter>
!   }
!
! The template name is used to identify the template to include
! in model definitions. Template parameters are a subset of model 
! parameters that can be set, and they may be optional or
! compulsory, set once, or allowed to be overwritten. The
! order in which parameters can be set is not important.

!
! Model definition syntax:
!
!   MODEL(<model identifier> [,<template name>])
!   {
!      <model parameter>
!      ...
!      <model parameter>
!   }
!
! A model definition may or may not include a template.
!
!
!
! Parameters:
!
!  DESCRIPTION
!
!     Syntax:
!        DESCRIPTION = "description string"
!
!     Availability:
!        Models only - single definition - optional
!
!     This parameter is a descriptive string stored with the model 
!     definition parameters in the VSB model manager.
!    
!  GEOMETRY_STYLE
!
!     Syntax:
!        GEOMETRY_STYLE = <geometry style>
!
!     Availability:
!        Templates, and models - singe definition - optional
!    
!     This parameter defines a group of models the model belongs to. 
!     Valid ones are;
!       UNCATEGORISED,
!	MISSILE,
!	WEAPON,
!	MACHINERY,
!	AIRCRAFT,
!	WATERCRAFT,
!	LANDCRAFT,
!	ARCHITECTURAL,
!	CIVIL_INFRASTRUCTURE,
!	GEOPHYSICAL,
!	FAUNA,
!	FLORA
!     If this parameter is not provided for a model definition,
!     a default "UNCATEGORISED" is assumed.
!
!  SELECTOR
!    
!   Syntax:
!      SELECTOR(NAME:<selector name>, DEFAULT:<default bitmask>)
!
!     Availability:
!        Templates, and models - multiple definition - optional
!
!   Selectors define the default state of a specified articulated
!   part selector. Various bits represent the visibility or otherwise
!   of geometry under the named selector. See PLIB documentation
!   on selectors
!
!  ORIENTATION
!
!     Syntax:
!        ORIENTATION = <heading>, <pitch>, <roll>
!
!     Availability:
!        Templates and Models - single definition - optional
!
!     This parameter re-orients the model. The axes are VSB
!     axes, which differ from data axes.
!
!   CENTROID
! 
!     Syntax:
!        CENTROID = <x>, <y>, <z>
!
!     Availability:
!        Templates and Models - single definition - optional
!
!     This parameter re-positions the centroid of the model. 
!     The axes are VSB axes, which differ from data axes.
!
!
!--------------------------------------------------------

!=================================================
! TEMPLATE DEFINITIONS
!=================================================

TEMPLATE("MSFlightSim")
{
	SELECTOR(Name:"GEAR",  Default:0x00)
	SELECTOR(Name:"FLAPS",  Default:0x02)
	SELECTOR(Name:"PROP",  Default:0x02)
	SELECTOR(Name:"SPOILERS",  Default:0x02)
	
	ORIENTATION = -90, 0, 0
	LOAD_STYLE = DEMAND_PAGE !(FLATTEN, STRIPIFY)
	
}




