This program is a test harness for the HIL 3 axis 3d visual model defined in the files "MotionTable.cpp/.h"

The class "mtRoot" is derived from the PLIB "ssgBranch" scene graph node and can be inserted into a
PLIB scene graph. It adds 2 methods to the ssgBranch;

void  Initialise(const char *model_path);

  - specifies the path where the HIL table 3d model components are found, and loads them

SetAngle(int axis_id, double angle)

  - sets the angle of rotation of the specified axis in degrees

mtRoot draws only on the PLIB library so it can be used in a PLIB based app, or if appropriate, in a
VSB app.

The Hil3AxisPrototype application acts only as a test bed, and is built using the VSB library only
in as far as to take advantage of some of the convenience functionality of VSB. VSB is used as a GLUT
based application in this prototype to minimise the amount of GUI programming. In a truly useful
app, an MFC app would be far more versatile, either at the Plib or VSB level.

To compile, check out the hil_3axis_vis project and compile.

This app also requires the VSB data set, as provided by ViewPoint. And environment variable "VIS3D_ROOT"
should be defined pointing to the root data directory, which in the ViewPoint distribution resides
in the viewpoint executable directory.
 
When the program is compiled, a copy of the models is put into the executable directory where they
are expected. This is done through the post_build commands in the VisualC compile settings.