//--------------------------------------------------------------------------
// � Rob Wyatt for Game Developer/Gamasutra
//--------------------------------------------------------------------------

#ifndef KNI_HEADER
#define KNI_HEADER

// SIMD Registers (with Mod/RM register prefix)
#define _XMM0 (0xc0)
#define _XMM1 (0xc1)
#define _XMM2 (0xc2)
#define _XMM3 (0xc3)
#define _XMM4 (0xc4)
#define _XMM5 (0xc5)
#define _XMM6 (0xc6)
#define _XMM7 (0xc7)

// MMX Registers (with Mod/RM register prefix)
#define _MM0 (0xc0)
#define _MM1 (0xc1)
#define _MM2 (0xc2)
#define _MM3 (0xc3)
#define _MM4 (0xc4)
#define _MM5 (0xc5)
#define _MM6 (0xc6)
#define _MM7 (0xc7)

// Integer registers used as address pointers
#define	EAX_PTR (0)
#define	EBX_PTR (3)
#define	ECX_PTR (1)
#define	EDX_PTR (2)
#define	ESI_PTR (6)
#define	EDI_PTR (7)
#define	EBP_PTR (5)
#define	ESP_PTR (4)

// actual integer registers
#define	EAX_REG (0xc0)
#define	EBX_REG (0xc3)
#define	ECX_REG (0xc1)
#define	EDX_REG (0xc2)
#define	ESI_REG (0xc6)
#define	EDI_REG (0xc7)
#define	EBP_REG (0xc5)
#define	ESP_REG (0xc4)

// compare values
#define _EQ			(0)
#define _LT			(1)
#define _LE			(2)
#define _UNORDERED	(3)
#define _NE			(4)
#define _NEQ		(4)
#define _GE			(5)
#define _NLT		(5)
#define _GT			(6)
#define _NLE		(6)
#define _ORDERED	(7)
// Some better names for ordered and unordered
#define _QNAN		(3) // true if one of the inputs is a QNAN
#define _NUM		(7)	// false if one of the inputs is a QNAN



//--------------------------------------------------------------------------
// MOVE INSTRUCTIONS
//--------------------------------------------------------------------------

// Store 128bits to the effective address in dst fron the
// specified SIMD src register.
// NOTE: This is a dst memory form of the MOVAPS instruction.
#define MOVAPS_ST(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x29							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define MOVAPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x28							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVUPS_ST(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x11							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define MOVUPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x10							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVSS_ST(dst, src)					\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x11							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define MOVSS(dst, src)					    \
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x10							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVHLPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x12							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVLHPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x16							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVMSKPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x50							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVNTPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2b							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define SHUFPS(dst, src, imm)				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xC6							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
    _asm _emit imm                          \
}

#define UNPCKHPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x15							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define UNPCKLPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x14							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVHPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x16							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVHPS_ST(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x17							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define MOVLPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x12							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVLPS_ST(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x13							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}



//--------------------------------------------------------------------------
// MATH INSTRUCTIONS
//--------------------------------------------------------------------------

#define ADDPS(dst, src)						\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x58							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define ADDSS(dst, src)						\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x58							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define SUBPS(dst, src)						\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x5c							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define SUBSS(dst, src)						\
{				   		    				\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x5c							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MULPS(dst, src)						\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x59							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MULSS(dst, src)						\
{				   		    				\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x59							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define DIVPS(dst, src)						\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x5e							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define DIVSS(dst, src)						\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x5e							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define SQRTPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x51							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define SQRTSS(dst, src)					\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x51							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define RCPPS(dst, src)		    			\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x53							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define RCPSS(dst, src)	    				\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x53							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define RSQRTPS(dst, src)	    			\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x52							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define RSQRTSS(dst, src)    				\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x52							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MAXPS(dst, src)	        			\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x5f							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MAXSS(dst, src)       				\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x5f							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MINPS(dst, src)	        			\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x5d							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MINSS(dst, src)       				\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x5d							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}




//--------------------------------------------------------------------------
// COMPARE INSTRUCTIONS
//--------------------------------------------------------------------------

#define CMPPS(dst, src, cond)				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xC2							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
	_asm _emit cond							\
}

#define CMPEQPS(dst,src) CMPPS(dst,src,_EQ)
#define CMPLTPS(dst,src) CMPPS(dst,src,_LT)
#define CMPLEPS(dst,src) CMPPS(dst,src,_LE)
#define CMPUNORDPS(dst,src) CMPPS(dst,src,_UNORDERED)
#define CMPNEQPS(dst,src) CMPPS(dst,src,_NEQ)
#define CMPNEPS(dst,src) CMPPS(dst,src,_NEQ)
#define CMPNLTPS(dst,src) CMPPS(dst,src,_NLT)
#define CMPGEPS(dst,src) CMPPS(dst,src,_NLT)
#define CMPNLEPS(dst,src) CMPPS(dst,src,_NLE)
#define CMPGTPS(dst,src) CMPPS(dst,src,_NLE)
#define CMPORDPS(dst,src) CMPPS(dst,src,_ORDERED)

#define CMPSS(dst, src, cond)				\
{											\
	_asm _eint 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0xC2							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
	_asm _emit cond							\
}

#define CMPEQSS(dst,src) CMPSS(dst,src,_EQ)
#define CMPLTSS(dst,src) CMPSS(dst,src,_LT)
#define CMPLESS(dst,src) CMPSS(dst,src,_LE)
#define CMPUNORDSS(dst,src) CMPSS(dst,src,_UNORDERED)
#define CMPNEQSS(dst,src) CMPSS(dst,src,_NEQ)
#define CMPNESS(dst,src) CMPSS(dst,src,_NEQ)
#define CMPNLTSS(dst,src) CMPSS(dst,src,_NLT)
#define CMPGESS(dst,src) CMPSS(dst,src,_NLT)
#define CMPNLESS(dst,src) CMPSS(dst,src,_NLE)
#define CMPGTSS(dst,src) CMPSS(dst,src,_NLE)
#define CMPORDSS(dst,src) CMPSS(dst,src,_ORDERED)

#define COMISS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2f							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define UCOMISS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2e							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}


//--------------------------------------------------------------------------
// LOGICAL INSTRUCTIONS
//--------------------------------------------------------------------------

#define ANDNPS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x55							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define ANDPS(dst, src)						\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x54							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define ORPS(dst, src)  					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x56							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define XORPS(dst, src) 					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x57							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}


//--------------------------------------------------------------------------
// CONVERSION INSTRUCTIONS
//--------------------------------------------------------------------------

#define CVTPI2PS(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2a							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define CVTPS2PI(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2d							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define CVTSI2SS(dst, src)					\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x2a							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define CVTSS2SI(dst, src)					\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x2d							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define CVTTPS2PI(dst, src)					\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x2c							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define CVTTSS2SI(dst, src)					\
{											\
	_asm _emit 0xf3							\
	_asm _emit 0x0f							\
	_asm _emit 0x2c							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}


//--------------------------------------------------------------------------
// MEMORY INSTRUCTIONS
//--------------------------------------------------------------------------
#define SFENCE                              \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xae							\
	_asm _emit 0xf8                         \
}

#define PREFETCHNTA(src)                    \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x18							\
    _asm _emit (src & 0x3f)                 \
}

#define PREFETCHT0(src)                     \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x18							\
    _asm _emit 0x08 | (src & 0x3f)          \
}

#define PREFETCHT1(src)                     \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x18							\
    _asm _emit 0x10 | (src & 0x3f)          \
}

#define PREFETCHT2(src)                     \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x18							\
    _asm _emit 0x18 | (src & 0x3f)          \
}



//--------------------------------------------------------------------------
// INTEGER/MMX INSTRUCTIONS
//--------------------------------------------------------------------------
#define PSHUFW(dst, src, imm)				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0x70							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
    _asm _emit imm                          \
}

#define PEXTRW(dst, src, imm)				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xc5							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
    _asm _emit imm                          \
}

#define PINSRW(dst, src, imm)				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xc4							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
    _asm _emit imm                          \
}

#define PMAXSW(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xee							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PMAXUB(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xde							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PMINSW(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xea							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PMINUB(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xda							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PMOVMSKB(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xd7							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PMULHUW(dst, src)    				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xe4							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PAVGB(dst, src)        				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xe0							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PAVGW(dst, src)        				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xe3							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define PSADBW(dst, src)       				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xf6							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}

#define MOVNTQ(dst, src)       				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xe7							\
	_asm _emit ((src & 0x3f)<<3) | (dst)	\
}

#define MASKMOVQ(dst, src)     				\
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xf7							\
	_asm _emit ((dst & 0x3f)<<3) | (src)	\
}


//--------------------------------------------------------------------------
// CONTROL INSTRUCTIONS
//--------------------------------------------------------------------------

#define LDMXCSR(src)   					    \
{											\
    _asm _emit 0x0f                         \
    _asm _emit 0xae                         \
	_asm _emit 0x10 | (src & 0x3f)          \
}

#define STMXCSR(dst)  					    \
{											\
	_asm _emit 0x0f							\
	_asm _emit 0xae							\
	_asm _emit 0x18 | (dst & 0x3f)          \
}

#define FXSAVE(dst)    					    \
{                                           \
	_asm _emit 0x0f							\
	_asm _emit 0xae							\
	_asm _emit (dst & 0x3f)                 \
}

#define FXRSTOR(src)   					    \
{											\
    _asm _emit 0x0f                         \
    _asm _emit 0xae                         \
	_asm _emit 0x08 | (src & 0x3f)          \
}



#endif