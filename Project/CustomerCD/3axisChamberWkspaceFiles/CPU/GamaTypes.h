//--------------------------------------------------------------------------
// � Rob Wyatt for Game Developer/Gamasutra
//--------------------------------------------------------------------------



//==============================================================================
//	Definition of several integer types compatible with the emerging ANSI/ISO
//	C9x standard. All code in Wyatts World will use these types.
//==============================================================================

#ifndef HEADER_GamaTypes_HEADER
#define HEADER_GamaTypes_HEADER

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <climits>

	//
	// 8 bit types.
	//
	typedef signed   char int8_t;
	typedef unsigned char uint8_t;

	//
	// 16 bit types.
	//
#if	UINT_MAX == 0xffffU
	// The compiler has 16 bit integers.
	typedef 		 int int16_t;
	typedef unsigned int uint16_t;

#elif USHRT_MAX == 0xffffU
	// The compiler has 16 bit short integers.
	typedef 		 short int int16_t;
	typedef unsigned short int uint16_t;
#else
	#error Cannot define 16 bit types: int16_t, uint16_t.
#endif

	//
	// 32 bit types.
	//
#if	UINT_MAX == 0xffffffffUL
	// The compiler has 32 bit integers.
	typedef			 int int32_t;
	typedef unsigned int uint32_t;

#elif ULONG_MAX == 0xffffffffUL
	// The compiler has 32 bit long integers.
	typedef			 long int32_t;
	typedef unsigned long uint32_t;

#else
	#error Cannot define 32 bit types: int32_t, uint32_t.
#endif

	//
	// 64 bit types
	//
#if defined(_MSC_VER)
	// Microsoft compilers have built-in MS specific 64 bit types.
	typedef          __int64 int64_t;
	typedef unsigned __int64 uint64_t;

#else
	#error Cannot define 64 bit types: int64_t, uint64_t.
#endif

	//
	// Generic types.
	//
	typedef unsigned int uint_t;


#endif //HEADER_GamaTypes_HEADER
