//------------------------------------------------------------------------------
// � Rob Wyatt for Game Developer/Gamasutra
// Processor Detect Version: 1.01
//------------------------------------------------------------------------------
#include "stdafx.h"

//------------------------------------------------------------------------------
// Things to do:
//  - Support processor frequency when there is no RDTSC
//  - Read the Cyrix device ID port under Win9x, NT will not allow it
//  - Get more specific info on the K7
//
//
// Version: 1.01 - August 11th 1999
//
//  - Fixed a crash bug that occured with a PIII processor and no OS support.
//  - Added support for Cyrix 586 processors.
//  - Added support for Cache Line Locking extended feature.
//  - measure_clock_speed() does not return 0 if the clock speed cannot be
//      measured, instead it returns 100 as a safe default, just in case an
//      application divides by it.
//  - decode_intel_signature() can now tell the difference between a CeleronA
//      and a Mobile Pentium II.
//  - decode_intel_signature() can now detect the mobile Pentium.
//  - Overdrive support added to processor_name() although Pentium overdrive
//      processors cannot be detected because of an errata, OEM will always
//      be returned as the processor type for Pentium processors.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
Detect::Detect
(
)
//-------------------------------------
{
	SYSTEM_INFO     sys_info;
    HANDLE          thread;
    uint32_t        thread_id;
    uint32_t        system_proc;

    Setup = false;

    // Get the system info to derive the number of processors
    // within the system. It will always be 1 on 95 or 98.
	GetSystemInfo(&sys_info);
    Processors = sys_info.dwNumberOfProcessors;
    ActiveProcessorMask = sys_info.dwActiveProcessorMask;

    if (!GetProcessAffinityMask( GetCurrentProcess(), 
                            reinterpret_cast<unsigned long*>(&ProcessAffinityMask), 
                            reinterpret_cast<unsigned long*>(&system_proc) ))
    {
        // could not get the process mask, setup is bad.
        return;
    }

    ProcessProcessors = count_bits(ProcessAffinityMask);

    // Set the current processor that we are working on.
    CurrentProcessor = 0;
    CurrentProcessorElement = 0;

    while (CurrentProcessor<Processors)
    {
        uint32_t    thread_processor_mask = 1<<CurrentProcessor;

        // Is the processor we are about to set for the detect thread part of the
        // process affinity mask. If it is not then it cannot be set.
        if ((ProcessAffinityMask & thread_processor_mask))
        {
            // create a thread that is suspended
            thread = CreateThread
                        (
                            NULL,
                            0,
                            (unsigned long (__stdcall *)(void *))detect_thread_entry_stub,
                            reinterpret_cast<void*>(this),
                            CREATE_SUSPENDED,
                            reinterpret_cast<unsigned long*>(&thread_id)
                        );

            if (thread != 0)
            {
                // Set the affinity of the thread so to force it to run
                // on the required processor
                if (SetThreadAffinityMask(thread, thread_processor_mask) != 0)
                {
                    uint32_t    exit_code;

                    // Now we have set the processor resume the thread and it
                    // will run only on the specified processor.
                    ResumeThread(thread);

                    // Wait for the current detection thread to finish
                    WaitForSingleObject(thread,INFINITE);

                    GetExitCodeThread(thread, reinterpret_cast<unsigned long*>(&exit_code) );
                    if (exit_code == 0)
                    {
                        // if the exit code was zero then the thread suffered an error and
                        // no memory was allocated so it is safe to just set the processor
                        // array element to zero.
                        ProcessorData[CurrentProcessorElement] = 0;
                        ProcessorInfo[CurrentProcessorElement] = 0;
                    }

                    // Close the handle to the now finished thread
                    CloseHandle(thread);
                }
                else
                {
                    ProcessorData[CurrentProcessorElement] = 0;
                    ProcessorInfo[CurrentProcessorElement] = 0;
                }
            }
            else
            {
                // Thread failed to create so detection of this processor
                // is bad.
                ProcessorData[CurrentProcessorElement] = 0;
                ProcessorInfo[CurrentProcessorElement] = 0;
            }

            // Use the next element of the array
            CurrentProcessorElement++;
        }

        // goto the next processor
        CurrentProcessor++;
    }

    // Setup is now good
    Setup = true;
}


//------------------------------------------------------------------------------
Detect::~Detect
(
)
//-------------------------------------
{
    for (uint32_t i=0; i<CurrentProcessorElement; i++)
    {
        delete ProcessorData[i];
        delete ProcessorInfo[i];
    }
}


//------------------------------------------------------------------------------
uint32_t __stdcall Detect::detect_thread_entry_stub
(
    void*   param
)
//-------------------------------------
{
    // This function only exists because Windows cannot call a class member. To
    // simulate calling a class member we pass in the class this pointer as the
    // user parameter and the cast it to the class type and call the class_entry
    // function. This function is a friend of this class so it can access the
    // class_thread_entry() private function.
    return reinterpret_cast<Detect*>(param)->class_thread_entry();
}


//------------------------------------------------------------------------------
uint32_t Detect::class_thread_entry
(
)
//-------------------------------------
{
    //
    // This function is only going to run from within a single thread and on a
    // single processor. Any detection must be done before this function returns.
    //

    // set these pointer variables to zero so if we get an exception we can
    // delete them again.
    ProcessorInfo[CurrentProcessorElement] = 0;
    ProcessorData[CurrentProcessorElement] = 0;

    __try
    {
        ProcessorInfo[CurrentProcessorElement] = new ProcInfo;

        ProcessorInfo[CurrentProcessorElement]->NoCPUIDFamily = 0;
        ProcessorInfo[CurrentProcessorElement]->CPUIDPresent = detect_cpuid_instruction(
            ProcessorInfo[CurrentProcessorElement]->NoCPUIDFamily,
            ProcessorInfo[CurrentProcessorElement]->NoCPUIDMake
        );

        ProcessorInfo[CurrentProcessorElement]->ClockSpeed = 100.0f;

        if (ProcessorInfo[CurrentProcessorElement]->CPUIDPresent)
        {
            // Gather all CPUID info and store it locally
            get_cpuid_info();

            // we must have a working RDTSC instruction before we can measure the clock speed
            if (ProcessorData[CurrentProcessorElement]->StandardCPUIDRegisters[1].RegEDX & (1<<4))
            {
                // rdtsc may be disabled so protect the measuring code.
                __try
                {
                    // We need to measure the frequency from within this thread while we are
                    // running on a single processor.
                    ProcessorInfo[CurrentProcessorElement]->ClockSpeed = measure_clock_speed();
                }
                __except(EXCEPTION_EXECUTE_HANDLER)
                {
                    ProcessorInfo[CurrentProcessorElement]->ClockSpeed = 100.0f;
                }
            }
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        // an error as occured, so delete any allocated memory and return zero.
        delete ProcessorInfo[CurrentProcessorElement];
        delete ProcessorData[CurrentProcessorElement];
        return 0;
    }

    return 1;
}


//------------------------------------------------------------------------------
uint32_t Detect::count_bits
(
    uint32_t    data
)
//-------------------------------------
{
    uint32_t    count = 0;
    uint32_t    pos = 0;

    // go through each bit and see if it is set in 'data'
    while (pos<32)
    {
        if (data & (1<<pos))
            count++;
        pos++;
    }

    return count;
}


//------------------------------------------------------------------------------
bool Detect::detect_cpuid_instruction
(
    uint32_t&               family,
    ProcessorManufacturer&   make
)
//-------------------------------------
{
    uint32_t    processor = 0;
    uint32_t    cpuid_instruction = 0;

    _asm
    {
        pushfd
        pop         eax                         // get original EFLAGS
        mov         ecx,eax                     // save origninal EFLAGS
        xor         eax,40000h                  // flip AC bit in EFLAGS
        push        eax                         // save for EFLAGS
        popfd                                   // copy to EFLAGS
        pushfd                                  // push EFLAGS
        pop         eax                         // get new EFLAGS value
        xor         eax,ecx                     // cant toggle AC bit, CPU=Intel386
        mov         processor, 3
        je          END_CPUID

        //	Getting to here means this processor must be at least a 486, try to toggle
        //  the ID bit and determine if the CPUID instruction is present.

        pushfd                                  // Get original EFLAGS
        pop         eax
        mov         ecx, eax
        xor         eax, 200000h                // Flip ID bit in EFLAGS
        push        eax                         // Save new EFLAGS value on stack        									
        popfd                                   // Replace current EFLAGS value
        pushfd                                  // Get new EFLAGS
        pop         eax                         // Store new EFLAGS in EAX
        xor         eax, ecx                    // Can not toggle ID bit,
        mov         processor, 4
        jz          END_CPUID                   // Processor=80486

        // Getting to here means the processor supports the CPUID instruction.
        mov         cpuid_instruction,1
END_CPUID:
    }

    family = processor;
    make = Unknown_Manufacturer;

    if ((family == 4) && (cpuid_instruction != 0))
    {
        uint32_t    cyrix = 0;

        // we are a family 4 without CPUID, we could be a cyrix
        // This code is from the Cyrix CPU detection manual
        _asm
        {
            xor     ax,ax
            sahf
            mov     ax,5
            mov     bx,2
            div     bl
            lahf
            cmp     ah,2
            jne     NOT_CYRIX

            // this is a cyrix
            mov     cyrix,1
NOT_CYRIX:
        }

        if (cyrix == 1)
            make = Cyrix;

        // we could go on an read the Device ID portsd that Cyrix define at Ports 0x22 and 0x23
        // but Windows is not going to be happy with this so we won't do it.
    }

    return cpuid_instruction != 0;
}


//------------------------------------------------------------------------------
uint32_t Detect::get_extended_cpuid_info
(
)
//-------------------------------------
{
	uint32_t extended = 0;

    uint32_t    eax_max;
    uint32_t    ebx_max;
    uint32_t    ecx_max;
    uint32_t    edx_max;

	_asm
	{
		mov     eax,0
		cpuid                       // eax = max cpuid
		cpuid                       // issue max CPUID and store the results
		mov     eax_max,eax
		mov     ebx_max,ebx
		mov     ecx_max,ecx
		mov     edx_max,edx

		mov     eax,0x80000000      // issue the base extended function
		cpuid

		// check the results of the extended base function with the max standard function
		cmp     eax, eax_max
		jne     extended_range
		cmp     ebx, ebx_max
		jne     extended_range
		cmp     ecx, ecx_max
		jne     extended_range
		cmp     edx, edx_max
		jne     extended_range
		jmp     no_extended         // all the registers as the same as the max base function, no extended functions

extended_range:
		test    eax,0x80000000      // the top bit of eax must be set
		je      no_extended

		mov     extended,eax
no_extended:
	}

	return extended;
}


//------------------------------------------------------------------------------
void Detect::get_cpuid_info
(
)
//-------------------------------------
{
    uint32_t    standard = 0;
    uint32_t    extended = 0;

    // detect the maximum function for standard CPUID functions
    _asm
    {
        mov     eax,0
        cpuid
        mov     standard,eax        // maximum standard CPUID
    }

	// Now the more difficult situation of detecting the presence of the extended
	// CPUID functions. The function is called from with a try/except block so if
	// it fails the thread will not quit.
	__try
	{
		// We have to put the detect code into a separate function because of the
		// try/except block. Visual C++ will not allow assembler jumps from directly
		// inside the block so we have to use a function.
		extended = get_extended_cpuid_info();
	}
	__except(EXCEPTION_EXECUTE_HANDLER)
	{
		// detecting the extended CPUID function has caused an exception so 
		// we will assume they are not present.
		extended = 0;
	}

    // allocate the memory for all the CPUID functions and calulate the required
    // pointers for the ProcPCUID structure.
    char*       base = new char[sizeof(ProcCPUID) + (sizeof(Registers) * ((extended&0x7fffffff)+standard+2)) ];
    
    ProcessorData[CurrentProcessorElement] = reinterpret_cast<ProcCPUID*>(base);
    uint32_t*   standard_base = reinterpret_cast<uint32_t*>(base+sizeof(ProcCPUID));
    uint32_t*   extended_base = reinterpret_cast<uint32_t*>(base+sizeof(ProcCPUID)+(sizeof(Registers) * (standard+1)));

    // fill the structure
    ProcessorData[CurrentProcessorElement]->MaxStandardCPUID = standard;
    ProcessorData[CurrentProcessorElement]->MaxExtendedCPUID = extended;
    ProcessorData[CurrentProcessorElement]->StandardCPUIDRegisters = reinterpret_cast<Registers*>(standard_base);
    ProcessorData[CurrentProcessorElement]->ExtendedCPUIDRegisters = reinterpret_cast<Registers*>(extended_base);

    // Call all the standard CPUID functions...
    _asm
    {
        mov     esi,standard_base
        mov     edi, 0

NEXT_STANDARD:
        mov     eax,edi
        cpuid
        mov     [esi+0],eax
        mov     [esi+4],ebx
        mov     [esi+8],edx
        mov     [esi+12],ecx
        add     esi,16
        inc     edi
        cmp     edi,standard
        jbe     NEXT_STANDARD
    }

    // Call all the extended CPUID functions if any are present
    if (extended>0)
    {
        _asm
        {
            mov     esi,extended_base
            mov     edi, 0x80000000

NEXT_EXTENDED:
            mov     eax,edi
            cpuid
            mov     [esi+0],eax
            mov     [esi+4],ebx
            mov     [esi+8],edx
            mov     [esi+12],ecx
            add     esi,16
            inc     edi
            cmp     edi,extended
            jbe     NEXT_EXTENDED
        }
    }
}


//------------------------------------------------------------------------------
bool Detect::detect_fpu
(
)
//-------------------------------------
{
	uint16_t	status;
	uint32_t	found = 0;

	_asm
	{
		fninit						// reset FP status word
		mov    	[status], 5a5ah		// initialise temp word to non-zero value
		fnstsw	[status]			// save FP status word
		mov		ax,[status]			// check FP status word
		cmp   	al,0				// see if correct status with written
		jne    	DONE

		fnstcw	[status]			// save FP control word
		mov		ax,[status]			// check FP control word
		and		ax,103fh			// see if seleced parts look OK
		cmp		ax,3fh				// check that 1s & 0s correctly read
		jne		DONE
		mov		found,1
DONE:
	}

	return found!=0;
}

//------------------------------------------------------------------------------
void Detect::detect_standard_features
(
	uint32_t	processor,
	uint32_t&	features
)
//-------------------------------------
{
	if (ProcessorInfo[processor]->CPUIDPresent)
	{
		features|=CPU_CPUID;		// CPUID is present

		if (ProcessorData[processor]->MaxStandardCPUID>=2)
		{
			// This processor can give cahce info as the config data function (2) is present
			features|=CPU_CACHEINFO;
		}

		// get the CPUID standard features, function 0, EDX
		uint32_t id_features = ProcessorData[processor]->StandardCPUIDRegisters[1].RegEDX;

		if (id_features & (1<<0))
			features |= CPU_FPUPRESENT;

		// check for Virtual Mode Extensions
		if (id_features & (1<<1))
		{
			features |= CPU_VME;
		}

		// check for extended DEBUG registers
		if (id_features & (1<<2))
		{
			features |= CPU_DEBUG;
		}

		// check for Page Size Extensions
		if (id_features & (1<<3))
		{
			features |= CPU_PSE;
		}

		// check for RDTSC and check to see if it is enabled
		if (id_features & (1<<4))
		{
			features |= CPU_RDTSC;

			// Ideally the best way to check for RDTSC being enabled is to
			// check the TSC bit in CR4. As we cannot access CR4 from ring 0
			// we will have to rely on the instruction generating an exception.
			// If the OS emulates the RDTSC instruction then this check will
			// succeed.
			__try
			{
				_asm rdtsc
			}
			__except(EXCEPTION_EXECUTE_HANDLER)
			{
				features &= ~CPU_RDTSC;
			}
		}

		// check for Model Specific Registers
		if (id_features & (1<<5))
		{
			features |= CPU_MSR;
		}

		// check for Physical Address extensions
		if (id_features & (1<<6))
		{
			features |= CPU_PAE;
		}

		// check for Machine Check exception
		if (id_features & (1<<7))
		{
			features |= CPU_MCE;
		}

		// check for the CMPXCHG8B instruction
		if (id_features & (1<<8))
		{
			features |= CPU_CMPXCHG8B;
		}

		// check for an APIC
		if (id_features & (1<<9))
		{
			features |= CPU_APIC;
		}

		// BIT 10 IS RESERVED..

		// check for Fast System Calls
		if (id_features & (1<<11))
		{
			features |= CPU_SEP;
		}

		// check for memory type range registers
		if (id_features & (1<<12))
		{
			features |= CPU_MTRR;
		}

		// check for Global Paging Extentions
		if (id_features & (1<<13))
		{
			features |= CPU_GPE;
		}

		// check for Machine Check Architecture
		if (id_features & (1<<14))
		{
			features |= CPU_MCA;
		}

		// check for CMOV instructions
		if (id_features & (1<<15))
		{
			features |= CPU_CMOV;
		}

		// check for Page Attribute Table
		if (id_features & (1<<16))
		{
			features |= CPU_PAT;
		}

		// check for 36 Bit Page Size Extension
		if (id_features & (1<<17))
		{
			features |= CPU_PSE36;
		}

		// check for Processor Serial Number
		if (id_features & (1<<18))
		{
			features |= CPU_SNR;
		}

		// Bits 19 to 22 are reserved

		// check for MMX
		if (id_features & (1<<23))
		{
			features |= CPU_MMX;
		}

		// Bit 24 is used in detect_simd_features()

		// check for SIMD instructions
		if (id_features & (1<<25))
		{
			features |= CPU_SIMD;
		}

		// Bits 26 to 31 are reserved


		//
		// Some processors return the wrong flags so lets go an correct them based
		// on known models and steppings.
		//
		ProcessorManufacturer	manu;
		if (get_manufacturer(processor,manu))
		{
			if (manu == Intel)
			{
				if (features & CPU_SEP)
				{
					// Pentium Pro Model 1 returned the fast system call flag set
					// when it was not supported so lets correct it.
					uint32_t	family;
					uint32_t	model;
					uint32_t	stepping;
					uint32_t	type;

					if (get_signature_components(processor,family,model,stepping,type))
					{
						if ((family == 6) && (model<3) && (stepping<3))
						{
							features &= ~CPU_SEP;
						}
					}
				}
			}
		}


		// all processors that support CPUID will run Pentium code as long as it has a FPU.
		// If it is a family 4 it just won't be fast.
		if (features & CPU_FPUPRESENT)
		{
			features |= CPU_PENTIUM;
		}

		// To execute PentiumII code we need to have the new instruction (cmov/fcomv) and MMX and be
		// able to execute pentium code.
		if ((features & CPU_PENTIUM) && (features & CPU_CMOV) && (features & CPU_MMX))
		{
			// This processor will run PentiumII code
			features |= CPU_PENTIUMII;
		}
	}
	else
	{
		// If there is no CPUID all we can do is detect the presence of an FPU and
		// we can check the family to see if we can run Pentium code or not.
		if (detect_fpu())
		{
			features |= CPU_FPUPRESENT;

			// now that we have an FPU there is no reason we cannot execute basic Pentium
			// code as long as we are at least a 486. The 486 had new instructions that
			// the 386 did not have.
            if (ProcessorInfo[processor]->NoCPUIDFamily >= 4)
			    features |= CPU_PENTIUM;
		}
	}
}


//------------------------------------------------------------------------------
void Detect::detect_extended_features
(
 	uint32_t	processor,
	uint32_t&	features
)
//-------------------------------------
{
	// get_standard_features() must hve been called before this

	// This processor has no extended features so no need to validate/modify the
	// feature flags.
	if (ProcessorData[processor]->MaxExtendedCPUID==0)
		return;

	// Can we get cache info from the extended features? 
	if (ProcessorData[processor]->MaxExtendedCPUID>=0x80000005)
	{
		// there is some cahce info present
		features|=CPU_CACHEINFO;
	}

	// get the extended features, extended CPUID function 0x80000001, EDX
	uint32_t	ex_features = ProcessorData[processor]->ExtendedCPUIDRegisters[1].RegEDX;

    // check if the processor can lock cache lines
    if (ex_features & (1<<27))
    {
        features |= CPU_CCL;
    }

    // Check for 3D Now! For safety we also check the MMX bit is set
    if ((ex_features & (1<<31)) && (ex_features & (1<<23)))
    {
        features |= CPU_3DNOW;
    }

    // There is nothing else in the extended features that we are concerned with. We
    // could be really paranoid and check to ensure the extended flags agree with
    // the standard flags.
    // We could also check for seperate integer and floating point cmov instructions
    // but there really is no point.
}


//------------------------------------------------------------------------------
void Detect::detect_simd_features
(
 	uint32_t	processor,
	uint32_t&	features
)
//-------------------------------------
{
	// get_standard_features() must hve been called before this

	// if we don't have SIMD instructions then we don't check any more.
	if ((features & CPU_SIMD)==0)
		return;

    // The processor has SIMD instructions but are they supported by the OS?
    // If so bit 24 of the standard features will be set if the OS supportes
    // the extended context and a SIMD instruction will not generate an invalid
    // op-code if executed.
    if (ProcessorData[processor]->StandardCPUIDRegisters[1].RegEDX & (1<<24))
    {
        //
        // Check for OS support
        //
        __try
        {
            _asm
            {
                //Execute a Streaming SIMD instruction and see if an
                //exception occurs. If the exception is an unknown
                //op-code then SIMD is not supported by the OS.

                 ADDPS(_XMM0,_XMM1)
            }
            features |= CPU_SIMD_ENABLE;
        }
        __except(EXCEPTION_EXECUTE_HANDLER)
        {
        }
    }

    // We have checked for SIMD OS support, the last check is SIMD exception support
    if (detect_simd_exception_support())
    {
        features |= CPU_SIMD_EXCEPT;
    }

    // If this processor supports all the PentiumII options has SIMD instructions and
    // they are enabled then we can execute PentiumIII code.
    if ((features & CPU_PENTIUMII) && (features & CPU_SIMD) && (features & CPU_SIMD_ENABLE))
    {
        features |= CPU_PENTIUMIII;
    }
}


//--------------------------------------------------------------------------
bool Detect::detect_simd_exception_support
(
)
//---------------------------
{
    uint32_t    control;
    bool        exception_support = true;
    float       test_val[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    uint32_t    control_original;

    __try
    {
        _asm
        {
            // Enable divide by zero exceptions by clearing
            // bit 9 in the SIMD control register.
            //push    ebp
            lea     eax,control
            STMXCSR(EAX_PTR)
            mov     ebx, DWORD PTR [eax]
            and     DWORD PTR [eax], 0fffffdffh
            LDMXCSR(EAX_PTR)
            //pop     ebp
            mov     control_original, ebx

            // clear XMM0, all bits being 0 is 0.0 in floating point
            lea     eax,test_val
            XORPS   (_XMM0,_XMM0)
            MOVUPS  (_XMM1,EAX_PTR)
            DIVPS   (_XMM1,_XMM0)
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER) 
    {
        // The divide by zero above has caused an illegal instruction
        // exception so the OS must not support SIMD exceptions. Any
        // exception is enough for us to fail SIMD excpection support.
        exception_support = false;
    }

    //
    // Put the original SIMD control register back, or try to.
    //
    __try
    {
        _asm
        {    
            lea     eax,control_original
            LDMXCSR(EAX_PTR)
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER) 
    {
    }

    return exception_support;
}

//------------------------------------------------------------------------------
void Detect::decode_intel_signature
(
	uint32_t			processor,
	ProcessorFamily&	res_family
)
//-------------------------------------
{
	uint32_t	family;
	uint32_t	model;
	uint32_t	stepping;
	uint32_t	type;

	ProcessorCacheInfo	cache_info;

	if (get_signature_components(processor,family,model,stepping,type))
	{
		if (family == 4)
		{
			// All 486 models that support CPUID have at least a stepping of 3
			if (stepping >= 3)
			{
				switch (model)
				{
				case 0x04:
					res_family = Intel_80486_SL;
					break;

				case 0x07:
					res_family = Intel_80486_DX2;
					break;

				case 0x08:
					res_family = Intel_80486_DX4;
					break;

				default:
					res_family = Intel_80486;
					break;
				}
			}
			else
			{
				res_family = Intel_80486;
			}
		}
		else if (family == 5)
		{
			res_family = Intel_Pentium;
			if (model == 4)
			{
				res_family = Intel_PentiumMMX;
			}
            else if (model == 8)
            {
                res_family = Intel_MobilePentium;
            }
		}
		else if (family == 6)
		{
			switch (model)
			{
			case 0x01:
				res_family = Intel_PentiumPro;
				break;

			case 0x03:
			    res_family = Intel_PentiumII;
				break;

			case 0x05:
				// This could be a Xeon or a Celeron or a standard PentiumII, we need
				// to look at the cache size to be sure.
				if (!get_cache_info(processor,cache_info))
				{
					// If we cannot get the cahce it must be some processor without any
					// which would be a celeron type processor but a Celeron should return
					// L1 info.....Anyway this is the best bet
					res_family = Intel_Celeron;
				}

                if (cache_info.UnifiedLevel2)
                {
				    if (cache_info.Level2UnifiedCacheSize == 0)
				    {
					    // No level 2 cache must be a celeron
					    res_family = Intel_Celeron;
				    }
				    else if (cache_info.Level2UnifiedCacheSize >= 1024*1024)
				    {
					    // 1Meg or more cache must be a xeon
					    res_family = Intel_PentiumIIXeon;
				    }
				    else
				    {
					    // Anything else is a Pentium II, you cannot distinguish between a
					    // PentiumII and a PentiumII Xeon with 512K of L2 cache
					    res_family = Intel_PentiumII;
				    }
                }
                else
                {
                    // We have a non-unified Level 2 cache, I have no idea what it is!
                    res_family = Intel_Unknown;
                }
				break;

			case 0x06:
                // This model is either a Pentium II Celeron A or a mobile Pentium II
				if (!get_cache_info(processor,cache_info))
				{
					// If we cannot get the cahce it must be some processor without any
					// which would be an unknown type.
					res_family = Intel_Unknown;
				}

                if (cache_info.UnifiedLevel2)
                {
				    if (cache_info.Level2UnifiedCacheSize == 128*1024)
				    {
					    // 128K Level 2 cahce is a CeleronA
					    res_family = Intel_CeleronA;
				    }
				    else
				    {
					    // Mobile Pentium II
					    res_family = Intel_MobilePentiumII;
				    }
                }
                else
                {
                    // No idea what this processor is
					res_family = Intel_Unknown;
                }
				break;

			case 0x07:
				// This could be a PentiumIII or a PentiumIII zeon, we need to look at
				// the cache size to be sure.
				if (!get_cache_info(processor,cache_info))
				{
					// Could not get the cahce info which is a serious error but
					// we will take the safe bet of a Pentium III
					res_family = Intel_PentiumIII;
				}

                if (cache_info.UnifiedLevel2)
                {
				    if (cache_info.Level2UnifiedCacheSize>=1024*1024)
				    {
					    res_family = Intel_PentiumIIIXeon;
				    }
				    else
				    {
					    res_family = Intel_PentiumIII;
				    }
                }
                else
                {
                    // We have a non-unified Level 2 cache, I have no idea what it is!
                    res_family = Intel_Unknown;
                }
				break;

			default:
				// If we do not know the model we know it is a family 6 so return PentiumPro
				res_family = Intel_PentiumPro;
			}
		}
		else
		{
			res_family = Intel_Unknown;
		}
	}
	else
	{
		// signature failed, all we know is that we are some make of Intel
		res_family = Intel_Unknown;
	}
}


//------------------------------------------------------------------------------
void Detect::decode_amd_signature
(
	uint32_t			processor,
	ProcessorFamily&	res_family
)
//-------------------------------------
{
	uint32_t	family;
	uint32_t	model;
	uint32_t	stepping;
	uint32_t	type;

	if (get_signature_components(processor,family,model,stepping,type))
	{
        if (family == 4)
        {
            res_family = AMD_AM486;
        }
        else if (family == 5)
        {
            if (model <= 3)
            {
                res_family = AMD_K5;
            }
            else if ((model == 6) || (model == 7))
            {
                res_family = AMD_K6;
            }
            else if (model == 8)
            {
                res_family = AMD_K62;
            }
            else if (model == 9)
            {
                res_family = AMD_K63;
            }
            else
            {
                res_family = AMD_Unknown;
            }
        }
        else if (family == 6)
        {
            res_family = AMD_K7;
        }
        else
        {
            // Family is above 6 so we have no idea what it is
		    res_family = AMD_Unknown;
        }
    }
    else
    {
		// signature failed, all we know is that we are some make of AMD
		res_family = AMD_Unknown;
    }
}


//------------------------------------------------------------------------------
void Detect::decode_cyrix_signature
(
	uint32_t			processor,
	ProcessorFamily&	res_family
)
//-------------------------------------
{
	uint32_t	family;
	uint32_t	model;
	uint32_t	stepping;
	uint32_t	type;

	if (get_signature_components(processor,family,model,stepping,type))
	{
        if (family == 4)
        {
            if (model == 4)
            {
                res_family = Cyrix_MediaGX;
            }
            else
            {
                res_family = Cyrix_Unknown;
            }
        }
        else if (family == 5)
        {
            if (model == 0)
            {
                res_family = Cyrix_586;
            }
            else if (model == 2)
            {
                res_family = Cyrix_686;
            }
            else if (model == 4)
            {
                res_family = Cyrix_GXm;
            }
            else
            {
                res_family = Cyrix_Unknown;
            }
        }
        else if (family == 6)
        {
            if (model == 0)
            {
                res_family = Cyrix_686MX;
            }
            else
            {
                res_family = Cyrix_Unknown;
            }
        }
        else
        {
            // Family is above 6 so we have no idea what it is
		    res_family = Cyrix_Unknown;
        }
    }
    else
    {
		// signature failed, all we know is that we are some make of Cyrix
		res_family = Cyrix_Unknown;
    }
}


//------------------------------------------------------------------------------
void Detect::decode_idt_signature
(
	uint32_t			processor,
	ProcessorFamily&	res_family
)
//-------------------------------------
{
	uint32_t	family;
	uint32_t	model;
	uint32_t	stepping;
	uint32_t	type;


	if (get_signature_components(processor,family,model,stepping,type))
	{
        if (family == 4)
        {
            // IDT do not make a family 4 processor
		    res_family = IDT_Unknown;
        }
        else if (family == 5)
        {
            if (model == 4)
            {
                res_family = IDT_WINCHIPC6;
            }
            else if (model == 8)
            {
                res_family = IDT_WINCHIP2;
            }
            else
            {
                res_family = IDT_Unknown;
            }
        }
        else if (family == 6)
        {
            // IDT do not make a family 6 processor
            res_family = IDT_Unknown;
        }
        else
        {
            // Family is above 6 so we have no idea what it is
		    res_family = IDT_Unknown;
        }
    }
    else
    {
		// signature failed, all we know is that we are some make of IDT
		res_family = IDT_Unknown;
    }
}

//------------------------------------------------------------------------------
// This code is a modied version of a similar function found in some very
// early Intel detection code from an early IAL CD. This is the most reliable
// method of detecting the processor speed I have found because it considers
// numerical limits.
float Detect::measure_clock_speed
(
)
//-------------------------------------
{
	uint32_t	    ticks;              // Microseconds elapsed during test
	uint32_t	    cycles;             // Clock cycles elapsed during test
	uint32_t	    stamp0, stamp1;     // Time Stamp Variable for beginning and end of test											
    uint32_t        freq = 0;
	uint32_t        freq2 =0;           // 2nd most current frequ. calc.
	uint32_t        freq3 =0;           // 3rd most current frequ. calc.
	uint32_t        total;              // Sum of previous three frequency calculations
	int32_t         tries=0;            // Number of times a calculation has been made on this call
	LARGE_INTEGER   t0,t1;              // Variables for High-Resolution Performance Counter reads					
	LARGE_INTEGER   count_freq;           // High Resolution Performance Counter frequency

	// Checks whether the high-resolution counter exists and returns an error if it does not exist.
	if (!QueryPerformanceFrequency( &count_freq ) )
	{
		return 0.0f;
	}

    // Get a copy of the current thread and process priorities
    uint32_t priority_class     = GetPriorityClass(GetCurrentProcess());
    int32_t  thread_priority    = GetThreadPriority(GetCurrentThread());

    // Make this thread the highest possible priority so we get the best timing
//    SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
//    SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);

	// Xompare elapsed time on the High-Resolution Counter with elapsed cycles on the Time Stamp Register.
	do 
	{
		// This do loop runs up to 20 times or until the average of the previous  three calculated
		// frequencies is within 1 MHz of each of the individual calculated frequencies. 
		// This resampling increases the accuracy of the results since outside factors could affect
		// this calculation			

		tries++;        //Increment number of times sampled on this call to cpuspeed
		freq3 = freq2;  //Shift frequencies back to make
		freq2 = freq;   //room for new frequency measurement

        //Get high-resolution performance counter time
        QueryPerformanceCounter(&t0); 
		
		t1.LowPart = t0.LowPart;		// Set Initial time
	  	t1.HighPart = t0.HighPart;

		// Loop until 50 ticks have passed since last read of hi-res counter.
		// This accounts for overhead later.
    	while ( (uint32_t)t1.LowPart - (uint32_t)t0.LowPart<50) 
		{	  
			QueryPerformanceCounter(&t1);
		}
		
   		_asm
        {
			rdtsc
        	mov stamp0, EAX
   		}

		t0.LowPart = t1.LowPart;		// Reset Initial 
		t0.HighPart = t1.HighPart;		//   Time

    	// Loop until 1000 ticks have passed since last read of hi-res counter. 
		// This allows for elapsed time for sampling.
    	while ((uint32_t)t1.LowPart-(uint32_t)t0.LowPart<1000 ) 
		{				
    		QueryPerformanceCounter(&t1);
   		}
			
		_asm
	    {
			rdtsc
    	    mov		stamp1, EAX
        }

        cycles = stamp1 - stamp0;
    	ticks = (uint32_t) t1.LowPart - (uint32_t) t0.LowPart;	

		// Note that some seemingly arbitrary mulitplies and divides are done below.
		// This is to maintain a high level of precision without truncating the most 
		// significant data. 

		ticks = ticks * 100000;				
		ticks = ticks / ( count_freq.LowPart/10 );		
		
		if ( ticks%count_freq.LowPart > count_freq.LowPart/2 )
		{				
			ticks++;			// Round up if necessary
		}
		
		freq = cycles/ticks;	// Cycles / us  = MHz
        									
        if ( cycles%ticks > ticks/2 )
		{
          	freq++;				// Round up if necessary
		}
          	
		total = ( freq + freq2 + freq3 );
			
	} while (	(tries < 3 ) || (tries < 20) && ((abs(3 * freq -total) > 3) ||
		(abs(3 * freq2-total) > 3) || (abs(3 * freq3-total) > 3)));
		
	if ( total / 3  !=  ( total + 1 ) / 3 )
	{
		total ++; 				// Round up if necessary
	}

	// restore the thread priority
//	SetPriorityClass(GetCurrentProcess(), priority_class);
//	SetThreadPriority(GetCurrentThread(), thread_priority);

    return float(total) / 3.0f;
}


//------------------------------------------------------------------------------
bool Detect::get_number_of_processors
(
    uint32_t&   number_of_processors,
    uint32_t&   processors_available
)
//-------------------------------------
{
    if (!Setup)
        return false;

    number_of_processors = Processors;
    processors_available = ProcessProcessors;

    return true;
}


//------------------------------------------------------------------------------
bool Detect::get_cpuid_limits
(
    uint32_t 			processor, 
    uint32_t& 			normal, 
    uint32_t& 			extended
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

    normal      = ProcessorData[processor]->MaxStandardCPUID;
    extended    = ProcessorData[processor]->MaxExtendedCPUID;

    return true;
}


//------------------------------------------------------------------------------
bool Detect::get_cpuid_data
(
    uint32_t 			processor, 
    uint32_t 			cmd, 
    Registers&	        result
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

    if (cmd & 0x80000000)
    {
        //
        // An extended CPUID function has been issued.
        //
        if (cmd > ProcessorData[processor]->MaxExtendedCPUID)
        {
            return false;
        }

        // the requested extended command is valid so fill the result struct.
        result = ProcessorData[processor]->ExtendedCPUIDRegisters[cmd&0x7fffffff];
    }
    else
    {
        //
        // A standard CPUID function has been issued.
        //
        if (cmd > ProcessorData[processor]->MaxStandardCPUID)
        {
            return false;
        }

        // the requested command is valid so fill the result struct.
        result = ProcessorData[processor]->StandardCPUIDRegisters[cmd];
    }

    return true;
}


//------------------------------------------------------------------------------
bool Detect::get_manufacturer
(
    uint32_t                processor, 
    ProcessorManufacturer&  man
)
//-------------------------------------
{
    char    intel_id[]  = "GenuineIntel";
    char    amd_id[]    = "AuthenticAMD";
    char    cyrix_id[]  = "CyrixInstead";
    char    idt_id[]    = "CentaurHauls";

    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	if (ProcessorInfo[processor]->CPUIDPresent)
	{
		if (strncmp(intel_id,reinterpret_cast<const char*>(&ProcessorData[processor]->StandardCPUIDRegisters[0].RegEBX),12) == 0)
		{
			// This processor is an Intel
			man = Intel;
		}
		else if (strncmp(amd_id,reinterpret_cast<const char*>(&ProcessorData[processor]->StandardCPUIDRegisters[0].RegEBX),12) == 0)
		{
			// This processor is an AMD
			man = AMD;
		}
		else if (strncmp(cyrix_id,reinterpret_cast<const char*>(&ProcessorData[processor]->StandardCPUIDRegisters[0].RegEBX),12) == 0)
		{
			// This processor is an Cyrix
			man = Cyrix;
		}
		else if (strncmp(idt_id,reinterpret_cast<const char*>(&ProcessorData[processor]->StandardCPUIDRegisters[0].RegEBX),12) == 0)
		{
			// This processor is an IDT
			man = IDT;
		}
		else
		{
			// We have been through all the known makes and its not one of those so its unknown.
			man = Unknown_Manufacturer;
		}
	}
	else
	{
		// No CPUID so the make is either unknown or a Cyrix
		man = ProcessorInfo[processor]->NoCPUIDMake;
	}

	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_features
(
    uint32_t 				processor, 
    uint32_t&				features
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	features = 0;
	detect_standard_features(processor, features);
	detect_extended_features(processor, features);
	detect_simd_features(processor, features);

	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_signature_components
(
	uint32_t 				processor, 
	uint32_t&				family,
	uint32_t&				model,
	uint32_t&				stepping,
	uint32_t&				type
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	if (!ProcessorInfo[processor]->CPUIDPresent)
		return false;

	family	= (ProcessorData[processor]->StandardCPUIDRegisters[1].RegEAX & 0xf00)>>8;
	model	= (ProcessorData[processor]->StandardCPUIDRegisters[1].RegEAX & 0xf0)>>4;
	stepping= (ProcessorData[processor]->StandardCPUIDRegisters[1].RegEAX & 0xf);
	type	= (ProcessorData[processor]->StandardCPUIDRegisters[1].RegEAX & 0x3000)>>12;

	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_family
(
	uint32_t 				processor, 
	ProcessorFamily&		family
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	if (ProcessorInfo[processor]->CPUIDPresent)
	{
		ProcessorManufacturer	manu;
        if (!get_manufacturer(processor,manu))
        {
			return false;
        }

		switch (manu)
		{
		case Intel:
			decode_intel_signature(processor,family);
			break;

		case AMD:
			decode_amd_signature(processor,family);
			break;

		case IDT:
			decode_idt_signature(processor,family);
			break;

		case Cyrix:
			decode_cyrix_signature(processor,family);
			break;

			// If the manfacturer is not onw of the above then we do not know how to
			// decode the signature.
		default:
			family = Unknown_Family;
			break;
		}
	}
	else
	{
        if (ProcessorInfo[processor]->NoCPUIDMake == Cyrix)
        {
            // CPUID is not present but we know this device is a Cyrix
            family = Cyrix_Unknown;
        }
        else
        {
		    // CPUID is not present so we must be a generic processor
		    family = ProcessorFamily( ProcessorInfo[processor]->NoCPUIDFamily );
        }
	}

	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_cache_info
(
	uint32_t 				processor, 
	ProcessorCacheInfo&		cache
)
//-------------------------------------
{
	// Any unknwon item in the cache structure is zero so lets clear the whole
	// structure to start with.
	memset(&cache,0,sizeof(ProcessorCacheInfo));

    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	uint32_t features;

	// features failed so return an error
	if (!get_features(processor,features))
		return false;

	// there is no cache info so return 'false' It is up to the caller
	// to check the feature flag before calling this member
	if ((features & CPU_CACHEINFO) == 0)
		return false;

    // we need the manufacturer of the processor, if we cannot get it the fail
    ProcessorManufacturer   make;
    if (!get_manufacturer(processor,make))
        return false;

	// Now we need to find the cahce info, it is around somewhere either in
	// standard or extended CPUID functions.

	// look for standard info first
    if (ProcessorData[processor]->MaxStandardCPUID>=2)
	{
		// Get the cache size from the config parameters

		if ((ProcessorData[processor]->StandardCPUIDRegisters[2].RegEAX & 0xff) == 0x01)
		{
			uint8_t	config[16];

			*((uint32_t*)&config[0]) = ProcessorData[processor]->StandardCPUIDRegisters[2].RegEAX>>8;
			*((uint32_t*)&config[4]) = ProcessorData[processor]->StandardCPUIDRegisters[2].RegEBX;
			*((uint32_t*)&config[8]) = ProcessorData[processor]->StandardCPUIDRegisters[2].RegECX;
			*((uint32_t*)&config[12]) = ProcessorData[processor]->StandardCPUIDRegisters[2].RegEDX;

            // Assume all the caches are unified to start with
			uint32_t	count = 0;
            cache.UnifiedTLB = true;
            cache.UnifiedLevel1 = true;
            cache.UnifiedLevel2 = true;

			while (count < 16)
			{
				//
				// These config numbers are assigned by Intel and are from application note AP-485
				//
				switch (config[count])
				{
				case 0x01:
                    if (make == Intel)
                    {
                        cache.UnifiedTLB                    = false;
					    cache.CodeTLBAssoc4K	            = 4;
					    cache.CodeTLBEntries4K	            = 32;
                    }
					break;

				case 0x02:
                    if (make == Intel)
                    {
                        cache.UnifiedTLB                    = false;
					    cache.CodeTLBAssoc4M	            = 0xffffffff;
					    cache.CodeTLBEntries4M	            = 2;
                    }
					break;

				case 0x03:
                    if (make == Intel)
                    {
                        cache.UnifiedTLB                    = false;
	    				cache.DataTLBAssoc4K	            = 4;
		    			cache.DataTLBEntries4K	            = 64;
                    }
					break;

				case 0x04:
                    if (make == Intel)
                    {
                        cache.UnifiedTLB                    = false;
					    cache.DataTLBAssoc4M            	= 4;
					    cache.DataTLBEntries4M	            = 8;
                    }
					break;

				case 0x06:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel1                 = false;
					    cache.Level1CodeCacheSize	    	= 8*1024;
					    cache.Level1CodeCacheLineWidth  	= 32;
					    cache.Level1CodeCacheAssoc	    	= 4;
                    }
					break;

				case 0x08:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel1                 = false;
    					cache.Level1CodeCacheSize	    	= 16*1024;
	    				cache.Level1CodeCacheLineWidth  	= 32;
			    		cache.Level1CodeCacheAssoc	    	= 4;
                    }
					break;

				case 0x0A:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel1                 = false;
					    cache.Level1DataCacheSize		    = 8*1024;
					    cache.Level1DataCacheLineWidth	    = 32;
					    cache.Level1DataCacheAssoc		    = 2;
                    }
					break;

				case 0x0C:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel1                 = false;
					    cache.Level1DataCacheSize		    = 16*1024;
					    cache.Level1DataCacheLineWidth	    = 32;
					    cache.Level1DataCacheAssoc		    = 4;
                    }
					break;

				case 0x40:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel2                 = true;
					    cache.Level2UnifiedCacheSize       	= 0;
					    cache.Level2UnifiedCacheLineWidth	= 0;
					    cache.Level2UnifiedCacheAssoc		= 0;
                    }
					break;

				case 0x41:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel1                 = true;
					    cache.Level2UnifiedCacheSize		= 128*1024;
					    cache.Level2UnifiedCacheLineWidth	= 32;
					    cache.Level2UnifiedCacheAssoc		= 4;
                    }
					break;

				case 0x42:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel2                 = true;
    					cache.Level2UnifiedCacheSize		= 256*1024;
	    				cache.Level2UnifiedCacheLineWidth	= 32;
			    		cache.Level2UnifiedCacheAssoc		= 4;
                    }
					break;

				case 0x43:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel2                 = true;
					    cache.Level2UnifiedCacheSize		= 512*1024;
					    cache.Level2UnifiedCacheLineWidth	= 32;
					    cache.Level2UnifiedCacheAssoc		= 4;
                    }
					break;

				case 0x44:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel2                 = true;
					    cache.Level2UnifiedCacheSize		= 1024*1024;
					    cache.Level2UnifiedCacheLineWidth	= 32;
					    cache.Level2UnifiedCacheAssoc		= 4;
                    }
					break;

				case 0x45:
                    if (make == Intel)
                    {
                        cache.UnifiedLevel2                 = true;
					    cache.Level2UnifiedCacheSize		= 2048*1024;
					    cache.Level2UnifiedCacheLineWidth	= 32;
					    cache.Level2UnifiedCacheAssoc		= 4;
                    }
					break;

                case  0x70:
                    if (make == Cyrix)
                    {
                        cache.UnifiedTLB                    = true;
					    cache.UnifiedTLBAssoc4K	            = 4;
					    cache.UnifiedTLBEntries4K       	= 32;
                    }
                    break;

                case  0x80:
                    if (make == Cyrix)
                    {
                        cache.UnifiedLevel1                 = true;
					    cache.Level1UnifiedCacheSize		= 16*1024;
					    cache.Level1UnifiedCacheLineWidth	= 16;
					    cache.Level1UnifiedCacheAssoc		= 4;
                    }
                    break;
				}
				count++;
			}
		}
		else
		{
			// This processor does not use 8 bit descriptors so we do not know how to
			// decode it, Therefore we cannot return any cache info
			return false;
		}
	}
	else if (ProcessorData[processor]->MaxExtendedCPUID>=0x80000005)
	{
        uint32_t tlb = ProcessorData[processor]->ExtendedCPUIDRegisters[5].RegEBX;

        cache.UnifiedTLB = false;
        cache.UnifiedLevel1 = false;
        cache.UnifiedLevel2 = true;

        cache.CodeTLBEntries4K = tlb & 0xff;
        cache.CodeTLBAssoc4K = ((tlb & 0xff00)>>8)==0xff?0xffffffff:((tlb & 0xff00)>>8);
        cache.DataTLBEntries4K = (tlb & 0xff0000)>>16;
        cache.DataTLBAssoc4K = ((tlb & 0xff000000)>>24)==0xff?0xffffffff:((tlb & 0xff000000)>>24);

        // There is no 4M page entries
        cache.CodeTLBEntries4M = 0;
        cache.CodeTLBAssoc4M = 0;
        cache.DataTLBEntries4M = 0;
        cache.DataTLBAssoc4M = 0;
        
        // Level 1 data cache
        uint32_t dcache = ProcessorData[processor]->ExtendedCPUIDRegisters[5].RegECX;
        cache.Level1DataCacheSize       = ((dcache & 0xff000000)>>24)*1024;
        cache.Level1DataCacheAssoc      = ((dcache & 0xff0000)>>16)==0xff?0xffffffff:((dcache & 0xff0000)>>16);
        cache.Level1DataCacheLineWidth  = dcache & 0xff;

        uint32_t icache = ProcessorData[processor]->ExtendedCPUIDRegisters[5].RegEDX;
        cache.Level1CodeCacheSize       = ((icache & 0xff000000)>>24)*1024;
        cache.Level1CodeCacheAssoc      = ((icache & 0xff0000)>>16)==0xff?0xffffffff:((icache & 0xff0000)>>16);
        cache.Level1CodeCacheLineWidth  = icache & 0xff;

        // Do we have Level 2 cache info?
        if (ProcessorData[processor]->MaxExtendedCPUID>=0x80000006)
        {
            uint32_t l2 = ProcessorData[processor]->ExtendedCPUIDRegisters[6].RegECX;
            cache.Level2UnifiedCacheSize       = ((l2 & 0xffff0000)>>16)*1024;
            cache.Level2UnifiedCacheAssoc      = ((l2 & 0xf000)>>12)==0xf?0xffffffff:((l2 & 0xf000)>>12);
            cache.Level2UnifiedCacheLineWidth  = l2 & 0xff;
        }
	}
	else
	{
		// This processor has cache info but we can't find it
		return false;
	}

	// all went well
	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_processor_name
(
	uint32_t 			processor, 
	std::string& 		name
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	uint32_t features;

	// features failed so return an error
	if (!get_features(processor,features))
		return false;

    uint32_t	family;
    uint32_t	model;
    uint32_t	stepping;
    uint32_t	type;

    if (!get_signature_components(processor,family,model,stepping,type))
        return false;

	if ((features & CPU_CPUID) && (ProcessorData[processor]->MaxExtendedCPUID>=0x80000004))
	{
		// This processor has the name feature in the extended CPUID functions so get the
		// name from there..
        uint8_t    processor_name[49];
        processor_name[48] = 0;

        // now get the 48 bytes from the CPUID name functions 
        _asm
        {
            mov		eax,0x80000002	
            CPUID
		            
            mov		DWORD PTR [processor_name+0],eax
            mov		DWORD PTR [processor_name+4],ebx
            mov		DWORD PTR [processor_name+8],ecx
            mov		DWORD PTR [processor_name+12],edx

            mov		eax,0x80000003
            CPUID
		            
            mov		DWORD PTR [processor_name+16],eax
            mov		DWORD PTR [processor_name+20],ebx
            mov		DWORD PTR [processor_name+24],ecx
            mov		DWORD PTR [processor_name+28],edx

            mov		eax,0x80000004
            CPUID
		            
            mov		DWORD PTR [processor_name+32],eax
            mov		DWORD PTR [processor_name+36],ebx
            mov		DWORD PTR [processor_name+40],ecx
            mov		DWORD PTR [processor_name+44],edx
        }

        // copy the char array into the string, we have to cast because our array
        // is an unsigned type.
        name = reinterpret_cast<char*>(processor_name);
	}
	else
	{
		// we need to build a name.
		ProcessorFamily		fam;
		if (!get_family(processor,fam))
			return false;

		switch(fam)
		{
		case Generic_Architecture3:
		case Intel_80386:
			name = "80386";
			break;		

		case Generic_Architecture4:
		case Intel_80486:
			name = "80486 ";

			// A 486 with an FPU is a DX, otherwise its an SX
			if (features & CPU_FPUPRESENT)
			{
				name += "DX";
			}
			else
			{
				name += "SX";
			}
			break;		

		case Intel_80486_SL:
			name = "Intel 486 SL";
			break;

		case Intel_80486_DX2:
			name = "Intel Writeback Enhanced 486 DX2";
			break;

		case Intel_80486_DX4:
			name = "Intel 486 DX4";
			break;

		case Generic_Architecture5:
			name = "Generic family 5 (Pentium class) processor";
			break;

		case Generic_Architecture6:
			name = "Generic family 6 (PentiumPro class) processor";
			break;

		case Intel_Pentium:
			name = "Intel Pentium";
			break;

		case Intel_PentiumMMX:
			name = "Intel Pentium with MMX";
			break;

		case Intel_PentiumPro:
			name = "Intel Pentium Pro";
			break;

		case Intel_PentiumII:
			name = "Intel Pentium II";
			break;

		case Intel_PentiumIIXeon:
			name = "Intel Pentium II Xeon";
			break;

		case Intel_PentiumIII:
			name = "Intel Pentium III";
			break;

		case Intel_PentiumIIIXeon:
			name = "Intel Pentium III Xeon";
			break;

		case Intel_Celeron:
			name = "Intel Pentium II Celeron";
			break;

		case Intel_CeleronA:
			name = "Intel Pentium II Celeron A";
			break;

		case Intel_MobilePentium:
			name = "Intel Mobile Pentium";
			break;

		case Intel_MobilePentiumII:
			name = "Intel Mobile Pentium II";
			break;

		case Intel_Unknown:
			name = "Unknown Intel Processor";
			break;

        //
		// A lot of the processors below support the name function in the extended CPUID,
		// we name them here anyway just in case a model is made with these extended CPUID
		// functions missing.
		//
        case Cyrix_MediaGX:
            name = "Cyrix MediaGX";
            break;

        case Cyrix_GXm:
            name = "Cyrix GXm"; // this processor has CPUID name functions
            break;

        case Cyrix_586:
            name = "Cyrix 586";
            break;

        case Cyrix_686:
            name = "Cyrix 686";
            break;

        case Cyrix_686MX:
            name = "Cyrix 686MX";
            break;

        case Cyrix_Unknown:
            name = "Unknown Cyrix Processor";
            break;

        case AMD_AM486:
            name = "AMD Am486";
            break;

        case AMD_K5:
            name = "AMD K5";
            break;

        case AMD_K6:
            name = "AMD K6";            // this processor has CPUID name functions
            break;

        case AMD_K62:
            name = "AMD K62 with 3D Now!";// this processor has CPUID name functions
            break;

        case AMD_K63:
            name = "AMD K63 with 3D Now!";// this processor has CPUID name functions
            break;

        case AMD_K7:                      // this processor has CPUID name functions
            name = "AMD K7";
            break;

        case AMD_Unknown:
            name = "Unknown AMD processor";
            break;

        case IDT_WINCHIPC6:
            name = "IDT WinChip";
            break;

        case IDT_WINCHIP2:
            name = "IDT WinChip2";          // this processor has CPUID name functions
            break;

        case IDT_Unknown:
            name = "Unknown IDT processor";
            break;

		// The default is an unknown processor
		default:
			name = "Unknown processor";
			break;
		}

        if (type == 0x01)
            name += " Overdrive";
	}

	return true;
}


//------------------------------------------------------------------------------
bool Detect::get_serial_number
(
    uint32_t            processor, 
    SerialNumber&       snr
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	uint32_t features;

	// features failed so return an error
	if (!get_features(processor,features))
		return false;

    if ((features & CPU_SNR)==0)
        return false;

    // this processor has a serial number so lets copy it from the CPUID buffers..

    if (ProcessorData[processor]->MaxStandardCPUID>=3)
    {
        // The upper 32 bits of the serial number are the processor signature
        snr.Upper   = ProcessorData[processor]->StandardCPUIDRegisters[1].RegEAX;
        snr.Middle  = ProcessorData[processor]->StandardCPUIDRegisters[3].RegEDX;
        snr.Lower   = ProcessorData[processor]->StandardCPUIDRegisters[3].RegECX;
    }
    else
    {
        // Not an intel format serial number....
        return false;
    }

    return true;
}


//------------------------------------------------------------------------------
bool Detect::get_serial_number
(
    uint32_t            processor, 
    std::string&        snr_string
)
//-------------------------------------
{
    SerialNumber        snr;

    if (!get_serial_number(processor,snr))
        return false;

    // now we have the serial number, lets format it
    static char hex_chars[16] = {'0','1','2','3','4','5','6','7',
                                 '8','9','A','B','C','D','E','F'};

    uint32_t*   num = &snr.Lower;
    char        buffer[32];
    uint32_t    ind = 0;

    // go through the
    for (int dw_count = 2; dw_count>=0; dw_count--)
    {
        for (int bp=28; bp>=0; bp-=4)
        {
            DWORD nibble = (num[dw_count] >> bp) & 0x0f;

            buffer[ind++] = hex_chars[nibble];            
            if ((bp == 16) || ((bp == 0) && (dw_count!=0)) )
                buffer[ind++] = '-';
        }
    }
    buffer[ind] = 0;

    snr_string = buffer;

    return true;
}


//------------------------------------------------------------------------------
bool Detect::get_clock_frequency
(
	uint32_t 			processor, 
	float&       		res_freq
)
//-------------------------------------
{
    if (!Setup)
        return false;

    // available process processor numbers are zero based
    if (processor>ProcessProcessors-1)
        return false;

	uint32_t features;

	// features failed so return an error
	if (!get_features(processor,features))
		return false;

    // If the copied speed is 0.0 then something went wrong while
    // measuring it.
    if (ProcessorInfo[processor]->ClockSpeed == 0.0f)
        return false;

	res_freq = ProcessorInfo[processor]->ClockSpeed;

    return true;
}