////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser.cpp
//
//  Implements a parser base class and utility classes for the construction
//  of syntactic parsers.
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/03/04 03:50:26 $
//  $Revision: 1.15 $
//  $RCSfile: parser.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser.h"

#ifndef parser_perrorH
    #include "parser_perror.h"
#endif
#ifndef tool_crcH
    #include "tool_crc.h"
#endif


#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef __STDIO_H
    #include <stdio.h>
    #define __STDIO_H
#endif
#ifndef __STDARG_H
    #include <stdarg.h>
    #define __STDARG_H
#endif
#ifndef __STDLIB_H
    #include <stdlib.h>
    #define __STDLIB_H
#endif
#ifndef __ASSERT_H
    #include <assert.h>
    #define __ASSERT_H
#endif

#ifndef limitsSTL
	#include <limits>
	#define limitsSTL
#endif

#ifndef tool_strtoolsH
    #include "tool_strtools.h"
#endif

using namespace std;

namespace Parser {

////////////////////////////////////////////////////////////////////////////////
//
// General purpose exception class that returns an integer value at
// the "catch"
//
////////////////////////////////////////////////////////////////////////////////


class _PREFIXCLASS TIntException {
  public:
     TIntException(int aVal) {itsVal = aVal;};
     int Code() {return itsVal;};
     virtual ~TIntException(){};
  private:
     int itsVal;
};

////////////////////////////////////////////////////////////////////////////////
//
//  Token Structure
//
////////////////////////////////////////////////////////////////////////////////

static const char NpQUOTE = '"';
static char NpCOMMENT[] = "!";

  // single character tokens
static char  theCharTokens[]= {"@#&^<>=+-*/(){}[],:;%|"}; // DONT CHANGE: Matched to above tokens
  // Default keyword/ident. delimiters
static char  theDelimiters[]= {" \a\b\f\v\n\t"};

static char theParserErrorString[MESSAGELENGTH];

static char **tempKeyWords; // Temporary pointer to keywords used for setting
                                     // up for a QSort!!! used immedialtely prior to qsort

/* internal error message strings */

enum EInternalErrors {
    ERR_INT_EOF          = -1,
    ERR_INT_INTEGER      = -2,
    ERR_INT_REAL         = -3,
    ERR_INT_VALUEMISSING = -4,
    ERR_INT_TOKEN        = -5,
    ERR_INT_RANGE        = -6,
    ERR_INT_VALUESIZE    = -7,
    ERR_INT_LINETOOLONG  = -8,
    };

static char *internalerror[]={
    "End of file encountered",                     //1
    "Integer value expected",                      //2
    "Real value expected",                         //3
    "Numeric value expected",                      //4
    "Numeric value range error",                   //5
    "General token anticipation error",            //6
    "A specified integer value is too large/small",//7
    "Source line too long",                        //8
    NULL
    };



///////////////////////////////////////////////////////////////////////////////
//
// local utility functions
//
///////////////////////////////////////////////////////////////////////////////

// Check to see if the string contains an
// exponential floating point number

static int IsExpPrefix(char *dest)
{
  int   dec_count = 0;
  int   num_count = 0;
  int   len       = strlen(dest);
  char *d         = dest;

  while (*d)
  {
     if (isdigit(*d)) num_count++;
     else if (*d=='.') dec_count++;
     d++;
  }
  if (toupper(dest[len-1])=='E'  && dec_count <= 1
        && num_count+dec_count==len-1) return 1;
  else return 0;
}


char *Pad(char *str, int pos, int length)
{

    // Pad a string with blamks either left or right justified

    static int  buffidx=0;
    enum { MAX_PAD_STRINGS = 3 };
    static char buffers[MAX_PAD_STRINGS][42];
    char   *tempstr;

    char blanks[]="                                             ";

    // Make sure we dont overflow our buffers
    int strlength = strlen(str);
    if (length > 40) length = 40;
    if (strlength >= length) return str;

    buffidx = (buffidx < MAX_PAD_STRINGS-1) ? buffidx + 1 : 0;
    tempstr = buffers[buffidx];

    tempstr[0]=0;

    if (pos==posTRAILING_BLANKS)
    {
      strncpy(tempstr,str,length);
      strncat(tempstr,blanks,length-strlength);
    } else
    {
      strncpy(tempstr,blanks,length-strlength);
      tempstr[length-strlength]=0;
      strncat(tempstr,str,length);
    }
    return tempstr;
}


ostream &Indent(ostream* os, int indSize)
{
  for (int i=0; i < indSize; i++) *os << ' ';
  return *os;
}

_PREFIXFUNC void  AppendCRLF(char *str)
{
  int len;
  if (str)
  {
     len = strlen(str);
     str[len]   = 0x0d;
     str[len+1] = 0x0a;
    str[len+2] = 0x00;

  }
}

///////////////////////////////////////////////////////////////////////////////
//
// TToken
//
///////////////////////////////////////////////////////////////////////////////


int TToken::ToInt() const
{
 switch (id)
 {
    case T_HEX        :
    case T_INT        : return (int) value.ival;
    default           : throw TIntException((int)ERR_INT_INTEGER); //AbortParse(ERR_INT_INTEGER);
 }
}


double TToken::ToReal() const
{
 switch (id)
 {
    case T_REAL       : return value.rval;
    case T_HEX        :
    case T_INT        : return (double) value.ival;
    default           : throw TIntException((int)ERR_INT_REAL); //AbortParse(ERR_INT_REAL);
 }
}

void TToken::CheckRange(ERangeCheckStyle r_chk_type, double r0, ...) const
{
  double     r1;
  double     val;
  va_list    ap;
  val = ToReal();
  switch (r_chk_type)
  {
    case
      R_ABOVE:   if (val < r0)
                     {
                        sprintf(theParserErrorString,
                            "Range Error: value must be above %0.4lf",r0);
                        throw TIntException((int)ERR_INT_RANGE);
                     } break;
    case
      R_BELOW:   if (val > r0)
                     {
                        sprintf(theParserErrorString,
                            "Range Error: value must be below %0.4lf",r0);
                        throw TIntException((int)ERR_INT_RANGE);
                     } break;
    case
      R_INSIDE:  va_start(ap, r0);
                     r1 = (double) va_arg(ap, double);
                     va_end(ap);
                     if (val < r0 || val > r1)
                     {
                        sprintf(theParserErrorString,
                            "Range Error: value must be between %0.4lf and %0.4lf"
                             ,r0,r1);
                        throw TIntException((int)ERR_INT_RANGE);
                     } break;
    case
      R_OUTSIDE: va_start(ap, r0);
                     r1 = (double) va_arg(ap, double);
                     va_end(ap);
                     if (val > r0 && val < r1)
                     {
                        sprintf(theParserErrorString,
                            "Range Error: value must not be between %0.4lf and %0.4lf"
                             ,r0,r1);
                  throw TIntException((int)ERR_INT_RANGE);
                     } break;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// local utility functions
//
///////////////////////////////////////////////////////////////////////////////

TLineInp::TLineInp():itsTokenCount(0),itsLineCount(0), itsCurrLineLength(0)
{
  itsCurrLineText[0] = 0;
  startSave = 0;
  destSave = 0;
  worklenSave = 0;

  itsTokenStack = new TToken[MAX_TOK_STACK];
};

TLineInp::~TLineInp()
{
    delete[] itsTokenStack;
}

void  TLineInp::ClearTokenStack()
{
  itsTokenCount = 0;
}

int TLineInp::PushToken(TToken t)
{
  if (itsTokenCount >= MAX_TOK_STACK) return 0;
  itsTokenStack[itsTokenCount++] = t;
  return 1;
}

int TLineInp::PopToken(TToken& t)
{
  if (itsTokenCount <= 0) return 0;
  t = itsTokenStack[--itsTokenCount];
  return 1;
}

int TLineInp::TokenStackEntries()
{
  return itsTokenCount;
}



////////////////////////////////////////////////////////////////////////////////
//
//
////////////////////////////////////////////////////////////////////////////////



static int QsortFunc(const void *elem1, const void *elem2)
{
	return tool::StrCmpNoCase(tempKeyWords[*((int *)elem1)],tempKeyWords[*((int *)elem2)]);
}

TParseObjectBase::TParseObjectBase():
  itsKeyWords(0), itsErrorStrings(0),itsKeyWordCount(0), itsErrorStrCount(0),
  itsKeyWordIdx(0),itsFirstLineFlag(1),itsCommentPos(-1), itsLineInput(0)
{
    currtoken = new TToken;
    itsLastError = new TParserError;       // Error report object
    itsCheckSum = new tool::TCheckSum;

    //theWorkStringBuff[0]=0;
}

TParseObjectBase::TParseObjectBase(char **keyWords, char **errStrs):
  itsKeyWords(0), itsErrorStrings(0),itsKeyWordCount(0), itsErrorStrCount(0),
  itsKeyWordIdx(0), itsCommentPos(-1), itsDefltPadLength(0)
{
    currtoken = new TToken;
    itsLastError = new TParserError;       // Error report object
    itsCheckSum = new tool::TCheckSum;
    SetStringTables(keyWords, errStrs);
}


TParseObjectBase::~TParseObjectBase()
{
    delete currtoken;
    delete itsLastError;
    delete itsCheckSum;
    if (itsKeyWordIdx) free(itsKeyWordIdx);
    itsKeyWordIdx = 0;
}

void TParseObjectBase::SetDefltPadLength(int len)
{
  itsDefltPadLength = (len > 40)?40:len;
}

const char* TParseObjectBase::LPaddedKeyWord(const int id, const int padlen)
{
  int len;
  assert(id >=0 && id <= itsKeyWordCount);
  if (padlen < 0)
     len = itsDefltPadLength;
  else
     len = (padlen > 40)?40:padlen;
  return Pad(itsKeyWords[id],posLEADING_BLANKS,len);
}

const char* TParseObjectBase::PaddedKeyWord(const int id, const int padlen)
{
  int len;
  assert(id >=0 && id <= itsKeyWordCount);
  if (padlen < 0)
     len = itsDefltPadLength;
  else
     len = (padlen > 40)?40:padlen;
  return Pad(itsKeyWords[id],posTRAILING_BLANKS,len);
}


const char* TParseObjectBase::KeyWord(int id)
{
  static char str[100];
  static char *doubleTokens[] = {":=","<>",">=","<="};	

  if (id >=0 && id <= itsKeyWordCount)
	return itsKeyWords[id];
  else if (id >= T_AT && id <= T_LINE)
  {
	str[0] = theCharTokens[id - T_AT];	
	str[1] = 0;
	return str;
  } else if (id >= T_ASSIGN && id <= T_LE)
  {
	return doubleTokens[id - T_ASSIGN];
  }	else
	  return "";
}


static int CountEntries(char **aStringTable)
{
  for (int i=0; 1 ;i++ )
     if (aStringTable[i] == 0)
        return i;
}

int TParseObjectBase::SetStringTables(char **aKeyWordList, char **anErrorMsgList)
{
  int i;
  itsKeyWords      = aKeyWordList;
  itsErrorStrings  = anErrorMsgList;
  itsKeyWordCount  = CountEntries(itsKeyWords);
  itsErrorStrCount = CountEntries(itsErrorStrings);
  if (itsKeyWordIdx) free(itsKeyWordIdx);
  itsKeyWordIdx = (int *) calloc(itsKeyWordCount, sizeof(int));
  if (!itsKeyWordIdx) return 0;
  for (i=0; i <  itsKeyWordCount; i++) itsKeyWordIdx[i] = i;

  tempKeyWords = itsKeyWords;
  qsort(itsKeyWordIdx, itsKeyWordCount, sizeof(int), QsortFunc);
  return 1;
}

int TParseObjectBase::GetKeywordIdx(const char *aKeyWord)
{
  int mid,cmp;
  int low = 0;
  int high = itsKeyWordCount-1;

  while(1)
  {
     if ( low > high)
        return -1;
     mid = (low + high) / 2;
     cmp = tool::StrCmpNoCase(aKeyWord, itsKeyWords[ itsKeyWordIdx[mid] ]);
     if (cmp < 0) high = mid - 1;
        else if (cmp > 0) low = mid + 1;
          else return mid;
  }
}


// Return the current line number we are parsing
// in the source line

int TParseObjectBase::CurrSourceLineNumber()
{
  return itsLineInput->GetLineCount();
}


///////////////////////////////////////////////////////////////////////////////


/***********************************************
* Return the next word in the input string
* and the corresponding token id in prelim_token
* if it can be determined at this early stage
* ie operators and strings can be identified
* immediately whereas identifiers have to be
* identified by doing a binary search on the
* symbol table - we do that later
***********************************************/

char * TParseObjectBase::GetTokenString(const char *string, int *prelim_token)
{
  static char *start;
  static char *dest;
  static int   worklen;
  char         ch1;
  char        *single;
  int          single_offs;
  char        *result=NULL;
  char        *line;
  int          linelen;

  assert(itsLineInput!=0);

  start = itsLineInput->startSave;
  dest = itsLineInput->destSave;
  worklen = itsLineInput->worklenSave;

  // hide the comment by setting EOL "\0" at the comment position

  line = itsLineInput->CurrLine();
  linelen = strlen(line);

  assert(line);
  assert(itsCommentPos <= linelen);

  if (string && itsCommentPos!=-1)
     line[itsCommentPos] = 0;

  *prelim_token = T_UNKNOWN;
  if (string)
  {
     start = line;
     worklen = strlen(line);
  }
  dest=theTokenStringBuff;
  if (!start)  goto Done;
  if (*start==0) goto Done;
  while (strchr(theDelimiters, *start) && start - itsLineInput->CurrLine() < worklen)
     start++;
  if (start >= itsLineInput->CurrLine() + worklen) goto Done;

  if (*start==NpQUOTE)
  {
     *dest++=*start++;
     while (*start!=NpQUOTE && start - itsLineInput->CurrLine()  < worklen){
        *dest++=*start++;
     }
     start++;
     *dest++=NpQUOTE;
     *dest=0;
     *prelim_token = T_STRING;
  } else if ((single=strchr(theCharTokens,*start))!=NULL )
  {
     single_offs=(int)(single-theCharTokens);
     *prelim_token = T_AT + single_offs;
     ch1=*dest++=*start++;
     switch (ch1)
     {
        case ':': if (*start == '=')
                     {
          *dest++ = *start++;
          *prelim_token = T_ASSIGN;
        }
        break;

        case '>': if (*start == '=')
                     {
          *dest++ = *start++;
          *prelim_token = T_GE;
        }
        break;
        case '<': if (*start == '=')
                     {
          *dest++ = *start++;
          *prelim_token = T_LE;
        } else if (*start == '>')
                     {
          *dest++ = *start++;
          *prelim_token = T_NE;
        }
        break;
        default : break;
     }
     *dest=0;
  } else {
     resume_tok:
     while (!strchr(theDelimiters,*start) &&
        !strchr(theCharTokens,*start) &&
        *start!=NpQUOTE
        && start - itsLineInput->CurrLine()  < worklen)
     {
        *dest++=*start++;
     }
     *dest=0;
     if (IsExpPrefix(theTokenStringBuff))
     {
			if (*start=='+' || *start=='-' )
			{
				*dest++=*start++;
				goto resume_tok;
			}
     }
  }
  result = theTokenStringBuff;
Done:

  // restore the comment by setting EOL "\0" back to the comment character

  if (string && itsCommentPos!=-1)
      itsLineInput->CurrLine()[itsCommentPos] = NpCOMMENT[0];

  itsLineInput->startSave = start;
  itsLineInput->destSave = dest;
  itsLineInput->worklenSave = worklen;


  return result;
}


/*******************************************************
*  Tokenize an input word initially scanned by GetTokenString and
*  return the corresponding token structure
********************************************************/
TToken TParseObjectBase::Tokenize(char * tokenstr, int prelim_tokenid)
{
  int decimal = 0;
  int exponent = 0;
  int matchid;
  int i;
  TToken matchtoken;
  char*  tempWorkString = theTokenStringBuff;
  char*  chindex;
  long   workLong;

  /*** check first for the simple case of a quoted string ****/

  if (tokenstr[0]==NpQUOTE)
  {
     strcpy(tempWorkString,tokenstr+1);
     tempWorkString[strlen(tempWorkString)-1]=0;
     matchtoken.id=T_STRING;
     strcpy(matchtoken.str,tempWorkString);
     return matchtoken;
  }

  /*** if we have already ascertained the token the input word
         represents, simply construct the token structure and return it ***/

  strcpy(tempWorkString,tokenstr);
  if (prelim_tokenid!=T_UNKNOWN)
  {
     strcpy(matchtoken.str,tempWorkString);
     matchtoken.id=prelim_tokenid;
     return matchtoken;
  }


  /******** Try to match the input word with a keyword */

  for (i=0;i< (int) strlen(tempWorkString); i++)
     if(islower(tempWorkString[i])) tempWorkString[i]= (char) toupper(tempWorkString[i]);

  if ((matchid=GetKeywordIdx(tempWorkString))>-1)
  {
     strcpy(matchtoken.str,itsKeyWords[itsKeyWordIdx[matchid]]);
     matchtoken.id = itsKeyWordIdx[matchid];
     return matchtoken;
  }


  /******** if not matched then see if it is an integer or real value */
  /*        lets assume it is an integer to start with                */

  strcpy(matchtoken.str,tokenstr);
  matchtoken.id = T_INT;
  chindex = matchtoken.str;

  /******** check the single character string case to see if numeric */

  if (strlen(tokenstr) == 1 && !isdigit(*chindex))
  {
     /******** we cannot identify this string as a token */
     strcpy(matchtoken.str,tokenstr);
     matchtoken.id = T_UNKNOWN;
     return matchtoken;
  };


  if (matchtoken.str[0]=='0' && toupper(matchtoken.str[1])=='X')
  {
     int i,j=1,k=strlen(matchtoken.str);
     if (k<3) j=0;
     else for (i=2;i<k;i++)
        if (!isxdigit(matchtoken.str[i]))
        {
    j=0;
    break;
        }
     if (j)
     {
        matchtoken.id = T_HEX;
        goto getvalue;
     }
  }

  /******** take care of sign */

  if (*chindex == '-' || *chindex == '+') chindex++;

  /******** look at the rest of the string to identify the string as one of */
  /*        type T_INT, T_REAL or T_UNKNOWN                                 */

  while(*chindex)
  {
     if (*chindex == '.')
     {
        if (decimal)
        {
            strcpy(matchtoken.str,tokenstr);     /* This is the second "."  */
            matchtoken.id = T_UNKNOWN;           /* neither T_REAL or T_INT */
            return matchtoken;                   /* => return T_UNKNOWN     */
        } else
        {
            decimal = 1;                         /* This is the first "."   */
            strcpy(matchtoken.str,tokenstr);     /* assume T_REAL for now   */
            matchtoken.id = T_REAL;              /* but continue checking   */
        }
     } else if (*chindex == 'e' || *chindex == 'E')
     {
        if (exponent)
        {
            strcpy(matchtoken.str,tokenstr);    /* This is the second "e" */
            matchtoken.id = T_UNKNOWN;          /* hence not T_REAL       */
            return matchtoken;                  /* => return T_UNKNOWN     */
        } else
        {
            decimal = 1;
            exponent = 1;
            strcpy(matchtoken.str,tokenstr);
            matchtoken.id = T_REAL;
            if (*(chindex+1) == '+' || *(chindex+1) == '-') chindex++;

//              else matchtoken.id = T_UNKNOWN;    /* we need a sign for exponent */

				 else if (!isdigit(*(chindex+1)) )
               matchtoken.id = T_UNKNOWN;    

        }
     } else if(isdigit(*chindex))
     {

        /******** do nothing */

     } else if(!isdigit(*chindex))
     {
        strcpy(matchtoken.str,tokenstr);     /* The character encountered     */
        matchtoken.id = T_UNKNOWN;           /* is not part of a numeric value*/
        return matchtoken;                   /* => return T_UNKNOWN     */
     }
     chindex++;
  }

  getvalue:

  /******** having found a numeric token, set the value of the token */

  switch (matchtoken.id)
  {
     case T_INT :workLong = atol(matchtoken.str);
		 if (workLong > std::numeric_limits<int>::max() || 
			 workLong < std::numeric_limits<int>::min())
                {
                  //AbortParse(ERR_INT_VALUESIZE);
                  matchtoken.id = T_REAL;
                  matchtoken.value.rval = atof(matchtoken.str);
                } else
                       matchtoken.value.ival = (int) workLong;
                     break;
     case T_REAL:matchtoken.value.rval = atof(matchtoken.str);
                     break;
     case T_HEX :sscanf(matchtoken.str+2,"%X",&(matchtoken.value.ival));
                     break;
  }
  return matchtoken;
}


/******** read a line from the source file and strip out any comments */
/*        keep a copy of the original string for the error display */

int TParseObjectBase::AdvanceSourceLine()
{
     int len;
     char *line;
    itsCommentPos = -1;
     if (!itsLineInput) return -1;
     if (itsLineInput->AdvanceLine())
        return -1;
     if ((len = itsLineInput->CurrLineLength()) == LINELENGTH)
        AbortParse(ERR_INT_LINETOOLONG);
     line = itsLineInput->CurrLine();
     if (line[len-1]=='\n') line[len-1]=0;

    ///////

    itsCheckSum->Update(line);

    ///////

     if (len != 0)
     {
         if (line[0]==NpCOMMENT[0])
            itsCommentPos =  0;
         else if ((itsCommentPos=strcspn(line,NpCOMMENT)) >= len-1)
             itsCommentPos = -1;
     }
     return itsLineInput->GetLineCount();
}


void TParseObjectBase::AbortParse(const int errnum, const char *aUserMessage)
{
  int erridx;
  int error_set=(errnum >= 0);

  erridx=(errnum<0)?abs(errnum)+itsErrorStrCount:errnum;
  if (!aUserMessage)
  {
          if (error_set)
             if (erridx < itsErrorStrCount)
                 strncpy(theParserErrorString,itsErrorStrings[erridx],MESSAGELENGTH);
			 else
                 strcpy(theParserErrorString,"Undefined Error");
          else
              strncpy(theParserErrorString,internalerror[-1-errnum],MESSAGELENGTH);
  }  else {
     if ((char  *) theParserErrorString != (char  *)aUserMessage)
        strncpy(theParserErrorString,aUserMessage,MESSAGELENGTH);
  }

  // strncpy doesnt necessarily put terminating nulls, need to do it ourselves,
  // just in case.

  theParserErrorString[MESSAGELENGTH-1] = 0;

  throw TParserError(erridx, itsLineInput->GetLineCount(),
                            itsLineInput->CurrLine(), theParserErrorString);
}


void TParseObjectBase::AbortParse(const TParserError & aParseError)
{
	throw TParserError(aParseError.Id(), aParseError.Pos(), 
		   aParseError.Line(), aParseError.Desc());
}


/******* Replace a token on the input stream ******/

void TParseObjectBase::ReplaceToken()
{

  if (itsLineInput->TokenStackEntries() < TLineInp::MAX_TOK_STACK)
     itsLineInput->PushToken(*currtoken);
}

/******** get the next token from the source file *****/

void TParseObjectBase::GetToken()
{
    int temp_tokenid;
    int getline_result;
    char * tokenstr;
    if (!itsLineInput->TokenStackEntries())
    {
      if (( tokenstr = GetTokenString(itsFirstLineFlag?itsLineInput->CurrLine():
              NULL,&temp_tokenid)) == NULL )
      {
         itsFirstLineFlag = 0;
         while (tokenstr == NULL)
         {
            getline_result = AdvanceSourceLine();
            if (getline_result==-1)
            {
              *currtoken = Tokenize("",T_EOF);
              return;
            }
            tokenstr = GetTokenString(itsLineInput->CurrLine(),&temp_tokenid);
         }
      }
      *currtoken = Tokenize(tokenstr, temp_tokenid);
    }
    else
    {
      itsLineInput->PopToken(*currtoken);

      /* Just in case we changed kewordArrays in the mean time we want
          to regenerate a token from scratch.. ie just in case such a
          token no longer exists                                       */

      if (currtoken->id < T_AT && currtoken->id > -1 || currtoken->id == T_UNKNOWN)
         *currtoken=Tokenize(currtoken->str, T_UNKNOWN);
    }
}

void TParseObjectBase::PeekToken()
{
  GetToken();
  ReplaceToken();
}

void TParseObjectBase::GetToken(int token_id,int user_error)
{
  static char *internal_syms[] = {"INT","REAL","HEX","Unrecognised","String",
                                             "Identifier",":=","<>",">=","<="};

  GetToken();
  if (currtoken->id!=token_id)
  {
     if (user_error) AbortParse(user_error);
     if (token_id < itsKeyWordCount)
        sprintf(theParserErrorString,"\"%s\" keyword expected",itsKeyWords[token_id]);
     else if (token_id>=T_INT&&token_id<=T_LE)
        sprintf(theParserErrorString,"%s expected",internal_syms[token_id-T_INT]);
     else if (token_id>=T_AT && token_id<=T_LINE)
        sprintf(theParserErrorString,"\"%c\" symbol expected",theCharTokens[token_id-T_AT]);
     else
        AbortParse(ERR_INT_TOKEN,theParserErrorString);
     AbortParse(1,theParserErrorString);
  }
}


/*********************************************************
* parse a text buffer containing source code for a user
*     defined language
*
* pointer to a currently open file
*
***********************************************************/

int TParseObjectBase::ParseSource(TLineInp *li, int continuation)
{
  int err=0;
  TLineInp *last_line_inp = itsLineInput;
  itsLineInput = li;

  assert(itsLineInput != 0);


  if (!continuation)
  {
    itsCheckSum->Clear();
     itsLineInput->ResetLineCount();
     itsLineInput->ClearTokenStack();
     itsFirstLineFlag = 1;
     itsLineInput->CurrLine()[0]   ='\0';
    itsCommentPos=-1;
  } else
    itsFirstLineFlag = 0;


  itsLastError->Clear();
  try {
     ParseFunc();
  }
  catch (TParserError &errInfo)
  {
     err = errInfo.Id();
     itsLastError->Set(errInfo);
  }
  catch (TIntException &intExc)
  {
     err = intExc.Code();
     itsLastError->Set(err, itsLineInput->GetLineCount(), itsLineInput->CurrLine(),
         (intExc.Code()==ERR_INT_RANGE) ? theParserErrorString : "");
  }
  itsLineInput = last_line_inp;
  return err;
}

int TParseObjectBase::KeywordTableSelection(int table[])
{
    int   i = 0;

    while(table[i]!=-1)
    {
      if (currtoken->id == table[i]) return i;
      i++;
    }
    return -1;
}


double TParseObjectBase::GetNumericValue(int user_error)
{
  int sign=1;
    /*-------------------------------------------------------------
     * Gets the next language token, and if its numeric returns
     * its value after casting it to type double.
     * If the token found isn't numeric the parser error handler is
     * invoked with an apropriate error message
     *-------------------------------------------------------------*/

  GetToken();
  if (currtoken->id==T_ADD || currtoken->id==T_SUB)
  {
     sign=(currtoken->id==T_SUB)*-1;
     GetToken();
  }
  switch(currtoken->id)
  {
     case T_INT     :
     case T_HEX     :
     case T_REAL    : return sign*currtoken->ToReal();
     default        : if (user_error)
                          AbortParse(user_error);
                      else
                          AbortParse(ERR_INT_VALUEMISSING);
  }
  return 0.0;
}

int TParseObjectBase::GetIntValue(int user_error)
{
  int sign=1;
    /*-------------------------------------------------------------
     * Gets the next language token, and if its numeric returns
     * its value after casting it to type double.
     * If the token found isn't numeric the parser error handler is
     * invoked with an apropriate error message
     *-------------------------------------------------------------*/

  GetToken();
  if (currtoken->id==T_ADD || currtoken->id==T_SUB)
  {
     sign=(currtoken->id==T_SUB)*-1;
  }  else
     ReplaceToken();
  GetToken(T_INT);
  return sign*currtoken->ToInt();
}


void  TParseObjectBase::GetString(char *buffer, int user_error)
{
   assert(buffer!=NULL);
   GetToken(T_STRING, user_error);
   strcpy(buffer, currtoken->str);
}

void  TParseObjectBase::SkipToToken(int token_id, int user_error)
{
    do {
       GetToken();
       if (currtoken->id == token_id) return;
       if (currtoken->id == T_EOF)
       {
           ReplaceToken();
           GetToken(token_id, user_error);
       }
    } while(1);
}


TLineInp *TParseObjectBase::ParserLineInp()
{
  return itsLineInput;
}


TParseObjectBase& TParseObjectBase::operator =(TParseObjectBase& aParser)
{
  itsKeyWords = aParser.itsKeyWords;
  itsErrorStrings = aParser.itsErrorStrings;
  itsLineInput = aParser.itsLineInput;
  itsLastError = aParser.itsLastError;
  itsCheckSum = aParser.itsCheckSum;
  itsFirstLineFlag = aParser.itsFirstLineFlag;
  itsCommentPos = aParser.itsCommentPos;
  itsKeyWordCount = aParser.itsKeyWordCount;
  itsErrorStrCount = aParser.itsErrorStrCount;
  itsDefltPadLength = aParser.itsDefltPadLength;
  if (itsKeyWordIdx) free(itsKeyWordIdx);
  itsKeyWordIdx = (int *) calloc(itsKeyWordCount, sizeof(int));
  memcpy(itsKeyWordIdx, aParser.itsKeyWordIdx, itsKeyWordCount* sizeof(int));

  return *this;
}

///////////////////////////////////////////////////////////////////////////////

/*
class TIntException {
  public:
     int itsVal;
     TIntException(int aVal) {itsVal = aVal;};
} ;
*/

void TParseObjectBase::SetCommentChar(const char* ch)
{
	strcpy(NpCOMMENT, ch);
}



}; // namespace Parser
