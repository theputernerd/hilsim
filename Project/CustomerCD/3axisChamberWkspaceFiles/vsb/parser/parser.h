////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser.h
//
//  Implements a parser base class and utility classes for the construction
//  of syntactic parsers.
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/03/04 03:50:31 $
//  $Revision: 1.8 $
//  $RCSfile: parser.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parserH
#define parserH

#pragma warning (disable:4786) //identifier truncated in the debug information

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

#ifndef ostreamSTL
    #include <ostream>
    #define ostreamSTL
#endif

namespace tool {
    class TCheckSum;
};

namespace Parser {

class TToken;
class TParserError;

enum {
    LINELENGTH    = 255,    // Max source file line length
    MESSAGELENGTH = 255,    // Max error message line length
    };

enum EDefaultTokens {
     T_AT    = 970,   /* @ 970*/
     T_HASH,          /* # 971*/
     T_AMPERSAND,     /* & 972*/
     T_CARAT,         /* ^ 973*/
     T_LT,            /* < 974*/
     T_GT,            /* > 975*/
     T_EQ,            /* = 976*/
     T_ADD,           /* + 977*/
     T_SUB,           /* - 978*/
     T_MUL,           /* * 979*/
     T_DIV,           /* / 980*/
     T_LBRACK,        /* ( 981*/
     T_RBRACK,        /* ) 982*/
     T_LBRACE,        /* { 983*/
     T_RBRACE,        /* } 984*/
     T_LSQBRACK,      /* [ 985*/
     T_RSQBRACK,      /* ] 986*/
     T_COMMA,         /* , 987*/
     T_COLON,         /* : 988*/
     T_SEMICOLON,     /* ; 989*/
     T_PERCENT,       /* % 990*/
     T_LINE,          /* | 991*/

                      /* system tokens */
     T_INT,           /* 992 */
     T_REAL,          /* 993 */
     T_HEX,           /* 994 */
     T_UNKNOWN,       /* 995 */
     T_STRING,        /* 996 */
     T_IDENT,         /* 997 */

     T_ASSIGN,      /* :=  998*/
     T_NE,          /* <>  999*/
     T_GE,          /* >=  1000*/
     T_LE,          /* <=  1001*/

     T_LF,          /* New line 1002*/

     T_EOF = -1     /* End of file 1003*/
};


///////////////////////////////////////////////////////////////////////////////
//
//  Base class for a "line of text" input class
//  This is a pure virtual class and does nothing as it is - you must derrive
//  a class that actually performs some input
//  IE. You can derrive a class to read lines from files/streams character
//  buffers whatever..
//
///////////////////////////////////////////////////////////////////////////////
class _PREFIXCLASS TLineInp
{
  friend class /*__export*/ _PREFIXCLASS TParseObjectBase;

public:
  TLineInp();
  virtual ~TLineInp();

  virtual long  GetLinePos()                   {return 0L;};
  virtual int   SetLinePos(long anOffset)      {return 1;};
  virtual int   AdvanceLine()                  {return 1;};
  virtual char* CurrLine()                     {return itsCurrLineText;};
  virtual int   CurrLineLength()               {return itsCurrLineLength;};
  virtual int   GetLineCount()                 {return itsLineCount;};
  virtual void  SetLineCount(int count)       {itsLineCount = count;};
  virtual void  ResetLineCount()               {itsLineCount = 0;};
  virtual int   IsOpen()                       {return 0;};

  // Token buffer implementation: Its here because the TLineInp
  // object may be used in more than one parser, and we want the
  // token buffer (in the form of a stack) to persist from one
  // parser object to the next

  // EG. after parsing one syntax, you may replace some tokens
  // into the token input stream so they can be used when this
  // TLineInput object is used to parse the next grammar

  // Note: these are only to be accessed by the TParserBaseObject
  // members GetToken and ReplaceToken, thus we make them private
  // so no derrived class can access them and we make TParserBaseObject
  // a friend class, so it can override the protection

  enum { MAX_TOK_STACK = 5 };

protected:
  int itsLineCount;
  char itsCurrLineText[LINELENGTH+1];
  int itsCurrLineLength;

private:

  int           itsTokenCount;
  TToken*       itsTokenStack;

  void          ClearTokenStack();
  int           PushToken(TToken t);
  int           PopToken(TToken& t);
  int           TokenStackEntries();


  // TParseObjectBase::GetTokenString specific state variables
  char *startSave;
  char *destSave;
  int   worklenSave;

};

///////////////////////////////////////////////////////////////////////////////
//
// TToken
//
///////////////////////////////////////////////////////////////////////////////

class _PREFIXCLASS TToken
{
  public:
    int            id;                         // Token Identifier
    char           str[LINELENGTH];            // Token string representation
    union ValType {
        int    ival;
        double rval;
        } value;

    // The following are valid if the token is numeric ie  REAL,INT or HEX

    int            ToInt() const;                           // Return int value
    double         ToReal() const;                          // Return real value
    enum ERangeCheckStyle {R_ABOVE, R_BELOW, R_INSIDE, R_OUTSIDE};
    void           CheckRange(ERangeCheckStyle r_chk_type, double r0, ...) const;
};

///////////////////////////////////////////////////////////////////////////////
//
// Base parse object containing the parse function, keywords and error messages
//
///////////////////////////////////////////////////////////////////////////////

class _PREFIXCLASS TParseObjectBase
{
    
  public:
     // Constructors

     TParseObjectBase();
     TParseObjectBase(char **keyWords, char **errStrs);
     TParseObjectBase& operator =(TParseObjectBase& aParser);

     // Destructor

     virtual ~TParseObjectBase();

     // Public access member funcs

     TLineInp*      ParserLineInp();

     int            SetStringTables(char **aKeyWordList, char **anErrorMsgList);
     int            ParseSource(TLineInp *li, int continuation);
     void           SetDefltPadLength(int len);
     const char*    KeyWord(int id);    // Returns string form of the keyword
     const char*    LPaddedKeyWord(const int id, const int padlen = -1); // as above, but left padded
     const char*    PaddedKeyWord(const int id, const int padlen = -1);  // right padded (-1 = default width)

      
     TParserError* ItsLastError() { return itsLastError; }

     // Members which may be accessed by derrived classes - these provide the
     // necessary tools to derrive parsers for user grammars

     void           AbortParse(const int errnum, const char *aUserMessage=0);
	 void           AbortParse(const TParserError &aparseError);
     int            GetKeywordIdx(const char *aKeyWord); // Find the index of a KeyWord
     int            CurrSourceLineNumber();

	 // protected token convenience accessors

protected:

	int				GetCurrTokenID()            { return currtoken->id; }
    const char*		GetCurrTokenStr()			{ return currtoken->str; }
    TToken::ValType GetCurrTokenValue()			{ return currtoken->value; }
    double			CurrTokenToReal()           { return currtoken->ToReal(); }
    void			SetCurrTokenID(const int in){ currtoken->id = in; }

     // token advance functions

public:     
	 
	 const TToken&  GetCurrToken() { return *currtoken; }
	 void           GetToken(); // Get next token
     void           GetToken(const int token_id, const int user_error=0);   // Get specified token
     void           SkipToken(int token_id, int user_error=0) { GetToken(token_id, user_error); }
 	 void           SkipToToken(int token_id, int user_error=0);   // Scan throug tokens
     													  		   // Till specified one found

	void            ReplaceToken();
     void           PeekToken();                                    // Retreive next token without advancing

     double         GetNumericValue(int user_error=0);             // Get a numeric value
     int            GetIntValue(int user_error=0);                 // Get an int
     void           GetString(char *buffer, int user_error=0);     // Get a string, and copy it
     
     int            KeywordTableSelection(int table[]);
     TLineInp*      itsLineInput;       // The source line input object

     // Main parser body function for user defined grammars.. the above set
     // of protected members can be used in a derrived ParserFinc() to implement
     // the grammar parser


	  const char** GetKeyWordTable() { return (const char**)itsKeyWords; }

	 void SetCommentChar(const char* ch);

  protected:

	 TToken*		currtoken;			// Current token
     char**         itsKeyWords;        // Keywords table
     char**         itsErrorStrings;    // Error message table
     int*           itsKeyWordIdx;      // Sorted Index array into itsKeyWords
     TParserError*  itsLastError;       // Error report object
     tool::TCheckSum*  itsCheckSum;

     virtual void   ParseFunc(){};   // virtual parse func for overriding

  private:

	  

     // Members concearned with the objects implementation and not called
     // outside of this class
      

     int            itsFirstLineFlag;   // Set if reading first line of source
     int            itsCommentPos;      // Position of comment in curr source line
     int            itsKeyWordCount;    // The number of keywords in this parse
     int            itsErrorStrCount;   // The number of error messages in this parser
     int            itsDefltPadLength;  // Default Padding length
     int            AdvanceSourceLine();       // Advance to the next input line

     char          *GetTokenString(const char *string, int *prelim_token); // source line -> string
     TToken         Tokenize(char * tokenstr, int prelim_tokenid);  // string -> token
     char           theTokenStringBuff[LINELENGTH+1];

};


////////////////////////////////////////////////////////////////////////////////
//
//  Error message reporting object
//
////////////////////////////////////////////////////////////////////////////////


enum {
    posTRAILING_BLANKS = 0,
    posLEADING_BLANKS  = 1
    };

_PREFIXFUNC char *  Pad(char *str, int pos, int length);
_PREFIXFUNC std::ostream& Indent(std::ostream* os, int indSize=2);
_PREFIXFUNC void AppendCRLF(char *str);

}; // namespace Parser

#endif


