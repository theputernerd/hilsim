////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_flinein.h
//
//  Derives a file line input class from the parser's base line input class
//  allowing parser objects to parse a file
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: fletched $
//  $Date: 2001/09/18 06:03:14 $
//  $Revision: 1.4 $
//  $RCSfile: parser_flinein.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_FlineinH
#define parser_FlineinH

#ifndef parserH
    #include "parser.h"
#endif
#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif

namespace Parser {

// classes private parts forward declared
class TFileLineInp_i;

class _PREFIXCLASS TFileLineInp: public TLineInp
{
    TFileLineInp_i *d_this;
public:
	TFileLineInp();
	virtual ~TFileLineInp();

	virtual long  GetLinePos();
	virtual int   SetLinePos(long anOffset);
	virtual int   AdvanceLine();
	virtual int   IsOpen();
	virtual int   IsEncrypted();
	int           Open(const char *fileName);
	void          Close();
   
};

}; // namespace Parser

#endif

