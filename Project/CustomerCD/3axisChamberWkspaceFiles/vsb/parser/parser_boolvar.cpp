////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_boolvar.cpp
//
//  A simple boolean expression evaluator
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2002/06/12 07:56:19 $
//  $Revision: 1.5 $
//  $RCSfile: parser_boolvar.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_boolvar.h"

#ifndef parser_perrorH
    #include "parser_perror.h"
#endif
#ifndef parser_SlineinH
    #include "parser_slinein.h"
#endif

#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef __ASSERT_H
    #include <assert.h>
    #define __ASSERT_H
#endif

#ifndef tool_strtoolsH
    #include "tool_strtools.h"
#endif

namespace Parser {

////////////////////////////

enum EBoolVarErrorEnums {
  ERR_DEFLT,
  ERR_bool_EXPR,
};

static char *theBoolVarErrors[] =
{
  "",
  "Error in boolean expression",
  NULL
};


static char *theBoolVarKeyWords[] = {
  "false", "true", "AND","OR", "XOR", "NOT", NULL
};

enum EBoolVarKeyWordsEnums {
  BV_FALSE,
  BV_TRUE,
  BV_AND,
  BV_OR,
  BV_XOR,
  BV_NOT,
  BV_VAR,
  } ;


TBoolVarList::TBoolVarList()
{
  itsNumVars = 0;
}

TBoolVarList::~TBoolVarList()
{
  int i;
  for (i=0; i<itsNumVars; i++)
	 delete[] itsName[i];
}

bool &TBoolVarList::FindVar(const char *name)
{
  for(int i=0; i<itsNumVars; i++)
	  if (!tool::StrCmpNoCase(name,itsName[i]))
		return itsBuff[i];
  assert(0);
  return itsBuff[0];
}

int TBoolVarList::FindVarIdx(const char *name)
{
  for(int i=0; i<itsNumVars; i++)
	 if (!tool::StrCmpNoCase(name,itsName[i]))
		return i;
  return -1;
}

void  TBoolVarList::AddBoolVar(const char *name)
{
  assert(itsNumVars < MAX_DYNCOND_VARS);
  itsName[itsNumVars] = new char[strlen(name)+1];
  strcpy(itsName[itsNumVars],name);
  itsBuff[itsNumVars] = false;
  itsNumVars++;
}

bool &TBoolVarList::operator[](int i)
{
  assert(i>= 0 && i<itsNumVars);
  return itsBuff[i];
}


bool &TBoolVarList::operator[](const char *name)
{
  return FindVar(name);
}


TBoolVarList &TBoolVarList::operator = (TBoolVarList &srcList)
{
  int i;
  for (i=0; i<itsNumVars; i++)
	 delete[] itsName[i];

  itsNumVars = srcList.itsNumVars;
  for (i=0; i<itsNumVars; i++)
  {
	 itsName[i] = new char[strlen(srcList.itsName[i])+1];
    strcpy(itsName[i],srcList.itsName[i]);
    itsBuff[i] = srcList.itsBuff[i];
  }
  return *this;
}

////////////////////////////////////////////////////
TBoolExpression::TBoolExpression(TBoolVarList &boolvarList):
  itsBoolVarList(boolvarList)
{
  itsNumTokens = 0;
}

TBoolExpression::~TBoolExpression()
{
}


bool  TBoolExpression::Evaluate()
{
    const int MAX_BSTACK  = 20;

	int sp=0;
	bool stack[MAX_BSTACK];
	for (int i=0; i<itsNumTokens; i++)
	{
	  switch (tokenId[i])
	  {
		  case BV_TRUE:  if (sp < MAX_BSTACK)
									stack[sp++] = true;
							  break;
		  case BV_FALSE: if (sp < MAX_BSTACK)
									stack[sp++] = false;
							  break;
		  case BV_OR   : if (sp > 1)
							  {
								 stack[sp-2] = stack[sp-1] || stack[sp-2];
								 sp-=1;
							  }
							  break;
		  case BV_XOR   : if (sp > 1)
							  {
								 stack[sp-2] = stack[sp-1] ^ stack[sp-2];
								 sp-=1;
							  }
							  break;
		  case BV_AND   : if (sp > 1)
							  {
								 stack[sp-2] = stack[sp-1] && stack[sp-2];
								 sp-=1;
							  }
							  break;
		  case BV_NOT   : if (sp > 0)
							  {
								 stack[sp-1] = !stack[sp-1];
							  }
							  break;
		  default        : if (tokenId[i] >= BV_VAR && sp < MAX_BSTACK)
								 stack[sp++] = itsBoolVarList[tokenId[i] - BV_VAR];
								 break;
	  }
	}
	return stack[0];
}

TBoolExpression & TBoolExpression::operator = (TBoolExpression& expr)
{
  itsNumTokens = expr.itsNumTokens;
  for (int i=0; i<itsNumTokens; i++)
	 tokenId[i] = expr.tokenId[i];
  return *this;
}


////////////////////////////////////////////////////////////////////////////////

TBoolExpressionParser::TBoolExpressionParser(TBoolVarList &boolvarList):
  TBoolExpression(boolvarList),TParseObjectBase(theBoolVarKeyWords,theBoolVarErrors)
{
}

TBoolExpressionParser::~TBoolExpressionParser()
{
}

void TBoolExpressionParser::PushToken(int i)
{
  assert (itsNumTokens < MAX_DYNCOND_TOKENS);
  tokenId[itsNumTokens++] = i;
};

void TBoolExpressionParser::ParseExpression()
{
  GetToken();
  if (GetCurrTokenID() == T_LBRACK)
  {
	  ParseElement();
	  GetToken();
	  do {
		  switch (GetCurrTokenID())
		  {
			 case BV_AND:
								ParseElement();
								PushToken( BV_AND);
								break;
			 case BV_OR :
								ParseElement();
								PushToken( BV_OR);
								break;
			 case BV_XOR :
								ParseElement();
								PushToken( BV_XOR);
								break;
			 default   :   ReplaceToken();
								SkipToken(T_RBRACK);
                        return;
		  }
		  GetToken();
	  } while(1);
  } else
  {
	 ReplaceToken();
	 ParseElement();
  }
}

void TBoolExpressionParser::ParseFunc()
{
  itsNumTokens = 0;
  ParseExpression();
}

void TBoolExpressionParser::ParseElement()
{
  bool negate = false;
  int varIdx;
  GetToken();
  if (GetCurrTokenID() == BV_NOT)
  {
	 negate = true;
	 GetToken();
  }

  switch (GetCurrTokenID())
  {
	  case T_LBRACK   : ReplaceToken();
							  ParseExpression();
							  break;
	  case BV_TRUE    : PushToken( BV_TRUE);
							  break;
	  case BV_FALSE   : PushToken(BV_FALSE);
							  break;
	  case T_UNKNOWN  :
	  case T_STRING   : if ((varIdx = itsBoolVarList.FindVarIdx(GetCurrTokenStr())) == -1)
                         AbortParse(ERR_bool_EXPR);

                       PushToken(BV_VAR+varIdx);
							  break;
  };
  if (negate)
	 PushToken(BV_NOT);
}


void TBoolExpressionParser::Load(TParseObjectBase *parentParser)
{
  assert(parentParser);
  int status = ParseSource(parentParser->ParserLineInp(),1);
  if (status)
    parentParser->AbortParse(ItsLastError()->Id(), ItsLastError()->Desc());
}

void TBoolExpressionParser::LoadFromString(TParseObjectBase *parentParser, char *str)
{
  assert(parentParser && str);
  TStringLineInp sLineInp;
  sLineInp.Open(str);
  int status = ParseSource(&sLineInp,0);
  if (status)
    parentParser->AbortParse(ItsLastError()->Id(), ItsLastError()->Desc());
}

}; // namespace Parser

