////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_baseunit.h
//
//  Defines the base class for variable-unit data edit boxes
//  Implements the foundation behaviour for numeric values that have
//  associated units
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/02/20 06:16:38 $
//  $Revision: 1.16 $
//  $RCSfile: parser_baseunit.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_baseunitH
#define parser_baseunitH
#define parser_BaseunitH // case safety for external include guards

#ifndef tool_prefixdecH
    #include "../tool/tool_prefixdec.h"
#endif
#ifndef parser_rangetblH
    #include "parser_rangetbl.h"
#endif

#pragma warning( disable : 4786 )

namespace Parser {

// forward declaration
class TRangeLimit;

enum EUnitEditStyle {
    eUnitEditSpinner,
    eUnitEditPopup
    };

enum EUnitType {
    eDist, eAlt, eSpeed,
    eAngle, eAngRate, eAngAccel,
    eMass, eMassRate,
    eAccel, eLatAccel,
    eTime, eForce, eNoUnit
    };

enum EUnitName {
    eM, eKm, eFt, eKFt, eNM,
    eMPS, eFtPS, eKPH, eKt,
    eRad, eDeg,
    eRadPS, eDegPS,
    eRadPSSq, eDegPSSq,
    eKg, eLb,
    eKgS, eLbS,
    eMPSSq, eFtPSSq, eKtPSSq, eG,
    eSec, eMin, eMSec,
    eN, eLbf,
    eNone
    };

//////////////////////////////////////////////////////////////////////////
//
//  CLASS:   TUnitConfig
//  AUTHOR:  T Gouthas   15/12/95
//  PURPOSE: This class defines a container class for the users selection
//           of default unit types for each of the classes of variable-unit
//           data entry edit boxes, namely Distance, ALtitude, Veocity,
//           angle and angular Rate. It also contains references to the
//           format string to be used for each individual sub type, Ie
//           Metres, Km, etc
//
//////////////////////////////////////////////////////////////////////////
class _PREFIXCLASS TUnitConfig
{
//protected:
private:
	int *itsUnits;
public:
	TUnitConfig();
	virtual ~TUnitConfig();

	virtual void        Set(EUnitType aUnitType, int aUnitIdx);
	virtual void        Set(EUnitType aUnitType, EUnitName aUnitEnum);
	virtual int         GetIdx(EUnitType aUnitType);
	virtual EUnitName   GetEnum(EUnitType aUnitType);
};


////////////////////////////////////////////////////////////////////////
//
//  CLASS:   TUnitValue
//  AUTHOR:  T Gouthas   15/12/95
//  PURPOSE:  This defines a class for values with changeable units
//            Ie a double precision value and a unit type specifier
//            There are several categories of units, ie Distance, Velocity
//            Angle and Angular rate.
//            Facilities are provided to handle a given value correctly
//            when supplied in different units as long as the unit categories
//            match
//
//            There are two sets of each method that requires a unit parameter
//            one for the unit specified as an enumerated type, and one for
//            the unit specified as an index (unit types are stored internally
//            as an index ranging from 0 to n-1 where n is the number of units
//            this value may have.
//
//
//  MODIFIED:
////////////////////////////////////////////////////////////////////////


class _PREFIXCLASS TUnitValue
{
    friend class /*_CUSTOMCLASS*/ TUnitEdit;
    friend class TUnitConfigDialog;

protected:
    int itsRangeId;
    TRangeLimit *itsLocalRange;

private:
    EUnitType itsType;
    double itsValue;
    int itsUnit;
    static EUnitName  theDistNames[];
    static EUnitName  theSpeedNames[];
    static EUnitName  theAngleNames[];
    static EUnitName  theAngRateNames[];
    static EUnitName  theAngAccelNames[];
    static EUnitName  theMassNames[];
    static EUnitName  theMassRateNames[];
    static EUnitName  theAccelNames[];
    static EUnitName  theTimeNames[];
    static EUnitName  theForceNames[];
    static EUnitName  theNoUnitNames[];

    static EUnitName *theUnitNames[];


  public:
    static char *theUnitKeys[];
    static int theCvtConstCount[eNoUnit+1];
    static int theCvtOffset[eNoUnit+1];
    static char *theUnitNameStrs[];


    virtual ~TUnitValue();
    TUnitValue(EUnitType aUnitType, double aValue=0.0, int aUnitIdx=0, int aRange = 0, bool defltUnits = true);

    TUnitValue(EUnitType aUnitType, double aValue, EUnitName aUnit, int aRange = 0, bool defltUnits = true);

    TUnitValue(const TUnitValue& copyRef);

    TUnitValue operator = (const TUnitValue& copyRef);
    TUnitValue operator + (TUnitValue& aUv);
    TUnitValue operator - (TUnitValue& aUv);
    TUnitValue operator * (TUnitValue& aUv);
    TUnitValue operator / (TUnitValue& aUv);

    int GetUnitIdx() const;
    int GetMaxUnitIdx() const;
    EUnitType GetUnitType() const;
    EUnitName GetUnit() const;
    double GetRawValue() const;

    double ConvertValue(int aFromUnit, int aToUnit, double aValue) const;
    double ConvertValue(EUnitName aFromUnit, EUnitName aToUnit, double aValue) const;

    virtual void SetValue(double aValue, int aUnit=0);
    virtual void SetValue(double aValue, EUnitName aUnit);
    virtual  void SetValue(const TUnitValue *src);

    virtual void SetValueAndUnit(double aValue, int aUnitIdx=0);
    virtual void SetValueAndUnit(double aValue, EUnitName aUnit);
    virtual void SetValueAndUnit(const TUnitValue *src);

    double       GetValue(int aUnit = 0) const;
    double 		 GetValue(EUnitName aUnit) const;
    virtual void ResetUnitType(EUnitType aUnitType, double aValue = 0.0, int aUnitIdx=0);

    void 			 ChangeUnit(int aUnit);
    void 			 ChangeUnit(EUnitName aUnit);
    void 			 SetDefaultUnit();

    void 			 SetValidRange(int aRangeIdx);
    void 			 SetValidRange(const ERangeType& aType, double aLimit1 =0, double aLimit2=0);


    ERangeType CheckRange(double aValue, int aUnitIdx=0) const;
    ERangeType CheckRange(double aValue, EUnitName aUnit) const;
    ERangeType CheckRange() const;

    // ToString functions return string with Value and Unit separated by two spaces
    const char        *ToString(double aValue, EUnitName aUnit) const;
    const char        *ToString(double aValue, int aUnitIdx) const;
    const char        *ToString(EUnitName aUnit) const;
    const char        *ToString() const;

    // ToString functions with No Spaces between Value and Unit
    const char        *ToStringNS(double aValue, EUnitName aUnit) const;
    const char        *ToStringNS(double aValue, int aUnitIdx) const;
    const char        *ToStringNS(EUnitName aUnit) const;
    const char        *ToStringNS() const;
    
    // Static functions


    static int         GetMatch(const char *str, EUnitType unitType);
    static EUnitType   MatchUnitKey(const char *unitKeyName);
    static EUnitName   MatchUnitName(const char *unitName);
    static const char* GetUnitCombinedNameStr(EUnitName aUnit);
    static const char* GetUnitCombinedNameStr(EUnitType aUnitType, int aUnitIdx);
    static const char* GetUnitNameStr(EUnitName aUnit);
    static const char* GetUnitNameStr(EUnitType aUnitType, int aUnitIdx);
    static const char* GetUnitLongNameStr(EUnitName aUnit);
    static const char* GetUnitLongNameStr(EUnitType aUnitType, int aUnitIdx);
    static const char* GetUnitClassNameStr(EUnitType aUnitClass);
    static const char* GetUnitFormatStr(EUnitName aUnit);
    static const char* GetUnitFormatStr(EUnitType aUnitType, int aUnitIdx);
    static int         GetUnitCount(EUnitType aUnitType);
    static int         GetUnitNum(EUnitType aUnitType, EUnitName aUnit);
    static double 		 ConvertFromRawValue(EUnitType aUnitType, int aToUnitIdx, double aValue);
    static double 		 ConvertValueUnit(EUnitType aUnitType, int aFromUnitIdx, int aToUnitIdx, double aValue);
};

_PREFIXFUNC TUnitConfig *& GetUserUnits();

}; // namespace Parser

#endif
