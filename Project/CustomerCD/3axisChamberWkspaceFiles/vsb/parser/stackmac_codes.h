////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  stackmac_codes.h
//
//  Define the codes, opcodes, etc for StackMac
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/05/21 08:38:48 $
//  $Revision: 1.6 $
//  $RCSfile: stackmac_codes.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef stackmac_codesH
#define stackmac_codesH

namespace Stackmac {

//////////////////////////////////////////
// Global variable type codes
//////////////////////////////////////////
enum {
    sINT       = 0,
    sLONG      = 1,
    sFLOAT     = 2,
    sDOUBLE    = 3,
    sVOID      = 4
    };

//////////////////////////////////////////
// Stack machine user symbol type codes
//////////////////////////////////////////
enum {
    sUNUSED     = 0,
    sLOCAL      = 1,
    sGLOBAL     = 2,
    sFUNC       = 3,
    sCONST      = 4,
    sARRAY      = 5,
    sCONSTARRAY = 6
    };

//////////////////////////////////////////
// Parameter style
//////////////////////////////////////////
enum {
    ps_VALUE     = 0,
    ps_POINTER   = 1
    };

enum {
    sATOM_CLASS        = 0,
    sCONST_ATOM_CLASS  = 1,
    sARRAY_CLASS       = 2,
    sCONST_ARRAY_CLASS = 3
    };

enum {
    sREAD_WRITE       = 0,
    sREAD_ONLY        = 1
    };

//////////////////////////////////////////
// Stack machine instruction op codes
//////////////////////////////////////////
enum {
// 1 param opcodes
    iSTOP      = 0,
    iPOP       = 1,
    iPOPA      = 2,
    iPUSH      = 3,

    iEQ        = 4,
    iNEQ       = 5,
    iLT        = 6,
    iGT        = 7,
    iLE        = 8,
    iGE        = 9,
    iAND       = 10,
    iOR        = 11,

    iADD       = 12,
    iSUB       = 13,
    iMUL       = 14,
    iDIV       = 15,
    iSIN       = 16,
    iCOS       = 17,
    iTAN       = 18,
    iASIN      = 19,
    iACOS      = 20,
    iATAN      = 21,
    iATAN2     = 22,
    iSINH      = 23,
    iCOSH      = 24,
    iTANH      = 25,
    iEXP       = 26,
    iLOG       = 27,
    iLOG10     = 28,
    iSQRT      = 29,
    iFLOOR     = 30,
    iCEIL      = 31,
    iROUND     = 32,
    iABS       = 33,
    iFMOD      = 34,
    iSQR       = 35,
    iMIN       = 36,
    iMAX       = 37,
    iSIGN      = 38,
    iLIMIT     = 39,
    iPIMOD     = 40,
    iEUCLID2   = 41,
    iEUCLID3   = 42,
    iDEG       = 43,
    iRAD       = 44,
    iNEG       = 45,
    iPOW       = 46,
    iXCHG      = 47,
    iRETN      = 48,

    DOUBLE_OP  = 49,

// 2 param opcodes
    iLABEL     = 49,
    iJMPNZ     = 50,
    iJMPZ      = 51,
    iJMP       = 52,
    iVAL       = 53,
    iPOPN      = 54,
    iSTOR      = 55,

// 3 param opcodes
    iRETR      = 56,
    iINTERP    = 57,
    iINTERP2D  = 58,
    iPRINT     = 59,
	iMESSAGE_STR = 60,
    iCONSOLE   = 61,

    MAX_INSTRUCTIONS = 62
    };

//////////////////////////////////////////
// Stack machine error codes
//////////////////////////////////////////
enum {
    seOK,
    seALLOC_ERR,
    seINSTR_ERR,
    seSTACK_UNDERFLOW,
    seSTACK_OVERFLOW,
    seRANGE_ERR,
    seILLEGAL_VAR,
    seZERO_DIVISION,
    seSQRT_NEG_ARG,
    seACOS_ARG_ERR,
    seASIN_ARG_ERR,
    seLOG_ARG_ERR,
    seLOG10_ARG_ERR,
    seFMOD_ZERO_ARG,
    seSIN_ARG_ERR,
    seCOS_ARG_ERR,
    seTAN_ARG_ERR,
    seEXP_ARG_ERR,
    sePARAM_ERR,
    seNO_MODULE,
    seLOCKED,
    seUNLOCKED,
    seDUPLICATE_VAR,
    seVAR_LIMIT,
    seINSTR_LIMIT,
    seLABEL_LIMIT,
    seLABEL_ERR,
    seTYPE_ERR,
    seJUMP_ERR,
    seSTACK_ERROR,
    seARRAY_BOUNDS,
    sePARAM_TYPE,
    seHEAP_OVERFLOW,
    seRECURSION_ERR
};

// Tokens
enum {
    T_INTERP, T_INTERP2D,
    T_SIN,   T_COS,   T_TAN,   T_ASIN,  T_ACOS, T_ATAN,
    T_ATAN2, T_SINH,  T_COSH,  T_TANH, T_EXP,   T_LOG,
    T_LOG10, T_SQRT,  T_FLOOR, T_CEIL, T_ROUND, T_ABS,
    T_FMOD,  T_SQR,   T_MIN,   T_MAX,  T_SIGN,  T_LIMIT,
    T_PIMOD, T_EUCLID2, T_EUCLID3,
    T_DEG,   T_RAD,   T_NEG,   T_POW,  T_AND,   T_OR,
    T_IF,    T_THEN,  T_BEGIN, T_END,  T_ELSE,  T_WHILE,
    T_DO,    T_REPEAT,T_UNTIL, T_FOR,  T_TO,    T_DOWNTO,
    T_GOTO,  T_GOSUB, T_RETURN,T_STOP, T_MODULE,T_VAR,
    T_CONST, T_WRITE,  T_WRITELN, T_REPORT, T_CONSOLE,
    };

// error codes
enum {
    ERR_SM_DEFAULT,
    ERR_SM_LABEL_OVERFLOW,
    ERR_SM_ILLEGAL_LABEL_REF,
    ERR_SM_NAME_EXPR,
    ERR_SM_ILLEGAL_VAR,
    ERR_SM_INSTRUCT_OVERFLOW,
    ERR_SM_NO_RETURN,
    ERR_SM_VAR_PARM_EXPECTED,
    ERR_SM_ILLEGAL_PARAM,
    ERR_SM_DIM_OVERFLOW,
    ERR_SM_PARAM_TYPE_MISMATCH,
    ERR_SM_EXPRESSION,
    ERR_SM_STATEMENT,
    ERR_SM_INTRINSIC_NOT_STATEMENT,
    ERR_SM_ASSIGNMENT,
    ERR_SM_STATEMT_SEP,
    ERR_SM_IF_STAT,
    ERR_SM_DO,
    ERR_SM_UNTIL,
    ERR_SM_UNTIL_STAT,
    ERR_SM_WHILE_STAT,
    ERR_SM_FOR_VAR,
    ERR_SM_DOWNTO,
    ERR_SM_FOR,
    ERR_SM_COLON_OR_VAR_EXPECTED,
    ERR_SM_GOTO,
    ERR_SM_MODULE_NAME_EXP,
    ERR_SM_MODULE_NOT_FOUND,
    ERR_SM_VARNAME_EXPECTED,
    ERR_SM_VAR_DEFINED,
    ERR_SM_VAR_INTERNAL,
    ERR_SM_INTEGER_EXPECTED,
    ERR_SM_SEMICOLON_EXPECTED,
    ERR_SM_VALUE,
    ERR_SM_CONST,
    ERR_SM_PROTECTION,
    ERR_SM_VAR_MISSING,
    ERR_SM_DOUBLE_ARRAY_EXPECTED,
    ERR_SM_SINGLE_DIM_ARRAY_EXPECTED,
    ERR_SM_INCOMPAT_ARRAY_DIMS,
    ERR_SM_INTERP_DOMAIN,
    ERR_SM_SOURCE_STORAGE,
    ERR_SM_FILE_IO,
    ERR_SM_FILE_OPEN,
    ERR_SM_NONPOSITIVE,
    };

}; // namespace Stackmac

#endif

