function [path, files, includes, defines] = parser_library
% PARSER_LIBRARY - Provide information about the parser library.
%
%    [path, files, includes, defines] = tool_library
%
%    path     - The path of the library, as a string.
%    files    - List of source files to be compiled for the library,
%               as a cell array of strings.
%    includes - List of include paths required to compile the library,
%               as a cell array of strings.
%    defines  - List of definitions required to compile the library,
%               as a cell array of strings.

% find full filename of this script file
script_full_filename = which('parser_library');

% get the library path from the script filename
[path, dummy, dummy, dummy] = fileparts(script_full_filename);

% if the path was all the caller wanted, don't return any more
if nargout < 2
    return;
end

% the list of files
files = { ...
    [path filesep 'parser.cpp'], ...
    [path filesep 'parser_baseunit.cpp'], ...
    [path filesep 'parser_boolvar.cpp'], ...
    [path filesep 'parser_flinein.cpp'], ...
    [path filesep 'parser_perror.cpp'], ...
    [path filesep 'parser_rangetbl.cpp'], ...
    [path filesep 'parser_slinein.cpp'], ...
    [path filesep 'stackmac.cpp'], ...
};

% include paths
includes = { ...
    Mars_GetLibraryPath('tool_library'), ...
    path, ...
};

% defines
defines = { ...
};

