////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_slinein.h
//
//  Derives and string buffer line input class from the parser's 
//  base line input class allowing parser objects to parse a string buffer
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: fletched $
//  $Date: 2001/09/18 06:03:14 $
//  $Revision: 1.3 $
//  $RCSfile: parser_slinein.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_SlineinH
#define parser_SlineinH

#ifndef parserH
    #include "parser.h"
#endif
#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace Parser {

class _PREFIXCLASS TStringLineInp: public TLineInp
{
private:
	char  *itsStr;
	long itsCurrPos;
	int  itsIsOpen;

public:
	TStringLineInp():TLineInp(),itsIsOpen(0),itsCurrPos(0), itsStr(0)  {};

	virtual ~TStringLineInp();
	virtual long  GetLinePos();
	virtual int   SetLinePos(long anOffset);
	virtual int   AdvanceLine();
	virtual int   IsOpen()                     {return itsStr!=0;};
	int           Open(char *str);
};

}; // namespace Parser

#endif

