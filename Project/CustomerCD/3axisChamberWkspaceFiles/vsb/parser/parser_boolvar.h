////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_boolvar.h
//
//  A simple boolean expression evaluator
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: fletched $
//  $Date: 2001/09/18 06:03:14 $
//  $Revision: 1.3 $
//  $RCSfile: parser_boolvar.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_BoolvarH
#define parser_BoolvarH

#ifndef parserH
    #include "parser.h"
#endif
#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace Parser {

class _PREFIXCLASS TBoolVarList
{
  public:
    TBoolVarList();
    virtual ~TBoolVarList();
    void  AddBoolVar(const char *name);
    bool &FindVar(const char *name);
    int  FindVarIdx(const char *name);
    bool &operator[](int i);
    bool &operator[](const char *name);
    TBoolVarList &operator = (TBoolVarList &srcList);

  //protected:
  private:
      int   itsNumVars;

      enum { MAX_DYNCOND_VARS = 30 };
      bool itsBuff[MAX_DYNCOND_VARS];
      char *itsName[MAX_DYNCOND_VARS];
};

class _PREFIXCLASS TBoolExpression
{
  public:
    virtual ~TBoolExpression();

    int           itsNumTokens;
    TBoolVarList& itsBoolVarList;

    TBoolExpression(TBoolVarList& boolvarList);

    bool  Evaluate();
    TBoolExpression & operator = (TBoolExpression& expr);

  protected:
    enum { MAX_DYNCOND_TOKENS = 40 };
    int   tokenId[MAX_DYNCOND_TOKENS];
};

class _PREFIXCLASS TBoolExpressionParser: public TBoolExpression, public TParseObjectBase
{
  public:
    TBoolExpressionParser(TBoolVarList& boolvarList);
    virtual ~TBoolExpressionParser();

    void Load(TParseObjectBase *parentParser);
    void LoadFromString(TParseObjectBase *parentParser, char *str);

  private:
    void ParseFunc();
    void PushToken(int i);
    void ParseExpression();
    void ParseElement();
} ;

}; // namespace Parser

#endif
