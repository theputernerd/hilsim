////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_rangetbl.cpp
//
//  TRangeLimit
//  Defines a range limit definition class with utility member functions
//
//  TRangeTbl
//  Implements a table of ranges for general data entry validation, the idea
//  being that for values with identical valid ranges, the ranges are stored
//  in one central place with savings of storage space and code.
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: themsta $
//  $Date: 2001/06/04 03:03:53 $
//  $Revision: 1.3 $
//  $RCSfile: parser_rangetbl.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_rangetbl.h"

#ifndef __ASSERT_H
    #include <assert.h>
    #define __ASSERT_H
#endif

namespace Parser {

static TRangeTbl theRangeTable;

_PREFIXFUNC TRangeTbl& GetRangeTable()
{
  return theRangeTable;
};




TRangeLimit::TRangeLimit()

//  CONSTRUCTOR:
//  Create a default range check node -> range unlimited

{
	itsLimit1 = itsLimit2 = 0.0;
	itsType = eNoRange;
}


TRangeLimit::TRangeLimit(ERangeType aType, double aLimit1,  double aLimit2)

// Create a new range check node of a specified type and with the give  limits
// note: "aLimit1" and "aLimit2" default to 0 if not specified in the parameter
// list... also note aLimit2 is ignored if it is supplied for "eAbove" "eBelow"
// or "eNoRange" range check types. aLimit1 is also ignored in the case of
// "eNoRange"

{
   SetRangeLimit(aType, aLimit1,  aLimit2);
}



void TRangeLimit::SetRangeLimit(ERangeType aType, double aLimit1,  double aLimit2)

// Create a new range check node of a specified type and with the give  limits
// note: "aLimit1" and "aLimit2" default to 0 if not specified in the parameter
// list... also note aLimit2 is ignored if it is supplied for "eAbove" "eBelow"
// or "eNoRange" range check types. aLimit1 is also ignored in the case of
// "eNoRange"

{
	itsType = aType;
	itsLimit1 = aLimit1;
	itsLimit2 = (aType == eAbove || aType == eBelow) ? aLimit1: aLimit2;
	assert(itsLimit2 >= itsLimit1);
}


TRangeLimit::~TRangeLimit()

//  DESTRUCTOR:
//  Destroy a range check node.. currently not necessary

{
}

int TRangeLimit::operator == (TRangeLimit& rn)

// operator == overload for comparing TRangeNode's

{
	if (itsType != rn.itsType)
	  return 0;
	else if (itsType == eNoRange)
	  return 1;
	else if (itsLimit1 != rn.itsLimit1)
	  return 0;
	else if (itsLimit2 != rn.itsLimit2)
	  return 0;
	else return 1;
}


ERangeType TRangeLimit::CheckRange(double aVal)
{
	switch ( itsType )
	{
		case eAbove :
					  if (aVal < itsLimit1)
						 return eAbove;
					  break;

		case eBelow :
					  if (aVal > itsLimit1)
						 return eBelow;
					  break;

		case eInside:
					  if (aVal < itsLimit1 || aVal > itsLimit2)
						 return eInside;
					  break;

		 case eOutside:
					 if (aVal > itsLimit1 && aVal < itsLimit2)
						return eOutside;
					 break;

	}
	return eNoRange;
};


////////////////////////////////////////////////////////////////////////
//
//  TRangeNode
//
//  This is a support class for TRangeTbl. It provides a class
//  for each individual range entry in TRangeTbl and the
//  mechanisms to build a linked list of entries
//
////////////////////////////////////////////////////////////////////////

class TRangeNode: public TRangeLimit
{
private:

  friend class TRangeTbl;
  TRangeNode   *next;

public:

  TRangeNode(TRangeNode *aParent, ERangeType aType, double aLimit1 = 0,
	 double aLimit2=0);
  TRangeNode(TRangeNode *aParent);
  ~TRangeNode();
  int operator == (TRangeNode& rn);
};


TRangeNode::TRangeNode(TRangeNode *aParent):TRangeLimit()

//  CONSTRUCTOR:
//  Create a default range check node -> range unlimited

{
	next = aParent;
}

TRangeNode::TRangeNode(TRangeNode *aParent, ERangeType aType, double aLimit1,
							  double aLimit2):TRangeLimit(aType,aLimit1,aLimit2)


// CONSTRUCTOR #2:
// Create a new range check node of a specified type and with the give  limits
// note: "aLimit1" and "aLimit2" default to 0 if not specified in the parameter
// list... also note aLimit2 is ignored if it is supplied for "eAbove" "eBelow"
// or "eNoRange" range check types. aLimit1 is also ignored in the case of
// "eNoRange"

{
	next = aParent;
}

TRangeNode::~TRangeNode()

//  DESTRUCTOR:
//  Destroy a range check node.. currently not necessary

{
}

int TRangeNode::operator == (TRangeNode& rn)

// operator == overload for comparing TRangeNode's
{
	if (itsType != rn.itsType)
	  return 0;
	else if (itsType == eNoRange)
	  return 1;
	else if (itsLimit1 != rn.itsLimit1)
	  return 0;
	else if (itsLimit2 != rn.itsLimit2)
	  return 0;
	else return 1;
}


//////////////////////////////////////////////////////////////////////
//
//  TRangeTbl
//
//////////////////////////////////////////////////////////////////////


TRangeTbl::TRangeTbl()

// CONSTRUCTOR:
//  Note: Range table entry  0 is set to a default .. range unlimited.

{
  itsHead = (TRangeNode *)0;
  itsCount = 0;
  AddRange(eNoRange);
}

TRangeTbl::~TRangeTbl()

// DESTRUCTOR:
// Necessary to perform the deletion of all the dynamically allocated range
// check entries (ie TRangeNode's)

{
  TRangeNode *iterator;
  for (int i=0; i < itsCount; i++)
  {
	  iterator = itsHead->next;
	  delete itsHead;
	  itsHead = iterator;
  }
}


int TRangeTbl::GetRangeCount()
{
  return itsCount;
}

int TRangeTbl::FindRange(TRangeNode& aRangeNode)

// Find a TRangeNode in the range table that matches the argument.

{
  TRangeNode *iterator = itsHead;
  if (itsCount == 0) return -1;
  for (int i=itsCount-1; i >=0 ; i--)
	 if (aRangeNode == *iterator)
		return i;
	 else iterator = iterator->next;
  return -1;
}


int TRangeTbl::FindRange(ERangeType aType, double aLimit1,
	 double aLimit2)

// Find a TRangeNode in the range table with the specified attributes

{
  TRangeNode tmpRange(itsHead, aType, aLimit1, aLimit2);
  return FindRange(tmpRange);
}


int TRangeTbl::AddRange(ERangeType aType, double aLimit1,
	 double aLimit2)

// Add a TangeNode ot the range table with the specified attributes and return
// its index number. If a matching entry already exists, return its index without
// creating a new entry

{
  TRangeNode *tmpRange = new TRangeNode(itsHead, aType, aLimit1, aLimit2);
  int index = FindRange( *tmpRange );
  if ( index >= 0 )
  {
	  delete tmpRange;
	  return index;
  }
  itsHead = tmpRange;
  return itsCount++;
}


TRangeNode* TRangeTbl::GetRangeNode(int anIndex)

// Find the TRangeNode corresponding to the index argument, and return a
// pointer to it

{
  TRangeNode *iterator = itsHead;
  assert( itsCount > 0 );
  assert( anIndex >= 0 && anIndex < itsCount );
  for (int i=itsCount-1; i > anIndex ; i--)
	 iterator = iterator->next;
  return iterator;
}


TRangeNode* TRangeTbl::operator [](int anIndex)

// overload the [] operator to return a pointer to TRangeNode corresponding
// to the index argument.. functions the same as GetRangeNode

{
  return GetRangeNode(anIndex);
}

double TRangeTbl::GetLimit1(int anIndex)

// Return itsLimit1 for the TRangeNode indexed by "anIndex"

{
  return (*this)[anIndex]->itsLimit1;
}

double TRangeTbl::GetLimit2(int anIndex)

// Return itsLimit2 for the TRangeNode indexed by "anIndex"

{
  // note, you can use either Getrange node or []
  return GetRangeNode(anIndex)->itsLimit2;
}

ERangeType TRangeTbl::CheckRange(int anIndex, double aVal)

// Checks the values suplied in "aVal against the range check conditions
// defined in the TRangeNode indexed by anIndex. If within the specified range
// eNoRange is returned (effectively a 0 or False) otherwise if the value
// fails the range check, the ERangeType of the performed check is returned.

{
	return (*this)[anIndex]->CheckRange(aVal);
}

}; // namespace Parser

#ifdef _TEST_RANGETBL

void main()
{
	int idx2,idx;
	TRangeTbl rt1;
	idx=rt1.AddRange(eInside, 2.0, 9.0);
	idx2=theRangeTable.AddRange(eAbove, 2.0);
	printf("%d %d\n",idx2, (int) theRangeTable.CheckRange(idx2,3.00001));

}

#endif
