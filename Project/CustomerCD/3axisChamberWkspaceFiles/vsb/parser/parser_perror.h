////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_perror.h
//
//  Implements a parser error exception class
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: fletched $
//  $Date: 2002/01/11 05:44:05 $
//  $Revision: 1.4 $
//  $RCSfile: parser_perror.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_perrorH
#define parser_perrorH

#ifndef tool_prefixdecH
    #include "../tool/tool_prefixdec.h"
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif

namespace Parser {

////////////////////////////////////////////////////////////////////////////////
//
//  Error message reporting object
//
////////////////////////////////////////////////////////////////////////////////

class _PREFIXCLASS TParserError
{
public:
  // Constructors

  TParserError():itsErrId(0),itsErrPos(0),itsLine(0),itsDesc(0){};
  TParserError(const TParserError &anSmErr);
  TParserError(const int anErrId, const int anErrPosition, const char *aLine, const char *aDesc);
  TParserError& operator =(const TParserError& anErr);

  // Destructor
  virtual ~TParserError(){Clear();};

  // members

  void  Set(const int anErrId, const int anErrPosition, const char *aLine, const char *aDesc);
  void  Set(const TParserError &anSmErr);
  void  Clear();
  int   Id() const {return itsErrId;};
  int   Pos() const {return itsErrPos;};
  const char* Line() const {return itsLine;};
  const char* Desc() const {return itsDesc;};

  std::string ErrorMessage();

private:
  int   itsErrId;    // Error ID number
  int   itsErrPos;   // Position in source (line number.) where it occurred
  char *itsLine;     // Copy of offending line
  char *itsDesc;     // Error description message

};

}; // namespace Parser

#endif

