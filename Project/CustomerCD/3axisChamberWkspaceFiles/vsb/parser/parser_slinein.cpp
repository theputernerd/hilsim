////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_slinein.cpp
//
//  Derives and string buffer line input class from the parser's 
//  base line input class allowing parser objects to parse a string buffer
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: fletched $
//  $Date: 2002/06/12 07:32:16 $
//  $Revision: 1.5 $
//  $RCSfile: parser_slinein.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_slinein.h"

#ifndef __MATH_H
#include <math.h>
#define __MATH_H
#endif
#ifndef tool_mathtoolsH
    #include <tool_mathtools.h>
#endif

using namespace tool;

namespace Parser {

static int getlinecount(const char *str)
{
   const char *s = str;
   int count = 0;
   if (str)
     count++;
   else
     return count;

   while (*s)
   {
     if (*s == '\n') count++;
     s++;
   }
   return count;

}

TStringLineInp::~TStringLineInp()
{
}

int TStringLineInp::Open(char *str)
{
	itsStr = str;
	itsCurrPos = 0L;
	itsLineCount = 0;
	itsCurrLineText[0] = 0;
	itsCurrLineLength = 0;
	return itsIsOpen;
}

long TStringLineInp::GetLinePos()
{
	return itsCurrPos;
}

int TStringLineInp::SetLinePos(long anOffset)
{

	if (!IsOpen())
	  return 1;               // Stream not open

	int n = getlinecount(itsStr);
	if (n<=0)
	  anOffset = 0L;
	else if (anOffset >= n)
	  anOffset = (long) n;
	itsCurrPos = anOffset;
	return 0;
}

int TStringLineInp::AdvanceLine()
{
  char *c;
  int len;
  itsCurrLineLength = 0;
  if (!IsOpen())
	 return 1;                 // Stream not open

  if (itsCurrPos > getlinecount(itsStr))
	 return 3;                // End of File

  c = itsStr;
  int i=0;
  while (*c && i < itsCurrPos)
  {
    if (*c == '\n') i++;
    c++;
  }

  len = strcspn(c,"\n");
  if (len)
  {
     strncpy(itsCurrLineText, c, tool::min((int)LINELENGTH,len));
    itsCurrLineText[tool::min((int)LINELENGTH,len)] = 0;
  }
  else
    strcpy(itsCurrLineText, "");

  itsCurrPos++;

  itsCurrLineLength = strlen(itsCurrLineText);
  if (itsCurrLineLength>0)
	 for (int i = itsCurrLineLength; i >= 0 && itsCurrLineText[itsCurrLineLength - 1] <= ' '; i--)
		 itsCurrLineText[--itsCurrLineLength] = 0;
  itsLineCount++;
  return  0;                // Success
};

}; // namespace Parser


