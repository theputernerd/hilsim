////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_flinein.cpp
//
//  Derives a file line input class from the parser's base line input class
//  allowing parser objects to parse a file
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/01/30 06:33:46 $
//  $Revision: 1.10 $
//  $RCSfile: parser_flinein.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_flinein.h"

#ifndef tool_compressH
    #include "tool_compress.h"
#endif
#ifndef tool_randnumH
    #include "tool_randnum.h"
#endif
#ifndef __STDIO_H
    #include <stdio.h>
    #define __STDIO_H
#endif


using namespace std;

namespace Parser {

// TFileLineInp's private parts
class TFileLineInp_i
{
public:
	long itsCurrPos;
	int  itsIsOpen;
	FILE* f;
	int itsEncryptionFlag;
};
//-----------------------------------------------------

static const char ENCRYPTION_KEY[] = "CRYPTO";
//-----------------------------------------------------

TFileLineInp::TFileLineInp()
 :  TLineInp()
{
    d_this = new TFileLineInp_i;
    d_this->itsIsOpen = 0;
    d_this->itsCurrPos = 0;
    d_this->f = 0;
    d_this->itsEncryptionFlag = 0;
};


TFileLineInp::~TFileLineInp()
{
  Close();
  delete d_this;
}

int TFileLineInp::Open(const char *fileName)
{
	char compare[20];
	d_this->itsEncryptionFlag = 0;
	d_this->f = fopen(fileName,"r+b");
	if (d_this->f)
	{
	  if (!fseek(d_this->f, -06L, SEEK_END))
	  {
		  fread(compare, strlen(ENCRYPTION_KEY), 1, d_this->f);
		  compare[strlen(ENCRYPTION_KEY)] = 0;
		  d_this->itsEncryptionFlag = (strncmp(compare,ENCRYPTION_KEY,strlen(ENCRYPTION_KEY)) == 0);
	  };
	  fclose(d_this->f);
	}

	d_this->f = fopen(fileName,(d_this->itsEncryptionFlag)?"rb":"rt");
	d_this->itsIsOpen = (d_this->f!=NULL);
	d_this->itsCurrPos = 0L;
	itsLineCount = 0;
	itsCurrLineText[0] = 0;
	itsCurrLineLength = 0;
	return d_this->itsIsOpen;
}

long TFileLineInp::GetLinePos()
{
	return d_this->itsCurrPos;
}


int TFileLineInp::SetLinePos(long anOffset)
{
	if (!d_this->itsIsOpen)
	  return 1;               // Stream not open
	if (fseek(d_this->f, anOffset, SEEK_SET)!=0)
	  return 2;               // Stream pointer error
	return 0;
}


int TFileLineInp::AdvanceLine()
{
  char codedLine[LINELENGTH];
  long pos;
  int  retStatus;
  tool::TRandNumGen randomNum;
  unsigned int linetag = 0;
  itsCurrLineLength = 0;
  if (!d_this->itsIsOpen)
	 return 1;                 // Stream not open
  pos=ftell(d_this->f);
  if (pos == -1L)
  {
	 d_this->itsCurrPos = 0;
	 return  2;               // Stream pointer error
  }  else
	 d_this->itsCurrPos = pos;
  if (d_this->itsEncryptionFlag)
  {
	 randomNum.Seed(ftell(d_this->f)+1L);
	 fread(&linetag,2,1,d_this->f);
	 if (!linetag) return 3;
	 linetag = linetag & (0xFFFF>>1);
	 fread(codedLine,linetag,1,d_this->f);

	 char *c = codedLine;
	 while(*c)
	 {
		//*c ^= 0xC0;
		*c++ ^= (char)(randomNum.NextNum() & 0x1F);
	 }


	 tool::RLDecode(codedLine,itsCurrLineText);
	 retStatus = 0;
  }
  else
	 retStatus = (fgets(itsCurrLineText, LINELENGTH, d_this->f)==NULL);
  if (retStatus)
	 return 3;                // End of File
  itsCurrLineLength = strlen(itsCurrLineText);
  if (itsCurrLineLength>0)
	 if (itsCurrLineText[itsCurrLineLength - 1] == '\n')
		itsCurrLineText[--itsCurrLineLength] = 0;
  itsLineCount++;
  return  0;                // Success

};

int TFileLineInp::IsOpen()
    {return d_this->itsIsOpen;};
int TFileLineInp::IsEncrypted()
    {return d_this->itsEncryptionFlag;};

void TFileLineInp::Close()
{
	if (d_this->itsIsOpen)
	{
		fclose(d_this->f);
		d_this->itsIsOpen = 0;
	}
}

}; // namespace Parser

