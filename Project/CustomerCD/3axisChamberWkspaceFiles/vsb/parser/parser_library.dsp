# Microsoft Developer Studio Project File - Name="parser_library" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=parser_library - Win32 Debug And Profile
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "parser_library.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "parser_library.mak" CFG="parser_library - Win32 Debug And Profile"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "parser_library - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "parser_library - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "parser_library - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
MTL=midl.exe
F90=df.exe
# ADD BASE F90 /include:"Release/"
# ADD F90 /include:"Release/"
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /I "../tool" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Parser_Static___Win32_Debug"
# PROP BASE Intermediate_Dir "Parser_Static___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
MTL=midl.exe
F90=df.exe
# ADD BASE F90 /include:"Parser_Static___Win32_Debug/"
# ADD F90 /include:"Debug/"
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /I "../tool" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "parser_library - Win32 Release"
# Name "parser_library - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\parser.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_baseunit.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_boolvar.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_flinein.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_perror.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_rangetbl.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\parser_slinein.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# Begin Source File

SOURCE=.\stackmac.cpp

!IF  "$(CFG)" == "parser_library - Win32 Release"

# SUBTRACT CPP /YX

!ELSEIF  "$(CFG)" == "parser_library - Win32 Debug"

!ENDIF 

# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\parser.h
# End Source File
# Begin Source File

SOURCE=.\parser_baseunit.h
# End Source File
# Begin Source File

SOURCE=.\parser_boolvar.h
# End Source File
# Begin Source File

SOURCE=.\parser_flinein.h
# End Source File
# Begin Source File

SOURCE=.\parser_perror.h
# End Source File
# Begin Source File

SOURCE=.\parser_rangetbl.h
# End Source File
# Begin Source File

SOURCE=.\parser_slinein.h
# End Source File
# Begin Source File

SOURCE=.\stackmac.h
# End Source File
# Begin Source File

SOURCE=.\stackmac_codes.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
