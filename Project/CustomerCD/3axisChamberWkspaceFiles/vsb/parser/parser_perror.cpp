////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_perror.cpp
//
//  Implements a parser error exception class
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: rehnj $
//  $Date: 2003/01/24 06:50:12 $
//  $Revision: 1.5 $
//  $RCSfile: parser_perror.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_perror.h"

#ifndef __STDIO_H
    #include <stdio.h>
    #define __STDIO_H
#endif

#ifndef tool_mathtoolsH
#include "tool_mathtools.h"
#endif 


using namespace std;

namespace Parser {

TParserError::TParserError(const TParserError &anSmErr): itsLine(0), itsDesc(0),
		itsErrId(0),itsErrPos(0)
{
  Set(anSmErr);
}

TParserError& TParserError::operator =(const TParserError& anErr)
{
  Set(anErr.Id(),anErr.Pos(),anErr.Line(), anErr.Desc());
  return *this;
}

TParserError::TParserError(const int anErrId, const int anErrPosition, const char *aLine, const char *aDesc):
  itsLine(0), itsDesc(0), itsErrId(0),itsErrPos(0)
{
  Set(anErrId,anErrPosition, aLine, aDesc);
}

string TParserError::ErrorMessage()
{
    if (!Id() || !Desc() || !Line())
        return "";

    string temp;

    temp = Desc();
    temp+="\nOccured at or near\n";
    char lineNoStr[40];
    sprintf(lineNoStr,"%04d: ",Pos());
    temp+=lineNoStr;
    temp+=Line();
    return temp;
}


void TParserError::Set(const int anErrId, const int anErrPosition, const char *aLine, const char *aDesc)
{

  Clear();
  itsErrId = anErrId;
  itsErrPos = anErrPosition;
  if (aLine)
  {
	 itsLine = new char[strlen(aLine)+1];
	 if (itsLine) strcpy(itsLine,aLine);     // memory isn't unlimited so cope with worst case scenario
  }
  if (aDesc)
  {
	 itsDesc = new char[strlen(aDesc)+1];
	 if (itsDesc) strcpy(itsDesc,aDesc);
  }
}

void  TParserError::Set(const TParserError &anSmErr)
{
  Set(anSmErr.Id(),anSmErr.Pos(),anSmErr.Line(), anSmErr.Desc());
}

void TParserError::Clear()
{
  itsErrId=itsErrPos=0;
  if (itsLine) delete [] itsLine;
  if (itsDesc) delete [] itsDesc;
  itsLine = itsDesc = 0;
}

}; // namespace Parser

