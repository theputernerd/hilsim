////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_rangetbl.h
//
//  TRangeLimit
//  Defines a range limit definition class with utility member functions
//
//  TRangeTbl
//  Implements a table of ranges for general data entry validation, the idea
//  being that for values with identical valid ranges, the ranges are stored
//  in one central place with savings of storage space and code.
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2003/01/30 06:33:46 $
//  $Revision: 1.4 $
//  $RCSfile: parser_rangetbl.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef parser_rangetblH
#define parser_rangetblH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace Parser {

// Available range tests

enum ERangeType {eNoRange, eAbove, eBelow, eInside, eOutside};

class TRangeNode;


class _PREFIXCLASS TRangeLimit
{
  public:
    TRangeLimit(ERangeType aType, double aLimit1 =0, double aLimit2=0);
    TRangeLimit();
    virtual ~TRangeLimit();
    int operator == (TRangeLimit& rn);
    void       SetRangeLimit(ERangeType aType, double aLimit1 =0, double aLimit2=0);
    ERangeType CheckRange(double aVal);
    double     GetLimit1(){return itsLimit1;};
    double     GetLimit2(){return itsLimit2;};

  protected:
    ERangeType   itsType;
    double       itsLimit1;
    double       itsLimit2;
};


class _PREFIXCLASS TRangeTbl
{

private:

  TRangeNode *itsHead;
  int         itsCount;

public:

  TRangeTbl();
  virtual ~TRangeTbl();
  int GetRangeCount();
  int FindRange(TRangeNode& aRangeNode);
  int FindRange(ERangeType aType, double aLimit1=0, double aLimit2=0);
  int AddRange(ERangeType aType, double aLimit1=0, double aLimit2=0);
  TRangeNode* GetRangeNode(int anIndex);
  TRangeNode* operator [](int anIndex);
  double GetLimit1(int anIndex);
  double GetLimit2(int anIndex);
  ERangeType CheckRange(int anIndex, double aVal);

};

_PREFIXFUNC TRangeTbl& GetRangeTable();

}; // namespace Parser

#endif
