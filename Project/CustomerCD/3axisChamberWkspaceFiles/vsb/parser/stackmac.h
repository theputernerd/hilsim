////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  stackmac.h
//
//  Implements the virtual stack machine which executes the
//  intermediate pseudo instructions that are generated by
//  the parser defined in parser.cpp.
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2002/10/18 07:21:27 $
//  $Revision: 1.4 $
//  $RCSfile: stackmac.h,v $
//
////////////////////////////////////////////////////////////////////////////////

#ifndef stackmacH
#define stackmacH

#ifndef parserH
    #include "parser.h"
#endif
#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif
#ifndef stackmac_codesH
    #include "stackmac_codes.h"
#endif

#ifndef ostreamSTL
    #include <ostream>
#endif

namespace tool {
    class TCompressBuffer;
    class TConsole;
};

namespace Stackmac {

class TInstruction;
class TVariable;
class TLabel;

class _PREFIXCLASS TCodeModule;
typedef void (*TImportFunc)(TCodeModule *cm);

/*****************************************
*
* Stack machine code module structure
*
******************************************/

class _PREFIXCLASS TCodeModule: public Parser::TParseObjectBase
{
    public:

      TCodeModule(int arrHeapSize = 100, TImportFunc anImportFunc = 0);
      virtual ~TCodeModule();
      TCodeModule& operator =(TCodeModule& aModule);

      const char*    Name() {return itsName;};

      void           SetName(char *aName);
      void           SetConsole(tool::TConsole *cmc);
      void           SetupImportGlobals();
      int            DefineGlobalVars(char *varName,...);
      int            DefineGlobalConsts(char *constName,...);
      int            DefineGlobalFuncs(char *funcName...);
      int            DefineLocalVar(char *varName,...);
      int            DefineLocalConsts(char *constName,...);
      void           ClearLocalVars();
      void           ModifyProtection(int prot, char *varName,...);
      void           ModifyProtection(int prot, int varId);
      void           Clear();

      int            ExecuteModule();

      int operator==(const TCodeModule& cm) const;
      int operator!=(const TCodeModule& cm) const;
      int operator==(const char *cmStr) const;
      int operator!=(const char *cmStr) const;

      /** variables used in parsing file **/
      enum ESyntaxStyle {
          NO_SYNTAX_STYLE     = -1,
          C_SYNTAX_STYLE      = 0,
          PASCAL_SYNTAX_STYLE = 1
          };

      int LoadFile(char *filename);
      int Load(Parser::TLineInp *li, int resetFlag);
      int LoadBody(Parser::TLineInp *li, int resetFlag);
      int IsBodyOnly() {return itsLoadBodyOnly;};
      void WriteSource(std::ostream *os, int indent=0);
      void SetSyntaxStyle(const ESyntaxStyle& synStyle){itsImposedStyle = synStyle;};

	   char *GetReport() { return itsMessageString;}
      void  ClearReport() {itsMessageString[0] = 0;};

//    protected:
    private:
      tool::TCompressBuffer*    itsSourceBuff;
      char*               itsName;                  // name of program module
      int                 itsStartLine, itsEndLine; // source code lines spanned

      TInstruction*       itsInstrs;      // instruction stack
      TVariable*          itsVars;         // variable table
      TLabel*             itsLbls;
      int                 instrN;                     // number of instructions
      int                 varsN;                      // number of variables
      int                 labelsN;
      int                 itsLocalVarOffs;
      double*             itsHeap;
      int                 itsHeapSize;
      int                 itsHeapOffs;
      tool::TConsole*           itsConsole;

	  // Report message
	  bool			 itsNewMessage;
	  char *		 itsMessageString;

      int            FindVar(const char *varname);
      int            FindLabel(const char *labelname);

    private:
      ESyntaxStyle  itsImposedStyle;
      ESyntaxStyle  itsSyntaxStyle;
      static char  *theirKeyWords[];
      static char  *theirErrorStrings[];
      static char  *theirExecErrorStrings[];

      TImportFunc  itsImportFunc;
      int          itsLoadBodyOnly;
      int          itsInitialLineNo;

      int          itsStatus;
      long         itsSourceStartOffs;
      long         itsSourceEndOffs;

      int          AddInstructions(int opCode,...);
      int          EmittInstruc(int opCode,...);
      void         ParseFunc();
      void         TestRuntimeError(int deflt_err);
      void         NameExpression();
      void         FuncReference(TVariable *tv);
      void         ArrayReference(TVariable *tv);
      void         Expression();
      void         Relation();
      void         SimpleExpression();
      void         Term();
      void         Factor();
      void         Primary();
      double       NumericConst(int intOnly = 0, int positiveOnly = 0);
      void         WriteStatement(int newLine);
	  void		   ReportStatement();	
      void         ConsoleStatement();
      int          Statement();
      void         StatementList();
      void         AssignStatement(TVariable *tv);
      void         IfStatement();
      void         WhileStatement();
      void         RepeatStatement();
      void         ForStatement();
      void         ArrayConst(char *currvar,int numindexes,int indexes[2]);
      void         ModuleDef();
      void         VariableDef();
};


class _PREFIXCLASS TCodeModuleList
{
//  protected:
  private:
    TCodeModule **itsCModules;
    int           itsCmCount;
    int           itsMaxCount;
  public:
    TCodeModuleList();
    TCodeModuleList(int aMaxCount);
    virtual ~TCodeModuleList();
    void         ClearList();
    int          DeleteModule(const char *aModuleName);
    void         WriteModuleSources(std::ostream *os, int indent=0);
    int          AddModule(TCodeModule *cm, int aReplaceFlag = false);
    int          GetModuleIndex(const char *aModuleName);
    int          GetCount(){return itsCmCount;};
    TCodeModule* operator[](const char *aModuleName);
    TCodeModule* operator[](int aModuleIndex);
    TCodeModule* GetNamedModule(const char *aModuleName);
    TCodeModuleList& operator =(TCodeModuleList& aModuleList);
};

_PREFIXFUNC double HashStringLiteral(const char *s);

}; // namespace Stackmac

#endif

