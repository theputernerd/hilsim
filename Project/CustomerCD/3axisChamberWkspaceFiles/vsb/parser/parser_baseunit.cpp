////////////////////////////////////////////////////////////////////////////////
//
//  GEMM  Generic Missile Model
//  Weapons Systems Division, DSTO, 2000
//
//  parser_baseunit.cpp
//
//  Defines the base class for variable-unit data edit boxes
//  Implements the foundation behaviour for numeric values that have
//  associated units
//
////////////////////////////////////////////////////////////////////////////////
//
//  CVS log
//
//  $Author: smitha $
//  $Date: 2002/09/18 03:02:14 $
//  $Revision: 1.18 $
//  $RCSfile: parser_baseunit.cpp,v $
//
////////////////////////////////////////////////////////////////////////////////

#include "parser_baseunit.h"

#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif

#ifndef __STDLIB_H
    #include <stdlib.h>
    #define __STDLIB_H
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef __ASSERT_H
    #include <assert.h>
    #define __ASSERT_H
#endif
#ifndef __STDIO_H
    #include <stdio.h>
    #define __STDIO_H
#endif

#ifndef tool_strtoolsH
    #include "tool_strtools.h"
#endif

namespace Parser {

using namespace tool;
using namespace tool::constants;
using namespace std;

#define CVTOFFS     (theCvtOffset[itsType])
#define CVTLASTOFFS (theCvtOffset[itsType] + theCvtConstCount[itsType])

char *TUnitValue::theUnitNameStrs[] =
{
    "m"       "\0"   "metres"  				"\0"   "%0lg"  "\0"   "1",
    "km"      "\0"   "kilometres"			"\0"   "%0lg"	"\0"   "0.01",
    "ft"      "\0"   "feet"					"\0"   "%0lg"	"\0"   "1",
    "kft"     "\0"   "kilofeet"				"\0"   "%0lg"	"\0"   "0.01",
    "nm"      "\0"   "nautical miles"		"\0"   "%0lg"	"\0"   "0.01",

    "m/s"     "\0"   "metres/sec"			"\0"   "%0lg"	"\0"   "1",
    "ft/s"    "\0"   "feet/sec"				"\0"   "%0lg"	"\0"   "1",
    "kph"     "\0"   "kilometres/hour"	   "\0"   "%0lg"	"\0"   "1",
    "kts"     "\0"   "knots"					"\0"   "%0lg"	"\0"   "1",

    "rad"     "\0"   "radians"				"\0"   "%0lg"	"\0"   "0.01",
    "deg"     "\0"   "degrees"		 		"\0"   "%0lg"	"\0"   "1",

    "rad/s"   "\0"   "radians/sec"			"\0"   "%0lg"	"\0"   "0.01",
    "deg/s"   "\0"   "degrees/sec"			"\0"   "%0lg"	"\0"   "1",

    "rad/s^2" "\0"   "radians/sec^2"		"\0"   "%0lg"	"\0"   ".01",
    "deg/s^2" "\0"   "degrees/sec^2"		"\0"   "%0lg"	"\0"   "1",

    "kg"      "\0"   "kilograms"			   "\0"   "%0lg"	"\0"   "0.1",
    "lb"      "\0"   "pounds"				   "\0"   "%0lg"	"\0"   "0.1",

    "kg/s"    "\0"   "kilograms/sec"		"\0"   "%0lg"	"\0"   "0.1",
    "lb/s"    "\0"   "pounds/sec"			"\0"   "%0lg"	"\0"   "0.1",

    "m/s^2"   "\0"   "metres/sec^2"		   "\0"   "%0lg"	"\0"   "0.1",
    "ft/s^2"  "\0"   "feet/sec^2"			"\0"   "%0lg"	"\0"   "0.1",
    "kts/s"   "\0"   "knots/sec"			   "\0"   "%0lg"	"\0"   "0.1",
    "g"       "\0"   "g's"					   "\0"   "%0lg"	"\0"   "0.1",

    "sec"     "\0"   "seconds"				"\0"   "%0lg"	"\0"   "0.1",
    "min"     "\0"   "minutes"				"\0"   "%0lg"	"\0"   "0.01",
    "msec"    "\0"   "milliseconds"	   		"\0"   "%0lg"	"\0"   "1",

    "N"       "\0"   "newtons"				"\0"   "%0lg"	"\0"   "1",
    "lbf"     "\0"   "pounds-force"		   "\0"   "%0lg"	"\0"   "1",

    ""        "\0"   ""						   "\0"   "%0lg"	"\0"   "0.01",
    NULL
};

char *TUnitValue::theUnitKeys[] =
{
    "DISTANCE",     "ALTITUDE",     "VELOCITY",
    "ANGLE",        "ANGULAR_RATE", "ANGULAR_ACCEL", "MASS",
    "MASS_RATE",    "ACCELERATION", "LAT_ACCELERATION", "TIME", "FORCE",
    NULL
};

EUnitName
    TUnitValue::theDistNames[]      = { eM, eKm, eFt, eKFt, eNM },
    TUnitValue::theSpeedNames[]     = { eMPS, eFtPS, eKPH, eKt },
    TUnitValue::theAngleNames[]     = { eRad, eDeg },
    TUnitValue::theAngRateNames[]   = { eRadPS, eDegPS },
    TUnitValue::theAngAccelNames[]  = { eRadPSSq, eDegPSSq },
    TUnitValue::theMassNames[]      = { eKg, eLb },
    TUnitValue::theMassRateNames[]  = { eKgS, eLbS },
    TUnitValue::theAccelNames[]     = { eMPSSq, eFtPSSq, eKtPSSq, eG },
    TUnitValue::theTimeNames[]      = { eSec, eMin, eMSec },
    TUnitValue::theForceNames[]     = { eN, eLbf },
    TUnitValue::theNoUnitNames[]    = { eNone };

int TUnitValue::theCvtConstCount[eNoUnit+1] = { 5, 5, 4, 2, 2,  2,  2,  2,  4,  4,  3,  2, 1 };
int TUnitValue::theCvtOffset[eNoUnit+1] =     { 0, 0, 5, 9, 11, 13, 15, 17, 19, 19, 23, 26, 28};

static double theDistCvtConsts[] =  { 1.0,
    M_TO_KM, M_TO_FT, M_TO_KFT, M_TO_NMI };
static double theSpeedCvtConsts[]  =	{ 1.0,
    M_TO_FT, MPS_TO_KPH, MPS_TO_KTS };
static double theAngleCvtConsts[]=  { 1.0,
    RAD_TO_DEG };
static double theMassCvtConsts[] =  { 1.0,
    KG_TO_LB };
static double theAccelCvtConsts[] = { 1.0,
    M_TO_FT, MPS_TO_KTS, MPS2_TO_G };
static double theTimeCvtConsts[] = { 1.0,
    SEC_TO_MIN, SEC_TO_MILLISEC };
static double theForceCvtConsts[] = { 1.0,
    N_TO_LBF};
static double theNoUnitCvtConsts[] ={ 1.0 };


EUnitName *TUnitValue::theUnitNames[]  = { theDistNames,    theDistNames,
												       theSpeedNames,   theAngleNames,
												       theAngRateNames, theAngAccelNames,
												       theMassNames,    theMassRateNames,
												       theAccelNames,   theAccelNames,
												       theTimeNames,    theForceNames,
												       theNoUnitNames,
												       NULL
};

static double *theUnitCvtConsts[] = { theDistCvtConsts,  theDistCvtConsts,
												  theSpeedCvtConsts,  theAngleCvtConsts,
												  theAngleCvtConsts,  theAngleCvtConsts,
												  theMassCvtConsts,   theMassCvtConsts,
												  theAccelCvtConsts,  theAccelCvtConsts,
												  theTimeCvtConsts,   theForceCvtConsts,
												  theNoUnitCvtConsts,
												  NULL
};


static TUnitConfig *theUserUnits = 0;

int TUnitValue::GetUnitNum(EUnitType aUnitType, EUnitName aUnit)
{
	return (int) aUnit - (int) TUnitValue::theCvtOffset[aUnitType];
}


_PREFIXFUNC TUnitConfig *& GetUserUnits()
{
  return theUserUnits;
}



EUnitType TUnitValue::MatchUnitKey(const char *unitKeyName)
{
  for (int i=0; unitKeyName && TUnitValue::theUnitKeys[i]; i++)
  {
	 if (!StrCmpNoCase(unitKeyName, TUnitValue::theUnitKeys[i])) return (EUnitType) i;
	 if (!stricmp(unitKeyName, TUnitValue::theUnitKeys[i])) return (EUnitType) i;
  }
  return eNoUnit;
}

EUnitName TUnitValue::MatchUnitName(const char *unitName)
{
  for (int i=0; unitName && TUnitValue::theUnitNameStrs[i]; i++)
  {
	 if (!StrCmpNoCase(unitName, TUnitValue::theUnitNameStrs[i])) return (EUnitName)i;
	 if (!stricmp(unitName, TUnitValue::theUnitNameStrs[i])) return (EUnitName)i;
  }
  return eNone;
}

int  TUnitValue::GetUnitCount(EUnitType aUnitType) 
{
  return TUnitValue::theCvtConstCount[aUnitType];
}



const char* TUnitValue::GetUnitClassNameStr(EUnitType aUnitClass)
{
  assert(aUnitClass <= eNoUnit && (int) aUnitClass >= 0);
  if (aUnitClass == eNoUnit) return "";
  return  TUnitValue::theUnitKeys[aUnitClass];
}

const char* TUnitValue::GetUnitNameStr(EUnitName aUnit)
{
  return TUnitValue::theUnitNameStrs[aUnit];
}

const char* TUnitValue::GetUnitNameStr(EUnitType aUnitType, int aUnitIdx)
{
	EUnitName aUnit;
	assert(aUnitIdx >=  0  && aUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
	aUnit = (EUnitName)(TUnitValue::theCvtOffset[aUnitType]+aUnitIdx);
	return GetUnitNameStr(aUnit);
}

const char* TUnitValue::GetUnitLongNameStr(EUnitName aUnit)
{
	char *tempStr = TUnitValue::theUnitNameStrs[aUnit];
	while (*tempStr++);
	return tempStr;
}

const char* TUnitValue::GetUnitLongNameStr(EUnitType aUnitType, int aUnitIdx)
{
	EUnitName aUnit;
	assert(aUnitIdx >=  0  && aUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
	aUnit = (EUnitName)(TUnitValue::theCvtOffset[aUnitType]+aUnitIdx);
	return GetUnitLongNameStr(aUnit);
}

const char* TUnitValue::GetUnitFormatStr(EUnitName aUnit)
{
	char *tempStr = TUnitValue::theUnitNameStrs[aUnit];
	while (*tempStr++);
	while (*tempStr++);
	return tempStr;
}

const char* TUnitValue::GetUnitFormatStr(EUnitType aUnitType, int aUnitIdx)
{
	EUnitName aUnit;
	assert(aUnitIdx >=  0  && aUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
	aUnit = (EUnitName)(TUnitValue::theCvtOffset[aUnitType]+aUnitIdx);
	return GetUnitFormatStr(aUnit);
}


const char* TUnitValue::GetUnitCombinedNameStr(EUnitName aUnit)
{
	static char tempStr[40];
	sprintf(tempStr,"%s (%s)", GetUnitLongNameStr(aUnit),
	  GetUnitNameStr(aUnit));
	return tempStr;
}

const char* TUnitValue::GetUnitCombinedNameStr(EUnitType aUnitType, int aUnitIdx)
{
	EUnitName aUnit;
	assert(aUnitIdx >=  0  && aUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
	aUnit = (EUnitName)(TUnitValue::theCvtOffset[aUnitType]+aUnitIdx);
	static char tempStr[40];
	sprintf(tempStr,"%s (%s)", GetUnitLongNameStr(aUnit),
	  GetUnitNameStr(aUnit));
	return tempStr;
}

double TUnitValue::ConvertFromRawValue(EUnitType aUnitType, int aToUnitIdx, double aValue)
{
	assert(aToUnitIdx >= 0 && aToUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);

	if (aUnitType == eNoUnit) return aValue;
	return aValue * theUnitCvtConsts[aUnitType][aToUnitIdx];
}

double TUnitValue::ConvertValueUnit(EUnitType aUnitType, int aFromUnitIdx, int aToUnitIdx, double aValue)
{
	assert(aToUnitIdx >= 0 && aToUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
	assert(aFromUnitIdx >= 0 && aFromUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);

	if (aUnitType == eNoUnit || aFromUnitIdx == aToUnitIdx) return aValue;
	return (aValue / theUnitCvtConsts[aUnitType][aFromUnitIdx]) * theUnitCvtConsts[aUnitType][aToUnitIdx];
}



////////////////////////////////////////////////////////////////////

TUnitConfig::TUnitConfig()
{
  int i;
  itsUnits = new int[eNoUnit+1];
  for (i=0; i <= eNoUnit; i++)
	 itsUnits[i] = 0;
}

TUnitConfig::~TUnitConfig()
{
  delete [] itsUnits;
}


void TUnitConfig::Set(EUnitType aUnitType, int aUnitIdx)
{
  assert(aUnitType >= eDist && aUnitType < eNoUnit);
  assert(aUnitIdx >= 0 && aUnitIdx < TUnitValue::theCvtConstCount[aUnitType]);
  itsUnits[aUnitType] = aUnitIdx;
}

void TUnitConfig::Set(EUnitType aUnitType, EUnitName aUnitEnum)
{
	Set(aUnitType, (int) aUnitEnum - TUnitValue::theCvtOffset[aUnitType]);
}

int TUnitConfig::GetIdx(EUnitType aUnitType)
{
	if (aUnitType == eNoUnit) return 0;
	return itsUnits[aUnitType];
}

EUnitName  TUnitConfig::GetEnum(EUnitType aUnitType)
{
	return EUnitName(TUnitValue::theCvtOffset[aUnitType] + GetIdx(aUnitType));
}


////////////////////////////////////////////////////////////////////////
//
//  CLASS:   TUnitValue
//  AUTHOR:  T Gouthas   15/12/95
//  PURPOSE:  This defines a class for values with changeable units
//            Ie a double precision value and a unit type specifier
//            There are several categories of units, ie Distance, Velocity
//            Angle and Angular rate.
//            Facilities are provided to handle a given value correctly
//            when supplied in different units as long as the unit categories
//            match
//
//            There are two sets of each method that requires a unit parameter
//            one for the unit specified as an enumerated type, and one for
//            the unit specified as an index (unit types are stored internally
//            as an index ranging from 0 to n-1 where n is the number of units
//            this value may have.
//
//
//  MODIFIED:
////////////////////////////////////////////////////////////////////////



// DESSTRUCTOR:
// Currently not necessary

TUnitValue::~TUnitValue()
{
    delete itsLocalRange;
}


//  Constructor
//
//

TUnitValue::TUnitValue(EUnitType aUnitType, double aValue, int aUnitIdx,
							  int aRange, bool defltUnits)
{
    itsLocalRange = new TRangeLimit;

	assert(aUnitIdx >= 0 && aUnitIdx < theCvtConstCount[aUnitType]);
	itsType = aUnitType;
	itsValue = aValue;
	itsUnit = aUnitIdx;
	itsRangeId = aRange;
	if (defltUnits) SetDefaultUnit();
}

TUnitValue::TUnitValue(EUnitType aUnitType, double aValue, EUnitName aUnit,
							  int aRange, bool defltUnits)
{
    itsLocalRange = new TRangeLimit;

	itsType = aUnitType;
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
	itsValue = aValue;
	itsUnit = aUnit - CVTOFFS;
	itsRangeId  = aRange;
	if (defltUnits) SetDefaultUnit();
}


TUnitValue::TUnitValue(const TUnitValue& copyRef)
{
  itsType   = copyRef.itsType;
  itsUnit   = copyRef.itsUnit;
  itsValue  = copyRef.itsValue;
  itsRangeId  = copyRef.itsRangeId;
  itsLocalRange = new TRangeLimit(*copyRef.itsLocalRange);
}

TUnitValue TUnitValue::operator = (const TUnitValue& copyRef)
{
  assert(copyRef.itsType == itsType);
  SetValueAndUnit(copyRef.itsValue, copyRef.itsUnit);
  return TUnitValue(itsType, itsValue, itsUnit);
}


TUnitValue TUnitValue::operator + (TUnitValue& uv)
{
  assert(itsType == uv.itsType);
  return TUnitValue(itsType, GetValue() + uv.GetValue(), itsUnit);
}

TUnitValue TUnitValue::operator - (TUnitValue& uv)
{
  assert(itsType == uv.itsType);
  return TUnitValue(itsType, GetValue() - uv.GetValue(), itsUnit);
}

TUnitValue TUnitValue::operator * (TUnitValue& uv)
{
  assert(itsType == uv.itsType);
  return TUnitValue(itsType, GetValue() * uv.GetValue(), itsUnit);
}

TUnitValue TUnitValue::operator / (TUnitValue& uv)
{
  assert(itsType == uv.itsType);
  return TUnitValue(itsType, GetValue() / uv.GetValue(), itsUnit);
}


double TUnitValue::GetRawValue() const
{
	return itsValue;
};

EUnitType TUnitValue::GetUnitType() const
{
  return itsType;
}

int TUnitValue::GetUnitIdx() const
{
	return itsUnit;
};

int TUnitValue::GetMaxUnitIdx() const
{
	return theCvtConstCount[itsType];
};


EUnitName TUnitValue::GetUnit() const
{
	return EUnitName(itsUnit+CVTOFFS);
};


void TUnitValue::SetValueAndUnit(double aValue, int aUnitIdx)
{
	assert(aUnitIdx >= 0 && aUnitIdx <   theCvtConstCount[itsType]);
	itsValue = aValue;
	itsUnit = aUnitIdx;
}

void TUnitValue::SetValueAndUnit(double aValue, EUnitName aUnit)
{
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
	itsValue = aValue;
	itsUnit = aUnit - CVTOFFS;
}

double TUnitValue::ConvertValue(int aFromUnitIdx, int aToUnitIdx, double aValue) const
{
	double intermediateValue;

	assert(aFromUnitIdx >= 0 && aFromUnitIdx < theCvtConstCount[itsType]);
	assert(aToUnitIdx >= 0 && aToUnitIdx < theCvtConstCount[itsType]);

	if (itsType == eNoUnit) return aValue;

	intermediateValue = aValue/theUnitCvtConsts[itsType][aFromUnitIdx];
	intermediateValue *=  theUnitCvtConsts[itsType][aToUnitIdx];
	return intermediateValue;
}


double TUnitValue::ConvertValue(EUnitName aFromUnit, EUnitName aToUnit, double aValue) const
{
	double intermediateValue;

	assert(aFromUnit >= CVTOFFS && aFromUnit < CVTLASTOFFS);
	assert(aToUnit >= CVTOFFS && aToUnit < CVTLASTOFFS);

	if (itsType == eNoUnit) return aValue;

	intermediateValue = aValue/theUnitCvtConsts[itsType][aFromUnit - CVTOFFS];
	intermediateValue *=  theUnitCvtConsts[itsType][aToUnit - CVTOFFS];
	return intermediateValue;
}


void TUnitValue::SetValue(double aValue, int aUnitIdx)
{
	assert(aUnitIdx >= 0 && aUnitIdx < theCvtConstCount[itsType]);
	itsValue = ConvertValue(aUnitIdx, itsUnit, aValue);
}

void TUnitValue::SetValue(double aValue, EUnitName aUnit)
{
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
	itsValue = ConvertValue(aUnit - CVTOFFS, itsUnit, aValue);
}


void TUnitValue::SetValue(const TUnitValue *src)
{
  SetValue(src->itsValue,src->itsUnit);
}

void TUnitValue::SetValueAndUnit(const TUnitValue *src)
{
  SetValueAndUnit(src->itsValue,src->itsUnit);
}


void TUnitValue::ChangeUnit(int aUnitIdx)
{
	assert(aUnitIdx >= 0 && aUnitIdx < theCvtConstCount[itsType]);
	if (itsUnit == aUnitIdx) return;
	SetValueAndUnit( ConvertValue(itsUnit, aUnitIdx, itsValue), aUnitIdx);
}

void TUnitValue::ChangeUnit(EUnitName aUnit)
{
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
	ChangeUnit( (int)(aUnit - CVTOFFS) );
}

void TUnitValue::SetDefaultUnit()
{
  ChangeUnit((theUserUnits != NULL)?theUserUnits->GetIdx(itsType):0);
}

double TUnitValue::GetValue(int aUnitIdx) const
{
	assert(aUnitIdx >= 0 && aUnitIdx < theCvtConstCount[itsType]);
	return ConvertValue(itsUnit, aUnitIdx, itsValue);
}


double TUnitValue::GetValue(EUnitName aUnit) const
{
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
	return ConvertValue(itsUnit, aUnit-CVTOFFS, itsValue);
}


void TUnitValue::ResetUnitType(EUnitType aUnitType, double aValue, int aUnitIdx)
{
	itsType = aUnitType;
	itsValue = aValue;
	itsUnit = aUnitIdx;
}


void TUnitValue::SetValidRange(int aRangeIdx)
{
  assert(aRangeIdx >= 0 && aRangeIdx < GetRangeTable().GetRangeCount());
  itsRangeId = (signed char) aRangeIdx;
}

void TUnitValue::SetValidRange(const ERangeType& aType, double aLimit1, double aLimit2)
{
  itsRangeId = -1;
  itsLocalRange->SetRangeLimit(aType, aLimit1, aLimit2);
}


ERangeType TUnitValue::CheckRange(double aValue, int aUnitIdx) const
{
	assert(aUnitIdx >= 0 && aUnitIdx < theCvtConstCount[itsType]);
   if (itsRangeId >= 0)
	  return GetRangeTable().CheckRange(itsRangeId, ConvertValue(aUnitIdx, 0, aValue) );
   else
     return itsLocalRange->CheckRange(ConvertValue(aUnitIdx, 0, aValue));
}

ERangeType TUnitValue::CheckRange(double aValue, EUnitName aUnit) const
{
	assert(aUnit >= CVTOFFS && aUnit < CVTLASTOFFS);
   if (itsRangeId >= 0)
	  return GetRangeTable().CheckRange(itsRangeId,
	    ConvertValue(aUnit - CVTOFFS, 0, aValue) );
   else
     return itsLocalRange->CheckRange(ConvertValue(aUnit - CVTOFFS, 0, aValue) );
}

ERangeType TUnitValue::CheckRange() const
{
   if (itsRangeId >= 0)
	  return GetRangeTable().CheckRange(itsRangeId, ConvertValue(itsUnit, 0, itsValue) );
   else
     return itsLocalRange->CheckRange( ConvertValue(itsUnit, 0, itsValue) );
}

const char *TUnitValue::ToString(double aValue, EUnitName aUnit) const
{
   static char fmtBuff[60];
   sprintf( fmtBuff, GetUnitFormatStr(aUnit), aValue );
   if (aUnit != eNone)
   {
     strcat(fmtBuff,"  ");
     strcat(fmtBuff,theUnitNameStrs[(int)aUnit]);
   }
   return fmtBuff;
}


const char *TUnitValue::ToString(double aValue, int aUnitIdx) const
{
	return ToString(aValue, theUnitNames[itsType][aUnitIdx]);
}

const char *TUnitValue::ToString(EUnitName aUnit) const
{
	return ToString(GetValue(aUnit), aUnit);
}


const char *TUnitValue::ToString() const
{
	return ToString(itsValue, itsUnit);
}

const char *TUnitValue::ToStringNS(double aValue, EUnitName aUnit) const
{
   static char fmtBuffNS[60];

   // Get format string that defines number of significant figures
  	char *tempStr = TUnitValue::theUnitNameStrs[aUnit];
	while (*tempStr++);
	while (*tempStr++);
	while (*tempStr++);

   double precision = atof(tempStr);
   
   sprintf( fmtBuffNS, "%lg", roundTo(aValue,precision));
   if (aUnit != eNone)
   {
     strcat(fmtBuffNS,theUnitNameStrs[(int)aUnit]);
   }
   return fmtBuffNS;
}


const char *TUnitValue::ToStringNS(double aValue, int aUnitIdx) const
{
	return ToStringNS(aValue, theUnitNames[itsType][aUnitIdx]);
}

const char *TUnitValue::ToStringNS(EUnitName aUnit) const
{
	return ToStringNS(GetValue(aUnit), aUnit);
}


const char *TUnitValue::ToStringNS() const
{
	return ToStringNS(itsValue, itsUnit);
}


// function GetMatch
// Returns the index of the first unit type name that the string *str matches.
// the comparison is based on the length of the str parameter, ie if str
// is only one characyer long, only the first character of all the candidate
// unit name strings are tested.

int TUnitValue::GetMatch(const char *str, EUnitType unitType)
{
  int len;
  if (str==NULL) return -1;
  len = strlen(str);
  if (len < 1) return -1;
  for (int i=0; i < TUnitValue::theCvtConstCount[unitType]; i++)
  {
	 if (!strncmp(str, TUnitValue::theUnitNameStrs[TUnitValue::theCvtOffset[unitType]+i], len))
		return i;
//	 if (!strncmpi(str, TUnitValue::theUnitNameStrs[TUnitValue::theCvtOffset[unitType]+i], len))
//		return i;
  }
  return -1;
}


}; // namespace Parser


