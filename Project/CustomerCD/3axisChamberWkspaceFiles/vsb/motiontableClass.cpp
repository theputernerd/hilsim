#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include "MotionTableClass.h"

#include "ssgLocal.h"


#include <string>
//using namespace std;

const char *mtRoot::getTypeName(void) { return "mtRoot"; }

void mtRoot::copy_from ( mtRoot *src, int clone_flags )
{
	ssgBranch::copy_from ( src, clone_flags ) ;
}

ssgBase *mtRoot::clone ( int clone_flags )
{
	mtRoot *b = new mtRoot ;
	b -> copy_from ( this, clone_flags ) ;
	return b ;
}


mtRoot::mtRoot (void)
{
	mInitialised = 0;
	type = ssgTypeBranch () ;
}

mtRoot::~mtRoot (void)
{
}


int mtRoot::load ( FILE *fd )
{
	return ssgBranch::load(fd) ;
}

int mtRoot::save ( FILE *fd )
{
	return ssgBranch::save(fd) ;
}

void mtRoot::Initialise(const char *model_path)
{
  mInitialised = true;
  ssgTransform *heightAdjust = new ssgTransform; 
  this->addKid(heightAdjust);
  mXfm[0] = heightAdjust;					// transform the base to sit on the ground
  sgMat4 init_heightadjust_mat;
  sgMakeTransMat4(init_heightadjust_mat, 0, 0, 1.1);
  ((ssgTransform*)mXfm[0])->setTransform(init_heightadjust_mat);
  
  mXfm[3] = new ssgTransform; mXfm[0]->addKid(mXfm[3]);		// outer axis
  mXfm[2] = new ssgTransform; mXfm[3]->addKid(mXfm[2]);		// centre axis
  mXfm[1] = new ssgTransform; mXfm[2]->addKid(mXfm[1]);		// inner axis

  std::string qualified_model_name;
  
  for (int i=0; i<4; i++)
  {//The 3ds files are loaded in here

	   qualified_model_name = model_path;
	   qualified_model_name += '\\';
	   qualified_model_name += "motiontable";
	   qualified_model_name += '0'+i;
	   qualified_model_name += ".3ds";
	   printf("%s\n",qualified_model_name.c_str());
	   ssgEntity *entity = ssgLoad3ds(qualified_model_name.c_str());
	   if (!entity) printf("failed to load \"%s\"\n",qualified_model_name.c_str());
	   mXfm[i]->addKid(entity);
  }


}


void mtRoot::SetAngle(int axis_id, double angle)
{
	sgMat4 rot_mat;
	sgVec3 axes[4] = {{0,0,0},{0,1,0},{0,0,1},{1,0,0}};
	if (axis_id < 1 || axis_id> 3) return;
	sgMakeRotMat4   ( rot_mat, (float) angle, axes[axis_id]) ;
	((ssgTransform*)mXfm[axis_id])->setTransform(rot_mat);
}