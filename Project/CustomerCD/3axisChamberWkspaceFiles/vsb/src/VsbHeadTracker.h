#ifndef _vsbHeadTracker_H
#define _vsbHeadTracker_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef SG_H
#include "sg.h"
#endif


#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

// Tracker device base class

class DLL_VSB_API vsbHeadTrackerDeviceInterface  
{
public:
								vsbHeadTrackerDeviceInterface();

	virtual						~vsbHeadTrackerDeviceInterface();

	const std::string			&DriverName(){return m_DriverName;};
	void						DriverName(const char *name){m_DriverName = name;};

	virtual const std::string &	DriverIdString() const = 0;
	virtual std::string			GetStatusString() = 0;
	virtual bool				Start() = 0;
	virtual void				Stop() = 0;
	virtual void				ResetAngles() = 0;
	virtual void				GetAngles(sgVec3 hpr) = 0;
	virtual bool				IsActive() {return false;};
private:

	std::string					m_DriverName;	

};


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbHeadTrackerDeviceFactory
    \brief  This class defines the pure abstract base factory object
    
    Used to derrive a factory for a specific entity.
*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbAbstractHeadTrackerDeviceFactory 
{
public:
	virtual vsbHeadTrackerDeviceInterface *Create() = 0;
};


template <class T> class DLL_VSB_API vsbHeadTrackerDeviceFactory : public vsbAbstractHeadTrackerDeviceFactory
{
public:

    /*! \brief Constructor
       \param em Pointer to the entity manager that will host the new entity

       The entity is given a default unique name.
    */
	vsbHeadTrackerDeviceInterface *Create()
	{
		return new T;
	}

};



class vsbHeadTrackerInterface
{
public:
	vsbHeadTrackerInterface();
	virtual ~vsbHeadTrackerInterface();

	int					AvailableDrivers();
	const std::string &	DriverName(int i); 


	bool				SelectNewTracker(const std::string &name);
	bool				SelectNewTracker(int i);

	bool				DriverInstalled();
	bool				DriverIsActive();
	const std::string &	DriverIdString();
	std::string			GetStatusString();
	bool				Start();
	void				Stop();
	void				ResetAngles();
	void				GetAngles(sgVec3 hpr);


	static bool RegisterHeadTrackerDeviceFactory(const std::string &entityType, vsbAbstractHeadTrackerDeviceFactory *deviceInterfaceFactory);
	static void DestroyFactories();	
private:

	vsbHeadTrackerDeviceInterface *m_pCurrTrackerDevice;

};




/*! \brief Defines a macro for registering new head tracker interface classes.

    Registers a new head tracker interface with the vsbHeadTracker's interface factory.
    Once an interface. 
*/

#define REGISTER_HEAD_TRACKER_DEVICE_FACTORY(trackerInterface, name) \
	vsbHeadTrackerInterface::RegisterHeadTrackerDeviceFactory(name, new vsbHeadTrackerDeviceFactory<trackerInterface>)


#endif 