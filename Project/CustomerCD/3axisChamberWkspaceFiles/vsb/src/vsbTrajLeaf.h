#ifndef _vsbTrajLeaf_H
#define _vsbTrajLeaf_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

class DLL_VSB_API vsbTrajEntity;

class DLL_VSB_API vsbTrajLeaf: public ssgVTable
{
friend int vsbTrajLeafPredraw(ssgEntity *entity);
public:
    vsbTrajLeaf(vsbTrajEntity *trajEntity);
    vsbTrajEntity &TrajEntity()	{return *mTrajEntity;};
	int NumVertices() const		{return mNumVertices; }; 
	void NumVertices(int n)		{mNumVertices = n;};

	int IdxT() const {return mIdxT;};
	int IdxX() const {return mIdxX;};
	int IdxY() const {return mIdxY;};
	int IdxZ() const {return mIdxZ;};
private: 
	vsbTrajEntity *mTrajEntity;
	int mNumVertices;
	ssgCullResult cull_test  ( sgFrustum *f, sgMat4 m, int test_needed ) ;
	void recalcBSphere ();
	int mIdxX, mIdxY, mIdxZ, mIdxT;
};

#endif