
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
using namespace std;

#ifdef WIN32
#include <windows.h>
#endif

#include<GL/gl.h>

#include "vsbHUDFramework.h"
#include "vsbStrokedText.h"
#include "vsbViewportContext.h"
#include "vsbFollowCamera.h"


vsbStrokedText  vsbHUDComponent::mStrokedText;

vsbHUDComponent::vsbHUDComponent() :
	m_px( 0.0f ),
	m_py( 0.0f ),
	m_sx( 1.0f ),
	m_sy( 1.0f )
{
    mIsEnabled = false;
    mpHUDManager = 0;
}



vsbRGB vsbHUDFramework::mHUDColour(DEFAULT_HUD_R, DEFAULT_HUD_G, DEFAULT_HUD_B);
vsbRGB vsbHUDFramework::mHUDBackground(DEFAULT_HUD_BG_R, DEFAULT_HUD_BG_G, DEFAULT_HUD_BG_B, DEFAULT_HUD_BG_A);
bool vsbHUDFramework::mAntialiaseLineDrawing = true;
float vsbHUDFramework::mAntialiasedThickness = 1.2f;
int vsbHUDFramework::mPixMargin = 0;


vsbHUDFramework::vsbHUDFramework() : 
	mpCockpit(0),
	m_px( 0.0f ),
	m_py( 0.0f ),
	m_sx( 1.0f ),
	m_sy( 1.0f )
{
};

vsbHUDFramework::~vsbHUDFramework()
{
    DeleteAllHUDComponents();
}

void vsbHUDFramework::Reshape(int w, int h)
{
	// chop pixel margin out of possible HUD area
	bool margin_overflow = false;

	float nw = w-2*mPixMargin;
	if (nw<0) margin_overflow = true;

	float nh = h-2*mPixMargin;
	if (nh<0) margin_overflow = true;

	if (margin_overflow) 
	{
		nw = w;
		nh = h;
	}


	int x_offset = 0;
	int y_offset = 0;


	float pix_aspect = nw / nh;

	//Determine a correct aspect ratio box, within the viewing area 

	if (pix_aspect > mLogicalAspect)
	{
		mPixelCoords[0] = (w - mLogicalAspect * nh) / 2 ;
		mPixelCoords[1] = (margin_overflow?0:mPixMargin);
		mPixelCoords[2] = w - mPixelCoords[0];
		mPixelCoords[3] = h - (margin_overflow?0:mPixMargin);

	} else if (pix_aspect < mLogicalAspect)
	{
		mPixelCoords[0] = (margin_overflow?0:mPixMargin);
		mPixelCoords[1] = (h - nw / mLogicalAspect) / 2 ;
		mPixelCoords[2] = w - (margin_overflow?0:mPixMargin);
		mPixelCoords[3] = h - mPixelCoords[1];
	}

	
}


void vsbHUDFramework::DrawHud(vsbCameraManager *cam)
{
	if (cam != 0 && cam->ActiveCam() && cam->ActiveCam()->HudMode() == HUD_COCKPIT && mpCockpit!= 0)
	{
		vsbFollowCamera *follow_cam = dynamic_cast<vsbFollowCamera *>(cam->ActiveCam());
		if (follow_cam && follow_cam->IsModeEnabled(vsbFollowCamera::FOLLOW_DESIGNATED_ENTITY))
		{	
			vsbBaseViewEntity *entity_to_follow = vsbViewportContext::EntityManager().LookupEntity(follow_cam->EntityToFollow());
			
			if (entity_to_follow)
			{
			/*			sgCoord coord;
			sgCopyVec3(coord.xyz,entity_to_follow->EntityPosXYZ());
			sgCopyVec3(coord.hpr,entity_to_follow->OrientationHPR());
			vsbToScaledVSBCoords(coord);
			
			  sgMat4 xfm;
			  
				sgMakeCoordMat4(xfm,&coord);
				
				  vsbModelEntry *model_entry;
				  model_entry = vsbViewportContext::ModelManager().FindByTagName(entity_to_follow->AttachedModelName(0));
				  if (model_entry)
				  {
				  sgMat4 cockpit_mat;
				  sgCopyMat4( cockpit_mat, model_entry->CockpitMat() );
				  cockpit_mat[3][0] *= entity_to_follow->EntityRoot()->getScale();
				  cockpit_mat[3][1] *= entity_to_follow->EntityRoot()->getScale();
				  cockpit_mat[3][2] *= entity_to_follow->EntityRoot()->getScale();
				  
					
					  xfm[3][0] = 0;//@@
					  xfm[3][1] = 0;
					  xfm[3][2] = 0;
					  
						sgPreMultMat4(xfm, cockpit_mat);
						}
						
						  mpCockpit->Reposition(xfm);
				*/
				
				
				glMatrixMode(GL_MODELVIEW);
//				glPushMatrix();
				
				
				sgdCoord cockpit_cam_coord;
				sgdSetVec3(cockpit_cam_coord.xyz, 0,0,0);
				sgdSetVec3(cockpit_cam_coord.hpr, 0,0,0);
				
				if (cam->ActiveCam()->HeadTrackingEnabled())
//					vsbViewportContext::HeadTrackerInterface().GetAngles(cockpit_cam_coord.hpr);
					sgdCopyVec3( cockpit_cam_coord.hpr, vsbBaseCamera::HeadOrientation() );
				
				sgCoord ccc;
				ccc.xyz[0] = cockpit_cam_coord.xyz[0];
				ccc.xyz[1] = cockpit_cam_coord.xyz[1];
				ccc.xyz[2] = cockpit_cam_coord.xyz[2];
				ccc.hpr[0] = cockpit_cam_coord.hpr[0];
				ccc.hpr[1] = cockpit_cam_coord.hpr[1];
				ccc.hpr[2] = cockpit_cam_coord.hpr[2];

				ssgSetCamera ( &ccc ) ;
				
				mpCockpit->Draw();
//				glPopMatrix();
				
			}
			
		}	
	}	  
	
	
	vsbFollowCamera *follow_cam = dynamic_cast<vsbFollowCamera *>(cam->ActiveCam());
	SetOpenGLState( follow_cam );
	
	const sgVec3 &hud_normal_color  = vsbViewportContext::EnvironmentOpts().HudNormalColor();
	const sgVec3 &hud_background_color  = vsbViewportContext::EnvironmentOpts().HudBackgroundColor();
	float background_transparency = vsbViewportContext::EnvironmentOpts().HudBackgroundTransparency();
	

	mHUDColour.Triple(hud_normal_color[0],hud_normal_color[1],  hud_normal_color[2]);

	mHUDBackground.Triple(hud_background_color[0], hud_background_color[1], hud_background_color[2],background_transparency);
	
	
	// Apply the HUD scale.
//	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f );
	glTranslatef( m_px, m_py, 0.0f );
	
	
	
	if (vsbViewportContext::EnvironmentOpts().HudBackgroundEnabled())
	{	
		mHUDBackground.Apply();
		//	  glRectf(0, 0, mLogicalW, mLogicalH);
		glRectf( -mLogicalW * 0.5f , -mLogicalH  * 0.5f , mLogicalW * 0.5f , mLogicalH * 0.5f  );
	}
	
	mHUDColour.Apply();
	
	DrawHUDComponents();
	
	// Undo HUD scale.
//	glPopMatrix();	
	
	ClearOpenGLState ();
	
	
}


void vsbHUDFramework::SetOpenGLState ( vsbFollowCamera *fc )
{
/*
glMatrixMode(GL_PROJECTION);
glPushMatrix();
glLoadIdentity();

  glViewport(mPixelCoords[0], mPixelCoords[1], 
  mPixelCoords[2]-mPixelCoords[0], mPixelCoords[3]-mPixelCoords[1]);
  
	
	  glOrtho(0, mLogicalW, 0, mLogicalH, 1.0, -1.0);
	  
	*/ 
	
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	
	glLoadIdentity();  
	
	
	// FIXME: This might need some going over...
	//	Floating and fixed HUDs.
	sgdVec3 HeadOrientation;
//	vsbViewportContext::HeadTrackerInterface().GetAngles(HeadOrientation);
	sgdCopyVec3( HeadOrientation, vsbBaseCamera::HeadOrientation() );

	sgdMat4 tmp;
	
	// sgMakeRotMat4  ( tmp, HeadOrientation[2], -HeadOrientation[1], -HeadOrientation[0] );
	sgdMakeRotMat4  ( tmp, fc->HeadOrientation()[2], -fc->HeadOrientation()[1], -fc->HeadOrientation()[0] );
	glMultMatrixd( (GLdouble *)tmp );
	
	
	// -1.5, make bigger to push HUD further away from camera
	//	glTranslatef(-0.5,-0.5,-1.5);
	glScalef(0.005f, 0.005f, 1.0f);
	glTranslatef( 0.0f, 0.0f, -1.0f);
	
	glDisable      ( GL_LIGHTING   ) ;
	glDisable      ( GL_TEXTURE_2D ) ;
	glDisable      ( GL_CULL_FACE  ) ;
	glDisable      ( GL_DEPTH_TEST );
	glDisable      ( GL_LIGHTING   );
	glDisable      ( GL_FOG        );
	glEnable       ( GL_BLEND      );
	glBlendFunc    (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glAlphaFunc(GL_GEQUAL, 0.0625);
	glEnable(GL_ALPHA_TEST);
	
	if( mAntialiaseLineDrawing/*anti-aliase*/) 
	{
		glEnable    ( GL_LINE_SMOOTH );
		glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
		glHint      (GL_LINE_SMOOTH_HINT, GL_DONT_CARE );
		glLineWidth (mAntialiasedThickness);
	} else 
	{
		glLineWidth(1.0);
	}
}

void vsbHUDFramework::ClearOpenGLState ()
{
//	glMatrixMode   ( GL_PROJECTION ) ;
//	glPopMatrix    () ;
	glMatrixMode   ( GL_MODELVIEW ) ;
	glDisable    ( GL_LINE_SMOOTH );
	glPopMatrix    () ;
	glLineWidth(1.0);
}


bool vsbHUDFramework::AddHUDComponent(vsbHUDComponent *hc)
{
    if (!hc) return false;
    map<string, vsbHUDComponent *>::iterator p; 
    pair<map<string, vsbHUDComponent *>::iterator, bool> res;
	
    hc->mpHUDManager = this;
    p = mHUDComponents.find(hc->Name());
	if (p != mHUDComponents.end()) 
		return true;
    res = mHUDComponents.insert(pair<string, vsbHUDComponent *>(hc->Name(), hc));
	return res.second;
}

void vsbHUDFramework::DeleteHUDComponent(vsbHUDComponent *hc)
{
    if (!hc) return;
    DeleteHUDComponent(hc->Name());
}

void vsbHUDFramework::DeleteHUDComponent(const string &name)
{
    map<string, vsbHUDComponent *>::iterator p; 
    p = mHUDComponents.find(name);
    if (p == mHUDComponents.end()) 
		return;
    vsbHUDComponent *comp = p->second;
    mHUDComponents.erase(p);
    delete comp;
}

void vsbHUDFramework::DeleteAllHUDComponents()
{
    map<string, vsbHUDComponent *>::iterator p; 
    p = mHUDComponents.begin();
    while (p != mHUDComponents.end())
    {
        delete p->second;
        p++;
    }
    mHUDComponents.clear();
	
	
}

void vsbHUDFramework::EnableHUDComponent(const string &name, bool onOff)
{
    map<string, vsbHUDComponent *>::iterator p; 
    p = mHUDComponents.find(name);
    if (p == mHUDComponents.end()) 
		return;
    p->second->IsEnabled(onOff);
}

void vsbHUDFramework::EnableHUDComponent(vsbHUDComponent *hc, bool onOff)
{
    if (!hc) return;
    EnableHUDComponent(hc->Name(), onOff);
}

vsbHUDComponent *vsbHUDFramework::HUDComponent(const string &name)
{
    map<string, vsbHUDComponent *>::iterator p; 
    p = mHUDComponents.find(name);
    if (p == mHUDComponents.end()) 
		return 0;
    return p->second;
}

void vsbHUDFramework::DrawHUDComponents()
{
    map<string, vsbHUDComponent *>::iterator p; 
    p = mHUDComponents.begin();
    while (p != mHUDComponents.end())
    {
        p->second->Draw();
        p++;
    }
}