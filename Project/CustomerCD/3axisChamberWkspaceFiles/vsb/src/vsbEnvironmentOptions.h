//////////////////////////////////////////////////////////////////
/*! 
    \file vsbEnvironmentOptions.h
    \brief Defines the class vsbEnvironmentOptions
 

	This file defines the vsbEnvironmentOptions class which is used by the 
	environment manager as a database of environmental parameters describing 
	the general attributes of the 3D scene to be displayed.

    Environment options include real world attributes such as cloud types
	and layers, time of day, weather, terrain type etc.

	\sa vsbEnvironmentOptions.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbEnvironmentOptions_H
#define _vsbEnvironmentOptions_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef parserH
#include "parser.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbRenderOptions_H
#include "vsbRenderOptions.h"
#endif 

#ifndef _vsbCloudParams_H
#include "vsbCloudParams.h"
#endif 

#ifndef _vsbGroundPlaneParams_H
#include "vsbGroundPlaneParams.h"
#endif


#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

	// Default HUD colour
#define DEFAULT_HUD_R 0.2f
#define DEFAULT_HUD_G 1.0f
#define DEFAULT_HUD_B 0.2f

// Default Background HUD colour
#define DEFAULT_HUD_BG_R 0.0f
#define DEFAULT_HUD_BG_G 0.0f
#define DEFAULT_HUD_BG_B 0.0f
#define DEFAULT_HUD_BG_A 0.3f

//////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbEnvironmentManager;

class DLL_VSB_API vsbEnvironmentOptionsParser;

////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbEnvironmentOptions
    \brief The class encapsulates all environmental parameters 

	The vsbEnvironmentOptions class is used by the environment manager as a
	database of environmental parameters describing the general attributes
	of the 3D scene to be displayed.

    Environment options include real world attributes such as cloud types
	and layers, time of day, weather, terrain type etc.
*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbEnvironmentOptions
{
friend class DLL_VSB_API vsbEnvironmentManager;
friend class DLL_VSB_API vsbEnvironmentOptionsParser;
public:

	enum TimeAdvanceRegimeEnum
	{
		ADVANCE_FIXED_STEP,
		ADVANCE_EXTERNAL,
		ADVANCE_REALTIME,
	};

protected:
/** @name Environment parameters 
	The list of environment parameters encapsulated by the vsbEnvironmentOptions class
*/
//@{
	bool				mSkyDomeOn;			//!< Draw dome/blended sky or flat filled sky
	bool				mSunOn;				//!< Draw Sun	
	bool				mCloudOn;	        //!< Draw clouds
	bool				mGroundPlaneOn;     //!< Ground Plane         
	bool				mWeatherOn;         //!< Draw weather visibility fog
	bool				mScalingOn;         //!< Enables/Disables scaling of coordinated
    bool				mScalingSymmetricOn; //!< Enables/Disables symmetric
    bool                mTrajectoryTraceOn; //!< Enables/Disables trajectory trace display for all entities with traj history   
	bool				mCloudPreDrawModeOn;  /*!< flag to indicate cloud predraw mode 
								
											  Indicates whether all clouds should be drawn 
											  before scene entities - useful if it is known
											  that all objects will be in the same layer of
											  clear space. With this mode selected explosions
											  and other transparent objects arent overdrawn by
											  clouds
								*/

	float				mHorizontalScale;
	float				mVerticalScale;
	float				mTrajTraceThickness;

	int					mUpdateRateHZ;
	TimeAdvanceRegimeEnum   mTimeAdvanceRegime;
	float				mLowerTimeBound;
	float				mUpperTimeBound;
	float				mTimeStep;


	double				mTimeOfDay;			//!< Positions sun relative to time of day (0.0 to 24.0 hours)	
	double				mTimeOfYear;		//!< Time of year in days (Southern Hemisphere)

	double				mAtmosphericVisibility; //!< Visibility range (due to weather)

	int					mNumCloudLayers;				//!< Number of cloud layers
	vsbCloudParams		mCloudLayers[MAX_CLOUD_LAYERS]; //!< Parameters for each cloud layer
	int					mCloudSortIdx[MAX_CLOUD_LAYERS]; //!< Cloud sorting index array

	int					mNumGroundPlanes;				//!< Number of ground planes
	vsbGroundPlaneParams mGroundPlanes[MAX_GROUND_PLANES]; //!< Parameters for each ground plane
	int					mGroundPlaneSortIdx[MAX_GROUND_PLANES]; //!< Ground plane sorting index array

	sgVec3				mHudHilightColor;           //!< HUD hilight colour
	sgVec3				mHudNormalColor;			   //!< HUD normal colour	
	sgVec3				mHudBackgroundColor;		   //!< HUD background colour	
	float				mHudBackgroundTransparency;	   //!< HUD background transparency
	bool				mHudBackgroundEnabled;		   //!< HUD background enabled flag
	bool                mEntityCentricCameraOn;          //! Determines primary rotation style of camera

	bool				m_htEnabled;	// bool to Update the head orientation for the camera matrix
										// from the	head tracker.

	bool				mChunkLODOn;  // enable chunk LOD rendering
	bool				mChunkLODWireframeOn;
	float				mChunkLODPixelError;
	std::string			mChunkLODName; // name of chunk LOD terrain to render

//@}


						/*! \brief Get the cloud layer sort index array
						*/
	const  int *		CloudSortIdx() const		{ return mCloudSortIdx; };

						/*! \brief Get the ground plane sort index array
						*/
	const  int *		GroundPlaneSortIdx() const	{ return mGroundPlaneSortIdx; };

public:						
				/*! \brief Sort the cloud layers for rendering

				This function is used by the environment manager during rendering
				of the scene to sort the cloud layers so that the will be drawn correctly.
				This is necessary as clouds are large planes with alpha transparency. If they
				are drawn in an incorrect order, some cloud layers may be obscured.
				*/
	void		SortCloudsByAltitude();

				/*! \brief Sort the ground planess for rendering

				This function is used by the environment manager during rendering
				of the scene to sort the ground planes. 
				\sa SortCloudsByAltitude for brief explanation of reason for sorting
				*/
	void		SortGroundPlanesByAltitude();

				/*! \brief Default constructor
				*/
				vsbEnvironmentOptions();

				/*! \brief Copy constructor 
				*/
				vsbEnvironmentOptions(const vsbEnvironmentOptions &eo);

				/*! \brief Assignment operator
				*/
				vsbEnvironmentOptions &operator = (const vsbEnvironmentOptions& eo);

				/*! \brief Constructor initialisation function
				*/
	void		SetDefault();

	int			Read(Parser::TLineInp *tli);
	void		Write(FILE *f, int indent);

/** @name General accessor functions 
Note: there are two forms of the enquire accessors
this is because for example, if texture is disabled and
clouds are enabled, seeing as they are textue mapped
the cannot be drawn so the form with the RenderOption
parameter takes the render options into consideration	
*/
//@{

				//----------------------------------------------------------

    bool		SkyDomeOn(vsbRenderOptions &ro) const	{ return mSkyDomeOn && ro.SolidPolygonsOn(); };
	bool		SkyDomeOn() const					{ return mSkyDomeOn; };
				/*! \brief Skydome visibility accessors

				Get and set enabled visibility status functions for the skydome based sky.
				*/
	void		SkyDomeOn(bool onOrOff)				{ mSkyDomeOn = onOrOff; };	

				//----------------------------------------------------------
	bool		SunOn(vsbRenderOptions &ro) const		{ return mSunOn  && ro.SolidPolygonsOn() 
		                                              && ro.TextureMappingOn(); };
	bool		SunOn() const						{ return mSunOn; };
				/*! \brief Sun visiblity accessors

				Get and set enabled status functions for the sun.
				*/
	void		SunOn(bool onOrOff) 				{ mSunOn = onOrOff; };
				//----------------------------------------------------------
	bool		CloudOn(vsbRenderOptions &ro) const	{ return mCloudOn && ro.TextureMappingOn() 
													&& ro.SolidPolygonsOn(); };
	bool		CloudOn() const						{ return mCloudOn; } ;

				/*! \brief Cloud visibility accessors

				Get and set enabled visibility status functions for the cloud layers.
				*/
	void  		CloudOn(bool onOrOff)				{ mCloudOn = onOrOff; };			

				//----------------------------------------------------------
	bool		GroundPlaneOn(vsbRenderOptions &ro) const	{ return mGroundPlaneOn && ro.TextureMappingOn() 
														&& ro.SolidPolygonsOn(); };
	bool		GroundPlaneOn() const				{ return mGroundPlaneOn; } ;
				/*! \brief Ground plane visibility accessors

				Get and set enabled visibility status functions for the textured ground planesy.
				*/
	void  		GroundPlaneOn(bool onOrOff)			{ mGroundPlaneOn = onOrOff; };


	bool		ChunkLODTerrainOn() const { return	mChunkLODOn;}
	void		ChunkLODTerrainOn(bool onOrOff) { mChunkLODOn = onOrOff;}

	void		ChunkLODWireframeOn(bool onOrOff) { mChunkLODWireframeOn = onOrOff;}
	bool		ChunkLODWireframeOn() const { return mChunkLODWireframeOn;}
	float		ChunkLODPixelError() const {return mChunkLODPixelError;};
	void		ChunkLODPixelError(float err) {mChunkLODPixelError = err;};


	std::string	&ChunkLODTerrainName()	{return	mChunkLODName;};

				//----------------------------------------------------------
	bool		WeatherOn(vsbRenderOptions &ro) const	{ return mWeatherOn && ro.FogOn(); };
	bool		WeatherOn() const 				    { return mWeatherOn; };

				/*! \brief Weather visibility accessors

				Get and set enabled visibility status functions for the weather visibility.
				*/
	void  		WeatherOn(bool onOrOff) 			{ mWeatherOn = onOrOff; };

	bool		ScalingOn(vsbRenderOptions &ro) const	{ return mScalingOn; };
	bool		ScalingOn() const 				    { return mScalingOn; };
	void  		ScalingOn(bool onOrOff) 			{ mScalingOn = onOrOff; };


    bool        TrajectoryTraceOn() const           { return mTrajectoryTraceOn; };
    void        TrajectoryTraceOn(bool onOrOff)     {  mTrajectoryTraceOn = onOrOff; };


    bool        ScalingSymmetricOn() const            { return mScalingSymmetricOn; }
    void        ScalingSymmetricOn(bool onOrOff)      { mScalingSymmetricOn = onOrOff; }

	bool		CloudPreDrawModeOn() const 				{ return mCloudPreDrawModeOn; };
	void  		CloudPreDrawModeOn(bool onOrOff) 		{ mCloudPreDrawModeOn = onOrOff; };

	bool        EntityCentricCameraOn()	const			{ return mEntityCentricCameraOn;}
	void        EntityCentricCameraOn(bool onOrOff) {  mEntityCentricCameraOn = onOrOff;}

	float		HorizontalScale() const				{ return mScalingOn?mHorizontalScale:1.0f;};
    float		VerticalScale()	const				{ return mScalingOn?(mScalingSymmetricOn?mHorizontalScale:mVerticalScale):1.0f; };
	float		TrajTraceThickness() const			{ return mTrajTraceThickness; }
	
	int			UpdateRateHZ() const			    { return mUpdateRateHZ;};
	void		HorizontalScale(float scale)		{ mHorizontalScale = scale;};
	void		VerticalScale(float scale)			{ mVerticalScale = scale; };
	void		TrajTraceThickness(float thickness)	{ mTrajTraceThickness = thickness; }

	void		UpdateRateHZ(int hz)			    { mUpdateRateHZ = hz; };

	TimeAdvanceRegimeEnum TimeAdvanceRegime() const {return mTimeAdvanceRegime; };
	void		TimeAdvanceRegime(TimeAdvanceRegimeEnum tar) {mTimeAdvanceRegime = tar;};

	float		LowerTimeBound() const		{ return mLowerTimeBound; };
	float		UpperTimeBound() const		{ return mUpperTimeBound; };
	float		TimeStep() const			{ return mTimeStep; };
	void		LowerTimeBound(float t) 	{  mLowerTimeBound = t; };
	void 		UpperTimeBound(float t) 	{ mUpperTimeBound = t; };
	void		TimeStep(float t) 			{ mTimeStep = t; };

				/*!< \brief Weather visibility accessors


				//----------------------------------------------------------
				
				/*! Get time of day
				*/
	double		TimeOfDay()	const					{ return mTimeOfDay; };
				/*! \brief Get/Set time of day
				\param tod Time of day given in hours since midnight
				*/
	void  		TimeOfDay(double tod); 

	double		TimeOfYear() const					{ return mTimeOfYear; };
				/*! \brief Get/Set time of year
				\param toy The day of the year [1..365]
				*/
	void  		TimeOfYear(double toy); 	
	
	double      AtmosphericVisibility() const		{ return mAtmosphericVisibility; };	
				/*! \brief Get/Set atmospheric visibility range

				\param av Visibility range in M.
				*/
	void		AtmosphericVisibility(double av)	{ mAtmosphericVisibility = av; };
//@}



//----------------------------------------- 
/** @name Cloud definition methods
Methods for creating/modifying the cloud layers in the scene
*/
//@{

				/*! \brief The number of clouds defined
				*/
    int			NumCloudLayers() const				{ return mNumCloudLayers; };

				/*! \brief Add a new cloud layer definition with default parameters

				The cloud layer has default parameters set which should be modified at 
				some stage before rendering for the first time.
				*/
    void		AddCloudLayer();

				/*! \brief Add a new cloud layer definition
				\param cloudType The cloud type enumeration
				\param alt The cloud layer altitude
				\param thickness The cloud layer thickness
				\param transition The cloud transition layer thickness
				*/
	void		AddCloudLayer(CloudTypeEnum cloudType, double alt, double thickness, double transition);

				/*! \brief Modify an existing cloud layer definition
				\param i The index of the cloud layer to modify
				\param cloudType The cloud type enumeration
				\param alt The cloud layer altitude
				\param thickness The cloud layer thickness
				\param transition The cloud transition layer thickness
				*/
	void		ModifyCloudLayer(int i, CloudTypeEnum cloudType, double alt, double thickness, double transition);

				//! Remove the indexed cloud layer definition
	void		RemoveCloudLayer(int i);

				//! Return the indexed cloud layer definiton parameters
	vsbCloudParams *CloudLayer(int i) ;

				//! Return the indexed cloud layer definiton parameters for cloud ordered by increasing altitude
	vsbCloudParams *CloudLayerByOrder(int i) ;


	bool HudBackgroundEnabled() const {return mHudBackgroundEnabled;};
	float HudBackgroundTransparency() const {return mHudBackgroundTransparency;};
	const sgVec3 &HudBackgroundColor() const { return mHudBackgroundColor;};
	const sgVec3 &HudHilightColor() const { return mHudHilightColor;};
	const sgVec3 &HudNormalColor() const { return mHudNormalColor;};

	void HudBackgroundEnabled(bool enabled)  { mHudBackgroundEnabled = enabled;};
	void HudBackgroundTransparency(float t)  {mHudBackgroundTransparency = t;};
	void HudBackgroundColor(const sgVec3 rgb)  {sgCopyVec3(mHudBackgroundColor,rgb);};
	void HudHilightColor(const sgVec3 rgb)  {sgCopyVec3(mHudHilightColor,rgb);};
	void HudNormalColor(const sgVec3 rgb)  {sgCopyVec3(mHudNormalColor,rgb);};



//@}


//----------------------------------------- 
/** @name Ground plane definition methods
Methods for creating/modifying the ground planes in the scene
*/
//@{

				//! Get the number of ground planes

	int			NumGroundPlanes() const		{ return mNumGroundPlanes; };

				//! Add a new ground plane definition with default parameters
    void		AddGroundPlane();

				
	int			AddGroundPlane(int groundTextureIdx, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);
	int			AddGroundPlane(const char *groundTextureLabel, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);
    int			AddGroundPlane(const std::string &groundTextureLabel, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);
				/*!< \brief Add a new ground plane definition	
				\param groundTextureLabel The logical name of the texture
				\param alt Altitude
				\param depthTest Apply Z buffer depth test
				*/

	void		ModifyGroundPlane(int i, int groundTextureIdx, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE );
	void		ModifyGroundPlane(int i, const char *groundTextureLabel, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE );
    void		ModifyGroundPlane(int i, std::string &groundTextureLabel, double alt, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE );
				/*!< Modify the indexed ground plane definiton
				\param groundTextureLabel The logical name of the texture
				\param alt Altitude
				\param depthTest Apply Z buffer depth test
				*/

				//! Remove the indexed ground plane definition
	void		RemoveGroundPlane(int i);

				//! Return the indexed ground plane definiton parameters
	vsbGroundPlaneParams *GroundPlane(int i) ;

				//! Return the indexed ground plane definiton parameters for cloud ordered by increasing altitude
	vsbGroundPlaneParams *GroundPlaneByOrder(int i);
//@}
	void HTenabled( bool flag ) { m_htEnabled = flag; }
	bool HTenabled() const { return m_htEnabled; }
};


class DLL_VSB_API vsbEnvironmentOptionsParser: public Parser::TParseObjectBase
{
public:
	vsbEnvironmentOptionsParser(vsbEnvironmentOptions &eo);
	virtual ~vsbEnvironmentOptionsParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
	vsbParserHelper *mpOutput;

	vsbEnvironmentOptions &mrEnvironmentOpts;
	vsbEnvironmentOptions *mpWorkingEnvironmentOpts;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
	void ParseOnOffState(bool &onOrOffVar, int id);
	void WriteOnOff(bool onOrOff);

};




#endif