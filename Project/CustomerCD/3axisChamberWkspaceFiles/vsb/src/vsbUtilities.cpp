#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifdef _MSC_VER
#include <io.h>
#include <direct.h>
#endif

#if (defined(__unix__) || defined(unix))
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include <assert.h>
#include <iostream>
#include <fstream>

#include "vsbUtilities.h"
#include "vsbSysPath.h"

using namespace std;

void FindSubDirectories(const string &start_dir, vector<string> &dirs, bool first)
{

	string model_dir = vsbGetSysPath();
	model_dir += "/";
	model_dir += start_dir;
	static len;

	#if defined (_MSC_VER) 

		struct _finddata_t c_file;
		long hFile;
		char old_dir[513];

		_getcwd(old_dir,512);

		if (_chdir(model_dir.c_str())) 
			return; // ultimately flag an error

		if (first)
			len = start_dir.length()+1;
		else
		{
			string entry = start_dir.c_str()+len;
			if (!first) dirs.push_back(entry);
		}
		model_dir+="/";

		/* Find first file in current directory */
		if((hFile = _findfirst("*.*", &c_file)) == -1L)
			return; // No subdirectories
		else
		{
			if((c_file.attrib & _A_SUBDIR ))
				if (strcmp(c_file.name,".") && strcmp(c_file.name,".."))
				{
					string newpath = start_dir;
					newpath+= "/";
					newpath += c_file.name;
					FindSubDirectories(newpath, dirs, false);
				}

			/* Find the rest of the *.* files */
			while(_findnext(hFile, &c_file) == 0)
				if (strcmp(c_file.name,".") && strcmp(c_file.name,".."))
				{
					string newpath = start_dir;
					
					newpath+= "/";
					newpath += c_file.name;
					FindSubDirectories(newpath, dirs, false);
				}
			
			_findclose(hFile);
		}
		_chdir(old_dir);

#elif (defined(__unix__) || defined(unix))  //UNIX
		DIR *dirp;
		struct dirent *dp;
		struct stat buf;

		dirp = opendir(model_dir.c_str());

		dirs.push_back(model_dir);
		model_dir+="/";

		while (dp = readdir(dirp)) 
		{
			if (stat(dp->d_name,&buf) == 0)
				if(buf.st_mode & S_IFDIR)
					if (!strcmp(dp->d_name,".") && !strcmp(dp->d_name,".."))
						FindSubDirectories(dp->d_name, dirs);
		}
		return closedir(dirp);
#endif
}
