#ifndef _vsbF18HUD_H
#define _vsbF18HUD_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbHUDFramework_H
#include "vsbHUDFramework.h"
#endif

#ifndef _vsbHUDComponents_H
#include "vsbHUDComponents.h"
#endif


class DLL_VSB_API vsbF18HUD: public vsbHUDFramework
{
public:

    //! Constructor
    vsbF18HUD();

    //! Virtual destructor
    virtual ~vsbF18HUD();

    void SetPositionEA( const sgdVec3 xyz );
    void SetPositionEA( const float x, const float y, const float z );
    void SetOrientationEA( const sgdVec3 hpr );
    void SetOrientationEA( const float heading, const float pitch, const float roll );
    void SetVelocityVecEA( const sgdVec3 uvw );
    void SetVelocityVecEA( const float x, const float y, const float z );

	// AltAirspeed
	void SetAirSpeed( const float airSpeed );

	// Attitude
	void SetAttitudeData( const float mach, const float alpha, const float G );

	// Lars
	void SetlarsMode( const vsbLarsHUD::eLarsMode mode ) { m_larsMode = mode; }
	void SetLarsData( const float rMin, const float rMax, const float rNoEscape, const float range );
	void SetSteeringDotData( const float SDx, const float SDy );

	// Set Angle to Target.
//	void SetAngleToTarget( const float angle ) { m_angleToTarget = angle; }
	void SetWithinRadar( const float withinRadar ) { m_bWithinRadar = withinRadar; }
	void SetWithinSeeker( const float withinSeeker ) { m_bWithinSeeker = withinSeeker; }

	// Aspect Vector.
	void SetAspectVector( const float aspect );

	// Weapon Name
	void SetSelectedWeapon( const char *name );

	// TargetLocator
	void SetPosData( const sgdVec3 &pos );
	void SetPosData( const double x, const double y, const double z );

	void SetTargetPosData( const sgdVec3 &targetPos );
	void SetTargetPosData( const double x, const double y, const double z );

	// HMS
	// Do these 2 functions really need to take parameters??
	void SetHasTarget( const bool bHasTarget ) { m_bHasTarget = bHasTarget;	}
	void SetHasHMS( const bool bHasHMS ) { m_bHaveHMS = bHasHMS; }

    virtual void DrawHud(vsbCameraManager *cam = 0);

protected:

    float mAltitude;			//!< Altitude
    float mAirspeed;			//!< Airspeed
    float mAlpha;				//!< Angle of attack
    float mMach;				//!< Mach number
    float mG;					//!< G's
    bool  mWatermarkOn;			//!< If watermark on pitch ladder displays A/C Attitude rel to earth
    bool  mLadderSteepnessOn;	//!< Displays anglled pitch ladder rungs at 0.5 times flight path angle
    float mPitchLadderX;		//!< If watermark off, pitch ladder rotates about this X point
    float mPitchLadderY;		//!< If watermark off, pitch ladder rotates about this Y point
    sgdVec3 mXYZ;                //!< Position vector
	sgdVec3 mVel;				//!< Velocity vector
    sgdVec3 mHPR;                //!< Orientation Euler angles

	// Lars vars.
	vsbLarsHUD::eLarsMode m_larsMode;
	float m_rMin;
	float m_rMax;
	float m_rNoEscape;
	float m_range;

	// Steering Dot vars.
	float m_SDx;
	float m_SDy;

	// Aspect Vector vars.
	float m_aspect;

	// new vars.
	bool m_bHaveHMS;
	bool m_bHasTarget;		// Set to 1.0f if has a taret, set to 0.0f otherwise.
	sgdVec3 m_Pos;			// Position of our Aircraft.
	sgdVec3 m_targetPos;		// If this wasn't a position set by the simulation, but
							//	rather an interpolated position by the gfx, then the
							//	target box wouldn't jump around all over the place.

//	float m_angleToTarget;
	float m_bWithinRadar;
	float m_bWithinSeeker;

private:
    bool mHaveVelocityVec;
    bool mHavePositionVec;
	bool mHaveAirSpeed;
	bool mHaveAttitude;
	bool mHaveSelectedWeapon;

	// Requires target to draw these.
	bool mHaveAspect;
	bool mHaveLars;
	bool mHavePosition;
	bool mHaveTargetPosition;

	vsbHeadingHUD		    *m_pHeadingHUD;
	vsbAltAirspeedHUD		*m_pAltAirspeedHUD;
	vsbAttitudeHUD			*m_pAttitudeHUD;
	vsbVelocityVectorHUD	*m_pVelocityVectorHUD;
	vsbPitchLadderHUD		*m_pPitchLadderHUD;
	vsbBankHUD				*m_pBankHUD;
	vsbLarsHUD				*m_pLarsHUD;
	vsbAspectVectorHUD		*m_pAspectVectorHUD;
	vsbSelectedWeaponHUD	*m_pSelectedWeaponHUD;
	vsbTargetLocatorHUD		*m_pTargetLocator;
	vsbHMSHUD				*m_pHMSHUD;
};

inline void vsbF18HUD::SetOrientationEA( const float heading, const float pitch, const float roll )
{
    mHPR[0] = heading;
    mHPR[1] = pitch;
    mHPR[2] = roll;
}

inline void vsbF18HUD::SetOrientationEA( const sgdVec3 hpr )
{
//    sgCopyVec3(mHPR,hpr); 
	mHPR[0] = hpr[0];
	mHPR[1] = hpr[1];
	mHPR[2] = hpr[2];
}

inline void vsbF18HUD::SetVelocityVecEA( const sgdVec3 xyz )
{
    mHaveVelocityVec = true;
//    sgCopyVec3(mVel,xyz);
	mVel[0] = xyz[0];
	mVel[1] = xyz[1];
	mVel[2] = xyz[2];
}

inline void vsbF18HUD::SetVelocityVecEA( const float x, const float y, const float z )
{
    mHaveVelocityVec = true;
    mVel[0] = x;
    mVel[1] = y;
    mVel[2] = z;
}

inline void vsbF18HUD::SetPositionEA( const sgdVec3 xyz )
{
    mHavePositionVec = true;
    sgdCopyVec3(mXYZ,xyz);
    mAltitude =  -mXYZ[2];
}

inline void vsbF18HUD::SetPositionEA( const float x, const float y, const float z )
{
    mHavePositionVec = true;
    
    mXYZ[0] = x;
    mXYZ[1] = y;
    mXYZ[2] = z;
    mAltitude =  -mXYZ[2];
}


#endif //_vsbF18HUD_H