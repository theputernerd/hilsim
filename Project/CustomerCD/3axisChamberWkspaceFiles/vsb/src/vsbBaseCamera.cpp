//////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseCamera.cpp
    \brief Implements the class vsbBaseCamera
    
     The class vsbBaseCamera is the pure abstract base class for all 
	 cameras, providing the basic funtionality to be managed by the 
	 camera manager.
	
	\sa vsbBaseCamera.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
#include <assert.h>

#include "ssg.h"
#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"
#include "vsbViewportContext.h"

#include "parser_perror.h"

using namespace std;
using namespace Parser;


const bool gIgnoreCase = true;

sgdVec3	vsbBaseCamera::mHeadOrientation = {0,0,0};
sgdVec3	vsbBaseCamera::mCameraToOriginDelta = {0,0,0};

const std::string & vsbBaseCamera::ClassName() const
{
    static string class_name = "vsbBaseCamera";
    return class_name;
}

vsbBaseCamera::vsbBaseCamera(vsbCameraManager *cm, const string &name, bool postfixed):
mpCameraManager(cm),mHandle(-1L),mName(name)
{

	int					unnamed_idx = 1;
	const string		prefix = name;
	string				auto_name;
	char				buffer[10];
	        

	if (!mpCameraManager) throw vsbCameraException(EC_NoCameraManager);

	if (postfixed)
	{
		do
		{
			auto_name = prefix;
			auto_name += itoa(unnamed_idx, buffer, 10);

			if (!cm->FindCam(auto_name))
				break;

			unnamed_idx++;
		} while (true);
	}
	else
	{	
		auto_name = name;
		if (cm->FindCam(auto_name)) throw vsbCameraException(EC_NameExists);
	}
	ConstructorSetup(auto_name);
	
}

vsbBaseCamera  &vsbBaseCamera::operator=(const vsbBaseCamera  &from)
{
//	mHandle			= from.mHandle;
//	mName			= from.mName;
	mFOV			= from.mFOV;
	mNearClip		= from.mNearClip;
	mFarClip		= from.mFarClip;
	mHudMode        = from.mHudMode;

	sgdCopyMat4(mRelativeGeometryMatrix,from.mRelativeGeometryMatrix);
    sgdCopyMat4(mComputedMatrix,from.mComputedMatrix);


	return *this;

}

void vsbBaseCamera::ConstructorSetup(const string &name)
{ 
	assert(mpCameraManager);
	mHandle = HashValue(name);
	mName = name;
	HeadTrackingEnabled(false);
	HudMode(HUD_FIXED);
    sgdMakeIdentMat4(mRelativeGeometryMatrix);
    sgdMakeIdentMat4(mComputedMatrix);
	pair<map<long, vsbBaseCamera *>::iterator, bool> res;
	res = mpCameraManager->mCameraInstances.insert(pair<long, vsbBaseCamera *>(mHandle, this));
	if (!res.second)  throw vsbCameraException(EC_ListInsertFailure);
}

vsbBaseCamera::~vsbBaseCamera()
{
	assert(mpCameraManager);
	if (mpCameraManager->FindCam(mHandle))
	{
		mpCameraManager->RemoveIteratorCam();
	}
}




long vsbBaseCamera::HashValue(const string &name)
{
	const char* s =  name.c_str();
	long		h = 0L;

	while (*s)
	{
	  h = (h<<2) ^ ((gIgnoreCase)?toupper(*s++):*s++);
	}
    h = h & 0x7FFFFFFFL;
	return (h%7999);
}


bool vsbBaseCamera::Rename(const std::string &newname)
{
	// if name unchanged return success
	if (Name() == newname) 
		return false;

	long newhandle = HashValue(newname);

	// check if the camera name already exists or maps to an existing camera id
	if (mpCameraManager->FindCam(newname))
		return false;

	// the next line should not fail
	if (!mpCameraManager->FindCam(mHandle))
		return false;

	mpCameraManager->RemoveIteratorCam();

	mHandle = newhandle;
	mName = newname;
	mpCameraManager->mCameraInstances.insert(pair<long, vsbBaseCamera *>(mHandle, this));
	return true;

}


void vsbBaseCamera::RelativeMatrix(const sgdMat4 mat)
{
	sgdCopyMat4(mRelativeGeometryMatrix, mat);
}

void vsbBaseCamera::ComputedMatrix(const sgdMat4 mat)
{
	sgdCopyMat4(mComputedMatrix, mat);
}

void vsbBaseCamera::RelativeMatrix(const sgdCoord &coord)
{
	sgdMat4 mat;
	sgdMakeCoordMat4(mat, &coord);
	sgdCopyMat4(mRelativeGeometryMatrix, mat);
}

void vsbBaseCamera::ComputedMatrix(const sgdCoord &coord)
{
	sgdMat4 mat;
	sgdMakeCoordMat4(mat, &coord);
	sgdCopyMat4(mComputedMatrix, mat);
}

void vsbBaseCamera::CameraMatrix(const sgdMat4 mat)
{
	sgdCopyMat4(mCamMatrix, mat);
}	

void vsbBaseCamera::CameraMatrix(const sgdCoord &coord)
{
	sgdMat4 mat;
	sgdMakeCoordMat4(mat, &coord);
	sgdCopyMat4(mCamMatrix, mat);
}	

//--------------------------------------------------------------------

char *vsbBaseCameraParser::mpKeyWords[] =
{
	"ON",
    "OFF",
	"NAME",
	"FOV",
	"NEAR_CLIP",
	"OFFSET_MATRIX",
    "COMPUTED_MATRIX",
	"X",
	"Y",
	"Z",
	"H",
	"P",
	"R",
	"HEAD_TRACKING",
	"HUD_STYLE",
	"HUD_COCKPIT",
	"HUD_FIXED",
	"HUD_GIMBALLED",
	0
};

enum {
    VSB_BCAM_ON,
    VSB_BCAM_OFF,
	VSB_BCAM_NAME,
	VSB_BCAM_FOV,
	VSB_BCAM_NEARCLIP,
	VSB_BCAM_OFFSET,
    VSB_BCAM_COMPUTED,
	VSB_BCAM_X,
	VSB_BCAM_Y,
	VSB_BCAM_Z,
	VSB_BCAM_H,
	VSB_BCAM_P,
	VSB_BCAM_R,
	VSB_BCAM_HEAD_TRACKING,
	VSB_HUD_STYLE,
	VSB_HUD_COCKPIT,
	VSB_HUD_FIXED,
	VSB_HUD_GIMBALLED,
};


char *vsbBaseCameraParser::mpErrorStrings[] =
{
	"",
	"Either \"ON\" or \"OFF\" keyword expected.",
	"A different camera with this name already exists",
	"Boolean keyword \"ON\" or \"OFF\" expected",
	"Hud style keyword expected (\"HUD_COCKPIT\", \"HUD_FIXED\", or \"HUD_GIMBALLED\")",
	0
};

enum {
	VSB_BCAM_ERR_UNUSED,
	VSB_BCAM_ERR_ONOFF_EXPECTED,
	VSB_BCAM_FOUND_ERROR,
	VSB_BCAM_ONOFF,
	VSB_BCAM_HUDSTYLE_EXPECTED,
};

vsbBaseCameraParser::vsbBaseCameraParser(vsbBaseCamera &bc):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrBaseCamera(bc) 
{
      mpOutput = new vsbParserHelper(*this);
}

vsbBaseCameraParser::~vsbBaseCameraParser()
{
      delete mpOutput;
}


void vsbBaseCameraParser::WriteOnOff(bool onOrOff)
{
	FILE *f = mpOutput->GetFile();
	if (f)
		fprintf(f,"(%s)",(onOrOff?"ON":"OFF"));
}


int	 vsbBaseCameraParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}


int vsbBaseCameraParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

	sgdMat4 matrix;
	sgdCoord coord;
	int hud_style_id[] = {VSB_HUD_FIXED, VSB_HUD_GIMBALLED, VSB_HUD_COCKPIT,-1};

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_NAME);
	mpOutput->WriteString(" = ");
	mpOutput->WriteName(mrBaseCamera.Name());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_FOV);
	mpOutput->WriteValue(mrBaseCamera.FOV());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_NEARCLIP);
	mpOutput->WriteValue(mrBaseCamera.NearClip());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_HEAD_TRACKING);
	mpOutput->WriteOnOff(mrBaseCamera.HeadTrackingEnabled());
	mpOutput->WriteString("\n");

	mpOutput->WriteKeyWord(VSB_HUD_STYLE);
	mpOutput->WriteString(" = ");
	mpOutput->WriteKeyWord(hud_style_id[mrBaseCamera.HudMode()]);
	mpOutput->WriteString("\n");

    // Computed matrix

	sgdCopyMat4(matrix,mrBaseCamera.ComputedMatrix());
	sgdSetCoord(&coord, matrix);
	vsbFromScaledVSBCoords(coord);

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_COMPUTED);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent+=3;

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_X);
	mpOutput->WriteValue(coord.xyz[0]);

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_Y);
	mpOutput->WriteValue(coord.xyz[1]);

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_Z);
	mpOutput->WriteValue(coord.xyz[2]);

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_H);
	mpOutput->WriteValue(coord.hpr[0]);
		
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_P);
	mpOutput->WriteValue(coord.hpr[1]);

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_BCAM_R);
	mpOutput->WriteValue(coord.hpr[2]);

	indent-=3;
	mpOutput->WriteNewline(indent); 
	mpOutput->WriteKeyWord(T_RBRACE);
		



	if (mrBaseCamera.RelativeGeom())
	{
		sgdCopyMat4(matrix,mrBaseCamera.RelativeMatrix());
		sgdSetCoord(&coord, matrix);
		vsbFromScaledVSBCoords(coord);

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_BCAM_OFFSET);
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
	    mpOutput->WriteKeyWord(VSB_BCAM_X);
		mpOutput->WriteValue(coord.xyz[0]);

		mpOutput->WriteNewline(indent);
	    mpOutput->WriteKeyWord(VSB_BCAM_Y);
		mpOutput->WriteValue(coord.xyz[1]);

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_BCAM_Z);
		mpOutput->WriteValue(coord.xyz[2]);

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_BCAM_H);
		mpOutput->WriteValue(coord.hpr[0]);
			
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_BCAM_P);
		mpOutput->WriteValue(coord.hpr[1]);

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_BCAM_R);
		mpOutput->WriteValue(coord.hpr[2]);

		indent-=3;
		mpOutput->WriteNewline(indent); 
		mpOutput->WriteKeyWord(T_RBRACE);
		
	}

	return 1;
}

void vsbBaseCameraParser::ParseFunc()
{
	char name_buff[100];
	float  fov, nearclip;
	bool relative_on;
	sgdCoord coord;
	sgdMat4 matrix;

	int hud_style_id[] = {VSB_HUD_FIXED, VSB_HUD_GIMBALLED,VSB_HUD_COCKPIT,-1};

	GetToken(VSB_BCAM_NAME); GetToken(T_EQ);
	GetString(name_buff);

	// check uniqueness of new parsed name.
	if (mrBaseCamera.mName != string(name_buff)) 
		if (mrBaseCamera.CameraManager()!=NULL)
		{
			if (mrBaseCamera.CameraManager()->FindCam(name_buff))
				AbortParse(VSB_BCAM_FOUND_ERROR);
		}

	GetToken(VSB_BCAM_FOV); GetToken(T_EQ);
	fov = GetNumericValue();

	GetToken(VSB_BCAM_NEARCLIP); GetToken(T_EQ);
	nearclip = GetNumericValue();

	PeekToken();
	if (GetCurrTokenID() == VSB_BCAM_HEAD_TRACKING)
	{
		GetToken(VSB_BCAM_HEAD_TRACKING); GetToken(T_EQ);
		GetToken();

		int bool_ids[]= {VSB_BCAM_OFF,VSB_BCAM_ON};
 		int tracking = KeywordTableSelection(bool_ids);
		if (tracking == -1)
		{
			AbortParse(VSB_BCAM_ONOFF);
		}
		mrBaseCamera.HeadTrackingEnabled(tracking?true:false);
	} else
	{
		mrBaseCamera.HeadTrackingEnabled(false);
	}
	

	PeekToken();
	int id;
	if (GetCurrTokenID() == VSB_HUD_STYLE)
	{
		GetToken(VSB_HUD_STYLE); 
		GetToken(T_EQ);
		GetToken();
		id = KeywordTableSelection(hud_style_id);
		if (id < 0)
			AbortParse(VSB_BCAM_HUDSTYLE_EXPECTED);
	} else
		id = HUD_FIXED;


	mrBaseCamera.HudMode((vsbHudMode) id);

    GetToken(VSB_BCAM_COMPUTED);
	GetToken(T_LBRACE);
	GetToken(VSB_BCAM_X); GetToken(T_EQ);
	coord.xyz[0] = GetNumericValue();
	GetToken(VSB_BCAM_Y); GetToken(T_EQ);
	coord.xyz[1] = GetNumericValue();
	GetToken(VSB_BCAM_Z); GetToken(T_EQ);
	coord.xyz[2] = GetNumericValue();
	GetToken(VSB_BCAM_H); GetToken(T_EQ);
	coord.hpr[0] = GetNumericValue();
	GetToken(VSB_BCAM_P); GetToken(T_EQ);
	coord.hpr[1] = GetNumericValue();
	GetToken(VSB_BCAM_R); GetToken(T_EQ);
	coord.hpr[2] = GetNumericValue();	
	GetToken(T_RBRACE);

	vsbToScaledVSBCoords(coord);
	sgdMakeCoordMat4(matrix, &coord);

    mrBaseCamera.ComputedMatrix(matrix);

	PeekToken();
	if (GetCurrTokenID()== VSB_BCAM_OFFSET)
	{
		GetToken(VSB_BCAM_OFFSET);
		GetToken(T_LBRACE);
		GetToken(VSB_BCAM_X); GetToken(T_EQ);
		coord.xyz[0] = GetNumericValue();
		GetToken(VSB_BCAM_Y); GetToken(T_EQ);
		coord.xyz[1] = GetNumericValue();
		GetToken(VSB_BCAM_Z); GetToken(T_EQ);
		coord.xyz[2] = GetNumericValue();
		GetToken(VSB_BCAM_H); GetToken(T_EQ);
		coord.hpr[0] = GetNumericValue();
		GetToken(VSB_BCAM_P); GetToken(T_EQ);
		coord.hpr[1] = GetNumericValue();
		GetToken(VSB_BCAM_R); GetToken(T_EQ);
		coord.hpr[2] = GetNumericValue();	
		GetToken(T_RBRACE);

		vsbToScaledVSBCoords(coord);
		sgdMakeCoordMat4(matrix, &coord);
		relative_on = true;
	} else
	{
		sgdMakeIdentMat4(matrix);
		relative_on = false;
	};


	mrBaseCamera.RelativeGeom(relative_on);
	mrBaseCamera.RelativeMatrix(matrix);
	mrBaseCamera.FOV(fov);
	//mrBaseCamera.mName = name_buff;
	//mrBaseCamera.mHandle = mrBaseCamera.HashValue(mrBaseCamera.mName);
	mrBaseCamera.NearClip(nearclip);

    mrBaseCamera.RenameCamera(name_buff);
}	

void vsbBaseCamera::RenameCamera(const string &name)
{
    assert(mpCameraManager);
    mpCameraManager->FindCam(mHandle);
    mpCameraManager->RemoveIteratorCam(); 
    mName = name;
	mHandle = HashValue(name);

   	pair<map<long, vsbBaseCamera *>::iterator, bool> res;
	res = mpCameraManager->mCameraInstances.insert(pair<long, vsbBaseCamera *>(mHandle, this));
  
    assert(res.second);
    // set hash
    // reset position in cam list
}


/*
void vsbBaseCamera::UpdateCameraMatrix(float time)
{
}
*/