//////////////////////////////////////////////////////////////////
/*! 
    \file vsbGroundPlaneParams.cpp
    \brief Defines the class vsbGroundPlaneParams
 
	This file implements the vsbGroundPlaneParams class which encapsulates 
    all the information necessary for the construction and manipulation 
    of flat textured ground planes (terrains), however it does not define 
    any ground plane implementation methods.
 	
	\sa vsbGroundPlaneParams.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>



#include "sg.h"
#include "ssg.h"
#include "vsbPlaneTextureList.h"
#include "vsbGroundPlaneParams.h"
#include "vsbSysPath.h"

using namespace std;
using namespace Parser;

// The name of the index file containing the list of terrain textures
const char *TextureIndexFile = "terrain_textures.idx";





char *vsbGroundPlaneParamsParser::mpKeyWords[] =
{
	"GROUND_PLANE",
	"ALTITUDE",
	"LENGTH",
	"SCALE",
	"TEXTURE_INDEX",
	"DEPTH_TEST",
	"TEST_ONLY",
    "TEST_AND_WRITE",
    "NO_TEST",
	0
};

enum {
	VSB_GP_GROUNDPLANE,
	VSB_GP_ALTITUDE,
	VSB_GP_LENGTH,
	VSB_GP_SCALE,
	VSB_GP_TEXTURE_INDEX,
	VSB_GP_DEPTH_TEST,
	VSB_GP_DEPTH_T,
	VSB_GP_DEPTH_TW,
	VSB_GP_DEPTH_N,
};	

char *vsbGroundPlaneParamsParser::mpErrorStrings[] =
{
	"",
    "Incorrect depth test enumeration",
	0
};

enum {
	VSB_GP_DEPTH_ENUM_ERROR = 1
};

vsbGroundPlaneParamsParser::vsbGroundPlaneParamsParser(vsbGroundPlaneParams &gp):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrGroundPlaneParams(gp)
{
	   mpOutput = new vsbParserHelper(*this);
}

vsbGroundPlaneParamsParser::~vsbGroundPlaneParamsParser()
{  
	delete mpOutput;
}

int	 vsbGroundPlaneParamsParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int  vsbGroundPlaneParamsParser::Write(FILE *f, int indent)
{
	int depth_opts[] = {VSB_GP_DEPTH_T, VSB_GP_DEPTH_TW, VSB_GP_DEPTH_N};
	mpOutput->SetFile(f);
	if (f==0) return 0;

	vsbGroundPlaneParams &working_groundplane_params = mrGroundPlaneParams;

	mpOutput->WriteSpaces(indent);
	mpOutput->WriteKeyWord(VSB_GP_GROUNDPLANE);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent+=3;

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_GP_TEXTURE_INDEX);
	mpOutput->WriteValue((int) working_groundplane_params.GroundTextureIdx());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_GP_ALTITUDE);
	mpOutput->WriteValue( working_groundplane_params.Alt());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_GP_SCALE);
	mpOutput->WriteValue(working_groundplane_params.Scale());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_GP_LENGTH);
	mpOutput->WriteValue( working_groundplane_params.Length());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_GP_DEPTH_TEST);
	mpOutput->WriteString(" = ");
	mpOutput->WriteKeyWord(depth_opts[working_groundplane_params.DepthTest()]);

	indent-=3;
	mpOutput->WriteNewline(indent); 
	mpOutput->WriteKeyWord(T_RBRACE);

	return 1;
}

void vsbGroundPlaneParamsParser::ParseFunc()
{
	int i;
	int depth_opts[] = {VSB_GP_DEPTH_T, VSB_GP_DEPTH_TW, VSB_GP_DEPTH_N, -1};

	vsbGroundPlaneParams working_ground_params;
	GetToken(VSB_GP_GROUNDPLANE);
	GetToken(T_LBRACE);

	GetToken(VSB_GP_TEXTURE_INDEX);
	GetToken(T_EQ);
	working_ground_params.GroundTextureIdx(GetIntValue());

	GetToken(VSB_GP_ALTITUDE);
	GetToken(T_EQ);
	working_ground_params.Alt(GetNumericValue());

	GetToken(VSB_GP_SCALE);
	GetToken(T_EQ);
	working_ground_params.Scale(GetNumericValue());

	GetToken(VSB_GP_LENGTH);
	GetToken(T_EQ);
	working_ground_params.Length(GetNumericValue());

	GetToken(VSB_GP_DEPTH_TEST);
	GetToken(T_EQ);
	GetToken();

	i = KeywordTableSelection(depth_opts);
    if (i<0) 
		AbortParse(VSB_GP_DEPTH_ENUM_ERROR);
			working_ground_params.DepthTest(vsbDepthTestStyle(i));

	GetToken(T_RBRACE);

	mrGroundPlaneParams = working_ground_params;
}









/////////////////////////////////////////////////////////////////////////////

int vsbGroundPlaneParams::mInstanceCount = 0;

vsbPlaneTextureList *vsbGroundPlaneParams::mPlaneTextureList = 0;

ssgSimpleState **vsbGroundPlaneParams::msGroundPlaneState;

vsbPlaneTextureList &	vsbGroundPlaneParams::TextureList() 
{
	static vsbPlaneTextureList empty;
	if (!mPlaneTextureList)
		return empty;
	return  *mPlaneTextureList; 
};

float vsbGroundPlaneParams::DefaultScale()
{
    return TextureList()[mGroundTextureIdx].Scale();
}


vsbGroundPlaneParams::vsbGroundPlaneParams()
{
	
	mInstanceCount++;
	if (mInstanceCount==1)
	{
		string texture_index_file(::vsbGetSysPath());
		texture_index_file += "/terrain/";
		texture_index_file += TextureIndexFile;
		mPlaneTextureList = new vsbPlaneTextureList(texture_index_file);
		msGroundPlaneState = new ssgSimpleState *[mPlaneTextureList->Count()];
		for (int i=0; i< mPlaneTextureList->Count(); i++) msGroundPlaneState[i] =0;

	}
	mGroundTextureIdx = -1;
	SetDefault();
};


vsbGroundPlaneParams::~vsbGroundPlaneParams()
{
	mInstanceCount--;
	if (mInstanceCount==0)
	{
		for (int i=0; i< mPlaneTextureList->Count(); i++) 
			if (msGroundPlaneState[i] !=0)
			{
				ssgDeRefDelete( msGroundPlaneState[i] );
			}
		delete [] msGroundPlaneState;
		mPlaneTextureList->Clear();
		delete mPlaneTextureList;
		mPlaneTextureList = 0;
	}
}



void vsbGroundPlaneParams::SetDefault()
{
	Alt(0.0f);
	Length(80000);
	DepthTest(vsbTEST_AND_WRITE);
	GroundTextureIdx(0);
	Scale(DefaultScale());
};

vsbGroundPlaneParams::vsbGroundPlaneParams(vsbGroundPlaneParams &cp)
{
	*this = cp;
};

vsbGroundPlaneParams &vsbGroundPlaneParams::operator = (const vsbGroundPlaneParams& cp)
{
	Alt(cp.Alt());
	Length(cp.Length());
	Scale(cp.Scale());
	DepthTest(cp.DepthTest());
	GroundTextureIdx(cp.GroundTextureIdx());
	return *this;
}

bool  vsbGroundPlaneParams::operator == (const vsbGroundPlaneParams& cp) const
{
	return	mAlt == cp.Alt() &&
			mLength == cp.Length() &&
			mScale == cp.Scale() &&
			mDepthTest == cp.DepthTest() &&
			mGroundTextureIdx == cp.GroundTextureIdx();
}

void vsbGroundPlaneParams::Set(int textureIdx, double alt, vsbDepthTestStyle depthTest)
{
	if (textureIdx<0 || textureIdx>=MaxPlaneTextures() ) return;
	DepthTest(depthTest);
	Alt((float) alt);
	GroundTextureIdx(textureIdx);
	Scale(DefaultScale());
}

void vsbGroundPlaneParams::Set(const char * textureLabel, double alt, vsbDepthTestStyle depthTest)
{
	if (!mPlaneTextureList) return;
	string lbl_str(textureLabel);
	int texture_idx = mPlaneTextureList->FindIndexByLabel(lbl_str);
	if (texture_idx < 0) return;
	Set(texture_idx, alt, depthTest);
}

void vsbGroundPlaneParams::Set(const string &textureLabel, double alt, vsbDepthTestStyle depthTest)
{
	Set(textureLabel.c_str(),  alt, depthTest);
}


int vsbGroundPlaneParams::MaxPlaneTextures()
{
	if (mPlaneTextureList == 0) return 0;
	return mPlaneTextureList->Count();
}
