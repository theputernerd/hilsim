#ifndef _vsbHUDFramework_H
#define _vsbHUDFramework_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbCockpit_H
#include "vsbCockpit.h"
#endif

#ifndef _vsbHUDTools_H 
#include "vsbHUDTools.h"
#endif 

#ifndef _vsbStrokedText_H
#include "vsbStrokedText.h"
#endif

#ifndef _vsbEnvironmentOptions_H
#include "vsbEnvironmentOptions.h"
#endif

#ifndef _vsbCameraManager_H
#include "vsbCameraManager.h"
#endif

//vsbCockpit vbc;
class vsbFollowCamera;
class DLL_VSB_API vsbHUDFramework;

class DLL_VSB_API vsbHUDComponent
{
    friend class DLL_VSB_API vsbHUDFramework;
public:
                        vsbHUDComponent();
    virtual const char * Name() const = 0;    
    bool                IsEnabled() const {return mIsEnabled;};
    void                IsEnabled(bool onOff) {mIsEnabled = onOff;};    

	void SetPosition( float x, float y ) { m_px = x; m_py = y; }
	void SetScale( float x, float y ) { m_sx = x; m_sy = y; }

	void GetPosition( float &x, float &y ) { x = m_px; y = m_py; }
	void GetScale( float &x, float &y ) { x = m_sx; y = m_sy; }
	
protected:
 
    bool					mIsEnabled;
    vsbHUDFramework       *mpHUDManager;
    static vsbStrokedText  mStrokedText;

    virtual void        Draw()=0;

	// These should probably be vectors.
	float m_px, m_py;	// position.
	float m_sx, m_sy;	// scale.

};



class DLL_VSB_API vsbHUDFramework
{


public:
	bool                AddHUDComponent(vsbHUDComponent *hc);
    void                DeleteHUDComponent(vsbHUDComponent *hc);
    void                DeleteHUDComponent(const std::string &name);
    void                DeleteAllHUDComponents();

    void                EnableHUDComponent(const std::string &name, bool onOff);
    void                EnableHUDComponent(vsbHUDComponent *hc, bool onOff);
    vsbHUDComponent    *HUDComponent(const std::string &name);
    void                DrawHUDComponents();

	void				SetPosition( float x, float y ) { m_px = x; m_py = y; }
	void				SetScale( float x, float y ) { m_sx = x; m_sy = y; }


	/*! \brief Constructor
	*/
	vsbHUDFramework();


    /*! \brief Virtual Destructor
	*/
	~vsbHUDFramework();


	/*! \brief Set Logical size of the HUD viewport
	\param w Width in logical units
	\param h Height in logical units

	All hud components draw in logical coordinates.
	\note This function must preceed a Reshape call for correct operation
	
	*/
	void SetLogicalDimensions(float w, float h) 
	{ mLogicalW = w; mLogicalH = h; mLogicalAspect = w/h;};

	/*! \brief Set the minimum margin in pixels from the window edge to 
		the HUD viewport
	*/
	void PixelMargin(int m) {mPixMargin = m;};

	/*! \brief Get the minimum pixel margin
	*/
	int PixelMargin() {return mPixMargin;};

	static void SetAntialiasedLinesThickness(float t) {mAntialiasedThickness = t;};
	static bool AntialiaseLineDrawing() {return mAntialiaseLineDrawing;};
	static void AntialiaseLineDrawing(bool onOff) {mAntialiaseLineDrawing = onOff;};

	vsbRGB &HUDColour(){return mHUDColour;};

	void Reshape(int w, int h);


	void Cockpit(vsbCockpit *cockpit) {mpCockpit = cockpit;};


	virtual void DrawHud(vsbCameraManager *cam = 0);

protected:
	vsbCockpit *mpCockpit;

private:
	static vsbRGB mHUDColour; //!< The default HUD colour
	static vsbRGB mHUDBackground; //!< The default HUD colour
	static bool   mAntialiaseLineDrawing; //!< Flag indicating HUD line drawing is to be antialiased
	static float   mAntialiasedThickness; //!< Antialiased line thickness

	static int mPixMargin; //!< The minimum margin between the hud viewport and the window edge
	int   mPixelCoords[4]; //!< The pixel LL,UR coordinates of the hud display viewport
	float mLogicalW;	   //!< The logical width of the hud display viewport
	float mLogicalH;       //!< The logical height of the hud display viewport
	float mLogicalAspect;  //!< The logical aspect ratio of the hud

	float m_px, m_py;		// Position of HUD.
	float m_sx, m_sy;		// Scale of HUD.



	void SetOpenGLState ( vsbFollowCamera *fc );
    void ClearOpenGLState ();
   
// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958     
#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

    std::map<std::string, vsbHUDComponent *> mHUDComponents;

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

};

#endif
