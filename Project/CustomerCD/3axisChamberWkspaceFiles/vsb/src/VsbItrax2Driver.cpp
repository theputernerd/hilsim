// vsbItrax2Driver.cpp: implementation of the vsbItrax2Driver class.
//
//////////////////////////////////////////////////////////////////////


#include "VsbItrax2Driver.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace std;


/* tracking system type */
#define ISD_NONE               0    /* none found, or can't identify */
#define ISD_PRECISION_SERIES   1    /* IS-300, IS-600 and IS-900 */
#define ISD_INTERTRAX_SERIES   2    /* InterTrax */

/* tracking system model */
#define ISD_UNKNOWN     0    
#define ISD_IS300       1    /* 3DOF system */
#define ISD_IS600       2    /* 6DOF system */
#define ISD_IS900       3    /* 6DOF system */
#define ISD_INTERTRAX   4    /* InterTrax (Serial) */
#define ISD_INTERTRAX_2 5    /* InterTrax (USB) */

/* for now limited to 8 */
#define ISD_MAX_STATIONS       8

/* assume no more than 4 rs232 ports */
#define ISD_MAX_TRACKERS       4 

/* number of supported joystick or stylus buttons */
#define ISD_MAX_BUTTONS        8

/* hardware is limited to 10 analog channels per station */
#define ISD_MAX_CHANNELS       10

/* orientation format */
#define ISD_EULER              1
#define ISD_QUATERNION         2


typedef int ISD_TRACKER_HANDLE;

typedef struct
{
    /* Following item are for information only and should not be changed */

    float  LibVersion;     /* InterSense Library version */

    DWORD  TrackerType;    /* IS Precision series or InterTrax. 
                              TrackerType can be: 
                              ISD_PRECISION_SERIES for IS-300, IS-600 and IS-900 model tracker, 
                              ISD_INTERTRAX_SERIES for InterTrax, or 
                              ISD_NONE if tracker is not initialized */

    DWORD  TrackerModel;   /* ISD_UNKNOWN, ISD_IS300, ISD_IS600, ISD_IS900, ISD_INTERTRAX */
    
    DWORD  CommPort;       /* Number of the rs232 port. Starts with 1. */

    /* Communications statistics. For information only. */

    DWORD  RecordsPerSec;
    float  KBitsPerSec;    

    /* Following items are used to configure the tracker and can be set in
       the isenseX.ini file */

    DWORD  SyncState;   /* 4 states: 0 - OFF, system is in free run 
                                     1 - ON, hardware genlock frequency is automatically determined
                                     2 - ON, hardware genlock frequency is specified by the user
                                     3 - ON, no hardware sygnal, lock to the user specified frequency */ 

    float  SyncRate;    /* Sync frequency - number of hardware sync signals per second, 
                           or, if SyncState is 3 - data record output frequency */

    DWORD  SyncPhase;   /* 0 to 100 per cent */   

    DWORD  wReserved1;  /* reserved for future use */
    DWORD  wReserved2;
    DWORD  wReserved3;  
    DWORD  wReserved4;

    float  fReserved1;  
    float  fReserved2;
    float  fReserved3;
    float  fReserved4;

    BOOL   bReserved1;
    BOOL   bReserved2;
    BOOL   bReserved3;
    BOOL   bReserved4;

} ISD_TRACKER_INFO_TYPE;



/* ISD_STATION_INFO_TYPE can only be used with IS Precision Series tracking devices.
   If passed to ISD_SetStationState or ISD_GetStationState with InterTrax, FALSE is returned. */

typedef struct
{
    DWORD   ID;             /* unique number identifying a station. It is the same as that 
                               passed to the ISD_SetStationState and ISD_GetStationState   
                               functions and can be 1 to ISD_MAX_STATIONS */

    BOOL    State;          /* TRUE if ON, FALSE if OFF */

    BOOL    Compass;        /* 0, 1 or 2 for OFF, PARTIAL and FULL. Older versions of tracker
                               firmware supported only 0 and 1, which stood for ON or OFF. Please
                               use the new notation. This API will correctly interpret the settings.
                               Compass setting is ignored if station is configured for 
                               Fusion Mode operation. */

    LONG    InertiaCube;    /* InertiaCube associated with this station. If no InertiaCube is
                               assigned, this number is -1. Otherwise, it is a positive number
                               1 to 4 */

    DWORD   Enhancement;    /* levels 0, 1, or 2 */
    DWORD   Sensitivity;    /* levels 1 to 5 */
    DWORD   Prediction;     /* 0 to 50 ms */
    DWORD   AngleFormat;    /* ISD_EULER or ISD_QUATERNION */
    BOOL    TimeStamped;    /* TRUE if time stamp is requested */
    BOOL    GetButtons;     /* TRUE if wand or stylus button state is requested */
    BOOL    GetCameraData;
    BOOL    GetAnalogData;  /* TRUE if analog joystick data is requested */

    DWORD   wReserved1;     /* reserved for future use */
    DWORD   wReserved2;
    DWORD   wReserved3;     
    DWORD   wReserved4;

    float   fReserved1;  
    float   fReserved2;
    float   fReserved3;
    float   fReserved4;

    BOOL    bReserved1;
    BOOL    bReserved2;     
    BOOL    bReserved3;
    BOOL    bReserved4;

} ISD_STATION_INFO_TYPE;


typedef struct
{
    float Orientation[4];   /* Supports both Euler and Quaternion formats */
    float Position[3];      /* Always in meters */
    float TimeStamp;        /* Seconds, reported only if requested */
    BOOL  ButtonState[ISD_MAX_BUTTONS];    /* Only if requested */

    /* Current hardware is limited to 10 channels, only 2 are used. 
       The only device using this is the IS-900 wand that has a built-in
       analog joystick. Channel 1 is x-axis rotation, channel 2 is y-axis
       rotation */

    short  AnalogData[ISD_MAX_CHANNELS]; 

} ISD_STATION_DATA_TYPE;


typedef struct
{
    ISD_STATION_DATA_TYPE Station[ISD_MAX_STATIONS];

} ISD_DATA_TYPE;


//////////////////////////////////////////////////////////////////////////////////////

static FARPROC getProcAddress( HMODULE hModule, LPCSTR lpProcName );

typedef ISD_TRACKER_HANDLE (*ISD_OPEN_FN)           ( HWND, DWORD, BOOL, BOOL );
typedef BOOL               (*ISD_COMMAND_FN)        ( ISD_TRACKER_HANDLE );
typedef BOOL               (*ISD_COMM_INFO_FN)      ( ISD_TRACKER_HANDLE, ISD_TRACKER_INFO_TYPE * );
typedef BOOL               (*ISD_SYSTEM_CONFIG_FN)  ( ISD_TRACKER_HANDLE, ISD_TRACKER_INFO_TYPE *, BOOL );
typedef BOOL               (*ISD_STATION_CONFIG_FN) ( ISD_TRACKER_HANDLE, ISD_STATION_INFO_TYPE *, WORD, BOOL );
typedef BOOL               (*ISD_DATA_FN)           ( ISD_TRACKER_HANDLE, ISD_DATA_TYPE * );
typedef BOOL               (*ISD_SCRIPT_FN)         ( ISD_TRACKER_HANDLE, char * );
typedef BOOL               (*ISD_COUNT_FN)          ( WORD * );
typedef BOOL               (*ISD_RESET_ANGLES_FN)   ( ISD_TRACKER_HANDLE, float, float, float );
typedef HWND               (*ISD_WINDOW_FN)         ( ISD_TRACKER_HANDLE );

/* dll function pointers */
ISD_OPEN_FN           _ISD_OpenTracker       = NULL;
ISD_COMMAND_FN        _ISD_CloseTracker      = NULL;
ISD_COMM_INFO_FN      _ISD_GetCommInfo       = NULL;
ISD_SYSTEM_CONFIG_FN  _ISD_GetTrackerConfig  = NULL;
ISD_SYSTEM_CONFIG_FN  _ISD_SetTrackerConfig  = NULL;
ISD_STATION_CONFIG_FN _ISD_GetStationConfig  = NULL;
ISD_STATION_CONFIG_FN _ISD_SetStationConfig  = NULL;
ISD_DATA_FN           _ISD_GetTrackerData    = NULL;      
ISD_SCRIPT_FN         _ISD_SendScript        = NULL;
ISD_COUNT_FN          _ISD_NumOpenTrackers   = NULL;
ISD_WINDOW_FN         _ISD_OpenConfigWindow  = NULL;
ISD_RESET_ANGLES_FN   _ISD_ResetAngles       = NULL; 
 
/* dll handle */  
HINSTANCE ISD_lib = NULL;

string last_message;


/*****************************************************************************
*
*   functionName:   printErrorMessage
*   Description:    
*   Created:        7/25/99
*   Author:         Yury Altshuler
*
*   Comments:       
*
******************************************************************************/
static void printErrorMessage( DWORD error, LPCSTR lpProcName )
{
    switch(error)
    {
        case ERROR_DLL_INIT_FAILED:
            last_message = "A DLL initialization routine failed.\n";
            break;
        case ERROR_INVALID_FUNCTION:
            last_message = "The function is incorrect.\n";
            break;
        case ERROR_FILE_NOT_FOUND:
            last_message = "The system cannot find the file specified.\n";
            break;
        case ERROR_PATH_NOT_FOUND:
            last_message = "The system cannot find the specified path.\n";
            break;
        case ERROR_TOO_MANY_OPEN_FILES:
            last_message = "The system cannot open the file.\n";
            break;
        case ERROR_ACCESS_DENIED:
            last_message = "Access is denied.\n";
            break;
        default:
			last_message = lpProcName;
			last_message += "failed failed. Error code ";
			char err_str[20];
			itoa(error, err_str, 10);
			last_message += err_str;
			last_message += "\n";

            break;
    }
}

/*****************************************************************************
*
*   functionName:   getProcAddress
*   Description:    loads specified dll routine
*   Created:        7/25/99
*   Author:         Yury Altshuler
*
*   Comments:       
*
******************************************************************************/
static FARPROC getProcAddress( HMODULE hModule, LPCSTR lpProcName )
{
    FARPROC proc;

    proc = GetProcAddress( hModule, lpProcName );
    if(proc == NULL)
    {
		last_message = "Failed to load ";
		last_message += lpProcName;

 //       last_message.Format("Failed to load %s. Error code %d\n", lpProcName, GetLastError());
    }
    return proc;
}


/*****************************************************************************
*
*   functionName:   load_ISLIB
*   Description:    loads isense.dll
*   Created:        12/7/98
*   Author:         Yury Altshuler
*
*   Comments:       
*
******************************************************************************/
static HINSTANCE load_ISLIB( void )
{
    HINSTANCE hLib;

    if(hLib = LoadLibrary ( "isense.dll" ))
    {
        _ISD_OpenTracker      = ( ISD_OPEN_FN )           getProcAddress( hLib, "ISD_OpenTracker" );
        _ISD_CloseTracker     = ( ISD_COMMAND_FN )        getProcAddress( hLib, "ISD_CloseTracker" );
        _ISD_GetCommInfo      = ( ISD_COMM_INFO_FN )      getProcAddress( hLib, "ISD_GetCommInfo" );
        _ISD_GetTrackerConfig = ( ISD_SYSTEM_CONFIG_FN )  getProcAddress( hLib, "ISD_GetTrackerConfig" );
        _ISD_SetTrackerConfig = ( ISD_SYSTEM_CONFIG_FN )  getProcAddress( hLib, "ISD_SetTrackerConfig" );
        _ISD_GetStationConfig = ( ISD_STATION_CONFIG_FN ) getProcAddress( hLib, "ISD_GetStationConfig" );
        _ISD_SetStationConfig = ( ISD_STATION_CONFIG_FN ) getProcAddress( hLib, "ISD_SetStationConfig" );
        _ISD_GetTrackerData   = ( ISD_DATA_FN )           getProcAddress( hLib, "ISD_GetTrackerData" );
        _ISD_SendScript       = ( ISD_SCRIPT_FN )         getProcAddress( hLib, "ISD_SendScript" );
        _ISD_NumOpenTrackers  = ( ISD_COUNT_FN )          getProcAddress( hLib, "ISD_NumOpenTrackers" );
        _ISD_OpenConfigWindow = ( ISD_WINDOW_FN )         getProcAddress( hLib, "ISD_OpenConfigWindow" );
        _ISD_ResetAngles      = ( ISD_RESET_ANGLES_FN )   getProcAddress( hLib, "ISD_ResetAngles" );
    }

    if(hLib == NULL)
    {
        printErrorMessage(GetLastError(), "LoadLibrary");
    }

    return hLib;
}


/*****************************************************************************
*
*   functionName:   free_ISLIB
*   Description:    frees isense.dll
*   Created:        12/8/98
*   Author:         Yury Altshuler
*
*   Comments:       
*
******************************************************************************/
static void free_ISLIB( HINSTANCE hLib )
{
    _ISD_OpenTracker      = NULL;
    _ISD_CloseTracker     = NULL;
    _ISD_GetCommInfo      = NULL;
    _ISD_GetTrackerConfig = NULL;
    _ISD_SetTrackerConfig = NULL;
    _ISD_GetStationConfig = NULL;
    _ISD_SetStationConfig = NULL;
    _ISD_GetTrackerData   = NULL;      
    _ISD_SendScript       = NULL;
    _ISD_NumOpenTrackers  = NULL;
    _ISD_OpenConfigWindow = NULL;
    _ISD_ResetAngles      = NULL;

	FreeLibrary ( hLib );  /* free the dll */
}



/* Returns -1 on failure. To detect tracker automatically specify 0 for commPort.
   hParent parameter to ISD_OpenTracker is optional and should only be used if 
   information screen or tracker configuration tools are to be used when available 
   in the future releases. If you would like a tracker initialization window to be 
   displayed, specify TRUE value for the infoScreen parameter (not implemented in
   this release). */

ISD_TRACKER_HANDLE ISD_OpenTracker( HWND hParent, DWORD commPort, 
                                      BOOL infoScreen, BOOL verbose );


/* This function call deinitializes the tracker, closes communications port and 
   frees the resources associated with this tracker. If 0 is passed, all currently
   open trackers are closed. When last tracker is closed, program frees the DLL. */

BOOL  ISD_CloseTracker( ISD_TRACKER_HANDLE );


/* Get general tracker information, such as type, model, port, etc.
   Also retrieves genlock synchronization configuration, if available. 
   See ISD_TRACKER_INFO_TYPE structure definition above for complete list of items */

BOOL  ISD_GetTrackerConfig( ISD_TRACKER_HANDLE, ISD_TRACKER_INFO_TYPE *, BOOL verbose );


/* When used with IS Precision Series (IS-300, IS-600, IS-900) tracking devices 
   this function call will set genlock synchronization  parameters, all other fields 
   in the ISD_TRACKER_INFO_TYPE structure are for information purposes only */

BOOL  ISD_SetTrackerConfig( ISD_TRACKER_HANDLE, ISD_TRACKER_INFO_TYPE *, BOOL verbose );


/* Get RecordsPerSec and KBitsPerSec without requesting genlock settings from the tracker.
   Use this instead of ISD_GetTrackerConfig to prevent your program from stalling while
   waiting for the tracker response. */

BOOL  ISD_GetCommInfo( ISD_TRACKER_HANDLE, ISD_TRACKER_INFO_TYPE * );


/* Configure station as specified in the ISD_STATION_INFO_TYPE structure. Before 
   this function is called, all elements of the structure must be assigned a value. 
   stationNum is a number from 1 to ISD_MAX_STATIONS. Should only be used with
   IS Precision Series tracking devices, not valid for InterTrax.  */

BOOL  ISD_SetStationConfig( ISD_TRACKER_HANDLE, ISD_STATION_INFO_TYPE *, 
                              WORD stationNum, BOOL verbose );


/* Fills the ISD_STATION_INFO_TYPE structure with current settings. Function
   requests configuration records from the tracker and waits for the response.
   If communications are interrupted, it will stall for several seconds while 
   attempting to recover the settings. Should only be used with IS Precision Series 
   tracking devices, not valid for InterTrax.
   stationNum is a number from 1 to ISD_MAX_STATIONS */

BOOL  ISD_GetStationConfig( ISD_TRACKER_HANDLE, ISD_STATION_INFO_TYPE *, 
                              WORD stationNum, BOOL verbose );


/* Get data from all configured stations. Data is places in the ISD_DATA_TYPE
   structure. Orientation array may contain Euler angles or Quaternions, depending
   on the settings of the AngleFormat field of the ISD_STATION_INFO_TYPE structure.
   TimeStamp is only available if requested by setting TimeStamped field to TRUE. */

BOOL  ISD_GetTrackerData( ISD_TRACKER_HANDLE, ISD_DATA_TYPE * );


/* Send a configuration script to the tracker. Script must consist of valid commands as
   described in the interface protocol. Commands in the script should be terminated by
   the New Line character '\n'. Line Feed character '\r' is added by the function and is
   not required. */

BOOL  ISD_SendScript( ISD_TRACKER_HANDLE, char * );


/* Number of currently opened trackers is stored in the parameter passed to this
   functions */

BOOL  ISD_NumOpenTrackers( WORD * );


/* Reset tracker angles to specified values, only serial version of InterTrax is supported 
   in this release */

BOOL  ISD_ResetAngles( ISD_TRACKER_HANDLE, float yaw, float pitch, float roll );


/* Open tracker configuration win32 interface. Provide tracker handle to configure one
   specific tracking device, or NULL to get all configured devices. 
   Not implemented in this release. */

HWND  ISD_OpenConfigWindow( ISD_TRACKER_HANDLE );

//////////////////////////////////////////////////////////////////////////////////////////////

/******************************************************************************/
ISD_TRACKER_HANDLE ISD_OpenTracker( HWND hParent, DWORD commPort, 
                                      BOOL infoScreen, BOOL verbose )
{
    if(!_ISD_OpenTracker) /* this will be NULL if dll not loaded */
    {
        ISD_lib = load_ISLIB();

        if(!ISD_lib)  /* failed to load dll */
        {
            return 0;
        }
    }
    return((*_ISD_OpenTracker)( hParent, commPort, infoScreen, verbose ));
}


/******************************************************************************/
BOOL  ISD_CloseTracker( ISD_TRACKER_HANDLE handle )
{
    BOOL ret;
    WORD num;

    if(_ISD_CloseTracker)
    {
        ret = (*_ISD_CloseTracker)( handle );

        /* if all trackers are closed the dll can be freed */
        if(ISD_NumOpenTrackers( &num ))
        {
            if(num == 0)
            {
                free_ISLIB( ISD_lib );
                ISD_lib = NULL;
            }
        }
        return ret;
    }
    return FALSE;
}


/******************************************************************************/
BOOL ISD_NumOpenTrackers( WORD *num )
{
    if(_ISD_NumOpenTrackers) 
    {
        return((*_ISD_NumOpenTrackers)( num ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_GetCommInfo( ISD_TRACKER_HANDLE handle, ISD_TRACKER_INFO_TYPE *Tracker )
{
    if(_ISD_GetCommInfo)
    {
        return((*_ISD_GetCommInfo)( handle, Tracker ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_GetTrackerConfig( ISD_TRACKER_HANDLE handle, 
                             ISD_TRACKER_INFO_TYPE *Tracker, BOOL verbose )
{
    if(_ISD_GetTrackerConfig)
    {
        return((*_ISD_GetTrackerConfig)( handle, Tracker, verbose ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_SetTrackerConfig( ISD_TRACKER_HANDLE handle, 
                              ISD_TRACKER_INFO_TYPE *Tracker, BOOL verbose )
{
    if(_ISD_SetTrackerConfig)
    {
        return((*_ISD_SetTrackerConfig)( handle, Tracker, verbose ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_SetStationConfig( ISD_TRACKER_HANDLE handle, 
                              ISD_STATION_INFO_TYPE *Station, WORD stationNum, BOOL verbose )
{
    if(_ISD_SetStationConfig)
    {
        return((*_ISD_SetStationConfig)( handle, Station, stationNum, verbose ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_GetStationConfig( ISD_TRACKER_HANDLE handle, 
                              ISD_STATION_INFO_TYPE *Station, WORD stationNum, BOOL verbose )
{
    if(_ISD_GetStationConfig)
    {
        return((*_ISD_GetStationConfig)( handle, Station, stationNum, verbose ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_GetTrackerData( ISD_TRACKER_HANDLE handle, ISD_DATA_TYPE *Data )
{
    if(_ISD_GetTrackerData)
    {
        return((*_ISD_GetTrackerData)( handle, Data ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_SendScript( ISD_TRACKER_HANDLE handle, char *script )
{
    if(_ISD_SendScript)
    {
        return((*_ISD_SendScript)( handle, script ));
    }
    return FALSE;
}


/******************************************************************************/
BOOL  ISD_ResetAngles( ISD_TRACKER_HANDLE handle, float yaw, float pitch, float roll )
{
    if(_ISD_ResetAngles) 
    {
        return((*_ISD_ResetAngles)( handle, yaw, pitch, roll ));
    }
    return FALSE;
}


/******************************************************************************/
HWND  ISD_OpenConfigWindow( ISD_TRACKER_HANDLE handle )
{
    if(_ISD_OpenConfigWindow)
    {
        return((*_ISD_OpenConfigWindow)( handle ));
    }
    return NULL;
}




//////////////////////////////////////////////////////////////////////////////




vsbItrax2Driver::vsbItrax2Driver()
{
	m_Handle = 0;
	DriverName("vsbItrax2Driver");
}

vsbItrax2Driver::~vsbItrax2Driver()
{
	Stop();
}

string vsbItrax2Driver::GetStatusString()
{
	return last_message;
}

const string &vsbItrax2Driver::DriverIdString() const 
{
	static string driver = "Intellisense ITRAX2";
	return driver;
}


bool vsbItrax2Driver::Start()
{
	m_Handle = -1;
	
    for(int i=1; i < 8 && m_Handle < 0; i++)
		m_Handle = ISD_OpenTracker( 0, i, 0, 0 );

	if (m_Handle<0) 
		m_Handle = 0;
	
	return m_Handle != 0;
}

void vsbItrax2Driver::Stop()
{
	if (m_Handle) 
		ISD_CloseTracker(m_Handle);

	m_Handle = 0;

}

void vsbItrax2Driver::ResetAngles()
{
  if (m_Handle)
	  ISD_ResetAngles( m_Handle, 0, 0, 0 );
}

void vsbItrax2Driver::GetAngles(sgVec3 hpr)
{
	if (m_Handle)
	{
		ISD_DATA_TYPE data;
		if (ISD_GetTrackerData( m_Handle, &data ))
		{
			hpr[0] = -data.Station[0].Orientation[0];
			hpr[1] = data.Station[0].Orientation[1];
			hpr[2] = data.Station[0].Orientation[2];
		} else
			sgSetVec3(hpr,0,0,0);
	}
	else
		sgSetVec3(hpr,0,0,0);
}

