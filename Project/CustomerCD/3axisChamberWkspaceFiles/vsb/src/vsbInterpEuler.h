//////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseCamera.h
    \brief Defines the class vsbBaseCamera
    
     The class vsbBaseCamera is the pure abstract base class for all 
	 cameras, providing the basic funtionality to be managed by the 
	 camera manager.
	
	\sa vsbBaseCamera.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbInterpEuler_H
#define _vsbInterpEuler_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif



void vsbInterpEuler(sgVec3 res, sgVec3 from, sgVec3 to,float t);
void vsbInterpEuler(sgdVec3 res, sgdVec3 from, sgdVec3 to,float t);

#endif