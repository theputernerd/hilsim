// VsbMfcItraxDriver.h: interface for the vsbItrax2Driver class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_VSBMFCITRAXDRIVER_H__C47234B5_4C68_4D6F_A534_2B27231FC1F1__INCLUDED_)
#define AFX_VSBMFCITRAXDRIVER_H__C47234B5_4C68_4D6F_A534_2B27231FC1F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "VsbHeadTracker.h"

class DLL_VSB_API vsbItrax2Driver : public vsbHeadTrackerDeviceInterface  
{
public:
	vsbItrax2Driver();
	virtual ~vsbItrax2Driver();

	virtual const std::string &DriverIdString() const;
	virtual std::string  GetStatusString();
	virtual bool	 Start();
	virtual void	 Stop();
	virtual void	 ResetAngles();
	virtual void	 GetAngles(sgVec3 hpr);
	virtual bool     IsActive() {return m_Handle!= 0;};
private:
	int m_Handle;

};

#endif // !defined(AFX_VSBMFCITRAXDRIVER_H__C47234B5_4C68_4D6F_A534_2B27231FC1F1__INCLUDED_)
