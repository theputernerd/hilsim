#ifndef vsbSystemInit_H
#define vsbSystemInit_H
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSystemInit.h
    \brief Defines the initialisation and closedown functions for the VSB library

    The vsbSystemInit class implements the Init and Close functions which
    are the first and last functions called by an application respectively.

  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


/*! \brief Initialises the VSB system.

    vsbSystemInit is the first function that must be called before 
    VSB classes are created and any functions or methods are called.
    Its initialisation procedure is to call ssg's initialisation function 
    ssgInit, as well as the vsbVewportContext class's static Init function to 
    create instances of all the necessary global objects and registers
    all the user cameras with the camera manager as well as all the entity
    types with the entity manager. Similar registrations can also be performed
    outside of vsbSystemInit by the user for custom cameras and entities.
*/
void DLL_VSB_API vsbSystemInit();

/*! \brief Shuts down the VSB system. Not strictly necessary, but included for consistency.

    Calls the vsbVewportContext class's static Close function 
*/
void DLL_VSB_API vsbSystemClose();



#endif