//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCockpit.h
    \brief Defines a cockpit view class
 
	

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbF18Cockpit_H
#define _vsbF18Cockpit_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbCockpit_H
#include "vsbCockpit.H"
#endif


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbCockpit
    \brief Cockpit view class
 

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbF18Cockpit: public vsbCockpit 
{

protected:

					 
				  

	
public:

    //! Constructor
    vsbF18Cockpit( );


    //! Virtual destructor
    virtual ~vsbF18Cockpit( void );


    virtual void Build() ;

};


#endif // _CloudLayer_H_
