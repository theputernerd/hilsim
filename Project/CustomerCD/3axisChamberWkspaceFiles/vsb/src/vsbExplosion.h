#ifndef _vsbExplosion_H
#define _vsbExplosion_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

class DLL_VSB_API vsbExplosionFXDirector;

class DLL_VSB_API vsbExplosion: public ssgTransform
{
	friend class DLL_VSB_API vsbExplosionFXDirector;
public:
	vsbExplosion();
    ~vsbExplosion();

	void				Initialise(ssgSimpleState *expl_state, int sqrt_count, float scale, float duration);
	
	void				Initialise(const std::string &explTypeName, int sqrt_count, float scale, float duration);



	void				SetScale(float scale);
	float				GetScale() const { return mSize; }
	
	void				SetFrameDuration(float r);
	float				GetFrameDuration() const {return mFrameDuration;};
    int                 FrameCount() const {return mCount;}
  
	void				StartAnimation(float initialTime);
	void				StopAnimation();
	float				InitialTime() const { return mInitialTime;};


	// Following are in model world coordinates

	void GetPosition	(float &x, float &y, float &z);
	void SetPosition    ( float x, float y, float z, float scale = -1) ;



	void			    CameraCentricTranslation();

	int					Id() { return mId;}
	bool				IsRunning();
	static ssgSimpleState *MakeState(const char *explFname = 0);

	const std::string   TypeName() { return mTypeName; }
	bool				SetType(const std::string &type_name);
	bool				SetType(const char * type_name);
				
protected:
	

	float				mInitialTime;
	float				mFrameDuration;
	bool				mRunning;
	int					mId;

	sgdVec3				mPos;
	ssgCutout			*mpCutout;
	ssgSimpleState		*mpState;
	ssgVertexArray		*mpVertices;
	ssgColourArray		*mpColourArray; 
	ssgTexCoordArray   **mpTextureCoordSets;

	float				 mSize;
	int			         mSqrtCount;
	int			         mCount;
	//ssgTimedSelector   *mpTimedSelector;
	ssgSelector   *mpSelector;
	ssgLeaf		       **mpLeafGeometrySets;

	std::string			mTypeName;


	

private:
	static int          msIdCounter;
	
	int					mListSize;
	static float		msTextureUnitCoord[4][2];
	void				CreateStructure();
	void				AdvanceFrame();	
	

};


#endif

