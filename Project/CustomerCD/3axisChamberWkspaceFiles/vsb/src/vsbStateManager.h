#ifndef _vsbStateManager_H
#define _vsbStateManager_H

//////////////////////////////////////////////////////////////////
/*! 
    \file vsbStateManager.h
    \brief Defines a class for managing shared ssgSimpleState's
 

	This file defines a cache of shared states which you can use to 
    load states and look-up and subsequently reference them as required 
    
    It also implements the ability to register a number of callback
    functions which can be called to create/load new states. 
	

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _SET_
#include <set>
#define _SET_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif



/*! \brief loader function 

This type defines a loader function that can be registered with the
state loader which can be called to populate the state manager.
Other classes such as the explosion class can have specific loaders
implemented to populate the state manager with the states they require.
These loaders can be calles once the loaders are registerd via a member
function.

*/


class DLL_VSB_API vsbStateManager;

/*! \brief vsbStateManagerLoader 

Defines the pointer to function type that all loader callback functions 
conform to
*/
typedef void (*vsbStateManagerLoader)(vsbStateManager *stateManager);


/*! \brief vsbStateManagerLoader 

Defines the pointer to function type that all loader callback functions 
conform to
*/

//////////////////////////////////////////////////////////////////
/*! 
	\class vsbStateManager
	\brief State manager class 
    
    The state manager class caches and manages a set of shared ssgSimpleState's
    
*/
/////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbStateManager
{
public:

                    //! Enumeration identifying results of state insertions
	enum			InsertStatus {EStateManagerFailed, EStateManagerOK, EStateManagerExists}; 

                    //! Constructoe
					vsbStateManager();

                    //! Virtual destructor
virtual				~vsbStateManager();

                    /*! \bried Insert a new state into the vsbStateManager's cache

                    New ssgState's are inserted into the vsbStateManager's cache
                    and associated with an identifier string.

                    \param stateName The identifier string to be associated with the state
                    \param someState A pointer to the new state to be managed

                    \returns If an ssgState already exist in the chach for the given 
                    identifier the function return EStateManagerExists. On successful
                    insertion EStateManagerOK is returned. Otherwise EStateManagerFailed
                    is returned

                    \note Tt is expected that all loader callback functions will call
                    this member function to insert their states into the cache
                    */
	InsertStatus	Insert(const std::string &stateName, ssgSimpleState *someState);

                    /*! \brief Find the state corresponding to the identifier string
                    \param stateName The string identifer matching the state sought
                    \returns a pointer to the found ssgSimpleState, NULL otherwise
                    */
	ssgSimpleState *Find(const std::string &stateName);

                     /*! \brief Find the state corresponding to the texture filename                    
                     \param stateName The string identifer matching the state sought
                    \returns a pointer to the found ssgSimpleState, NULL otherwise
                    */
	ssgSimpleState *FindByTextureFilename(const std::string &textureFileName);

                    /*! \brief Delete the state corresponding to the identifier string
                    \param stateName The string identifer matching the state sought

                    If no matching state is found in the cache, the function does nothing 
                    */
	void			Delete(const std::string &stateName);

                    /*! \brief Clears the ssgSimpleState cache

                    All cached states are DeRefDeleted and the chache is cleared
                    */
	void			ClearStates();

                    /*! \brief Clears the loader callback list
                    */
	void			ClearLoaders();


                    /*! \brief Add a loader callback function ot the list of loaders
                    \param loader Pointer to a loader callback function
                    */
	InsertStatus	AddLoader(vsbStateManagerLoader loader);

                    /*! \brief Remove a loader callback 

                    Remove a specific loader callback function from the loader callback
                    function list
                    */
	void			RemoveLoader(vsbStateManagerLoader loader);

                    /*! \bried Call all loader callback functions
                    */
	void			CallLoaders();

private:

#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

	std::map<std::string, ssgSimpleState *> mStateCache; //!< ssgSimpleState cache
	std::set<vsbStateManagerLoader>    mLoaderList;  //! Loader callback function list

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif
};



#endif