//////////////////////////////////////////////////////////////////
/*! 
    \file vsbModelRoot.cpp
    \brief Implements the class vsbModelRoot and its auxillary class 
	vsbSelectorInterface
	
	\sa vsbModelRoot.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>
using namespace std;

#include "sg.h"
#include "ssg.h"
#include "vsbModelRoot.h"
#include "ssgLocal.h"


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbSelectorInterfaceNode
    \brief An ssgSelector state management class

	Defines a selector node entry, representing an ssgSelector, and its 
    settings, for the vsbSelectorInterface list.

    The node entry consists of	a selector state bitfield, default
    state bitfield, and all necessary information to reference and interact 
    with a target ssgSelector in the sceneTree underneath the parent 
    vsbModelRoot.

	\sa ssgSelector, vsbSelectorInterface
*/
//////////////////////////////////////////////////////////////////

class vsbSelectorInterfaceNode
{
protected:
	string			mName;          //!< The actual name of the ssgSelector (case sensitive)
	string			mLogicalName;   //!< The actual logical name of the ssgSelector (case sensitive)
	int				mSelectorBits;  //!< The current bit settings for the selector
	int				mDefaultBits;   //!< The default bit settings for the selector
	int				mSavedBits;     /*!< \brief Saved state bits, used to restore ssgSelectorState to original 
                                        settings 
                                        \note This is necessary as the model containing the ssgSelector 
                                        of interest, may have more than one reference and not
                                        all references necessarily have an vsbSelectorInterfaceNode so
                                        we dont want each vsbSelectorInterfaceNode to leave the model's
                                        ssgSelector in a different state to what it found it in*/
	ssgSelector *	mpSelector;     //!< A pointer to the actual ssgSelector this node represents
	vsbModelRoot *	mpModelRoot; 	//!< A pointer to the parent mpModelRoot	

public:

                    //! Default constructor
	                vsbSelectorInterfaceNode();

                    /*! \brief Constructor

                        Initialises the vsbSelectorInterfaceNode with all the necessary information to reference and
                        control a given ssgSelector via its parent vsbModelRoot. The logical name for this selector
                        is assumed to be the same as the actual name.

                        \param actualSelectorName The actual name of the ssgSelector
                        \param mr Pointer the the vsbModelRoot parent
                        \param defaultBits the default bit settings for this ssgSelector
                    */
	                vsbSelectorInterfaceNode(const string &actualSelectorName, vsbModelRoot *mr, int defaultBits);

                    /*! \brief Constructor

                        Initialises the vsbSelectorInterfaceNode with all the necessary information to reference and
                        control a given ssgSelector via its parent vsbModelRoot. This version introduces a logical 
                        name for the selector.

                        \param actualSelectorName The actual name of the ssgSelector
                        \param logicalSelectorName The logical name of the ssgSelector
                        \param mr Pointer the the vsbModelRoot parent
                        \param defaultBits the default bit settings for this ssgSelector
                    */
	                vsbSelectorInterfaceNode(const string &actualSelectorName, const string &logicalSelectorName, 
                                              vsbModelRoot *mr, int defaultBits);

                    //! Copy constructor
	                vsbSelectorInterfaceNode(const vsbSelectorInterfaceNode &src);

                    //! Virtual destructor
	virtual         ~vsbSelectorInterfaceNode();

                    /*! \brief Initialise the selector 
                        
                        Used by constructors and also provided to allow this vsbSelectorInterfaceNode to be
                        re initialised for a new ssgSelector on the fly
                    */
   	void			Initialise(const string actualSelectorName, const string logicalSelectorName, vsbModelRoot *mr, int defaultBits);

                    //! Assignment operator
                    vsbSelectorInterfaceNode &operator = (const vsbSelectorInterfaceNode &selInterface);

                    //! Equality operator
	bool			operator == (const vsbSelectorInterfaceNode &selInterface);

                    //! Equality operator
	bool			operator == (const string &logicalSelectorName);


                    //! Set the default bit settings for the selector
	void			Default()					{mSelectorBits = mDefaultBits;}

                    //! Apply the selector settings to the actual ssgSelector
	bool			Apply();

                    //! Restore the original selector settings
	bool			Restore();

                    /*! \brief Hook the actual ssgSelector into this vsbSelectorInterfaceNode 
                        Enables this vsbSelectorInterfaceNode to operate on a ssgSelector by
                        finding the selector in the scene tree and keeping a pointer to it.
                    */
	bool			Hook();

                    /*! \brief Unhook the ssgSelector from this vsbSelectorInterfaceNode 
                        Settings will no longer affect any ssgSelector as the pointer to
                        an ssgSelector is discarded
                    */
	void			Unhook() {mpSelector = 0;};

                    /*! \brief Reports if this vsbSelectorInterfaceNode is active, ie, Hooked
                    */
	bool			IsActivated();

                    //! Get the actual name of the selector
	const string   &Name() const { return mName;};

                    //! Get the logical name of the selector
	const string   &LogicalName() const { return mLogicalName;};

	void			ReplaceBits(int bits)		{mSelectorBits = bits;};
	void			SetBits(int bits)			{mSelectorBits |= bits;};
	void			Clear()						{mSelectorBits = 0;};
	void			ClearBits(int bits)			{mSelectorBits &= (~bits);};

	void			ReplaceDefaultBits(int bits){mDefaultBits = bits;};
	void			SetDefaultBits(int bits)	{mDefaultBits |= bits;};
	void			ClearDefault()				{mDefaultBits = 0;};
	void			ClearDefaultBits(int bits)	{mDefaultBits &= (~bits);};

	int				GetBits()	const			{return mSelectorBits;};
	int				GetDefaultBits() const		{return mDefaultBits;};

private:

};


vsbSelectorInterfaceNode::vsbSelectorInterfaceNode()
{
	Initialise("", "", 0, 0);
}

vsbSelectorInterfaceNode::vsbSelectorInterfaceNode( const string & actualSelectorName, vsbModelRoot *mr, int defaultBits)
{
	Initialise(actualSelectorName, actualSelectorName, mr, defaultBits);
}

vsbSelectorInterfaceNode::vsbSelectorInterfaceNode( const string & actualSelectorName, const string &logicalSelectorName, vsbModelRoot *mr, int defaultBits)
{
	Initialise(actualSelectorName, logicalSelectorName, mr, defaultBits);
}

vsbSelectorInterfaceNode::~vsbSelectorInterfaceNode()
{
}

void vsbSelectorInterfaceNode::Initialise(const string name, const string logicalName, vsbModelRoot *mr, int defaultBits)
{
	mName		= name;
	mLogicalName = logicalName;
	mpModelRoot = mr;
	mSelectorBits = mDefaultBits	= defaultBits;
	mpSelector  = 0;
	mSavedBits		= 0;
}

vsbSelectorInterfaceNode &vsbSelectorInterfaceNode::operator = (const vsbSelectorInterfaceNode &src)
{
	Initialise(src.mName, src.mLogicalName, src.mpModelRoot, src.mDefaultBits);
	mSelectorBits = src.mSelectorBits;
	mpSelector = src.mpSelector;
	return *this;
}


bool vsbSelectorInterfaceNode::IsActivated() 
{
	if (mpSelector == 0 || mpModelRoot == 0) return false;
	if (mpModelRoot->getNumKids() == 0)
	{
		mpSelector = 0;
		return false;
	}
	return true;
};

bool vsbSelectorInterfaceNode::Apply()
{
	if (!IsActivated() || !mpSelector) return false;
	mSavedBits = mpSelector->getSelect();
	mpSelector->select(mSelectorBits);
	return true;
}

bool vsbSelectorInterfaceNode::Restore()
{
	if (!IsActivated() || !mpSelector) return 0;
	mpSelector->select(mSavedBits);
	return true;
}

bool vsbSelectorInterfaceNode::Hook()
{
	mpSelector = 0;

	if (mpModelRoot == 0 || mpModelRoot->getNumKids() == 0) return false;

	ssgBase *entity = mpModelRoot->getByName ( const_cast<char *>(mName.c_str()) ) ;
	if (!entity || !entity->isAKindOf ( _SSG_TYPE_SELECTOR )) return false;
	mpSelector = (ssgSelector *)entity;
	return true;
}


bool vsbSelectorInterfaceNode::operator == (const vsbSelectorInterfaceNode &selInterface)
{
	return (mLogicalName == selInterface.mLogicalName);
}

bool vsbSelectorInterfaceNode::operator == (const string &name)
{
	return (mLogicalName == name);
}

////////////////////////////////////////////////////////////////////////////////
// vsbModelRoot
////////////////////////////////////////////////////////////////////////////////


vsbSelectorInterface::vsbSelectorInterface(vsbModelRoot &mr)
{
	mpModelRoot = &mr;
	mpSelectorInterfaceNodes = new vsbSelectorInterfaceNode[MAX_SELECTORS];
	mNumInterfaceNodes = 0;
}

vsbSelectorInterface::~vsbSelectorInterface()
{
	delete[] mpSelectorInterfaceNodes;
}

int vsbSelectorInterface::RegisterSelector(string actualSelectorName, int defaultVal)
{
	return RegisterSelector(actualSelectorName, actualSelectorName, defaultVal);
}	

int vsbSelectorInterface::RegisterSelector(string actualSelectorName, string logicalSelectorName, int defaultVal)
{
	int idx;
	assert (mpSelectorInterfaceNodes!= 0);
	
	for(idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == logicalSelectorName)
		{
			mpSelectorInterfaceNodes[idx].Initialise(actualSelectorName, logicalSelectorName, mpModelRoot, defaultVal);
			return idx;
		}

	if (mNumInterfaceNodes == MAX_SELECTORS) 
		return -1;

	idx = mNumInterfaceNodes++;
	mpSelectorInterfaceNodes[idx].Initialise(actualSelectorName, logicalSelectorName, mpModelRoot, defaultVal);
	if (mpModelRoot && mpModelRoot->getNumKids()) ActivateSelectorHooks();

	return idx;
}

bool vsbSelectorInterface::ReplaceSelectorBits(int selectorIndex, int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].ReplaceBits(opts);
	return true;
}

bool vsbSelectorInterface::SetSelectorBits(int selectorIndex, int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].SetBits(opts);
	return true;
}

bool vsbSelectorInterface::ClearSelectorBits(int selectorIndex)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].Clear();
	return true;
}

bool vsbSelectorInterface::ClearSelectorBits(int selectorIndex, int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].ClearBits(opts);
	return true;
}

bool  vsbSelectorInterface::ReplaceDefaultSelectorBits(int selectorIndex,  int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].ReplaceDefaultBits(opts);
	return true;
}


bool  vsbSelectorInterface::SetDefaultSelectorBits(int selectorIndex, int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].SetDefaultBits(opts);
	return true;
}

bool  vsbSelectorInterface::ClearDefaultSelectorBits(int selectorIndex)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].ClearDefault();
	return true;
}


bool  vsbSelectorInterface::ClearDefaultSelectorBits(int selectorIndex, int opts)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].ClearDefaultBits(opts);
	return true;
}



bool vsbSelectorInterface::ReplaceSelectorBits(const string &selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].ReplaceBits(opts);
			return true;
		}
    return false;
}

bool vsbSelectorInterface::SetSelectorBits(const string & selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].SetBits(opts);
			return true;
		}
    return false;
}


bool vsbSelectorInterface::ClearSelectorBits(const string & selectorLogicalName)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].Clear();
			return true;
		}
    return false;
}

bool vsbSelectorInterface::ClearSelectorBits(const string & selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].ClearBits(opts);
			return true;
		}
    return false;
}


bool vsbSelectorInterface::ReplaceDefaultSelectorBits(const string & selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].ReplaceDefaultBits(opts);
			return true;
		}
    return false;
}

bool vsbSelectorInterface::SetDefaultSelectorBits(const string & selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].SetDefaultBits(opts);
			return true;
		}
    return false;
}


bool vsbSelectorInterface::ClearDefaultSelectorBits(const string & selectorLogicalName)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].ClearDefault();
			return true;
		}
    return false;
}

bool vsbSelectorInterface::ClearDefaultSelectorBits(const string & selectorLogicalName, int opts)
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].ClearDefaultBits(opts);
			return true;
		}
    return false;
}


bool vsbSelectorInterface::Default(int selectorIndex)
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return false;
	mpSelectorInterfaceNodes[selectorIndex].Default();
	return true;
}

bool vsbSelectorInterface::Default(const string & selectorLogicalName)
{
		for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
		{
			mpSelectorInterfaceNodes[idx].Default();
			return true;
		}
		return false;
}

int vsbSelectorInterface::GetSelectorBits(int selectorIndex) const
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return 0;
	return mpSelectorInterfaceNodes[selectorIndex].GetBits();
}
	
int vsbSelectorInterface::GetSelectorBits(const string & selectorLogicalName) const
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
			return mpSelectorInterfaceNodes[idx].GetBits();
		return 0;
}

int vsbSelectorInterface::GetDefaultSelectorBits(int selectorIndex) const
{
	if (selectorIndex<0 || selectorIndex >= mNumInterfaceNodes) return 0;
	return mpSelectorInterfaceNodes[selectorIndex].GetDefaultBits();
}
	
int vsbSelectorInterface::GetDefaultSelectorBits(const string & selectorLogicalName) const
{
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		if (mpSelectorInterfaceNodes[idx] == selectorLogicalName)
			return mpSelectorInterfaceNodes[idx].GetDefaultBits();
		return 0;
}


void vsbSelectorInterface::ActivateSelectorHooks()
{
	if (mpModelRoot && mpModelRoot->getNumKids() > 0)
		for (int i=0; i<mNumInterfaceNodes; i++)
			mpSelectorInterfaceNodes[i].Hook();
}


void vsbSelectorInterface::DeactivateSelectorHooks()
{
	for(int i =0; i<mNumInterfaceNodes; i++)
		mpSelectorInterfaceNodes[i].Unhook();
}

void vsbSelectorInterface::ApplySelectors()
{
	if (mpModelRoot == 0 || mpModelRoot->getNumKids() == 0) return;
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		mpSelectorInterfaceNodes[idx].Apply();
}

void vsbSelectorInterface::RestoreSelectors()
{
	if (mpModelRoot == 0 || mpModelRoot->getNumKids() == 0) return;
	for(int idx =0; idx<mNumInterfaceNodes; idx++)
		mpSelectorInterfaceNodes[idx].Restore();
}


const string  &vsbSelectorInterface::SelectorName(int idx) const
{
	assert (idx>=0 && idx < mNumInterfaceNodes) ;
	return mpSelectorInterfaceNodes[idx].LogicalName();
}


////////////////////////////////////////////////////////////////////////////////
// vsbModelRoot
////////////////////////////////////////////////////////////////////////////////


vsbModelRoot::vsbModelRoot(void):ssgTransform () 
{
	  type |= SSG_TYPE_MODELROOT ;
	  mScale = 1.0;
	  sgMakeIdentMat4(mPreXfm);
	  mpSelectors = new vsbSelectorInterface(*this);
}

vsbModelRoot::vsbModelRoot( sgCoord *c ): ssgTransform(c)
{
	type |= SSG_TYPE_MODELROOT ;
	mScale = 1.0;
	sgMakeIdentMat4(mPreXfm);
	mpSelectors = new vsbSelectorInterface(*this);
}

vsbModelRoot::~vsbModelRoot()
{
	delete mpSelectors; 
}

void vsbModelRoot::copy_from ( vsbModelRoot *src, int clone_flags )
{
  ssgTransform::copy_from ( src, clone_flags ) ;
  mScale = src->mScale;
  sgCopyMat4(mPreXfm, src->mPreXfm);
}

ssgBase *vsbModelRoot::clone ( int clone_flags ) 
{
  vsbModelRoot *b = new vsbModelRoot ;
  b -> copy_from ( this, clone_flags ) ;
  return b ;
}

void vsbModelRoot::recalcBSphere (void)
{
	ssgTransform::recalcBSphere();
	sgVec3 centre;
	bsphere.getCenter ( centre );
	scaleBSphere(mScale, this);
}


void vsbModelRoot::scaleBSphere(float scale, ssgEntity *entity)
{

	float radius;
	sgSphere *bsphere;

	bsphere = entity->getBSphere();

	radius = bsphere->getRadius();


	bsphere->setRadius(bsphere->getRadius ()*scale);

	if (entity -> isAKindOf ( ssgTypeBranch() ) )
	{
		ssgBranch *branch = (ssgBranch *) entity;
	    for ( ssgEntity *k = branch->getKid ( 0 ) ; k != NULL ; k = branch->getNextKid () )
		   scaleBSphere(scale, k);
	}
    
}

void vsbModelRoot::dirtyBSphere ()

{
	ssgEntity::dirtyBSphere ();
	dirtyChildBSpheres(this);
  
}

void vsbModelRoot::dirtyChildBSpheres(ssgEntity *entity)
{
	entity->invalidateBSphere();
	if (entity -> isAKindOf ( ssgTypeBranch() ) )
	{
		ssgBranch *branch = (ssgBranch *) entity;
	    for ( ssgEntity *k = branch->getKid ( 0 ) ; k != NULL ; k = branch->getNextKid () )
		   dirtyChildBSpheres(k);
	}

}




const char *vsbModelRoot::getTypeName (void) { return "vsbModelRoot"; }


int vsbModelRoot::load ( FILE *fd ) 
{
  _ssgReadMat4 ( fd, transform ) ;
  _ssgReadMat4 ( fd, mPreXfm );
  _ssgReadFloat( fd, &mScale );
	

  updateTransform () ;
  first_time = TRUE ;
  return ssgBranch::load(fd) ;
}

int vsbModelRoot::save ( FILE *fd ) 
{
	  _ssgWriteMat4 ( fd, transform ) ;
	  _ssgWriteMat4 ( fd, mPreXfm ) ;
	  _ssgWriteFloat ( fd, mScale ) ;

	  return ssgBranch::save(fd) ;
}

void vsbModelRoot::setLocalTransform ( sgVec3 xyz )
{
	sgMakeTransMat4 ( mPreXfm, xyz ) ;
}

void vsbModelRoot::setLocalTransform ( sgCoord *xform ) 
{
	sgMakeCoordMat4 ( transform, xform ) ;
}


void vsbModelRoot::setLocalTransform ( sgMat4 xform ) 
{
	sgCopyMat4( mPreXfm, xform ) ;
}

void vsbModelRoot::setTransform ( sgVec3 xyz ) 
{
  updateTransform () ;
  sgMakeTransMat4 (transform, xyz ) ;
  sgPreMultMat4(transform, mPreXfm);
  sgScaleVec3     ( transform[0], mScale ) ;
  sgScaleVec3     ( transform[1], mScale ) ;
  sgScaleVec3     ( transform[2], mScale ) ;
  firsttime       () ; 
  dirtyBSphere    () ;
}

void vsbModelRoot::setTransform ( sgCoord *xform ) 
{
  updateTransform () ;
  sgMakeCoordMat4 ( transform, xform ) ;
  sgPreMultMat4(transform, mPreXfm);
  sgScaleVec3     ( transform[0], mScale ) ;
  sgScaleVec3     ( transform[1], mScale ) ;
  sgScaleVec3     ( transform[2], mScale ) ;
  firsttime       () ; 
  dirtyBSphere () ;
}

void vsbModelRoot::setTransform ( sgdCoord *xform ) 
{
	updateTransform () ;
	sgMakeCoordMat4 ( transform, xform->xyz[0], xform->xyz[1], xform->xyz[2], xform->hpr[0], xform->hpr[1], xform->hpr[2] ) ;
	sgPreMultMat4(transform, mPreXfm);
	sgScaleVec3     ( transform[0], mScale ) ;
	sgScaleVec3     ( transform[1], mScale ) ;
	sgScaleVec3     ( transform[2], mScale ) ;
	firsttime       () ; 
	dirtyBSphere () ;
}

void vsbModelRoot::setTransform ( sgCoord *xform, float sx, float sy, float sz ) 
{
  updateTransform () ;
  sgMakeCoordMat4 ( transform, xform ) ;
  sgPreMultMat4(transform, mPreXfm);
  sgScaleVec3     ( transform[0], sx ) ;
  sgScaleVec3     ( transform[1], sy ) ;
  sgScaleVec3     ( transform[2], sz ) ;
  firsttime       () ; 
  dirtyBSphere () ;
}

void vsbModelRoot::setTransform ( sgMat4 xform ) 
{
  updateTransform () ;
  sgCopyMat4      ( transform, xform ) ;
  sgPreMultMat4   (transform, mPreXfm);
  sgScaleVec3     ( transform[0], mScale ) ;
  sgScaleVec3     ( transform[1], mScale ) ;
  sgScaleVec3     ( transform[2], mScale ) ;
  firsttime       () ; 
  dirtyBSphere () ;
}


void vsbModelRoot::ApplySelectors()
{
	Selectors().ApplySelectors();
}

void vsbModelRoot::RestoreSelectors()
{
	Selectors().RestoreSelectors();
}

void vsbModelRoot::cull( sgFrustum *f, sgMat4 m, int test_needed ) 
{
	if ( ! preTravTests ( &test_needed, SSGTRAV_CULL ) )
    return ;

  int cull_result = cull_test ( f, m, test_needed ) ;

  if ( cull_result == SSG_OUTSIDE )
    return ;

  sgMat4 tmp ;

  sgCopyMat4 ( tmp, m ) ;
  sgPreMultMat4 ( tmp, transform ) ;

  _ssgPushMatrix ( tmp ) ;
  glPushMatrix () ;
  glLoadMatrixf ( (float *) tmp ) ;

  ApplySelectors();	


  for ( ssgEntity *e = getKid ( 0 ) ; e != NULL ; e = getNextKid() )
    e -> cull ( f, tmp, cull_result != SSG_INSIDE ) ;

  RestoreSelectors();

  glPopMatrix () ;
  _ssgPopMatrix () ;
  

  postTravTests ( SSGTRAV_CULL ) ; 
}

void vsbModelRoot::isect( sgSphere  *s, sgMat4 m, int test_needed ) 
{
  if ( ! preTravTests ( &test_needed, SSGTRAV_ISECT ) )
    return ;

  int isect_result = isect_test ( s, m, test_needed ) ;

  if ( isect_result == SSG_OUTSIDE )
    return ;

  sgMat4 tmp ;

  sgCopyMat4 ( tmp, m ) ;
  sgPreMultMat4 ( tmp, transform ) ;

  _ssgPushPath ( this ) ;
  
  ApplySelectors();

  for ( ssgEntity *e = getKid ( 0 ) ; e != NULL ; e = getNextKid() )
    e -> isect ( s, tmp, isect_result != SSG_INSIDE ) ;

  RestoreSelectors();

  _ssgPopPath () ;

  postTravTests ( SSGTRAV_ISECT ) ; 
}


void vsbModelRoot::hot( sgVec3     s, sgMat4 m, int test_needed ) 
{
  if ( ! preTravTests ( &test_needed, SSGTRAV_HOT ) )
    return ;

  int hot_result = hot_test ( s, m, test_needed ) ;

  if ( hot_result == SSG_OUTSIDE )
    return ;


  sgMat4 tmp ;

  sgCopyMat4 ( tmp, m ) ;
  sgPreMultMat4 ( tmp, transform ) ;

  _ssgPushPath ( this ) ;
  
  ApplySelectors();

  for ( ssgEntity *e = getKid ( 0 ) ; e != NULL ; e = getNextKid() )
    e -> hot ( s, tmp, hot_result != SSG_INSIDE ) ;

  RestoreSelectors();

  _ssgPopPath () ;

  postTravTests ( SSGTRAV_HOT ) ;
}

void vsbModelRoot::setScale(float scale) 
{ 
	mScale = scale; 
	dirtyBSphere ();
};

