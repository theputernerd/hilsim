//////////////////////////////////////////////////////////////////
/*! 
    \file vsbEnvironmentOptions.cpp
    \brief Defines the class vsbEnvironmentOptions
 

	This file defines the vsbEnvironmentOptions class which is used by the 
	environment manager as a database of environmental parameters describing 
	the general attributes of the 3D scene to be displayed.

    Environment options include real world attributes such as cloud types
	and layers, time of day, weather, terrain type etc.

	\sa vsbEnvironmentOptions.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#ifdef WIN32
#include <windows.h>
#endif

#include <math.h>
#include <string>
#include <map>


#include "parser_perror.h"


#include "vsbGroundPlaneParams.h"
#include "vsbPlaneTextureList.h"
#include "vsbEnvironmentOptions.h"

using namespace std;
using namespace Parser;
/////////////////////////////////////////////////////



char *vsbEnvironmentOptionsParser::mpKeyWords[] =
{
	"ENVIRONMENT_OPTIONS",
	"SKY_DOME",
	"SUN",
	"CLOUDS",
	"GROUND_PLANES",
	"WEATHER",
	"SCALING",
	"TRAJECTORY_TRACES",
	"SYMMETRIC",
	"INDEPENDANT",
	"HRIZONTAL_SCALE",
	"VERTICAL_SCALE",
	"SCALE",
	"UPDATE_RATE",
	"TIME_OF_DAY",
	"TIME_OF_YEAR",
	"VISIBILITY_RANGE",
	"CLOUD_LAYER_COUNT",
	"GROUND_PLANE_COUNT",
	"CLOUD_PRE_DRAW",
	"TIME_ADVANCE_REGIME",
	"FIXED_STEP",
	"EXTERNAL",
	"REALTIME",
	"LOWER_TIMEBOUND",
	"UPPER_TIMEBOUND",
	"TIME_STEP",
	"ON",
    "OFF",
	"HUD",
	"HILIGHT_RGB",
	"NORMAL_RGB",
	"BACKGROUND_RGB",
	"TRANSPARENCY",
	"TRAJ_THICKNESS",
	"ENTITY_CENTRIC_CAM",
	"CHUNKLOD_TERRAIN",
	"WIREFRAME",
	0
};

enum 
{
	VSB_EO_ENVIRONMENT_OPTIONS,
	VSB_EO_SKY_DOME,
	VSB_EO_SUN,
	VSB_EO_CLOUDS,
	VSB_EO_GROUND_PLANES,
	VSB_EO_WEATHER,
	VSB_EO_SCALING,
	VSB_EO_TRAJECTORY_TRACES,
	VSB_EO_SYMMETRIC,
	VSB_EO_INDEPENDANT,
	VSB_EO_HORIZONTAL,
	VSB_EO_VERTICAL,
	VSB_EO_UNIFORM,
	VSB_EO_UPDATE_RATE,
	VSB_EO_TIME_OF_DAY,
	VSB_EO_TIME_OF_YEAR,
	VSB_EO_VISIBILITY_RANGE,
	VSB_EO_CLOUD_LAYER_COUNT,
	VSB_EO_GROUND_PLANE_COUNT,
	VSB_EO_PRE_DRAW,
	VSB_EO_TIME_ADVANCE_REGIME,
	VSB_EO_FIXED_STEP,
	VSB_EO_EXTERNAL,
	VSB_EO_REALTIME,
	VSB_EO_LOWER_TIMEBOUND,
	VSB_EO_UPPER_TIMEBOUND,
	VSB_EO_TIME_STEP,
	VSB_EO_ON,
	VSB_EO_OFF,
	VSB_EO_HUD,
	VSB_EO_HILIGHT_RGB,
	VSB_EO_NORMAL_RGB,
	VSB_EO_BACKGROUND_RGB,
	VSB_EO_TRANSPARENCY,
	VSB_EO_TRAJ_THICKNESS,
	VSB_EO_ENTITY_CENTRIC_CAM,
	VSB_EO_CHUNKLOD_TERRAIN,
	VSB_EO_WIREFRAME,
};

static int regime_table[] = {VSB_EO_FIXED_STEP, VSB_EO_EXTERNAL, VSB_EO_REALTIME, -1};


char *vsbEnvironmentOptionsParser::mpErrorStrings[] =
{
	"",
	"Either \"ON\" or \"OFF\" keyword expected.",
	"Either \"VFIXED_STEP\", \"EXTERNAL\" or \"REALTIME\" keyword expected.",
	0
};

enum {
	VSB_EO_ERR_UNUSED,
	VSB_EO_ERR_ONOFF_EXPECTED,
	VSB_EO_ERR_TIMEREGIME_EXPECTED,
};

vsbEnvironmentOptionsParser::vsbEnvironmentOptionsParser(vsbEnvironmentOptions &eo):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrEnvironmentOpts(eo) 
{
	  mpOutput = new vsbParserHelper(*this);
}

vsbEnvironmentOptionsParser::~vsbEnvironmentOptionsParser()
{
  delete mpOutput;
}



void vsbEnvironmentOptionsParser::WriteOnOff(bool onOrOff)
{
	FILE *f = mpOutput->GetFile();
	if (f)
		fprintf(f,"(%s)",(onOrOff?"ON":"OFF"));
}


int	 vsbEnvironmentOptionsParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int vsbEnvironmentOptionsParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

	vsbEnvironmentOptions &working_env_opts = mrEnvironmentOpts;

	mpOutput->WriteSpaces(indent);
	mpOutput->WriteKeyWord(VSB_EO_ENVIRONMENT_OPTIONS);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent+=3;
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_UPDATE_RATE);
	mpOutput->WriteValue(working_env_opts.mUpdateRateHZ);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_TIME_ADVANCE_REGIME);
	mpOutput->WriteString(" = ");
	mpOutput->WriteKeyWord(regime_table[working_env_opts.TimeAdvanceRegime()]);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_LOWER_TIMEBOUND);
	mpOutput->WriteValue(working_env_opts.mLowerTimeBound);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_UPPER_TIMEBOUND);
	mpOutput->WriteValue(working_env_opts.mUpperTimeBound);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_TIME_STEP);
	mpOutput->WriteValue(working_env_opts.mTimeStep);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_TIME_OF_DAY);
	mpOutput->WriteValue(working_env_opts.mTimeOfDay);
	mpOutput->WriteNewline(indent);

	mpOutput->WriteKeyWord(VSB_EO_TIME_OF_YEAR);
	mpOutput->WriteValue(working_env_opts.mTimeOfYear);
	mpOutput->WriteNewline(indent);



	// Skydome

	mpOutput->WriteKeyWord(VSB_EO_SKY_DOME);
	WriteOnOff(working_env_opts.mSkyDomeOn); 
	mpOutput->WriteNewline(indent);

	// Sun

	mpOutput->WriteKeyWord(VSB_EO_SUN);
	WriteOnOff(working_env_opts.mSunOn); 
	mpOutput->WriteNewline(indent);

	// Weather

	mpOutput->WriteKeyWord(VSB_EO_WEATHER);
	WriteOnOff(working_env_opts.mWeatherOn); 
	mpOutput->WriteNewline(indent);

	if (working_env_opts.mWeatherOn)
	{
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_VISIBILITY_RANGE);
		mpOutput->WriteValue(working_env_opts.mAtmosphericVisibility);

		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);
	}

	// Clouds

	mpOutput->WriteKeyWord(VSB_EO_CLOUDS);
	WriteOnOff(working_env_opts.mCloudOn); 
	mpOutput->WriteNewline(indent);

	if (working_env_opts.mCloudOn)
	{
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_CLOUD_LAYER_COUNT);
		mpOutput->WriteValue(working_env_opts.mNumCloudLayers);
		//mpOutput->WriteNewline(0);

        for (int i=0; i < working_env_opts.mNumCloudLayers; i++)
        {
			mpOutput->WriteNewline(0);
            vsbCloudParamsParser cloud_parser(working_env_opts.mCloudLayers[i]);
            cloud_parser.Write(file, indent);
        }

		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);
	}


	// Ground Planes

	mpOutput->WriteKeyWord(VSB_EO_GROUND_PLANES);
	WriteOnOff(working_env_opts.mGroundPlaneOn); 
	mpOutput->WriteNewline(indent);

	if (working_env_opts.mGroundPlaneOn)
	{
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_GROUND_PLANE_COUNT);
		mpOutput->WriteValue(working_env_opts.mNumGroundPlanes);
		//mpOutput->WriteNewline(0);

		for (int i=0; i < working_env_opts.mNumGroundPlanes; i++)
        {
			mpOutput->WriteNewline(0);
            vsbGroundPlaneParamsParser ground_plane_parser(working_env_opts.mGroundPlanes[i]);
            ground_plane_parser.Write(file, indent);
        }

		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);
	}

	// CHUNK LOD Terrain

	if (working_env_opts.ChunkLODTerrainOn())
	{
		mpOutput->WriteKeyWord(VSB_EO_CHUNKLOD_TERRAIN);
		mpOutput->WriteKeyWord(T_LBRACK);
		mpOutput->WriteName(working_env_opts.ChunkLODTerrainName());
		mpOutput->WriteKeyWord(T_RBRACK);
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_WIREFRAME);
		WriteOnOff(working_env_opts.mChunkLODWireframeOn);
		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);

	} 

	// scaling

	mpOutput->WriteKeyWord(VSB_EO_SCALING);
	WriteOnOff(working_env_opts.mScalingOn); 
	mpOutput->WriteNewline(indent);

	if (working_env_opts.mScalingOn)
	{
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_SYMMETRIC);
		WriteOnOff(working_env_opts.mScalingSymmetricOn);
		mpOutput->WriteNewline(indent);


		if (!working_env_opts.mScalingSymmetricOn)
			mpOutput->WriteKeyWord(VSB_EO_HORIZONTAL);
		else
			mpOutput->WriteKeyWord(VSB_EO_UNIFORM);

		mpOutput->WriteValue(working_env_opts.mHorizontalScale);

		if (!working_env_opts.mScalingSymmetricOn)
		{
			mpOutput->WriteNewline(indent);
			mpOutput->WriteKeyWord(VSB_EO_VERTICAL);
			mpOutput->WriteValue(working_env_opts.mVerticalScale);
		}

		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);

	}

	mpOutput->WriteKeyWord(VSB_EO_TRAJECTORY_TRACES);
	WriteOnOff(working_env_opts.mTrajectoryTraceOn); 
	mpOutput->WriteNewline(indent);
	if (working_env_opts.mTrajectoryTraceOn)
	{
		mpOutput->WriteKeyWord(T_LBRACE);
		indent+=3;

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_TRAJ_THICKNESS);
		mpOutput->WriteValue(working_env_opts.mTrajTraceThickness);
		
		indent-=3;
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(T_RBRACE);
		mpOutput->WriteNewline(indent);		
	}


	mpOutput->WriteKeyWord(VSB_EO_PRE_DRAW);
	WriteOnOff(working_env_opts.mCloudPreDrawModeOn); 
	mpOutput->WriteNewline(indent);

	
	mpOutput->WriteKeyWord(VSB_EO_ENTITY_CENTRIC_CAM);
	WriteOnOff(working_env_opts.mEntityCentricCameraOn); 
	mpOutput->WriteNewline(indent);


	// HUD parameters

	mpOutput->WriteKeyWord(VSB_EO_HUD);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent +=3;

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_EO_NORMAL_RGB);
	mpOutput->WriteVec3(working_env_opts.HudNormalColor());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_EO_HILIGHT_RGB);
	mpOutput->WriteVec3(working_env_opts.HudHilightColor());

	if (working_env_opts.HudBackgroundEnabled())
	{
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_BACKGROUND_RGB);
		mpOutput->WriteVec3(working_env_opts.HudBackgroundColor());

		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_EO_TRANSPARENCY);
		mpOutput->WriteValue(working_env_opts.HudBackgroundTransparency());

	}
	indent-=3;
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_RBRACE);


	indent-=3;
	mpOutput->WriteNewline(indent); 
	mpOutput->WriteKeyWord(T_RBRACE);
	mpOutput->WriteNewline(indent);
		

	return 1;
}

void vsbEnvironmentOptionsParser::ParseFunc()
{
	int i;
	vsbEnvironmentOptions working_env_opts;
	GetToken(VSB_EO_ENVIRONMENT_OPTIONS);
	GetToken(T_LBRACE);

	GetToken(VSB_EO_UPDATE_RATE);
	GetToken(T_EQ);
	working_env_opts.mUpdateRateHZ = GetNumericValue();


	GetToken(VSB_EO_TIME_ADVANCE_REGIME);
	GetToken(T_EQ);
	GetToken();
	i = KeywordTableSelection(regime_table);
	if (i<0) AbortParse(VSB_EO_ERR_TIMEREGIME_EXPECTED);
	working_env_opts.TimeAdvanceRegime((vsbEnvironmentOptions::TimeAdvanceRegimeEnum)i);

	GetToken(VSB_EO_LOWER_TIMEBOUND);
	GetToken(T_EQ);
	working_env_opts.mLowerTimeBound = GetNumericValue();

	GetToken(VSB_EO_UPPER_TIMEBOUND);
	GetToken(T_EQ);
	working_env_opts.mUpperTimeBound = GetNumericValue();

	GetToken(VSB_EO_TIME_STEP);
	GetToken(T_EQ);
	working_env_opts.mTimeStep = GetNumericValue();

	GetToken(VSB_EO_TIME_OF_DAY);
	GetToken(T_EQ);
	working_env_opts.mTimeOfDay = GetNumericValue();

	GetToken(VSB_EO_TIME_OF_YEAR);
	GetToken(T_EQ);
	working_env_opts.mTimeOfYear = GetNumericValue();


	ParseOnOffState(working_env_opts.mSkyDomeOn, VSB_EO_SKY_DOME);
	ParseOnOffState(working_env_opts.mSunOn, VSB_EO_SUN);
	ParseOnOffState(working_env_opts.mWeatherOn, VSB_EO_WEATHER);
	if (working_env_opts.mWeatherOn)
	{
		GetToken(T_LBRACE);
		GetToken(VSB_EO_VISIBILITY_RANGE);
		GetToken(T_EQ);
		working_env_opts.mAtmosphericVisibility = GetNumericValue();
		GetToken(T_RBRACE);
	}
	ParseOnOffState(working_env_opts.mCloudOn, VSB_EO_CLOUDS);
	if (working_env_opts.mCloudOn)
	{
		GetToken(T_LBRACE);
		GetToken(VSB_EO_CLOUD_LAYER_COUNT);
		GetToken(T_EQ);
		working_env_opts.mNumCloudLayers = GetNumericValue();
		for(i=0; i<working_env_opts.mNumCloudLayers; i++)
		{
			vsbCloudParamsParser cloud_parser(working_env_opts.mCloudLayers[i]);
			if (cloud_parser.Parse(itsLineInput)!=0)
			{
				  throw  *cloud_parser.ItsLastError();
			}
		}
		GetToken(T_RBRACE);
	}
	ParseOnOffState(working_env_opts.mGroundPlaneOn, VSB_EO_GROUND_PLANES);
	if (working_env_opts.mGroundPlaneOn)
	{
		GetToken(T_LBRACE);
		GetToken(VSB_EO_GROUND_PLANE_COUNT);
		GetToken(T_EQ);
		working_env_opts.mNumGroundPlanes = GetNumericValue();
		for(i=0; i<working_env_opts.mNumGroundPlanes; i++)
		{
			vsbGroundPlaneParamsParser ground_plane_parser(working_env_opts.mGroundPlanes[i]);
			if (ground_plane_parser.Parse(itsLineInput)!= 0)
			{
				throw  *ground_plane_parser.ItsLastError();
			}
		}
		GetToken(T_RBRACE);
	}

	PeekToken();
	if (GetCurrTokenID() == VSB_EO_CHUNKLOD_TERRAIN)
	{
		working_env_opts.ChunkLODTerrainOn(true);
		GetToken();
		GetToken(T_LBRACK);
		GetToken(T_STRING);
		working_env_opts.ChunkLODTerrainName() = GetCurrTokenStr();
		GetToken(T_RBRACK);
		GetToken(T_LBRACE);
		ParseOnOffState(working_env_opts.mChunkLODWireframeOn, VSB_EO_WIREFRAME);
		GetToken(T_RBRACE);



	}

	ParseOnOffState(working_env_opts.mScalingOn, VSB_EO_SCALING);
	if (working_env_opts.mScalingOn)
	{
		GetToken(T_LBRACE);
		ParseOnOffState(working_env_opts.mScalingSymmetricOn, VSB_EO_SYMMETRIC);

		if (working_env_opts.mScalingSymmetricOn)
			GetToken(VSB_EO_UNIFORM);
		else
			GetToken(VSB_EO_HORIZONTAL);

		GetToken(T_EQ);
		working_env_opts.mHorizontalScale = GetNumericValue();


		if (!working_env_opts.mScalingSymmetricOn)
		{
			GetToken(VSB_EO_VERTICAL);
			GetToken(T_EQ);
			working_env_opts.mVerticalScale = GetNumericValue();
		} else
		{
			working_env_opts.mVerticalScale = working_env_opts.mHorizontalScale;
		}
		GetToken(T_RBRACE);
	}

	ParseOnOffState(working_env_opts.mTrajectoryTraceOn, VSB_EO_TRAJECTORY_TRACES);
	if (working_env_opts.mTrajectoryTraceOn)
	{
		GetToken(T_LBRACE);

		GetToken(VSB_EO_TRAJ_THICKNESS);
		GetToken(T_EQ);
		working_env_opts.mTrajTraceThickness = GetNumericValue();
		
		GetToken(T_RBRACE);
	}

	ParseOnOffState(working_env_opts.mCloudPreDrawModeOn,VSB_EO_PRE_DRAW);
	ParseOnOffState(working_env_opts.mEntityCentricCameraOn,VSB_EO_ENTITY_CENTRIC_CAM);

	GetToken(VSB_EO_HUD);
	GetToken(T_LBRACE);

	float transparency;
	sgVec3 color;

	GetToken(VSB_EO_NORMAL_RGB);
	GetToken(T_EQ);
	GetToken(T_LBRACK);
	color[0] = GetNumericValue();
	GetToken(T_COMMA);
	color[1] = GetNumericValue();
	GetToken(T_COMMA);
	color[2] = GetNumericValue();
	GetToken(T_RBRACK);
	working_env_opts.HudNormalColor(color);

	GetToken(VSB_EO_HILIGHT_RGB);
	GetToken(T_EQ);
	GetToken(T_LBRACK);
	color[0] = GetNumericValue();
	GetToken(T_COMMA);
	color[1] = GetNumericValue();
	GetToken(T_COMMA);
	color[2] = GetNumericValue();
	GetToken(T_RBRACK);
	working_env_opts.HudHilightColor(color);

	PeekToken();
	if (GetCurrTokenID() == VSB_EO_BACKGROUND_RGB)
	{
		working_env_opts.HudBackgroundEnabled(true);
		GetToken(VSB_EO_BACKGROUND_RGB);
		GetToken(T_EQ);
		GetToken(T_LBRACK);
		color[0] = GetNumericValue();
		GetToken(T_COMMA);
		color[1] = GetNumericValue();
		GetToken(T_COMMA);
		color[2] = GetNumericValue();
		GetToken(T_RBRACK);
		working_env_opts.HudBackgroundColor(color);

		GetToken(VSB_EO_TRANSPARENCY);
		GetToken(T_EQ);
		transparency = GetNumericValue();
		working_env_opts.HudBackgroundTransparency(transparency);
	} else
		working_env_opts.HudBackgroundEnabled(false);


	GetToken(T_RBRACE);


	GetToken(T_RBRACE);

	// Finalize
	mrEnvironmentOpts = working_env_opts;

	for (i=0; i<mrEnvironmentOpts.NumCloudLayers(); i++)
		mrEnvironmentOpts.CloudLayer(i)->NeedRebuild(true);

	for (i=0; i<mrEnvironmentOpts.NumGroundPlanes(); i++)
		mrEnvironmentOpts.GroundPlane(i)->NeedRebuild(true);

}

void vsbEnvironmentOptionsParser::ParseOnOffState(bool &onOrOffVar, int id)
{
	int state_table[] = {VSB_EO_OFF, VSB_EO_ON};
	
	GetToken(id);
	GetToken(T_LBRACK);
	GetToken();

	int state = KeywordTableSelection(state_table);
    if (state < 0)
		AbortParse(VSB_EO_ERR_ONOFF_EXPECTED);
	onOrOffVar = state?true:false;

	GetToken(T_RBRACK);
}

/////////////////////////////////////////////////////

vsbEnvironmentOptions::vsbEnvironmentOptions()
{
	SetDefault();
}

vsbEnvironmentOptions::vsbEnvironmentOptions(const vsbEnvironmentOptions &cp)
{
	*this = cp;
}

vsbEnvironmentOptions &vsbEnvironmentOptions::operator = (const vsbEnvironmentOptions& eo)
{
	int i;
	mSkyDomeOn             = eo.SkyDomeOn();
	mSunOn                 = eo.SunOn();
	mCloudOn			   = eo.CloudOn();
	mGroundPlaneOn		   = eo.GroundPlaneOn();
	mWeatherOn             = eo.WeatherOn();
	mScalingOn			   = eo.ScalingOn();
    mTrajectoryTraceOn     = eo.TrajectoryTraceOn();
    mScalingSymmetricOn    = eo.ScalingSymmetricOn();
	mCloudPreDrawModeOn    = eo.CloudPreDrawModeOn();
	mHorizontalScale       = eo.HorizontalScale();
	mVerticalScale		   = eo.VerticalScale();
	mTrajTraceThickness    = eo.TrajTraceThickness();

	mTimeOfDay			   = eo.TimeOfDay();
	mTimeOfYear			   = eo.TimeOfYear();
	mAtmosphericVisibility = eo.AtmosphericVisibility();
	mNumCloudLayers        = eo.NumCloudLayers();
	mNumGroundPlanes       = eo.NumGroundPlanes();
	
	mUpdateRateHZ		   = eo.UpdateRateHZ();	
	mTimeAdvanceRegime     = eo.TimeAdvanceRegime();

	mLowerTimeBound		   = eo.LowerTimeBound();
	mUpperTimeBound		   = eo.UpperTimeBound();
	mTimeStep			   = eo.TimeStep();

	mChunkLODOn		= eo.ChunkLODTerrainOn();
	mChunkLODWireframeOn = eo.ChunkLODWireframeOn();
	mChunkLODPixelError = eo.ChunkLODPixelError();
	if (mChunkLODOn)
		mChunkLODName = eo.mChunkLODName;

	mEntityCentricCameraOn = eo.EntityCentricCameraOn();

	for(i=0; i<mNumCloudLayers; i++)
		mCloudLayers[i] = *const_cast<vsbEnvironmentOptions&>(eo).CloudLayer(i); 

	for(i=0; i<mNumGroundPlanes; i++)
		mGroundPlanes[i] = *const_cast<vsbEnvironmentOptions&>(eo).GroundPlane(i); 

	SortCloudsByAltitude();
	SortGroundPlanesByAltitude();

	mHudBackgroundEnabled = eo.HudBackgroundEnabled();
	mHudBackgroundTransparency = eo.HudBackgroundTransparency();
	HudBackgroundColor(eo.HudBackgroundColor());
	HudHilightColor(eo.HudHilightColor());
	HudNormalColor(eo.HudNormalColor());

	m_htEnabled = eo.HTenabled();

	return *this;
}


void vsbEnvironmentOptions::SetDefault()
{
	mSkyDomeOn             = true;
	mSunOn                 = true;
	mCloudOn			   = true;
	mGroundPlaneOn		   = true;
	mWeatherOn             = true;
	mScalingOn			   = false;	
    mTrajectoryTraceOn     = false;
    mScalingSymmetricOn    = false; 
	mCloudPreDrawModeOn	   = false;	
	mEntityCentricCameraOn  = false;
	


	mTimeOfDay			   = 1.0;
	mTimeOfYear			   = 182.0;
	mAtmosphericVisibility = 40000;
	mNumCloudLayers		   = 0;	
	mNumGroundPlanes	   = 0;	
	mHorizontalScale	   = 1;
	mVerticalScale		   = 1;
	mTrajTraceThickness	   = 1;

	mUpdateRateHZ		   = 30;
	mTimeAdvanceRegime	   = ADVANCE_FIXED_STEP;
	
	mLowerTimeBound		   = 0;
	mUpperTimeBound		   = 60;
	mTimeStep			   = 0.001f;

	mChunkLODOn				= false;
	mChunkLODWireframeOn	= false;
	mChunkLODPixelError		= -1;
	mChunkLODName = "";

	mHudBackgroundEnabled = false;
	mHudBackgroundTransparency = 0.3f;
	sgSetVec3(mHudBackgroundColor, DEFAULT_HUD_BG_R, DEFAULT_HUD_BG_G, DEFAULT_HUD_BG_B);
	sgSetVec3(mHudHilightColor, DEFAULT_HUD_R, DEFAULT_HUD_G, DEFAULT_HUD_B);
	sgSetVec3(mHudNormalColor, DEFAULT_HUD_R, DEFAULT_HUD_G, DEFAULT_HUD_B);

	m_htEnabled = true;
}

void vsbEnvironmentOptions::AddCloudLayer() 
 { 
	 if (mNumCloudLayers >= MAX_CLOUD_LAYERS) return;
	 mCloudLayers[mNumCloudLayers++].SetDefault();
	 SortCloudsByAltitude();
 };
	
void vsbEnvironmentOptions::AddCloudLayer(CloudTypeEnum cloudType, double alt, double thickness, double transition)
{
	if (mNumCloudLayers >= MAX_CLOUD_LAYERS) return;
	 mCloudLayers[mNumCloudLayers++].Set(cloudType, alt, thickness, transition);
	 SortCloudsByAltitude();
}

void vsbEnvironmentOptions::ModifyCloudLayer(int i, CloudTypeEnum cloudType, double alt, double thickness, double transition)
{
	if ( i < 0 || i >= mNumCloudLayers ) return;
	mCloudLayers[i].Set(cloudType, alt, thickness, transition);
	SortCloudsByAltitude();
}

void vsbEnvironmentOptions::RemoveCloudLayer(int i)
{
	if ( i < 0 || i >= mNumCloudLayers ) return;
	mNumCloudLayers--;
	for (; i < mNumCloudLayers; i++)
		mCloudLayers[i] = mCloudLayers[i+1];
	SortCloudsByAltitude();
}

vsbCloudParams *vsbEnvironmentOptions::CloudLayer(int i) 
{
	if ( i < 0 || i >= mNumCloudLayers ) return 0;
	return &mCloudLayers[i];
}

vsbCloudParams *vsbEnvironmentOptions::CloudLayerByOrder(int i) 
{
	if ( i < 0 || i >= mNumCloudLayers ) return 0;
	return &mCloudLayers[mCloudSortIdx[i]];
}

void vsbEnvironmentOptions::TimeOfDay(double tod) 				
{ 
//	while (tod < -0) tod +=24;
	//while (tod > 24) tod -=24;
	mTimeOfDay = fmod(tod,24); 
};

void vsbEnvironmentOptions::TimeOfYear(double toy) 				
{ 
	mTimeOfYear = fmod(toy,365.0); 
};

void vsbEnvironmentOptions::SortCloudsByAltitude()
{
	int		i, tmp;
	bool	swapped = true;
	float	alt[MAX_CLOUD_LAYERS];

	if (mNumCloudLayers <= 0) return;

	// initialise the sort
	for( i = 0; i < mNumCloudLayers; i++)
	{
		mCloudSortIdx[i] = i;
		alt[i] = mCloudLayers[i].Alt();
	}

	if (i >= 2)  // no need to sort if we dont have more than 1
		while  (swapped)
		{
			swapped = false;
			for(int i=0; i < mNumCloudLayers-1; i++)
				if (alt[mCloudSortIdx[i]] > alt[mCloudSortIdx[i+1]]) 
				{
					swapped = true;
					tmp = mCloudSortIdx[i];
					mCloudSortIdx[i] = mCloudSortIdx[i+1];
					mCloudSortIdx[i+1] = tmp;
				}  
		}

}


void vsbEnvironmentOptions::SortGroundPlanesByAltitude()
{
	int		i, tmp;
	bool	swapped = true;
	float	alt[MAX_GROUND_PLANES];

	// initialise the sort
	for( i = 0; i < mNumGroundPlanes; i++)
	{
		mGroundPlaneSortIdx[i] = i;
		alt[i] = mGroundPlanes[i].Alt();
	}

	if (i >= 2)  // no need to sort if we dont have more than 1
		while  (swapped)
		{
			swapped = false;
			for(int i=0; i < mNumGroundPlanes-1; i++)
				if (alt[mGroundPlaneSortIdx[i]] > alt[mGroundPlaneSortIdx[i+1]]) 
				{
					swapped = true;
					tmp = mGroundPlaneSortIdx[i];
					mGroundPlaneSortIdx[i] = mGroundPlaneSortIdx[i+1];
					mGroundPlaneSortIdx[i+1] = tmp;
				}  
		}

}


void vsbEnvironmentOptions::AddGroundPlane()
{
	 if (mNumGroundPlanes >= MAX_GROUND_PLANES || vsbGroundPlaneParams::MaxPlaneTextures() == 0) return;
	 mGroundPlanes[mNumGroundPlanes++].SetDefault();
	 SortGroundPlanesByAltitude();
}

				// Add a new ground plane definition
int vsbEnvironmentOptions::AddGroundPlane(int groundTextureIdx, double alt, vsbDepthTestStyle depthTest)
{
	if (mNumGroundPlanes >= MAX_GROUND_PLANES || 
		groundTextureIdx<0 || 
		groundTextureIdx>=vsbGroundPlaneParams::MaxPlaneTextures() ) return -1;
	 mGroundPlanes[mNumGroundPlanes++].Set(groundTextureIdx, alt, depthTest);
	 SortGroundPlanesByAltitude();
	 return mNumGroundPlanes-1;
}

int vsbEnvironmentOptions::AddGroundPlane(const char *groundTextureLabel, double alt, vsbDepthTestStyle depthTest)
{
	if (!groundTextureLabel || vsbGroundPlaneParams::MaxPlaneTextures() < 1) return -1;
    int texture_index = vsbGroundPlaneParams::TextureList().FindIndexByLabel(string(groundTextureLabel));
	if (texture_index == -1) return -1;
	return AddGroundPlane(texture_index,  alt,  depthTest);
}


int vsbEnvironmentOptions::AddGroundPlane(const string &groundTextureLabel, double alt, vsbDepthTestStyle depthTest )
{
    int texture_index = vsbGroundPlaneParams::TextureList().FindIndexByLabel(string(groundTextureLabel));
	if (texture_index == -1) return -1;
	return AddGroundPlane(texture_index,  alt,  depthTest);
}

void vsbEnvironmentOptions::ModifyGroundPlane(int i, int groundTextureIdx, double alt, vsbDepthTestStyle depthTest )
{
	if ( i < 0 || i >= mNumGroundPlanes ) return;
	mGroundPlanes[i].Set(groundTextureIdx, alt, depthTest);
	SortGroundPlanesByAltitude();
}

void vsbEnvironmentOptions::ModifyGroundPlane(int i, const char *groundTextureLabel, double alt, vsbDepthTestStyle depthTest)
{
	if ( i < 0 || i >= mNumGroundPlanes ) return;
	mGroundPlanes[i].Set(groundTextureLabel, alt, depthTest);
	SortGroundPlanesByAltitude();
}

void vsbEnvironmentOptions::ModifyGroundPlane(int i, string &groundTextureLabel, double alt, vsbDepthTestStyle depthTest)
{
	if ( i < 0 || i >= mNumGroundPlanes ) return;
	mGroundPlanes[i].Set(groundTextureLabel, alt, depthTest);
	SortGroundPlanesByAltitude();
}


void vsbEnvironmentOptions::RemoveGroundPlane(int i)
{
	if ( i < 0 || i >= mNumGroundPlanes ) return;
	mNumGroundPlanes--;
	for (; i < mNumGroundPlanes; i++)
		mGroundPlanes[i] = mGroundPlanes[i+1];
	SortGroundPlanesByAltitude();
}

vsbGroundPlaneParams *vsbEnvironmentOptions::GroundPlane(int i) 
{
	if ( i < 0 || i >= mNumGroundPlanes ) return 0;
	return &mGroundPlanes[i];
}


vsbGroundPlaneParams *vsbEnvironmentOptions::GroundPlaneByOrder(int i) 
{
	if ( i < 0 || i >= mNumGroundPlanes ) return 0;
	return &mGroundPlanes[mGroundPlaneSortIdx[i]];
}


int	 vsbEnvironmentOptions::Read(Parser::TLineInp *tli)
{
	vsbEnvironmentOptionsParser env_parser(*this);
	int err_val = env_parser.Parse(tli);
	if (err_val)
	{
		char buffer[400];
		sprintf(buffer,"While loading Environment Options from file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			"", //parser.CurrentActiveSourceFile().c_str(), 
			env_parser.ItsLastError()->Id(),
			env_parser.ItsLastError()->Desc(),
			env_parser.ItsLastError()->Pos(),
			env_parser.ItsLastError()->Line());
		ulSetError(UL_WARNING, buffer);
	}
	return err_val;
};

void vsbEnvironmentOptions::Write(FILE *f, int indent)
{
	vsbEnvironmentOptionsParser env_parser(*this);
	env_parser.Write(f, indent);
	
};


//-----------------------------------------------------------------------