//////////////////////////////////////////////////////////////////
/*! 
    \file vsbParserHelper.h

	
	\sa vsbParserHelper.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbParserHelper_H
#define _vsbParserHelper_H

#ifndef __cplusplus
# error This file requires C++
#endif  

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _STDIO_
#include <stdio.h>
#define _STDIO_
#endif

#include "sg.h"
#include "parser.h"


class DLL_VSB_API vsbParserHelper
{
public:
	vsbParserHelper(Parser::TParseObjectBase &some_parser);
	virtual ~vsbParserHelper();


	void OpenFile(const std::string &filename);
	void CloseFile();
	

	void WriteSpaces(int n);
	void WriteNewline(int indent=0);
	void WriteString(const std::string &s, int n=-1);
    void WriteName(const std::string &s, int n=-1);
	void WriteKeyWord(int keyword, int n=-1);
	void WriteOnOff(bool onOrOff);
	void WriteValue(double val);
	void WriteVec3(const sgVec3 vec);

	FILE *GetFile()			{return m_F;};
	void SetFile(FILE *f)	{m_F = f;};
private:

	FILE *m_F;
	Parser::TParseObjectBase &m_Parser;
};




#endif