//////////////////////////////////////////////////////////////////
/*! 
    \file vsbRenderOptions.h
    \brief Defines the class vsbRenderOptions
 

	This file defines the vsbRenderOptions class that manages the 
	options and parameters related to rendering and implementation 
	of the EnvironmentManager and in fact any other application module 
	requiring simiar feature control. 
	
	\sa vsbRenderOptions.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbRenderOptions_H
#define _vsbRenderOptions_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif  

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


//! hints for hardware algorithm selection
enum RenderOptionEnum
{
	RenderFastest = GL_FASTEST, 
	RenderNicest  = GL_NICEST , 
	RenderDontCare = GL_DONT_CARE
};



/////////////////////////////////////////////////////////////////
/*! 
   \class vsbRenderOptions vsbRenderOptions.h 	
   \brief Class used to set various global rendering options for 
   the entire system
 

   A global instance of the vsbRenderOptions class manages the 
   options and parameters related to rendering and implementation 
   of the EnvironmentManager and in fact any other application module 
   that requires simiar feature control. 

	\note Can, and may well be expanded in future to encapsulate new 
	parameters.

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////


class DLL_VSB_API vsbRenderOptions
{

private:

	bool				mTextureMappingOn; //!< Texturing status for enabling or disabling texture mapping 
	bool				mFogOn;			   //!< Fog status for enabling or disabling Fog effects	 
	bool				mSolidPolygonsOn;  //!< Wire-frame or solid drawing g
	RenderOptionEnum	mPerspectiveCorrectionHint; //!< OpenGL perspective correciton hint
	RenderOptionEnum	mFogHint;					//!< OpenGL fog hint
	bool				mGLNormalize;				// !< Open GL normalization of surface normals
	bool				mCameraCentricTransforms;   //!< Flag to enable camera centric transformations



public:
						//! Default Constructor
						vsbRenderOptions();

						//! Set the default values for all vsbRenderOptions member variables 
	void				SetDefault();		

						
	bool				TextureMappingOn();				//!< Texture mapping status enquiry 
	void				TextureMappingOn(bool onOrOff);	//!< Texture mapping status setting 

	bool				GLNormalizeOn();				//!< Automatic normalization enquiry	
	void				GLNormalizeOn(bool OnOrOff);	//!< Automatic normalization setting
	
	bool				CameraCentricTransformsOn();				//!< Camera centric transforms flag enquiry
	void				CameraCentricTransformsOn(bool OnOrOff);	//!< Camera centric transforms flag settings
	

	bool				FogOn();						//!< Fog status enquiry 	
	void				FogOn(bool OnOrOff);			//!< Fog status setting 	
				
						
	bool				SolidPolygonsOn();				//!< Solid polygon  rendering enquiry 	
	void				SolidPolygonsOn(bool onOrOff);	//!< Solid polygon  rendering setting 
	
	RenderOptionEnum	PerspectiveCorrectionHint(); 
	void				PerspectiveCorrectionHint(RenderOptionEnum hint); 

						
	RenderOptionEnum	FogHint();						//!< OpenGL Fog Hint status enquiry 	
	void				FogHint(RenderOptionEnum hint); //!< OpenGL Fog Hint status setting
};



inline DLL_VSB_API bool vsbRenderOptions::TextureMappingOn()				
{ 
	return  mTextureMappingOn; 
};

inline DLL_VSB_API void vsbRenderOptions::TextureMappingOn(bool onOrOff)	
{ 
	mTextureMappingOn = onOrOff; 
};


inline DLL_VSB_API bool vsbRenderOptions::GLNormalizeOn()						
{ 
	return  mGLNormalize; 
};

inline DLL_VSB_API void vsbRenderOptions::GLNormalizeOn(bool onOrOff)				
{ 
	mGLNormalize = onOrOff; 
};


inline DLL_VSB_API bool vsbRenderOptions::CameraCentricTransformsOn()						
{ 
	return  mCameraCentricTransforms; 
};

inline DLL_VSB_API void vsbRenderOptions::CameraCentricTransformsOn(bool onOrOff)				
{ 
	mCameraCentricTransforms = onOrOff; 
};

inline DLL_VSB_API bool vsbRenderOptions::FogOn()						
{ 
	return  mFogOn; 
};

inline DLL_VSB_API void vsbRenderOptions::FogOn(bool onOrOff)				
{ 
	mFogOn = onOrOff; 
};

inline DLL_VSB_API bool vsbRenderOptions::SolidPolygonsOn()					
{ 
	return  mSolidPolygonsOn; 
};

inline DLL_VSB_API void	 vsbRenderOptions::SolidPolygonsOn(bool onOrOff)		
{ 
	mSolidPolygonsOn = onOrOff; 
};

inline DLL_VSB_API RenderOptionEnum  vsbRenderOptions::PerspectiveCorrectionHint() 
{ 
	return mPerspectiveCorrectionHint;
};

inline DLL_VSB_API void vsbRenderOptions::PerspectiveCorrectionHint(RenderOptionEnum hint) 
{
	mPerspectiveCorrectionHint = hint;
};

inline	DLL_VSB_API RenderOptionEnum  vsbRenderOptions::FogHint()				
{
	return mFogHint;
};

inline DLL_VSB_API void vsbRenderOptions::FogHint(RenderOptionEnum hint) 
{
	mFogHint = hint;
};


#endif