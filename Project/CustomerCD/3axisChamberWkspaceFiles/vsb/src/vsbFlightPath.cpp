#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
#include <stdio.h>
#include <assert.h>
#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "vsbFlightPath.h"

void vsbFlightPathVertexArray::binarysearch(int &first, int &last, float time)
{
	int mid;
	assert(last>first);
	while (first < last)
	{
		mid = (first+last+1)/2;
		if (time < get(mid)[3])
			last = mid - 1;
		else
			first = mid;
	}
}

bool vsbFlightPathVertexArray::find(int &idx, float t)
{
	if (total == 0) 
		return false;

	int first = 0;
	int last = total - 1;

	binarysearch(first, last, t);
	idx = last;
	if (last >= 0 && t == get(last)[3])
		return true;
	return false;
}	

bool vsbFlightPathVertexArray::find(int &idx, int starting_at, float t)
{
	if (total == 0) 
		return false;

	int first = starting_at;
	int last = total - 1;
	if (first>last) first = last;

	binarysearch(first, last, t);
	idx = last;
	if (last >= 0 && t == get(last)[3])
		return true;
	return false;
}

float *vsbFlightPathVertexArray::find(float t)
{
	int idx;
	if (find(idx, t))
		return get(idx);
	return 0;
}

void   vsbFlightPathVertexArray::add ( sgVec4   thing ) 
{ 
	float time = thing[3];
	float *f;
	if (total == 0)
		raw_add ( (char *) thing ) ; 
	else if (thing[3] > get(total-1)[3])
		raw_add ( (char *) thing ) ; 
	else if (thing[3] < get(0)[3])
	{
		total++;
		sizeChk(total);
		memmove(list+size_of, list, (total-1)*size_of);
		sgCopyVec4((float *)list, thing); 
	} else
	{
		int first = 0;
		int last = total - 1;
		binarysearch(first, last, time);

		assert(last >= 0);

		if (time == (f = get(last))[3]) 
			sgCopyVec4(f, thing);
		else
		{
			//cout << "insert at " << last << "\n";
			total++;
			sizeChk(total);
			memmove(list +(last+2)*size_of, list +(last+1)*size_of,  (total-last-2)*size_of);
			sgCopyVec4((float *)(list +(last+1)*size_of), thing);
		}

	}
} 

void   vsbFlightPathVertexArray::add ( float x, float y, float z, float t )
{
	sgVec4 thing = {x, y, z, t};
	add(thing);
}

void   vsbFlightPathVertexArray::set ( sgVec4   thing, unsigned int n )
{ 
	raw_set ( (char *) thing, n ) ; 
} 


void   vsbFlightPathVertexArray::set ( float x, float y, float z, float t, unsigned int n )
{
	sgVec4 thing = {x, y, z, t};
	set(thing,n);

}


void vsbFlightPathVertexArray::copy( ssgSimpleList *src, int clone_flags )
{
	ssgBase::copy_from ( src, clone_flags ) ;

	delete list ;
	size_of = src -> getSizeOf () ;
	total   = src -> getNum () ;
	limit   = total ;
	list    = new char [ limit * size_of ] ;
	memcpy ( list, src->raw_get ( 0 ), limit * size_of ) ;
}


//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////


vsbFlightPath::vsbFlightPath()
{
	mpFlightPath = new vsbFlightPathVertexArray;
	mpFlightPath->ref();
	Clear();
}

vsbFlightPath::~vsbFlightPath()
{
	ssgDeRefDelete(mpFlightPath);
}

void vsbFlightPath::Clear()
{
	mpFlightPath->removeAll();
}

vsbFlightPath::vsbFlightPath(const vsbFlightPath &src)
{
	mpFlightPath->copy(const_cast<vsbFlightPathVertexArray *>(src.mpFlightPath), 0);
}

vsbFlightPath &vsbFlightPath::operator = (const vsbFlightPath &src)
{
	return *this;
}

void vsbFlightPath::AddPoint(float time, const sgVec3 xyz)
{
}


void vsbFlightPath::AddPoint(float time,  const sgMat4 mat)
{

};

void vsbFlightPath::AddPoint(float time,  const sgCoord coord)
{
};





