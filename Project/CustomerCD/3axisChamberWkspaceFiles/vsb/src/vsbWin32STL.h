#ifndef _vsbWin32STL_H
#define _vsbWin32STL_H

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#if defined(DLL_VSB_EXPORTS) ||  defined(DLL_VSB_IMPORTS) 

#include <string>

namespace std 
{
#pragma warning(disable:4231) /* the extern before template is a non-standard extension */

    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const char, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl operator>>(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl getline(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl getline(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&, const char);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_ostream<char, char_traits<char> >& __cdecl operator<<(
            basic_ostream<char, char_traits<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);

    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const wchar_t, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl operator>>(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl getline(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl getline(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_ostream<wchar_t, char_traits<wchar_t> >& __cdecl operator<<(
            basic_ostream<wchar_t, char_traits<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);

#pragma warning(default:4231) /* restore previous warning */

} // namespace
#endif

#endif