////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbEnvironmentManager.h
    \brief Defines the class vsbEnvironmentManager and its auxillary classes
    
     The class vsbEnvironmentManager is responsible for managing environmental
	 parameters and rendering the scene according to those parameters.

	\sa vsbEnvironmentManager.cpp

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifndef _vsbEnvironmentManager_H
#define _vsbEnvironmentManager_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbEnvironmentOptions_H
#include "vsbEnvironmentOptions.h"
#endif

#ifndef _vsbLightingParams_H
#include "vsbLightingParams.h"
#endif


#ifndef _vsbChunkLODElevationManager_H
#include "vsbChunkLODElevationManager.h"
#endif


class DLL_VSB_API vsbCameraManager;



////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbSceneContext
    \brief Manages the viewport settings, such as viewport geometry

*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbSceneContext
{
private:
	int mViewId;			//!< Viewport ID 
	int mView_W;			//!< Viewport width
	int mView_H;			//!< Viewport height
protected:
					//! Default constructor
					vsbSceneContext()					{};

					//! Virtual destructor
	virtual			~vsbSceneContext()					{};

					//! Register the viewpoer dimensions
	void			ViewportShape(int w, int h)		{mView_W = w; mView_H = h;};

					//! Get the viewport width
	int				ViewportWidth() const			{return mView_W;};

					//! Get the viewport height
	int				ViewportHeight() const			{return mView_H;};
};

////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbEnvironmentManager
    \brief Manages environmental parameters and scene rendering 

     The class vsbEnvironmentManager is responsible for managing environmental
	 parameters and rendering the scene according to those parameters.


*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbEnvironmentManager
{

private:

	vsbChunkLODElevation  mChunkLODElevation;
	vsbEnvironmentOptions *mpEnvironmentOpts;	//!< Pointer to the environment options 
	vsbRenderOptions	  *mpRenderOpts;		//!< Pointer to the render options

	int					mInstanceCount;			//!< Used to ensure only one instance is created	
	
	void				DrawClouds();			//!< Draw the clouds on the current viewport.
	void				DrawGroundPlanes();		//!< Draw the ground planes on the current viewport.

public:

	/*! \brief Constructor

	Constructs an environment manager with the specified render and environment options.
	If not render or environment options are specified, a default instance of vsbEnvironmentOptions
	and vsbRenderOptions is created.

	\param eo Environment options
	\param ro Render options

	*/
	vsbEnvironmentManager(vsbEnvironmentOptions *eo = 0,vsbRenderOptions *ro = 0);


	//! Virtual destructor 
	virtual				~vsbEnvironmentManager();


	//! Get a reference to the environment options 
	vsbEnvironmentOptions  &EnvironmentOpts();

	//! Get a reference to the render options
	vsbRenderOptions		&RenderOpts();

	const vsbLightingParams &LightingParams() const;

	//! Set the environment manager to a new environment options object
	void			    EnvironmentOpts(vsbEnvironmentOptions *eo);

	//! Set the environment manager to a new render options object
	void	   			RenderOpts(vsbRenderOptions *ro);


	//! Set the environment manager to new render and environment options objects
	void				SetOpts( vsbEnvironmentOptions *eo = 0,  vsbRenderOptions *ro = 0);
	

	//! Get the scene root node
	ssgRoot				*SceneRoot() {return mScene;};

	//! Forces the default state on the current OpenGL rendering context
	void			    ForceDefaultGLState();	

	/*! \brief Reshape the viewport
	\param w Viewport width
	\param h Viewport height
	*/
	void			    ReshapeViewport(int w, int h);

	//! Redraw the scene on the current OpenGL rendering context from the specified camera position and orientation
	void				RedrawBuffer(sgdCoord &campos);

	//! Redraw the scene on the current OpenGL rendering context from the specified camera matrix
	void				RedrawBuffer( sgdMat4 &campos);

	//! Add a new ssgEntity node into the scene graph
	void				AddSceneChild(ssgEntity *entity);					   	


	vsbChunkLODElevation &ChunkLODTerrain() { return mChunkLODElevation;};

	void				 SelectChunkLODTerrain(const std::string &name);	

    
	// Drawing utility funcs
private:
	ssgRoot				*mScene; //!< Pointer to the scene graph root node	

	void			    PreDraw(const sgCoord &viewPos); //!< Called before main scene is drawn
	void			    PostDraw();	//!< Called after main scene is drawn



};

#endif