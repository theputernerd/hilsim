//////////////////////////////////////////////////////////////////
/*! 
    \file vsbFollowInertiaCamera.h
    \brief Defines a follow camera with inertia class vsbFollowInertiaCamera
    
     The class vsbFollowInertiaCamera is derrived from vsbFollowCamera, 
     as well as vsbLookAtTransform, implementing  a versatile follow 
     camera object that has inertia. 

	\sa vsbFollowInertiaCamera.cpp

    \author D. Fletcher
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbFollowInertiaCamera_H
#define _vsbFollowInertiaCamera_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbFollowCamera_H
#include "vsbFollowCamera.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

#ifndef mars_vector3H
	#include "mars_vector3.h"
#endif

#ifndef mars_quaternionH
	#include "mars_quaternion.h"
#endif

#ifndef mars_integH
    #include "mars_integ.h"
#endif

class DLL_VSB_API vsbFollowInertiaCameraParser;

//////////////////////////////////////////////////////////////////
/*! 
	\class vsbFollowInertiaCamera
	\brief Advanced entity following and tracking camera that has
		   a second order response giving it the appearance of 
		   having inertia. 
    
*/
/////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbFollowInertiaCamera: public vsbFollowCamera, private Mars::TIntegrator::TEqnBase
{
friend class DLL_VSB_API vsbFollowInertiaCameraParser;
public:

    /*! \brief Constructor 

    Initialises the camera with its parent camera manager
    \param cm A pointer to a valid parent camera manager

    A name for the camera is automatically generated
    */
	vsbFollowInertiaCamera(vsbCameraManager *cm);

    /*! \brief Constructor

        Initialises the camera with its parent camera manager, and a name for the camera
        \param cm A pointer to a valid parent camera manager
        \param name A name for the camera
        \param postfix A flag to indicate whether the name should be postfixed by a 
        unique camera number
    */    
    vsbFollowInertiaCamera(vsbCameraManager *cm, const std::string &name, bool postfixed = false);

    //! Virtual Destructor 
	virtual ~vsbFollowInertiaCamera();

	virtual vsbBaseCamera *Clone();

    vsbFollowInertiaCamera  &operator=(const vsbFollowInertiaCamera  &from);

    virtual const std::string & ClassName() const; // { return mClassName; };
 
    //! Set gain on position error feedback
	void SetPositionGain(const double& gain) { d_APosGain = gain; }

    //! Get gain on position error feedback
	double GetPositionGain() { return d_APosGain; }

    //! Set gain on angle error feedback
	void SetAngleGain(const double& gain) { d_AAngleGain = gain; }

    //! Get gain on angle error feedback
	double GetAngleGain() { return d_AAngleGain; }

    //! Set natural frequency (radians)
	void SetOmegaN(const double& omegaN) { d_AOmegaN = omegaN; }

    //! Get natural frequency (radians)
	double GetOmegaN() { return d_AOmegaN; }

    //! Set damping ratio
	void SetZeta(const double& zeta) { d_AZeta = zeta; }

    //! Get damping ratio
	double GetZeta() { return d_AZeta; }

    /*! \brief Update camera matrix 
    
    This function generates the computed matrix, based on entity geometry
    as well as option flags, followed by the camera matrix based on the computed and 
    relative geometry matrix, for the specified time
    \param time Time stampe for the next frame update

    \sa vsbBaseCamera::UpdateCameraMatrix
    */
	void UpdateCameraMatrix(float time);
	
	int  Write(FILE *f, int indent);
	int	 Parse(Parser::TLineInp *tli);

private:
	//! Common function called by both constructors
	void Setup();

	// attributes
	double d_APosGain;
	double d_AAngleGain;
	double d_AOmegaN;
	double d_AZeta;

	// state variables
	Mars::TVector3 d_camPosE;
	Mars::TVector3 d_camVelE;
	Mars::TQuaternion d_camQuatE;
	Mars::TVector3 d_camAngVelE;

	// integrator-related
	Mars::TIntegrator* d_integ;
	double d_lastUpdateTime;
	Mars::TVector3 d_camPosCmdE;  // stored here to be visable to calc func, otherwise would be local
	Mars::TQuaternion d_camQuatCmdE;  // stored here to be visable to calc func, otherwise would be local
    virtual void CalcDerivsAndDirectOutputs(const double &t);
    virtual void TransformIntegOutputs(const double &t);

};



class DLL_VSB_API vsbFollowInertiaCameraParser: public Parser::TParseObjectBase
{
public:
	vsbFollowInertiaCameraParser(vsbFollowInertiaCamera &sc);
	virtual ~vsbFollowInertiaCameraParser();

	int	 Parse(Parser::TLineInp *tli, bool skipHeader = false);
	int  Write(FILE *f, int indent);

private:
    vsbParserHelper *mpOutput;
	bool mSkipHeader;

	vsbFollowInertiaCamera &mrFollowInertiaCamera;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};

#endif