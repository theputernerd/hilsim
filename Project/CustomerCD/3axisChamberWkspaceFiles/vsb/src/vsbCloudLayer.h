//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCloudLayer.h
    \brief Defines the class vsbCloudLayer
 
	This file defines a vsbCloudLayer class, instances of which manage
	the construction, manipulation and drawing of a cloud layer. It builds 
	an ssgRoot node with all the apropriate children including textures, 
	and cloud layer geometry. It further provides functions to allow the 
	positioning and colouration of the cloud according to the next frame's 
	requirements.
	
	\sa vsbCloudLayer.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbCloudLayer_H_
#define _vsbCloudLayer_H_

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbCloudParams_H
#include "vsbCloudParams.h"
#endif


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbCloudLayer
    \brief Cloud Layer Class
 
	The vsbCloudLayer class manages the construction, manipulation and 
    drawing of a cloud layer. It builds	an ssgRoot node with all the 
    apropriate children including textures, and cloud layer geometry. 
    It further provides functions to allow the positioning and colouration 
    of the cloud according to the next frame's  requirements.

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbCloudLayer : public vsbCloudParams 
{

private:

					 
    ssgRoot          *mpCloudLayerRoot;  //!< The root node of the cloud of the cloud layer

					 
    ssgTransform     *mpCloudLayerXfm;	//!< The cloud layer's transformation	

    ssgColourArray   *mpColorArr;		//!< Cloud layer colour array
    ssgVertexArray   *mpVertexArr;		//!< Cloud layer vertex array
    ssgTexCoordArray *mpTextureArr;		//!< Cloud layer  texture array

					  /* \brief Storage for texture coordinate origin

					   Texture coordinate origin (which is randomly purturbed so that 
                       different layers of the same cloud type are randomly offset). 
                       Also critical for the "moving" effect by altering texture 
                       coordinates
					  */	
	
	sgVec2            mInitialTextureCoords[6];

					  // a facor by which cloud textures are
					  // stretched over the cloud plane

 	
					  
	static char		  *mspCloudFiles[]; //!< Static list of all cloud texture file names	 

	
					  
	static ssgSimpleState *msCloudState[MAX_CLOUD_TYPES];
							//!< Static list of states for all cloud types  which are shared by all cloud instances


	
public:

    //! Constructor
    vsbCloudLayer( void );

    //! Virtual destructor
    virtual ~vsbCloudLayer( void );

	/*! \brief Comparison operator 

	   Allows the testing equality of cloud parameters with current cloud layer's parameters
	   \param cp vsbCloudParams operand
	*/
	bool operator == (const vsbCloudParams &cp); 

	/*! \brief Assignment operator
	
	  Assign new cloud parameters to cloud layer, rebuilding the cloud 
	  immediately after assignment, and clearing the modified flag of the
	  vsbCloudParams parameter.
	  \param cp vsbCloudParams operand to copy from
    */
	vsbCloudLayer &operator = (const vsbCloudParams &cp); 

	//! Bestroy the cloud geometry 
	void Destroy();

    //! Build the cloud object, constructing the geometry, and mapping textures
	void Build();

	/*! \brief Build the cloud object, constructing the geometry, and mapping textures
	
	As above but overriding the previously set values with new ones
	\param mSize      The size of the cloud, from eye point to furthest point along a 
				      major axis (X or Y)
	\param asl        The altitude above sea level of the cloud layer
	\param thickness  The thickness of the cloud layer
	\param transition The thickness of the transitional area above & below a cloud
	\param cloudType  The cloud type for this layer	
	*/
    void Build( double mSize, double asl, double thickness,
		double transition, CloudTypeEnum cloudType );


	/*! \brief Update the geometry parameters fot the cloud layer.

	If this cloud layer is already built, but the texture has not been changed,
	updates the layer's texture coordinates otherwise build the cloud layer from
	scratch
	*/
	void UpdateGeometry();

	//! Test to see whether cloud geometry has been built

	bool IsBuilt() const { return mpCloudLayerRoot != 0; };

	/*! \brief Repaint the cloud colors 
	
     Repaint the cloud colors based on current value of sun_angle,
     sky, and fog colors.  This updates the color arrays for
     ssgVtxTable. 
	 \param fog_color Determines the colour of the cloud
	 which should blend into the skydomes fog_colour.
	*/
    
    bool Repaint( const sgVec3 fog_color );

	/*!\brief Reposition the cloud layer 
	  Reposition the cloud layer to be centred directly over
	  the vertical line through the specified position (which
	  should be the current camera position) and compensate 
      for this movement in the texture coordinates.
	  \param camerapos The camera position

	*/
    bool Reposition( const sgVec3 camerapos );

    //! draw the cloud layer
    void Draw();

	//! Retreive the texture file name for the given cloud type
	static char *CloudFileName(CloudTypeEnum cloudType);

private:

	//! Utility function to create cloud states
	ssgSimpleState *MakeCloud(CloudTypeEnum cloudType);

};


#endif // _CloudLayer_H_
