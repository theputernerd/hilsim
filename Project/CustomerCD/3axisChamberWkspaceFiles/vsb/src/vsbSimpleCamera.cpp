//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSimpleCamera.cpp
    \brief Defines the class vsbSimpleCamera
    
     The class vsbSimpleCamera is derrived from vsbBaseCamera, 
     implementing  a simple static camera object. This camera object
     is the default camera of the vsbCameraManager.

	\sa vsbSimpleCamera.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>

#include "ssg.h"
#include "vsbBaseCamera.h"
#include "vsbSimpleCamera.h"
#include "vsbViewPortContext.h"
#include "parser_perror.h"

using namespace std;
using namespace Parser;

void vsbSimpleCamera::Setup()
{
	FOV(60);
	NearClip(1.0f);
	FarClip(80000.0f);
	RelativeGeom(true);
	HeadTrackingEnabled(false);
	sgdMakeIdentMat4 ( mCamMatrix );
	sgdMakeIdentMat4 ( mComputedMatrix );
	sgdMakeIdentMat4 ( mRelativeGeometryMatrix );
}


vsbSimpleCamera::vsbSimpleCamera(vsbCameraManager *cm, const string &name, bool postfixed) : 
vsbBaseCamera(cm, name, postfixed), vsbLookAtTransform(cm)
{
	Setup();
}


vsbSimpleCamera::vsbSimpleCamera(vsbCameraManager *cm) : 
vsbBaseCamera(cm,"vsbSimpleCamera",true), vsbLookAtTransform(cm)
{
	Setup();
}

vsbSimpleCamera::~vsbSimpleCamera()
{
}

vsbBaseCamera *vsbSimpleCamera::Clone() 
{
	string name = Name();
	name+="-";
	vsbSimpleCamera *newcam = new vsbSimpleCamera(mpCameraManager, name, true);
	*newcam = *this;
	return newcam;
}

const std::string & vsbSimpleCamera::ClassName() const
{
    static string class_name = "vsbSimpleCamera";
    return class_name;
} 

void vsbSimpleCamera::UpdateCameraMatrix(float time)
{
	static sgdCoord cam_coord;
	sgdSetCoord (&cam_coord, mComputedMatrix);

//	sgCopyMat4(mCamMatrix,mComputedMatrix);

	sgdCopyMat4(mCamMatrix,MakeLookAtTargetMatrix(cam_coord));

	if (RelativeGeom())
		sgdPreMultMat4 ( mCamMatrix, mRelativeGeometryMatrix);

	if (HeadTrackingEnabled())
	{
		sgdMat4 headMatrix;
		sgdMakeRotMat4  ( headMatrix, HeadOrientation()[0], HeadOrientation()[1],HeadOrientation()[2] );
		sgdPreMultMat4 ( mCamMatrix, headMatrix);

	}

	// calculate offset from camera to origin
	sgdCopyVec3(vsbBaseCamera::CameraToOriginDelta(),mCamMatrix[3]);
	vsbBaseCamera::CameraToOriginDelta()[0] = -vsbBaseCamera::CameraToOriginDelta()[0];
	vsbBaseCamera::CameraToOriginDelta()[1] = -vsbBaseCamera::CameraToOriginDelta()[1];
	vsbBaseCamera::CameraToOriginDelta()[2] = -vsbBaseCamera::CameraToOriginDelta()[2];

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		mCamMatrix[3][0] = mCamMatrix[3][1] = mCamMatrix[3][2] =0;
	}
}

void vsbSimpleCamera::PositionCamera(float x, float y, float z, float h , float p, float r)
{
	sgdCoord coord;
	coord.xyz[0] = x; coord.xyz[1] = y; coord.xyz[2] = z;
	coord.hpr[0] = h; coord.hpr[1] = p; coord.hpr[2] = r;
	vsbToScaledVSBCoords(coord); 

	sgdMakeCoordMat4(mComputedMatrix, &coord);
}

vsbSimpleCamera &vsbSimpleCamera::operator=(const vsbSimpleCamera  &from)
{
	vsbBaseCamera::operator=(from);
	vsbLookAtTransform::operator=(from);
	return *this;
}

int  vsbSimpleCamera::Write(FILE *f, int indent)
{
    vsbSimpleCameraParser cam_parser(*this);
    return cam_parser.Write(f,indent);
}


int	 vsbSimpleCamera::Parse(Parser::TLineInp *tli)
{
	mParseError.Clear();
	vsbSimpleCameraParser cam_parser(*this);
	int result = cam_parser.Parse(tli, true );
	if (result)
	{
		mParseError.Set(*cam_parser.ItsLastError());
	}
	return result;
}

///////////////////////////////////////////////////////////

char *vsbSimpleCameraParser::mpKeyWords[] =
{
	"ON",
    "OFF",
    "CAMERA",
	"LOOK_AT_ENTITY",
	0
};

enum {
    VSB_SCAM_ON,
    VSB_SCAM_OFF,
    VSB_SCAM_CAMERA,
	VSB_SCAM_LOOK_AT,
};


char *vsbSimpleCameraParser::mpErrorStrings[] =
{
	"",
	"Either \"ON\" or \"OFF\" keyword expected.",
	0
};

enum {
	VSB_SCAM_ERR_UNUSED,
	VSB_SCAM_ERR_ONOFF_EXPECTED,
};

vsbSimpleCameraParser::vsbSimpleCameraParser(vsbSimpleCamera &sc):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrSimpleCamera(sc) 
{
      mpOutput = new vsbParserHelper(*this);
}

vsbSimpleCameraParser::~vsbSimpleCameraParser()
{
      delete mpOutput;
}


void vsbSimpleCameraParser::WriteOnOff(bool onOrOff)
{
	FILE *f = mpOutput->GetFile();
	if (f)
		fprintf(f,"(%s)",(onOrOff?"ON":"OFF"));
}


int	 vsbSimpleCameraParser::Parse(TLineInp *tli,bool skipHeader )
{
	mSkipHeader = skipHeader;
	return ParseSource(tli, true);
}

int vsbSimpleCameraParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

    mpOutput->WriteSpaces(indent);
    mpOutput->WriteKeyWord(VSB_SCAM_CAMERA);
	mpOutput->WriteKeyWord(T_LBRACK);
	mpOutput->WriteName(mrSimpleCamera.ClassName());
	mpOutput->WriteKeyWord(T_RBRACK);

    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;



    vsbBaseCameraParser base_camera_parser(mrSimpleCamera);
    base_camera_parser.Write(file, indent);

    int count = mrSimpleCamera.CountTargetEntities();

    if ( count > 0)
    {
        mpOutput->WriteKeyWord(VSB_SCAM_LOOK_AT);
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_LBRACE);
        indent+=3;
        mpOutput->WriteNewline(indent);

        for (int i=0; i<count; i++)
        {
            if (i)
            {
                mpOutput->WriteKeyWord(T_COMMA);
                mpOutput->WriteNewline(indent);
            }
            mpOutput->WriteName(mrSimpleCamera.GetTargetEntity(i).c_str());
        }

        indent-=3;
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_RBRACE);
    }

    indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);

	return 1;
}

void vsbSimpleCameraParser::ParseFunc()
{
   	vsbBaseCameraParser base_camera_parser(mrSimpleCamera);

	char class_name[100],entity_name_buff[100];

	if (!mSkipHeader)
	{
		GetToken(VSB_SCAM_CAMERA);
		GetToken(T_LBRACK);
		GetString(class_name);
		GetToken(T_RBRACK);
		GetToken(T_LBRACE);
	}

	int status = base_camera_parser.Parse(ParserLineInp());
	if (status)
	{
		AbortParse(*base_camera_parser.ItsLastError());
	}

	mrSimpleCamera.ClearTargetEntities();

	PeekToken();
	if (GetCurrTokenID() == VSB_SCAM_LOOK_AT)
	{
		GetToken(VSB_SCAM_LOOK_AT);
		GetToken(T_LBRACE);
		PeekToken();
		while (GetCurrTokenID() == T_STRING)
		{
			GetString(entity_name_buff);
			mrSimpleCamera.AddTargetEntity(entity_name_buff);
			PeekToken();
			if (GetCurrTokenID() == T_COMMA)
			{
				GetToken();
				PeekToken();
				GetToken(T_STRING);
				ReplaceToken();
			}
		}
		GetToken(T_RBRACE);
	}


	GetToken(T_RBRACE);


}	

