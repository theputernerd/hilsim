//////////////////////////////////////////////////////////////////
/*! 
    \file vsbLookAtTransform.h
	\brief Defines a class that generates a matrix orienting the camera
	to look at a designated target

    A vsbLookAtTransform instance, given an initial, camera position and 
	orientation in space, and a target or list of targets, can 
	generate a matrix representing a look-at vector from the camera 
	to the highest priority target in terms of a viewpoint orientation 
	matrix
 

	\sa vsbLookAtTransform.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbLookAtTransform_H
#define _vsbLookAtTransform_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


class DLL_VSB_API vsbCameraManager;
class DLL_VSB_API vsbBaseViewEntity;


class _vsbLookAtTransform;


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbLookAtTransform

	\brief Generates a matrix orienting the camera to look at a designated 
	target 

    Given an initial, camera position and orientation in space and a target 
	or list of targets, will generate a matrix representing a look-at vector 
	from the camera to the highest priority target in terms of a viewpoint 
	orientation matrix.
 
	If there is a list of targets provided the first one on the list that 
	is active and visible will be the subject target the.

	The primary member function is MakeLookAtTargetMatrix, which perfomrs the 
	matrix generation.

	\note Algorithmically, the vector to the target target and the up vector
	cannot be collinear. (This condition is automatically compensated for
	by incrementing the euler pitch angle of the source camera position by 1 
	degree - this is crude, but general.



    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbLookAtTransform
{

public:
					/*! \brief Constructor
					  Initialises the object
					  \param cm A pointer to a valid parent camera manager
					*/
					vsbLookAtTransform(vsbCameraManager *cm);
	
					/*! \brief Destructor
					*/
	virtual			~vsbLookAtTransform();

	vsbLookAtTransform &operator=(const vsbLookAtTransform &from);
	
					/*! \brief Create the look at matrix for the camera
					\sa The vsbLookAtTransform class description 
					*/
	sgdMat4 &	    MakeLookAtTargetMatrix(const sgdCoord &camOrigin);
	
					/*! \brief Add target to the list of designated camera look-at targets
					
					  \param name The entity name of the target to add
					  \param position The position in the camera look-at target priority list. The default
					  is -1 indicating that it is to be placed last on the list.
					  If an invalid position is provided, the darget is inserted at the end of the list
					*/
    void			AddTargetEntity(const std::string &name, int position = -1);
	
	
					/*! \brief Get the name of the target at the specified position on the designated
					camera look-at target list
					
					  \param position The position in the camera look-at target priority list.
					  If the position is invalid an empty string is returned.
								  
					*/
    const std::string&	GetTargetEntity(int position = 0) const;
	
	
					/*! \brief Remove the named entity from the camera look-at target priority list.
					
					  \param name The name of the entity to remove. If an invalid name is provided there is no action taken.
					*/
    void			RemoveTargetEntity(const std::string &name);

					/*! \brief Remove the indexed entity from the camera look-at target priority list.
					
					  \param position The position at which to remove an entity from. If an invalid position
							 is supplied, no action is taken.
					*/
	void			RemoveTargetEntity(int position);

					//! Clear the the camera look-at target priority list.
	void			ClearTargetEntities();

    int             CountTargetEntities();
	
protected:
	
						/*! \brief Find the first entity in the camera look-at target priority list that is Active
						\sa vsbBaseViewEntity::IsActive
						*/
	vsbBaseViewEntity *FirstActiveTargetEntity();

						/*! \brief Find the first entity in the camera look-at target priority list that is Visible
						\sa vsbBaseViewEntity::IsVisible
						*/
	vsbBaseViewEntity *FirstVisibleTargetEntity();
	
private:

	_vsbLookAtTransform *mpPrivateData; //! Private local data
	
};



#endif //_vsbLookAtTransform_H