
#include <string>

#include "vsbParserHelper.h"

using namespace std;
using namespace Parser;

void vsbParserHelper::OpenFile(const string &filename)
{
	m_F = fopen(filename.c_str(),"wt");
}

void vsbParserHelper::CloseFile()
{
	if (m_F) 
		fclose(m_F);
	m_F = NULL;
}


vsbParserHelper::vsbParserHelper(TParseObjectBase &some_parser):
m_Parser(some_parser)
{
}

vsbParserHelper::~vsbParserHelper()
{
}


void vsbParserHelper::WriteSpaces(int n)
{
	if (m_F)
		for (int i=0; i<n; i++)
			fprintf(m_F,"%c",' ');
}

void vsbParserHelper::WriteNewline(int indent)
{
	if (m_F)
	{
		WriteString("\n");
		if (indent)
			WriteSpaces(indent);
	}
}

void vsbParserHelper::WriteString(const string &s, int n)
{
	if (m_F)
	{
		fprintf(m_F,"%s",s.c_str());
		if (n>0)
		{
			n -= s.length();
			if (n>0) WriteSpaces(n);
		}
	}
}

void vsbParserHelper::WriteName(const string &s, int n)
{
	if (m_F)
	{
		fprintf(m_F,"\"%s\"",s.c_str());
		if (n>0)
		{
			n -= s.length();
			if (n>0) WriteSpaces(n);
		}
	}
}

void vsbParserHelper::WriteKeyWord(int keyword, int n)
{
	if (m_F)
		WriteString(m_Parser.KeyWord(keyword),n);
}


void vsbParserHelper::WriteValue(double val)
{
	if (m_F)
		fprintf(m_F," = %lg",val);
}

void vsbParserHelper::WriteVec3(const sgVec3 vec)
{
	if (m_F)
		fprintf(m_F," = (%lg, %lg, %lg)",vec[0],vec[1],vec[2]);
}

void vsbParserHelper::WriteOnOff(bool onOrOff)
{
	if (m_F)
		fprintf(m_F," = %s",onOrOff?"ON":"OFF");

}