//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCloudLayer.cpp
    \brief Implements the class vsbCloudLayer
 
	This file implements a vsbCloudLayer class, instances of which manage
	the construction, manipulation and drawing of a cloud layer. It builds 
	an ssgRoot node with all the apropriate children including textures, 
	and cloud layer geometry. It further provides functions to allow the 
	positioning and colouration of the cloud according to the next frame's 
	requirements.
	
	\sa vsbCloudLayer.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "sg.h"
#include "ssg.h"

#include "vsbCloudLayer.h"
#include "vsbRenderOptions.h"
#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentOptions.h"

float rnd()
{
	static bool first_time = true;

#if defined(HAVE_RAND) || defined(WIN32) 
    return ((rand() & 0xFFFF)/65536.0);
#else
    return ((random() & 0xFFFF)/65536.0);
#endif                                       
}



#define CLOUD_SCALE_FACTOR 8000.0f

ssgSimpleState *vsbCloudLayer::msCloudState[MAX_CLOUD_TYPES] = 
{
	0, 0, 0, 0, 0
};

char* vsbCloudLayer::mspCloudFiles[] =
{
	"overcast.rgba",
	"mostlycloudy.rgba",
	"moderatelycloudy.rgba",
    "mostlysunny.rgba",
    "cirrus.rgba",
};


char *vsbCloudLayer::CloudFileName(CloudTypeEnum cloudType) 
{
	return mspCloudFiles[cloudType];
}


// Constructor
vsbCloudLayer::vsbCloudLayer( void ) 
{
	mpCloudLayerRoot		= 0; // essential initialisation

	mpCloudLayerRoot		= 0; // remainig are for safety
    mpCloudLayerXfm			= 0;

    mpColorArr				= 0; 
    mpVertexArr				= 0;
    mpTextureArr			= 0;
	
	vsbCloudParams::SetDefault();
}


// Destructor
vsbCloudLayer::~vsbCloudLayer( void ) 
{
	Destroy();
}


bool vsbCloudLayer::operator == (const vsbCloudParams &cp) 
{
    vsbCloudParams &this_cp = *(vsbCloudParams *)this;
	return this_cp == cp;
};

vsbCloudLayer &vsbCloudLayer::operator = (const vsbCloudParams &cp) 
{
    vsbCloudParams &this_cp = *(vsbCloudParams *)this;
	
	this_cp = cp;

	if (this_cp.NeedRebuild())
		Destroy();

	if (!IsBuilt()) 
		Build();

	else if (this_cp.IsModified())
		UpdateGeometry();

	this_cp.ClearModified();
	const_cast<vsbCloudParams &>(cp).ClearModified();

	return *this;
};




void vsbCloudLayer::Destroy()
{
	if (!IsBuilt()) return;

	// all other ssgBase derrived dynamic members are deleted automatically, 
	// by the reference counting mechanism.. except for state which
	// for state which is assumed to be managed externally
	// see note about reference counting above.

	if (mpCloudLayerRoot)
    {
		delete mpCloudLayerRoot;
		mpCloudLayerRoot = 0;
	}

	// Clean up any cloud states that are no longer required

	if (msCloudState[CloudType()]!=0)
	{
		if  (msCloudState[CloudType()]->getRef() == 1)
		{
			// this was the last cloud to reference this state so lets
			// remove it from the cloud state list to save on texture
			// memory.
		   ssgDeRefDelete(msCloudState[CloudType()]);
		   msCloudState[CloudType()] = 0;
        } 
	}
}


static int cloudPreDraw(ssgEntity *)
{
  return 1;
}

static int cloudPostDraw(ssgEntity *)
{
   return 1;
}

void vsbCloudLayer::Build()
{
	// Cloud layer vertex and texture 
	// arrays

	if (IsBuilt()) return;


	if (msCloudState[CloudType()] == 0)
	{ 
		msCloudState[CloudType()] = MakeCloud(CloudType());
		msCloudState[CloudType()]->ref(); // just so that it doesnt aotomatically
										  // get deleted with the last cloud root	
	}

	// tile size is the size in actual world units, of the
	// texture tile that is repeated

	float tile_size   = Length() * 2 / Scale();

    // build the cloud layer

	mpColorArr		  = new ssgColourArray( 6 );
    mpVertexArr		  = new ssgVertexArray( 6 );
    mpTextureArr      = new ssgTexCoordArray( 6 );
    
	// Default cloud layer colour

	sgVec4 colour;
    sgSetVec4( colour, 1.0f, 1.0f, 1.0f, 1.0f );
 
	// randomize the wrap offset of the cloud texture
	// tiles so that overlapping cloud layers of the 
	// same cloud types appear offset
	
	sgVec3 vertex; // temporary sky corner vertex
    sgVec2 base;   // texture base coordinate

    sgSetVec2( base, rnd(), rnd() );

	sgSetVec2( mInitialTextureCoords[0], base[0] + tile_size/2, base[1] + tile_size/2);
	mpTextureArr->add( mInitialTextureCoords[0] );
	sgSetVec3( vertex, 0.0f, 0.0f, 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );


	sgCopyVec2( mInitialTextureCoords[1], base );
	mpTextureArr->add( mInitialTextureCoords[1] );
	sgSetVec3( vertex, -Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );

      
    sgSetVec2( mInitialTextureCoords[2], base[0] + tile_size, base[1] );
    mpTextureArr->add( mInitialTextureCoords[2] );
	sgSetVec3( vertex, Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );

    sgSetVec2( mInitialTextureCoords[3], base[0] + tile_size, base[1] + tile_size );
    mpTextureArr->add( mInitialTextureCoords[3] );
	sgSetVec3( vertex, Length(), Length(), 0.0f );
    mpVertexArr->add( vertex );
    mpColorArr->add( colour );   
	
    sgSetVec2( mInitialTextureCoords[4], base[0], base[1] + tile_size );
    mpTextureArr->add( mInitialTextureCoords[4] );
	sgSetVec3( vertex, -Length(), Length(), 0.0f );
    mpVertexArr->add( vertex );
    mpColorArr->add( colour );

	sgCopyVec2( mInitialTextureCoords[5], base );
	mpTextureArr->add( mInitialTextureCoords[5] );
	sgSetVec3( vertex, -Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );

	// create the tri-strip leaf node for the cloud square

    ssgLeaf *layerGeometry = new ssgVtxTable ( GL_TRIANGLE_FAN, mpVertexArr, 0, mpTextureArr, mpColorArr );

//	layerGeometry->setCallback( SSG_CALLBACK_PREDRAW, cloudPreDraw );
//	layerGeometry->setCallback( SSG_CALLBACK_POSTDRAW, cloudPostDraw );

	// give it a state

    layerGeometry->setState( msCloudState[CloudType()] );

    // force a repaint of the moon colors with arbitrary defaults
    Repaint( colour );

    mpCloudLayerXfm = new ssgTransform;

    mpCloudLayerXfm->addKid( layerGeometry );

    mpCloudLayerRoot = new ssgRoot;
    mpCloudLayerRoot->addKid( mpCloudLayerXfm );
}


// build the cloud object

void vsbCloudLayer::Build( double size, double asl, double thickness,
			  double transition, CloudTypeEnum cloudType )
{

	if (IsBuilt()) Destroy();

	CloudType(cloudType);

	// Override the cloud parameters

    Alt(asl);				// altitude of cloud layer
    Thickness(thickness);	// thickness of cloud layer
    Transition(transition);	// transition value 
    Length(size);			// world size of cloud object

	Build();	
}

void vsbCloudLayer::UpdateGeometry()
{
	if (!IsBuilt()) 
	{
		Build();
	} else 
	{
		float *vertex, *textvert;
		sgVec2 base;   // texture base coordinate

		// tile size is the size in actual world units, of the
		// texture tile that is repeated

		float tile_size   = Length() * 2 / Scale();

sgCopyVec2(base, mInitialTextureCoords[1]);


		//-----------------------------
		textvert = mpTextureArr->get(0);
		sgSetVec2( mInitialTextureCoords[0], base[0] + tile_size/2, base[1] + tile_size/2 );
		sgCopyVec2( textvert, mInitialTextureCoords[2]);
		//-----------------------------

		textvert = mpTextureArr->get(1);
		vertex = mpVertexArr->get(1);
		sgCopyVec2( textvert, mInitialTextureCoords[1]);
		sgSetVec3( vertex, -Length(), -Length(), 0.0f );
		//------------------------------

		textvert = mpTextureArr->get(2);
		vertex = mpVertexArr->get(2);
		sgSetVec2( mInitialTextureCoords[2], base[0] + tile_size, base[1] );
		sgCopyVec2( textvert, mInitialTextureCoords[2]);
		sgSetVec3( vertex, Length(), -Length(), 0.0f );
		
		textvert = mpTextureArr->get(3);
		vertex = mpVertexArr->get(3);
		sgSetVec2( mInitialTextureCoords[3], base[0] + tile_size, base[1] + tile_size );
		sgCopyVec2( textvert, mInitialTextureCoords[3]);
		sgSetVec3( vertex, Length(), Length(), 0.0f );

		textvert = mpTextureArr->get(4);
		vertex = mpVertexArr->get(4);
		sgSetVec2( mInitialTextureCoords[4], base[0], base[1] + tile_size );
		sgCopyVec2( textvert, mInitialTextureCoords[4]);
		sgSetVec3( vertex, -Length(), Length(), 0.0f );


		textvert = mpTextureArr->get(5);
		vertex = mpVertexArr->get(5);
		sgCopyVec2( textvert, mInitialTextureCoords[5]);
		sgSetVec3( vertex, -Length(), -Length(), 0.0f );
		//------------------------------
	}
}

///////////////////////////////////////////////////////////////
// Repaint
//
// repaint the cloud layer colors given the fog colour
//
bool vsbCloudLayer::Repaint( const sgVec3 fog_colour ) 
{
	// check to see this cloud layer is built
	if (!mpCloudLayerRoot)
		return false;

    float *colour;

    for ( int i = 0; i < 4; ++i ) 
	{
		colour = mpColorArr->get( i );
		sgCopyVec4( colour, fog_colour );
    }

    return true;
}

///////////////////////////////////////////////////////////////
// Reposition
//
// reposition the cloud layer at the specified origin 
// which generally would be the camera coordinates for
// the given view. The result being that although the 
// sky is centred above the cameras world-up vector, it
// appears to be fixed relative to the ground

bool vsbCloudLayer::Reposition( const sgVec3 p )
{
	// check to see this cloud layer is built
	if (!mpCloudLayerRoot)
		return false;

	// check to see this cloud layer is built

    sgMat4 T1;

	// tile size is the size in actual world units, of the
	// texture tile that is repeated

	float tile_size = Length() * 2 / Scale();

	// force the origin of the cloud at the origin of the
	// camera, but at the cloud altitude and build the 
	// transformation matrix

	//p[2] = mAlt;

    sgMakeTransMat4( T1, p );
	T1[3][2] = mAlt*vsbViewportContext::EnvironmentOpts().VerticalScale();

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		T1[3][2] +=vsbBaseCamera::CameraToOriginDelta()[2];
	}
	
	// set the position of the cloud from the transformation
	// matrix T1

 //   sgCoord cloud_layer_pos;
 //   sgSetCoord( &cloud_layer_pos, T1 );
    mpCloudLayerXfm->setTransform( T1 );	
	
	// to give the appearance of motion, whilst keeping the
	//cloud layer centred about  the camera up vector, we 
	// offset the start position of the texture within each 
	// tile knowing that it will wrap and match up with 
	// adjacent and similarly wrapped tiles

	// calculate the fraction of the tile length to wrap
	// in each direction, given the current camera position

	double x_modulo, y_modulo;
	float h_scale = vsbViewportContext::EnvironmentOpts().HorizontalScale()*Scale();

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		x_modulo =  fmod(-vsbBaseCamera::CameraToOriginDelta()[0],h_scale)/h_scale;
		y_modulo =  fmod(-vsbBaseCamera::CameraToOriginDelta()[1],h_scale)/h_scale;
	} else
	{
		x_modulo =  fmod(p[0],h_scale)/h_scale;
		y_modulo =  fmod(p[1],h_scale)/h_scale;
	}


	sgVec2 base;   // texture's base offset

	base[0] = mInitialTextureCoords[0][0] + x_modulo;
	base[1] = mInitialTextureCoords[0][1] + y_modulo;

    //texture base offset must be between 0.0f and 1.0f so 
	//wrap if necessary 

	while ( base[0] > 1.0 ) { base[0] -= 1.0; }
	while ( base[0] < 0.0 ) { base[0] += 1.0; }

    // now re-calculate update texture coordinates

	float *texture_coord;

	texture_coord = mpTextureArr->get(0);
	sgSetVec2(texture_coord, base[0]+ tile_size/2, base[1]+ tile_size/2);

	texture_coord = mpTextureArr->get(1);
	sgSetVec2(texture_coord, base[0], base[1]);

	texture_coord = mpTextureArr->get( 2 );
	sgSetVec2( texture_coord, base[0] + tile_size, base[1] );

	texture_coord = mpTextureArr->get( 3 );
	sgSetVec2( texture_coord, base[0] + tile_size, base[1] + tile_size );

	texture_coord = mpTextureArr->get( 4 );
	sgSetVec2( texture_coord, base[0], base[1] + tile_size );

	texture_coord = mpTextureArr->get(5);
	sgSetVec2(texture_coord, base[0], base[1]);


    return true;
}


void vsbCloudLayer::Draw() {
	// check to see this cloud layer is built
	if (!mpCloudLayerRoot)
		return;

    ssgCullAndDraw( mpCloudLayerRoot );
}

ssgSimpleState *vsbCloudLayer::MakeCloud(CloudTypeEnum cloudType)
{
	string texture_path = vsbGetSysPath()+"/Sky/"+CloudFileName(cloudType);

	ssgSimpleState *state = new ssgSimpleState();
    state->setTexture( const_cast<char *>(texture_path.c_str()) );
    state->setShadeModel( GL_SMOOTH );
    state->disable( GL_LIGHTING );
    state->disable( GL_CULL_FACE );
    state->enable( GL_TEXTURE_2D );
    state->enable( GL_COLOR_MATERIAL );
    state->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    state->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    state->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    state->enable( GL_BLEND );
    state->enable( GL_ALPHA_TEST );
    state->setAlphaClamp( 0.01f );
    return state;
}



