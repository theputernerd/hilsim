#ifndef _vsbSmokeEmitter_H
#define _vsbSmokeEmitter_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 



#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _LIST_
#include <list>
#define _LIST_
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#ifndef _vsbSmokeVtxTable_H
#include"vsbSmokeVtxTable.h"
#endif

class _vsbSmokeEmitterDefinitionParser;
class DLL_VSB_API vsbSmokeEmitterDefinition;
class DLL_VSB_API vsbBaseViewEntity;

const int DEFAULT_MAX_PUFF_COUNT = 10000;


// Condition under which to delete emitter


#define EMITTER_FORCE		(1)				// Delete immediately
#define EMITTER_ACTIVE		(1<<1)			// Delete if not active
#define EMITTER_SCOPE		(1<<2)			// Delete if activity time is out of scope
#define EMITTER_ENTITY		(1<<3)			// Delete if entity is not available
#define EMITTER_ENTITY_VISIBILITY (1<<4)	// Delete if entity is not available, or not visible

class DLL_VSB_API vsbSmokeEmitter;

typedef void (*NameChangeHandlerFunc) ( vsbSmokeEmitter *emitter ) ;

class DLL_VSB_API vsbSmokeEmitter
{
	friend class DLL_VSB_API vsbSmokeFXDirector;
public:
    vsbSmokeEmitter();
    vsbSmokeEmitter(vsbSmokeEmitterDefinition *def);
    
    // Initialise this smoke emitter        
    void         Init(double time, vsbSmokeEmitterDefinition *emitter_def = 0);
    void         Interval(double t0, double t1) {mStartTime = t0; mEndTime = t1;};
    void         StartTime(double t) {mStartTime = t;} 
    void         EndTime(double t)   {mEndTime = (t>mStartTime)?t:mStartTime;} 

	double       StartTime() const {return mStartTime;} 
    double       EndTime() const  {return mEndTime;} 
    
    void         Activate(bool truefalse) { mActive = truefalse;};
	bool         IsActive() const {return mActive; };

	void         DeleteOn(unsigned int delflags, bool reset = false);
	bool		 CanDelete();
    
        
     int		Id() const {	return mId;	};  // unique id of this class of emitter
    virtual char const * SmokeEmitterClassName(); // unique name of this classd of emitter

	const std::string &AttachedEntityName() const {return mEntityName;};
	int			AttachedEntityHandle() const {return mEntityHandle;};

	int         ModelAttachmentIndex() const {return mModelAttachmentIndex;}
	const std::string &ModelAttachmentName() const {return mModelAttachmentName;};

    
    const        vsbSmokeEmitterDefinition *EmitterDef() const     {return mEmitterDef;}


	void		 ResetEmitter() { mFirstEmission = true; };	

				 // attach by entity ptr. will also set entity name

	void		 AttachToEntity(vsbBaseViewEntity *entityPtr, int attachmentIndex);
	void		 AttachToEntity(vsbBaseViewEntity *entityPtr, const std::string &attachment_name);

				 // attach by entity handle (name will be undeterminate)
	
	void		 AttachToEntity(int handle, int attachmentIndex);
	void		 AttachToEntity(int handle, const std::string &attachment_name);

				 // // attach by entity name)	
	void		 AttachToEntity(const std::string &entity_name, int attachmentIndex);
	void		 AttachToEntity(const std::string &entity_name, const std::string &attachment_name);
	


	void		 DetachFromEntity();
    vsbBaseViewEntity *AttachedEntity();	
	int          EntityHandle() const { return mEntityHandle; }

	// Force current position of the smoke emitter (will be overridden if entity is attached)

	void		 SetCurrPos(sgdVec3 pos) {sgdCopyVec3(mCurrUpdatePos, pos);}; 	
	void		 SetCurrPos(double x, double y, double z)  {sgdSetVec3(mCurrUpdatePos, x, y,z);}; 	
	void         GetCurrPos(double &x, double &y, double &z) { x = mCurrUpdatePos[0];y = mCurrUpdatePos[1]; z = mCurrUpdatePos[2];}; 	

    void NameChangeHandler(NameChangeHandlerFunc handler){mNameChangeHandler = handler;};

	bool		SetEmitterType(const std::string &def_name);
    
protected:
    
    
    
    virtual void UpdateEmitter(double time, bool suppress_emission = false);
    
    virtual void Regenerate(double time); 
	
    void         InitParticle(vsbSmokeVtxTable *smokeVtxTable, float time, sgdVec3 pos);
	void         EmitterDef(vsbSmokeEmitterDefinition *emitter_def){mEmitterDef = emitter_def;}
    
private:

    sgdVec3  mLastUpdatePos; // Emitter position
    sgdVec3  mCurrUpdatePos;
    
    bool    mActive;
    double  mStartTime;       // Absolute time emissions begin
    double  mEndTime;       // Absolute time emissions end -- interval between which emissions occur 
    
    double  mLastUpdateTime;  // last update time
    double  mCurrUpdateTime;  // current update time

	bool	mFirstEmission;        // Set to true for first update
	int		mEntityHandle;	      // handle of entity emitter attached to (-1 = no entity)
    std::string mEntityName;       // name of entity emitter attached to ("" = not available)

	NameChangeHandlerFunc mNameChangeHandler;

	int					   mId;	
	static int            msCounter;  
	


	int		mModelAttachmentIndex; // index of emitter on model
	std::string mModelAttachmentName;   
	
	

	unsigned int mDeleteFlags;

    vsbSmokeEmitterDefinition *mEmitterDef; // Defines the parameters of the smoke effect
    
    void    EmittParticles();
};

/////////////////////////////////////////////////////////////////////////////////////



class DLL_VSB_API vsbSmokeEmitterDefinition
{
    friend  class _vsbSmokeEmitterDefinitionParser;
public:
    vsbSmokeEmitterDefinition(const vsbSmokeEmitterDefinition &src);
    vsbSmokeEmitterDefinition(float lifespan, float startsize, float deltasize, 
		                      const std::string &textureFilename = "", bool preload = true);

    vsbSmokeEmitterDefinition();
    
    vsbSmokeEmitterDefinition  &operator =  (const vsbSmokeEmitterDefinition &src) ;

	const std::string &     Name() const {return mName;}
	void					Name(const std::string &name) {mName = name; }
    
    float                   LifeSpan() const {return mLifeSpan; }  
	void					LifeSpan(float lifespan) {mLifeSpan = lifespan; }  

    float                   StartSize() const {return mStartSize; }  
	void					StartSize(float size) {mStartSize = size;}

    float                   DeltaSize() const {return mDeltaSize; }   
	void                    DeltaSize(float dsize) {mDeltaSize = dsize; }   

	float                   EmissionRate() const {return mEmissionRate; }  
	void                    EmissionRate(float f)  {mEmissionRate = (f>= 0)?f:0; }  
	
    bool                    Preload() const {return mPreload; }        // Preload the puff texture?
	void                    Preload(bool truefalse) {mPreload = truefalse;}

    bool                    Loaded() const {return mLoaded; }
    const   std::string &   TextureFilename() const {return mTextureFilename; }
	void				    TextureFilename(const std::string &name); 
    
    
    void                    GetVelVector(sgdVec3 accel); // Get the puff's velocity vector
	void                    GetPosVector(sgdVec3 accel); // Get the puff's position vector

	void                    SetVelVectorLowerElement(int i, double val) { mLowerVelBound[i] = val; }; 
	void                    SetVelVectorUpperElement(int i, double val) { mUpperVelBound[i] = val; }; 
	void                    SetPosVectorLowerElement(int i, double val) { mLowerPosBound[i] = val; }; 
	void                    SetPosVectorUpperElement(int i, double val) { mUpperPosBound[i] = val; }; 

	void                    SetVelRanomize(int i, bool val) { mRandomizeVel[i] = val; }; 
	void                    SetPosRanomize(int i, bool val) { mRandomizePos[i] = val; };
	bool                    GetVelRanomize(int i) const { return mRandomizeVel[i] ; }; 
	bool                    GetPosRanomize(int i) const { return mRandomizePos[i] ; };

	double                  GetVelVectorLowerElement(int i) const { return mLowerVelBound[i]; }; 
	double                  GetVelVectorUpperElement(int i) const { return mUpperVelBound[i]; }; 
	double                  GetPosVectorLowerElement(int i) const { return mLowerPosBound[i]; }; 
	double                  GetPosVectorUpperElement(int i) const { return mUpperPosBound[i]; }; 
	


	void                    InitParticle(vsbSmokeVtxTable *smokeVtxTable, float time, sgdVec3 pos);
	void					DecayMode(EDecayMode mode) {mDecayMode = mode;}
	EDecayMode				DecayMode() {return mDecayMode;}
   
protected:

	
    void                    Loaded(bool truefalse) { mLoaded = truefalse; }
	void					Lighting(bool ambient_lit, sgVec3 color) {mAmbientLit = ambient_lit; sgCopyVec3(mColor,color);}

private:    

	// Smoke particle characteristics 

	std::string			   mName; 	
    std::string            mTextureFilename; //! The filename name of the texture 
	
	float				   mLifeSpan;	// lifespan of particles
	EDecayMode			   mDecayMode; //decay style
	float				   mStartSize; // initial size of particle
	float				   mDeltaSize; // change in size per sec	
	bool                   mPreload;    //!< Whether the state (texture) for this explosion is to be pre-loaded
    bool                   mLoaded;     //!< Indicates whether the texture is loaded

	bool				   mAmbientLit;
	sgVec3				   mColor;
    
	float				   mEmissionRate; // smoke particles per sec 	
    sgdVec3                mLowerVelBound; // random generator bounds - x, y, z
    sgdVec3                mUpperVelBound;
    bool                   mRandomizeVel[3];   // random generator state

	sgdVec3                mLowerPosBound; // random generator bounds - x, y, z
    sgdVec3                mUpperPosBound;
    bool                   mRandomizePos[3];   // random generator state
    
    sgdVec3                mUnitDirectionVector;

	// Emission state


	static ssgSimpleState * MakeState(const char *emitterTextureFilename);
	                 
				   	
	
   void                   SetVelPerturbation(int idx, double lower_bound, double upper_bound = 0, bool randomized = false);
    double                 GetVelPerturbation(int idx);
	void                   SetPosPerturbation(int idx, double lower_bound, double upper_bound = 0, bool randomized = false);
    double                 GetPosPerturbation(int idx);
};




DLL_VSB_API std::string  vsbMakeSmokeFXPath(std::string fname);


#endif