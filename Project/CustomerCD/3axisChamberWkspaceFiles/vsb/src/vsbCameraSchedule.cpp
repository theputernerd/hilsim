///////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbCameraManager.cpp
    \brief Implements the class vsbCameraManager and itas auxillary classes
    
     The class vsbCameraManager is responsible for managing all the cameras
     in a vsbViewContext. 
	 
	\sa vsbCameraManager.h

    \author T. Gouthas
*/
///////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 


#include <algorithm>

#include <assert.h>


#include "ssg.h"
#include "vsbBaseCamera.h"
#include "vsbCameraManager.h"
#include "vsbCameraSchedule.h"

using namespace std;
using namespace Parser;

vsbCameraScheduleEntry::vsbCameraScheduleEntry()
{
    mpCamSchedule = 0;
    Clear(); 
}


vsbCameraScheduleEntry::vsbCameraScheduleEntry(vsbCameraSchedule *camSchedule)
{
    assert(camSchedule!=0);
    mpCamSchedule = camSchedule;
    Clear(); 
}

vsbCameraScheduleEntry::vsbCameraScheduleEntry(const vsbCameraScheduleEntry& from)
{
    *this = from;
}

void vsbCameraScheduleEntry::Clear()
{
    mCameraId = -1;
    mActivationTime = 0;
}

vsbCameraScheduleEntry& vsbCameraScheduleEntry::operator=(const vsbCameraScheduleEntry& from)
{
    mpCamSchedule = from.mpCamSchedule;
    mScheduleId = from.mScheduleId;
    mCameraId = from.mCameraId;
    mActivationTime = from.mActivationTime;
    return *this;    
}

bool vsbCameraScheduleEntry::operator>(const vsbCameraScheduleEntry& from)
{
     
        return mActivationTime > from.mActivationTime;
}

bool vsbCameraScheduleEntry::operator<(const vsbCameraScheduleEntry& from)
{
   
        return mActivationTime < from.mActivationTime;
}

bool vsbCameraScheduleEntry::operator==(const vsbCameraScheduleEntry& from)
{

   return mActivationTime == from.mActivationTime;
}

void vsbCameraScheduleEntry::Set(long camID, double time)
{
    vsbBaseCamera *cam;
    assert(mpCamSchedule);
    cam = mpCamSchedule->CamManager().FindCam(camID);
    if (cam)
    {
        mCameraId = cam->Handle();
	    mActivationTime = time;	
    } else
        Clear();
    if (mpCamSchedule)
        mpCamSchedule->Sort();
}

void vsbCameraScheduleEntry::Set(const std::string &camName, double time)
{
    vsbBaseCamera *cam;
    assert(mpCamSchedule);
    cam = mpCamSchedule->CamManager().FindCam(camName);
    if (cam)
    {
        mCameraId = cam->Handle();
	    mActivationTime = time;	
    } else
        Clear();
    if (mpCamSchedule)
        mpCamSchedule->Sort();

}

void vsbCameraScheduleEntry::Set(const vsbBaseCamera *camera, double time)
{
	assert(camera);
    mCameraId = camera->Handle();
	mActivationTime = time;
    if (mpCamSchedule)
        mpCamSchedule->Sort();
}

void vsbCameraScheduleEntry::Set(double time)
{
    mActivationTime = time;
    if (mpCamSchedule)
        mpCamSchedule->Sort();
}


std::string vsbCameraScheduleEntry::CameraName()
{
    vsbBaseCamera *cam;
    cam = mpCamSchedule->CamManager().FindCam(mCameraId);
    if (!cam) return "";
    return cam->Name();
}
/////////////////////////////////////////////////////////

vsbCameraSchedule::vsbCameraSchedule(vsbCameraManager *camManager)
{
    assert(camManager);
    mpCamManager = camManager;
    ClearSchedule();
}

void vsbCameraSchedule::ClearSchedule()
{
    mScheduleList.clear();
}

vsbCameraScheduleEntry* vsbCameraSchedule::operator[](int index) 
{
    if (index < 0 || index >= mScheduleList.size()) return 0;
    return FindById(mScheduleList[index].ScheduleId());
}

vsbCameraScheduleEntry* vsbCameraSchedule::FindById(int scheduleId) 
{
    vector<vsbCameraScheduleEntry>::iterator iter;
    for (iter = mScheduleList.begin(); iter != mScheduleList.end(); iter++)
    {
        if ((*iter).ScheduleId() == scheduleId)
            return &(*iter);
    }
    return 0;
}

void vsbCameraSchedule::InsertSchedule(long camID, double time)
{
    vsbCameraScheduleEntry new_entry(this);
    new_entry.ScheduleId(FindFreeId());
    new_entry.Set(camID, time);
    mScheduleList.push_back(new_entry);
    Sort();

}

void vsbCameraSchedule::InsertSchedule(const std::string &camName, double time)
{
    vsbCameraScheduleEntry new_entry(this);
    new_entry.ScheduleId(FindFreeId());
    new_entry.Set(camName, time);
    mScheduleList.push_back(new_entry);
}

void vsbCameraSchedule::InsertSchedule(const vsbBaseCamera *camera, double time)
{
    vsbCameraScheduleEntry new_entry(this);
    new_entry.Set(camera, time);
    new_entry.ScheduleId(FindFreeId());
    mScheduleList.push_back(new_entry);
   
}

void vsbCameraSchedule::DeleteSchedule(int scheduleId)
{
    vector<vsbCameraScheduleEntry>::iterator iter;
    for (iter = mScheduleList.begin(); iter != mScheduleList.end(); iter++)
    {
        if ((*iter).ScheduleId() == scheduleId)
        {
            mScheduleList.erase(iter);
            return;
        }
    }
}

void vsbCameraSchedule::DeleteAllSchedules()
{
    mScheduleList.clear();
}

int  vsbCameraSchedule::ScheduleCout() const
{
    return mScheduleList.size();
}

void vsbCameraSchedule::VerifyCameras(bool cull)
{
    vector<vsbCameraScheduleEntry>::iterator iter;
	bool deleted = true;

	while(deleted)
	{
		deleted = false;
		for (iter = mScheduleList.begin(); iter != mScheduleList.end(); iter++)
		{
			int id = (*iter).mCameraId;
			if (id >= 0 && CamManager().FindCam(id)==0)
			{
				if (cull)
				{
					iter = mScheduleList.erase(iter);
					// iter is invalidated; need to start from the beginning
					deleted = true;
					break;
				} 
				else
					(*iter).Clear();
			}
		}
		Sort();
	}
}


void vsbCameraSchedule::Sort()
{
    sort(mScheduleList.begin(), mScheduleList.end());
}


long vsbCameraSchedule::SelectCamera(double timeSec)
{
    long camera = -1;
    double last_time = -1e99;
    vector<vsbCameraScheduleEntry>::iterator iter;
    for (iter = mScheduleList.begin(); iter != mScheduleList.end(); iter++)
    {
        double act_time = (*iter).ActivationTime();
        if (act_time > last_time && act_time <= timeSec)
        {
            last_time = act_time;
            camera = (*iter).CamId();
        }    
    }
    return camera;
}

void vsbCameraSchedule::ActivateCamera(double timeSec)
{
    long cam_id = SelectCamera(timeSec);
    if (cam_id < 0)
    {
        vsbBaseCamera *cam = CamManager().FindCam(CamManager().DefaultCameraId());
        if (cam) CamManager().ActiveCam(cam);
    } else
    {
        vsbBaseCamera *cam = CamManager().FindCam(cam_id);
        if (!cam) cam = CamManager().FindCam(CamManager().DefaultCameraId());
        if (cam) CamManager().ActiveCam(cam);
    }
}


int vsbCameraSchedule::FindFreeId()
{
    int i;
    bool match;
    for (i = 0; i<mScheduleList.size(); i++)
    {
        vector<vsbCameraScheduleEntry>::iterator iter;
        for (iter = mScheduleList.begin(); iter != mScheduleList.end(); iter++)
        {
            match = ((*iter).ScheduleId() == i);
            if (match) break;
        }
        if (match) continue;
        break;
    }
    return i;
}

int  vsbCameraSchedule::Write(FILE *f, int indent)
{
    vsbCameraScheduleParser s_parser(*this);
	return s_parser.Write(f,indent);
}

int	 vsbCameraSchedule::Parse(TLineInp *tli)
{
	mParseError.Clear();
	vsbCameraScheduleParser s_parser(*this);
	int result = s_parser.Parse(tli);
	if (result)
	{
		mParseError.Set(*s_parser.ItsLastError());
	}
	return result;
}

////----------------------------------------------------


char *vsbCameraScheduleParser::mpKeyWords[] =
{
	"CAMERA_SCHEDULE",
    "EVENT",
    "CAMERA",
	"NAME",
    "TIME",
	0
};

enum {
    VSB_CAMSCHED_SCHEDULE,
    VSB_CAMSCHED_EVENT,
    VSB_CAMSCHED_CAMERA,
    VSB_CAMSCHED_NAME,
	VSB_CAMSCHED_TIME
};

char *vsbCameraScheduleParser::mpErrorStrings[] =
{
	"",
	0
};

enum {
	VSB_CAMSCHED_ERR_UNUSED,
};

vsbCameraScheduleParser::vsbCameraScheduleParser(vsbCameraSchedule &cs):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrCameraSchedule(cs) 
{
      mpOutput = new vsbParserHelper(*this);
}


vsbCameraScheduleParser::~vsbCameraScheduleParser()
{
      delete mpOutput;
}

int	 vsbCameraScheduleParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int vsbCameraScheduleParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CAMSCHED_SCHEDULE);
    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent+=3;

    for(int i=0; i<mrCameraSchedule.ScheduleCout(); i++)
    {
        mpOutput->WriteNewline(indent);
	    mpOutput->WriteKeyWord(VSB_CAMSCHED_EVENT);
        mpOutput->WriteNewline(indent);
	    mpOutput->WriteKeyWord(T_LBRACE);
	    indent+=3;

        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(VSB_CAMSCHED_CAMERA);
        mpOutput->WriteString(" = ");
        mpOutput->WriteName(mrCameraSchedule[i]->CameraName().c_str());

        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(VSB_CAMSCHED_TIME);
        mpOutput->WriteValue(mrCameraSchedule[i]->ActivationTime());


        indent-=3;
	    mpOutput->WriteNewline(indent); 
	    mpOutput->WriteKeyWord(T_RBRACE);

    }

    indent-=3;
	mpOutput->WriteNewline(indent); 
	mpOutput->WriteKeyWord(T_RBRACE);
    return 1;
}

void vsbCameraScheduleParser::ParseFunc()
{
    mrCameraSchedule.DeleteAllSchedules();
    GetToken(VSB_CAMSCHED_SCHEDULE);
    GetToken(T_LBRACE);
    PeekToken();
    while(GetCurrTokenID() == VSB_CAMSCHED_EVENT)
    {
        string cam_name;
        double cam_time;

        GetToken(VSB_CAMSCHED_EVENT);
        GetToken(T_LBRACE);

        GetToken(VSB_CAMSCHED_CAMERA);
        GetToken(T_EQ);
        GetToken(T_STRING);
        cam_name = GetCurrTokenStr();
        
        GetToken(VSB_CAMSCHED_TIME);
        GetToken(T_EQ);
        cam_time = GetNumericValue();
        
        mrCameraSchedule.InsertSchedule(cam_name,cam_time);
        GetToken(T_RBRACE);
        PeekToken();
    }
    GetToken(T_RBRACE);
}