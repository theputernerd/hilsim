#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#include <iostream>
#include <string>
using namespace std;

#include "vsbEnvironmentOptions.h"
#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbTrajLeaf.h"
#include "vsbViewportContext.h"

typedef std::map<float, vsbInstanceData>           vsbInstanceDataMap; //!< Time referenced map of vsbInstanceData, ie, Trajectory
typedef std::map<float, vsbInstanceData>::iterator vsbInstanceDataIter; //!< Iterator for Trajectories


int vsbTrajLeafPredraw(ssgEntity *entity)
{

    vsbTrajLeaf *trajLeaf = (vsbTrajLeaf *)entity;
    if (trajLeaf->hasState())
      trajLeaf->getState()->apply();

    ETrajDisplayStatus status =  trajLeaf->TrajEntity().TrajDisplay();
    if (status == ETrajectory_Off) return 0;
    else 
        if (status == ETrajectory_Default && !vsbViewportContext::EnvironmentOpts().TrajectoryTraceOn())
            return 0;

	float curr_time				  = trajLeaf->TrajEntity().CurrTime();
	vsbInstanceDataMap  traj_data; //&traj_data = trajLeaf->TrajEntity().TrajData();
	vsbInstanceDataIter iter;


    switch(trajLeaf->TrajEntity().Team())
    {
        case ETeam_Blue:
            glColor4f(0.2f, 0.2f, 1.0f, 1.0f);
            break;
        case ETeam_Red:
            glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
            break;
        case ETeam_Yellow:	
            glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
            break;
        case ETeam_Green:
            glColor4f(0.2f, 0.8f, 0.2f, 1.0f);
            break;
        case ETeam_Magenta:
            glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
            break;
    }


	glAlphaFunc(GL_GEQUAL, 0.0625);
	glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glHint      (GL_LINE_SMOOTH_HINT, GL_DONT_CARE );

	glLineWidth(vsbViewportContext::EnvironmentOpts().TrajTraceThickness());

	glEnable( GL_FOG );
	glBegin(GL_LINE_STRIP);


	vsbInstanceData idat;
	int it = trajLeaf->IdxT();
	int ix = trajLeaf->IdxX();
	int iy = trajLeaf->IdxY();
	int iz = trajLeaf->IdxZ();


	for(bool found = trajLeaf->TrajEntity().FirstInstanceData(idat); 
			found && idat[it] < curr_time; 
			found = trajLeaf->TrajEntity().NextInstanceData(idat))
	{	
/*
		sgVec3 coord = {idat[ix],idat[iy],idat[iz]};
		vsbToScaledVSBCoords(coord);
		if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
			sgAddVec3(coord,vsbBaseCamera::CameraToOriginDelta());

		glVertex3fv(coord);
*/
		sgdVec3 coord = {idat[ix],idat[iy],idat[iz]};
		vsbToScaledVSBCoords(coord);
		if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
			sgdAddVec3(coord,vsbBaseCamera::CameraToOriginDelta());

		glVertex3dv(coord);

	}

	sgdVec3 coord;

//	coord[0] = trajLeaf->TrajEntity().EntityPosXYZ()[0];
//	coord[1] = trajLeaf->TrajEntity().EntityPosXYZ()[1];
//	coord[2] = trajLeaf->TrajEntity().EntityPosXYZ()[2];
	sgdCopyVec3(coord, trajLeaf->TrajEntity().EntityPosXYZ());
	
	vsbToScaledVSBCoords(coord);

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
				sgdAddVec3(coord,vsbBaseCamera::CameraToOriginDelta());

	glVertex3dv(coord);
	glEnd();

    return 0;
}



vsbTrajLeaf::vsbTrajLeaf(vsbTrajEntity *trajEntity):ssgVTable()
{
    mTrajEntity = trajEntity;

    ssgSimpleState *state = new ssgSimpleState();
    state->disable( GL_LIGHTING );
	state->disable( GL_TEXTURE_2D );
    state->disable( GL_CULL_FACE );
    state->enable( GL_COLOR_MATERIAL );
    state->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    state->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    state->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
//	state->disable( GL_BLEND );
//    state->disable( GL_ALPHA_TEST );

	state->enable( GL_BLEND );
    state->enable( GL_ALPHA_TEST );
    state->enable(GL_ALPHA_TEST);
//	state->enable( GL_LINE_SMOOTH );

	

    setState(state);
    setCallback(SSG_CALLBACK_PREDRAW, vsbTrajLeafPredraw);
	mNumVertices = 0;

	mIdxT = mTrajEntity->FindVarIdx("t");
	mIdxX = mTrajEntity->FindVarIdx("x");
	mIdxY = mTrajEntity->FindVarIdx("y");
	mIdxZ = mTrajEntity->FindVarIdx("z");
}


void vsbTrajLeaf::recalcBSphere ()
{
}

ssgCullResult vsbTrajLeaf::cull_test  ( sgFrustum *f, sgMat4 m, int test_needed ) 
{
	// We force the bsphere test to return SG_INSIDE so as to force all trajectories
	// to be always drawn as we dont manage the bounding spheres to well
	return SSG_INSIDE;
}