/////////////////////////////////////////////////////////////////
/*! 
    \file vsbRenderOptions.cpp
    \brief Implements the class vsbRenderOptions
 

	This file implements the vsbRenderOptions class that manages the 
	options and parameters related to rendering and implementation 
	of the EnvironmentManager and in fact any other application module 
	requiring simiar feature control. 
	
	\sa vsbRenderOptions.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef WIN32
#include <windows.h>
#endif

#include <string>

using namespace std;

#include "vsbRenderOptions.h"
#include "vsbSysPath.h"

vsbRenderOptions::vsbRenderOptions()
{
	SetDefault();
};

void vsbRenderOptions::SetDefault()
{
	mTextureMappingOn			= true;
	mFogOn						= true;
	mSolidPolygonsOn			= true;
	mGLNormalize				= false;
	mCameraCentricTransforms	= true;
	mPerspectiveCorrectionHint  = RenderFastest;
	mFogHint					= RenderNicest;
}



