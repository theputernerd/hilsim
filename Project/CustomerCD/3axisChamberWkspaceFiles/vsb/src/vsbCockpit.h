//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCockpit.h
    \brief Defines a cockpit view class
 
	

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbCockpit_H
#define _vsbCockpit_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif



//////////////////////////////////////////////////////////////////
/*! 
    \class vsbCockpit
    \brief Cockpit view class
 

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbCockpit 
{

protected:

					 
    ssgRoot          *mpCockpitRoot;  //!< The root node of the cockpit

					 
    ssgTransform     *mpCockpitXfm;	//!< The Cockpit's transformation	

    ssgColourArray   *mpColorArr;		//!< Cockpit colour array
    ssgVertexArray   *mpVertexArr;		//!< Cockpit vertex array
    ssgTexCoordArray *mpTextureArr;		//!< Cockpit texture array
					  

	
public:

    //! Constructor
    vsbCockpit( );


    //! Virtual destructor
    virtual ~vsbCockpit( void );

	//! Attach to hud display

    virtual void Build() {};

	virtual void Reposition(sgCoord &coord);
	void Reposition(sgMat4 mat4);
	virtual void RepositionWorldFrame(float x, float y, float z, float psi, float the, float phi);
	virtual void RepositionWorldFrame(sgVec3 pos, sgVec3 rot);

    //! draw the cockpit layer
    virtual void Draw();


};

#endif