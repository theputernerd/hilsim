#ifndef _vsbStrokedText_H
#define _vsbStrokedText_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


enum StrokeJustify
{
    JUST_MIDDLE,
    JUST_LOWER,
    JUST_UPPER
};

class DLL_VSB_API vsbStrokedText
{
public:
    vsbStrokedText();

    float Scale()const {return mScale;};
    void Scale(float s) {mScale = s/10.0; };
    void Hilight(bool onOff) {mHilight = onOff;};
	bool Hilight() { return mHilight;};

    void   DrawChar(char c, float &x, float &y);
    void   DrawRevChar(char c, float &x, float &y);
    void   DrawStr(const char *s, float x, float y, bool reverse = false);
    int   TextDimensions(const char *s, float &w, float &h);
    void  Justify(StrokeJustify horiz, StrokeJustify vert) 
                {mHorizJustificaton = horiz; mVertJustificaton = vert;};


	static void InitialiseFont();

private:
    float mScale;
	bool mHilight;
    StrokeJustify mVertJustificaton;
    StrokeJustify mHorizJustificaton;    
    static char msStrokeFont[][4];
    static int  msCharOffset[128];
    static int  msCharDefLength[128];
    static int msCharWidth[128];
    static bool msInitialised;
    static char *curr_op;

	
	static unsigned int msDListBase;

	static void  BuildCharacters();
	static void  BuildChar(char c);

    static float vsbStrokedText::X() { return float(curr_op[1]-'0'); };
    static float vsbStrokedText::Y() { return float(curr_op[2]-'0'); };

    
};

#endif