#ifndef _vsbSysPath_H
#define _vsbSysPath_H

//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSysPath.h
    \brief Defines the vsbGetSysPath() function.

    \sa vsbSysPath.cpp

    \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


/*! \brief Returns the fully qualified path to the application's root data directory

  The root data directory may be obtained through several techniques.. 

  - If a path was explicitly secified using SpecifySysPath before the first call, it 
    is taken to be the root data directory

  - If an environment variable exists matching SysEnvironmentVar, the value of the 
    variable is taken to be the root data directory.

  -  If the environment variable doesnt exist, then the behaviour is platform dependent;
     -  On Win32, the rood data directory is simply the fully qualified path to the executable
        appended with "/data".
     -  On Unix, it is taken to be the current working directory.


*/
DLL_VSB_API const std::string &vsbGetSysPath();

/*! \brief Specify a fully qualified path to the application's root data directory

  This function only has effect if it is called before vsbGetSysPath is called. After
  the first call to vsbGetSysPath, it has no effect. This function was designed to
  allow a system data path to be specified at the command line that overrides environment
  and default paths.
*/

DLL_VSB_API void SpecifySysPath(const std::string &str);


#ifdef _MSC_VER

/*! \brief Returns the fully qualified path to the application's executable directory

  This function is only available in Microsoft Windows
*/

DLL_VSB_API const std::string &GetExecutableDir();

#endif // _MSC_VER

#endif