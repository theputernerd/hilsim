#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "sg.h"
#include "ssg.h"


#include "vsbRenderOptions.h"
#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentOptions.h"

#include "vsbF18Cockpit.h"


vsbF18Cockpit::vsbF18Cockpit():vsbCockpit()
{
	 Build();
}

vsbF18Cockpit::~vsbF18Cockpit( void )
{


}

void vsbF18Cockpit::Build()
{
	string texture_path = vsbGetSysPath()+"/Effects/Cockpit/"+"fa18cockpit.rgb";
	string model_path = vsbGetSysPath()+"/Effects/Cockpit/fa18cockpit.3ds";

	ssgSimpleState *state = new ssgSimpleState();
    state->setTexture( const_cast<char *>(texture_path.c_str()) );
    state->setShadeModel( GL_SMOOTH );
    state->disable( GL_LIGHTING );
    state->disable( GL_CULL_FACE );
    state->enable( GL_TEXTURE_2D );
    state->enable( GL_COLOR_MATERIAL );
    state->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    state->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    state->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    state->enable( GL_BLEND );
    state->enable( GL_ALPHA_TEST );
    state->setAlphaClamp( 0.01f );

	ssgEntity *entity = ssgLoad(model_path.c_str());
	mpCockpitXfm->addKid(entity);


	ssgColourArray   *color_arr		  = new ssgColourArray( 4 );
    ssgVertexArray   *vertex_arr	  = new ssgVertexArray( 4 );
    ssgTexCoordArray *texture_arr     = new ssgTexCoordArray( 4 );

	sgVec4 colour;
    sgSetVec4( colour, 1.0f, 1.0f, 1.0f, 1.0f );

	sgVec2 txtpt;   // texture base coordinate

    

	sgVec3 verts[4] = {{-0.25f, 0.775f, 0.219f},  {0.25f,0.775f,0.219f},
						{-0.25f,0.604f,-0.301f},   {0.25f,0.604f,-0.301f}};

    sgSetVec2( txtpt, 0, 0.999f ); 
	texture_arr->add(txtpt);
    vertex_arr->add( verts[0] );
	color_arr->add( colour );

	sgSetVec2( txtpt, 0, 0 ); 
	texture_arr->add(txtpt);
	vertex_arr->add( verts[2] );
	color_arr->add( colour );

	sgSetVec2( txtpt, 1, 0 ); 
	texture_arr->add(txtpt);
	vertex_arr->add( verts[3] );
	color_arr->add( colour );

	sgSetVec2( txtpt, 1, 0.999f ); 
	texture_arr->add(txtpt);
	vertex_arr->add( verts[1] );
	color_arr->add( colour );


	ssgVtxTable* dash = new ssgVtxTable ( GL_TRIANGLE_FAN, vertex_arr, 0, texture_arr, color_arr );
	mpCockpitXfm->addKid(dash);
	dash->setState(state);



}

