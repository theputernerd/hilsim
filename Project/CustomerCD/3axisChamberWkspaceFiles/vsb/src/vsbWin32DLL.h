#ifndef _vsbWin32DLL_H
#define _vsbWin32DLL_H

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLL_VSB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLL_VSB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.


// NOTE:
// You must define _DLL for stl member functions to be exported from a dll user class


//#define DLL_VSB_API


#if !defined(MSC_VER)
#  define DLL_VSB_API
#else(MSC_VER)
#  if defined (DLL_VSB_EXPORTS)   
#     define  DLL_VSB_API __declspec(dllexport)
#     define  DLL_ENABLED_VSB
#  elif defined (DLL_VSB_IMPORTS)
#     define DLL_VSB_API __declspec(dllimport)
#     define DLL_ENABLED_VSB
#  else    
#     define DLL_VSB_API
#  endif
#endif


//The following shit is because fucking MS Visual C++ wont export STL members
//even if the class is exported. Worse still, the following workaround doesnt
//exist at all for complex templates such as Maps
//
//If you intend to access STL members of a DLL from a calling program then
//the DLL should be discarded and made a static library - its the only solution.

// NOTE: _DLL is defined when runtime c-libraries are selected
//       MSC_VER is defined when the compiler is MS Visual C++
// DLL_VSB_EXPORTS is defined by the user when compiling the DLL
// DLL_VSB_IMPORTS is defined by the user when compiling the application importing the DLL


#if defined(DLL_ENABLED_VSB) && !defined(_DLL)
// 1. Dont need to export the string STL class if not importing/exporting from DLL 
// 2. Do need to check that a DLL runtime library is not selected for MS Visual C++
//    because if it is, the following has laready been done


// We need to export the string template.. lets load the header first
#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#if defined (DLL_VSB_EXPORTS)
#define    EXPIMP_TEMPLATE 
#else
#define    EXPIMP_TEMPLATE extern
#endif

namespace std 
{
#pragma warning(disable:4231) // the extern before template is a non-standard extension 

    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const char, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<char, char_traits<char>, allocator<char> > __cdecl operator+(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<char, char_traits<char>, allocator<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const char *, const basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<char, char_traits<char>, allocator<char> >&, const char *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl operator>>(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl getline(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<char, char_traits<char> >& __cdecl getline(
            basic_istream<char, char_traits<char> >&,
            basic_string<char, char_traits<char>, allocator<char> >&, const char);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_ostream<char, char_traits<char> >& __cdecl operator<<(
            basic_ostream<char, char_traits<char> >&,
            const basic_string<char, char_traits<char>, allocator<char> >&);

    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const wchar_t, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> > __cdecl operator+(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator==(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator!=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator<=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const wchar_t *, const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template DLL_VSB_API bool __cdecl operator>=(
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t *);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl operator>>(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl getline(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_istream<wchar_t, char_traits<wchar_t> >& __cdecl getline(
            basic_istream<wchar_t, char_traits<wchar_t> >&,
            basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&, const wchar_t);
    EXPIMP_TEMPLATE template class DLL_VSB_API
        basic_ostream<wchar_t, char_traits<wchar_t> >& __cdecl operator<<(
            basic_ostream<wchar_t, char_traits<wchar_t> >&,
            const basic_string<wchar_t, char_traits<wchar_t>, allocator<wchar_t> >&);

#pragma warning(default:4231) // restore previous warning 

} // namespace

#endif // defined(DLL_ENABLED_VSB) && !defined(_DLL)


#endif