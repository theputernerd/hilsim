#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#include <string>
#include <list>

#include "ssg.h"
#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

#include "vsbUtilities.h"
#include "vsbSysPath.h"
#include "vsbExplosion.h"
#include "vsbExplosionFXDirector.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"

const char RELATIVE_EXPLOSION_DIRECTORY[] = "/effects/explosions/";

// The name of the explosion definition file containing the list of explosions
const char EXPLOSION_DEFINITION_FILE[] = "explosion.idx";

using namespace std;
using namespace Parser;


string vsbMakeExplosionFXPath(string fname)
{
	string work_str = vsbGetSysPath()+ RELATIVE_EXPLOSION_DIRECTORY;
	vsbRemoveEnclosingSlashes(fname);
	work_str += fname;
	return work_str;
}


//////////////////////////////////////////////////////////////////////////////
vsbExplosionDefinition::vsbExplosionDefinition()
{
    mFrameDelay = 0;
    mFrameCount = 0;
	mPreload = true;
	mLoaded = false;
}

vsbExplosionDefinition::vsbExplosionDefinition(int frames, float delay, const string &textureFilename, bool preload)
{
    mFrameCount = frames;
    mFrameDelay = delay;
	mPreload = preload;
	mTextureFilename = textureFilename;

}

vsbExplosionDefinition::vsbExplosionDefinition(const vsbExplosionDefinition &src)
{
    *this = src;
}

vsbExplosionDefinition &vsbExplosionDefinition::operator =  (const vsbExplosionDefinition &src)
{
    mFrameCount = src.mFrameCount;
    mFrameDelay = src.mFrameDelay;
	mPreload = src.mPreload;
	mTextureFilename = src.mTextureFilename;
	mLoaded = src.mLoaded;
    return *this;
}

////////////////////////////////////////////////////////////////////////////////
/*!
  \class _vsbExplosionDefinitionParser 

  \brief Defines a parse object for the explosion definition file grammar

*/
////////////////////////////////////////////////////////////////////////////////


class _vsbExplosionDefinitionParser: public TParseObjectBase
{

public:
    /*! \brief Constructor
        */
	_vsbExplosionDefinitionParser(vsbStateManager *stateManager);

    /*! \brief Parse a model index file
    */
	int ParseFile(const string &fname);

private:
    vsbStateManager *mpStateManager;
	static char     *mpsKeywords[];    //!< Explosion definition file grammar key words
	static char     *mpsErrors[];      //!< Explosion definition file error strings 
	

	void   ParseFunc();	                //!< Overidden virtual parse function (see parser)


/** @name Sub-Parsers
Private functions that parse different blocks of the model file grammar
*/
//@{
    void ParseExplosionBlock();
//@}

};


// Enumerations for keywords in explosion definition grammar

enum {
	EXPL_EXPLOSION_LIST,
    EXPL_VERSION,
    EXPL_EXPLOSION,    
    EXPL_FILENAME,
    EXPL_FRAMECOUNT,
    EXPL_FRAMEDELAY,
	EXPL_PRELOAD,
	EXPL_FALSE,
	EXPL_TRUE,

};

// Enumerations for error messages when parsing explosion definition grammar

enum {
	EXPL_UNUSED,
	EXPL_INTERNAL_ERR,
	EXPL_FILE_ERR,
    EXPL_FRAMCOUNT_ERR,
    EXPL_FRAMECOUNT_SQRT_ERR,
    EXPL_FRAMEDELAY_ERR,
    EXPL_MULTIPLE_STATE_DEF_ERR,
	EXPL_MULTIPLE_DEF_ERR,
	EXPL_BOOLEAN_EXPECTED_ERR,

};



char * _vsbExplosionDefinitionParser::mpsKeywords[] = 
{
	"EXPLOSION_LIST",
    "VERSION",
    "EXPLOSION",
    "FILENAME",
    "FRAMECOUNT",
    "FRAMEDELAY",
	"PRELOAD",
	"FALSE",
	"TRUE",
	0
};

char * _vsbExplosionDefinitionParser::mpsErrors[] = 
{
	"",
	"Internal Error",
	"Unable to open explosion index file",
    "Frame count must be at leas 1",
    "Frame count must have a whole square root",
    "Frame delay must be greater than 0 seconds",
    "Explosion state multiply defined",
	"Explosion multiply defined",
	"Expected \"TRUE\" or \"FALSE\" keyword",
	0
};



_vsbExplosionDefinitionParser::_vsbExplosionDefinitionParser(vsbStateManager *stateManager):
TParseObjectBase(mpsKeywords, mpsErrors), mpStateManager(stateManager)
{
	vsbExplosionFXDirector::msExplosionDefinitions.clear();
}


int _vsbExplosionDefinitionParser::ParseFile(const string &fname)
{
	TFileLineInp mInput;
	vsbExplosionFXDirector::msExplosionDefinitions.clear();


	if (!mInput.Open(fname.c_str()))
	{
		mInput.Close();
        itsLastError->Set(EXPL_FILE_ERR,0,"",mpsErrors[EXPL_FILE_ERR]);
        return EXPL_FILE_ERR;
	} 

	int errNum = ParseSource(&mInput, 1);
    mInput.Close();
	return errNum;
}

void _vsbExplosionDefinitionParser::ParseExplosionBlock()
{
	bool preload;
    string explosion_name;
    string explosion_file;
    int framecount;
    int sqrt_framecount;
    float framedelay;

    GetToken(EXPL_EXPLOSION);
    GetToken(T_LBRACK);
    GetToken(T_STRING);
    explosion_name = GetCurrTokenStr();
    GetToken(T_RBRACK);
    GetToken(T_LBRACE);

    GetToken(EXPL_FILENAME);
    GetToken(T_EQ);
    GetToken(T_STRING);
    explosion_file = GetCurrTokenStr();

	GetToken(EXPL_PRELOAD);
	GetToken(T_EQ);
	GetToken();
	if (GetCurrTokenID() == EXPL_TRUE)
		preload = true;
    else if (GetCurrTokenID() == EXPL_FALSE)
		preload = false;
	else
		AbortParse(EXPL_BOOLEAN_EXPECTED_ERR);


    GetToken(EXPL_FRAMECOUNT);
    GetToken(T_EQ);
    framecount = GetIntValue();
	if (framecount < 1)
        AbortParse(EXPL_FRAMCOUNT_ERR);

    sqrt_framecount = (int) sqrt(framecount);
    if (sqrt_framecount*sqrt_framecount != framecount) 
        AbortParse(EXPL_FRAMECOUNT_SQRT_ERR);

    GetToken(EXPL_FRAMEDELAY);
    GetToken(T_EQ);
    framedelay = GetNumericValue();
    if (framedelay <= 0)
        AbortParse(EXPL_FRAMEDELAY_ERR);

    // Need to create state and inser into state manager here
    
    if (mpStateManager->Find(explosion_name)) 
        AbortParse(EXPL_MULTIPLE_STATE_DEF_ERR);

	//See if a like named explosion has already been added
	map<string, vsbExplosionDefinition>::iterator iter;

    iter = vsbExplosionFXDirector::msExplosionDefinitions.find(explosion_name);
    if (iter != vsbExplosionFXDirector::msExplosionDefinitions.end())
		AbortParse(EXPL_MULTIPLE_DEF_ERR);

    vsbExplosionDefinition expl_def(framecount,framedelay, explosion_file, preload);

    // if texture already present, reference it rather than reloading a copy
    ssgSimpleState *expl_state = mpStateManager->FindByTextureFilename(explosion_file);
    if (!expl_state)
	{
		if (preload)
		{
	        expl_state = vsbExplosion::MakeState(explosion_file.c_str());
			mpStateManager->Insert(explosion_name, expl_state);
			expl_def.Loaded(true);
		} else
			expl_def.Loaded(false);
	} else 
    {
		mpStateManager->Insert(explosion_name, expl_state);
		expl_def.Loaded(true);
    }
    
    vsbExplosionFXDirector::msExplosionDefinitions.insert(pair<string, vsbExplosionDefinition>(explosion_name,expl_def));

    GetToken(T_RBRACE);
    PeekToken();

}


void _vsbExplosionDefinitionParser::ParseFunc()
{
	double ver_num;

	GetToken(EXPL_EXPLOSION_LIST);
	GetToken(T_LBRACK);
	GetToken(EXPL_VERSION);
	ver_num = GetNumericValue();
	GetToken(T_RBRACK);
    GetToken(T_LBRACE);
    PeekToken();
    while (GetCurrTokenID() == EXPL_EXPLOSION)
    {
        ParseExplosionBlock();
    }
    GetToken(T_RBRACE);

}


////////////////////////////////////////////////////////////////////////////////////

map<string, vsbExplosionDefinition> vsbExplosionFXDirector::msExplosionDefinitions;
int vsbExplosionFXDirector::mInstanceCount = 0;

vsbExplosionFXDirector::vsbExplosionFXDirector()
{
	mInstanceCount++;
	assert(mInstanceCount == 1);
}

vsbExplosionFXDirector::~vsbExplosionFXDirector()
{
	StopAll();
	ClearFreeList();
	if (--mInstanceCount == 0) 
		ClearExplosionDefinitions();
}

ssgSimpleState * vsbExplosionFXDirector::GetState(map<string, vsbExplosionDefinition>::iterator &iter)
{
	//@@ Fill and use
    if (iter->second.Loaded())
    {
        return vsbViewportContext::StateManager().Find(iter->first);	
    } else if (iter->second.Preload())
    {
        return 0;
    } else
    {
        ssgSimpleState *expl_state = vsbExplosion::MakeState(iter->second.TextureFilename().c_str());
        if (!expl_state) return 0;
        vsbViewportContext::StateManager().Insert(iter->first, expl_state);
        iter->second.Loaded(true);
        return expl_state;
    }
}

ssgSimpleState *vsbExplosionFXDirector::GetState(const std::string &explTypeName)
{
	map<string, vsbExplosionDefinition>::iterator iter;
    iter = msExplosionDefinitions.find(explTypeName);
    if (iter == msExplosionDefinitions.end()) return false;

    return GetState(iter);
}

vsbExplosion *vsbExplosionFXDirector::Initiate(float x, float y, float z, float size, const string explosionTypeName)
{
	map<string, vsbExplosionDefinition>::iterator iter;
    iter = msExplosionDefinitions.find(explosionTypeName);
    if (iter == msExplosionDefinitions.end()) return 0;

    ssgSimpleState *expl_state = GetState(iter);
	if (!expl_state) return 0;

	vsbExplosion *expl = GetFreeExplosion();
    if (expl == 0) return 0;

	expl->mTypeName = explosionTypeName;
    mActiveExplosions.push_front(expl);


	expl->Initialise(expl_state, sgSqrt(iter->second.FrameCount()), size, iter->second.FrameDelay());
    expl->SetPosition(x,y,z);
	expl->StartAnimation(vsbViewportContext::EntityManager().CurrTime());
    vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(expl);
	return expl;
}

vsbExplosion * vsbExplosionFXDirector::Initiate(sgVec3 pos, float size, const string explosionTypeName)
{
    return  Initiate(pos[0], pos[1], pos[2], size, explosionTypeName);   
}

vsbExplosion * vsbExplosionFXDirector::Initiate(float x, float y, float z, float size, int explosionIndex)
{
    int i;
    map<string, vsbExplosionDefinition>::iterator iter;
    for(i = 0, iter = msExplosionDefinitions.begin(); iter != msExplosionDefinitions.end() && i < explosionIndex; i++, iter++);
    if (i != explosionIndex || iter == msExplosionDefinitions.end()) return false; 
    
    ssgSimpleState *expl_state = GetState(iter);
	if (!expl_state) return 0;

    vsbExplosion *expl = GetFreeExplosion();
    if (expl == 0) return 0;

	expl->mTypeName=iter->first;
    mActiveExplosions.push_front(expl);


	expl->Initialise(expl_state, sgSqrt(iter->second.FrameCount()), size, iter->second.FrameDelay());
    expl->SetPosition(x,y,z);
	expl->StartAnimation(vsbViewportContext::EntityManager().CurrTime());
    vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(expl);
	return expl;
}

vsbExplosion * vsbExplosionFXDirector::Initiate(sgVec3 pos, float size, int explosionIndex)
{
    return  Initiate(pos[0], pos[1], pos[2], size, explosionIndex);
}


bool vsbExplosionFXDirector::Update(const string &explosionTypeName, vsbExplosion *expl)
{
	map<string, vsbExplosionDefinition>::iterator iter;
    iter = msExplosionDefinitions.find(explosionTypeName);
    if (iter == msExplosionDefinitions.end()) return false;

    ssgSimpleState *expl_state = GetState(iter);
	if (!expl_state) return false;

    if (expl == 0) return false;

	expl->mTypeName = explosionTypeName;
	expl->Initialise(expl_state, sgSqrt(iter->second.FrameCount()), expl->mSize, iter->second.FrameDelay());
	return true;

}    

void vsbExplosionFXDirector::ClearFreeList()
{
	list<vsbExplosion *>::iterator iter;
	for (iter = mFreeExplosions.begin(); iter != mFreeExplosions.end(); iter++)
		ssgDeRefDelete(*iter);
	mFreeExplosions.clear();
}



void vsbExplosionFXDirector::CreateNewExplosion()
{
	vsbExplosion *expl = new vsbExplosion;
	expl->ref();
	mFreeExplosions.push_front(expl);
}

vsbExplosion *vsbExplosionFXDirector::GetFreeExplosion(int requestedFrameCount)
{
	vsbExplosion *expl;
    list<vsbExplosion *>::iterator iter;

    // Make sure inactive explosions are put back on the free list
    FreeInactiveExplosions();

	// If the free explosion list is empty, add a new
	if (mFreeExplosions.size() == 0) CreateNewExplosion();

    // Else if we are looking for a specific framecount explosion, look for it
    else if (requestedFrameCount > 0)
    {
        for(iter = mFreeExplosions.begin(); iter != mFreeExplosions.end(); iter++)
        {
            if ( (*iter)->FrameCount() == requestedFrameCount)
            {
                expl = *iter;
                mFreeExplosions.erase(iter);
                return expl;
            }
        }
    }

    // Otherwise return the next free explosion on the free list
	iter = mFreeExplosions.begin(); 
	expl = *iter;
	mFreeExplosions.pop_front();
	return expl;
}

void vsbExplosionFXDirector::FreeInactiveExplosions()
{
    vsbExplosion *expl;
    list<vsbExplosion *>::iterator iter;
    int i, num_parents;

    for (iter = mActiveExplosions.begin(); iter!=mActiveExplosions.end();)
    {
        if ((*iter)->IsRunning() == false)
        {
            expl = *iter;

            mFreeExplosions.push_front(expl);

            //expl->removeParent(vsbViewportContext::EnvironmentManager().SceneRoot());

		    //detatch the explosion from the parent scene node
            num_parents = (*iter)->getNumParents();
		    for (i=0; i<num_parents; i++)
		    {
			    ssgEntity *parent = (*iter)->getParent(i);
			    if (parent->isAKindOf(_SSG_TYPE_BRANCH))
				    ((ssgBranch *)parent)->removeKid(*iter);
                else 
                    ulSetError( UL_FATAL, "Invalid parent of explosion found");
		    }
            iter = mActiveExplosions.erase(iter);
        } else
            iter++;
    }
        
}


void vsbExplosionFXDirector::StopAll()
{
	int i;
	list<vsbExplosion *>::iterator iter;

	// loop through explosions on the active list
	for (iter = mActiveExplosions.begin(); iter != mActiveExplosions.end(); iter++)
	{
		(*iter)->StopAnimation();
		int num_parents = (*iter)->getNumParents();

		//detatch the explosion from the parent scene node
		for (i=0; i<num_parents; i++)
		{
			ssgEntity *parent = (*iter)->getParent(i);
			if (parent->isAKindOf(_SSG_TYPE_BRANCH))
				((ssgBranch *)parent)->removeKid(*iter);
		}

    	// add the explosion to the free list
		mFreeExplosions.push_front(*iter);

	}
	mActiveExplosions.clear();
}


void  vsbExplosionFXDirector::Stop(int id)
{
	vsbExplosion *expl = FindExplosionById(id);
	if (!expl) return;
	Stop(expl);
}

void  vsbExplosionFXDirector::Stop(vsbExplosion *expl)
{
		int i;
	list<vsbExplosion *>::iterator iter;

	// loop through explosions on the active list
	for (iter = mActiveExplosions.begin(); iter != mActiveExplosions.end(); iter++)
	{
		if ((*iter) == expl)
		{
			int num_parents = expl->getNumParents();

			//detatch the explosion from the parent scene node
			for (i=0; i<num_parents; i++)
			{
				ssgEntity *parent = expl->getParent(i);
				if (parent->isAKindOf(_SSG_TYPE_BRANCH))
					((ssgBranch *)parent)->removeKid(expl);
			}

    		// add the explosion to the free list
			mFreeExplosions.push_front(expl);
			mActiveExplosions.erase(iter);
			return;
		}
	}
}

string  vsbExplosionFXDirector::DebugStatusString()
{
    string output;
    char  buffer[40];
    output = "Explosions:: Active ";
    itoa(mActiveExplosions.size(),buffer,10);
    output += buffer;
    output += " Free ";
    itoa(mFreeExplosions.size(),buffer,10);
    output += buffer;
    return output;
}

void vsbExplosionFXDirector::StateLoaderCallback(vsbStateManager *stateManager)
{

	LoadExplosionDefinitions(stateManager, EXPLOSION_DEFINITION_FILE);
/*
    _vsbExplosionDefinitionParser parser(stateManager);
    int result = parser.ParseFile(vsbMakeExplosionFXPath(EXPLOSION_DEFINITION_FILE).c_str());
    if (result)
    {
		char buffer[400];
		sprintf(buffer,"While loading explosion index file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			vsbMakeExplosionFXPath(EXPLOSION_DEFINITION_FILE).c_str(), 
			parser.ItsLastError()->Id(),
			parser.ItsLastError()->Desc(),
			parser.ItsLastError()->Pos(),
			parser.ItsLastError()->Line());
		ulSetError(UL_FATAL, buffer);
		return;
    }
*/
}


vsbExplosion *vsbExplosionFXDirector::FindExplosionById(int id)
{
	list<vsbExplosion *>::iterator iter;

	// loop through explosions on the active list
	for (iter = mActiveExplosions.begin(); iter != mActiveExplosions.end(); iter++)
	{
		if ((*iter)->Id() == id) return *iter;
	}
	return 0;
}

void vsbExplosionFXDirector::LoadExplosionDefinitions(vsbStateManager *stateManager, const std::string &explosionIndexFile)
{
	_vsbExplosionDefinitionParser parser(stateManager);
	ClearExplosionDefinitions();
    int result = parser.ParseFile(vsbMakeExplosionFXPath(explosionIndexFile).c_str());
    if (result)
    {
		char buffer[400];
		sprintf(buffer,"While loading explosion index file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			vsbMakeExplosionFXPath(explosionIndexFile).c_str(), 
			parser.ItsLastError()->Id(),
			parser.ItsLastError()->Desc(),
			parser.ItsLastError()->Pos(),
			parser.ItsLastError()->Line());
		ulSetError(UL_FATAL, buffer);
		return;
    }
}

void vsbExplosionFXDirector::ClearExplosionDefinitions()
{
	msExplosionDefinitions.clear();

}


void	vsbExplosionFXDirector::CameraCentricTranslation()
{
	list<vsbExplosion *>::iterator iter;

	// loop through explosions on the active list
	for (iter = mActiveExplosions.begin(); iter != mActiveExplosions.end(); iter++)
	{
		(*iter)->CameraCentricTranslation();
	}
}


void vsbExplosionFXDirector::AdvanceAll()
{
	list<vsbExplosion *>::iterator iter;

	// loop through explosions on the active list
	for (iter = mActiveExplosions.begin(); iter != mActiveExplosions.end(); iter++)
	{
		(*iter)->AdvanceFrame();
	}
	FreeInactiveExplosions();
}


const char *vsbExplosionFXDirector::LoadFile(const std::string &filename)
{
	static char buffer[400];

	vsbExplosionFXListParser expl_parser(*this);
	TFileLineInp fli;

	if (!fli.Open(filename.c_str()))
	{
		sprintf(buffer,"Could not open Explosion file: \"%s\"\n",	filename.c_str());
		return buffer;
	}


	int err_val = expl_parser.Parse(&fli);
	fli.Close();

	

	if (err_val)
	{
		sprintf(buffer,"While loading Explosions from file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			filename.c_str(), //parser.CurrentActiveSourceFile().c_str(), 
			expl_parser.ItsLastError()->Id(),
			expl_parser.ItsLastError()->Desc(),
			expl_parser.ItsLastError()->Pos(),
			expl_parser.ItsLastError()->Line());
		ulSetError(UL_WARNING, buffer);
		return buffer;
	}

	return 0;
	
}

bool vsbExplosionFXDirector::SaveFile(const std::string &filename)
{
	FILE *f;
	f = fopen(filename.c_str(),"wt");
	if (!f) return false;

	vsbExplosionFXListParser expl_parser(*this);
	bool success = (expl_parser.Write(f, 0) == 1);
	fclose(f);
	return success;


}

///////////////////////////////////////////////////////////////////////////////////////


class DLL_VSB_API vsbExplosionSeting
{
public:
	vsbExplosionSeting(const std::string& name, float st,float x, float y, float z, float s, float dt);
	vsbExplosionSeting(const vsbExplosionSeting & from);
	vsbExplosionSeting &operator = (const vsbExplosionSeting & from);
	std::string mExplName;
	float mX, mY, mZ;
	float mSize;
	float mStartTime;
	float mFrameDelay;
};


vsbExplosionSeting::vsbExplosionSeting(const std::string& name, float st, float x, float y, float z, float s, float dt)
{
	mExplName = name;
	mX = x;
	mY = y;
	mZ = z;
	mSize = s;
	mStartTime = st;
	mFrameDelay = dt;
}


vsbExplosionSeting::vsbExplosionSeting(const vsbExplosionSeting & from)
{
	*this = from;
}

vsbExplosionSeting &vsbExplosionSeting::operator = (const vsbExplosionSeting & from)
{
	this->mExplName		= from.mExplName;
	this->mStartTime	= from.mStartTime;
	this->mFrameDelay	= from.mFrameDelay;
	this->mX			= from.mX;
	this->mY			= from.mY;
	this->mZ			= from.mZ;
	this->mSize			= from.mSize;
	return *this;
}

///////////////////////////////////////////////////////////////////////////////////////


char *vsbExplosionFXListParser::mpKeyWords[] = {0};
char *vsbExplosionFXListParser::mpErrorStrings[]= {"","Unexpected Token",0};

enum vsbExplosionFXListParserError
{
	FX_UNUSED,
	FX_UNEXPECTED_TOKEN,
};

vsbExplosionFXListParser::vsbExplosionFXListParser(vsbExplosionFXDirector &efxd):
mrExplosionFXDirector(efxd)
{
	mpOutput = new vsbParserHelper(*this);
}

vsbExplosionFXListParser::~vsbExplosionFXListParser()
{
}

void vsbExplosionFXListParser::ParseFunc()
{
	mExplosionList.clear();
	GetToken();
	string name;
	float x, y, z, s, st, dt;

	while (GetCurrTokenID() == T_STRING || GetCurrTokenID() == T_UNKNOWN)
	{
		name = GetCurrTokenStr();
		st = GetNumericValue();
		x = GetNumericValue();
		y = GetNumericValue();
		z = GetNumericValue();
		s = GetNumericValue();
		
		PeekToken();
		if (GetCurrTokenID() != T_STRING && GetCurrTokenID() != T_UNKNOWN && GetCurrTokenID() != T_EOF)
			dt = GetNumericValue();
		else
			dt = -1.0; // indicates automatic setting
		GetToken();
		mExplosionList.push_back(vsbExplosionSeting(name, st, x, y, z, s, dt));

	}
	if (GetCurrTokenID() != T_EOF)
		AbortParse(FX_UNEXPECTED_TOKEN);
	
	vsbExplosionFXDirector &fxdir = vsbViewportContext::ExplosionFXDirector();

	for (std::vector<vsbExplosionSeting>::iterator iter = mExplosionList.begin();
		iter != mExplosionList.end(); iter++)
	{
		vsbExplosion *expl =  fxdir.Initiate(iter->mX, iter->mY, iter->mZ, iter->mSize, iter->mExplName);
		if (expl)
		{
			expl->StartAnimation(iter->mStartTime);
			if (iter->mFrameDelay > 0) expl->SetFrameDuration(iter->mFrameDelay);
		} else
		{
			ulSetError(UL_WARNING, "An invalid explosion was read from file\n");
			//invalid explosion
		}

	}
}


int	 vsbExplosionFXListParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int vsbExplosionFXListParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

	vsbExplosionFXDirector &fxdir = vsbViewportContext::ExplosionFXDirector();
	std::list<vsbExplosion *> &expl_list = fxdir.ActiveExplosions();
	fprintf(file, "! Explosion type, start time, x, y, z, size, delay\n");
	fprintf(file, "! ------------------------------------------------\n");
	for (std::list<vsbExplosion *>::iterator iter = expl_list.begin(); iter != expl_list.end(); iter++)
	{
		float x, y, z;
		(*iter)->GetPosition(x, y, z);
		// name t, x, y, z, s, dt
		fprintf(file,"\"%s\" %f %f %f %f %f %f\n",(*iter)->TypeName().c_str(), (*iter)->InitialTime(), x, y, z,
			(*iter)->GetScale(),  (*iter)->GetFrameDuration());
			
	}

	return 1;
}