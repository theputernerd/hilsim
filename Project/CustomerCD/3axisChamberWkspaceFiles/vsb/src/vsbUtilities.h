#ifndef _vsbUtilities_H
#define _vsbUtilities_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif


#include <vector>


#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

/*! \func vsbRemoveEnclosingSlashes

 The string passed is assumed to be a relative path name and consequently
 we require that path to not be lead or trailed with a path separator ie
 an '/' or an '\'
*/

inline void vsbRemoveEnclosingSlashes(std::string &s)
{
	if (s.length() < 1) return; 
	// remove leading/trailing '/' or '\'
	if (s[0] == '/' || s[0] == '\\')
		s.erase(0,1);
	if (s[s.length()-1] == '/' || s[s.length()-1] == '\\')
		s.erase(s.length()-1,1);
};



void FindSubDirectories(const std::string &start_dir, std::vector<std::string> &dirs, bool first = true);


#endif