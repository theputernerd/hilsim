////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseViewEntity.cpp
    \brief Defines the class vsbBaseViewEntity and itas auxillary classes
    
     The class vsbBaseEntity is the  abstract base class for all entities
	 in the 3D scene, providing the basic funtionality necessary to to be 
	 managed by the entity manager.
	
	\sa vsbBaseViewEntity.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <assert.h>

#include "ssg.h"

using namespace std;

#include "vsbBaseViewEntity.h"
#include "vsbEnvironmentManager.h"
#include "vsbEntityManager.h"
#include "vsbModelManager.h"
#include "vsbModelRoot.h"
#include "vsbViewportContext.h"
#include "vsbExplosionFXDirector.h"
#include "vsbChunkLODElevationManager.h"

#include "tool_hashstr.h"


//@@@@@
#include "vsbExplosion.h"

// Defines whether hash function's case sensitive
const bool gIgnoreCase	= true;



////////////////////////////////////////////////////////////////////////////////////////////
//  vsbAttachmentPoint
////////////////////////////////////////////////////////////////////////////////////////////


const vsbAttachmentPoint * vsbModelEntry::GetAttachmentPoint(long pt_id)  
{
	map<long,vsbAttachmentPoint>::iterator iter;
	iter = mAttachmentPoints.find(pt_id);
	if (iter != mAttachmentPoints.end())
		return &(iter->second);
	return 0;
}

const vsbAttachmentPoint * vsbModelEntry::GetAttachmentPoint(const string &pt_name) 
{
	
	long id = vsbAttachmentPoint::MakeId(pt_name);
	return GetAttachmentPoint(id);
	
}



////////////////////////////////////////////////////////////////////////////////////////////
//  vsbBaseViewEntity
////////////////////////////////////////////////////////////////////////////////////////////




// This constructor creates an entity with a default name, checking that
// the name is valid.

const std::string &vsbBaseViewEntity::ClassName() const
{
	static string class_name = "vsbBaseViewEntity";
	return class_name;
}


bool vsbBaseViewEntity::IsATypeOf(int typeId)
{
	return ((TypeId() & typeId) == typeId);
}

bool vsbBaseViewEntity::IsA(int typeId)
{
	return ((TypeId() | typeId) == typeId == TypeId());
}

bool vsbBaseViewEntity::IsThisType(vsbBaseViewEntity *entity)
{
	return ((TypeId() | entity->TypeId()) == TypeId() == entity->TypeId());
}


bool vsbBaseViewEntity::IsATypeOfThis(vsbBaseViewEntity *entity)
{
	return ((TypeId() & entity->TypeId()) == TypeId());
}

vsbBaseViewEntity::vsbBaseViewEntity(/*vsbEntityManager *em,*/ const string &name, bool postfixed):
mHandle(-1),mEntityRoot(0),mTypeID(vsbTypeBaseEntity())
{
//	mpEntityManager = em;

	 int			    unnamed_idx = 0;

	 const string prefix = name;
	string				auto_name;
	char				buffer[10];

	
	if (!vsbViewportContext::IsInitialised())
		throw vsbEntityException(ES_NoEntityManager);

	if (postfixed)
	{
		do
		{
			auto_name = prefix;
			auto_name += itoa(unnamed_idx, buffer, 10);

			if (!vsbViewportContext::EntityManager().LookupEntity(auto_name))
				break;

			unnamed_idx++;
		} while (true);
		ConstructorSetup(auto_name);
	}
	else ConstructorSetup(name);

}

void vsbBaseViewEntity::ConstructorSetup(const string &name)

{
	mHaveOrientation	= false;   
	mHaveVelocity		= false;		
	mHaveAngularRates	= false;

	mIsVisible			= false;		
	mIsActive			= false;		
	mIsMoving			= false;	
	
	mDoPostProcess		= false;

	mDeleteNextPass		= false;

	mLimitedLifecycle	= false;
	mForceInvisible     = false;

	mOutOfRangeBehavior[0] = EOR_Invisible;
	mOutOfRangeBehavior[1] = EOR_Invisible;

	mCurrTime			= 0;

    mTeam               = ETeam_Blue;
    mTrajDisplay        = ETrajectory_Default;

	mMinTime			= 0; // Effectively infinite range
	mMaxTime			=  10000.0f;

	memset(mEntityPosXYZ,	0,	sizeof(sgVec3));
	memset(mOrientationHPR,	0,	sizeof(sgVec3));
	memset(mVelocityXYZ,	0,	sizeof(sgVec3));
	memset(mAngularRatesHPR,0,	sizeof(sgVec3));
	

	mpPrev = 0;
	mpNext = 0;

	// set the name
	
	mName = name;

	if (vsbViewportContext::EntityManager().EntityCount() > 
		vsbViewportContext::EntityManager().HashSize()/2)
	{
		// Hash table is 50% saturated - do not allow any more 
		// objects to be created
		if ( !vsbViewportContext::EntityManager().HashTableResize())
			throw vsbEntityException(ES_TableSaturated);
	}

    EEntityManagerStatus entity_status = vsbViewportContext::EntityManager().AddEntity(this);
	if ( entity_status != EM_OK )
		throw vsbEntityException(ES_EntityManagerAddFailure);

	mEntityRoot = new vsbModelRoot;
	mEntityRoot->ref();

/*	vsbCompiledShape *hemi = new vsbCompiledShape;
	hemi->Scale(50);
	hemi->LengthScale(5);
	hemi->ShapeName("vsbCylinder");
	mEntityRoot->addKid(hemi);
*/
	
}

void vsbBaseViewEntity::DataFields(bool orientation, bool velocity, bool angRates)
{
	mHaveOrientation	= orientation;   
	mHaveVelocity		= velocity;		
	mHaveAngularRates	= angRates;

}


vsbBaseViewEntity::~vsbBaseViewEntity()
{
    DetachAllModels();
	if (vsbViewportContext::EnvironmentManager().SceneRoot()->searchForKid(mEntityRoot) >= 0)
		vsbViewportContext::EnvironmentManager().SceneRoot()->removeKid(mEntityRoot);
	ssgDeRefDelete(mEntityRoot); 
	if (!vsbViewportContext::IsInitialised()) return;
	vsbViewportContext::EntityManager().RemoveEntity(this);
}


void vsbBaseViewEntity::UpdateTransforms()
{
	assert(mEntityRoot);
    sgdCoord crd;

    //Convert to internal axes system  

//    sgCopyCoord(&crd,(sgCoord *)mEntityPosXYZ);
// FIXME:DOUBLE PRECISON.
	crd.xyz[0] = mEntityPosXYZ[0];
	crd.xyz[1] = mEntityPosXYZ[1];
	crd.xyz[2] = mEntityPosXYZ[2];

	crd.hpr[0] = mOrientationHPR[0];
	crd.hpr[1] = mOrientationHPR[1];
	crd.hpr[2] = mOrientationHPR[2];

	sgdMakeCoordMat4 ( mOrientationMat, 0,0,0, -mOrientationHPR[0]-90,mOrientationHPR[1],mOrientationHPR[2] ) ;


	vsbToScaledVSBCoords(crd);

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		sgdAddVec3(crd.xyz,vsbBaseCamera::CameraToOriginDelta());
	}


	if ( IsVisible() )
	{
		mEntityRoot->setTransform(&crd);
	}

	ClearAttachmentPointEvaluatedFlag();
	//cout << "Update Transforms (" << Name() << "):\n";
	//cout << "Time: " << CurrTime() << "\n";
	//cout << "  X=" << mEntityPosXYZ[0]   << ",   Y=" << mEntityPosXYZ[1]   << ",   Z=" << mEntityPosXYZ[2] << "\n";
	//cout << "Psi=" << mOrientationHPR[0] << ", The=" << mOrientationHPR[1] << ", phi=" << mOrientationHPR[2] << "\n";
}

//---------------------------------------------------------------
// HashValue()
// Convert a string to its hash value representation

int vsbBaseViewEntity::HashValue(const string &name, int hash_range) 
{

	const char* s =  name.c_str();
	long		h = 0L;

	while (*s)
	{
	  h = (h<<2) ^ ((gIgnoreCase)?toupper(*s++):*s++);
	}
    h = h & 0x7FFFFFFFL;
	return (h % hash_range);
}



void vsbBaseViewEntity::IsActive(bool trueFalse)			
{ 
	assert(vsbViewportContext::IsInitialised()); // should never be zero
	if (trueFalse!= mIsActive)
	{
		
		if (trueFalse && mIsVisible && !mForceInvisible)
		{
			// if active and visible, ensure this entitys subtree is in
			// the root scene graph

			if (vsbViewportContext::EnvironmentManager().SceneRoot()->searchForKid(mEntityRoot) < 0)
				vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(mEntityRoot);
		} else
		{
			// if not active or not visible, ensure this entity's subtree is not in 
			// the root scene graph
			if (vsbViewportContext::EnvironmentManager().SceneRoot()->searchForKid(mEntityRoot) >= 0)
				vsbViewportContext::EnvironmentManager().SceneRoot()->removeKid(mEntityRoot);
		}
		mIsActive = trueFalse;
	}
};


void vsbBaseViewEntity::IsVisible(bool trueFalse)			
{ 
	mIsVisible = trueFalse;
	assert(vsbViewportContext::IsInitialised()); // should never be zero
	if (mIsActive && mIsVisible && !mForceInvisible)
	{
		// if active and visible, ensure this entitys subtree is in
		// the root scene graph

		if (vsbViewportContext::EnvironmentManager().SceneRoot()->searchForKid(mEntityRoot)== -1)
			vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(mEntityRoot);
	} else
	{
		// if not active or not visible, ensure this entity's subtree is not in 
			// the root scene graph
		if (vsbViewportContext::EnvironmentManager().SceneRoot()->searchForKid(mEntityRoot) != -1)
			vsbViewportContext::EnvironmentManager().SceneRoot()->removeKid(mEntityRoot);
	}
};


void vsbBaseViewEntity::SetCurrTime(float timeto) 
{
	mCurrTime = timeto;
	//cout << Name() << ": SetCurrTime t = " << timeto << endl;
};

void vsbBaseViewEntity::AdvanceTime(float deltaTime) 
{
	mCurrTime += deltaTime;
	//cout << Name() << ": AdvanceTime dt = " << deltaTime << endl;
};

void vsbBaseViewEntity::PostProcessFunc()			 
{
	//cout << Name() << ": Post process update " << endl;
};			


void vsbBaseViewEntity::CheckBounds()
{
	IsVisible(true);
	if (!mLimitedLifecycle) return;


	float t = vsbViewportContext::EntityManager().CurrTime();
	// t = CurrTime();
	if (t < MinTime() || t > MaxTime())
	{
		if (t < MinTime())
		{
			switch(mOutOfRangeBehavior[0])
			{
			case EOR_Invisible:
				IsVisible(false);
				break;
			case EOR_Delete: 
				DeleteNextPass(true);
				break;
			case EOR_Custom:
				TimeBoundsExceededFunc(0,true);
				break;

			default:
				break;
			}
			// mDeleteNextPass to delete
		} else 
		{
			switch(mOutOfRangeBehavior[1])
			{
			case EOR_Invisible:
				IsVisible(false);
				break;
			case EOR_Custom:
				TimeBoundsExceededFunc(1,true);
			case EOR_Delete: 
				DeleteNextPass(true);
				break;
			default:
				break;

			}
		}
	} else 
	{
		
		switch(mOutOfRangeBehavior[0])
		{
		case EOR_Invisible:
			break;
		case EOR_Custom:
			TimeBoundsExceededFunc(0,false);
		case EOR_Delete:
			IsVisible(false);
			break;
		default:
			break;

		}

		switch(mOutOfRangeBehavior[1])
		{
		case EOR_Invisible:
			break;
		case EOR_Custom:
			TimeBoundsExceededFunc(1,false);
		case EOR_Delete:
			IsVisible(false);
			break;
		default:
			break;

		}
	}

	if (mForceInvisible) IsVisible(false);
}

bool vsbBaseViewEntity::AttachModel(const string &modelName, bool clear)
{
	assert(vsbViewportContext::IsInitialised());
	list<string>::iterator iter;




	if (clear) 
		DetachAllModels();
	else if (!mModelNames.empty())
	{

		for (iter = mModelNames.begin(); iter != mModelNames.end(); iter++)
			if (*iter == modelName)
				return true;
	}

	if (!vsbViewportContext::ModelManager().Attach(modelName, this))
		return false;

	mModelNames.push_back(modelName);

	return true;
}



bool vsbBaseViewEntity::DetachModel(const string &modelName)
{
	assert(vsbViewportContext::IsInitialised());
	list<string>::iterator iter;  
	if (!mModelNames.empty())
	{
		
		for (iter = mModelNames.begin(); iter != mModelNames.end(); iter++)
			if (*iter == modelName)
			{
				vsbViewportContext::ModelManager().Detach(modelName, this);
				mModelNames.erase(iter);
				return true;
			}	
		
	} 
	return true;
	
}

void vsbBaseViewEntity::DetachAllModels()
{
	assert(vsbViewportContext::IsInitialised());
	list<string>::iterator iter = mModelNames.begin();
	if (iter!= mModelNames.end()) 
	do 
	{
		string model_name = *iter;
		vsbViewportContext::ModelManager().Detach(model_name, this);
		iter++;
	} while (iter!= mModelNames.end());
	mModelNames.clear();

}

const string &vsbBaseViewEntity::AttachedModelName(int index) 
{
	static string default_name = "";


	if (index < 0 && index >= mModelNames.size()) 
		return default_name;

	if (mModelNames.empty()) 
		return default_name;

	list<string>::iterator iter = mModelNames.begin();

	for (int i=0; i<index; i++, iter++);
	if (iter != mModelNames.end())
		return (*iter);
	return default_name;
}


void vsbBaseViewEntity::DeleteAllAttachmentPoints()
{
	mAttachmentPoints.clear();
}

void vsbBaseViewEntity::DeleteAttachmentPoint(std::string attachmentName)
{
	DeleteAttachmentPoint(vsbAttachmentPoint::MakeId(attachmentName));
}

void vsbBaseViewEntity::DeleteAttachmentPoint(long attachmentId)
{
	map<long,vsbEntityAttachmentPoint>::iterator iter;
	iter = mAttachmentPoints.find(attachmentId);
	if (iter != mAttachmentPoints.end())
		mAttachmentPoints.erase(iter);
}

void vsbBaseViewEntity::ClearAttachmentPointEvaluatedFlag()
{
	map<long,vsbEntityAttachmentPoint>::iterator iter;
	for (iter =  mAttachmentPoints.begin(); iter != mAttachmentPoints.end(); iter++)
		iter->second.IsEvaluated(false);
}

void vsbBaseViewEntity::CopyAttachmentPoints(map<long,vsbAttachmentPoint> &attachment_points)
{
	mAttachmentPoints.clear();
	map<long,vsbAttachmentPoint>::iterator iter;
	for(iter = attachment_points.begin(); iter!=attachment_points.end(); iter++)
	{
		vsbEntityAttachmentPoint eap;
		eap = iter->second;
		mAttachmentPoints.insert(pair<long,vsbEntityAttachmentPoint>(iter->first,eap));
	}
}

bool vsbBaseViewEntity::AttachmentPointExists(int pos_id)
{
	if (!mAttachmentPoints.size()) return false;
	map<long,vsbEntityAttachmentPoint>::iterator iter;
	iter = mAttachmentPoints.find(pos_id);
	return (iter != mAttachmentPoints.end());

}

bool vsbBaseViewEntity::AttachmentPointName(int pos_id, std::string &pos_name)
{
	if (!mAttachmentPoints.size()) return false;
	map<long,vsbEntityAttachmentPoint>::iterator iter;
	iter = mAttachmentPoints.find(pos_id);
	if (iter == mAttachmentPoints.end()) 
		return false;
	pos_name = iter->second.Name();
	return true;
	
}


bool vsbBaseViewEntity::AttachmentPointExists(std::string &pos_name)
{
	return AttachmentPointExists(vsbAttachmentPoint::MakeId(pos_name));
}

bool vsbBaseViewEntity::GetAttachmentPointPos(sgdVec3 pos, int pos_id)
{
	if (!mAttachmentPoints.size()) 
		return false;
	map<long,vsbEntityAttachmentPoint>::iterator iter;

	iter = mAttachmentPoints.find(pos_id);
	if (iter == mAttachmentPoints.end()) 
		return false;

	vsbEntityAttachmentPoint &attachment = iter->second;
	
	if (attachment.IsEvaluated())
		sgdCopyVec3(pos,attachment.Point());
	else
	{

		sgdXformPnt3(pos,attachment.TemplatePoint(), mOrientationMat);
		//vsbFromScaledVSBCoords(pos);
		pos[1] = -pos[1];
		pos[2] = -pos[2];
			

		pos[0]=mEntityRoot->getScale()*pos[0]+ EntityPosXYZ()[0];
		pos[1]=mEntityRoot->getScale()*pos[1]+ EntityPosXYZ()[1];
		pos[2]=mEntityRoot->getScale()*pos[2]+ EntityPosXYZ()[2];
		sgdCopyVec3(attachment.Point(),pos);
		attachment.IsEvaluated(true);

	}
	return true;
	
}

bool vsbBaseViewEntity::GetAttachmentPointPos(sgdVec3 pos, std::string &pos_name)
{
	return GetAttachmentPointPos( pos, vsbAttachmentPoint::MakeId(pos_name));
}
