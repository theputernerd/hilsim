#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbTrajEntitySTLVector.cpp
    \brief Defines an entity class derived from vsbTrajEntity that has the 
	ability to retain a temporal state history	This retention is acheived via 
	STL vectors.

	Pros:
	Access is relatively fast as each point can be found by offset
	Adding new points is fast but is subject to the following cons.

	Cons:
	Data points must be inserted in increasing and equidistant time steps
	
 
   	\sa vsbTrajEntitySTLVector.h

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#include <string>
#include <set>
#include <iostream>
#include <assert.h>

using namespace std;

#include "ssg.h"
#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbModelRoot.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"
#include "vsbInterpEuler.h"
#include "vsbTrajEntitySTLVector.h"


/////////////////////////////////////////////////////////////////

const std::string &vsbTrajEntitySTLVector::ClassName() const
{
	static string class_name = "vsbTrajEntitySTLVector";
	return class_name;
}


vsbTrajEntitySTLVector::vsbTrajEntitySTLVector(const string &name, bool postfixed):
vsbTrajEntity(name,postfixed)
{
	mTypeID = vsbTypeTrajEntityVector(); // initialise the type id
	mReserveVectorSize = 1000;
	mVectorSizeIncrease = 500;

	mTraj.reserve(mReserveVectorSize);
}

vsbTrajEntitySTLVector::~vsbTrajEntitySTLVector()
{

}

void vsbTrajEntitySTLVector::ClearTraj()
{
	mTraj.clear();
}


void vsbTrajEntitySTLVector::SetCurrTime(float timeto)
{
	if (mTraj.size() == 0) return;

	float time0 = mTraj[0][0];
	float time1 = mTraj[mTraj.size()-1][0];

	if (timeto <= time0)
	{
		mCurrData = mTraj[0];
		mTrajVarDef.UpdateInstanceData(mCurrData);
		mCurrTime = timeto;
	} 
	else if ( timeto >= time1)
	{
		mCurrData = mTraj[mTraj.size()-1];
		mTrajVarDef.UpdateInstanceData(mCurrData);
		mCurrTime = timeto;
	}
	else
	{
		float steps = (timeto-time0)/mTimeStepSize;
		int lower = floor(steps);
		int upper = lower+1;
		if( upper >= mTraj.size() )
		{
			// Cut and paste of the "else if ( timeto >= time1)" block
			mCurrData = mTraj[mTraj.size()-1];
			mTrajVarDef.UpdateInstanceData(mCurrData);
			mCurrTime = timeto;
		}
		else
		{
			float proportion = steps-lower;
			
			mCurrData = mTraj[lower].Interpolate(mTraj[upper], proportion, mTrajVarDef.InterpStyle());
			mTrajVarDef.UpdateInstanceData(mCurrData);
			mCurrTime = timeto;
		}

	}

	CheckBounds();
	return;
}

void vsbTrajEntitySTLVector::AdvanceTime(float deltaTime)
{
	float timeto = CurrTime() + deltaTime;
	SetCurrTime(timeto);
}

bool vsbTrajEntitySTLVector::AddVar(const string &varname, double *addr, EInterpolationStyle interpStyle )
{
	if (mTraj.size()>0) return false;  // Mustn't be done whilst data is present in the trajectory
	return vsbTrajEntity::AddVar( varname, addr,  interpStyle );
}


int InTolerance(double a, double b, double tol)
{
	if (fabs(a - b) <= tol) return 0;
	if (a>b) return 1;
	return -1;
}


bool vsbTrajEntitySTLVector::AddNewTime(float atTime, bool updateFromState)
{
	int curr_count = mTraj.size();

	if (curr_count == mTraj.capacity())
		mTraj.reserve(curr_count+mVectorSizeIncrease);

	if (curr_count == 0)
	{
		mCurrTime = atTime;
		if (updateFromState) UpdateInstanceDataFromState();
		
	} 

	else if (curr_count == 1)
	{
		if (atTime <= mCurrTime) return false;
		mTimeStepSize = atTime;
		mTimeStepSize-= mTraj[0][0];

		mCurrTime = atTime;
		if (updateFromState) UpdateInstanceDataFromState();

	} else
	{
		double last_time = mTraj[curr_count-1][0];

//		assert(InTolerance(last_time+mTimeStepSize, atTime, 1.0e-4)==0);

		mCurrTime = mTimeStepSize * curr_count + mTraj[0][0]; // atTime;
		if (updateFromState) UpdateInstanceDataFromState();
	}

	mTraj.push_back(mCurrData); 
	mLastData = mCurrData;
	return true;
  
}


bool	vsbTrajEntitySTLVector::FirstInstanceData(vsbInstanceData &data)
{
	mSteppingIter = mTraj.begin();
	if (mSteppingIter == mTraj.end())
		return false;
	data = *mSteppingIter;
	return true;
}

bool	vsbTrajEntitySTLVector::LastInstanceData(vsbInstanceData &data)
{
	mSteppingIter = mTraj.end();
	mSteppingIter--;
	if (mSteppingIter == mTraj.end())
		return false;
	data = *mSteppingIter;
	return true;
}

bool	vsbTrajEntitySTLVector::NextInstanceData(vsbInstanceData &data)
{
	mSteppingIter++;
	if (mSteppingIter == mTraj.end())
		return false;
	data = *mSteppingIter;
	return true;
}

bool	vsbTrajEntitySTLVector::PrevInstanceData(vsbInstanceData &data)
{
	mSteppingIter++;
	if (mSteppingIter == mTraj.end())
		return false;
	data = *mSteppingIter;
	return true;
}

void	vsbTrajEntitySTLVector::ReserveSpace(int count)
{
	if (count > mTraj.capacity()) mTraj.reserve(count);
}