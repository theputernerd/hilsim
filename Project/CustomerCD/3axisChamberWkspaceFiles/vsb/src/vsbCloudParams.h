//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCloudParams.h
    \brief Defines the class vsbCloudParams
 
	This file defines the vsbCloudParams class which encapsulates all the 
    information necessary for the construction and manipulation of cloud 
    layers, however it does not define any cloud implementation methods.
 	
	\sa vsbCloudParams.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbCloudParams_H
#define _vsbCloudParams_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef parserH
#include "parser.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif


#define MAX_CLOUD_LAYERS 5

#define MAX_CLOUD_TYPES 5

//! Cloud type enumeration
enum CloudTypeEnum 
{
    CLOUD_OVERCAST = 0,
    CLOUD_MOSTLY_CLOUDY = 1,
	CLOUD_MODERATELY_CLOUDY = 2,
    CLOUD_MOSTLY_SUNNY = 3,
    CLOUD_CIRRUS = 4
};

class DLL_VSB_API vsbCloudLayer;
class DLL_VSB_API vsbCloudParamsParser;


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbCloudParams
    \brief Cloud parameters class
 
	The vsbCloudParams class encapsulates all the information necessary 
    for the construction and manipulation of cloud layers, however it 
    does not define any cloud implementation methods.


    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbCloudParams
{
	friend class vsbCloudLayer;
	friend class vsbCloudParamsParser;

private:

	float		  mAlt;			//!< Altitude of the cloud layer
	float		  mThickness;	//!< Thickness of the cloud layer (abover mAlt)
	float	  	  mTransition;  //!< Thickness of transition into cloud layer
	float		  mLength;		//!< Cloud length in M from one horizon to the next	
	CloudTypeEnum mCloudType;	//!< Cloud type 
	float		  mScale;		//!< A facor by which cloud textures are stretched

	bool		  mModified;    //!< flag indicating change in any of the parameters
    bool		  mNeedRebuild;    //!< flag indicating change in any of the parameters

public:

	//-------- Standard member functions

                    //! Constructor
	                vsbCloudParams();

                    //! Copy constructor
	                vsbCloudParams(vsbCloudParams &cp);

                    //! Assignment operator
	vsbCloudParams	&operator = (const vsbCloudParams& cp);

                    //! Comparison operator overloading "=="
	bool			operator == (const vsbCloudParams& cp) const;

                    //! Comparison operator overloading "!="
	bool			operator != (const vsbCloudParams& cp) const {return !(*this == cp);};

	//-------- Class members

				 //! Set the default cloud parameters 
	void		 SetDefault();

    //-------- Enquire & Set accessor functions

                  /*! \brief Convenience function to set primary cloud parameters
                     \param cloudType   
                     \param alt Altitude of the cloud layer
                     \param thickness Thickness of the cloud layer
                     \param transition Thickness of the transitional layers above and below cloud
                  */
	void		  Set(CloudTypeEnum cloudType, double alt, double thickness, double transition);
 
                  //! Set cloud layer altitude  
	void		  Alt(double alt)			{ mAlt = (float) alt; mModified = true; }; 

                  //! Get cloud layer altitude     
    float		  Alt() const			    { return mAlt; };	

                   //! Set cloud layer thickness
 	void		  Thickness(double th)		{ mThickness = (float) th; mModified = true; }; 

                  //! Get cloud layer thickness  
	float		  Thickness() const			{ return mThickness; };	

                  //! Set cloud layer transition thickness 
	void		  Transition(double tr)		{ mTransition = (float) tr; mModified = true; }; 

                  //! Get cloud layer transition thickness  
	float		  Transition() const		{ return mTransition; };	

                  //! Set cloud layer length  
	void		  Length(double len)		{ mLength = (float) len; mModified = true; }; 

                  //! Get cloud layer length  
	float		  Length() const			{ return mLength; };	

                  //! Set cloud layer texture scale  
	void		  Scale(double s)		    { mScale = (float) s; mModified = true; }; 

                  //! Get cloud layer texture scale   
	float		  Scale() const			    { return mScale; };	
 
                   //! Set cloud type 
	void		  CloudType(CloudTypeEnum ct) { mNeedRebuild = (mCloudType != ct);
												mCloudType = ct; 
												mModified = true; };

                  //! Get cloud type  
	CloudTypeEnum CloudType() const			{ return mCloudType; } ;

                  //! Get the parameter modification status of the cloud layer
	bool		  IsModified() const		{ return mModified; };

    bool         NeedRebuild() const        { return mNeedRebuild; };

	void		 NeedRebuild(bool trueFalse) { mNeedRebuild = trueFalse; 
													  if (mNeedRebuild) mModified = true;};

protected:
                  //! Clear the parameter modification status of the cloud layer
	void		  ClearModified() 			{ mNeedRebuild = mModified = false; };


};

class DLL_VSB_API vsbCloudParamsParser: public Parser::TParseObjectBase
{
public:
	vsbCloudParamsParser(vsbCloudParams &eo);
	virtual ~vsbCloudParamsParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
	vsbParserHelper *mpOutput;

	vsbCloudParams &mrCloudParams;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};

#endif