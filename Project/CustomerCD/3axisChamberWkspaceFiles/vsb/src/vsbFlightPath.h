#ifndef _vsbFlightPath_H
#define _vsbFlightPath_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif




class DLL_VSB_API vsbFlightPath;

class DLL_VSB_API vsbFlightPathVertexArray: public ssgVertexArray
{
friend class DLL_VSB_API vsbFlightPath; // to access setNum
public:

	vsbFlightPathVertexArray ( int init = 3 )
	{
		limit = init ;
		size_of = sizeof(sgVec4);
		total = 0 ;
		list = new char [ limit * size_of ] ;
		type |= _SSG_TYPE_VERTEXARRAY ;
	} 

  void   add ( sgVec4   thing ) ;
  void   add ( float x, float y, float z, float t );
  void   add ( sgVec3 thing, float t );
  void   set ( sgVec4   thing, unsigned int n ) ;
  void   set ( float x, float y, float z, float t, unsigned int n );

  bool   find(int &idx, float t);
  bool   find(int &idx, int starting_at, float t);
  float *find(float t);
  void   copy( ssgSimpleList *src, int clone_flags );
private:
  void binarysearch(int &fisrt, int &last, float time);
  void setNum(int num) {total = num;};
  
  
};


class DLL_VSB_API vsbFlightPath
{
public:
	vsbFlightPath();
	vsbFlightPath(const vsbFlightPath &src);
	virtual ~vsbFlightPath();
	vsbFlightPath &operator = (const vsbFlightPath &src);

	void Clear();
	void AddPoint(float time, const sgVec3 xyz);
	void AddPoint(float time,  const sgMat4 mat);
	void AddPoint(float time,  const sgCoord coord);

	void SetMaxDisplayTime(float t);

private:
	float mTrueFlightPathVertexCount;
	vsbFlightPathVertexArray *mpFlightPath;

    
};


#endif