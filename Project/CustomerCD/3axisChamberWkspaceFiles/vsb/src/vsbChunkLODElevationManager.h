////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbModelManager.h
    \brief Defines the class vsbModelManager and itas auxillary classes
    
     The class vsbModelManager is responsible for managing and caching 3D 
	 mesh models that may be attached to vsbBaseViewEntity derrived objects.
	 
	\sa vsbModelManager.cpp

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifndef _vsbChunkLODElevationManager_H
#define _vsbChunkLODElevationManager_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#include "chunklod.h"
#include "chunklod_tqt.h"



class DLL_VSB_API vsbViewportContext;

class DLL_VSB_API vsbChunkLODElevationEntry
{
friend class DLL_VSB_API vsbChunkLODElevationListParser;
friend class DLL_VSB_API vsbChunkLODElevationManager; 

public:	

	vsbChunkLODElevationEntry();
	virtual ~vsbChunkLODElevationEntry();

	vsbChunkLODElevationEntry(const vsbChunkLODElevationEntry &from);
	vsbChunkLODElevationEntry &operator=(const vsbChunkLODElevationEntry &from);

	void CopyParameters(const vsbChunkLODElevationEntry &from);

	void LogicalName(const std::string &name) {mLogicalName = name;	mNeedReload = true;};
	void GeometryFileName(const std::string &name)	{ mGeometryFileName = name;	mNeedReload = true;};
	void TextureFileName(const std::string &name) { mTextureFileName = name; mNeedReload = true;};

	const std::string	&LogicalName() const { return mLogicalName ;};
	const std::string	&GeometryFileName()	const	{return mGeometryFileName;};
	const std::string	&TextureFileName() const	{return mTextureFileName;};

	void Morph(bool truefalse) {mMorph = truefalse; mIsModified = true;}
	bool Morph() {return mMorph;}

	void WireFrame(bool truefalse) {mMorph = truefalse; mIsModified = true;};
	bool WireFrame() {return mMorph;};

	void TexelSize(float ts)  {mTexelSize = ts; mIsModified = true;};
	float TexelSize() {return mTexelSize;};

	void MaxPixelError(float pe){mMaxPixelError = pe; mIsModified = true;};
	float MaxPixelError() {return mMaxPixelError;};


	void  SetModified() {mIsModified = true;};
	void  SetNeedUpdate() {mNeedReload = true;};	
	bool  NeedUpdate() const {return mNeedReload;};
	bool  IsModified() const {return mIsModified;};


	const sgMat4 &AlignmentMat() const { return	mAlignmentMat; }
	void  AlignmentMat(sgMat4 m) {sgCopyMat4(mAlignmentMat,m); mIsModified = true;}

	

protected:
	bool				mNeedReload;
	bool				mIsModified; 
	bool				mMorph;
	bool				mWireFrame;
	float				mTexelSize;
	float				mMaxPixelError;
	std::string			mLogicalName;                //!< The logical name of the model
	std::string			mGeometryFileName;           //!< The base elevation file name (path exclusive)
	std::string			mTextureFileName;		     //!< The list of available textures (index might 
	sgMat4				mAlignmentMat;               //!< The realignment matrix used to position and orient the terrain in world coordinates
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbChunkLODElevation : public vsbChunkLODElevationEntry
{
	friend class DLL_VSB_API vsbChunkLODElevationManager; 
public:
	vsbChunkLODElevation();
	virtual ~vsbChunkLODElevation();
	vsbChunkLODElevation(const std::string &logical_name);
	vsbChunkLODElevation(const vsbChunkLODElevationEntry &elevation_entry);

	vsbChunkLODElevation &operator = (const std::string &logical_name);
	vsbChunkLODElevation &operator = (const vsbChunkLODElevationEntry &elevation_entry);


	void  SetViewportContext(vsbViewportContext *vpcontext) {mpVieportContext = vpcontext;};

	void  UpdatePosition();
	void  Draw();

	bool  IsPointAboveGround(sgVec3 pt); // coordinates in vsb z+ up axis system

	bool  IsLoaded() const {return mIsLoaded;};
	lod_chunk_tree*	Geometry() { return mGeometry; };

protected:

	vsbViewportContext *mpVieportContext;
	chunklod::tqt*	mTextureQuadtree;
	chunklod::lod_chunk_tree*	mGeometry;

	std::string mCurrent;
	bool mIsLoaded;


};



///////////////////////////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbChunkLODElevationManager
{
friend class vsbChunkLODElevationListParser;
friend class vsbChunkLODElevation;
public:

			/*! \brief Constructor

			On construction of the vsbModelManager the model index file is 
			parsed, creating a database of 3D mesh models and pertinant attributes
			for those models.

			\note VSB only allows one instance of this class per application, the
			assumption being that theres only one scene per application.

			\sa LoadIndex()
			*/
			vsbChunkLODElevationManager();

			/*! \brief Virtual Destructor
			*/
	virtual ~vsbChunkLODElevationManager();

			/*! \brief Load the chunkLOD elevation index files 
			*/
	void	LoadIndex();

	int		Count() const {return mCount;};

	vsbChunkLODElevationManager  &operator=(std::string new_name);
	vsbChunkLODElevationManager  &operator=(vsbChunkLODElevationEntry &elevEntry);
	
	vsbChunkLODElevationEntry *	Find(const std::string &new_name);
	const std::string *		FindFirstName();
    const std::string *	FindNextName();
	vsbChunkLODElevationEntry *FindFirst();
	vsbChunkLODElevationEntry *FindNext();


private:
			int										mCount;				//!< number of available elevation models

		    static int								mInstanceCount;		//!< Number of instances of this class

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 

#       ifdef _MSC_VER
#       pragma warning( push )
#       pragma warning( disable : 4251 ) // STL not exported warning
#       endif

        std::map<std::string, vsbChunkLODElevationEntry>	mElevationList;			//!< list of elevation model specification entries
        std::map<std::string, vsbChunkLODElevationEntry>::iterator mElevationListIter;		//!< iterator for aove


#       ifdef _MSC_VER
#       pragma warning( pop )
#       endif

};		





#endif