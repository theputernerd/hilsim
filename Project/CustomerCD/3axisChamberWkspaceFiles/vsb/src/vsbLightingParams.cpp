//////////////////////////////////////////////////////////////////
/*! 
    \file vsbLightingParams.cpp
    \brief Implements the class vsbLightingParams  
	
	The vsbLightingParams class maintains a record of the current sun 
    angle and declination and can be used to determine the lighting and 
    colours of the sky sun and other atmospheric phenomena    

	\sa vsbLightingParams.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "vsbLightingParams.h"


vsbLightingParams gCurrLightParams;

// Lighting look up tables (based on sun angle with local horizon)

static int table_size = 23;

static double sky_brightness_table[][2] = 
{
	{0.0,  1.000},	{10.0, 1.000},	{20.0, 1.000},	{30.0, 1.000},	{40.0, 1.000},	
	{50.0, 0.997},	{60.0, 0.989},	{70.0, 0.962},	{80.0, 0.895},	{85.0, 0.806},	
	{90.0, 0.616},	{95.0, 0.350},	{100.0, 0.200},	{105.0, 0.120},	{110.0, 0.110},	
	{115.0, 0.090},	{120.0, 0.090},	{130.0, 0.090},	{140.0, 0.080},	{150.0, 0.080},	
	{160.0, 0.080},	{170.0, 0.080},	{180.0, 0.080}
};

static double lighting_diffuse_table[][2] = 
{
	{0.0, 1.000},	{10.0, 1.000},	{20.0, 1.000},	{30.0, 1.000},	{40.0, 1.000},
	{50.0, 0.999},	{60.0, 0.995},	{70.0, 0.977},	{80.0, 0.914},	{85.0, 0.849},
	{90.0, 0.748},	{95.0, 0.500},	{100.0, 0.200},	{105.0, 0.100},	{110.0, 0.030},
	{115.0, 0.000},	{120.0, 0.000},	{130.0, 0.000},	{140.0, 0.000},	{150.0, 0.000},
	{160.0, 0.000},	{170.0, 0.000},	{180.0, 0.000}
};

static double lighting_ambient_table[][2] = 
{
	{0.0, 0.140},	{10.0, 0.160},	{20.0, 0.180},	{30.0, 0.200},	{40.0, 0.220},	
	{50.0, 0.240},	{60.0, 0.260},	{70.0, 0.280},	{80.0, 0.300},	{85.0, 0.320},	
	{90.0, 0.220},	{95.0, 0.182},	{100.0, 0.100},	{105.0, 0.030},	{110.0, 0.010},	
	{115.0, 0.000},	{120.0, 0.000},	{130.0, 0.000},	{140.0, 0.000},	{150.0, 0.000},	
	{160.0, 0.000},	{170.0, 0.000},	{180.0, 0.000}
};



// if the 4th field is 0.0, this specifies a direction ...
static float white[4]		   = { 1.0f, 1.0f, 1.0f, 1.0f };

// base sky color
static float base_sky_color[4] = {0.30f, 0.30f, 0.80f, 1.0f};

// base fog color
static float base_fog_color[4] = {0.90f, 0.90f, 1.00f, 1.0f};



// Destructor
vsbLightingParams::vsbLightingParams( void ) 
{
	Init();
}

// Destructor
vsbLightingParams::~vsbLightingParams( void ) 
{
}


static float Interpolate(double table[][2], int tableSize, float indexVal)
{
	int i;
	if (indexVal <= table[0][0]) return (float) table[0][1];
	if (indexVal >= table[tableSize-1][0]) return (float) table[tableSize-1][1];
	for (i=0; i< 21; i++)
	{
		if (indexVal > table[i][0] && indexVal <= table[i+1][0] )
		{
			double frac =  (indexVal - table[i][0])/(table[i+1][0] - table[i][0]); 
			frac =  table[i][1] +  frac*(table[i+1][1] - table[i][1]);
			return (float) frac;
		}	
    }
	return (float) table[0][1];
}


// initialize lighting tables
void vsbLightingParams::Init( void ) 
{
	Update(45.0f, 0.0f);
}


// update lighting parameters based on current sun position
void vsbLightingParams::Update( void ) 
{

    float deg, ambient, diffuse, sky_brightness;

	// calculate lighting parameters based on sun's relative angle to
    // local up

    deg = fabsf(mSunAngle - 90.0f); 

    ambient =        Interpolate(lighting_ambient_table, table_size, deg)*0.8f;
	diffuse =        Interpolate(lighting_diffuse_table, table_size, deg);
	sky_brightness = Interpolate(sky_brightness_table, table_size, deg);
   
    // sky_brightness = 0.15;  // used to force a dark sky (when testing)

     //if ( ambient < 0.02 ) { ambient = 0.02; }
     //if ( diffuse < 0.0 ) { diffuse = 0.0; }
     //if ( sky_brightness < 0.1 ) { sky_brightness = 0.1; }

    mSceneAmbient[0] = white[0] * ambient;
    mSceneAmbient[1] = white[1] * ambient;
    mSceneAmbient[2] = white[2] * ambient;
    mSceneAmbient[3] = 1.0f;


	// old code
	/*
    mSceneDiffuse[0] = white[0] * diffuse;
    mSceneDiffuse[1] = white[1] * diffuse;
    mSceneDiffuse[2] = white[2] * diffuse;
    mSceneDiffuse[3] = 1.0;
	*/

	
	float sun_angle = deg * (float)SGD_DEGREES_TO_RADIANS;
	double x_10 = pow(sun_angle, 10);
	float amb = (float)(0.4 * pow (1.1, - x_10 / 30.0));
	if (amb < 0.3) { amb = 0.3f; }
	if (amb > 1.0) { amb = 1.0f; }
	
	
	sgSetVec4( mSceneDiffuse,
		((amb * 6.0f)  - 1.0f)* diffuse, // minimum value = 0.8
		((amb * 11.0f) - 3.0f)* diffuse, // minimum value = 0.3
		((amb * 12.0f) - 3.6f)* diffuse, // minimum value = 0.0
		1.0f );
	
	if (mSceneDiffuse[0] > 1.0) mSceneDiffuse[0] = 1.0;
	if (mSceneDiffuse[1] > 1.0) mSceneDiffuse[1] = 1.0;
	if (mSceneDiffuse[2] > 1.0) mSceneDiffuse[2] = 1.0;



    // set sky color
    mSkyColour[0] = base_sky_color[0] * sky_brightness;
    mSkyColour[1] = base_sky_color[1] * sky_brightness;
    mSkyColour[2] = base_sky_color[2] * sky_brightness;
    mSkyColour[3] = base_sky_color[3];

    // set fog color
    mFogColour[0] = base_fog_color[0] * sky_brightness;
    mFogColour[1] = base_fog_color[1] * sky_brightness;
    mFogColour[2] = base_fog_color[2] * sky_brightness;
    mFogColour[3] = base_fog_color[3];

	AdjForSunriseOrSet();


}


// calculate fog color adjusted for sunrise/sunset effects
void vsbLightingParams::AdjForSunriseOrSet( void ) 
{
    //FGInterface *f;
    float sun_angle_deg, rotation, param1[3], param2[3];
	
    // set fog color (we'll try to match the sunset color in the
    // direction we are looking
	
    // first determine the difference between our view angle and local
    // direction to the sun
	
    // next check if we are in a sunset/sunrise situation
    sun_angle_deg  = fabsf(mSunAngle - 90.0f); 
    if ( (sun_angle_deg > 80.0f) && (sun_angle_deg < 100.0f) ) 
	{
		/* 0.0 - 0.6 */
		param1[0] = (10.0f - fabsf(90.0f - sun_angle_deg)) / 20.0f;
		param1[1] = (10.0f - fabsf(90.0f - sun_angle_deg)) / 40.0f;
		param1[2] = (10.0f - fabsf(90.0f - sun_angle_deg)) / 30.0f;
	} else 
	{
		param1[0] = param1[1] = param1[2] = 0.0f;
    }
	
	rotation = sun_angle_deg;
	if ( rotation - 180.0f <= 0.0f ) 
	{
		param2[0] = param1[0] * (180.0f - rotation) / 180.0f;
		param2[1] = param1[1] * (180.0f - rotation) / 180.0f;
		param2[2] = param1[2] * (180.0f - rotation) / 180.0f;
		// printf("param1[0] = %.2f param2[0] = %.2f\n", param1[0], param2[0]);
    } else 
	{
		param2[0] = param1[0] * (rotation - 180.0f) / 180.0f;
		param2[1] = param1[1] * (rotation - 180.0f) / 180.0f;
		param2[2] = param1[2] * (rotation - 180.0f) / 180.0f;
		// printf("param1[0] = %.2f param2[0] = %.2f\n", param1[0], param2[0]);
    }
	
    mAdjFogColour[0] = mFogColour[0] + param2[0];
    if ( mAdjFogColour[0] > 1.0f ) { mAdjFogColour[0] = 1.0f; }
	
    mAdjFogColour[1] = mFogColour[1] + param2[1];
    if ( mAdjFogColour[1] > 1.0f ) { mAdjFogColour[1] = 1.0f; }
	
    mAdjFogColour[2] = mFogColour[2] + param2[2];
    if ( mAdjFogColour[2] > 1.0f ) { mAdjFogColour[2] = 1.0f; }
	
    mAdjFogColour[3] = mFogColour[3];
}




void vsbLightingParams::Update(float sunAngle, float sunDeclinationN)
{
	mSunAngle = sunAngle;
	mSunDeclinationNorth = sunDeclinationN;
	
	// update lighting parameters based on current sun position
    Update();
	
}

