#ifdef _MSC_VER
#include <windows.h>
#endif
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSysPath.cpp
    \brief Implements the vsbGetSysPath() function.

    \sa vsbSysPath.h

    \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#include <iostream> 
#include <string>
using namespace std;

#include "ul.h"

const char *SysEnvironmentVar = "VIS3D_ROOT"; //< The VSB root data directory environment variable

#include "vsbSysPath.h"

static string sSysPathString = ""; 

DLL_VSB_API void SpecifySysPath(const string &str)
{
    sSysPathString = str;
}

#ifdef _MSC_VER

/*!
	\brief Get the fully qualified path to the executable
	\note Only available on Win32 platforms
*/
DLL_VSB_API const string &GetExecutableDir()
{
	static string exe_string;
	static once = false;
	char szFullPath[_MAX_PATH];
	char szDir[_MAX_DIR];
	char szDrive[_MAX_DRIVE];

	if (!once)
	{
		::GetModuleFileName(NULL, szFullPath, _MAX_PATH);
		_splitpath(szFullPath, szDrive, szDir, NULL, NULL);
		exe_string = (char *)szDrive;
		exe_string += (char *)szDir;
		once = true;
	}
	
	// Erase any trailing / or \ from the path
	if (exe_string[exe_string.length()-1] == '/' || exe_string[exe_string.length()-1] == '\\')
	  exe_string.erase(exe_string.length()-1, 1);

	return exe_string;
}
#endif



DLL_VSB_API const string &vsbGetSysPath()
{
	static string syspath;
	static bool once = false;

	if (!once)
	{
		const char *pathstr;

        if (sSysPathString != "") 
            pathstr = sSysPathString.c_str();
        else
            pathstr = getenv(SysEnvironmentVar);

		if (pathstr)
		{
			syspath = pathstr;
			
			// Erase any trailing / or \ from the path
			if (syspath[syspath.length()-1] == '/' ||  syspath[syspath.length()-1] == '\\')
				syspath.erase(syspath.length()-1, 1);
			
		} else
		{
#ifdef _MSC_VER
			syspath = GetExecutableDir();
			
			// Erase any trailing / or \ from the path
			if (syspath[syspath.length()-1] == '/' || syspath[syspath.length()-1] == '\\')
			  syspath.erase(syspath.length()-1, 1);

			syspath +=  "/data";
#else
		
		syspath = ".";
#endif
		}
		once = true;
		char buff[400];
		sprintf(buff,"SysPath has been set as: %s",syspath.c_str());
		ulSetError(UL_DEBUG, buff);
	}    
	

	return syspath;
}
