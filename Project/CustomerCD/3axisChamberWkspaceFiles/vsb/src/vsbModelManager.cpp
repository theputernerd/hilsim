////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbModelManager.cpp
    \brief Implements the class vsbModelManager and itas auxillary classes
    
     The class vsbModelManager is responsible for managing and caching 3D 
	 mesh models that may be attached to vsbBaseViewEntity derrived objects.
	 
	\sa vsbModelManager.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <set>

#define MINIMUM_VERSION 2


#ifdef _MSC_VER
#include <io.h>
#include <direct.h>
#endif

#if (defined(__unix__) || defined(unix))
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

using namespace std;

#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

using namespace Parser;

#include "ssg.h"

#include "vsbViewportContext.h"
#include "vsbSysPath.h"
#include "vsbModelManager.h"
#include "vsbModelRoot.h"
#include "vsbBaseViewEntity.h"
#include "vsbUtilities.h"

#include "tool_hashstr.h"


// The name of the index file containing the list of terrain textures

const char *ModelIndexFile = "models.idx";

// Enumerations for keywords in model index grammar

enum {
	ML_MODEL_LIST,
	ML_VERSION,
	ML_INCLUDE,
	ML_MODEL,
	ML_TEMPLATE,
	ML_NAME,
	ML_ALIAS,
	ML_DESCRIPTION,
	ML_FILENAME,
	ML_CATEGORY,
	ML_RELPATH,
	ML_RELTEXTPATH,
	ML_LOADSTYLE,
	ML_PRELOAD,
	ML_DEMANDPAGE,
	ML_DEMANDPERSIST,
	ML_FLATTEN,
	ML_STRIPIFY,
	ML_CENTROID,
	ML_ORIENTATION,
	ML_DEFINE_CATEGORY,
	ML_SELECTOR,
	ML_DEFAULT,
	ML_GEOMETRY_STYLE,
	ML_COCKPIT_POSITION,
	ML_ATTACHMENT_POINT,
};

// Enumerations for error messages when parsing model index grammar

enum {
	ML_UNUSED,
	ML_INTERNAL_ERR,
	ML_FILE_ERR,
	ML_INCLUDE_FILE_ERROR,
	ML_MODEL_TAGNAME_EXPECTED,
	ML_MODEL_FILENAME_EXPECTED,
	ML_MODEL_CATEGORY_EXPECTED,
	ML_MODEL_RELPATH_EXPECTED,
	ML_LOAD_OPT_EXPECTED,
	ML_LOADOPT_TEXTPATH_EXPECTED,
	ML_MODEL_TAG_NAME_REPEATED,
	ML_MODEL_TEMPLATE_NAME_EXPECTED,
	ML_LOAD_SUB_OPT_EXPECTED,
	ML_COMMA_RBRACK_EXPECTED,
	ML_DESCRIPTION_STR_OR_FILENAME,
	ML_TEXTURE_RELPATH_EXPECTED,
	ML_MULTIPLE_DEFINITION,
	ML_MODEL_OR_TEMPLATE_EXPECTED,
	ML_TEMPLATE_ALREADY_DEFINED,
	ML_UNDEFINED_TEMPLATE_USED,
	ML_RBRACE_OR_MODEL_ATTRIBUTES_EXPECTED,
	ML_RBRACE_OR_TEMPLATE_ATTRIBUTES_EXPECTED,
	ML_CATEGORY_UNDEFINED,
	ML_SELECTOR_NAME_EXPECTED,
	ML_SELECTOR_ALIAS_NAME_EXPECTED,
	ML_ALIAS_OR_DEFLT_EXPECTED,
	ML_DUPLICATE_SELECTOR_NAME,
	ML_DUPLICATE_SELECTOR_ALIAS_NAME,
	ML_UNRECOGNISED_GEOMETRY_STYLE_NAME,
	ML_RECURSIVE_INCLUDE_ERROR,
	ML_VERSION_OBSOLETE,
	ML_ATTACHMENT_POINT_REDEFINITION,
};

static char *GeometryGroups[] = 
{
	"UNCATEGORISED",
	"MISSILE",
	"WEAPON",
	"MACHINERY",
	"UAV",
	"AIRCRAFT_CIVIL",
	"AIRCRAFT_MILITARY",
	"WATERCRAFT_CIVIL",
	"WATERCRAFT_MILITARY",
	"LANDCRAFT_CIVIL",
	"LANDCRAFT_MILITARY",
	"ARCHITECTURAL",
	"CIVIL_INFRASTRUCTURE",
	"GEOPHYSICAL",
	"FAUNA",
	"FLORA",
	0
};

////////////////////////////////////////////////////////////////////////////////
//
//  LOCAL function definitions
//
////////////////////////////////////////////////////////////////////////////////


/*! \func vsbMakeModelPath

 Composes an explicit, fully qualified path to the model filename specified in 
 the fname parameter. This is based on the SysPath and the hard wired rule that 
 model paths in the model index file are implicitly relative to the "models" 
 subdirectory of 
 the directory specified by the vsbGetSysPath() function.
*/

string vsbMakeModelPath(string fname)
{
	string work_str = vsbGetSysPath()+ "/models/";
	vsbRemoveEnclosingSlashes(fname);
	work_str += fname;
	return work_str;
}


////////////////////////////////////////////////////////////////////////////////
/*!
 \class _vsbParseableModelDefinitionBlock 

  \brief This class is an assistant to class _vsbModelListParser. 
  
  The _vsbParseableModelDefinitionBlock provides a mechanism to easily define 
  parseable blocks in the model index file grammar, and identify
  them as compulsory or optional as well as nominate how many instances of the
  parseable component are allowed to be parsed.

  \sa _vsbModelListParser
*/
////////////////////////////////////////////////////////////////////////////////

/*!  
   Pointer to _vsbModelListParser member function that handles the parsing
   of a model index file block component (attribute, ie: Mode, Template or
  Category definition)
*/
typedef bool (_vsbModelListParser::* BlockParseFuncPtr)(void);


class DLL_VSB_API _vsbParseableModelDefinitionBlock
{

public:
	/*! \brief Constructor

	Creates the block definition, which defines the parsing details of a parseable block
	\param keyWord The keyword that starts the block
	\param func The parse function for the block
	\param oneShot Defines whether this block is to be strictly parsed once, or any number of times
	\param compulsory Defines whether this block must be parsed
	*/
	_vsbParseableModelDefinitionBlock(int keyWord, BlockParseFuncPtr func, 
		               bool oneShot = true, bool compulsory = true);

	//! Copy Constructor
	_vsbParseableModelDefinitionBlock(const _vsbParseableModelDefinitionBlock& src);

	//! Assignment operator
	_vsbParseableModelDefinitionBlock &operator = (const _vsbParseableModelDefinitionBlock& src);


				/*! \brief Parse a block of this type 
				
				If the next token in the token input stream is the block start key word,
				proceeds to parse the block. If not this function returns having taken no
				action
				*/
	void		ParseBlock();

				/*! \brief Checks whether the block has been parsed

				If it is a compulsory block, and it has not been parsed this function throws
				an error to be caught by the parent parser module. The error is forced by 
				attempting to parse a keyword token that doesnt match the actual next token,
				essentially the block's starting keyword.
				*/
	void		TestCompulsoryBlockPresence();

				//! Set the parent parser object of the parseable blocks
	static void	SetModelListParser(_vsbModelListParser *ml_parser) {mspParser = ml_parser;};

			//! Get the number of blocks of this type that have been parsed
	int		NumCalls() const { return mNumCalls;};
	
			//! Get the leading keyword that identifies this model definition component
	int		KeyWord() const { return mKeyWord; };

private:
	static  _vsbModelListParser *mspParser; //!< Pointer to the parent ModelListParser object
	bool	mOneShot;    //!< Set to true if only 1 block of the specified type is to be allowed
	int		mNumCalls;   //!<  Number of parse calls to this parseable component
	bool	mCompulsory; //!< Set to true if this is compulsory block
	int     mKeyWord;    //!<  Key word identifying the beginning of this block       
	BlockParseFuncPtr mpBlockParseFunc; //!< The parse function for this block
};




////////////////////////////////////////////////////////////////////////////////
/*!
  \class _vsbModelAttributes 

  \brief Class that allows attributes to be stored as templates

  The model specification in an vsbModelEntry is comprised of a set of 
  attributes encapsulated by this class.
  This class is separated from the vsbModelEntry in order to allow
  attribute sets to be stored in lists and recalled by the parser to 
  initialise the attributes in the vsbModelEntry
  More specifically, it allows attributes to be stored as templates
  so the parser can then initialise new model entries from a template
  entry.
 */
///////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API _vsbModelAttributes
{
friend class DLL_VSB_API _vsbModelListParser;
private:

    vsbSelectorNode	mSelectors[MAX_SELECTORS];  //!< Contains the selector attribute nodes expected in the model
	int					mNumSelectors;              //!< The number of selector in the above list
	string				mLogicalName;                //!< The logical name of the model
	string				mFileName;                  //!< The model file name (path exclusive)
	string				mRelModelPath;              //!< The path to the model relative to the base model path
	string				mRelTexturePath;            //!< The relative texture path (if required) relative to the base model path
	bool				mHaveRelativeTexturePath;   //!< Flag identifying whether texture path is present
	int			        mLoadStyle;                 //!< Load style options
	sgMat4				mCentroidMat;               //!< The Centroid matrix
    sgMat4              mOrientationMat;            //!< The Orientation matrix
	string		        mDescription;               //!< Model description string
	string		        mClassName;	                //!< Model class name string
	bool		        mModelOptions[2];           //!< Flag identifying whether the model is to be stripified and/or flattened on load
	int					mModelGroup;				//!< Identifies the model geometry category ie Aircraft/landcraft etc
	sgdCoord			mCockpitCoord;				//!< Identifies the position of the cockpit relative to the centroid
    map<long,vsbAttachmentPoint> mAttachmentPoints;
 
    //! Resets the class attributes to their defaults
    void ResetModelAttributes();


    //! Increment the block count for the given id
	void IncrementBlockCount(int keyWord);

#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

    map<int, int> mBlockCount;  //!< Stores the number of blocks, where the first int is the block id the second is the count

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

public:

    //! Default constructor
	_vsbModelAttributes(); 
    
    //! Copy constructor
	_vsbModelAttributes(const _vsbModelAttributes &src);

    //! Assignment operator
	_vsbModelAttributes &operator = (const _vsbModelAttributes &src);

	int BlockCount(int keyword);
};

////////////////////////////////////////////////////////////////////////////////
/*!
  \class _vsbModelListParser 

  \brief Defines a parse object for the model index file grammar

  This class derrives an object from the TParseObjectBase class and defines 
  a grammar for the model index file.  It has the ability to parse a model
  index file and populate the vsbModelManager's mModelList with vsbModelEntry's
  that contain all the information necessary to load a model and attach it to
  an entity
*/
////////////////////////////////////////////////////////////////////////////////


class DLL_VSB_API _vsbModelListParser: public TParseObjectBase, public _vsbModelAttributes
{
friend  class DLL_VSB_API _vsbParseableModelDefinitionBlock;
friend  class DLL_VSB_API vsbModelManager;

public:
    /*! \brief Constructor
        \param ptl A reference to the model manager
    */
	_vsbModelListParser(vsbModelManager &ptl);

    /*! \brief Parse a model index file
    */
	int ParseFile(const string &fname);

    /*! Get the currently active model source file

    A model file can recursively pull in other model files. This function returns the name
    of the actual file being parsed.
    */
	const string &CurrentActiveSourceFile() const {return mCurrentActiveSourceFile;};
private:

	static char     *mpsKeywords[];    //!< Model index file grammar key words
	static char     *mpsErrors[];      //!< Model index error strings 
	
	string          mCurrentActiveSourceFile; //!< Current file being parsed

	vsbModelManager &mModelManager;     //!< Reference to the model manager

	void   ParseFunc();	                //!< Overidden virtual parse function (see parser)

	void RecurseSubDirectories();

/** @name Sub-Parsers
Private functions that parse different blocks of the model file grammar
*/
//@{
	void  ParseAllBlocks();
	bool  ParseModelBlock();
	bool  ParseTemplateBlock();
	bool  ParseCategoryDefinition();
	bool  ParseDesc();
	bool  ParseFileName();
	bool  ParseCategoryName();
	bool  ParseRelModelPath();
	bool  ParseRelTexturePath();
	bool  ParseCentroid();
	bool  ParseOrientation();
	bool  ParseLoadOpts();
	bool  ParseSelector();
	bool  ParseGeometryStyle();
	bool  ParseCockpitPosition();
	bool  ParseAttachmentPoint();
//@}


            /*! \brief Register a block as part of the grammar 

            The block parser registration allows the construction of block grammara
            with a set of blocks that can appear in an unordered fashion. Each Block 
            structure is identified by a unique starting keyword.

            \param keyword The keyword that starts the block
            \param func Pointer to the function that perfomrs the block parse
            \param oneShot Flag indicating whether only on of these blocks is allowed
            \param compulsory Flag indicating that at least one block must be parsed
            */
	void    RegisterParseBlock(int keyWord, BlockParseFuncPtr func, 
		              bool oneShot = true, bool compulsory = true);

            /*! \brief Clear all the registered parse blocks
            */
	void	ClearParseBlockRegistrations();

            //! Tests whether a parse block starting with the given keyword is registered
	bool	IsRegistered(int keyWord);

            /*! \brief Parses all the registered blocks that follow in the input stream

            While a peek into the input token stream reveals a registerd block keyword
            the parser for that block is invoked.
            */
	void    PerformParseBlocks();
    
            //! Tests whether all compulsory blocks have been parsed
	void    TestCompulsoryBlocksParsed();
            
            //! Test whether a particular block has been parsed
	bool    HasBeenParsed(int keyword);

#           ifdef _MSC_VER
#           pragma warning( push )
#           pragma warning( disable : 4251 ) // STL not exported warning
#           endif

	        vector<_vsbParseableModelDefinitionBlock> mComponentBlocks; //!< List of the parsed blocks
	        map<string, _vsbModelAttributes> mAttribTemplates; //!< A list of attribute templates

	        set<string> mIncludedFiles; //!< used to keep a record of included files so they are included only once
	        set<string> mDefinedCategories; //!< A list of model category names

			bool mRecursing;
			string mDefaultModelPath;

#           ifdef _MSC_VER
#           pragma warning( pop )
#           endif

};


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbModelEntry
    \brief vsbModelEntry's contain all the information necessary to load a model and 
	attach it to an entity.

	The vsbModelManager holds a list of vsbModelEntry's
    that are loaded at program start-up (ie when the vsbModelManager class is 
    created).
*/
////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: vsbModelManager -- Member Definitions
//
////////////////////////////////////////////////////////////////////////////////

int vsbModelManager::mInstanceCount = 0;

// mspDefaultCategories defines a list of default model categories
// these categories are used to determine how to customize the entity
// to enable it to take advantage of any special features or capabilities
// the model may have, or any special purposes the model may be put to
//
char *vsbModelManager::mspDefaultCategories[] =
{
	"MS_FlightSim",
	0
};


vsbModelManager::vsbModelManager()
{
	assert(mInstanceCount == 0);
	mInstanceCount++;
	mCount = 0;
	LoadIndex();
}

vsbModelManager::~vsbModelManager()
{
	Clear();
	mInstanceCount--;
}

void vsbModelManager::Clear()
{
	map<string, vsbModelEntry *>::iterator p; 

	p = mModelList.begin();


	while (p != mModelList.end())
	{	
		delete p->second;
		p++;
	}
	mModelList.clear();
	mCount = 0;
}




void vsbModelManager::LoadIndex()
{
	string model_idxfile = vsbMakeModelPath(ModelIndexFile);

	ulSetError(UL_DEBUG,"Loading model index file: %s",model_idxfile.c_str());

	_vsbModelListParser parser(*this);
	int err_val = parser.ParseFile(model_idxfile);
	if (err_val)
	{
		char buffer[400];
		sprintf(buffer,"While loading model index file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			parser.CurrentActiveSourceFile().c_str(), 
			parser.ItsLastError()->Id(),
			parser.ItsLastError()->Desc(),
			parser.ItsLastError()->Pos(),
			parser.ItsLastError()->Line());
		ulSetError(UL_FATAL, buffer);
		return;
	}

	// All models that have a ME_PRELOAD loadOption setting are
	// loaded
	
	mAttachmentPointNameList.clear();

	vsbModelEntry * model_entry = FindFirst();
	while (model_entry != 0)
	{
		// load all models marked pre-load
		
		if (model_entry->LoadOpts() == vsbModelEntry::ME_PRELOAD)
			model_entry->Load();

		// Make a list of attachment point names

		map<long,vsbAttachmentPoint>::iterator iter;
		for (iter = model_entry->mAttachmentPoints.begin(); iter != model_entry->mAttachmentPoints.end(); iter++)
		{
			set<string>::iterator attachment_iter;
			attachment_iter = mAttachmentPointNameList.find(iter->second.Name());
			if (attachment_iter == mAttachmentPointNameList.end())
			{
				mAttachmentPointNameList.insert(iter->second.Name());
			}

		}

		model_entry = FindNext();
	}
	
}


vsbModelEntry *vsbModelManager::FindByTagName(const string &tagname)
{
	if (mModelList.size() == 0) 
		return 0;
	mModelListIter = mModelList.find(tagname);
	if (mModelListIter == mModelList.end()) 
		return 0;
	return mModelListIter->second;
}

vsbModelEntry * vsbModelManager::FindFirst() 
{
	mModelListIter = mModelList.begin();
	if (mModelListIter == mModelList.end()) 
		return 0;
	return mModelListIter->second;
}

vsbModelEntry *vsbModelManager::FindNext() 
{
	mModelListIter++;
	if (mModelListIter == mModelList.end()) 
		return 0;
	return mModelListIter->second;
}

bool vsbModelManager::Attach(const string &tagname, vsbBaseViewEntity *entity)
{
	char buffer[400]; 
	vsbModelEntry *model_entry = FindByTagName(tagname);
	if (!model_entry) 
	{
		sprintf(buffer,"Model \"%s\" is undefined", tagname.c_str());
		ulSetError(UL_WARNING, buffer);
		return false;
	}


	entity->CopyAttachmentPoints(model_entry->mAttachmentPoints);
	//@@@@ try entity->mAttachmentPoints = model_entry->mAttachmentPoints later @@@
	
	return model_entry->Attach(entity);

}

bool vsbModelManager::Detach(const string &tagname, vsbBaseViewEntity *entity)
{
	char buffer[400];
	vsbModelEntry *model_entry =FindByTagName(tagname);
	if (!model_entry) 	
	{
		sprintf(buffer,"Model \"%s\" is undefined", tagname.c_str());
		ulSetError(UL_WARNING, buffer);
		return false;
	}

	entity->DeleteAllAttachmentPoints();
	return model_entry->Detach(entity);
}

bool vsbModelManager::Attach(const string &tagname, ssgBranch *branch)
{
	char buffer[400]; 
	vsbModelEntry *model_entry = FindByTagName(tagname);
	if (!model_entry) 
	{
		sprintf(buffer,"Model \"%s\" is undefined", tagname.c_str());
		ulSetError(UL_WARNING, buffer);
		return false;
	}
	return model_entry->Attach(branch);

}

bool vsbModelManager::Detach(const string &tagname, ssgBranch *branch)
{
	char buffer[400];
	vsbModelEntry *model_entry =FindByTagName(tagname);
	if (!model_entry) 	
	{
		sprintf(buffer,"Model \"%s\" is undefined", tagname.c_str());
		ulSetError(UL_WARNING, buffer);
		return false;
	}
	return model_entry->Detach(branch);
}


const string * vsbModelManager::FindFirstModelName()
{
	vsbModelEntry *me = FindFirst();
	if (me) 
		return &me->LogicalName();
	else 
		return 0;
}

const string * vsbModelManager::FindNextModelName()
{
	vsbModelEntry *me = FindNext();
	if (me) 
		return &me->LogicalName();
	else 
		return 0;

}



const std::string *	 vsbModelManager::CurrentModelGroup()
{
	static std::string group_name;

	group_name = "";
	 vsbModelEntry *modelEntry = mModelListIter->second;
	 if (mModelListIter != mModelList.end()) 
		 group_name = GeometryGroups[modelEntry->ModelGroup()];
	return &group_name;
}


////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: _vsbModelAttributes -- Member Definitions
//
//  The model specification in an vsbModelEntry is comprised of a set of 
//  attributes encapsulated by this class.
//  This class is separated from the vsbModelEntry in order to allow
//  attribute sets to be stored in lists and recalled by the parser to 
//  initialise the attributes in the vsbModelEntry
//  More specifically, it allows attributes to be stored as templates
//  so the parser can then initialise new model entries from a template
//  entry.
////////////////////////////////////////////////////////////////////////////////

_vsbModelAttributes::_vsbModelAttributes()
{
	ResetModelAttributes();
}

_vsbModelAttributes::_vsbModelAttributes(const _vsbModelAttributes &src)
{
	*this = src;
}

void _vsbModelAttributes::ResetModelAttributes()
{
	int i;
	mDescription = mRelTexturePath = mFileName = mClassName = mRelModelPath = "";
	sgMakeIdentMat4(mCentroidMat);
	sgMakeIdentMat4(mOrientationMat);
	mLoadStyle = 0;
	mNumSelectors = 0;
	mBlockCount.clear();
	mAttachmentPoints.clear();
	mModelGroup = 0;
	for (i=0; i<2; i++) mModelOptions[i] = false;
}


_vsbModelAttributes &_vsbModelAttributes::operator = (const _vsbModelAttributes &src)
{
	int i;
	mDescription	= src.mDescription;
	mFileName		= src.mFileName;
	mClassName		= src.mClassName;
	mRelModelPath	= src.mRelModelPath;
	mRelTexturePath	= src.mRelTexturePath;
	sgCopyMat4(mCentroidMat,src.mCentroidMat);
	sgCopyMat4(mOrientationMat,src.mOrientationMat);	
	mLoadStyle		= src.mLoadStyle;
	mModelGroup  = src.mModelGroup;
	mAttachmentPoints = src.mAttachmentPoints;

	for (i=0; i<2; i++) mModelOptions[i] = src.mModelOptions[i];

	mNumSelectors = src.mNumSelectors;
	for (i=0; i<mNumSelectors; i++)
		mSelectors[i] = src.mSelectors[i];

	mBlockCount = src.mBlockCount;
	return *this;
}

void _vsbModelAttributes::IncrementBlockCount(int keyWord)
{
	map<int, int>::iterator iter;
	iter = mBlockCount.find(keyWord);
	if (iter == mBlockCount.end())
	{
		mBlockCount.insert(pair<int,int>(keyWord, 1));
	} else
	{
		iter->second++;
	}
}

int _vsbModelAttributes::BlockCount(int keyWord)
{
	map<int, int>::iterator iter;
	iter = mBlockCount.find(keyWord);
	if (iter == mBlockCount.end())
	   return 0;
	else
		return iter->second;
}



////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: _vsbParseableModelDefinitionBlock -- Class Definition
//
//  This class is an assistant to class _vsbModelListParser. It provides a mechanism
//  to easily define parseable blocks in the model index file grammar, and identify
//  them as compulsory or optional as well as nominate how many instances of the
//  parseable component are allowed to be parsed.
//
////////////////////////////////////////////////////////////////////////////////


_vsbModelListParser *_vsbParseableModelDefinitionBlock::mspParser = 0;

_vsbParseableModelDefinitionBlock::_vsbParseableModelDefinitionBlock(int keyWord, BlockParseFuncPtr func, bool oneShot, bool compulsory )
{
	mKeyWord = keyWord;
	mNumCalls = 0;
	mpBlockParseFunc = func;
	mOneShot = oneShot;
	mCompulsory = compulsory;
}


_vsbParseableModelDefinitionBlock::_vsbParseableModelDefinitionBlock(const _vsbParseableModelDefinitionBlock& src)
{
	*this = src;
}

_vsbParseableModelDefinitionBlock & _vsbParseableModelDefinitionBlock::operator = (const _vsbParseableModelDefinitionBlock& src)
{
	mKeyWord = src.mKeyWord;
	mNumCalls = src.mNumCalls;
	mpBlockParseFunc = src.mpBlockParseFunc;
	mOneShot = src.mOneShot;
	mCompulsory = src.mCompulsory;
	return *this;
}

void _vsbParseableModelDefinitionBlock::ParseBlock()
{
	assert(mpBlockParseFunc);
	assert(mspParser);
	mspParser->PeekToken();
	if (mspParser->GetCurrTokenID() != mKeyWord) return;
	if (mOneShot && (mNumCalls > 0 || mspParser->BlockCount(mKeyWord)>0))
		mspParser->AbortParse(ML_MULTIPLE_DEFINITION);
	mNumCalls++;
	(mspParser->*mpBlockParseFunc)();
}

void _vsbParseableModelDefinitionBlock::TestCompulsoryBlockPresence()
{
	assert(mpBlockParseFunc);
	assert(mspParser);
	if (mCompulsory && mNumCalls == 0)
		mspParser->GetToken(mKeyWord); // throw keyword expected exception

}

////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: _vsbModelListParser -- Member Definitions
//
//  This class derrives an object from the TParseObjectBase class and defines 
//  a grammar for the model index file.  It has the ability to parse a model
//  index file and populate the vsbModelManager's  mModelList with vsbModelEntry's
//	that contain all the information necessary to load a model, attach it to
//  and configure an entity
////////////////////////////////////////////////////////////////////////////////


char * _vsbModelListParser::mpsKeywords[] = 
{
	"MODEL_LIST",
	"VERSION",
	"INCLUDE",
	"MODEL",
	"TEMPLATE",
	"NAME",
	"ALIAS",
	"DESCRIPTION",
	"FILE_NAME",
	"CATEGORY",
	"MESH_PATH",
	"TEXTURE_PATH",
	"LOAD_STYLE",
	"PRELOAD",
	"DEMAND_PAGE",
	"DEMAND_PERSIST",
	"FLATTEN",
	"STRIPIFY",
	"CENTROID",
	"ORIENTATION",
	"DEFINE_CATEGORY",
	"SELECTOR",
	"DEFAULT",
	"GEOMETRY_STYLE",
	"COCKPIT_POSITION",
	"ATTACHMENT_POINT",
	0
};

char * _vsbModelListParser::mpsErrors[] = 
{
	"",
	"Internal Error",
	"Unable to open model index file", 
	"Unable to open include file",
	"Model tag name expected",
	"Model file name expected",
	"Model category name expected",
	"Relative model path name expected",
	"Load option (PRELOAD, DEMAND_PAGE or DEMAND_PERSIST) expected",
	"Relative texture path or load option (PRELOAD, DEMAND_PAGE or DEMAND_PERSIST) expected",
	"Model tag name repeated",
	"Model template name expected",
	"Model Load suboption (FLATTEN or STRIPIFY) expected",
	"\",\" or \"}\" expected",
	"Model description string or \"FILE_NAME\" expected",
	"Relative texture path name string expected",
	"Multiple definitions not allowed",
	"\"MODEL\" or \"TEMPLATE\" definition block expected",
	"Template already defined",
	"Reference to undefined model template",
	"Valid MODEL attribute definitions or \"}\" expected",
	"Valid TEMPLATE attribute definitions or \"}\" expected",
	"Undefined category",
	"Selector name string expected",
	"Selector alias name string expected",
	"Invalid parameter. Alias or Default expected",
	"Selector has already been defined",
	"Alias has already been defined",
	"Unrecognised geometry style name",
	"Includes not allowed in recursed model sub-index files",
	"The model index file is obsolete",
	"The attachment point name is already defined or maps to same id as a prior attachment point",
	0
};



_vsbModelListParser::_vsbModelListParser(vsbModelManager &modelMan):
TParseObjectBase(mpsKeywords, mpsErrors),
_vsbModelAttributes(),	mRecursing(false),
mModelManager(modelMan) 
{
	mAttribTemplates.clear();
}

int _vsbModelListParser::ParseFile(const string &fname)
{

	TFileLineInp mInput;
	mModelManager.Clear();
	mIncludedFiles.clear();
	mDefinedCategories.clear();
	const char *category_name;

	for (int i=0; (category_name=vsbModelManager::mspDefaultCategories[i])!=0 ; i++)
	{
		if (mDefinedCategories.find(category_name) == mDefinedCategories.end())
			mDefinedCategories.insert(category_name);
	}

    mCurrentActiveSourceFile = fname;

	if (!mInput.Open(fname.c_str()))
	{
		if (!mRecursing) 
		{
			try {
				RecurseSubDirectories();
			}
			catch(...)
			{
				return itsLastError->Id();
			}
			return 0;
		} else
		{
			mInput.Close();
			itsLastError->Set(ML_FILE_ERR,0,"",mpsErrors[ML_FILE_ERR]);
			return ML_FILE_ERR;
		}
	} 

	int errNum = ParseSource(&mInput, 1);
    mInput.Close();
	return errNum;
}


bool  _vsbModelListParser::ParseDesc()
{
	PeekToken();
	if (GetCurrTokenID()  == ML_DESCRIPTION)
	{
		IncrementBlockCount(GetCurrTokenID());
		int string_count = 0;
		mDescription = "";
		GetToken();
		GetToken(T_EQ);
		PeekToken();
		while (GetCurrTokenID()  == T_STRING)
		{
			const char *next_string;
			GetToken(T_STRING, ML_DESCRIPTION_STR_OR_FILENAME);
			next_string = GetCurrTokenStr();
			if (!isspace(next_string[0]) && string_count)
				mDescription += ' ';
			mDescription += next_string;
			string_count++;
			PeekToken();
		}
		return true;
	}
	return false;
}


bool  _vsbModelListParser::ParseFileName()
{
	PeekToken();
	if (GetCurrTokenID()  == ML_FILENAME)
    {
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_MODEL_FILENAME_EXPECTED);
		mFileName = GetCurrTokenStr();
		vsbRemoveEnclosingSlashes(mFileName);
		return true;
    }
	return false;
}

bool  _vsbModelListParser::ParseCategoryName()
{
	PeekToken();
	if (GetCurrTokenID()  == ML_CATEGORY)
    {
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_MODEL_CATEGORY_EXPECTED);
		mClassName = GetCurrTokenStr();
		if (mDefinedCategories.find(mClassName) == mDefinedCategories.end())
			AbortParse(ML_CATEGORY_UNDEFINED);
		return true;
    }
	return false;
}

bool  _vsbModelListParser::ParseRelModelPath()
{
	PeekToken();
	if (GetCurrTokenID()  == ML_RELPATH)
    {
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_MODEL_RELPATH_EXPECTED);
		mRelModelPath = GetCurrTokenStr();
		vsbRemoveEnclosingSlashes(mRelModelPath);
		return true;
	}
	return false;
}

bool  _vsbModelListParser::ParseRelTexturePath()
{
	PeekToken();
	if (GetCurrTokenID()  == ML_RELTEXTPATH)
    {
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_TEXTURE_RELPATH_EXPECTED);
		mRelTexturePath = GetCurrTokenStr();
		vsbRemoveEnclosingSlashes(mRelTexturePath);
		return true;
	}
	return false;
}

bool  _vsbModelListParser::ParseCentroid()
{
	PeekToken();
	if (GetCurrTokenID() == ML_CENTROID)
	{
		IncrementBlockCount(GetCurrTokenID());
		sgVec3 xlate;
		GetToken();
		GetToken(T_EQ);
		xlate[0] = GetNumericValue();
		GetToken(T_COMMA);
		xlate[1] = GetNumericValue();
		GetToken(T_COMMA);
		xlate[2] = GetNumericValue();
		sgMakeTransMat4(mCentroidMat,xlate);
		return true;
	}
	return false;
}

bool  _vsbModelListParser::ParseOrientation()
{
	PeekToken();
	if (GetCurrTokenID() == ML_ORIENTATION)
	{
		IncrementBlockCount(GetCurrTokenID());
		sgVec3 xlate;
		GetToken();
		GetToken(T_EQ);
		xlate[0] = GetNumericValue();
		GetToken(T_COMMA);
		xlate[1] = GetNumericValue();
		GetToken(T_COMMA);
		xlate[2] = GetNumericValue();
		sgMakeRotMat4(mOrientationMat,xlate);
		return true;
	}
	return false;
}

bool _vsbModelListParser::ParseLoadOpts()
{
	static int load_opts[] = 
								{	
									ML_PRELOAD,
									ML_DEMANDPAGE,
									ML_DEMANDPERSIST,
									0
								};

	static int model_opts[] =  {
									ML_FLATTEN,
									ML_STRIPIFY,
									0
								};
	PeekToken();

	if (GetCurrTokenID() == ML_LOADSTYLE)
	{
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();

		mLoadStyle = KeywordTableSelection(load_opts);
		if (mLoadStyle < 0) AbortParse(ML_LOAD_OPT_EXPECTED);


		PeekToken();
		if (GetCurrTokenID() == T_LBRACK)
		{
			int opt;
			GetToken();
			while(GetCurrTokenID() != T_RBRACK)
			{
				GetToken();
				opt = KeywordTableSelection(model_opts);
				if (opt < 0) AbortParse(ML_LOAD_SUB_OPT_EXPECTED);
				mModelOptions[opt] = true;
				GetToken();
				if (GetCurrTokenID() == T_COMMA || GetCurrTokenID() == T_RBRACK) continue;
				AbortParse(ML_COMMA_RBRACK_EXPECTED);
			}
		}
		return true;
	}
	return false;
}

bool _vsbModelListParser::ParseCategoryDefinition()
{
	PeekToken();
	if (GetCurrTokenID() == ML_DEFINE_CATEGORY)
	{
		GetToken();
		GetToken(T_STRING);
		string category = GetCurrTokenStr();
		if (mDefinedCategories.find(category) == mDefinedCategories.end())
			mDefinedCategories.insert(category);
		PeekToken();
		return true;
	}
	return false;
}

bool _vsbModelListParser::ParseSelector()
{
	string name, alias;
	int deflt = 0;
	int i;
	PeekToken();
	if (GetCurrTokenID() == ML_SELECTOR)
	{
		GetToken();
		GetToken(T_LBRACK);
		GetToken();
		if (GetCurrTokenID()  == ML_NAME)
		{
			GetToken(T_COLON);
			GetToken();
		}
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_SELECTOR_NAME_EXPECTED);

		alias = name = GetCurrTokenStr();
		PeekToken();

		if (GetCurrTokenID() == T_COMMA)
		{
			GetToken();
			PeekToken();

			if (GetCurrTokenID() == ML_ALIAS || GetCurrTokenID() == T_STRING)
			{
				GetToken();
				if (GetCurrTokenID()  == ML_ALIAS)
				{
					GetToken(T_COLON);
					GetToken();
				}
				if (GetCurrTokenID()  != T_STRING)
					AbortParse(ML_SELECTOR_ALIAS_NAME_EXPECTED);
				alias = GetCurrTokenStr();
				PeekToken();
			} else if (GetCurrTokenID() == ML_DEFAULT || GetCurrTokenID() == T_INT || GetCurrTokenID() == T_HEX)
			{
				if (GetCurrTokenID()  == ML_DEFAULT)
				{
					GetToken();
					GetToken(T_COLON);
				}

				deflt = GetNumericValue();
				goto complete;

			} else AbortParse(ML_ALIAS_OR_DEFLT_EXPECTED);

			if (GetCurrTokenID() == T_COMMA)
			{
				GetToken();
				PeekToken();
				if (GetCurrTokenID() == ML_DEFAULT || GetCurrTokenID() == T_INT || GetCurrTokenID() == T_HEX)
				{
					if (GetCurrTokenID()  == ML_DEFAULT)
					{
						GetToken();
						GetToken(T_COLON);
					}
					deflt = GetNumericValue();
				}
			}

		}

complete:
		GetToken(T_RBRACK);

		for (i=0; i<mNumSelectors; i++)
		{
			if (mSelectors[i].mName == name) 
			{
				mSelectors[i].mDefault = deflt;
				return true;
				//AbortParse(ML_DUPLICATE_SELECTOR_NAME);
			}
			else if (mSelectors[i].mAlias == alias) 
				AbortParse(ML_DUPLICATE_SELECTOR_ALIAS_NAME);
		}

		mSelectors[mNumSelectors].mName = name;
		mSelectors[mNumSelectors].mAlias = alias;
		mSelectors[mNumSelectors].mDefault = deflt;
		mNumSelectors++;

		return true;		
	}
	return false;
}

bool _vsbModelListParser::ParseGeometryStyle()
{
	PeekToken();
	if (GetCurrTokenID() == ML_GEOMETRY_STYLE)
	{
		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		GetToken();
		for (int i=0; GeometryGroups[i]!= 0; i++)
		{
			if (!stricmp(GeometryGroups[i],GetCurrTokenStr()))
			{
				mModelGroup = i;
				return true;
			}
		}
		AbortParse(ML_UNRECOGNISED_GEOMETRY_STYLE_NAME);
	}
	return false;
}

bool _vsbModelListParser::ParseCockpitPosition()
{
	PeekToken();
	if (GetCurrTokenID() == ML_COCKPIT_POSITION)
	{

		sgdSetVec3(mCockpitCoord.hpr, 0, 0, 0);

		IncrementBlockCount(GetCurrTokenID());
		GetToken();
		GetToken(T_EQ);
		mCockpitCoord.xyz[0] = GetNumericValue();
		GetToken(T_COMMA);
		mCockpitCoord.xyz[1] = GetNumericValue();
		GetToken(T_COMMA);
		mCockpitCoord.xyz[2] = GetNumericValue();

		PeekToken();

		if (GetCurrTokenID() == T_COMMA)
		{
			GetToken(T_COMMA);
			mCockpitCoord.hpr[0] = GetNumericValue();
			GetToken(T_COMMA);
			mCockpitCoord.hpr[1] = GetNumericValue();
			GetToken(T_COMMA);
			mCockpitCoord.hpr[2] = GetNumericValue();
		} 
			
		return true;
	}
	return false;
}

bool _vsbModelListParser::ParseAttachmentPoint()
{
	sgdVec3 pos; 
	string  name;
	long id;	
	
	PeekToken();
	if (GetCurrTokenID() == ML_ATTACHMENT_POINT)
	{

		map<long,vsbAttachmentPoint>::iterator iter;

		GetToken();
		GetToken(T_LBRACK);
		GetToken(T_STRING);
		name = GetCurrTokenStr();

		for(string::iterator i = name.begin(); i != name.end(); i++)
			*i = toupper(*i);
			
		id = vsbAttachmentPoint::MakeId(name);
		iter = mAttachmentPoints.find(id);
		if (iter != mAttachmentPoints.end())
			AbortParse(ML_ATTACHMENT_POINT_REDEFINITION);
		GetToken(T_RBRACK);
		GetToken(T_EQ);
		pos[0] = GetNumericValue();
		GetToken(T_COMMA);
		pos[1] = GetNumericValue();
		GetToken(T_COMMA);
		pos[2] = GetNumericValue();
		//vsbToScaledVSBCoords(pos);

		vsbAttachmentPoint attachment_point(name,pos);
		
		mAttachmentPoints.insert(pair<long, vsbAttachmentPoint>(id,attachment_point));
		return true;
	}
	return false;
}

bool _vsbModelListParser::ParseModelBlock()
{
	string tagname, templatename ;
	int i;

	bool have_realignment_mods;
	char buffer[400];
	sgMat4 realignment_matrix;

	
	vsbModelEntry *temp_model_entry; 
	map<string, vsbModelEntry *>::iterator p; 
	map<string, _vsbModelAttributes>::iterator template_iter; 
	pair<map<string, vsbModelEntry *>::iterator, bool> res;

	
	PeekToken();
	if (GetCurrTokenID() == ML_MODEL)
	{
		
		GetToken();
		GetToken(T_LBRACK);

		GetToken();
		if (GetCurrTokenID()  == ML_NAME)
		{
			GetToken(T_COLON);
			GetToken();
		}
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_MODEL_TAGNAME_EXPECTED);
		tagname = GetCurrTokenStr();

		p = mModelManager.mModelList.find(tagname);
		if (p != mModelManager.mModelList.end()) 
			AbortParse(ML_MODEL_TAG_NAME_REPEATED);

		ClearParseBlockRegistrations();
		
		RegisterParseBlock(ML_DESCRIPTION, ParseDesc, true, false);
		RegisterParseBlock(ML_FILENAME, ParseFileName, true, true);
		RegisterParseBlock(ML_CATEGORY, ParseCategoryName, true, false);
		RegisterParseBlock(ML_RELPATH, ParseRelModelPath, true, false);
		RegisterParseBlock(ML_RELTEXTPATH, ParseRelTexturePath, true, false);
		RegisterParseBlock(ML_CENTROID, ParseCentroid, false, false);
		RegisterParseBlock(ML_ORIENTATION, ParseOrientation, false, false);
		RegisterParseBlock(ML_LOADSTYLE, ParseLoadOpts, false, true);
		RegisterParseBlock(ML_SELECTOR, ParseSelector, false, false);
		RegisterParseBlock(ML_GEOMETRY_STYLE, ParseGeometryStyle, false, false);
		RegisterParseBlock(ML_COCKPIT_POSITION, ParseCockpitPosition, true, false);
		RegisterParseBlock(ML_ATTACHMENT_POINT, ParseAttachmentPoint, false, false);

		PeekToken();
		if (GetCurrTokenID() == T_COMMA)
		{
			GetToken();
			GetToken();
			if (GetCurrTokenID()  == ML_TEMPLATE)
			{
				GetToken(T_COLON);
				GetToken();
			}
			if (GetCurrTokenID()  != T_STRING)
			   AbortParse(ML_MODEL_TEMPLATE_NAME_EXPECTED);
			templatename = GetCurrTokenStr();
			// check to see if template exists

			template_iter = mAttribTemplates.find(templatename);
			if (template_iter == mAttribTemplates.end())
			AbortParse(ML_UNDEFINED_TEMPLATE_USED);

			*((_vsbModelAttributes *)this) = template_iter->second;	
			
		}

		GetToken(T_RBRACK);
		GetToken(T_LBRACE);

		sgdVec3 pos = {0,0,0};
		string  name = "CENTROID";
		long id;	
		
		// Define a default attachment point at the centroid of a model
		id = vsbAttachmentPoint::MakeId(name);
		vsbAttachmentPoint attachment_point(name,pos);
		mAttachmentPoints.insert(pair<long, vsbAttachmentPoint>(id,attachment_point));
		
		PerformParseBlocks();

		TestCompulsoryBlocksParsed();
		GetToken(T_RBRACE, ML_RBRACE_OR_MODEL_ATTRIBUTES_EXPECTED);


		if (!HasBeenParsed(ML_RELPATH))
				mRelModelPath = mDefaultModelPath;


		//cout << "New Model Entry: " << tagname << " " << mFileName << " " << mRelModelPath << " " << mLoadStyle << endl;
		temp_model_entry = new vsbModelEntry( tagname, mFileName, mRelModelPath, (vsbModelEntry::LoadOptions)mLoadStyle, mModelGroup);

		if (HasBeenParsed(ML_RELTEXTPATH))
			temp_model_entry->RelativeTexturePath(mRelTexturePath);

		temp_model_entry->mFlatten =  mModelOptions[0]; 
		temp_model_entry->mStripify = mModelOptions[1];

		have_realignment_mods = false;
		if (HasBeenParsed(ML_CENTROID)) 
		{
			have_realignment_mods = true;
			sgCopyMat4(realignment_matrix,mCentroidMat);
		} else 
			sgMakeIdentMat4(realignment_matrix);

		if (HasBeenParsed(ML_ORIENTATION)) 
		{
			have_realignment_mods = true;
			sgPostMultMat4(realignment_matrix, mOrientationMat);
		}

		if (HasBeenParsed(ML_COCKPIT_POSITION)) 
		{
			temp_model_entry->CockpitMat(mCockpitCoord);
		}

		temp_model_entry->Realign(have_realignment_mods, realignment_matrix);
		
		res = mModelManager.mModelList.insert(pair<string, vsbModelEntry *>(tagname, temp_model_entry));

		temp_model_entry->mNumSelectors = mNumSelectors;

		for (i=0; i<mNumSelectors; i++)
			temp_model_entry->mSelectors[i] = mSelectors[i];

		temp_model_entry->mAttachmentPoints = mAttachmentPoints;
		
/*
		vsbSelectorNode				mSelectors[MAX_SELECTORS];  
		int								mNumSelectors;
*/
		if (!res.second)
		{
			sprintf(buffer,"Failed to insert \"%s\" into model index list",tagname.c_str());
			ulSetError(UL_WARNING,buffer);
		} else
		{
			sprintf(buffer,"Inserted \"%s\" into model index list",tagname.c_str());
			ulSetError(UL_DEBUG,buffer);
		}

		mModelManager.mCount++;
		PeekToken();
		return true;
	} else
		return false;
}

bool _vsbModelListParser::ParseTemplateBlock()
{
	string template_name;
	map<string, _vsbModelAttributes>::iterator iter; 

	PeekToken();
	if (GetCurrTokenID() == ML_TEMPLATE)
	{
		GetToken();
		GetToken(T_LBRACK);

		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(ML_MODEL_TAGNAME_EXPECTED);
		template_name = GetCurrTokenStr();

		iter = mAttribTemplates.find(template_name);
		if (iter != mAttribTemplates.end())
			AbortParse(ML_TEMPLATE_ALREADY_DEFINED);

		GetToken(T_RBRACK);
		GetToken(T_LBRACE);

		ClearParseBlockRegistrations();

		RegisterParseBlock(ML_CENTROID, ParseCentroid, true, false);
		RegisterParseBlock(ML_ORIENTATION, ParseOrientation, true, false);
		RegisterParseBlock(ML_LOADSTYLE, ParseLoadOpts, true, false);
		RegisterParseBlock(ML_CATEGORY, ParseCategoryName, true, false);
		RegisterParseBlock(ML_SELECTOR, ParseSelector, false, false);
		RegisterParseBlock(ML_GEOMETRY_STYLE, ParseGeometryStyle, false, false);

	    PerformParseBlocks();

		TestCompulsoryBlocksParsed();
		GetToken(T_RBRACE, ML_RBRACE_OR_TEMPLATE_ATTRIBUTES_EXPECTED);

		mAttribTemplates[template_name] = *this;
		return true;
	}
	return false;
}

void _vsbModelListParser::RecurseSubDirectories()
{
	mRecursing = true;
	vector<string> dirs;


	FindSubDirectories("models",dirs);
	vector<string>::iterator dir_iter;


	for(dir_iter = dirs.begin(); dir_iter!= dirs.end(); dir_iter++)
	{
		// do not read the current file again.

		if (*dir_iter == "." || *dir_iter == "..") continue;


		mDefaultModelPath = *dir_iter;


		string sub_index_file = vsbGetSysPath()+ "/models/"+mDefaultModelPath.c_str()+"/models.idx";
		TFileLineInp sub_index_inp;
		
		if (!sub_index_inp.Open(sub_index_file.c_str())) 
			continue;
		string last_source	= mCurrentActiveSourceFile;
		TLineInp *last_line_input 	= itsLineInput;
		mCurrentActiveSourceFile = sub_index_file;

		int recurse_status = ParseSource(&sub_index_inp, 1);

		itsLineInput = last_line_input;

		if (recurse_status) AbortParse(*ItsLastError());
		mIncludedFiles.insert(mCurrentActiveSourceFile);
		mCurrentActiveSourceFile = last_source;

	}

}

void _vsbModelListParser::ParseAllBlocks()
{
	double ver_num;
	string include_filename;

	GetToken(ML_MODEL_LIST);
	GetToken(T_LBRACK);
	GetToken(ML_VERSION);
	ver_num = GetNumericValue();
	if (ver_num < MINIMUM_VERSION)
		AbortParse(ML_VERSION_OBSOLETE);

	GetToken(T_RBRACK);

	if (!mRecursing)
	  mDefaultModelPath = ".";

    PeekToken();
	while (GetCurrTokenID() == ML_INCLUDE)
	{
		if (mRecursing)
			AbortParse(ML_RECURSIVE_INCLUDE_ERROR);

		GetToken();
		GetToken(T_STRING);
		string include_filename = vsbMakeModelPath(GetCurrTokenStr());
		if (mIncludedFiles.find(include_filename) == mIncludedFiles.end())
		{
			TFileLineInp include_input;

			if (!include_input.Open(include_filename.c_str()))
			{
				string error_msg = "Unable to open include file \"";
				error_msg += include_filename;
				error_msg += "\"";
				include_input.Close();
				AbortParse(ML_INCLUDE_FILE_ERROR, error_msg.c_str());
			}
			
			string last_source				= mCurrentActiveSourceFile;
			TLineInp *last_line_input		= itsLineInput;
			mCurrentActiveSourceFile		= include_filename;

			int include_status = ParseSource(&include_input, 1);

			itsLineInput = last_line_input;

			if (include_status) AbortParse(*ItsLastError());
			mIncludedFiles.insert(mCurrentActiveSourceFile);
			mCurrentActiveSourceFile = last_source;
		}
		PeekToken();
	}

	while(GetCurrTokenID() == ML_MODEL || 
		  GetCurrTokenID() == ML_TEMPLATE ||
		  GetCurrTokenID() == ML_DEFINE_CATEGORY)
	{
		ParseCategoryDefinition();
		ParseModelBlock();
		ParseTemplateBlock();
		PeekToken();
	}

	

	if (GetCurrTokenID() != T_EOF)
		AbortParse(ML_MODEL_OR_TEMPLATE_EXPECTED);


	// Do Recursive Index load //
	if (!mRecursing)
		RecurseSubDirectories();
	
	// End Recursive Index load//

}

void _vsbModelListParser::ParseFunc()
{
	ParseAllBlocks();
}


void _vsbModelListParser::ClearParseBlockRegistrations()
{
	mComponentBlocks.clear();
	ResetModelAttributes();
	_vsbParseableModelDefinitionBlock::SetModelListParser(this);
}

void _vsbModelListParser::RegisterParseBlock(int keyWord, BlockParseFuncPtr func, 
		          bool oneShot, bool compulsory )
{
	mComponentBlocks.push_back(_vsbParseableModelDefinitionBlock(keyWord, func,oneShot,compulsory));
}

bool _vsbModelListParser::IsRegistered(int keyWord)
{
	vector<_vsbParseableModelDefinitionBlock>::iterator iter;
	for (iter = mComponentBlocks.begin(); iter != mComponentBlocks.end(); iter++)
		if (iter->KeyWord() == keyWord) return true;
	return false;
}

void _vsbModelListParser::PerformParseBlocks()
{
	vector<_vsbParseableModelDefinitionBlock>::iterator iter;
	int i;
    PeekToken();
    while(IsRegistered(GetCurrTokenID()))
	{
	    for (i=0, iter = mComponentBlocks.begin(); iter != mComponentBlocks.end(); iter++, i++)
		    (*iter).ParseBlock();	
	    PeekToken();
	}	
}

void _vsbModelListParser::TestCompulsoryBlocksParsed()
{
	vector<_vsbParseableModelDefinitionBlock>::iterator iter;
	for (iter = mComponentBlocks.begin(); iter != mComponentBlocks.end(); iter++)
		if (BlockCount(iter->KeyWord()) == 0)
		(*iter).TestCompulsoryBlockPresence();
}

bool _vsbModelListParser::HasBeenParsed(int keyword)
{
	vector<_vsbParseableModelDefinitionBlock>::iterator iter;
	for (iter = mComponentBlocks.begin(); iter != mComponentBlocks.end(); iter++)
		if ((*iter).KeyWord() == keyword)
			return ( BlockCount(iter->KeyWord()) > 0 || (*iter).NumCalls() > 0);
	return 0;
}


////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: vsbModelEntry -- Class Declaration
//
//  vsbModelEntry's contain all the information necessary to load a model and 
//  attach it to an entity. the vsbModelManager holds a list of vsbModelEntry's
//  that are loaded at program start-up (ie when the vsbModelManager class is 
//  created).
////////////////////////////////////////////////////////////////////////////////

vsbModelEntry::vsbModelEntry(): 
mId(0), mpModel(0), mValidModel(true), mHaveRelativeTexturePath(false),mRealign(false),mModelGroup(0)
{
}

vsbModelEntry::vsbModelEntry(string &id, string &filename, string &relmodelpath, LoadOptions lopt, int modelGroup):
mId(0), mpModel(0), mValidModel(true), mHaveRelativeTexturePath(false), mRealign(false),
mLogicalName(id), mFileName(filename), mRelativeModelPath(relmodelpath), mLoadOpts(lopt), mModelGroup(modelGroup)
{
	sgdMakeIdentMat4(mCockpitMat);
}

void vsbModelEntry::Load()
{
	char buffer[400];
	if (IsLoaded() || !IsValidModel()) return;

	string model_path = vsbMakeModelPath(RelativeModelPath()) ;
	 
	ssgModelPath(model_path.c_str());
	if (!HaveRelativeTexturePath())
	{
		ssgTexturePath(model_path.c_str());
	} else
	{
		string texture_path = vsbMakeModelPath(RelativeTexturePath()) ;
		ssgTexturePath(model_path.c_str());
	}

	mpModel = ssgLoad(Filename().c_str());
	
	if (mpModel)
	{
		sprintf(buffer,"Model %s loaded successfully",Filename().c_str());
		
		mpModel->ref();
		mValidModel = true;
	} else
	{
		sprintf(buffer,"Model %s load failed",Filename().c_str());
		mValidModel = false;
	}
	ulSetError(UL_DEBUG,buffer);

	if (mValidModel && (mFlatten || mStripify))
	{
		sprintf(buffer,"Model %s ",Filename().c_str());
		if (mFlatten) 
		{
			ssgFlatten((ssgEntity *)mpModel);
			strcat(buffer,"flattened ");
		}
		if (mStripify) 
		{	
			if (mFlatten) strcat(buffer,"and ");
			ssgStripify((ssgEntity *)mpModel);
			strcat(buffer,"stripified ");
		}
		strcat(buffer,"successfully");
		ulSetError(UL_DEBUG,buffer);

	}

}


// Unload the model, if possible

void vsbModelEntry::Unload()
{
	char buffer[400];
	if (!IsLoaded()) return;
	if (mLoadOpts != ME_DEMAND_PAGE) return; // can only unload demand_paged models
	if (mpModel->getRef() == 1)		// Check if it is referenced, and delete it if it isn't
	{
		sprintf(buffer,"Model %s unloaded successfully",Filename().c_str());
		ssgDeRefDelete(mpModel);
		mpModel = 0;
	} else
	{
		sprintf(buffer,"Model %s unload request blocked",Filename().c_str());
	}
	ulSetError(UL_DEBUG,buffer);
}


vsbModelEntry::~vsbModelEntry()
{
	ssgDeRefDelete(mpModel);
}


// Force load is called to load the model. 
// * If the model's load option is ME_DEMAND_PAGE, it is subsequently
//   changed to ME_DEMAND_RETAIN so it is no unloaded when its not
//   referenced
// * If the load option is ME_PRELOAD it is assumed the model is already
//   loaded and this function does nothing. The load option is nucmodified.

void vsbModelEntry::ForceLoad()
{
	if (mLoadOpts==ME_PRELOAD) return; // already loaded
	mLoadOpts = ME_DEMAND_RETAIN;      // we want to remain persistant
	Load();
}

// Force unload is called to unload thr model. 
// * If the model's load option is ME_DEMAND_RETAIN, it is
//   changed to ME_DEMAND_PAGE so it can be reloaded if entities require it
// * If the model is referenced it is not unloaded, but the load options
//   are changed to ME_DEMAND_PAGE
// * If the model's load option is ME_PRELOAD, this function has no effect


void vsbModelEntry::ForceUnload()
{
	if (mLoadOpts==ME_PRELOAD) return; // already loaded / cannot unload
	mLoadOpts = ME_DEMAND_PAGE;
	Unload();
}


bool vsbModelEntry::Attach(ssgBranch *branch)
{
  vsbModelRoot *mr = 0; 

  if (!branch) return false; // no branch to attach to;	
  if (!mpModel) Load();
  if (!mpModel) return false;

  if (branch->isAKindOf ( SSG_TYPE_MODELROOT ) && mRealign)	
  {
	  mr = (vsbModelRoot *)branch; 
	  mr->setLocalTransform(mRealignMat);
  }	

  //if (mRealign)
  //{
  //	  ssgTransform *xfm = new ssgTransform;
  //	  xfm->setTransform(mRealignMat);
  //	  xfm->addKid((ssgEntity *)mpModel);
  //	  mpModel = xfm;
  //}	
  
  branch->addKid((ssgEntity *)mpModel);

  if (mr)	
  {
	  mr->Selectors().DeleteSelectors();
	  if (mNumSelectors)
	  {	
		for (int i=0; i<mNumSelectors; i++)
			mr->Selectors().RegisterSelector(mSelectors[i].mName, mSelectors[i].mAlias, mSelectors[i].mDefault);

		mr->Selectors().ActivateSelectorHooks();
	  }
  }	
  return true;

}

bool vsbModelEntry::Attach(vsbBaseViewEntity *entity)
{

  if (!entity) return false;

  vsbModelRoot *mr = entity->EntityRoot();

  if (!mr) return false; // no branch to attach to;	
  if (!mpModel) Load();
  if (!mpModel) return false;

  mr->setLocalTransform(mRealignMat);


  //if (mRealign)
  //{
  //	  ssgTransform *xfm = new ssgTransform;
  //	  xfm->setTransform(mRealignMat);
  //	  xfm->addKid((ssgEntity *)mpModel);
  //	  mpModel = xfm;
  //}	
  
  mr->addKid((ssgEntity *)mpModel);

  mr->Selectors().DeleteSelectors();

  if (mNumSelectors)
  {	
	for (int i=0; i<mNumSelectors; i++)
		mr->Selectors().RegisterSelector(mSelectors[i].mName, mSelectors[i].mAlias, mSelectors[i].mDefault);

	mr->Selectors().ActivateSelectorHooks();
  }
	
  return true;

}


bool vsbModelEntry::Detach(ssgBranch *branch)
{
	int kid_idx;
	if (!branch) return false; // no branch to detach from
	if (!mpModel) return true;
	if ((kid_idx=branch->searchForKid((ssgEntity *)mpModel))<0) 
	{
		char buffer[400];
		sprintf(buffer,"Detach fail. Model \"%s\" is not attached to branch",LogicalName().c_str());
		ulSetError(UL_FATAL, buffer);
		return false;
    }
	branch->removeKid(kid_idx);
	if (branch->isAKindOf ( SSG_TYPE_MODELROOT ))
		((vsbModelRoot *)branch)->Selectors().DeleteSelectors();
	Unload();
	return true;
}


bool vsbModelEntry::Detach(vsbBaseViewEntity *entity)
{
	int kid_idx;
	if (!entity) return false;

    vsbModelRoot *mr = entity->EntityRoot();


	if (!mr) return false; // no mr to detach from
	if (!mpModel) return true;
	if ((kid_idx=mr->searchForKid((ssgEntity *)mpModel))<0) 
	{
		char buffer[400];
		sprintf(buffer,"Detach fail. Model \"%s\" is not attached to model root",LogicalName().c_str());
		ulSetError(UL_FATAL, buffer);
		return false;
    }
	mr->removeKid(kid_idx);
	mr->Selectors().DeleteSelectors();
	Unload();
	return true;
}


void vsbModelEntry::Realign(bool trueFalse, const sgMat4 mat) 
{
	mRealign = trueFalse;
	sgCopyMat4(mRealignMat,mat);
};


void	vsbModelEntry::CockpitMat(const sgdMat4 mat)
{
	sgdCopyMat4(mCockpitMat, mat);
}

void	vsbModelEntry::CockpitMat(const sgdCoord &coord)
{
	sgdMakeCoordMat4(mCockpitMat, coord.xyz, coord.hpr);
}

