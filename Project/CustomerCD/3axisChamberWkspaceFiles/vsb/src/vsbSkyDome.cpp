//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSkyDome.cpp
    \brief Implements the class vsbSkyDome

    This file implements a sky dome class that is designed to track
    a camera on a flat earth. It provides realistic sky colouration 
	given a specific son position and an artificial curved horizon 

 	\sa vsbSkyDome.h

    \author T. Gouthas
*/



#ifdef HAVE_WINDOWS_H
#  include <windows.h>
#endif

#include <math.h>

#include <GL/glut.h>

#include "sg.h"

#include "vsbSkyDome.h"
#include "vsbViewportContext.h"



// Coordinates for construction of sky-dome
// in meters of course

#define CENTER_ELEV   30000.0

#define UPPER_RADIUS  50000.0
#define UPPER_ELEV    20000.0

#define MIDDLE_RADIUS 70000.0
#define MIDDLE_ELEV    8000.0

#define LOWER_RADIUS  80000.0
#define LOWER_ELEV        0.0

#define BOTTOM_RADIUS 50000.0
#define BOTTOM_ELEV   -2000.0


// Skydome slice number limits
// if you consider the sky as a construction orange 
// slices, then these values represent the number of 
// these slices comprising the sky dome




// Set up dome rendering callbacks
static int vsbSkyDomePreDraw( ssgEntity *e ) 
{
    ssgLeaf *f = (ssgLeaf *)e;
    if ( f -> hasState () ) f->getState()->apply() ;

    glPushAttrib( GL_DEPTH_BUFFER_BIT | GL_FOG_BIT );

    glDisable( GL_DEPTH_TEST );
    glDisable( GL_FOG );

    return true;
}


static int vsbSkyDomePostDraw( ssgEntity *e ) 
{
    glPopAttrib();
    return true;
}

// Constructor

vsbSkyDome::vsbSkyDome( void ) 
{
}

vsbSkyDome::vsbSkyDome( int slices ) {
}

// Destructor

vsbSkyDome::~vsbSkyDome( void ) 
{
	// deleting the sky dome object wont remove the skydome 
	// from the scene graph, however it will not be accessible
	// other than through the standard Scene graph calls
}


// Initialize the sky dome object and connect it into our scene graph

ssgBranch * vsbSkyDome::Build( ) 
{

    sgVec4 color;
	
    float theta;
    int i;
	
    // set up the state

    mpDomeState = new ssgSimpleState();
    mpDomeState->setShadeModel( GL_SMOOTH );
    mpDomeState->disable( GL_LIGHTING );
    mpDomeState->disable( GL_CULL_FACE );
    mpDomeState->disable( GL_TEXTURE_2D );
    mpDomeState->enable( GL_COLOR_MATERIAL );
    mpDomeState->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    mpDomeState->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    mpDomeState->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    mpDomeState->disable( GL_BLEND );
    mpDomeState->disable( GL_ALPHA_TEST );
	
    // initialize arrays
    mpCentreDiskVert = new ssgVertexArray( 14 );
    mpCentreDiskColour = new ssgColourArray( 14);
	
    mpUpperRingVert = new ssgVertexArray( 26 );
    mpUpperRingColour = new ssgColourArray( 26);
	
    mpMiddleRingVert = new ssgVertexArray(26 );
    mpMiddleRingColour = new ssgColourArray( 26 );
	
    mpLowerRingVert = new ssgVertexArray( 26 );
    mpLowerRingColour = new ssgColourArray( 26 );
	
    // initially seed to all blue
    sgSetVec4( color, 0.0, 0.0, 1.0, 1.0 );
	
    // generate the raw vertex data
    sgVec3 center_vertex;
    sgVec3 upper_vertex[12];
    sgVec3 middle_vertex[12];
    sgVec3 lower_vertex[12];
    sgVec3 bottom_vertex[12];
	
    sgSetVec3( center_vertex, 0.0, 0.0, CENTER_ELEV );
	
    for ( i = 0; i < 12; i++ ) {
		theta = (i * 30) * SGD_DEGREES_TO_RADIANS;
		
		sgSetVec3( upper_vertex[i],
			FastCos(theta) * UPPER_RADIUS,
			FastSin(theta) * UPPER_RADIUS,
			UPPER_ELEV );
		
		sgSetVec3( middle_vertex[i],
			FastCos(theta) * MIDDLE_RADIUS,
			FastSin(theta) * MIDDLE_RADIUS,
			MIDDLE_ELEV );
		
		sgSetVec3( lower_vertex[i],
			FastCos(theta) * LOWER_RADIUS,
			FastSin(theta) * LOWER_RADIUS,
			LOWER_ELEV );
		
		sgSetVec3( bottom_vertex[i],
			FastCos(theta) * BOTTOM_RADIUS,
			FastSin(theta) * BOTTOM_RADIUS,
			BOTTOM_ELEV );
    }
	
    // generate the center disk vertex/color arrays
    mpCentreDiskVert->add( center_vertex );
    mpCentreDiskColour->add( color );
    for ( i = 11; i >= 0; i-- ) {
		mpCentreDiskVert->add( upper_vertex[i] );
		mpCentreDiskColour->add( color );
    }
    mpCentreDiskVert->add( upper_vertex[11] );
    mpCentreDiskColour->add( color );
	
    // generate the upper ring
    for ( i = 0; i < 12; i++ ) {
		mpUpperRingVert->add( middle_vertex[i] );
		mpUpperRingColour->add( color );
		
		mpUpperRingVert->add( upper_vertex[i] );
		mpUpperRingColour->add( color );
    }
    mpUpperRingVert->add( middle_vertex[0] );
    mpUpperRingColour->add( color );
	
    mpUpperRingVert->add( upper_vertex[0] );
    mpUpperRingColour->add( color );
	
    // generate middle ring
    for ( i = 0; i < 12; i++ ) {
		mpMiddleRingVert->add( lower_vertex[i] );
		mpMiddleRingColour->add( color );
		
		mpMiddleRingVert->add( middle_vertex[i] );
		mpMiddleRingColour->add( color );
    }
    mpMiddleRingVert->add( lower_vertex[0] );
    mpMiddleRingColour->add( color );
	
    mpMiddleRingVert->add( middle_vertex[0] );
    mpMiddleRingColour->add( color );
	
    // generate lower ring
    for ( i = 0; i < 12; i++ ) {
		mpLowerRingVert->add( bottom_vertex[i] );
		mpLowerRingColour->add( color );
		
		mpLowerRingVert->add( lower_vertex[i] );
		mpLowerRingColour->add( color );
    }
    mpLowerRingVert->add( bottom_vertex[0] );
    mpLowerRingColour->add( color );
	
    mpLowerRingVert->add( lower_vertex[0] );
    mpLowerRingColour->add( color );
	
    // force a repaint of the sky colors with ugly defaults
    sgVec4 fog_color;
    sgSetVec4( fog_color, 1.0, 1.0, 1.0, 1.0 );
    Repaint( color, fog_color, 0.0, 5000.0 );
	
    // build the ssg scene graph sub tree for the sky and connected
    // into the provide scene graph branch

    ssgVtxTable *center_disk, *upper_ring, *middle_ring, *lower_ring;
	
    center_disk = new ssgVtxTable( GL_TRIANGLE_FAN, 
		mpCentreDiskVert, NULL, NULL, mpCentreDiskColour );
	
    upper_ring = new ssgVtxTable( GL_TRIANGLE_STRIP, 
		mpUpperRingVert, NULL, NULL, mpUpperRingColour );
	
    middle_ring = new ssgVtxTable( GL_TRIANGLE_STRIP, 
		mpMiddleRingVert, NULL, NULL, mpMiddleRingColour );
	
    lower_ring = new ssgVtxTable( GL_TRIANGLE_STRIP, 
		mpLowerRingVert, NULL, NULL, mpLowerRingColour );
	
    center_disk->setState( mpDomeState );
    upper_ring->setState( mpDomeState );
    middle_ring->setState( mpDomeState );
    lower_ring->setState( mpDomeState );
	
    mpDomeTransform = new ssgTransform;
    mpDomeTransform->addKid( center_disk );
    mpDomeTransform->addKid( upper_ring );
    mpDomeTransform->addKid( middle_ring );
    mpDomeTransform->addKid( lower_ring );

	   // thing we add to a parent is the first drawn
    center_disk->setCallback( SSG_CALLBACK_PREDRAW, vsbSkyDomePreDraw );
    center_disk->setCallback( SSG_CALLBACK_POSTDRAW, vsbSkyDomePostDraw );

    upper_ring->setCallback( SSG_CALLBACK_PREDRAW, vsbSkyDomePreDraw );
    upper_ring->setCallback( SSG_CALLBACK_POSTDRAW, vsbSkyDomePostDraw );

    middle_ring->setCallback( SSG_CALLBACK_PREDRAW, vsbSkyDomePreDraw );
    middle_ring->setCallback( SSG_CALLBACK_POSTDRAW, vsbSkyDomePostDraw );

    lower_ring->setCallback( SSG_CALLBACK_PREDRAW, vsbSkyDomePreDraw );
    lower_ring->setCallback( SSG_CALLBACK_POSTDRAW, vsbSkyDomePostDraw );

	
    return mpDomeTransform;
}


// repaint the sky colors based on current value of sun_angle, sky,
// and fog colors.  This updates the color arrays for ssgVtxTable.
// sun angle in degrees relative to verticle
// 0 degrees = dawn
// 90 degrees = high noon
// 180 degrees = dusk
// 270 degrees = midnight
 
bool vsbSkyDome::Repaint( const sgVec4 sky_color, const sgVec4 fog_color, const double sun_angle,
					  const double vis )
{
    double diff;
    sgVec3 outer_param, outer_amt, outer_diff;
    sgVec3 middle_param, middle_amt, middle_diff;
    int i, j;
	
	mSunAngle = sun_angle;
	float angle = fabs(sun_angle-90);
    // Check for sunrise/sunset condition

    if ( (angle > 80.0) && (angle < 100.0) ) {
		// 0.0 - 0.4
		sgSetVec3( outer_param,
			(10.0 - fabs(90.0 - angle)) / 20.0,
			(10.0 - fabs(90.0 - angle)) / 40.0,
			-(10.0 - fabs(90.0 - angle)) / 30.0 );
		
		sgSetVec3( middle_param,
			(10.0 - fabs(90.0 - angle)) / 40.0,
			(10.0 - fabs(90.0 - angle)) / 80.0,
			0.0 );
		
		sgScaleVec3( outer_diff, outer_param, 1.0f / 6.0f );
		
		sgScaleVec3( middle_diff, middle_param, 1.0f / 6.0f );
    } else {
		sgSetVec3( outer_param, 0.0, 0.0, 0.0 );
		sgSetVec3( middle_param, 0.0, 0.0, 0.0 );
		
		sgSetVec3( outer_diff, 0.0, 0.0, 0.0 );
		sgSetVec3( middle_diff, 0.0, 0.0, 0.0 );
    }
    // printf("  outer_red_param = %.2f  outer_red_diff = %.2f\n", 
    //        outer_red_param, outer_red_diff);
	
    // calculate transition colors between sky and fog
    sgCopyVec3( outer_amt, outer_param );
    sgCopyVec3( middle_amt, middle_param );
	
    //
    // First, recalulate the basic colors
    //
	
    sgVec4 center_color;
    sgVec4 upper_color[12];
    sgVec4 middle_color[12];
    sgVec4 lower_color[12];
    sgVec4 bottom_color[12];
		
    double vis_factor;
	
    if ( vis < 3000.0 ) {
		vis_factor = (vis - 1000.0) / 2000.0;
		if ( vis_factor < 0.0 ) {
			vis_factor = 0.0;
		}
    } else {
		vis_factor = 1.0;
    }
	
    for ( j = 0; j < 3; j++ ) {
		diff = sky_color[j] - fog_color[j];
		center_color[j] = sky_color[j] - diff * ( 1.0 - vis_factor );
    }
	
    for ( i = 0; i < 6; i++ ) {
		for ( j = 0; j < 3; j++ ) {
			diff = sky_color[j] - fog_color[j];

			upper_color[i][j] = sky_color[j] - diff * ( 1.0 - vis_factor * 0.7);
			middle_color[i][j] = sky_color[j] - diff * ( 1.0 - vis_factor * 0.1)
				+ middle_amt[j];
			lower_color[i][j] = fog_color[j] + outer_amt[j];
			
			if ( upper_color[i][j] > 1.0 ) { upper_color[i][j] = 1.0f; }
			if ( upper_color[i][j] < 0.1 ) { upper_color[i][j] = 0.1f; }
			if ( middle_color[i][j] > 1.0 ) { middle_color[i][j] = 1.0f; }
			if ( middle_color[i][j] < 0.1 ) { middle_color[i][j] = 0.1f; }
			if ( lower_color[i][j] > 1.0 ) { lower_color[i][j] = 1.0f; }
			if ( lower_color[i][j] < 0.1 ) { lower_color[i][j] = 0.1f; }
		}
		upper_color[i][3] = middle_color[i][3] = lower_color[i][3] = 1.0f;
		
		for ( j = 0; j < 3; j++ ) {
			outer_amt[j] -= outer_diff[j];
			middle_amt[j] -= middle_diff[j];
		}
		
    }
	
    sgSetVec3( outer_amt, 0.0, 0.0, 0.0 );
    sgSetVec3( middle_amt, 0.0, 0.0, 0.0 );
	
    for ( i = 6; i < 12; i++ ) 
	{
		for ( j = 0; j < 3; j++ ) {
			diff = sky_color[j] - fog_color[j];
			

			upper_color[i][j] = sky_color[j] - diff * ( 1.0 - vis_factor * 0.7);
			middle_color[i][j] = sky_color[j] - diff * ( 1.0 - vis_factor * 0.1)
				+ middle_amt[j];
			lower_color[i][j] = fog_color[j] + outer_amt[j];
			
			if ( upper_color[i][j] > 1.0 ) { upper_color[i][j] = 1.0f; }
			if ( upper_color[i][j] < 0.1 ) { upper_color[i][j] = 0.1f; }
			if ( middle_color[i][j] > 1.0 ) { middle_color[i][j] = 1.0f; }
			if ( middle_color[i][j] < 0.1 ) { middle_color[i][j] = 0.1f; }
			if ( lower_color[i][j] > 1.0 ) { lower_color[i][j] = 1.0f; }
			if ( lower_color[i][j] < 0.1 ) { lower_color[i][j] = 0.1f; }
		}
		upper_color[i][3] = middle_color[i][3] = lower_color[i][3] = 1.0f;
		
		for ( j = 0; j < 3; j++ ) {
			outer_amt[j] += outer_diff[j];
			middle_amt[j] += middle_diff[j];
		}
		
    }
	
    for ( i = 0; i < 12; i++ ) {
		sgCopyVec4( bottom_color[i], fog_color );
    }
	
    //
    // Second, assign the basic colors to the object color arrays
    //
	
    float *slot;
    int counter;
	
    // update the center disk color arrays

    counter = 0;
    slot = mpCentreDiskColour->get( counter++ );

    // sgVec4 red;
    // sgSetVec4( red, 1.0, 0.0, 0.0, 1.0 );

    sgCopyVec4( slot, center_color );
    for ( i = 11; i>= 0; i-- ) { //@@added the 1
		slot = mpCentreDiskColour->get( counter++ );
		sgCopyVec4( slot, upper_color[i] );
    }
    slot = mpCentreDiskColour->get( counter++ );
    sgCopyVec4( slot, upper_color[11] );

	
    // generate the upper ring

    counter = 0;
    for ( i = 0; i < 12; i++ ) {		
		slot = mpUpperRingColour->get( counter++ );
		sgCopyVec4( slot, middle_color[i] );
		
		slot = mpUpperRingColour->get( counter++ );
		sgCopyVec4( slot, upper_color[i] );
    }
    slot = mpUpperRingColour->get( counter++ );
    sgCopyVec4( slot, middle_color[0] );
	
    slot = mpUpperRingColour->get( counter++ );
    sgCopyVec4( slot, upper_color[0] );

	
    // generate middle ring
    counter = 0;
    for ( i = 0; i < 12; i++ ) {
		slot = mpMiddleRingColour->get( counter++ );
		sgCopyVec4( slot, lower_color[i] );
		
		slot = mpMiddleRingColour->get( counter++ );
		sgCopyVec4( slot, middle_color[i] );
    }
    slot = mpMiddleRingColour->get( counter++ );
    sgCopyVec4( slot, lower_color[0] );
	
    slot = mpMiddleRingColour->get( counter++ );
    sgCopyVec4( slot, middle_color[0] );

	
    // generate lower ring
    counter = 0;
    for ( i = 0; i < 12; i++ ) {
		slot = mpLowerRingColour->get( counter++ );
		sgCopyVec4( slot, bottom_color[i] );
		
		slot = mpLowerRingColour->get( counter++ );
		sgCopyVec4( slot, lower_color[i] );
    }
    slot = mpLowerRingColour->get( counter++ );
    sgCopyVec4( slot, bottom_color[0] );
	
    slot = mpLowerRingColour->get( counter++ );
    sgCopyVec4( slot, lower_color[0] );
	
	sgSetVec4(mBottomColor,bottom_color[0][0],bottom_color[0][1],
		      bottom_color[0][2],bottom_color[0][3]);
    return true;
}


// reposition the sky at the specified origin and orientation
bool vsbSkyDome::Reposition(const sgVec3 camPos ) 
{
	sgCoord domeCoord;
	sgSetVec3(domeCoord.hpr,-80+(mSunAngle > 90 || mSunAngle < -90)*180,0,0);
	
	//camPos[2]/=3;
	float domeAlt = 0;
	if (camPos[2] > CENTER_ELEV-1000)
		domeAlt = camPos[2] - (CENTER_ELEV-1000);
	//sgVec3 viewPoint = {camPos[0], camPos[1], domeAlt};
	sgSetVec3(domeCoord.xyz,camPos[0], camPos[1], domeAlt);

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		//viewPoint[2] +=vsbBaseCamera::CameraToOriginDelta()[2];
		domeCoord.xyz[2] +=vsbBaseCamera::CameraToOriginDelta()[2];
	}

	sgMat4 domeMat;
	sgMakeCoordMat4(domeMat, &domeCoord);
	mpDomeTransform->setTransform( domeMat/*viewPoint*/ );
    return true;
}

