#include <string>
#include <math.h>
using namespace std;

#ifdef WIN32
#include <windows.h>
#endif

#include<GL/gl.h>

#include "vsbHUDTools.h"

vsbRGB::vsbRGB()
{
	mpTriple = &mRed;
}

vsbRGB::vsbRGB(vsbRGB &copy)
{
	mpTriple = &mRed;
	*this = copy;
}

vsbRGB::vsbRGB(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
{
	mpTriple = &mRed;
	Triple(r, g, b, a);
}

vsbRGB & vsbRGB::operator==(const vsbRGB &copy)
{
	memcpy(mpTriple,copy.mpTriple, sizeof(GLfloat)*4);
	return *this;
}

void vsbRGB::Apply()
{
	glColor4f(mRed, mGreen, mBlue, mAlpha);
}

void vsbRGB::Apply(float scaled)
{
	if (scaled<0) scaled = 0;
	if (scaled<1) scaled = 1;
	glColor4f(mRed*scaled, mGreen*scaled, mBlue*scaled, mAlpha);
}


void vsbDrawArrow(float x,float y,float angle_rad,float len)
{
    float c1 = FastCos(angle_rad-ToRad(22));
    float s1 = FastSin(angle_rad-ToRad(22));
    float c2 = FastCos(angle_rad+ToRad(22));
    float s2 = FastSin(angle_rad+ToRad(22));
    glBegin(GL_LINE_STRIP);
    glVertex3f(x, y,  0);
    glVertex3f(x+c1*len, y+s1*len,  0);
    glVertex3f(x+c2*len, y+s2*len,  0);
    glVertex3f(x, y,  0);
    glEnd();
}

void vsbDrawCircle(float x,float y,float radius, int resolution)
{
        if (resolution < 3) resolution=3;
    else 
        if (resolution > 19) resolution=19;


    float step_coords[20][2];
    int count;
    int i;
    float angle;
    
     float incr = 0.7853981633974/resolution;
    for(count=0, angle=0.0f; angle< 0.7853981633974+incr; count++,angle+=incr)
    {
        step_coords[count][0] = radius*FastCos(angle);
        step_coords[count][1] = radius*FastSin(angle);
    }

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x+step_coords[i][0],y+step_coords[i][1]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x-step_coords[i][0],y+step_coords[i][1]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x+step_coords[i][0],y-step_coords[i][1]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x-step_coords[i][0],y-step_coords[i][1]);
    }
    glEnd();

    ////

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x+step_coords[i][1],y+step_coords[i][0]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x-step_coords[i][1],y+step_coords[i][0]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x+step_coords[i][1],y-step_coords[i][0]);
    }
    glEnd();

    glBegin(GL_LINE_STRIP);
    for(i=0; i<count; i++)
    {
        glVertex2d(x-step_coords[i][1],y-step_coords[i][0]);
    }
    glEnd();
}

void vsbDrawArc(float x,float y,float radius, float start_angle_rad, float end_angle_rad)
{
}


void vsbMakeEarthToBodyMat(sgMat4 m, float h, float p, float r)
{
	float cr, cp, ch, sr, sp, sh, srsp, srcp, crsp;
	if (h==0)
	{
		sh = 0;
		ch = 1;
	} else
	{
		sh = FastSin( h * SG_DEGREES_TO_RADIANS ) ;
		ch = FastCos( h * SG_DEGREES_TO_RADIANS ) ; 
	}

	if (p==0)
	{
		sp = 0;
		cp = 1;
	} else
	{
		sp = FastSin( p * SG_DEGREES_TO_RADIANS ) ;
		cp = FastCos( p * SG_DEGREES_TO_RADIANS ) ; 
	}

	if (r==0)
	{
		sr = 0;
		cr = 1;
		srsp = 0;
		srcp = 0;
		crsp = sp;
	} else
	{
		sr = FastSin( r * SG_DEGREES_TO_RADIANS ) ;
		cr = FastCos( r * SG_DEGREES_TO_RADIANS ) ; 

		srsp = sr*sp;
		srcp = sr*cp;
		crsp = cr*sp;
	}

	m[0][0] = cp*ch;
	m[1][0] = -cr*sh + srsp*ch;
	m[2][0] = sr*sh + crsp*ch;
	m[3][0] = 0;

    m[0][1] = cp*sh;
	m[1][1] = cr*ch + srsp*sh;
	m[2][1] = -sr*ch + crsp*sh;
	m[3][1] = 0;

	m[0][2] = -sp;
	m[1][2] = srcp;
	m[2][2] = crsp;
	m[3][2] = 0;

	m[0][3] = 0;
	m[1][3] = 0;
	m[2][3] = 0;
	m[3][3] = 1;
}