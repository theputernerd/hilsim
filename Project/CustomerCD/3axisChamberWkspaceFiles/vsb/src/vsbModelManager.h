////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbModelManager.h
    \brief Defines the class vsbModelManager and itas auxillary classes
    
     The class vsbModelManager is responsible for managing and caching 3D 
	 mesh models that may be attached to vsbBaseViewEntity derrived objects.
	 
	\sa vsbModelManager.cpp

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifndef _vsbModelManager_H
#define _vsbModelManager_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _SET_
#include <set>
#define _SET_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbBaseViewEntity_H
#include "vsbBaseViewEntity.h"
#endif

#ifndef _vsbModelRoot_H
#include "vsbModelRoot.h"
#endif

#ifndef _vsbAttachmentPoint_H
#include "vsbAttachmentPoint.h"
#endif

class DLL_VSB_API _vsbModelListParser;
class DLL_VSB_API vsbModelEntry;

enum EGeometryGroup
{
	GEOMETRY_UNCATEGORISED = 0,
	GEOMETRY_MISSILE,
	GEOMETRY_WEAPON,
	GEOMETRY_MACHINERY,
	GEOMETRY_UAV,
	GEOMETRY_AIRCRAFT_CIVIL,
	GEOMETRY_AIRCRAFT_MILITAR,	
	GEOMETRY_WATERCRAFT_CIVIL,
	GEOMETRY_WATERCRAFT_MILITARY,
	GEOMETRY_LANDCRAFT_CIVIL,
	GEOMETRY_LANDCRAFT_MILITARY,
	GEOMETRY_BUILDING,
	GEOMETRY_CIVIL_INFRASTRUCTURE,
	GEOMETRY_GEOPHYSICAL,
	GEOMETRY_FAUNA,
	GEOMETRY_FLORA
};




////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbSelectorNode
    \brief Contains information about ssgSelector nodes that should nominally
    (although not necessarily) exist in the model
    
    This information is critical in the linking of selectors in a model with 
    an vsbBaseViewEntity's selector interface in the vsbRootNode which will
    be the parent of the model on Attachment

*/
////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbSelectorNode
{
public:
	std::string mName;       //!< The actual string name of the ssgSelector 
	std::string mAlias;      //!< The logical string the ssgSelector will be accesed through    
	int	   mDefault;    //!< The default ssgSelector state bitmask

    /*! \brief Default Constructor

    Assumes the logical and actual names of the selector are identical
    */
	vsbSelectorNode() {mName = mAlias = "";};

    /*! \brief Assignment operator
    */
	vsbSelectorNode &operator = (const vsbSelectorNode &src) 
		{mName = src.mName; mAlias = src.mAlias; mDefault = src.mDefault; return *this;};

};


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbModelEntry
    \brief vsbModelEntry's contain all the information necessary to load a model and 
	attach it to an entity.

	The vsbModelManager holds a list of vsbModelEntry's
    that are loaded at program start-up (ie when the vsbModelManager class is 
    created).
*/
////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbModelEntry
{
friend class DLL_VSB_API _vsbModelListParser;
friend class DLL_VSB_API vsbModelManager; 
public:	
	
	//! Enumeration of the type of load options for models
	enum LoadOptions {
		ME_PRELOAD,			//!< The model is loaded once and retained in memory
		ME_DEMAND_PAGE,		//!< The model is loaded on demand, unoladed on non demand	
		ME_DEMAND_RETAIN	//!< The model is loaded on demand and retained  
	};


					//! Get the logical name of the model
	const std::string	&LogicalName() const				{return mLogicalName;};

					//! Get the file name of the model (path info exclusive)
	const std::string	&Filename()	const				{return mFileName;	};

					//! Get the path of the model relative to the root model path
	const std::string	&RelativeModelPath() const		{return mRelativeModelPath;	};

					//! Get the LoadOptions type for this model
	LoadOptions		LoadOpts()const					{return mLoadOpts;	};

					//! Get the unique model Id (index into the model entry list)
	int				Id() const						{return mId;		};

					//! Determine whether there is a relative texture path
	bool			HaveRelativeTexturePath() const {return mHaveRelativeTexturePath;};	

					//! Get the relative texture path 
	const std::string	RelativeTexturePath()const		{return mRelativeTexturePath;};


					/*! \brief Attach a model to a vsbBaseViewEntity derrived object

					Attaches the model to a scene graph branch of a vsbBaseViewEntity 
					derrived object, loading it if not currently in memory. If the 
					branch is a vsbRoot, the model's selectors are also registered.
					\note models attached to branches in this way must be Detached using the
					complementing Detach function and NOT deleted.

					\param branch The ssgBranch The branch the model is to be attached to
					\sa vsbModelEntry::Attach
					*/
	bool			Attach(vsbBaseViewEntity *entity);

					//! Detatch the model from the vsbBaseViewEntity derived object
	bool			Detach(vsbBaseViewEntity *entity);

					/*! \brief Attach a model to an ssgBranch

					Attaches the model to a scene graph branch, loading it if not currently in
					memory. If the branch is a vsbRoot, the model's selectors are also registered.
					\note models attached to branches in this way must be Detached using the
					complementing Detach function and NOT deleted.

					\param branch The ssgBranch The branch the model is to be attached to
					\sa vsbModelEntry::Attach
					*/
	bool			Attach(ssgBranch *branch);

					//! Detatch the model from the ssgBranch
	bool			Detach(ssgBranch *branch);

					/*! \brief Force load is called to load and retain the model in memory  

					The model is loaded according to the fillowin rules;
					- If the model's load option is ME_DEMAND_PAGE, it is subsequently
					  changed to ME_DEMAND_RETAIN so it is no unloaded when its not referenced
					- If the load option is ME_PRELOAD it is assumed the model is already
					  loaded and this function does nothing. The load option is nucmodified.
					*/
	void			ForceLoad();


					/*! \brief Force unload is called to unload thr model. 

					The model is unloaded according to the fillowin rules;
					 - If the model's load option is ME_DEMAND_RETAIN, it is
					   changed to ME_DEMAND_PAGE so it can be reloaded if entities require it
					 - If the model is referenced it is not unloaded, but the load options
					   are changed to ME_DEMAND_PAGE
					 - If the model's load option is ME_PRELOAD, this function has no effect
					*/
	void			ForceUnload();

					//! Inquire about the validity of the model
	bool			IsValidModel() const {return mValidModel;};

					//! Inquire whether the model has been loaded
	bool			IsLoaded() const     {return mpModel != 0;};

	int				ModelGroup() { return mModelGroup; }

					//! Coordinates and orientation of cockpit
	const sgdMat4	&CockpitMat() const {return mCockpitMat;};

	void			CockpitMat(const sgdMat4 mat);
	void			CockpitMat(const sgdCoord &coord);


protected:
					/*! \brief Constructor
					\param tagname The logical name of the model
					\param filename The name of the file containing the model geometry
					\param relmodelpath The relative model path to the model file
					\param lopt The load options for this model
					\note The construtor is protected so as to restrict creation privelleges
					to its friend classes.
					*/
					vsbModelEntry(std::string &tagname, std::string &filename, 
									std::string &relmodelpath, LoadOptions lopt, int modelGroup);
					//! Default constructor
					vsbModelEntry();

					//! Default destructor
	virtual			~vsbModelEntry();

					/*! \brief Load the model

					Low level function to physically load a model into memory. It ensures
					that the model is not currently loaded, and applies all the filtering
					and set-up attributes specified in the vsbModelEntry, once loaded.
					
					*/
	void			Load();

					/*! \brief Unload the mode

					The model is unloaded only if
					- It is of the load type ME_DEMAND_PAGE
					- And there are no references to it
					*/
	void			Unload();

                    //! Set the validity status of the model to true
	void			ResetValidity() {mValidModel = true; };

					//! Get the relative texture path of the model
	inline void		RelativeTexturePath(std::string &reltextpath);

					/*! \brief Set the realignment status of the model

					If realignment is enabled, the matrix supplies the realignment matrix
					otherwise it is ignored. Realignment involves applying rotations and 
                    translations specified in the model index file to alter the orientation 
                    and centroid of the model.

					\param trueFalse Realignment status
					\param mat The realignment matrix
					*/
	void			Realign(bool trueFalse, const sgMat4 mat);

					/*! \brief Inquire whether realignment of the model relative to its root is necessary

                    Realignment involves applying rotations and translations specified in the model
                    index file to alter the orientation and centroid of the model.
                    */
	bool			Realign() {return mRealign;};


	const vsbAttachmentPoint * GetAttachmentPoint(long pt_id) ;	
	const vsbAttachmentPoint * GetAttachmentPoint(const std::string &pt_name) ;	

private:

	vsbSelectorNode	mSelectors[MAX_SELECTORS];  //!< Contains the selector attribute nodes expected in the model
	int					mNumSelectors;              //!< The number of selector in the above list
	std::string			mLogicalName;                //!< The logical name of the model
	std::string			mFileName;                  //!< The model file name (path exclusive)
	std::string			mRelativeModelPath;         //!< The path to the model relative to the base model path
	std::string			mRelativeTexturePath;       //!< The relative texture path (if required) relative to the base model path
	bool				mHaveRelativeTexturePath;   //!< Flag identifying whether texture path is present
	int					mModelGroup;
	LoadOptions			mLoadOpts;                  //!< Load style options
	int					mId;                        //!< The id of the model
	bool				mValidModel;                //!< Flag identifying whether the model is valid  
	bool				mStripify;                  //!< Flag identifying whether the model is to be stripified on load
	bool				mFlatten;                   //!< Flag identifying whether the model is to be flattened on load
	bool				mRealign;                   //!< Flag identifying whether the model is to be realigned on load
	sgMat4				mRealignMat;                //!< The realignment matrix (combination of centroid and orientation)
	ssgBase *			mpModel;                    //!< Pointer to the model geometry 
	sgdMat4				mCockpitMat;

	std::map<long,vsbAttachmentPoint> mAttachmentPoints;

};


inline void	vsbModelEntry::RelativeTexturePath(std::string &reltextpath)
{
	mHaveRelativeTexturePath = true;
	mRelativeTexturePath = reltextpath;
}

////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbModelManager
    \brief The class vsbModelManager is responsible for managing and caching 3D 
	 mesh models that may be attached to vsbBaseViewEntity derrived objects.

*/
////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbModelManager
{
friend class _vsbModelListParser;
public:
							/*! \brief Constructor

							On construction of the vsbModelManager the model index file is 
							parsed, creating a database of 3D mesh models and pertinant attributes
							for those models.

							\note VSB only allows one instance of this class per application, the
							assumption being that theres only one scene per application.

							\sa LoadIndex()
							*/
							vsbModelManager();

							/*! \brief Virtual Destructor
							*/
							~vsbModelManager();

							/*! \brief Clear the model entry list 

							The model entry list, mModelList, contains a list of entries for all the
							models in the index file, and consequently managed by this vsbModelManager
							object 
							\sa _vsbModelList, vsbModelEntry
							*/
		void				Clear();

							/*! \brief Attach a model to an ssgBranch

							Attaches a model to a scene graph branch, loading it if not currently in
							memory. If the branch is a vsbRoot, the model's selectors are also registered.
							\note models attached to branches in this way must be Detached using the
							complementing Detach function and NOT deleted.

							\param tagname The logical name of the model
							\param branch The ssgBranch The branch the model is to be attached to
							\sa vsbModelEntry::Attach
							*/
        bool				Attach(const std::string &tagname, ssgBranch *branch);

							/*! \brief Detach a model from an ssgBranch

							As well as Detatching the model, it deregisters any selectors, and unloads the 
							model if there are no other references to it
							\sa vsbModelEntry::Detach
							*/
        bool				Detach(const std::string &tagname, ssgBranch *branch);

							/*! \brief Attach a model to a vsbBaseViewEntity derrived object

							Attaches a model to a scene graph branch of the specified vsbEntity, loading it 
							if not currently in memory. If the branch is a vsbRoot, the model's selectors 
							are also registered.
							\note Models attached to entities in this way must be Detached using the
							complementing Detach function and NOT deleted.
							\sa vsbModelEntry::Attach
							*/
        bool				Attach(const std::string &tagname, vsbBaseViewEntity *entity);

							/*! Detach a model from a vsbBaseViewEntity derrived object

							As well as Detatching the model, it deregisters any selectors, and unloads the 
							model if there are no other references to it
							\sa vsbModelEntry::Detach
							*/
        bool				Detach(const std::string &tagname, vsbBaseViewEntity *entity);


							//! Get the number of available Models (total number in index file)
		int					Count() const {return mCount;};

							/*! \brief Load the model index file recording all the model attributes
							*/
		void				LoadIndex();

							/*! \brief Get the logical name of the first model in the vsbModelEntry list
							\returns name string or NULL if no models are available
							*/
        const std::string *		FindFirstModelName();

							/*! \brief Get the logical name of the next model in the vsbModelEntry list
							\returns name string or NULL if there are no more models
							*/
        const std::string *	FindNextModelName();

		const std::string *	CurrentModelGroup();

		BOOL				ModelExists(const std::string &tagname)
							{
								return FindByTagName(tagname)!=0;
							};


							// Model entry lookup functions	
												
        vsbModelEntry *	FindByTagName(const std::string &tagname);
		vsbModelEntry *	FindFirst();
		vsbModelEntry *	FindNext();			


		std::set<std::string> &AttachmentNameList() {return mAttachmentPointNameList;};
private:
	


		int										mCount;				//!< number of available models

		static char *							mspDefaultCategories[];

		static int								mInstanceCount;		//!< Number of instances of this class

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 
#       ifdef _MSC_VER
#       pragma warning( push )
#       pragma warning( disable : 4251 ) // STL not exported warning
#       endif

        std::map<std::string, vsbModelEntry *>	mModelList;			//!< list of model specification entries
        std::map<std::string, vsbModelEntry *>::iterator mModelListIter;		//!< iterator for aove

		std::set<std::string> mAttachmentPointNameList;

#       ifdef _MSC_VER
#       pragma warning( pop )
#       endif

};


/*! \func vsbMakeModelPath

 Composes an explicit, fully qualified path to the model filename specified in 
 the fname parameter. This is based on the SysPath and the hard wired rule that 
 model paths in the model index file are implicitly relative to the "models" 
 subdirectory of 
 the directory specified by the vsbGetSysPath() function.
*/

std::string vsbMakeModelPath(std::string fname);

#endif