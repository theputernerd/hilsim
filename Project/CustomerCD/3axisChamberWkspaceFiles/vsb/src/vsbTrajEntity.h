#ifndef _vsbTrajEntity_H
#define _vsbTrajEntity_H

//////////////////////////////////////////////////////////////////
/*! 
    \file vsbTrajEntity.h
    \brief Defines an entity class vsbTrajEntity that has the ability
    to retain a temporal state history
    
    This file defines several helper classes and the primary class
    vsbTrajEntity class designed to maintain, along with its current 
    state data, (as every vsbBaseEntity derived class does)  
    a state data (trajectory) history. Any time referenced state variables
    can be stored as part of the trajectory history.
    
    This enables entities to, for example, load a future state history on 
    instantiation and step through it, or to store instantaneous state as 
    each time step elapses to facilitate a replay capability or the capability 
    to provide history information for trajectory plots or any other history 
    aware behaviour.
 
   	\sa vsbTrajEntity.cpp

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#include "vsbBaseViewEntity.h"


#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbTrajLeaf_H
#include "vsbTrajLeaf.h"
#endif


#define _BV_TRAJ_ENTITY		0x0002
inline int vsbTypeTrajEntity   () { return _BV_TRAJ_ENTITY | vsbTypeBaseEntity() ; }


enum InstanceDataType;

#define MAX_TRAJ_VARS 50 /*!< The maximum number of variables each 
                             instantaneous trajectory entry may have */



/////////////////////////////////////////////////////////////////
/*! 
    \class vsbInstanceData vsbTrajEntity.h 

    \brief Represents one instantaneous snapshot of the trajectory history

*/
////////////////////////////////////////////////////////////////
enum EInterpolationStyle
{
	INTERP_NONE,    //!< No interpolation
	INTERP_LINEAR,  //!< Linear interpolation
	INTERP_SLERP,	//!< Slerp (Spherical linear) assumes h,p,r order
};


class DLL_VSB_API vsbInstanceData   
{

public:
				/*! \brief Default constructor

				This constructor allocates the default number of variables
				for the vsbInstanceData from the static msDefaultAllocationSize member.
				These variables hold parameters such as X, Y, Z etc. as required by the
				Traj Entity.
				
				*/
				vsbInstanceData();


				/*! \brief Copy constructor */
				vsbInstanceData(const vsbInstanceData &asomeCopy);

				/*! \brief Constructor

				This constructor allocates the number of variables for the 
				vsbInstanceData from the parameter nElements. These variables 
				hold parameters such as X, Y, Z etc. as required by the
				Traj Entity.
				
				*/
				vsbInstanceData(int nElements);


				//! Destructor
	virtual     ~vsbInstanceData();

				/*! \brief [] operator returns a referene to an indexed variable
					within the Instance Data.
				*/
	double		&operator [](int idx);

				//! Assignment operator
	vsbInstanceData &operator = (const vsbInstanceData &someCopy);

				/*! \brief Resize the instance data 
				
				Reallocates internal variable array, changing the number of 
				variables comprising it whilst maintainig the original values
				of variables still present after the resize
				*/
	void		Resize(int n);
	int			Size() { return mNumElements; }

				/*! \brief Interpolate between two vsbInstanceData instances

				Interpolates between this vsbInstanceData and the one passed
				in by reference in the inst. 
				
                \param inst The second vsbInstanceData object to bound the interpolation
				\param proportion Linear interpolation proportion ranging from 0.0 to 1.0
				with 0.0 resulting in <I>this<?I> returned, and 1.0 resulting in inst beign returned
				\return a new vsbInstanceData object resulting from the interpolation
				*/
	vsbInstanceData Interpolate(vsbInstanceData &inst, float proportion, const EInterpolationStyle *interpStyle = 0);
private:
	
	static int	msDefaultAllocationSize; //!< Default number of variables
	double	   *mpData;					 //!< Storage for component variables	
	int			mNumElements;			 //!< the Number of variables allocated

};



/////////////////////////////////////////////////////////////////
/*! 
    \class vsbTrajVar vsbTrajEntity.h 

    \brief Maintains the list of variables that comprise a trajectory

	This class defines an object that maintains the list of variables 
	that make-up a trajectory. These variables have a direct corellation 
	to the variables in vsbInstanceData, however this class provides
	an identifier for those variables and higher level methods of accessing 
	and setting those variables.

*/
////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbTrajVar
{
public:
				/*! \brief Default constructor
				*/
				vsbTrajVar();

				/*! \brief Virtual destructor
				*/
	virtual		~vsbTrajVar();

				/*! \brief Add a new variable into the Trajectory variable list

				Insert a variable into the Trajectory variable list including a pointer
				to the address where the value may be retreived from. 

				\param varname The name of the variable
				\param addr  The address of the location where the value may be retreived from.
				This address is intended for automatic update of the variable value in the trajectory
				and will usually be a pointer to a member variable in the vsbTrajEntity derived class.
				See <I>UpdateInstanceData</I> and <I>RefreshFromInstanceData</I> 
				\returns true if added successfully
				*/
    bool		AddVar(const std::string &varname,double *addr = 0, EInterpolationStyle interpStyle = INTERP_NONE);

				/*! \brief Retreives the index offset of the specified variable
				    \param index_str the name of the variable. If the variable is
					zero, this variable does not participate in automatic updates 
					\returns the index offset or -1 if variable specified is not found in the list
				*/
    int			Index(const std::string &index_str);

				/*! \brief Clears the variable list 
				*/
	void		Clear();

				/*! \brief Retreives a reference to the named variable within the
				specified vsbInstanceData object.
				\params varname The name of the variable to retreive a reference to
				\params inst The vsbInstanceData containing the named variable
				\returns A reference to a variable within <I>inst</I>
				*/
    double		&Var(const std::string &varname, vsbInstanceData &inst);


				/*! \brief Retreives a reference to the indexed variable within the
				specified vsbInstanceData object.
				\params index The index offset of the variable to retreive a reference to
				\params inst The vsbInstanceData containing the named variable
				\returns A reference to a variable within <I>inst</I>
				*/
	double		&Var(int index, vsbInstanceData &inst);


				/*! \brief Updates all the vsbInstanceData variables from the external source pointers

				Updates all the vsbInstanceData variables that have a non zero address
				by retreiving the new values from those addresses. If the addresses are zero
				no update of the corresponding trajectory value is performed.
				\params inst The vsbInstanceData containing the named variable
				*/

	void		UpdateInstanceData(vsbInstanceData &inst);


				/*! \brief Refreshes external state variables from vsbInstanceData 

                Refreshes all the external state values pointed to by the source pointers 
				from the current values in vsbInstanceData. If the the source pointer for a variable
				is zero, then no external state variable is updated
				\params inst The vsbInstanceData containing the named variable
				*/
	void		RefreshFromInstanceData(vsbInstanceData &inst) const;

				//! Returns the number of variables defined in the trajectory
	int			Count() { return mVarCount; }

	const EInterpolationStyle *InterpStyle() const {return &mInterpStyle[0];};


protected:
	int			mVarCount;				//!< The number of variables in the trajectory
    std::string	mVars[MAX_TRAJ_VARS];	//!< The variable names
	EInterpolationStyle mInterpStyle[MAX_TRAJ_VARS]; // The interpolation style
	double      *mpVarAddress[MAX_TRAJ_VARS]; //!< The variable source addresses
	
};


/////////////////////////////////////////////////////////////////
/*! 
    \class vsbTrajEntity vsbTrajEntity.h 

    \brief Defines an vsbBaseViewEntity derived class that maintains a 
    trajectory history

     
    The vsbTrajEntity class is designed to maintain, along with its current 
    state data, (as every vsbBaseEntity derived class does)  
    a state data (trajectory) history. Any time referenced state variables
    can be stored as part of the trajectory history.
    
    This enables entities to, for example, load a future state history on 
    instantiation and step through it, or to store instantaneous state as 
    each time step elapses to facilitate a replay capability or the capability 
    to provide history information for trajectory plots or any other history 
    aware behaviour.

*/
////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbTrajEntity : public  vsbBaseViewEntity
{
public:

                        /*! \brief Constructor

                        Initialises the vsbTrajEntity object.
                        \param name A name for the vsbTrajEntity object
                        \param postfixed If this parameter is set to true, the object name is
                        constructed from the provided name postfixed with a unique integer value
				         */
                        vsbTrajEntity(const std::string &name, bool postfixed = false);

                        //*! Virtual destructor
	virtual				~vsbTrajEntity();

						/*! \brief Get the string name of the class of entity
						*/
	virtual const std::string &	ClassName() const;


                        //! Clear the trajectory history for this entity
    virtual void		ClearTraj()= 0; 

                        /*! \brief Set the entity's state for the specified time from history data
                        if available.

                        For the required time instant, trajectory data is looked up from th mTraj
                        member, and if  mInterpolate is set to true, the data is interpolated for
                        the specified time, otherwise the trajectory data for the lower time bound 
                        is used. This data is then used to update the current state of the vsbTrajEntity.

                        The CheckBounds member is called to determine what to do when the requested
                        time is beyond the time range specified for this entity.
                        \sa also MinTime, MaxTime, MinMaxTime, InfiniteTimeRange found in vsbBaseViewEntity.

                        \param timeto The time at which to set the entity's state
                        */
    virtual void		SetCurrTime(float timeto) = 0;

                        /*! \brief Advance the entity's state for the specified time increment if 
                        history data is available. Same interpolation rules apply as in <I>SetCurrTime</I>.
                
                        For the required time increment advance from the current time, trajectory data is 
                        looked up from the mTraj member, and if mInterpolate is set to true, the data is 
                        interpolated for the new calculated current time, otherwise the trajectory data for 
                        the lower time bound is used.  This data is then used to update the current state of 
                        the vsbTrajEntity.

                        The CheckBounds member is called to determine what to do when the requested
                        time is beyond the time range specified for this entity.
                        \sa MinTime, MaxTime, MinMaxTime, InfiniteTimeRange found in vsbBaseViewEntity.

                        \param timeto The time increment for which to advance the entity's state
                        */
	virtual void		AdvanceTime(float deltaTime) = 0;

						/*! \brief Add a new vsbInstanceData entry for a specified time instant 
						into the trajectory
						
						Creates a new vsbInstanceData entry for the time specified by <I>atTime</I> and 
						inserts it into the trajectory <I>mTraj</I>. If the specified time already exists 
						in the trajectory, it is overwritten. Otherwise it is inserted int the apropriate 
						position according to its time.

						If the <I>updateFromState</I> parameter is true, then the new vsbInstanceData entry
						is initialised from the current state of the entity, which happens via the 
						<I>mpVarAddress</I> members of the vsbTrajVar object <I>mTrajVarDef</I>.

						\param atTime The time stamp of the new entity state (vsbInstanceData) to insert 
						into the trajectory

						\param updateFromState If set to true, the entity state inserted into the trajectory 
						is initialised with the entity's current state parameters, otherwise the new entity 
						state remains un-initialised.

						 \sa UpdateInstanceDataFromState
						*/
	virtual bool		AddNewTime(float atTime, bool updateFromState = false)=0;
	

						/*! \brief Updates the current vsbInstanceData, <I>mCurrData</I>, from the entity's 
						current state

						\note <I>mCurrData</I> reflects the state of the entity in a vsbInstanceData object, 
						however the	actual state of the entity is essentially a set of member variables of the 
						vsbTrajEntity class.

						\sa UpdateInstanceData, UpdateStateFromInstanceData
						*/
	virtual void		UpdateInstanceDataFromState();

						/*! \brief Updates the entity's current state from the current vsbInstanceData, 
						<I>mCurrData</I>

						This is essentially the converse of what UpdateInstanceDataFromState does. The entity's 
						state member functions are updated from the current vsbInstanceData <I>mCurrData</I> 

						\sa RefreshFromInstanceData, UpdateInstanceDataFromState
						*/
	virtual void		UpdateStateFromInstanceData();

						/*! \brief Add a new state variable to be included in mTraj's vsbInstanceData entries.
						
						Defines a new time dependant variable to be tracked in the vsbInstanceData entries of
						mTraj. This variable is registered with mTrajVarDef so that it new vsbInstanceData
						instances contain it. 
						
						\note Adding new variables is illegal if there is already data in mTraj since this 
						would result in an inconsistent set of variables in the mTraj entries. It is essential 
						that all mTrajVar's vsbInstanceData entries have the same set of variables.

						\param varname The name of the variable
						\param addr  The address of the location where the value may be retreived from.
						This address is intended for automatic update of the variable value in the trajectory
						and will usually be a pointer to a member variable in the vsbTrajEntity derived class.
						\returns  true if variable added successfully successfully

						/sa The <I>AddVar</I> member function of vsbTrajVar, UpdateInstanceData and RefreshFromInstanceData 
						*/
    virtual bool		AddVar(const std::string &varname, double *addr = 0, EInterpolationStyle interpStyle = INTERP_NONE);

						/*! \brief Get a reference to the trajectory data for this entity
						\returns A reference to vsbInstanceDataMap
                        \note CANNOT BE EXPORTED OUTSIDE OF DLL
						*/
//	vsbInstanceDataMap  &TrajData() { return mTraj; }

						/*! \brief Get a reference to the variable in the current vsbInstanceData object,
						mCurrData, with the specified index
						\param index The index of the required trajectory variable
						\returns A reference to a float variable
						*/
	double &		operator[](int index);

						/*! \brief Get a reference to the variable in the current vsbInstanceData object,
						mCurrData, with the specified name
						\param varname The name of the required trajectory variable
						\returns A reference to a float variable
						*/
    double &		operator[](const std::string &varname);

						
						/*! \brief Get a reference to the current vsbInstanceData object, mCurrData
						\returns A reference to vsbInstanceData
						*/
	vsbInstanceData &	CurrData() { return mCurrData; };


	virtual bool		FirstInstanceData(vsbInstanceData &data) = 0;
	virtual bool		LastInstanceData(vsbInstanceData &data)  = 0;
	virtual bool		NextInstanceData(vsbInstanceData &data) = 0;
	virtual bool		PrevInstanceData(vsbInstanceData &data) = 0;


						/*! \brief Get the index for the named trajectory variable registered with <I>mTrajVarDef</I>
						\returns An integer index, -1 if the variable is not found
						*/
    int					FindVarIdx(const std::string &varname);

						/*! \brief Inquire about the intorpolation status
						*/
	bool				Interpolate();

						/*! \brief Set the intorpolation status
						*/
	void				Interpolate(bool trueFalse) {mInterpolate = trueFalse;};

						/*! \brief Inquire about the auto orientation status
						*/
	bool				AutoOrientation()				{return mAutoOrientation;};

						/*! \brief Set the auto orientation status
						*/
	void				AutoOrientation(bool trueFalse) {mAutoOrientation = trueFalse;};

protected:

	bool				mInterpolate;		//!< True if interpolation is enable
	bool				mAutoOrientation;   //!< True if automatic orientation generation is enabled


	vsbTrajVar			mTrajVarDef;		//!< The trajectory variable definitions


	vsbInstanceData		mCurrData;			//!< The currently selected vsbInstanceData instance

	vsbTrajLeaf			*mpTrajLeaf;

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958  
/*
#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

	vsbInstanceDataMap  mTraj;				//!< The trajectory data, a map of type <time,vsbInstanceData>
	vsbInstanceDataIter mLowerIter;			//!< Trajectory iterator
	vsbInstanceDataIter mUpperIter;			//!< Trajectory iterator	

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

private:
*/

};	


#endif