//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSun.cpp
    \brief Implements the class vsbSun
 	\sa vsbSun.h

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "ssg.h"

#include "vsbRenderOptions.h"
#include "vsbSun.h"
#include "vsbSysPath.h"


/*! return a sphere object as an ssgBranch (and connect in the
 specified ssgSimpleState
*/
ssgBranch *vsbMakeSphereObject( ssgSimpleState *state, ssgColourArray *cl,
			  double radius, int slices, int stacks,
			  ssgCallback predraw, ssgCallback postdraw )
{
    float rho, drho, theta, dtheta;
    float x, y, z;
    float s, t, ds, dt;
    int i, j, imin, imax;
    float nsign = 1.0;
    ssgBranch *sphere = new ssgBranch;
    sgVec2 vec2;
    sgVec3 vec3;

    drho = (float) (SG_PI / stacks);
    dtheta = (float) (2.0 * SG_PI / slices);

    /* texturing: s goes from 0.0/0.25/0.5/0.75/1.0 at +y/+x/-y/-x/+y
       axis t goes from -1.0/+1.0 at z = -radius/+radius (linear along
       longitudes) cannot use triangle fan on texturing (s coord. at
       top/bottom tip varies) */

    ds = 1.0f / slices;
    dt = 1.0f / stacks;
    t = 1.0;  /* because loop now runs from 0 */
    imin = 0;
    imax = stacks;

    /* build slices as quad strips */
    for ( i = imin; i < imax; i++ ) {
	ssgVertexArray   *vl = new ssgVertexArray();
	ssgNormalArray   *nl = new ssgNormalArray();
	ssgTexCoordArray *tl = new ssgTexCoordArray();

	rho = i * drho;
	s = 0.0;
	for ( j = 0; j <= slices; j++ ) {
	    theta = (j == slices) ? 0.0f : j * dtheta;
	    x = (float) (-FastSin(theta) * FastSin(rho));
	    y = (float) (FastCos(theta) * FastSin(rho));
	    z = (float) (nsign * FastCos(rho));

	    // glNormal3f( x*nsign, y*nsign, z*nsign );
	    sgSetVec3( vec3, x*nsign, y*nsign, z*nsign );
	    sgNormalizeVec3( vec3 );
	    nl->add( vec3 );

	    // glTexCoord2f(s,t);
	    sgSetVec2( vec2, s, t );
	    tl->add( vec2 );

	    // glVertex3f( x*radius, y*radius, z*radius );
	    sgSetVec3( vec3, (float)(x*radius), (float)(y*radius), (float)(z*radius) );
	    vl->add( vec3 );

	    x = (float) (-FastSin(theta) * FastSin(rho+drho));
	    y = (float) (FastCos(theta) * FastSin(rho+drho));
	    z = (float) (nsign * FastCos(rho+drho));

	    // glNormal3f( x*nsign, y*nsign, z*nsign );
	    sgSetVec3( vec3, x*nsign, y*nsign, z*nsign );
	    sgNormalizeVec3( vec3 );
	    nl->add( vec3 );

	    // glTexCoord2f(s,t-dt);
	    sgSetVec2( vec2, s, t-dt );
	    tl->add( vec2 );
	    s += ds;

	    // glVertex3f( x*radius, y*radius, z*radius );
	    sgSetVec3( vec3, (float)(x*radius), float(y*radius), float(z*radius) );
	    vl->add( vec3 );
	}

	ssgLeaf *slice = 
	    new ssgVtxTable ( GL_TRIANGLE_STRIP, vl, nl, tl, cl );

	if ( vl->getNum() != nl->getNum() ) 
	{
	    //cout << "bad sphere1\n";
	    //exit(-1);
		return 0;
	}
	if ( vl->getNum() != tl->getNum() ) 
	{
	    //cout << "bad sphere2\n";
	    //exit(-1);
		return 0;
	}
	slice->setState( state );
	slice->setCallback( SSG_CALLBACK_PREDRAW, predraw );
	slice->setCallback( SSG_CALLBACK_POSTDRAW, postdraw );

	sphere->addKid( slice );

	t -= dt;
    }

    return sphere;
}


// Constructor
vsbSun::vsbSun( void ) {
}


// Destructor
vsbSun::~vsbSun( void ) {
}



// initialize the sun object and connect it into our scene graph root
ssgBranch * vsbSun::Build(double sun_size ) 
{
/*	char texture_path[512];
	strcpy(texture_path,GetSysPath());
	strcat(texture_path,"/Sky/halo.rgba");
*/
	string texture_path = vsbGetSysPath()+"/Sky/halo.rgba";

    // set up the orb state
    mpOrbState = new ssgSimpleState();
    mpOrbState->setShadeModel( GL_SMOOTH );
    mpOrbState->disable( GL_LIGHTING );
    mpOrbState->disable( GL_CULL_FACE );
    mpOrbState->disable( GL_TEXTURE_2D );
    mpOrbState->enable( GL_COLOR_MATERIAL );
    mpOrbState->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    mpOrbState->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    mpOrbState->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    mpOrbState->disable( GL_BLEND );
    mpOrbState->disable( GL_ALPHA_TEST );

    mpColourArr = new ssgColourArray( 1 );
    sgVec4 color;
    sgSetVec4( color, 1.0, 1.0, 1.0, 1.0 );
    mpColourArr->add( color );

    ssgBranch *orb = vsbMakeSphereObject( mpOrbState, mpColourArr, sun_size, 10, 10, 
				    NULL,NULL);

    // force a Repaint of the sun colors with safe arbitrary defaults
    Repaint( 45, 0, 200000 );

    // set up the halo state

    mpHaloState = new ssgSimpleState();
    mpHaloState->setTexture( const_cast<char *>(texture_path.c_str()) );
    mpHaloState->enable( GL_TEXTURE_2D );
    mpHaloState->disable( GL_LIGHTING );
    mpHaloState->setShadeModel( GL_SMOOTH );
    mpHaloState->disable( GL_CULL_FACE );
    mpHaloState->enable( GL_COLOR_MATERIAL );
    mpHaloState->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    mpHaloState->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    mpHaloState->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    mpHaloState->enable( GL_ALPHA_TEST );
    mpHaloState->setAlphaClamp(0.01f);
    mpHaloState->enable ( GL_BLEND ) ;

    // Build ssg structure
    double size = sun_size * 10.0;
    sgVec3 v3;

	mpHaloVertArr = new ssgVertexArray;
    sgSetVec3( v3, -size, 0.0f, -size );
    mpHaloVertArr->add( v3 );
    sgSetVec3( v3, size, 0.0f, -size );
    mpHaloVertArr->add( v3 );
    sgSetVec3( v3, -size, 0.0f , size);
    mpHaloVertArr->add( v3 );
    sgSetVec3( v3, size, 0.0f,  size);
    mpHaloVertArr->add( v3 );

    sgVec2 v2;
    mpHaloTextureArr = new ssgTexCoordArray;
    sgSetVec2( v2, 0.0f, 0.0f );
    mpHaloTextureArr->add( v2 );
    sgSetVec2( v2, 1.0, 0.0 );
    mpHaloTextureArr->add( v2 );
    sgSetVec2( v2, 0.0, 1.0 );
    mpHaloTextureArr->add( v2 );
    sgSetVec2( v2, 1.0, 1.0 );
    mpHaloTextureArr->add( v2 );

    ssgLeaf *halo = 
	new ssgVtxTable ( GL_TRIANGLE_STRIP, mpHaloVertArr, NULL, mpHaloTextureArr, mpColourArr );
    halo->setState( mpHaloState );

    // Build the ssg scene graph sub tree for the sky and connected
    // into the provide scene graph branch
    mpSunTransform = new ssgTransform;

//    halo->setCallback( SSG_CALLBACK_PREDRAW, sgSunHaloPreDraw );
 //   halo->setCallback( SSG_CALLBACK_POSTDRAW, sgSunHaloPostDraw );

    mpSunTransform->addKid( halo );
    mpSunTransform->addKid( orb );

	//	string texture_path = vsbGetSysPath()+"/Sky/halo.rgba";
	//  mpHaloState->setTexture( const_cast<char *>(texture_path.c_str()) );

    return mpSunTransform;
}


// Repaint the sun colors based on current value of sun_angle in
// degrees relative to verticle
// 90 degrees = high noon
// 0/180 degrees = sun rise/set
// 270 degrees = darkest midnight

bool vsbSun::Repaint(double sunAngle , double sunNorthDeclination, double sunDist ) // deg
{

	double sun_angle = fabs(sunAngle - 90.0f);
	
    if ( sun_angle  < 100 ) 
	{
		// else sun is well below horizon (so no point in repainting it)
		
		sun_angle *= SGD_DEGREES_TO_RADIANS;
		double x_10 = sun_angle * sun_angle * sun_angle * sun_angle * sun_angle
			* sun_angle * sun_angle * sun_angle * sun_angle * sun_angle;
		
		float ambient = (float)(0.4 * pow (1.1, - x_10 / 30.0));
		if (ambient < 0.3) { ambient = 0.3f; }
		if (ambient > 1.0) { ambient = 1.0f; }
		
		sgVec4 color;
		sgSetVec4( color,
			(ambient * 6.0)  - 1.0, // minimum value = 0.8
			(ambient * 11.0) - 3.0, // minimum value = 0.3
			(ambient * 12.0) - 3.6, // minimum value = 0.0
			1.0 );
		
		if (color[0] > 1.0) color[0] = 1.0;
		if (color[1] > 1.0) color[1] = 1.0;
		if (color[2] > 1.0) color[2] = 1.0;
		
		
		float *ptr;
		ptr = mpColourArr->get( 0 );
		sgCopyVec4( ptr, color );
    }
	

	//temporary matrices

	sgMat4 T, GST, DEC;
	sgVec3 axis;
	sgVec3 vec;

	// transformation matrices applied in the specified order

	/*

    sgSetVec3( vec, sunDist, 0.0, 0.0 );
    sgMakeTransMat4( T, vec );

    sgSetVec3( axis, 0.0, -1.0, 0.0 );
    sgMakeRotMat4( GST, sunAngle, axis );	
	
	sgSetVec3( axis, -1.0, 0.0, 0.0 );
    sgMakeRotMat4( DEC, sunNorthDeclination, axis );

	*/

	sgSetVec3( vec, 0.0, -sunDist, 0.0 );
    sgMakeTransMat4( T, vec );

    sgSetVec3( axis, -1.0, 0.0, 0.0 );
    sgMakeRotMat4( GST, sunAngle, axis );	
	
	sgSetVec3( axis, 0, -1.0, 0.0 );
    sgMakeRotMat4( DEC, sunNorthDeclination, axis );

	sgCopyMat4( mSunXForm, DEC );
	sgPreMultMat4( mSunXForm, GST );
	sgPreMultMat4( mSunXForm, T);
 	

    return true;
}


// Reposition the sun at the specified right ascension and
// declination, offset by our current position (p) so that it appears
// fixed at a great distance from the viewer.  Also add in an optional
// rotation (i.e. for the current time of day.)

bool vsbSun::Reposition( const sgVec3 campos )
{
    sgMat4 cam_xfm;
   
	//	campos[2] = 0; ///=3;

    sgMakeTransMat4( cam_xfm, campos[0], campos[1], 0 );
    sgPreMultMat4( cam_xfm, mSunXForm);

    sgSetCoord( &mSunPos, cam_xfm );
    mpSunTransform->setTransform( &mSunPos );

//	string texture_path = vsbGetSysPath()+"/Sky/halo.rgba";
//	mpHaloState->setTexture( const_cast<char *>(texture_path.c_str()) );


    return true;
}
