#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <math.h>
#include <assert.h>


#include "ssg.h"
#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

#include "vsbParserHelper.h"
#include "vsbUtilities.h"
#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"

#include "vsbSmokeVtxTable.h"
#include "vsbSmokeEmitter.h"


#include "vsbBaseViewEntity.h"
#include "assert.h"


using namespace std;
using namespace Parser;

const char RELATIVE_SMOKE_DIRECTORY[] = "/effects/smoke/";

#define MAX(A,B) (((A)>(B))?A:B)
#define MIN(A,B) (((A)<(B))?A:B)

string vsbMakeSmokeFXPath(string fname)
{
    string work_str = vsbGetSysPath()+ RELATIVE_SMOKE_DIRECTORY;
    vsbRemoveEnclosingSlashes(fname);
    work_str += fname;
    return work_str;
}

/////////////////////////////////////////////////////////////////////////////////
//
//  vsbSmokeEmitter
//
/////////////////////////////////////////////////////////////////////////////////
int vsbSmokeEmitter::msCounter = 0;

vsbSmokeEmitter::vsbSmokeEmitter()
{
    mEmitterDef = 0;
    mStartTime =  mCurrUpdateTime = 0;
	
	mEndTime = 1e300; //effectively infinite
	mLastUpdateTime = mEndTime;
	mDeleteFlags = 0;
	DetachFromEntity();
	mFirstEmission = true;
	mModelAttachmentName = mEntityName = "";
	mNameChangeHandler = 0;
	mId = ++msCounter;
    
}

vsbSmokeEmitter::vsbSmokeEmitter(vsbSmokeEmitterDefinition *def)
{
    mEmitterDef = def;
    mStartTime = mCurrUpdateTime = 0;
	mEndTime = 1e300; //effectively infinite
	mLastUpdateTime = mEndTime;
	mDeleteFlags = 0;
	DetachFromEntity();
	mFirstEmission = true;
	mModelAttachmentName = mEntityName = "";
	mNameChangeHandler = 0;
	mId = ++msCounter;
}




char const * vsbSmokeEmitter::SmokeEmitterClassName()
{
    static char *class_name = "vsbSmokeEmitter";
    return class_name;
}

vsbBaseViewEntity *vsbSmokeEmitter::AttachedEntity()
{
	if (mEntityHandle >= 0)
		return vsbViewportContext::EntityManager().LookupEntity(mEntityHandle);
	else
		return 0;

}	

void vsbSmokeEmitter::UpdateEmitter(double time, bool suppress_emission)
{
	
	if (!mActive) return;

	//+++ Get current pos/orient if have one
	// mCurrUpdatePos, mCurrUpdateTime

	mCurrUpdateTime = time;

	if (time < mLastUpdateTime) 
	{
		vsbViewportContext::SmokeFXDirector().StopActivePuffs();
		mLastUpdateTime = time;

		mFirstEmission = true;
	}

	assert(time >= mLastUpdateTime); // later maybe do an emitter reset insted of assert?

	if (mEntityHandle >= 0)
	{
		vsbBaseViewEntity *entity = vsbViewportContext::EntityManager().LookupEntity(mEntityHandle);
		if (entity) 
		{
			// Later take into consideration model, and attachment points
	
			if (mEntityName != entity->Name())
			{
				mEntityName = entity->Name();
				if (mNameChangeHandler!= 0)
					(*mNameChangeHandler)(this);

			}

			if (!entity->IsVisible() || !entity->GetAttachmentPointPos(mCurrUpdatePos, mModelAttachmentIndex))
			{	
				mFirstEmission = true;
				return;
			}
				
		}
	} 
	/*
	else
	{
		vsbBaseViewEntity *entity = vsbViewportContext::EntityManager().LookupEntity(mEntityName);
		if (entity)
		{
			mEntityHandle = entity->Handle();		
			if (!entity->IsVisible() || !entity->GetAttachmentPointPos(mCurrUpdatePos, mModelAttachmentIndex))
			{	
				mFirstEmission = true;
				return;
			}
		}
	}*/


	if (mFirstEmission)
	{
		mFirstEmission = false;
		sgdCopyVec3(mLastUpdatePos, mCurrUpdatePos);
		
		mLastUpdateTime = mCurrUpdateTime;

	} else
	{
		if (!suppress_emission) 
			EmittParticles();
		
		sgdCopyVec3(mLastUpdatePos, mCurrUpdatePos);

		mLastUpdateTime = mCurrUpdateTime;
	}

}


void vsbSmokeEmitter::EmittParticles()
{
	
	assert(mEmitterDef);
	double rate = 1.0/mEmitterDef->EmissionRate();

	double start_t = MAX(mStartTime, mLastUpdateTime);

	double start_segment_time = start_t-fmod(start_t, rate);
	double end_segment_time  = MIN(mCurrUpdateTime,mEndTime);
	

	double delta_time = mCurrUpdateTime - mLastUpdateTime;

	if (!delta_time) 
		return;
	
	sgdVec3 delta_pos;
	sgdCopyVec3(delta_pos,mCurrUpdatePos);
	sgdSubVec3(delta_pos,mLastUpdatePos);
	
	int i = 0;

	for (double t = start_segment_time; t < end_segment_time;i++, t+=rate)
	{
		if (t < mStartTime || t < mLastUpdateTime) continue;

		// calculate position of particle 

		sgdVec3 particle_pos; 
		double frac = (t - mLastUpdateTime)/delta_time;
		sgdScaleVec3(particle_pos,delta_pos,frac);
		sgdAddVec3(particle_pos,mLastUpdatePos);


		vsbSmokeVtxTable *new_puff = vsbViewportContext::SmokeFXDirector().NewSmokePuff();
		InitParticle(new_puff, t, particle_pos);

	}

	i=0;
	
}

void vsbSmokeEmitter::Regenerate(double time)
{
    //@@@@ to be done
}

void vsbSmokeEmitter::Init(double time, vsbSmokeEmitterDefinition *emitter_def )
{
    mStartTime = mLastUpdateTime = mCurrUpdateTime = time;
    if (emitter_def)
        EmitterDef(emitter_def);
	DetachFromEntity();
    
}

void  vsbSmokeEmitter::InitParticle(vsbSmokeVtxTable *smokeVtxTable, float currtime, sgdVec3 pos)
{
	assert(mEmitterDef != 0);
	mEmitterDef->InitParticle(smokeVtxTable,currtime, pos);
}


void	vsbSmokeEmitter::AttachToEntity(vsbBaseViewEntity *entityPtr, int attachmentIndex)
{
	if (!entityPtr) 
		DetachFromEntity();
	else
	{
		mEntityHandle = entityPtr->Handle();
		mModelAttachmentIndex = attachmentIndex;

		if (entityPtr)
		{
			mEntityName = entityPtr->Name();
			if (entityPtr->AttachmentPointExists(attachmentIndex))
				entityPtr->AttachmentPointName(attachmentIndex,mModelAttachmentName);
		}
		else
		{
			mModelAttachmentName = mEntityName = "";
		}
	}

}

void vsbSmokeEmitter::AttachToEntity(vsbBaseViewEntity *entityPtr, const std::string &attachment_name)
{
	
	AttachToEntity(entityPtr,vsbEntityAttachmentPoint::MakeId(attachment_name));
	mEntityName = entityPtr->Name();
	mModelAttachmentName = attachment_name;
}

void vsbSmokeEmitter::AttachToEntity(int handle, const std::string &attachment_name)
{

	mModelAttachmentIndex = vsbEntityAttachmentPoint::MakeId(attachment_name);
	AttachToEntity(handle, mModelAttachmentIndex);
	mModelAttachmentName = attachment_name;
}

void vsbSmokeEmitter::AttachToEntity(const std::string &entity_name, const std::string &attachment_name)
{
	mEntityName = entity_name;
	mModelAttachmentName = attachment_name;

	mModelAttachmentIndex = vsbEntityAttachmentPoint::MakeId(attachment_name);
	mEntityHandle = vsbViewportContext::EntityManager().GenerateHandle(entity_name);

}


void	vsbSmokeEmitter::AttachToEntity(int handle, int attachmentIndex)
{
	mEntityHandle = handle;
	mModelAttachmentIndex = attachmentIndex;

	vsbBaseViewEntity *entity =  vsbViewportContext::EntityManager().LookupEntity(handle) ;

	if (entity)
	{
		mEntityName = entity->Name();
		if (entity->AttachmentPointExists(attachmentIndex))
			entity->AttachmentPointName(attachmentIndex,mModelAttachmentName);
	}
	else
	{
		mModelAttachmentName = mEntityName = "";
	}

}




void	vsbSmokeEmitter::DetachFromEntity()
{
	mEntityHandle = -1;
	mModelAttachmentIndex = 0;
}

void  vsbSmokeEmitter::DeleteOn(unsigned int delflags, bool reset)
{
	if (reset) mDeleteFlags = delflags;
	else mDeleteFlags |= delflags;
}

bool	vsbSmokeEmitter::CanDelete()
{

	if (mDeleteFlags & EMITTER_FORCE)
		return true;
	if (mDeleteFlags & EMITTER_ACTIVE && ! mActive)
		return true;

	double t = vsbViewportContext::EntityManager().CurrTime();


	if (mDeleteFlags & EMITTER_SCOPE && mActive)
		if (t < mStartTime || t > mEndTime)
			return true;

	if (mDeleteFlags & (EMITTER_ENTITY | EMITTER_ENTITY_VISIBILITY))
	{
		vsbBaseViewEntity *entity;     
		entity = (mEntityHandle<0)? 0 : vsbViewportContext::EntityManager().LookupEntity(mEntityHandle);
		if (!entity) return true;

		if (mDeleteFlags &  EMITTER_ENTITY_VISIBILITY)
			if (!entity->IsVisible())
				return true;
	}

	return false;

}


bool	vsbSmokeEmitter::SetEmitterType(const std::string &def_name)
{
	std::map<std::string, vsbSmokeEmitterDefinition>::iterator iter;
	std::map<std::string, vsbSmokeEmitterDefinition> &emitter_defs = 
	   vsbViewportContext::SmokeFXDirector().SmokeEmitterDefinitions();
	iter = emitter_defs.find(def_name);
	if (iter != emitter_defs.end())
	{
		EmitterDef(&(iter->second));
		return true;
	} else
		return false;
}
//////////////////////////////////////////////////////////////////////////////
vsbSmokeEmitterDefinition::vsbSmokeEmitterDefinition()
{
    mLifeSpan  = 0;
    mStartSize = 0;
	mDeltaSize = 0;
	mEmissionRate = 0;
	
    mPreload = true;
    mLoaded = false;
	mDecayMode = DECAY_LINEAR;
    
    for (int i=0; i<4; i++)
    {
        mLowerVelBound[i] = mUpperVelBound[i] = 0;
        mRandomizeVel[i] = false;
		mLowerPosBound[i] = mUpperPosBound[i] = 0;
        mRandomizePos[i] = false;
    }
    
    sgdSetVec3(mUnitDirectionVector,0,0,0); 
}

vsbSmokeEmitterDefinition::vsbSmokeEmitterDefinition(float lifespan, float startsize, float deltasize, 
													 const std::string &textureFilename, bool preload)

{
	mLifeSpan  = lifespan;
    mStartSize = startsize;
	mDeltaSize = deltasize;
    mPreload = preload;
    mTextureFilename = textureFilename;
  
}


void vsbSmokeEmitterDefinition::SetVelPerturbation(int idx, double lower_bound, double upper_bound, 
                                         bool randomized)
{
    assert(idx>=0 && idx <3);
    mLowerVelBound[idx] = lower_bound; 
    mUpperVelBound[idx] = upper_bound;
    mRandomizeVel[idx] = randomized;   
}

double  vsbSmokeEmitterDefinition::GetVelPerturbation(int idx)
{
    assert(idx>=0 && idx <3);
    if (!mRandomizeVel[idx])
        return mLowerVelBound[idx];
    else
    {
        double range = mUpperVelBound[idx] - mLowerVelBound[idx];
        return mLowerVelBound[idx] + range*(rand()/double(RAND_MAX));
    }
}

void vsbSmokeEmitterDefinition::SetPosPerturbation(int idx, double lower_bound, double upper_bound, 
													   bool randomized)
{
    assert(idx>=0 && idx <3);
    mLowerPosBound[idx] = lower_bound; 
    mUpperPosBound[idx] = upper_bound;
    mRandomizePos[idx] = randomized;   
}

double  vsbSmokeEmitterDefinition::GetPosPerturbation(int idx)
{
	//return 0;
	//@@@
    assert(idx>=0 && idx <3);
    if (!mRandomizePos[idx])
        return mLowerPosBound[idx];
    else
    {
        double range = mUpperPosBound[idx] - mLowerPosBound[idx];
        return mLowerPosBound[idx] + range*(rand()/double(RAND_MAX));
    }
}




void    vsbSmokeEmitterDefinition::GetVelVector(sgdVec3 vel_vec)
{ 
    sgdSetVec3(vel_vec, GetVelPerturbation(0),GetVelPerturbation(1),GetVelPerturbation(2));
    sgdCopyVec3(mUnitDirectionVector,vel_vec);
    sgdNormaliseVec3(mUnitDirectionVector);
}

void    vsbSmokeEmitterDefinition::GetPosVector(sgdVec3 pos_vec)
{ 
    sgdSetVec3(pos_vec, GetPosPerturbation(0),GetPosPerturbation(1),GetPosPerturbation(2));
}


vsbSmokeEmitterDefinition::vsbSmokeEmitterDefinition(const vsbSmokeEmitterDefinition &src)
{
    *this = src;
}

vsbSmokeEmitterDefinition &vsbSmokeEmitterDefinition::
 operator = (const vsbSmokeEmitterDefinition &src)
{
	mLifeSpan  = src.mLifeSpan;
    mStartSize = src.mStartSize;
	mDeltaSize = src.mDeltaSize;
	mEmissionRate = src.mEmissionRate;
    mPreload = src.mPreload;
    mTextureFilename = src.mTextureFilename;
	mName = src.mName;
	mDecayMode = src.mDecayMode;
    mLoaded = src.mLoaded;
    sgdCopyVec3(mUnitDirectionVector,src.mUnitDirectionVector);
    sgdCopyVec3(mLowerVelBound,src.mLowerVelBound);
    sgdCopyVec3(mUpperVelBound,src.mUpperVelBound);
	
	sgdCopyVec3(mLowerPosBound,src.mLowerPosBound);
    sgdCopyVec3(mUpperPosBound,src.mUpperPosBound);
    
    for(int i=0; i<3; i++)
        mRandomizeVel[i] = src.mRandomizeVel[i];
	for( i=0; i<3; i++)
        mRandomizePos[i] = src.mRandomizePos[i];
    
    return *this;
}

ssgSimpleState *vsbSmokeEmitterDefinition::MakeState(const char *emitterTextureFilename)
{
    
    string  texture_path = vsbMakeSmokeFXPath(emitterTextureFilename ? 
                                              emitterTextureFilename: "smoke.rgb");
   
    
    ssgSimpleState *state = new ssgSimpleState();
    state->setTexture( const_cast<char *>(texture_path.c_str()),TRUE,TRUE,FALSE );
    state->setShadeModel( GL_SMOOTH );
    state->disable( GL_LIGHTING );
    state->disable( GL_CULL_FACE );
    state->enable( GL_TEXTURE_2D );
    state->enable( GL_BLEND );
    //state->enable( GL_ALPHA_TEST );
    //state->setAlphaClamp( 0.0f );
	state->disable( GL_ALPHA_TEST );
    state->setTranslucent();
    
    
    state->disable(GL_COLOR_MATERIAL);


    
    return state;
}



void  vsbSmokeEmitterDefinition::InitParticle(vsbSmokeVtxTable *smokeVtxTable, float time, sgdVec3 pos)
{
	assert(smokeVtxTable != 0);

    vsbSmokeVtxTable *puff = smokeVtxTable;

	puff->mExpired = false;

	// Initialise position for smoke puff

    sgdVec3 pos_vec;
	GetPosVector(pos_vec);

	sgdAddVec3(pos_vec,pos);
	puff->SetPos(pos_vec);


	puff->vertices->setNum(1);

	
	
	// Initialise velocity for smoke puff
	GetVelVector(puff->mVel);
	
	
	puff->mMaxLife = mLifeSpan;
	puff->mStartTime = time;
	puff->mExpansionRate = mDeltaSize;
	puff->mDecayMode = mDecayMode;

	puff->mAmbientLit = mAmbientLit;
	sgCopyVec3(puff->mColor,mColor);
	
	puff->StartSize(mStartSize);

	// if texture already present, reference it rather than reloading a copy
	
	string texture_file = TextureFilename();
	ssgSimpleState *emitter_state = vsbViewportContext::StateManager().Find(texture_file);

	if (!emitter_state)
	{
		emitter_state = vsbSmokeEmitterDefinition::MakeState(texture_file.c_str());
		vsbViewportContext::StateManager().Insert(texture_file, emitter_state);
		Loaded(true);
		assert(emitter_state);
	} else 
	{
		Loaded(true);
	}
	
	puff->setState(emitter_state);

}



void  vsbSmokeEmitterDefinition::TextureFilename(const std::string &name)
{
	mLoaded = false;
	mTextureFilename = name;
}



















