////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbAttachmentPoint.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////


#ifndef _vsbAttachmentPoint_H
#define _vsbAttachmentPoint_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif


#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbAttachmentPoint
    \brief Contains information about attachment points on 3d models that can
	be used to attach smoke emitters or perhaps missiles
    


*/
////////////////////////////////////////////////////////////////////////////////

#define ATTACHPT_EVALUATED 1



class DLL_VSB_API vsbAttachmentPoint
{
friend  class DLL_VSB_API vsbEntityAttachmentPoint;
public:

	vsbAttachmentPoint();
	vsbAttachmentPoint(const std::string &name, sgdVec3 pt);
		
	virtual ~vsbAttachmentPoint();

	void				    Set(const std::string &name, sgdVec3 pt);

							//! Copy Constructor
	vsbAttachmentPoint &	operator =(const vsbAttachmentPoint &from);
	
	const std::string &		Name() const const {return mName;};


	int 					Id() const {return mId;}

							//! Reference to attachment point coords in vsb coordinates
	sgdVec3	&				Point() {return mPoint;}

							//! Convert an attachment point name to an ID value
	static int				MakeId(const std::string &name);

protected:
	std::string				mName;    //!< String name of attachment point
	int 					mId;	  //!< Id of attachment point (hash value)	
	sgdVec3					mPoint;   //!< Attachment point relative to model reference point

	
};


class DLL_VSB_API vsbEntityAttachmentPoint: public vsbAttachmentPoint
{
public:
	vsbEntityAttachmentPoint():vsbAttachmentPoint(){};
	vsbEntityAttachmentPoint(const std::string &name, sgdVec3 pt, bool evaluated = false):
	vsbAttachmentPoint(name, pt),mIsEvaluated(false){}
	;
	//! Copy Constructor
	
	void	  Set(const std::string &name, sgdVec3 pt, bool evaluated = false);
	vsbEntityAttachmentPoint &	operator =(const vsbAttachmentPoint &from);
	vsbEntityAttachmentPoint &	operator =(const vsbEntityAttachmentPoint &from);

	void IsEvaluated(bool truefalse) {mIsEvaluated = truefalse;}
	bool IsEvaluated() const {return mIsEvaluated;};

	const sgdVec3	&	TemplatePoint() const {return mTemplateAttachmentPoint->mPoint;}

	void Transform(sgdMat4 mat);
	void Transform(sgMat4 mat);
	
	
protected:
	const vsbAttachmentPoint *mTemplateAttachmentPoint;
	bool					  mIsEvaluated;   //!< Misclaneous flags, currently unused
};





#endif