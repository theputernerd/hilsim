//////////////////////////////////////////////////////////////////
/*! 
    \file vsbFollowCamera.h
    \brief Defines the follow camera class vsbFollowCamera
    
     The class vsbFollowCamera is derrived from vsbBaseCamera, 
     as well as vsbLookAtTransform, implementing  a versatile follow 
     camera object. 

	\sa vsbFollowCamera.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbFollowCamera_H
#define _vsbFollowCamera_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#ifndef _vsbBaseCamera_H
#include "vsbBaseCamera.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

#ifndef _vsbLookAtTransform_H
#include "vsbLookAtTransform.h"
#endif


class DLL_VSB_API vsbFollowCameraParser;

//////////////////////////////////////////////////////////////////
/*! 
	\class vsbFollowCamera
	\brief Advanced entity following and tracking camera. 
    
*/
/////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbFollowCamera: public vsbBaseCamera, public vsbLookAtTransform
{
friend class DLL_VSB_API vsbFollowCameraParser;
public:

    /*! \brief option flags for follow camera behaviour

    The options are bitmasks, and some options are combinations of the more primitive
    bitmasks enabling multiple behaviours.
    */
	enum EMode {MAINTAIN_HEADING		 =	0x0001, //!< Enable follow mode in heading
				MAINTAIN_PITCH			 =	0x0002, //!< Enable follow mode in pitch
				MAINTAIN_ROLL			 =	0x0004, //!< Enable follow mode in roll
				MAINTAIN_X				 =	0x0008, //!< Enable follow mode in X
				MAINTAIN_Y				 =	0x0010, //!< Enable follow mode in Y
				MAINTAIN_Z				 =	0x0020, //!< Enable follow mode in Z

				FOLLOW_DESIGNATED_ENTITY =	0x0040, //!< Enable entity following
 				TRACK_TARGET_ENTITY		 =	0x0080, //!< Enable target tracking
				FOLLOW_ENTITY_HUD		 =  0x0100,

				LAG_CAMERA_POSITION		 =	0x0200,	//!< Position the camera along the velocity vector.
					
				MAINTAIN_POSITION		 =  0x0038, //!< Enable follow mode in position
				MAINTAIN_ORIENTATION	 =  0x0007, //!< Enable follow mode in orientation 
				MAINTAIN_ATTITUDE		 =  0x003F  //!< Enable follow mode in orientation  and position
    };

    /*! \brief Constructor 

    Initialises the camera with its parent camera manager
    \param cm A pointer to a valid parent camera manager

    A name for the camera is automatically generated
    */
	vsbFollowCamera(vsbCameraManager *cm);

    /*! \brief Constructor

        Initialises the camera with its parent camera manager, and a name for the camera
        \param cm A pointer to a valid parent camera manager
        \param name A name for the camera
        \param postfix A flag to indicate whether the name should be postfixed by a 
        unique camera number
    */    
    vsbFollowCamera(vsbCameraManager *cm, const std::string &name, bool postfixed = false);

    //! Virtual Destructor 
	virtual ~vsbFollowCamera();

	virtual vsbBaseCamera *Clone();

    vsbFollowCamera  &operator=(const vsbFollowCamera  &from);

    virtual const std::string & ClassName() const; // { return mClassName; };
 

    /*! \brief Update camera matrix 
    
    This function generates the computed matrix, based on entity geometry
    as well as option flags, followed by the camera matrix based on the computed and 
    relative geometry matrix, for the specified time
    \param time Time stampe for the next frame update

    \sa vsbBaseCamera::UpdateCameraMatrix
    */
	void UpdateCameraMatrix(float time);
	
    /*! \brief Get the logical name of entity being followed

        The camera's position follows this entity     
    */
    const std::string &EntityToFollow() {return mEntityToFollow;};

    /*! \brief Select the entity to follow
    \param name The logical name of the object to follow. 

    The camera subsequently follows this object in position
    */
    void EntityToFollow(const std::string &name) {mEntityToFollow = name;};


    //! Set the mode flags to the specified mask
	void SetModeFlags(int mode_flags);

    //! Get the mode flags mask in its current setting
	int  GetModeFlags();

    //! Enable the specified mode flags
	void EnableMode(EMode track_element);

    //! Disable the specified mode flags
	void DisableMode(EMode track_elemen);

    //! Convenience function to enable attitude tracking flags
	void EnableAllAttitudeTracking();

    //! Convenience function to disable attitude tracking flags
	void DisableAllAttitudeTracking();

    //! Test if a specified mode is enabled
	bool IsModeEnabled(EMode track_elemen);

    //! Test to see if a HUD should is to be drawn
    bool IsHUDEnabled() {return IsModeEnabled(FOLLOW_ENTITY_HUD) && IsModeEnabled(FOLLOW_DESIGNATED_ENTITY);};

	int  Write(FILE *f, int indent);
	int	 Parse(Parser::TLineInp *tli);

	float FollowDelayTime() const {return mFollowDelayTime;};
	void FollowDelayTime(float t) {mFollowDelayTime = t;};


private:
	void Setup();
	unsigned int mModeFlags;  
	std::string mEntityToFollow;
	float mFollowDelayTime;

};



class DLL_VSB_API vsbFollowCameraParser: public Parser::TParseObjectBase
{
public:
	vsbFollowCameraParser(vsbFollowCamera &sc);
	virtual ~vsbFollowCameraParser();

	int	 Parse(Parser::TLineInp *tli, bool skipHeader = false);
	int  Write(FILE *f, int indent);

private:
    vsbParserHelper *mpOutput;
	bool mSkipHeader;

	vsbFollowCamera &mrFollowCamera;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
	void ParseOnOffState(bool &onOrOffVar, int id);
	void WriteOnOff(bool onOrOff);
};

#endif