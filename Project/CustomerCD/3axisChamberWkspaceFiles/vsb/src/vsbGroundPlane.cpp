//////////////////////////////////////////////////////////////////
/*! 
    \file vsbGroundPlane.cpp
    \brief Defines the class vsbGroundPlane
 
	This file implements a vsbGroundPlane class, which manages the
	the construction, manipulation and drawing of a ground plane. It builds 
	an ssgRoot node with all the apropriate children including textures, 
	and ground plane geometry. It further provides functions to allow the 
	positioning the ground plane according to the next frame's requirements.
	
	\sa vsbGroundPlane.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <stdio.h>
#include <iostream>
#include <string>
#include <map>

using namespace std;

#include "sg.h"
#include "ssg.h"

#include "vsbGroundPlane.h"
#include "vsbRenderOptions.h"
#include "vsbSysPath.h"
#include "vsbPlaneTextureList.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentOptions.h"







// Constructor
vsbGroundPlane::vsbGroundPlane( void ) 
{
	mpGroundPlaneRoot		= 0; // essential initialisation

    mpGroundPlaneXfm		= 0;

    mpColorArr				= 0; 
    mpVertexArr				= 0;
    mpTextureArr			= 0;
	
	vsbGroundPlaneParams::SetDefault();
}


// Destructor
vsbGroundPlane::~vsbGroundPlane( void ) 
{
	Destroy();
}


const string &vsbGroundPlane::GroundPlaneTextureFileName(int terrainTextureIdx)
{
	//return (*mPlaneTextureList)[terrainTextureIdx].Fname().c_str();
	return TextureList()[terrainTextureIdx].Fname();
	//return mspGroundPlaneTextureFiles[terrainTextureIdx];
}

bool vsbGroundPlane::operator == (const vsbGroundPlaneParams &cp) 
{
	return *(vsbGroundPlaneParams *)this == cp;
};

vsbGroundPlane &vsbGroundPlane::operator = (const vsbGroundPlaneParams &gp) 
{
	vsbGroundPlaneParams &this_gp = *(vsbGroundPlaneParams *)this;

	*(vsbGroundPlaneParams *)this = gp;

	if (this_gp.NeedRebuild())
		Destroy();

	if (!IsBuilt()) 
		Build();
	else if (this_gp.IsModified())
		UpdateGeometry();

	this_gp.ClearModified();
	const_cast<vsbGroundPlaneParams &>(gp).ClearModified();
	return *this;
};


void vsbGroundPlane::Destroy()
{
	if (!IsBuilt()) return ;

	// all other ssgBase derrived dynamic members are deleted automatically, 
	// by the reference counting mechanism.. except for state which
	// for state which is assumed to be managed externally
	// see note about reference counting above.

	if (mpGroundPlaneRoot)
    {
		ssgDeRefDelete(mpGroundPlaneRoot);
		mpGroundPlaneRoot = 0;
	}

	// Clean up any terrain states that are no longer required

	if (msGroundPlaneState[GroundTextureIdx()]!=0)
	{
        int refcount = msGroundPlaneState[GroundTextureIdx()]->getRef();

		// this was the last terrain to reference this state so lets
		// remove it from the terrain state list to save on texture
		// memory.
	   ssgDeRefDelete(msGroundPlaneState[GroundTextureIdx()]);

        if  (refcount == 1)
            msGroundPlaneState[GroundTextureIdx()] = 0;
	}
}

void vsbGroundPlane::Build()
{
	// Ground plane vertex and texture 
	// arrays

	if (IsBuilt()) Destroy();


	if (msGroundPlaneState[GroundTextureIdx()] == 0)
		msGroundPlaneState[GroundTextureIdx()] = MakeGroundPlaneState(GroundTextureIdx());

    msGroundPlaneState[GroundTextureIdx()]->ref(); // just so that it doesnt aotomatically
										           // get deleted with the last terrain root	

	// tile size is the size in actual world units, of the
	// texture tile that is repeated

	//float h_scale = Scale()*vsbViewportContext::EnvironmentOpts().HorizontalScale();
	float h_scale = Scale();
	float tile_size   = Length() * 2 / h_scale;

    // build the terrain layer

	mpColorArr		  = new ssgColourArray( 6 );
    mpVertexArr		  = new ssgVertexArray( 6 );
    mpTextureArr      = new ssgTexCoordArray( 6 );
    
	// Default terrain layer colour

	sgVec4 colour;
    sgSetVec4( colour, 1.0f, 1.0f, 1.0f, 1.0f );
 
	sgVec3 vertex; // temporary sky corner vertex
    sgVec2 base;   // texture base coordinate

    sgSetVec2( base, 0.0, 0.0 );

	sgSetVec2( mInitialTextureCoords[0], base[0] + tile_size/2, base[1] + tile_size/2);
	mpTextureArr->add( mInitialTextureCoords[0] );
	sgSetVec3( vertex, 0.0f, 0.0f, 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );


	sgCopyVec2( mInitialTextureCoords[1], base );
	mpTextureArr->add( mInitialTextureCoords[1] );
	sgSetVec3( vertex, -Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );

      
    sgSetVec2( mInitialTextureCoords[2], base[0] + tile_size, base[1] );
    mpTextureArr->add( mInitialTextureCoords[2] );
	sgSetVec3( vertex, Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );

    sgSetVec2( mInitialTextureCoords[3], base[0] + tile_size, base[1] + tile_size );
    mpTextureArr->add( mInitialTextureCoords[3] );
	sgSetVec3( vertex, Length(), Length(), 0.0f );
    mpVertexArr->add( vertex );
    mpColorArr->add( colour );   
	
    sgSetVec2( mInitialTextureCoords[4], base[0], base[1] + tile_size );
    mpTextureArr->add( mInitialTextureCoords[4] );
	sgSetVec3( vertex, -Length(), Length(), 0.0f );
    mpVertexArr->add( vertex );
    mpColorArr->add( colour );

	sgCopyVec2( mInitialTextureCoords[5], base );
	mpTextureArr->add( mInitialTextureCoords[5] );
	sgSetVec3( vertex, -Length(), -Length(), 0.0f );
    mpVertexArr->add( vertex );
	mpColorArr->add( colour );


	// create the tri-strip leaf node for the terrain square

    ssgLeaf *layerGeometry = new ssgVtxTable ( GL_TRIANGLE_FAN, mpVertexArr, 0, mpTextureArr, mpColorArr );

	// give it a state

    layerGeometry->setState( msGroundPlaneState[GroundTextureIdx()] );

    // force a repaint of the moon colors with arbitrary defaults
    Repaint( colour );

    mpGroundPlaneXfm = new ssgTransform;

    mpGroundPlaneXfm->addKid( layerGeometry );

    mpGroundPlaneRoot = new ssgRoot;
	mpGroundPlaneRoot->ref();
    mpGroundPlaneRoot->addKid( mpGroundPlaneXfm );

//	return mpGroundPlaneRoot;
}

// build the terrain object

void vsbGroundPlane::Build( double size, double asl,  int terrainTextureIdx )
{

	if (IsBuilt()) Destroy();

	GroundTextureIdx(terrainTextureIdx);

	// Override the terrain parameters

    Alt(asl);				// altitude of terrain layer
    Length(size);			// world size of terrain object

	Build();	
}

void vsbGroundPlane::UpdateGeometry()
{
	if (!IsBuilt()) 
	{
		Build();
	} else 
	{
		float *vertex, *textvert;
		sgVec2 base;   // texture base coordinate

		// tile size is the size in actual world units, of the
		// texture tile that is repeated

		float tile_size   = Length() * 2 / Scale();

		sgCopyVec2(base, mInitialTextureCoords[1]);


		//-----------------------------
		textvert = mpTextureArr->get(0);
		sgSetVec2( mInitialTextureCoords[0], base[0] + tile_size/2, base[1] + tile_size/2 );
		sgCopyVec2( textvert, mInitialTextureCoords[2]);
		//-----------------------------

		textvert = mpTextureArr->get(1);
		vertex = mpVertexArr->get(1);
		sgCopyVec2( textvert, mInitialTextureCoords[1]);
		sgSetVec3( vertex, -Length(), -Length(), 0.0f );
		//------------------------------

		textvert = mpTextureArr->get(2);
		vertex = mpVertexArr->get(2);
		sgSetVec2( mInitialTextureCoords[2], base[0] + tile_size, base[1] );
		sgCopyVec2( textvert, mInitialTextureCoords[2]);
		sgSetVec3( vertex, Length(), -Length(), 0.0f );
		
		textvert = mpTextureArr->get(3);
		vertex = mpVertexArr->get(3);
		sgSetVec2( mInitialTextureCoords[3], base[0] + tile_size, base[1] + tile_size );
		sgCopyVec2( textvert, mInitialTextureCoords[3]);
		sgSetVec3( vertex, Length(), Length(), 0.0f );

		textvert = mpTextureArr->get(4);
		vertex = mpVertexArr->get(4);
		sgSetVec2( mInitialTextureCoords[4], base[0], base[1] + tile_size );
		sgCopyVec2( textvert, mInitialTextureCoords[4]);
		sgSetVec3( vertex, -Length(), Length(), 0.0f );


		textvert = mpTextureArr->get(5);
		vertex = mpVertexArr->get(5);
		sgCopyVec2( textvert, mInitialTextureCoords[5]);
		sgSetVec3( vertex, -Length(), -Length(), 0.0f );
		//------------------------------
	}
}

///////////////////////////////////////////////////////////////
// Repaint
//
// repaint the terrain layer colors given the fog colour
//
bool vsbGroundPlane::Repaint( const sgVec3 fog_colour ) 
{
	// check to see this terrain layer is built
	if (!mpGroundPlaneRoot)
		return false;

    float *colour;

    for ( int i = 0; i < 4; ++i ) 
	{
		colour = mpColorArr->get( i );
		sgCopyVec4( colour, fog_colour );
    }

    return true;
}

///////////////////////////////////////////////////////////////
// Reposition
//
// reposition the terrain layer at the specified origin 
// which generally would be the camera coordinates for
// the given view. The result being that although the 
// sky is centred above the cameras world-up vector, it
// appears to be fixed relative to the ground

bool vsbGroundPlane::Reposition( const sgVec3 p )
{


	// check to see this terrain layer is built
	if (!mpGroundPlaneRoot)
		return false;


 	// check to see this terrain layer is built

    sgMat4 T1;

	// tile size is the size in actual world units, of the
	// texture tile that is repeated

	//float h_scale = Scale()*vsbViewportContext::EnvironmentOpts().HorizontalScale();
	double h_scale = Scale();

	double tile_size = Length() * 2 / h_scale;

	// force the origin of the terrain at the origin of the
	// camera, but at the terrain altitude and build the 
	// transformation matrix


    sgMakeTransMat4( T1, p );
	T1[3][2] = mAlt*vsbViewportContext::EnvironmentOpts().VerticalScale();

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		T1[3][2] +=vsbBaseCamera::CameraToOriginDelta()[2];
	}

	
	// set the position of the terrain from the transformation
	// matrix T1

	mpGroundPlaneXfm->setTransform(T1);
	
	// to give the appearance of motion, whilst keeping the
	//terrain layer centred about  the camera up vector, we 
	// offset the start position of the texture within each 
	// tile knowing that it will wrap and match up with 
	// adjacent and similarly wrapped tiles

	// calculate the fraction of the tile length to wrap
	// in each direction, given the current camera position

	double x_modulo, y_modulo;


	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		x_modulo =  fmod(-vsbBaseCamera::CameraToOriginDelta()[0],h_scale)/h_scale;
		y_modulo =  fmod(-vsbBaseCamera::CameraToOriginDelta()[1],h_scale)/h_scale;
	} else
	{
		x_modulo =  fmod(p[0],h_scale)/h_scale;
		y_modulo =  fmod(p[1],h_scale)/h_scale;
	}

	sgVec2 base;   // texture's base offset

	base[0] = mInitialTextureCoords[0][0] + x_modulo;
	base[1] = mInitialTextureCoords[0][1] + y_modulo;

    //texture base offset must be between 0.0f and 1.0f so 
	//wrap if necessary 

	while ( base[0] >= 1.0 ) { base[0] -= 1.0; }
	while ( base[0] < 0.0 ) { base[0] += 1.0; }

    // now re-calculate update texture coordinates

	float *texture_coord;

	texture_coord = mpTextureArr->get(0);
	sgSetVec2(texture_coord, base[0]+ tile_size/2, base[1]+ tile_size/2);

	texture_coord = mpTextureArr->get(1);
	sgSetVec2(texture_coord, base[0], base[1]);

	texture_coord = mpTextureArr->get( 2 );
	sgSetVec2( texture_coord, base[0] + tile_size, base[1] );

	texture_coord = mpTextureArr->get( 3 );
	sgSetVec2( texture_coord, base[0] + tile_size, base[1] + tile_size );

	texture_coord = mpTextureArr->get( 4 );
	sgSetVec2( texture_coord, base[0], base[1] + tile_size );

	texture_coord = mpTextureArr->get(5);
	sgSetVec2(texture_coord, base[0], base[1]);


    return true;
}


void vsbGroundPlane::Draw() {
	// check to see this terrain layer is built
	if (!mpGroundPlaneRoot)
		return;

	if (mDepthTest == vsbNO_TEST_NO_WRITE) 
        glDisable( GL_DEPTH_TEST );
    else  if (mDepthTest == vsbTEST_ONLY)
        glDepthMask(false);

    ssgCullAndDraw( mpGroundPlaneRoot );

	if (mDepthTest == vsbNO_TEST_NO_WRITE) 
        glEnable( GL_DEPTH_TEST );
    else  if (mDepthTest == vsbTEST_ONLY)
        glDepthMask(true);
}

ssgSimpleState *vsbGroundPlane::MakeGroundPlaneState(int terrainTextureIdx)
{
	
	string texture_path = vsbGetSysPath()+"/Terrain/"+GroundPlaneTextureFileName(terrainTextureIdx);

	ssgSimpleState *state = new ssgSimpleState();
    state->setTexture( const_cast<char *>(texture_path.c_str()) );
    state->setShadeModel( GL_SMOOTH );
    state->disable( GL_LIGHTING );
    state->disable( GL_CULL_FACE );
    state->enable( GL_TEXTURE_2D );
    state->enable( GL_COLOR_MATERIAL );
    state->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    state->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    state->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    state->enable( GL_BLEND );
    state->enable( GL_ALPHA_TEST );
    state->setAlphaClamp( 0.01f );
    return state;
}



