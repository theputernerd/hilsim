#ifndef _vsbHUDComponents_H
#define _vsbHUDComponents_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbHUDFramework_H
#include "vsbHUDFramework.h"
#endif



class vsbBaseCamera;

class DLL_VSB_API vsbHeadingHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
                        vsbHeadingHUD();
    const char * Name() const;     
    void  HookHeading(double *h){mpHeading = h;};
    void  SetRectangle(float x, float y, float w, float h);
    void  SetMarkings(float rangeDeg, float majorTickDeg);
  

protected:
    float       mX,mY,mW,mH;
    float       mAngleRange;
    float       mMajorTickDeg;
    float       mMinorTickDeg;
    double      *mpHeading;
    void        Draw();
};


class DLL_VSB_API vsbAltAirspeedHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
    vsbAltAirspeedHUD();
    const char * Name() const;     
    void  HookAltAirspeed(float *alt, float *speed){mpAltitude = alt; mpAirspeed = speed;};
    void  SetRectangle(float x, float y, float w, float h, float box_w);
   

protected:
    float       mX,mY,mW,mH,mBW;
    float      *mpAltitude;
    float      *mpAirspeed;
    void        Draw();
};

class DLL_VSB_API vsbAttitudeHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
    vsbAttitudeHUD();
    const char * Name() const;     
    void  HookAttitude(float *alpha, float *mach, float *g){mpAlpha = alpha; mpMach = mach; mpG = g;};
    void  SetPos(float x, float y) {mX = x; mY = y;};
    void  ResetPeakG() {mPeakG=0;};
   

protected:
    float       mX,mY;
    float      *mpAlpha;
    float      *mpMach;
    float      *mpG;
    float       mPeakG;

    void        Draw();
};


class DLL_VSB_API vsbPitchLadderHUD:public vsbHUDComponent
{
public:
    vsbPitchLadderHUD();

    const char * Name() const;     

    void  HookPitchLadder(double *pitchDeg, double *rollDeg, float *xCentre, float *yCentre, bool *watermark, bool * steepness);
    void  SetGeometryPos(float waterMarkX, float waterMarkY, float waterMarkW, float waterMarkH, float ladderWidth, float ladderHeight);
    void  SetMarkings(float rangeDeg, float tickDeg);
 

protected:
    bool       *mpWatermarkOn;  //!< If watermark on pitch ladder displays A/C Attitude rel to earth
    bool       *mLadderSteepnessOn;  //!< Displays anglled pitch ladder rungs at 0.5 times flight path angle
    double      *mpPitch;        //!< Pitch displayed by pitch ladder
    double      *mpRoll;         //!< Roll displayed by pitch ladder
    float      *mpXCentre;      //!< If watermark off, pitch ladder rotates about this X point
    float      *mpYCentre;      //!< If watermark off, pitch ladder rotates about this Y point
    float       mWaterMarkX;    //!< Watermark X position 
    float       mWaterMarkY;    //!< Watermark Y position 
    float       mWaterMarkW;    //!< Watermark Width 
    float       mWaterMarkH;    //!< Watermark Height
    float       mWidth;         //!< Pitch ladder width
    float       mHeight;        //!< Pitch ladder height
    float       mAngleRange;    //!< Pitch ladder visible range 
    float       mTickDeg;       //!< Pitch ladder graduations


    void        Draw();
private:
    void        DrawLadder();
	void        DrawLadderStep(float angle_rung_deg, float angle_deg, float val0, float val1);
};


class DLL_VSB_API vsbBankHUD:public vsbHUDComponent
{
public:
    vsbBankHUD();
    const char * Name() const;     
    void  HookBank(double *bankDeg){mpBank = bankDeg;};
    void  SetPos(float x, float y, float r, float tickLen) {mX = x; mY = y; mRadius = r; mTickLen = tickLen;};


protected:
    float       mX,mY;
    float       mRadius;
    float       mTickLen;
    double      *mpBank;
    void        Draw();
};


class DLL_VSB_API vsbVelocityVectorHUD:public vsbHUDComponent
{
public:
    vsbVelocityVectorHUD();
    const char * Name() const;     
    void  HookVelocityVector(	double *headingDeg, double *pitchDeg, double *rollDeg, 
								double *u, double *v, double *w );
//    void  SetPos(float x, float y, float r, float r_angle);

	void  Caged(bool onOff)					{ mCaged = onOff;};
	bool  Caged()							{ return mCaged;};
	void  SetIconRadius(float radius)		{ mIconRadius = radius;};

	void  GetVectorPos(float &x, float &y);


protected:
	float       mVX,mVY;
	float       mRadius;
	float		mIconRadius;

	double       *mpHeading;
	double		*mpPitch;
	double		*mpRoll;
	double       *mpU;
	double		*mpV;
	double		*mpW;

	bool		mCaged;

	void		DrawBody(float x, float y);
	void		DrawFins(float x, float y);

	void		DrawIcon();
    void        Draw();
};

class DLL_VSB_API vsbLarsHUD:public vsbHUDComponent
{
	// Remove this friend class looseness and put const accessors to range, etc...
	friend class vsbHMSHUD;	
	friend class vsbTargetLocatorHUD;

public:

	enum eLarsMode { eASRAAM, eAMRAAM }; // The two modes for the lars HUD require different outputs.


    vsbLarsHUD();
    const char * Name() const; 
	void  SetPos(float x, float y, float r);
	void HookLars( const eLarsMode *mode, const float *rMin, const float *rMax, const float *rNoEscape, const float *range );
	void HookSD( const float *SDx, const float *SDy );

protected:
	float       mX,mY;
    float       mRadius;

	const eLarsMode *m_pMode;
	const float *m_pMax;
	const float *m_pMin;
	const float *m_pNoEscape;
	const float *m_pRange;

	// Steering Dot vars
	const float *m_SDx;
	const float *m_SDy;

	void		DrawOuterMarker(float angleDeg, bool diamond=false);
	void		DrawRangeBar(float angleDeg);
	void        Draw();
};

///////////////////////////////////////////////////////////////////////////////
//
// Aspect Vector, HUD component
//

class DLL_VSB_API vsbAspectVectorHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
	vsbAspectVectorHUD();
	const char * Name() const;

	void SetLength( const float length ) { m_length = length; }
	void SetRadius( const float rad ) { m_radius = rad; }

	void HookAspectVector( const float *aspect, const float *speed )
	{ m_aspect = aspect; m_speed = speed; }

protected:
	const float *m_aspect;
	const float *m_speed;
	
	float m_length;
	float m_radius;

	void        Draw();

};


///////////////////////////////////////////////////////////////////////////////
//
// Selected Weapon, HUD component
//

class DLL_VSB_API vsbSelectedWeaponHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
	vsbSelectedWeaponHUD();
	const char * Name() const;

	void SetSelectedWeapon( const char *name ) { strcpy( m_selectedWeapon, name ); }

protected:
	char m_selectedWeapon[25];

	void        Draw();

};


///////////////////////////////////////////////////////////////////////////////
//
// Target locator line, HUD component
//

class DLL_VSB_API vsbTargetLocatorHUD:public vsbHUDComponent
{
	friend class vsbHMSHUD;

public:
	vsbTargetLocatorHUD();
	const char * Name() const;

	void SetCam( const vsbBaseCamera *cam ) { m_pCam = cam; }
	void HookLars( vsbLarsHUD *pLars ) { m_pLarsHUD = pLars; }
	void HookWithinRadar( const float *pWithinRadar ) { m_bWithinRadar = pWithinRadar; }

	void HookPos( const sgdVec3 *pos ) {	m_pos = pos; }
	void HookTargetPos( const sgdVec3 *targetPos ) { m_targetPos = targetPos; }

protected:

	vsbLarsHUD *m_pLarsHUD;
	const vsbBaseCamera *m_pCam;

	const sgdVec3 *m_pos;
	const sgdVec3 *m_targetPos;
	const float *m_bWithinRadar;

	void        Draw();

};



///////////////////////////////////////////////////////////////////////////////
//
// Helmet Mounted Sight, HUD component
//
class DLL_VSB_API vsbHMSHUD:public vsbHUDComponent
{
public:
    vsbHMSHUD();
    const char * Name() const;     
	
	void SetCam( const vsbBaseCamera *cam ) { m_pCam = cam; }

	void HookLars( vsbLarsHUD *pLars ) { m_pLarsHUD = pLars; }
	void HookAspectVector( vsbAspectVectorHUD *pAspectVectorHUD ) { m_pAspectVectorHUD = pAspectVectorHUD; }
	void HookHeading( vsbHeadingHUD *pHeadingHUD ) { m_pHeadingHUD = pHeadingHUD; }
	void HookAltAirSpeed( vsbAltAirspeedHUD *pAltAirspeedHUD ) { m_pAltAirspeedHUD = pAltAirspeedHUD; }
	void HookAttitude( vsbAttitudeHUD *pAttitudeHUD ) { m_pAttitudeHUD = pAttitudeHUD; }
	void HookWeaponName( vsbSelectedWeaponHUD *pSelectedWeaponHUD ) { m_pSelectedWeaponHUD = pSelectedWeaponHUD; }
	void HookWithinRadar( const float *pWithinRadar ) { m_bWithinRadar = pWithinRadar; }
	void HookWithinSeeker( const float *pWithinSeeker ) { m_bWithinSeeker = pWithinSeeker; }

	void HookPos( const sgdVec3 *pos ) { m_pos = pos; }
	void HookTargetPos( const sgdVec3 *targetPos ) { m_targetPos = targetPos; }
	void HookHasTarget( const bool *bHasTarget ) { m_bHasTarget = bHasTarget; }

protected:

    void        Draw();

private:
	const vsbBaseCamera *m_pCam;

	// Hooked HUD's
	vsbLarsHUD *m_pLarsHUD;
	vsbAspectVectorHUD *m_pAspectVectorHUD;
	vsbHeadingHUD *m_pHeadingHUD;
	vsbAltAirspeedHUD *m_pAltAirspeedHUD;
	vsbAttitudeHUD *m_pAttitudeHUD;
	vsbSelectedWeaponHUD *m_pSelectedWeaponHUD;

	const bool *m_bHasTarget;

	const sgdVec3 *m_pos;
	const sgdVec3 *m_targetPos;

	const float *m_bWithinRadar;
	const float *m_bWithinSeeker;

	unsigned int m_canShoot;
	vsbFollowCamera *m_fc;
	float m_time;
	float m_lastTime;
	sgVec3 m_lastSeekerVec;

	float m_hmsRadius;


	void DrawAspectVector();
	void DrawAircraftRelativeCircle( float angle, float segmentSize, float gapSize );
	void DrawTargetBox();
	void DrawSeekerCircle();

};




#endif