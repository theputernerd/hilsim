#include <string>
#include <iostream>
#include <assert.h>

#ifdef WIN32
#include <windows.h>
#endif

#include<GL/gl.h>

using namespace std;

#include "vsbStrokedText.h"    
#include "vsbViewPortContext.h"

#define DOT_THICKNESS 0.3

int vsbStrokedText::msCharOffset[128];
int vsbStrokedText::msCharDefLength[128];
int vsbStrokedText::msCharWidth[128];

bool vsbStrokedText::msInitialised = false;
char *vsbStrokedText::curr_op;
unsigned int vsbStrokedText::msDListBase;

	

//Incomplete font... complete as required
char vsbStrokedText::msStrokeFont[][4] = 
{
    "C 4",
    "C!1","M08","L02","P00", 
    "C\"2","908","918",
    "C#4","M18","L11","M28","L21","M06","L36","M03","L33",
    "C$5",
    "C%5","M08","L18","L17","L07","08", "M31","L41","L42","L32","L31", "M01","L48",
    "C&5",
    "C'1","908",
    "C(2","M11","L03","L06","L18",
    "C)2","M01","L13","L16","L08",	
    "C*3","M05","L25","M22","L27","M07","L22","M01","L27",
    "C+3","M05","L25","M22","L27",
    "C,1","901",
    "C-4","M05","L35",
    "C.1",".01",
    "C/5","M01","L48",
    "C05","M07","L18","L38","L47","L42","L31","L11","L02","L07",
    "C15","M08","L28","L21","L01","L41",
    "C25","M06","L07","L18","L38","L47","L46","L03","L01", "L41","L42",
    "C35","M07","L18","L38","L47","L46","L35","L15","M35","L44","L42","L31","L11","L02",
    "C45","M31","L38","L28","L04","L44",
    "C55","M03","L02","L11","L31","L42","L44","L35","L05","L08","L48","L47",
    "C65","M46","L47","L38","L18","L07","L02","L11","L31","L42","L44","L35","L15","L04",
    "C75","M07","L08","L48","L01",
    "C85","M15","L06","L07","L18","L38","L47","L46","L35","L15","L04","L02","L11","L31","L42","L44","L35",
    "C95","M03","L02","L11","L31","L42","L47","L38","L18","L07","L05","L14","L34","L45",
    "C:1","P01","P06",
    "C;1","P01","P06","M01","L00",
    "C<5",
    "C=4",
    "C>5",
    "C?5",
    "C@5",
    "CA5","M01","L07","L18","L38","L47","L41","M05","L45", 
    "CB5","M01","L08","L38","L47","L46","L35","L05","M35","L44","L42","L31","L01",
    "CC5","M43","L42","L31","L11","L02","L07","L18","L38","L47","L46",
    "CD5","M01","L08","L38","L47","L42","L31","L01",
    "CE5","M41","L01","L08","L48","M05","L45",
    "CF5","M01","L08","L48","M05","L45",
    "CG5","M24","L25","L45","L42","L31","L11","L02","L07","L18","L38","L47",
    "CH5","M01","L08","M41","L48","M05","L45",
    "CI3","M11","L18","M01","L21","M08","L28",
    "CJ5","M04","L02","L11","L21","L32","L38","M28","L48",
    "CK5","M01","L08","M05","L48","M05","L41",
    "CL5","M08","L01","L41",
    "CM5","M01","L08","L25","L48","L41",
    "CN5","M01","L08","L41","L48",
    "CO5","M42","L31","L11","L02","L07","L18","L38","L47","L42",
    "CP5","M01","L08","L38","L47","L46","L35","L05",
    "CQ5","M42","L31","L11","L02","L07","L18","L38","L47","L42","M50","L32",
    "CR5","M01","L08","L38","L47","L46","L35","L05","M25","L41",
    "CS5","M03","L02","L11","L31","L42","L44","L35","L15","L06","L07","L18","L38","L47","L46", 
    "CT5","M08","L48","M21","L28",
    "CU5","M08","L02","L11","L31","L42","L48",
    "CV5","M08","L21","L48",
    "CW5","M08","L01","L25","L41","L48",
    "CX5","M08","L41","M48","L01",
    "CY5","M08","L24","L48","M24","L21",
    "CZ5","M08","L48","L01","L41",
    "C[5",
    "C\\5",
    "C]5",
    "C^5",
    "C_5",
    "C`1","608", 
    "Ca5","M44","L35","L15","L04","L02","L11","L31","L43","M45","L41",
    "Cb5","M02","L11","L31","L42","L44","L35","L15","L04","M08","L01",
	"Cc5","M44","L35","L15","L04","L02","L11","L31","L42",
    "Cd5","M44","L35","L15","L04","L02","L11","L31","L42","M48","L41",
    "Ce5","M44","L35","L15","L04","L02","L11","L31","L42","M44","L03",
    "Cf5","M47","L38","L28","L17","L11","M04","L24",
    "Cg5","M43","L32","L12","L03","L04","L15","L35","L44","L41","L30","L10","L01",
    "Ch5","M08","L01","M04","L15","L35","L44","L51",
    "Ci3","M05","L02","L11","L21","P07",
    "Cj4","M01","L10","L20","L31","L35","L25","P37",
    "Ck4","M01","L07","M03","L35","M03","L31",
    "Cl5","M18","L12","L21","L31","L42",
    "Cm7","M01","L05","M04","L15","L25","L34","L31","M34","L45","L55","L64","L61",
    "Cn5","M01","L05","M04","L15","L35","L44","L41",
    "Co5","M44","L35","L15","L04","L02","L11","L31","L42","L44",
    "Cp5","M44","L35","L15","L04","L03","L12","L32","L43","L44","M05","L00",
    "Cq5","M44","L35","L15","L04","L03","L12","L32","L43","L44","M44","L40",
    "Cr5","M11","L14","L05","M14","L25","L35","L44",
    "Cs5","M02","L11","L31","L42","L43","L04","L15","L35","L44",
    "Ct3","M18","L12","L21","L31","M05","L35",
    "Cu5","M05","L02","L11","L31","L43","M45","L41",
    "Cv5","M05","L21","L45",
    "Cw5","M05","L11","L25","L31","L45",
	"Cx5","M45","L01","M41","L05",
    "Cy5","M05","L03","L12","L32","L44","M45","L41","L30","L10","L01",
	"Cz5","M05","L45","L01","L41",
    "C{5",
    "C|5",
    "C}5",
    "C~5",/*alpha*/"M45","L21","L11","L02","L04","L15","L25","L34","L32","L41",
    "XXX"
};     



vsbStrokedText::vsbStrokedText()
{
    mScale = 1.0;
    mVertJustificaton=JUST_LOWER;
    mHorizJustificaton=JUST_LOWER;
	mHilight = false;
}

void vsbStrokedText::InitialiseFont()
{
	int i;
    if (!msInitialised)
    {
        msInitialised = true;
        

        for (i=0; i<128 ;i++)
        {
            msCharOffset[i] = 0;
            msCharDefLength[i] = 0;
            msCharWidth[i] = 0;
        }

        int offset = 0;
        for( i=32; i<128; i++)
        {
            while (msStrokeFont[offset][0]!= 'C' && msStrokeFont[offset][0]!= 'X') 
                offset++;

            if (msStrokeFont[offset][0]== 'X') break;

            // Check that all font characters are present and correct
            //cout << msStrokeFont[offset][1] << " - " << (char) i << endl;
            //assert (msStrokeFont[offset][1] == (char) i);

            //Set this characters offset
            msCharOffset[i] = offset;

            // Set this charactes width;
            msCharWidth[i] = msStrokeFont[offset][2]-'0';

            offset++;

            // Find characters definition length;
            int count = 0;
            while (msStrokeFont[offset+count][0]!= 'C' && msStrokeFont[offset+count][0]!= 'X')
                count++;
            msCharDefLength[i] = count;
        }
		BuildCharacters();

    }
	
}


void vsbStrokedText::BuildCharacters() 
{
	msDListBase = glGenLists(128);
	for (int i=32; i<128; i++)
		BuildChar(i);
}

void vsbStrokedText::BuildChar(char c)
{
	if (!msInitialised || msCharDefLength[c] == 0) return ;

	float x , y;
    bool line_begun = false;

	if (!glIsList(msDListBase + (unsigned int)c)) return; 
	glNewList(  msDListBase + (unsigned int)c,   GL_COMPILE);
 
	for (int i=0; i<msCharDefLength[c]; i++)
    {
        curr_op =  msStrokeFont[msCharOffset[c]+i+1];
        switch(curr_op[0])
		{
			case '6':
            if (line_begun)
            {
                line_begun = false;
                glEnd(); 
            }
            x = X();
            y = Y();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2-DOT_THICKNESS);
            glVertex2f(x+DOT_THICKNESS/2, y-DOT_THICKNESS/2-DOT_THICKNESS);
            glVertex2f(x+DOT_THICKNESS/2, y+DOT_THICKNESS/2-DOT_THICKNESS);
            glVertex2f(x-DOT_THICKNESS/2, y+DOT_THICKNESS/2-DOT_THICKNESS);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2-DOT_THICKNESS);
            glEnd();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x-DOT_THICKNESS/2, y+DOT_THICKNESS/2-DOT_THICKNESS);
            glVertex2f(x+DOT_THICKNESS, y+DOT_THICKNESS*2-DOT_THICKNESS);
            glEnd();
            break;
        case '9':
            if (line_begun)
            {
                line_begun = false;
                glEnd(); 
            }
            x = X();
            y = Y();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glVertex2f(x+DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glVertex2f(x+DOT_THICKNESS/2, y+DOT_THICKNESS/2);
            glVertex2f(x-DOT_THICKNESS/2, y+DOT_THICKNESS/2);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glEnd();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x+DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glVertex2f(x-DOT_THICKNESS, y-DOT_THICKNESS*2);
            glEnd();
            break;
  
        case 'P':
            if (line_begun)
            {
                line_begun = false;
                glEnd(); 
            }
            x = X();
            y = Y();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glVertex2f(x+DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glVertex2f(x+DOT_THICKNESS/2, y+DOT_THICKNESS/2);
            glVertex2f(x-DOT_THICKNESS/2, y+DOT_THICKNESS/2);
            glVertex2f(x-DOT_THICKNESS/2, y-DOT_THICKNESS/2);
            glEnd();
            //Fill in here
            break;

        case '.':
            if (line_begun)
            {
                line_begun = false;
                glEnd(); 
            }
            x = X();
            y = Y();
            glBegin(GL_LINE_STRIP);
            glVertex2f(x-DOT_THICKNESS/2, y);
            glVertex2f(x+DOT_THICKNESS/2, y);
            glVertex2f(x+DOT_THICKNESS/2, y+DOT_THICKNESS);
            glVertex2f(x-DOT_THICKNESS/2, y+DOT_THICKNESS);
            glVertex2f(x-DOT_THICKNESS/2, y);
            glEnd();
            //Fill in here
            break;

        //MOVE COMMAND
        case 'M':\
            if (line_begun)
            {
                line_begun = false;
                glEnd(); 
            }
            x = X();
            y = Y();
            break;

        //** LINE COMMAND
        case 'L':
            if (!line_begun)
            {
                line_begun = true;
                glBegin(GL_LINE_STRIP);
                glVertex2f(x, y);
            }
            x = X();
            y = Y();
            glVertex2f(x, y);
            break;
        }    
		
	}
	if (line_begun)
    {
        line_begun = false;
        glEnd(); 
    }
	glEndList();
}


static built = false;
void vsbStrokedText::DrawChar(char c, float &cx, float &cy)
{
	if (!msInitialised) return;
    float x = cx, y = cy;
    bool line_begun = false;
	glPushMatrix();
    glTranslatef(cx, cy, 0);
	glScalef(mScale, mScale, 0);
	glCallList(msDListBase+(unsigned int) c);
    glPopMatrix();
    cx+=(msCharWidth[c]+1)*mScale;
}

void vsbStrokedText::DrawRevChar(char c, float &cx, float &cy)
{
	if (!msInitialised) return;
    float x = cx, y = cy;
    bool line_begun = false;
	glPushMatrix();
    glTranslatef(cx, cy, 0);
	glScalef(-mScale, mScale, 0);
	glCallList(msDListBase+(unsigned int) c);
    glPopMatrix();
    cx-=(msCharWidth[c]+1)*mScale;
}

void vsbStrokedText::DrawStr(const char *s, float x, float y, bool reverse)
{
	if (!msInitialised) return;
    float w, h;
	if (Hilight())
	  glColor3fv((GLfloat *)vsbViewportContext::EnvironmentOpts().HudHilightColor());
	else
	  glColor3fv((GLfloat *)vsbViewportContext::EnvironmentOpts().HudNormalColor());	

	
    if (s)
    {
        if (!TextDimensions(s, w, h))
            return;

        if (reverse)
        {
            switch(mHorizJustificaton)
            {
            case JUST_MIDDLE: 
                x+= w/2;
                break;
            case JUST_LOWER:
                x+=w;
                break;
            case JUST_UPPER:
                break;
            }

            switch(mVertJustificaton)
            {
            case JUST_MIDDLE: 
                y+= h/2;
                break;
            case JUST_LOWER:
                y+=h;
                break;
            case JUST_UPPER:
                break;
            }

            while (*s) 
                DrawRevChar(*s++, x, y);
        }
        else
        {
            switch(mHorizJustificaton)
            {
            case JUST_MIDDLE: 
                x-= w/2;
                break;
            case JUST_LOWER:
                break;
            case JUST_UPPER:
                x-=w;
                break;
            }

            switch(mVertJustificaton)
            {
            case JUST_MIDDLE: 
                y-= h/2;
                break;
            case JUST_LOWER:
                break;
            case JUST_UPPER:
                y-=h;
                break;
            }
            while (*s) 
                DrawChar(*s++, x, y);
        }
    
    }
	glColor3fv((GLfloat *)vsbViewportContext::EnvironmentOpts().HudNormalColor());
}

int vsbStrokedText::TextDimensions(const char *s, float &w, float &h)
{
    if (!s || !msInitialised)
    { 
        w = h = 0;
        return 0;
    }

    w = 0;
    h = 9 * mScale;
    int charcount = 0;
    while (*s)
    {
        w += (msCharWidth[*s++]+1)*mScale;
        if (charcount++) w+= mScale; 
    }
    return charcount;
 
}


