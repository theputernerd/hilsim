//////////////////////////////////////////////////////////////////
/*! 
    \file vsbFollowCamera.cpp
    \brief Implements the follow camera class vsbFollowCamera
    
     The class vsbFollowCamera is derrived from vsbBaseCamera, 
     as well as vsbLookAtTransform, implementing  a versatile follow 
     camera object. 

	\sa vsbFollowCamera.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
#include <set>

#include "ssg.h"

#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"
#include "vsbFollowCamera.h"

#include "vsbEntityManager.h"
#include "vsbBaseViewEntity.h"
#include "vsbViewportContext.h"
#include "vsbModelRoot.h"

#include "parser_perror.h"

#include "vsbTrajEntity.h"

using namespace std;
using namespace Parser;

#define FOLLOW_DELAY 0.5

void vsbFollowCamera::Setup()
{
	FOV(60);
	NearClip(0.5f);
	FarClip(80000.0f);
	RelativeGeom(true);
	HeadTrackingEnabled(false);
	sgdMakeIdentMat4 ( mCamMatrix );
	sgdMakeIdentMat4 ( mComputedMatrix );
	sgdMakeIdentMat4 ( mRelativeGeometryMatrix );
	mEntityToFollow = "";
	mFollowDelayTime = FOLLOW_DELAY;
	vsbBaseViewEntity *head_entity = vsbViewportContext::EntityManager().Head();
	if (head_entity) EntityToFollow(head_entity->Name());
	mModeFlags = (unsigned int) MAINTAIN_ATTITUDE | (unsigned int) FOLLOW_DESIGNATED_ENTITY;
}


vsbFollowCamera::vsbFollowCamera(vsbCameraManager *cm, const string &name, bool postfixed) : 
vsbBaseCamera(cm, name, postfixed), vsbLookAtTransform(cm)
{
	Setup();
}

const std::string & vsbFollowCamera::ClassName() const
{
    static string class_name = "vsbFollowCamera";
    return class_name;
} 
 

vsbFollowCamera::vsbFollowCamera(vsbCameraManager *cm) : 
vsbBaseCamera(cm,"vsbFollowCamera",true), vsbLookAtTransform(cm)
{
	Setup();
}

vsbFollowCamera::~vsbFollowCamera()
{
}

vsbBaseCamera *vsbFollowCamera::Clone() 
{
	string name = Name();
	name+="-";
	vsbFollowCamera *newcam = new vsbFollowCamera(mpCameraManager, name, true);
	*newcam = *this;
	return newcam;
}

void vsbFollowCamera::UpdateCameraMatrix(float time)
{
	static sgdCoord cam_coord;
	vsbBaseViewEntity  *entity_to_follow = 0;

	vsbBaseCamera::UpdateCameraMatrix(time);

	if (IsModeEnabled(vsbFollowCamera::FOLLOW_DESIGNATED_ENTITY))
	{
		// ensure that if follow is anabled and entity not specified, chose the first
		// available entity
		if (mEntityToFollow == "")
		{
 			vsbBaseViewEntity *head_entity = vsbViewportContext::EntityManager().Head();
			if (head_entity) EntityToFollow(head_entity->Name());
		}

		entity_to_follow = vsbViewportContext::EntityManager().LookupEntity(mEntityToFollow);

		if (entity_to_follow)
		{
			sgdCoord entity_pos;
			sgdCopyVec3(entity_pos.xyz, entity_to_follow->EntityPosXYZ());

			vsbTrajEntity *trajEntity = dynamic_cast< vsbTrajEntity * >( entity_to_follow ); 

			if( trajEntity && IsModeEnabled( vsbFollowCamera::LAG_CAMERA_POSITION ) )
			{
				// I think I just need to intercept this call, to lag the camera along the velocity
				//	vector. If I just fiddle with the orientation so that it points along the velocity 
				//	vector instead of the entities orientation, then the relative matrix will be offset
				//	to the right position, and it will be all good (for aircraft anyways).
	
				float currTime = entity_to_follow->CurrTime(); 
				float oldTime = currTime - mFollowDelayTime;
				if( oldTime < entity_to_follow->MinTime() )
				{
					// We don't have data for velocity vector interpolation, just use orientation.
					sgdCopyVec3(entity_pos.hpr, entity_to_follow->OrientationHPR());
				}
				else
				{
					trajEntity->SetCurrTime( oldTime );
					
					sgdVec3 velocityVector;
					sgdSubVec3( velocityVector, entity_pos.xyz, entity_to_follow->EntityPosXYZ() );


					if( sgdLengthVec3( velocityVector ) == 0.0f )
					{
						// We have no velocity. Just use orientation.
						sgdCopyVec3(entity_pos.hpr, entity_to_follow->OrientationHPR());
					}
					else
					{
						sgdNormaliseVec3( velocityVector );

						// Calculate heading from x,y.
						if( (velocityVector[0] == 0.0) && (velocityVector[1] == 0.0) )
						{
							// Going either straight up/down, hence can't calculate heading based
							//	on velocity (think about it!). Just use orientation.
							entity_pos.hpr[0] = entity_to_follow->OrientationHPR()[0];
						}
						else
						{
							float result = atan2( velocityVector[1], velocityVector[0] );
							if( result == 0.0f )
							{
								if( velocityVector[0] == 0.0f )
								{
									// Going either left or right.
									if( (-velocityVector[1]) > 0.0 )
										entity_pos.hpr[0] = 90.0f;
									else
										entity_pos.hpr[0] = 270.0f;
								}
								else if( velocityVector[1] == 0.0f )
								{
									// Going either directly forward or backward.
									if( velocityVector[0] > 0.0 )
										entity_pos.hpr[0] = 0.0f;
									else
										entity_pos.hpr[0] = 180.0f;
								}
								else 
									assert( 0 );	// Shouldn't get here.
							}
							else
							{
								entity_pos.hpr[0] = result * 180.0f / 3.141f;
							}
						}

						// Calculate pitch from x,z.
						if( (velocityVector[0] == 0.0) && (velocityVector[2] == 0.0) )
						{
							// Going either straight up/down, hence can't calculate heading based
							//	on velocity (think about it!). Just use orientation.
							entity_pos.hpr[1] = entity_to_follow->OrientationHPR()[1];
						}
						else
						{
							sgdVec2 pitchVec;
							sgdSetVec2( pitchVec, sqrt( velocityVector[0]*velocityVector[0] + 
													velocityVector[1]*velocityVector[1] ), -velocityVector[2] );
							sgdNormaliseVec2( pitchVec );
							
							float result = atan2( pitchVec[1], pitchVec[0] );
							if( result == 0.0f )
							{
								if( velocityVector[0] == 0.0 )
								{
									// Going either directly up/down.
									if( (-velocityVector[2]) > 0.0 )
										entity_pos.hpr[1] = 90.0f;
									else
										entity_pos.hpr[1] = 270.0f;
								}
								else if( velocityVector[2] == 0.0 )
								{
									// Going either directly forward or backward.
									if( velocityVector[0] > 0.0 )
										entity_pos.hpr[1] = 0.0f;
									else
										entity_pos.hpr[1] = 180.0f;
								}
								else 
									assert( 0 );	// Shouldn't get here.
							}						
							else
							{
								entity_pos.hpr[1] = result * 180.0 / SG_PI;
							}
							
						}

						// Don't adjust roll, just heading and pitch.
						entity_pos.hpr[2] = entity_to_follow->OrientationHPR()[2];
//						entity_pos.hpr[2] = 0.0f;


					}

					trajEntity->SetCurrTime( currTime );
				}
			}
			else
			{
				sgdCopyVec3(entity_pos.hpr, entity_to_follow->OrientationHPR());
			}

			vsbToScaledVSBCoords(entity_pos);
			/*
			above function does the following
            entity_pos.xyz[1] = - entity_pos.xyz[1]; 
            entity_pos.xyz[2] = - entity_pos.xyz[2]; 
            entity_pos.hpr[0] = - entity_pos.hpr[0] - 90;
			*/

			//entity_to_follow->EntityRoot()->getLastTransform ( mComputedMatrix );

//			sgCoord entity_posf;
//			entity_posf.xyz[0] = entity_pos.xyz[0];
//			entity_posf.xyz[1] = entity_pos.xyz[1];
//			entity_posf.xyz[2] = entity_pos.xyz[2];
//			entity_posf.hpr[0] = entity_pos.hpr[0];
//			entity_posf.hpr[1] = entity_pos.hpr[1];
//			entity_posf.hpr[2] = entity_pos.hpr[2];

			sgdMakeCoordMat4(mComputedMatrix, &entity_pos);
		}

	}
	sgdCopyMat4 ( mCamMatrix, mComputedMatrix );

	
	if (entity_to_follow && HudMode() == HUD_COCKPIT && IsModeEnabled(vsbFollowCamera::FOLLOW_ENTITY_HUD))
	{
		vsbModelEntry *model_entry;
		model_entry = vsbViewportContext::ModelManager().FindByTagName(entity_to_follow->AttachedModelName(0));
		if (model_entry)
		{
//		    const sgMat4 &cockpit_mat = model_entry->CockpitMat();
			sgdMat4 cockpit_mat;
			sgdCopyMat4( cockpit_mat, model_entry->CockpitMat() );
			cockpit_mat[3][0] *= entity_to_follow->EntityRoot()->getScale();
			cockpit_mat[3][1] *= entity_to_follow->EntityRoot()->getScale();
			cockpit_mat[3][2] *= entity_to_follow->EntityRoot()->getScale();

		    sgdPreMultMat4(mCamMatrix, cockpit_mat);
		}
	}

		
	sgdSetCoord      (&cam_coord, mCamMatrix);

	if (IsModeEnabled(vsbFollowCamera::FOLLOW_DESIGNATED_ENTITY) && entity_to_follow)
	{
		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_ROLL))		cam_coord.hpr[2] = 0;
		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_X))		cam_coord.xyz[0] = 0;  
		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_Y))		cam_coord.xyz[1] = 0;
		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_Z))		cam_coord.xyz[2] = 0;


		//------------------------
		// The following is a kludge, purpose being to enable the correct behaviour for the
		// various tethering switch combinations. There must be a better way, but for now,
		// this produces the desired results.
		// Setting the h & p in the cam coord to 0, as we did for roll, does not acheive 
		// the desired result

		
		sgdCoord view_adjustment_coord;
		int pitch_roll_untethered = 0;
		sgdSetVec3(view_adjustment_coord.xyz,0,0,0);
		sgdSetVec3(view_adjustment_coord.hpr,0,0,0);

		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_HEADING))
		{
			pitch_roll_untethered++;
			view_adjustment_coord.hpr[0] = -cam_coord.hpr[0];
		}

		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_PITCH)) 
		{
			view_adjustment_coord.hpr[1]	= -cam_coord.hpr[1];
			pitch_roll_untethered++;
		}
		
		if (!IsModeEnabled(vsbFollowCamera::MAINTAIN_ROLL) && pitch_roll_untethered) 
		{
			pitch_roll_untethered++;
			if (pitch_roll_untethered == 3)
			{
				//cludge on cludge for the sake of getting the right behavior
				cam_coord.hpr[0] =  cam_coord.hpr[1] = 0;
			} 	
			pitch_roll_untethered = 0;
		}

		if (pitch_roll_untethered)
		{
			sgdMakeCoordMat4(mCamMatrix, &cam_coord);
			sgdMat4 view_adjustment_mat;
			sgdMakeCoordMat4(view_adjustment_mat, &view_adjustment_coord);

			sgdPreMultMat4(mCamMatrix, view_adjustment_mat);
			sgdSetCoord(&cam_coord, mCamMatrix);
		}

	}



//---------------------------

	if (IsModeEnabled(vsbFollowCamera::TRACK_TARGET_ENTITY))
		sgdCopyMat4(mCamMatrix,MakeLookAtTargetMatrix(cam_coord));
	else
		sgdMakeCoordMat4(mCamMatrix, &cam_coord);


	
	
	if (RelativeGeom())
		if (HudMode() != HUD_COCKPIT || !IsModeEnabled(vsbFollowCamera::FOLLOW_ENTITY_HUD))
		   sgdPreMultMat4 ( mCamMatrix, mRelativeGeometryMatrix);


	
	if (HeadTrackingEnabled())
	{
		sgdMat4 headMatrix;
		sgdMakeRotMat4  ( headMatrix, HeadOrientation()[0], HeadOrientation()[1],HeadOrientation()[2] );
		sgdPreMultMat4 ( mCamMatrix, headMatrix);
	}	

	// calculate offset from camera to origin
	sgdCopyVec3( vsbBaseCamera::CameraToOriginDelta(), mCamMatrix[3] );
	vsbBaseCamera::CameraToOriginDelta()[0] = -vsbBaseCamera::CameraToOriginDelta()[0];
	vsbBaseCamera::CameraToOriginDelta()[1] = -vsbBaseCamera::CameraToOriginDelta()[1];
	vsbBaseCamera::CameraToOriginDelta()[2] = -vsbBaseCamera::CameraToOriginDelta()[2];

	if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
	{
		mCamMatrix[3][0] = mCamMatrix[3][1] = mCamMatrix[3][2] =0;
	}

}


void vsbFollowCamera::SetModeFlags(int mode_flags)
{
	mModeFlags = mode_flags;
}

int  vsbFollowCamera::GetModeFlags()
{
	return mModeFlags;
}

void vsbFollowCamera::EnableMode(EMode track_element)
{
	mModeFlags |= (unsigned int) track_element;
}

void vsbFollowCamera::DisableMode(EMode track_element)
{
	mModeFlags &= ~((unsigned int) track_element);
}

bool vsbFollowCamera::IsModeEnabled(EMode track_element)
{
	return ((mModeFlags & (unsigned int)track_element) == (unsigned int)track_element);
}

void vsbFollowCamera::EnableAllAttitudeTracking()
{
	mModeFlags |= (unsigned int)MAINTAIN_ATTITUDE;
}
void vsbFollowCamera::DisableAllAttitudeTracking()
{
	mModeFlags  &= ~((unsigned int)MAINTAIN_ATTITUDE);
}

vsbFollowCamera  &vsbFollowCamera::operator=(const vsbFollowCamera  &from)
{
    mModeFlags = from.mModeFlags;
    mEntityToFollow = from.mEntityToFollow;
    vsbBaseCamera::operator=(from);
	vsbLookAtTransform::operator=(from);
	return *this;
}

int  vsbFollowCamera::Write(FILE *f, int indent)
{
    vsbFollowCameraParser cam_parser(*this);
    return cam_parser.Write(f,indent);
}

int	 vsbFollowCamera::Parse(Parser::TLineInp *tli)
{
	mParseError.Clear();
	vsbFollowCameraParser cam_parser(*this);
	int result = cam_parser.Parse(tli, true );
	if (result)
	{
		mParseError.Set(*cam_parser.ItsLastError());
	}
	return result;
}

////////////////////////////////////////////////////////////

char *vsbFollowCameraParser::mpKeyWords[] =
{
	"ON",
    "OFF",
	"CAMERA",
	"LOOK_AT_ENTITY",
	"ATTRIBUTES",
	"FOLLOW_ENTITY",
    "ENABLE_TETHER_X",
    "ENABLE_TETHER_Y",
    "ENABLE_TETHER_Z",
    "ENABLE_TETHER_H",
    "ENABLE_TETHER_P",
    "ENABLE_TETHER_R",
    "ENABLE_FOLLOW_ENTITY",
    "ENABLE_LOOKAT_ENTITY",
	"ENABLE_HUD",
	"FOLLOW_DELAY",

	0
};

enum {
    VSB_FCAM_ON,
    VSB_FCAM_OFF,
	VSB_FCAM_CAMERA,
	VSB_FCAM_LOOK_AT,
	VSB_FCAM_ATTRIBUTES,
	VSB_FCAM_FOLLOW,
    VSB_FCAM_ENABLE_TETHER_X,
    VSB_FCAM_ENABLE_TETHER_Y,
    VSB_FCAM_ENABLE_TETHER_Z,
    VSB_FCAM_ENABLE_TETHER_H,
    VSB_FCAM_ENABLE_TETHER_P,
    VSB_FCAM_ENABLE_TETHER_R,
    VSB_FCAM_ENABLE_FOLLOW_ENTITY,
    VSB_FCAM_ENABLE_LOOKAT_ENTITY,
	VSB_FCAM_ENABLE_HUD,
	VSB_FCAM_FOLLOW_DELAY,

};


char *vsbFollowCameraParser::mpErrorStrings[] =
{
	"",
	"Either \"ON\" or \"OFF\" keyword expected.",
	0
};

enum {
	VSB_FCAM_ERR_UNUSED,
	VSB_FCAM_ERR_ONOFF_EXPECTED,
};

vsbFollowCameraParser::vsbFollowCameraParser(vsbFollowCamera &fc):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrFollowCamera(fc) 
{
      mpOutput = new vsbParserHelper(*this);
}

vsbFollowCameraParser::~vsbFollowCameraParser()
{
      delete mpOutput;
}


void vsbFollowCameraParser::WriteOnOff(bool onOrOff)
{
	FILE *f = mpOutput->GetFile();
	if (f)
		fprintf(f,"(%s)",(onOrOff?"ON":"OFF"));
}

void vsbFollowCameraParser::ParseOnOffState(bool &onOrOffVar, int id)
{
	int state_table[] = {VSB_FCAM_OFF, VSB_FCAM_ON};
	
	GetToken(id);
	GetToken(T_LBRACK);
	GetToken();

	int state = KeywordTableSelection(state_table);
    if (state < 0)
		AbortParse(VSB_FCAM_ERR_ONOFF_EXPECTED);
	onOrOffVar = state?true:false;

	GetToken(T_RBRACK);
}

int	 vsbFollowCameraParser::Parse(TLineInp *tli, bool skipHeader)
{
	mSkipHeader = skipHeader;
	return ParseSource(tli, true);
}

int vsbFollowCameraParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

	mpOutput->WriteSpaces(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_CAMERA);
	mpOutput->WriteKeyWord(T_LBRACK);
	mpOutput->WriteName(mrFollowCamera.ClassName());
	mpOutput->WriteKeyWord(T_RBRACK);

    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;

    vsbBaseCameraParser base_camera_parser(mrFollowCamera);
    base_camera_parser.Write(file, indent);

    int count = mrFollowCamera.CountTargetEntities();

    if ( count > 0 && mrFollowCamera.IsModeEnabled(vsbFollowCamera::TRACK_TARGET_ENTITY))
    {
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(VSB_FCAM_LOOK_AT);
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_LBRACE);
        indent+=3;
        mpOutput->WriteNewline(indent);

        for (int i=0; i<count; i++)
        {
            if (i)
            {
                mpOutput->WriteKeyWord(T_COMMA);
                mpOutput->WriteNewline(indent);
            }
            mpOutput->WriteName(mrFollowCamera.GetTargetEntity(i).c_str());
        }

        indent-=3;
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_RBRACE);
    }

    if (mrFollowCamera.IsModeEnabled(vsbFollowCamera::FOLLOW_DESIGNATED_ENTITY))
    {
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(VSB_FCAM_FOLLOW);
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_LBRACE);
        indent+=3;
        mpOutput->WriteNewline(indent);
        mpOutput->WriteName(mrFollowCamera.EntityToFollow());
        indent-=3;
        mpOutput->WriteNewline(indent);
        mpOutput->WriteKeyWord(T_RBRACE);
    }


    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_FCAM_ATTRIBUTES);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;
    mpOutput->WriteNewline(indent);

    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_X);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_X) != 0);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_Y);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_Y) != 0);


    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_Z);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_Z) != 0);


    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_H);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_HEADING) != 0);


    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_P);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_PITCH) != 0);


    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_TETHER_R);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::MAINTAIN_ROLL) != 0);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FCAM_ENABLE_HUD);
    WriteOnOff((mrFollowCamera.mModeFlags & vsbFollowCamera::FOLLOW_ENTITY_HUD) != 0);
//@@
	if (mrFollowCamera.mFollowDelayTime != FOLLOW_DELAY)
	{
		// follow delay is only written if diverges from the default value
		mpOutput->WriteNewline(indent);
		mpOutput->WriteKeyWord(VSB_FCAM_FOLLOW_DELAY);
		mpOutput->WriteValue(mrFollowCamera.mFollowDelayTime);
	}
	
    indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);

    indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);
    return 1;

}

void vsbFollowCameraParser::ParseFunc()
{
    bool onOrOff;
    int attribs = 0;
    mrFollowCamera.mModeFlags = 0;
	vsbBaseCameraParser base_camera_parser(mrFollowCamera);

	char class_name[100],entity_name_buff[100];

	if (!mSkipHeader)
	{
		GetToken(VSB_FCAM_CAMERA);
		GetToken(T_LBRACK);
		GetString(class_name);
		GetToken(T_RBRACK);
		GetToken(T_LBRACE);
	}

	int status = base_camera_parser.Parse(ParserLineInp());
	if (status)
	{
		AbortParse(*base_camera_parser.ItsLastError());
	}

	mrFollowCamera.ClearTargetEntities();

	PeekToken();
	if (GetCurrTokenID() == VSB_FCAM_LOOK_AT)
	{
        mrFollowCamera.mModeFlags |= (int) vsbFollowCamera::TRACK_TARGET_ENTITY;
		GetToken(VSB_FCAM_LOOK_AT);
		GetToken(T_LBRACE);
		PeekToken();
		while (GetCurrTokenID() == T_STRING)
		{
			GetString(entity_name_buff);
			mrFollowCamera.AddTargetEntity(entity_name_buff);
			PeekToken();
			if (GetCurrTokenID() == T_COMMA)
			{
				GetToken();
				PeekToken();
				GetToken(T_STRING);
				ReplaceToken();
			}
		}
		GetToken(T_RBRACE);
	}

    PeekToken();
    if (GetCurrTokenID() == VSB_FCAM_FOLLOW)
	{
        mrFollowCamera.mModeFlags |= (int) vsbFollowCamera::FOLLOW_DESIGNATED_ENTITY;
        GetToken(VSB_FCAM_FOLLOW);
        GetToken(T_LBRACE);
        GetString(entity_name_buff);
        mrFollowCamera.EntityToFollow(entity_name_buff);
        GetToken(T_RBRACE);
    }


	GetToken(VSB_FCAM_ATTRIBUTES);
    GetToken(T_LBRACE);

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_X);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_X;

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_Y);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_Y;

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_Z);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_Z;

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_H);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_HEADING;

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_P);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_PITCH;

    ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_TETHER_R);
    if (onOrOff) attribs |= (int) vsbFollowCamera::MAINTAIN_ROLL;

	ParseOnOffState(onOrOff, VSB_FCAM_ENABLE_HUD);
    if (onOrOff) attribs |= (int) vsbFollowCamera::FOLLOW_ENTITY_HUD;


    mrFollowCamera.mModeFlags |= attribs;

	PeekToken();
	if (GetCurrTokenID() == VSB_FCAM_FOLLOW_DELAY)
	{
		GetToken();
		GetToken(T_EQ);
		mrFollowCamera.mFollowDelayTime = GetNumericValue();
	} else
		mrFollowCamera.mFollowDelayTime = FOLLOW_DELAY;

    GetToken(T_RBRACE);

	GetToken(T_RBRACE);

}	

