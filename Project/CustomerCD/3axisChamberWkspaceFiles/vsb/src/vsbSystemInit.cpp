#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSystemInit.cpp
    \brief Implements the initialisation and closedown functions for the VSB library

    The vsbSystemInit class implements the Init and Close functions which
    are the first and last functions called by an application respectively.

  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#include <string>
#include <map>
#include <set>
#include <assert.h>
using namespace std;

#include "ssg.h"

#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"
#include "vsbEnvironmentOptions.h"
#include "vsbRenderOptions.h"
#include "vsbModelManager.h"

#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"
#include "vsbSimpleCamera.h"
#include "vsbFollowCamera.h"

#include "vsbEntityManager.h"
#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbTrajEntitySTLMap.h"
#include "vsbTrajEntitySTLVector.h"

#include "vsbSystemInit.h"
#include "vsbViewportContext.h"
#include "vsbExplosionFXDirector.h"
#include "vsbSmokeFXDirector.h"
#include "vsbStrokedText.h"
#include "vsbHeadTracker.h"
#include "VsbItrax2Driver.h"

#include "vsbShapeDList.h"
#include "vsbHemisphere.h"
#include "vsbCone.h"
#include "vsbCylinder.h"

void StateLoaderCallback(vsbStateManager *stateManager)
{
}

void DLL_VSB_API vsbSystemInit()
{
	ssgInit();

	REGISTER_CAMERA_FACTORY(vsbSimpleCamera);
	REGISTER_CAMERA_FACTORY(vsbFollowCamera);

	REGISTER_VIEW_ENTITY_FACTORY(vsbBaseViewEntity);
//	REGISTER_VIEW_ENTITY_FACTORY(vsbTrajEntity);
	REGISTER_VIEW_ENTITY_FACTORY(vsbTrajEntitySTLMap);
	REGISTER_VIEW_ENTITY_FACTORY(vsbTrajEntitySTLVector);

	REGISTER_HEAD_TRACKER_DEVICE_FACTORY(vsbItrax2Driver, "InterSense Itrax2");
	

    vsbViewportContext::Init();
	vsbViewportContext::RenderOpts().GLNormalizeOn(true); // for scaling


    vsbViewportContext::StateManager().AddLoader(vsbExplosionFXDirector::StateLoaderCallback);		
	vsbViewportContext::StateManager().AddLoader(vsbSmokeFXDirector::StateLoaderCallback);

    vsbViewportContext::StateManager().CallLoaders();

	



	REGISTER_COMPILED_GL_SHAPE(vsbHemisphere);
	REGISTER_COMPILED_GL_SHAPE(vsbSphere);
	REGISTER_COMPILED_GL_SHAPE(vsbCone);
	REGISTER_COMPILED_GL_SHAPE(vsbCylinder);

	vsbStrokedText::InitialiseFont();

}

void DLL_VSB_API vsbSystemClose()
{
	vsbCameraManager::DestroyCameraCreators();
	vsbHeadTrackerInterface::DestroyFactories();
	vsbViewportContext::StateManager().ClearStates();
    vsbViewportContext::Close();

	CLEAR_COMPILED_GL_SHAPES();

	ssgClose();
}