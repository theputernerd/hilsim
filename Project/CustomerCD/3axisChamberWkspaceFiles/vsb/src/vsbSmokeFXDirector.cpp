#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <math.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>
#include <ctype.h>

#include <list>
#include <map>



#include "ssg.h"
#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

#include "vsbParserHelper.h"
#include "vsbUtilities.h"
#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"

#include "vsbSmokeVtxTable.h"
#include "vsbSmokeFXDirector.h"


// The name of the explosion definition file containing the list of explosions
const char SMOKE_DEFINITION_FILE[] = "smoke.idx";


using namespace std;
using namespace Parser;


class _vsbSmokeEmitterDefinitionParser: public TParseObjectBase
{
    
public:
/*! \brief Constructor
    */
    _vsbSmokeEmitterDefinitionParser(vsbStateManager *stateManager);
    ~_vsbSmokeEmitterDefinitionParser();

    /*! \brief Parse a model index file
    */
    int ParseFile(const string &fname);
	int Write(FILE *file, int indent);
    
private:
	vsbParserHelper *mpOutput;
    vsbStateManager *mpStateManager;
    static char     *mpsKeywords[];    //!< Explosion definition file grammar key words
    static char     *mpsErrors[];      //!< Explosion definition file error strings 
    
    
    void   ParseFunc();                 //!< Overidden virtual parse function (see parser)
    
    
                                        /** @name Sub-Parsers
                                        Private functions that parse different blocks of the model 
                                        file grammar
    */
    //@{
    void ParseSmokeEmitterDefinitionBlock();
    //@}
    
};




////////////////////////////////////////////////////////////////////////////////////

int vsbSmokeFXDirector::mInstanceCount = 0;

std::map<std::string, vsbSmokeEmitterDefinition> vsbSmokeFXDirector::msSmokeEmitterDefinitions;

vsbSmokeFXDirector::vsbSmokeFXDirector()
{
    mInstanceCount++;
	mMaxPuffCount = DEFAULT_MAX_PUFF_COUNT;
	mPreallocatePuffs = false;
    assert(mInstanceCount == 1);
	mTransform = new ssgTransform;
	vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(mTransform);
	mTransform->ref();
}

vsbSmokeFXDirector::~vsbSmokeFXDirector()
{
    StopActivePuffs();
    ClearFreeList();
    ClearAllEmitters();
    if (--mInstanceCount == 0) 
        ClearSmokeEmitterDefinitions();
	ssgDeRefDelete(mTransform);
}

void vsbSmokeFXDirector::StopActivePuffs()
{
    list<vsbSmokeVtxTable *>::iterator iter;
    for (iter = mActivePuffs.begin(); iter != mActivePuffs.end(); iter++)
    {
       // disconnect from scene graph and place in the free list
        mTransform->removeKid((*iter));
        mFreePuffs.push_back((*iter));
    }
    mActivePuffs.clear();
 }

// delete active puffs that arent in time scope

void vsbSmokeFXDirector::StopActiveExpiredPuffs(/*double time*/)
{
    list<vsbSmokeVtxTable *>::iterator iter;
	list<vsbSmokeVtxTable *>::iterator del_iter;
    for (iter = mActivePuffs.begin(); iter != mActivePuffs.end();  iter++)
    {
		
		while(iter != mActivePuffs.end() && (*iter)->IsExpired())
		{


			mTransform->removeKid((*iter));
			mFreePuffs.push_back((*iter));
			iter = mActivePuffs.erase(iter);
		}
    }
}

void vsbSmokeFXDirector::ClearAllEmitters()
{
    list<vsbSmokeEmitter *>::iterator iter;
    for (iter = mEmitters.begin(); iter != mEmitters.end(); iter++)
        delete (*iter);
    mEmitters.clear();
}

void vsbSmokeFXDirector::ClearUnattachedEmitters()
{
    list<vsbSmokeEmitter *>::iterator iter;

    for (iter = mEmitters.begin(); iter != mEmitters.end();  iter++)
	{

		while(iter != mEmitters.end() && (*iter)->AttachedEntity() == 0)
		{
			delete (*iter);
			iter = mEmitters.erase(iter);
		} 
	 
	}
}

void vsbSmokeFXDirector::ClearFlaggedEmitters()
{
    list<vsbSmokeEmitter *>::iterator iter;

	for (iter = mEmitters.begin(); iter != mEmitters.end();  iter++)
	{
		
		while(iter != mEmitters.end() && (*iter)->CanDelete())
		{
			delete (*iter);
			iter = mEmitters.erase(iter);
		} 
		
	}
	
}



void vsbSmokeFXDirector::ClearAttachedEmitters(int entity_id)
{
    list<vsbSmokeEmitter *>::iterator iter;
	for (iter = mEmitters.begin(); iter != mEmitters.end();  iter++)
	{
		
		while(iter != mEmitters.end() && (*iter)->AttachedEntity() != 0)
		{
			delete (*iter);
			iter = mEmitters.erase(iter);
		} 
		
	}
}


vsbSmokeEmitter  *vsbSmokeFXDirector::NewSmokeEmitter(std::string emitter_name)
{

	map<string, vsbSmokeEmitterDefinition>::iterator iter;
	assert(msSmokeEmitterDefinitions.size() > 0);

	iter = msSmokeEmitterDefinitions.find(emitter_name);
	if (iter == msSmokeEmitterDefinitions.end())
		return 0;

	vsbSmokeEmitter *emitter = new vsbSmokeEmitter;
	
	emitter->EmitterDef(&(iter->second));

	mEmitters.push_back(emitter);

	
	return emitter;
}

void vsbSmokeFXDirector::ClearFreeList()
{
    list<vsbSmokeVtxTable *>::iterator iter;
    for (iter = mFreePuffs.begin(); iter != mFreePuffs.end(); iter++)
    {
        ssgDeRefDelete(*iter);
    }
    mFreePuffs.clear();
}

void vsbSmokeFXDirector::StateLoaderCallback(vsbStateManager *stateManager)
{
    
    LoadSmokeEmitterDefinitions(stateManager, SMOKE_DEFINITION_FILE);
}

void vsbSmokeFXDirector::LoadSmokeEmitterDefinitions(vsbStateManager *stateManager, 
                                                     const std::string &smokeIndexFile)
{
    _vsbSmokeEmitterDefinitionParser parser(stateManager);
    ClearSmokeEmitterDefinitions();
    int result = parser.ParseFile(vsbMakeSmokeFXPath(smokeIndexFile).c_str());
    if (result)
    {
        char buffer[400];
        sprintf(buffer,"While loading smoke emitter index file: \"%s\"\n(%d) %s\n"
            "occurred at or near line;\n%03d: %s\n",
            vsbMakeSmokeFXPath(smokeIndexFile).c_str(), 
            parser.ItsLastError()->Id(),
            parser.ItsLastError()->Desc(),
            parser.ItsLastError()->Pos(),
            parser.ItsLastError()->Line());
        ulSetError(UL_FATAL, buffer);
        return;
    }
}

void vsbSmokeFXDirector::SaveSmokeEmitterDefinitions()
{
	 std::string smokeIndexFile = "smoke.idx";
 	 _vsbSmokeEmitterDefinitionParser parser(&vsbViewportContext::StateManager());

	 FILE *f;
	 f = fopen(vsbMakeSmokeFXPath(smokeIndexFile).c_str(),"wt");
	 if (f)
	 {
		 parser.Write(f,0);
		 fclose(f);
	 }

	 

}

void vsbSmokeFXDirector::ClearSmokeEmitterDefinitions()
{
    msSmokeEmitterDefinitions.clear();
}

void vsbSmokeFXDirector::SetMaxPuffs(int max_puffs)
{
	mMaxPuffCount = max_puffs;
	if (mPreallocatePuffs)
		PreCreatePuffs();
}


void vsbSmokeFXDirector::PreallocPuffs(bool truefalse)
{
   mPreallocatePuffs = truefalse;
   if (mPreallocatePuffs)
	   PreCreatePuffs();
}

void vsbSmokeFXDirector::PreCreatePuffs()
{
    if (!mMaxPuffCount) return; // we have not defined howmany puffs to handle

    int curr_count = mFreePuffs.size() + mActivePuffs.size();
    if (curr_count < mMaxPuffCount)
    {
        for(int i=0; i< mMaxPuffCount - curr_count; i++)
        {
            vsbSmokeVtxTable *tmp = new vsbSmokeVtxTable;   
            tmp->ref();
            mFreePuffs.push_back(tmp);
        }
    }
}

void vsbSmokeFXDirector::Update(double time)
{

	list<vsbSmokeEmitter *>::iterator iter;

	for (iter = mEmitters.begin(); iter!= mEmitters.end(); iter++)
	{
		(*iter)->UpdateEmitter(time);
	}
	StopActiveExpiredPuffs();
	ClearFlaggedEmitters();
	
}

void vsbSmokeFXDirector::Regenerate(double time) // when jumping (also sort wrt to time)
{
    //@@@@@@ To be done
	//update emitters
}

vsbSmokeVtxTable *vsbSmokeFXDirector::NewSmokePuff()
{
	vsbSmokeVtxTable * new_puff = 0;

	
	if (mFreePuffs.size()!=0) // have puffs in free list
	{
		new_puff = mFreePuffs.front();
		mFreePuffs.pop_front();
		mActivePuffs.push_front(new_puff);
		mTransform->addKid(new_puff);


		return new_puff;
	}  
	else if (mActivePuffs.size()==0) // no puffs have been allocated
	{
		PreCreatePuffs();
		new_puff = mFreePuffs.front();
		mFreePuffs.pop_front();
		mActivePuffs.push_front(new_puff);
		mTransform->addKid(new_puff);

		return new_puff;
	}
	else // all puffs are active
	{
		
		new_puff = mActivePuffs.back();
		mActivePuffs.pop_back();
		mActivePuffs.push_front(new_puff);

		return new_puff;
		
	}

}


vsbSmokeEmitter *vsbSmokeFXDirector::FindEmitterById(int id)
{
	list<vsbSmokeEmitter *>::iterator iter;
	
	for (iter = mEmitters.begin(); iter!= mEmitters.end(); iter++)
	{
		if ((*iter)->Id() == id) return *iter;
	}
	return 0;

}

void  vsbSmokeFXDirector::DeleteEmitterById(int id)
{
	list<vsbSmokeEmitter *>::iterator iter;
	
	for (iter = mEmitters.begin(); iter!= mEmitters.end(); iter++)
	{
		if ((*iter)->Id() == id) 
		{
			delete (*iter);
			mEmitters.erase(iter);
			return;
		}
	}
}


bool vsbSmokeFXDirector::SaveFile(const std::string &name)
{
	FILE *f;
	f = fopen(name.c_str(),"wt");
	if (!f) return false;
	
	vsbSmokeFXListParser expl_parser(*this);
	bool success = (expl_parser.Write(f, 0) == 1);
	fclose(f);
	return success;

}

const char *vsbSmokeFXDirector::LoadFile(const std::string &filename)
{
	static char buffer[400];
	
	vsbSmokeFXListParser smoke_parser(*this);
	TFileLineInp fli;
	
	if (!fli.Open(filename.c_str()))
	{
		sprintf(buffer,"Could not open smoke emitter list file: \"%s\"\n",	filename.c_str());
		return buffer;
	}
	
	
	int err_val = smoke_parser.Parse(&fli);
	fli.Close();
	
	
	
	if (err_val)
	{
		sprintf(buffer,"While loading smoke emitters from file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			filename.c_str(), //parser.CurrentActiveSourceFile().c_str(), 
			smoke_parser.ItsLastError()->Id(),
			smoke_parser.ItsLastError()->Desc(),
			smoke_parser.ItsLastError()->Pos(),
			smoke_parser.ItsLastError()->Line());
		ulSetError(UL_WARNING, buffer);
		return buffer;
	}
	
	return 0;
	
}

////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
/*!
\class _vsbSmokeEmitterDefinitionParser 

  \brief Defines a parse object for the smoke emitter definition file grammar
  
*/
////////////////////////////////////////////////////////////////////////////////


// Enumerations for keywords in explosion definition grammar

enum {
    SME_DEFINITION_LIST,
        SME_VERSION,
        SME_EMITTER,    
        SME_FILENAME,
        SME_STARTSIZE,
        SME_DELTASIZE,
		SME_LIFESPAN,
        SME_PRELOAD,
        SME_FALSE,
        SME_TRUE,
        SME_P0_X,
        SME_P0_Y,
        SME_P0_Z,
		SME_V0_X,
        SME_V0_Y,
        SME_V0_Z,
        SME_TO,
		SME_EMISSIONRATE,
		SME_DECAY,
		SME_LINEAR,
		SME_SQUARE,
		SME_INVERSE_SQUARE,
		SME_CUBE,
		SME_INVERSE_CUBE,
		SME_MASS_CONSERVATION,
		SME_LIGHTING,
		SME_AMBIENT,
		SME_RGB,
       
};

// Enumerations for error messages when parsing explosion definition grammar

enum {
    SME_UNUSED,
        SME_INTERNAL_ERR,
        SME_FILE_ERR,
        SME_STARTSIZE_ERR,
		SME_DELTASIZE_ERR,
        SME_LIFESPAN_ERR,
        SME_MULTIPLE_STATE_DEF_ERR,
        SME_MULTIPLE_DEF_ERR,
        SME_BOOLEAN_EXPECTED_ERR,
		SME_EMISSIONRATE_ERR,
		SME_DECAY_ERR,
		SME_LIGHTING_ERR,
		SME_COLOR_RANGE_ERR,
        
};



char * _vsbSmokeEmitterDefinitionParser::mpsKeywords[] = 
{
    "SMOKE_DEFINITION_LIST",
        "VERSION",
        "EMITTER",
        "FILENAME",
        "STARTSIZE",
		"DELTASIZE",
        "LIFESPAN",
        "PRELOAD",
        "FALSE",
        "TRUE",
        "P0_X",
        "P0_Y",
        "P0_Z",
		"V0_X",
        "V0_Y",
        "V0_Z",
        "TO",
		"EMISSIONRATE",
		"DECAY",
		"LINEAR",
		"SQUARE",
		"INVERSE_SQUARE",
		"CUBE",
		"INVERSE_CUBE",
		"MASS_CONSERVATION",
		"LIGHTING",
		"AMBIENT",
		"RGB",
        0
};

char * _vsbSmokeEmitterDefinitionParser::mpsErrors[] = 
{
    "",
        "Internal Error",
        "Unable to open smoke emitter index file",
        "Startsize must be greater than 0",
		"Deltasize must be greater than 0",
        "Life time must be greater than 0 seconds",
        "Smoke Emitter state multiply defined",
        "Smoke Emitter \"TRUE\" or \"FALSE\" keyword",
		"Emissionrate must be greater than 0",
		"Decay must be specified as LINEAR or INVERSE_SQUARE",
		"Lighting keyword AMBIENT or RGB expected",
        0
};

_vsbSmokeEmitterDefinitionParser::~_vsbSmokeEmitterDefinitionParser()
{
	delete mpOutput;
}

_vsbSmokeEmitterDefinitionParser::_vsbSmokeEmitterDefinitionParser(vsbStateManager *stateManager):
    TParseObjectBase(mpsKeywords, mpsErrors), mpStateManager(stateManager)
    {
		mpOutput = new vsbParserHelper(*this);
     }
    
    
    int _vsbSmokeEmitterDefinitionParser::ParseFile(const string &fname)
    {
        TFileLineInp mInput;
        vsbSmokeFXDirector::msSmokeEmitterDefinitions.clear();
        
        
        if (!mInput.Open(fname.c_str()))
        {
            mInput.Close();
            itsLastError->Set(SME_FILE_ERR,0,"",mpsErrors[SME_FILE_ERR]);
            return SME_FILE_ERR;
        } 
        
        int errNum = ParseSource(&mInput, 1);
        mInput.Close();
        return errNum;
    }
    
    void _vsbSmokeEmitterDefinitionParser::ParseFunc()
    {
        double ver_num;
        
        GetToken(SME_DEFINITION_LIST);
        GetToken(T_LBRACK);
        GetToken(SME_VERSION);
        ver_num = GetNumericValue();
        GetToken(T_RBRACK);
        GetToken(T_LBRACE);
        PeekToken();
        while (GetCurrTokenID() == SME_EMITTER)
        {
            ParseSmokeEmitterDefinitionBlock();
			PeekToken();
        }
        GetToken(T_RBRACE);
        
    }
    
    
    
    void _vsbSmokeEmitterDefinitionParser::ParseSmokeEmitterDefinitionBlock()
    {
        bool   preload;
        string emitter_name;
        string emitter_file;
        float    startsize;
        float  lifespan;
		float  deltasize;
        float  temp;
		float emissionrate;
		int i;
		EDecayMode  decay_mode;
		bool  ambient_light;
		sgVec3 light_color;
        
        sgdVec3 lower_vel_bound;    // random generator bounds - x, y, z, acc
        sgdVec3 upper_vel_bound;
        bool    randomize_vel[3];   // random generator state

		sgdVec3 lower_pos_bound;    // random generator bounds - x, y, z, acc
        sgdVec3 upper_pos_bound;
        bool    randomize_pos[3];   // random generator state
		


        
        GetToken(SME_EMITTER);
        GetToken(T_LBRACK);
        GetToken(T_STRING);
        emitter_name = GetCurrTokenStr();
        GetToken(T_RBRACK);
        GetToken(T_LBRACE);
        
        GetToken(SME_FILENAME);
        GetToken(T_EQ);
        GetToken(T_STRING);
        emitter_file = GetCurrTokenStr();
        
        GetToken(SME_PRELOAD);
        GetToken(T_EQ);
        GetToken();
        if (GetCurrTokenID() == SME_TRUE)
            preload = true;
        else if (GetCurrTokenID() == SME_FALSE)
            preload = false;
        else
            AbortParse(SME_BOOLEAN_EXPECTED_ERR);
		
		PeekToken();
		
		if (GetCurrTokenID() == SME_LIGHTING)
		{
			GetToken();
			GetToken(T_EQ);
			GetToken();
			if (GetCurrTokenID() == SME_AMBIENT)
			{
				ambient_light = true;
			} 
			else if (GetCurrTokenID() == SME_RGB)
			{
				ambient_light = false;
				GetToken(T_LBRACK);
				light_color[0] = GetNumericValue();
				if (light_color[0]<0 || light_color[0]>1)  
					AbortParse(SME_COLOR_RANGE_ERR);
				GetToken(T_COMMA);
				light_color[1] = GetNumericValue();
				if (light_color[1]<0 || light_color[1]>1)  
					AbortParse(SME_COLOR_RANGE_ERR);

				GetToken(T_COMMA);
				light_color[2] = GetNumericValue();
				if (light_color[2]<0 || light_color[2]>1)  
					AbortParse(SME_COLOR_RANGE_ERR);
				GetToken(T_RBRACK);
			} 
			else
			{
				AbortParse(SME_LIGHTING_ERR);
			}
		} else
		{
			ambient_light = false;
			sgSetVec3(light_color,1,1,1);
		}

		
		PeekToken();
		if (GetCurrTokenID() == SME_DECAY)
		{
			GetToken();
			GetToken(T_EQ);
			GetToken();                                           
			switch(GetCurrTokenID())
			{
				
			case SME_LINEAR:
				decay_mode = DECAY_LINEAR;
				break;

			case SME_INVERSE_SQUARE:
				decay_mode = DECAY_INVERSESQUARE;
			    break;

			case SME_SQUARE:
				decay_mode = DECAY_SQUARE;
				break;

			case SME_INVERSE_CUBE:
				decay_mode = DECAY_INVERSECUBE;
				break;

			case SME_CUBE:
				decay_mode = DECAY_CUBE;
				break;

			case SME_MASS_CONSERVATION:
				decay_mode = DECAY_MASSCONSERVATION;
				break;
				
			default:
				AbortParse(SME_DECAY_ERR);
			}
		} else
			decay_mode = DECAY_LINEAR;

        GetToken(SME_LIFESPAN);
        GetToken(T_EQ);
        lifespan = GetNumericValue();
        if (lifespan <= 0)
            AbortParse(SME_LIFESPAN_ERR);
        


		GetToken(SME_EMISSIONRATE);
        GetToken(T_EQ);
        emissionrate = GetNumericValue();
        if (emissionrate <= 0)
            AbortParse(SME_EMISSIONRATE_ERR);

        GetToken(SME_STARTSIZE);
        GetToken(T_EQ);
        startsize = GetNumericValue();
        if (startsize <= 0)
            AbortParse(SME_STARTSIZE_ERR);

		GetToken(SME_DELTASIZE);
        GetToken(T_EQ);
        deltasize = GetNumericValue();
        if (startsize < 0)
            AbortParse(SME_DELTASIZE_ERR);
        

             
        
		PeekToken();
		// velocity perturbation is optional
		if (GetCurrTokenID() == SME_P0_X)
        {
			
			GetToken(SME_P0_X);
			GetToken(T_EQ);
			lower_pos_bound[0] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_pos_bound[0] = GetNumericValue();
				
				if (upper_pos_bound[0] < lower_pos_bound[0])
				{
					temp = upper_pos_bound[0];
					upper_pos_bound[0] = lower_pos_bound[0];
					lower_pos_bound[0] = temp;
				}
				randomize_pos[0] = true;
			} else
			{
				upper_pos_bound[0] = 0;
				randomize_pos[0] = false;
			}
			
			
			GetToken(SME_P0_Y);
			GetToken(T_EQ);
			lower_pos_bound[1] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_pos_bound[1] = GetNumericValue();
				if (upper_pos_bound[1] < lower_pos_bound[1])
				{
					temp = upper_pos_bound[1];
					upper_pos_bound[1] = lower_pos_bound[1];
					lower_pos_bound[1] = temp;
				}
				randomize_pos[1] = true;
			} else
			{
				upper_pos_bound[1] = 0;
				randomize_pos[1] = false;
			}
			
			GetToken(SME_P0_Z);
			GetToken(T_EQ);
			lower_pos_bound[2] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_pos_bound[2] = GetNumericValue();
				if (upper_pos_bound[2] < lower_pos_bound[2])
				{
					temp = upper_pos_bound[2];
					upper_pos_bound[2] = lower_pos_bound[2];
					lower_pos_bound[2] = temp;
				}
				randomize_pos[2] = true;
			} else
			{
				upper_pos_bound[2] = 0;
				randomize_pos[2] = false;
			}
			
		} else
		{
			sgdSetVec3(lower_pos_bound,0,0,0);
			sgdCopyVec3(upper_pos_bound, lower_pos_bound);
			randomize_pos[0] = randomize_pos[1] = randomize_pos[2] = false; 
			
		}
        
        
		PeekToken();
		// velocity perturbation is optional
		if (GetCurrTokenID() == SME_V0_X)
        {
			
			GetToken(SME_V0_X);
			GetToken(T_EQ);
			lower_vel_bound[0] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_vel_bound[0] = GetNumericValue();
				
				if (upper_vel_bound[0] < lower_vel_bound[0])
				{
					temp = upper_vel_bound[0];
					upper_vel_bound[0] = lower_vel_bound[0];
					lower_vel_bound[0] = temp;
				}
				randomize_vel[0] = true;
			} else
			{
				upper_vel_bound[0] = 0;
				randomize_vel[0] = false;
			}
			
			
			GetToken(SME_V0_Y);
			GetToken(T_EQ);
			lower_vel_bound[1] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_vel_bound[1] = GetNumericValue();
				if (upper_vel_bound[1] < lower_vel_bound[1])
				{
					temp = upper_vel_bound[1];
					upper_vel_bound[1] = lower_vel_bound[1];
					lower_vel_bound[1] = temp;
				}
				randomize_vel[1] = true;
			} else
			{
				upper_vel_bound[1] = 0;
				randomize_vel[1] = false;
			}
			
			GetToken(SME_V0_Z);
			GetToken(T_EQ);
			lower_vel_bound[2] = GetNumericValue();
			PeekToken();
			if (GetCurrTokenID() == SME_TO)
			{
				GetToken();
				upper_vel_bound[2] = GetNumericValue();
				if (upper_vel_bound[2] < lower_vel_bound[2])
				{
					temp = upper_vel_bound[2];
					upper_vel_bound[2] = lower_vel_bound[2];
					lower_vel_bound[2] = temp;
				}
				randomize_vel[2] = true;
			} else
			{
				upper_vel_bound[2] = 0;
				randomize_vel[2] = false;
			}
			
		} else
		{
			sgdSetVec3(lower_vel_bound,0,0,0);
			sgdCopyVec3(upper_vel_bound, lower_vel_bound);
			randomize_vel[0] = randomize_vel[1] = randomize_vel[2] = false; 
			
		}


        
        // Need to create state and inser into state manager here
        
        if (mpStateManager->Find(emitter_name)) 
            AbortParse(SME_MULTIPLE_STATE_DEF_ERR);
        
        //See if a like named explosion has already been added
        map<string, vsbSmokeEmitterDefinition>::iterator iter;
        
        iter = vsbSmokeFXDirector::msSmokeEmitterDefinitions.find(emitter_name);
        if (iter != vsbSmokeFXDirector::msSmokeEmitterDefinitions.end())
            AbortParse(SME_MULTIPLE_DEF_ERR);   
        
        vsbSmokeEmitterDefinition emitter_def(lifespan,startsize,deltasize,emitter_file, preload);
        
		emitter_def.EmissionRate(emissionrate);
		emitter_def.DecayMode(decay_mode);
		emitter_def.Lighting(ambient_light, light_color);
		emitter_def.mName = emitter_name;

		for (i=0; i<3; i++)
            emitter_def.SetPosPerturbation(i, lower_pos_bound[i], upper_pos_bound[i], randomize_pos[i]);
		
        for (i=0; i<3; i++)
            emitter_def.SetVelPerturbation(i, lower_vel_bound[i], upper_vel_bound[i], randomize_vel[i]);
        
        
        // if texture already present, reference it rather than reloading a copy
        ssgSimpleState *emitter_state = mpStateManager->FindByTextureFilename(emitter_file);
        if (!emitter_state)
        {
            if (preload)
            {
                emitter_state = vsbSmokeEmitterDefinition::MakeState(emitter_file.c_str());
                mpStateManager->Insert(emitter_file, emitter_state);


                emitter_def.Loaded(true);
            } else
                emitter_def.Loaded(false);
        } else 
        {
            mpStateManager->Insert(emitter_file, emitter_state);
            emitter_def.Loaded(true);
        }

        emitter_def.mName = emitter_name;
        vsbSmokeFXDirector::msSmokeEmitterDefinitions.insert(
            pair<string, vsbSmokeEmitterDefinition>(emitter_name,emitter_def));
        
        GetToken(T_RBRACE);
        PeekToken();
        
}

#define KWWIDTH 13
int _vsbSmokeEmitterDefinitionParser::Write(FILE *file, int indent)
{
	map<string, vsbSmokeEmitterDefinition> *definitionsMap = &vsbViewportContext::SmokeFXDirector().SmokeEmitterDefinitions();
	mpOutput->SetFile(file);
	mpOutput->WriteKeyWord(SME_DEFINITION_LIST);
	mpOutput->WriteKeyWord(T_LBRACK);
	mpOutput->WriteKeyWord(SME_VERSION);
	mpOutput->WriteString( " 1.0");
	mpOutput->WriteKeyWord(T_RBRACK);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent += 3;
	mpOutput->WriteNewline(indent);

	map<string, vsbSmokeEmitterDefinition>::iterator iter;
	for (iter = definitionsMap->begin();  iter != definitionsMap->end();  iter++)
	  {
			mpOutput->WriteNewline(indent);
			mpOutput->WriteKeyWord(SME_EMITTER);
			mpOutput->WriteKeyWord(T_LBRACK);
			mpOutput->WriteString("\"");
			mpOutput->WriteString(iter->second.Name());
			mpOutput->WriteString("\"");
			mpOutput->WriteKeyWord(T_RBRACK);
			mpOutput->WriteNewline(indent);
			mpOutput->WriteKeyWord(T_LBRACE);
			indent+=3;
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_FILENAME, KWWIDTH);
			mpOutput->WriteString(" = \"");
			mpOutput->WriteString(iter->second.mTextureFilename);
			mpOutput->WriteString("\"");
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_PRELOAD, KWWIDTH);
			mpOutput->WriteString(" = ");
			mpOutput->WriteKeyWord(iter->second.mPreload?SME_TRUE:SME_FALSE);
			mpOutput->WriteNewline(indent);

			int modes[] = {SME_LINEAR,SME_SQUARE,SME_INVERSE_SQUARE,SME_CUBE,
				           SME_INVERSE_CUBE,SME_MASS_CONSERVATION};
			mpOutput->WriteKeyWord(SME_DECAY, KWWIDTH);
			mpOutput->WriteString(" = ");
			mpOutput->WriteKeyWord(modes[iter->second.mDecayMode]);
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_LIFESPAN, KWWIDTH);
			mpOutput->WriteValue(iter->second.mLifeSpan);
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_EMISSIONRATE, KWWIDTH);
			mpOutput->WriteValue(iter->second.mEmissionRate);
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_STARTSIZE, KWWIDTH);
			mpOutput->WriteValue(iter->second.mStartSize);
			mpOutput->WriteNewline(indent);

			mpOutput->WriteKeyWord(SME_DELTASIZE, KWWIDTH);
			mpOutput->WriteValue(iter->second.mDeltaSize);
			
			
			if (iter->second.mRandomizePos[0] || iter->second.mRandomizePos[1] || iter->second.mRandomizePos[2] 
			|| iter->second.mLowerPosBound[0] != 0 || iter->second.mLowerPosBound[1] != 0 || iter->second.mLowerPosBound[0] != 0)
			{
				int labels[] = {SME_P0_X,SME_P0_Y,SME_P0_Z};
				for (int i=0; i<3; i++)
				{
					mpOutput->WriteNewline(indent);
					mpOutput->WriteKeyWord(labels[i], KWWIDTH);
					mpOutput->WriteValue(iter->second.mLowerPosBound[i] );
					if (iter->second.mRandomizePos[i])
					{
						mpOutput->WriteString(" TO ");
						fprintf(file,"%lg",iter->second.mUpperPosBound[i] );
						
					}
					
					
				}	
			}

			if (iter->second.mRandomizeVel[0] || iter->second.mRandomizeVel[1] || iter->second.mRandomizeVel[2] 
				|| iter->second.mLowerVelBound[0] != 0 || iter->second.mLowerVelBound[1] != 0 || iter->second.mLowerVelBound[0] != 0)
			{
				int labels[] = {SME_V0_X,SME_V0_Y,SME_V0_Z};
				for (int i=0; i<3; i++)
				{
					mpOutput->WriteNewline(indent);
					mpOutput->WriteKeyWord(labels[i], KWWIDTH);
					mpOutput->WriteValue(iter->second.mLowerVelBound[i] );
					if (iter->second.mRandomizeVel[i])
					{
						mpOutput->WriteString(" TO ");
						fprintf(file,"%lg",iter->second.mUpperVelBound[i] );
					}
					
					
				}	
			}



			indent-=3;
			mpOutput->WriteNewline(indent);
			mpOutput->WriteKeyWord(T_RBRACE);
			mpOutput->WriteNewline(indent);
	  }

	indent-=3;
    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_RBRACE);



	return TRUE;
}
/////////////////////////////////////////////////////////////////////



char *vsbSmokeFXListParser::mpKeyWords[] = {0};
char *vsbSmokeFXListParser::mpErrorStrings[]= {"","Unexpected Token",0};

enum vsbSmokeFXListParserError
{
	SMFX_UNUSED,
	SMFX_UNEXPECTED_TOKEN,
};

vsbSmokeFXListParser::vsbSmokeFXListParser(vsbSmokeFXDirector &efxd):
mrSmokeFXDirector(efxd)
{
	mpOutput = new vsbParserHelper(*this);
}
	
vsbSmokeFXListParser::~vsbSmokeFXListParser()
{
	delete mpOutput;
}

void vsbSmokeFXListParser::ParseFunc()
{
	mSmokeList.clear();

	GetToken();
	while (GetCurrTokenID() == T_STRING || GetCurrTokenID() == T_UNKNOWN)
	{
		mTypeName = GetCurrTokenStr();
		mStartTime = GetNumericValue();
		mEndTime = GetNumericValue();
		PeekToken();
		if (GetCurrTokenID() == T_STRING || GetCurrTokenID() == T_UNKNOWN)
		{
			mAttached	= true;
			GetToken();
			mEntityName = GetCurrTokenStr();
			PeekToken();
			if (GetCurrTokenID() == T_STRING || GetCurrTokenID() == T_UNKNOWN)
			{
				GetToken();
				mAttachmentName = GetCurrTokenStr();
			} else
				mAttachmentName = "";
			
		} else
		{
			mAttached = false;
			mX = GetNumericValue();
			mY = GetNumericValue();
			mZ = GetNumericValue();
		}
		GetToken();
		mSmokeList.push_back(*this);
		
	}
	if (GetCurrTokenID() != T_EOF)
		AbortParse(SMFX_UNEXPECTED_TOKEN);

	vsbSmokeFXDirector &fxdir = vsbViewportContext::SmokeFXDirector();
	
	for (std::vector<vsbSmokeSeting>::iterator iter = mSmokeList.begin();
	iter != mSmokeList.end(); iter++)
	{
		vsbSmokeEmitter *smoke_emitter = fxdir.NewSmokeEmitter(iter->mTypeName);

		if (smoke_emitter)
		{
			smoke_emitter->Activate(true);
			smoke_emitter->Interval(iter->mStartTime,iter->mEndTime);
			if (iter->mAttached)
			{
				smoke_emitter->AttachToEntity(iter->mEntityName,iter->mAttachmentName);
			} 
			else
			{
				smoke_emitter->AttachToEntity(-1,0);
				smoke_emitter->SetCurrPos(iter->mX,iter->mY,iter->mZ);
			}
		} 
		
	}
}

int	 vsbSmokeFXListParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int vsbSmokeFXListParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;
	
	
	fprintf(file, "! Smoke-type, start-time, end-time, x, y, z\n");
	fprintf(file, "! Smoke-type, start-time, end-time, entity-name, attachment-name\n");
	fprintf(file, "! ------------------------------------------------\n");

	std::list<vsbSmokeEmitter *> &emitter_list = vsbViewportContext::SmokeFXDirector().SmokeEmitters();
	std::list<vsbSmokeEmitter *>::iterator iter;

	for ( iter = emitter_list.begin(); iter != emitter_list.end(); iter++)
	{
		fprintf(file,"\"%s\" ",(*iter)->EmitterDef()->Name().c_str());
		fprintf(file,"%lf %lf ",(*iter)->StartTime(), (*iter)->EndTime());

		if ((*iter)->AttachedEntityHandle()>=0)
		{
			fprintf(file,"\"%s\" \"%s\"\n", (*iter)->AttachedEntityName().c_str(),(*iter)->ModelAttachmentName().c_str());
			
		} else
		{
			double x, y, z;
			(*iter)->GetCurrPos(x, y, z);
			fprintf(file,"%lf %lf %lf\n",x,y,z );
		}

	}
	return 1;
}
	


