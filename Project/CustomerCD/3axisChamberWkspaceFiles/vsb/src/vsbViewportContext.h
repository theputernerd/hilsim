#ifndef _vsbViewportContext_H
#define _vsbViewportContext_H
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbViewportContext.h
    \brief Defines the vsbViewportContext class.

    The vsbViewportContext class is the interface between VSB,
    the OpenGL context and the user's visualisation system 
    applicatio code. The vsbViewportContext maps directly to a 
    view window in the application, ie; for each view the application 
    presents, there must be a corresponding vsbViewportContext object 
    (as	there should also be an OpenGL context)

    \sa vsbViewportContext.cpp

    \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#ifndef _vsbArcBall_H
#include "vsbArcBall.h"
#endif


#ifndef _vsbCameraManager_H
#include "vsbCameraManager.h"
#endif

#ifndef _vsbEnvironmentManager_H
#include "vsbEnvironmentManager.h"
#endif


#ifndef _vsbEntityManager_H
#include "vsbEntityManager.h"
#endif


#ifndef _vsbExplosionFXDirector_H
#include "vsbExplosionFXDirector.h"
#endif

#ifndef _vsbSmokeFXDirector_H
#include "vsbSmokeFXDirector.h"
#endif

#ifndef _vsbModelManager_H
#include "vsbModelManager.h"
#endif



#ifndef _vsbHeadTracker_H
#include "VsbHeadTracker.h"
#endif

#ifndef vsbShapeDList_H
#include "VsbShapeDList.h"
#endif

#ifndef vsbHemisphere_H
#include "vsbHemisphere.h"
#endif

#include <set>

/////////////////////////////////////////////////////////////////
/*! 
    \class vsbViewportContext vsbViewportContext.h 

    \brief The vsbViewportContext class is the interface between 
    VSB, OpenGL and the application code. 
  
    Each vsbViewportContext class instance maps directly to a view 
    window in the application, ie; for each view the application 
    presents, there must be a corresponding vsbViewportContext object 
    (as there should also be an OpenGL context) 

    This relationship is implied; there is no strict requirement or
    sourc ecode enforcement of this relationship.

    This class is also responsible for creating the static instances 
    of vsbEnvironmentOptions,  vsbRenderOptions, vsbEnvironmentManager, 
    vsbEntityManager, vsbModelManager

*/
///////////////////////////////////////////////////////////////
class DLL_VSB_API vsbViewportContext
{
private:
    
    static int						mInstanceCount;
                                    //!< Number of instances of this class
    
    static vsbEnvironmentOptions	*mspEnvironmentOpts;
                                    //!< Pointer reference to the vsbEnvironmentOptions object
    
    static vsbRenderOptions			*mspRenderOpts;
                                    //!< Pointer reference to the vsbRenderOptions object
    
    static vsbEnvironmentManager	*mspEnvironmentManager;
                                     //!< Pointer reference to the vsbEnvironmentManager object
    
    static vsbModelManager			*mspModelManager;
                                    //!< Pointer reference to the vsbModelManager object

	static vsbChunkLODElevationManager	*mspChunkLODElevationManager;
                                    //!< Pointer reference to the vsbChunkLODElevationManager object
    
    static vsbEntityManager			*mspEntityManager;
                                    //!< Pointer reference to the vsbEntityManager object

    static vsbStateManager          *mspStateManager;
                                    //!< Pointer reference to the vsbStateManager object



    static vsbExplosionFXDirector   *mspExplosionFXDirector;

	static vsbSmokeFXDirector   *mspSmokeFXDirector;

	static vsbHeadTrackerInterface *mspHeadTrackerInterface;
	
	static vsbCompiledGLShapeList   *mspCompiledGLShapeList;
    
    vsbCameraManager				*mpCameraManager;
                                    //!< Pointer reference to its vsbCameraManager object

	
    
    int								mView_W,      //< Associated viewport width
                                    mView_H;      //< Associated viewport height
	float							mView_Aspect; //< Associated viewport aspect ratio

	vsbArcBall						mArcBall;

	static std::set<vsbViewportContext *> msContextList;

	
    
public:
    
                                    //! Default constructor
                                    vsbViewportContext();  
                                    
                                    //! Destructor 
    virtual							~vsbViewportContext(); 
    
	static bool						IsInitialised() {return mspRenderOpts!= 0;};
                                  
                                    //! Initialises the VSB system.
    static void						Init();                                    

                                    /*!<Initialises the VSB system buy creating all the 
                                        supporting class instances if they have not been 
                                        created, namely vsbEnvironmentOptions, 
                                        vsbRenderOptions, vsbEnvironmentManager, 
                                        vsbEntityManager, vsbModelManager. Subsequent calls 
                                        to Init are ignored. */  
    
                                    
                                    //! Close down the VSB system. 
    static void						Close();

                                    /*!<Closes down the VSB system buy deleting all the 
                                        supporting class instances if they have been 
                                        created, namely vsbEnvironmentOptions, 
                                        vsbRenderOptions, vsbEnvironmentManager, 
                                        vsbEntityManager, vsbModelManager. 
                                        
                                        Subsequent calls to vsbViewportContext members
                                        or its referenced dynamic class instances will
                                        result in assertion errors. */ 

    
                                    //! Get a reference to the	environment options object
    static vsbEnvironmentOptions	&EnvironmentOpts()		{return *mspEnvironmentOpts;};
                                    
                                    //! Get a reference to the	render options object
    static vsbRenderOptions	        &RenderOpts() {return *mspRenderOpts;};		
                                    
                                   
                                    //! Get a reference to the	environment manager object 
    static vsbEnvironmentManager	&EnvironmentManager()	{return *mspEnvironmentManager;};
                                    
                                    //! Get a reference to the	entity manager object
    static vsbEntityManager			&EntityManager()		{return *mspEntityManager;};
                                    
                                    //! Get a reference to the	model manager object
    static vsbModelManager			&ModelManager()			{return *mspModelManager;};

									//! Get a reference to the	ChunkedLOD elevation manager object
	static vsbChunkLODElevationManager &ChunkLODElevationManager() {return *mspChunkLODElevationManager;};

                                    //! Get a reference to the	state manager object
    static vsbStateManager			&StateManager()			{return *mspStateManager;};
                                    
                                    //! Get a reference to the	explosion effects director object
    static vsbExplosionFXDirector   &ExplosionFXDirector()  {return *mspExplosionFXDirector;};

	static vsbSmokeFXDirector       &SmokeFXDirector()		{return *mspSmokeFXDirector;};

	static vsbHeadTrackerInterface  &HeadTrackerInterface() {return *mspHeadTrackerInterface;};

	static vsbCompiledGLShapeList   &CompiledGLShapeList()  {return *mspCompiledGLShapeList;};

                                    //! Get a reference to the camera manager object
    vsbCameraManager				&CameraManager() const	{return *mpCameraManager;};
                                     
                                    //! Get a reference to the currently active camera from this view 
                                    //! context instance's camera manager 
   vsbBaseCamera					&ActiveCamera() const;
                                    
									//! Get a reference to the currently active camera from this view 
                                    //! context instance's arcBall 
   vsbArcBall						&ArcBall() {return mArcBall;}

   void								ArcBallClick(EArcBallMode arcBallMode, sgVec2 pt);
   void								ArcBallDrag(sgVec2 dragPt);	    

                                    //! Perform the display update on the OpenGL context, including 
                                    //! environment visuals as well as entities in the scene 
    void							RedrawBuffer();
                                    
                                   //! Set the view window dimensions to be used in rendering calculations
    void							ViewportShape(int w, int h)		{mView_W = w; 
																	 mView_H = h; 
																	 mView_Aspect = float(w)/float(h);
																	 mArcBall.RecalcGeometry(0, 0, float(w), float(h));};
 
                                   //! Get the last set viewport width
    int								ViewportWidth() const			{return mView_W;};
 
                                    //! Get the last set viewport height
    int								ViewportHeight() const			{return mView_H;};

	//! Update the context list, including camera updates for current time
	static void UpdateContext();

    
};

DLL_VSB_API void vsbToScaledVSBCoords(sgVec3 xyz, sgVec3 hpr);
DLL_VSB_API void vsbToScaledVSBCoords(sgVec3 xyz);

inline void vsbToScaledVSBCoords(sgCoord &c) 
{
	vsbToScaledVSBCoords(c.xyz, c.hpr);
};

DLL_VSB_API void vsbToScaledVSBCoords(sgdVec3 xyz, sgdVec3 hpr);
DLL_VSB_API void vsbToScaledVSBCoords(sgdVec3 xyz);

inline void vsbToScaledVSBCoords(sgdCoord &c) 
{
	vsbToScaledVSBCoords(c.xyz, c.hpr);
};


void DLL_VSB_API vsbFromScaledVSBCoords(sgVec3 xyz, sgVec3 hpr);
void DLL_VSB_API vsbFromScaledVSBCoords(sgVec3 xyz);

inline void vsbFromScaledVSBCoords(sgCoord &c) 
{
	vsbFromScaledVSBCoords(c.xyz, c.hpr);
};

void DLL_VSB_API vsbFromScaledVSBCoords(sgdVec3 xyz, sgdVec3 hpr);
void DLL_VSB_API vsbFromScaledVSBCoords(sgdVec3 xyz);

inline void vsbFromScaledVSBCoords(sgdCoord &c) 
{
	vsbFromScaledVSBCoords(c.xyz, c.hpr);
};

#endif