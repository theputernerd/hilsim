// VsbHeadTracker.cpp: implementation 
//
//////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include "VsbHeadTracker.h"

#include <map>
#include <string>


using namespace std;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

vsbHeadTrackerDeviceInterface::vsbHeadTrackerDeviceInterface()
{

}

vsbHeadTrackerDeviceInterface::~vsbHeadTrackerDeviceInterface()
{

}



static map<string, vsbAbstractHeadTrackerDeviceFactory *> tracker_device_factories;


vsbHeadTrackerInterface::vsbHeadTrackerInterface()
{
	m_pCurrTrackerDevice = 0;
}

vsbHeadTrackerInterface::~vsbHeadTrackerInterface()
{
	DestroyFactories();
}

bool vsbHeadTrackerInterface::RegisterHeadTrackerDeviceFactory(const string &entityType, vsbAbstractHeadTrackerDeviceFactory *deviceInterfaceFactory)
{
    pair <map<string, vsbAbstractHeadTrackerDeviceFactory *>::iterator, bool> res;
	tracker_device_factories.insert(pair<string, vsbAbstractHeadTrackerDeviceFactory *>(entityType, deviceInterfaceFactory));
	return res.second;
}

void vsbHeadTrackerInterface::DestroyFactories()
{
	map<string, vsbAbstractHeadTrackerDeviceFactory *>::iterator iter;
	for (iter = tracker_device_factories.begin(); iter != tracker_device_factories.end(); iter++)
	{
		delete iter->second;
	}
	tracker_device_factories.clear();

}


bool vsbHeadTrackerInterface::SelectNewTracker(const std::string &name)
{
	if (m_pCurrTrackerDevice)
	{
		m_pCurrTrackerDevice->Stop();
		delete m_pCurrTrackerDevice;
		m_pCurrTrackerDevice = 0;
	}

	map<string, vsbAbstractHeadTrackerDeviceFactory *>::iterator iter;
	iter = tracker_device_factories.find(name);
	if (iter!= tracker_device_factories.end())
	{
		m_pCurrTrackerDevice = iter->second->Create();
	}
	return (m_pCurrTrackerDevice!= 0);

}


bool vsbHeadTrackerInterface::SelectNewTracker(int idx)
{
	if (m_pCurrTrackerDevice)
	{
		m_pCurrTrackerDevice->Stop();
		delete m_pCurrTrackerDevice;
		m_pCurrTrackerDevice = 0;
	}

	int count = AvailableDrivers();
	if (idx < 0 || idx >= count) return false;
	int i;
	map<string, vsbAbstractHeadTrackerDeviceFactory *>::iterator iter;
	for(i=0, iter = tracker_device_factories.begin(); i<idx; iter++, i++);
	  return SelectNewTracker(iter->first);

}

bool vsbHeadTrackerInterface::DriverInstalled()
{
	return (m_pCurrTrackerDevice!= 0);
}

bool vsbHeadTrackerInterface::DriverIsActive()
{
	if (!DriverInstalled())
		return false;
	return 
		m_pCurrTrackerDevice->IsActive();
}

const string & vsbHeadTrackerInterface::DriverIdString()
{
	static string deflt = "No Head Tracking";
	if (!DriverInstalled())
		return deflt;
	return 	m_pCurrTrackerDevice->DriverIdString();
}

std::string vsbHeadTrackerInterface::GetStatusString()
{
	static string deflt = "No head tracking interface Driver installed.";
	if (!DriverInstalled())
		return deflt;
	return 	m_pCurrTrackerDevice->GetStatusString();
}

bool vsbHeadTrackerInterface::Start()
{
	if (!DriverInstalled())
		return false;
	else 
		return m_pCurrTrackerDevice->Start();

}

void vsbHeadTrackerInterface::Stop()
{
	if (!DriverInstalled())
		return;
	m_pCurrTrackerDevice->Stop();
}

void vsbHeadTrackerInterface::ResetAngles()
{
	if (!DriverInstalled())
		return;
	m_pCurrTrackerDevice->ResetAngles();
}

void vsbHeadTrackerInterface::GetAngles(sgVec3 hpr)
{
	if (!DriverInstalled())
		sgSetVec3(hpr,0,0,0);
	else
		m_pCurrTrackerDevice->GetAngles(hpr);

}


int	vsbHeadTrackerInterface::AvailableDrivers()
{
	return tracker_device_factories.size();
}

const string &vsbHeadTrackerInterface::DriverName(int idx)
{
	static string dflt = "";
	int count = AvailableDrivers();
	if (idx < 0 || idx >= count) return dflt;
	map<string, vsbAbstractHeadTrackerDeviceFactory *>::iterator iter;
	int i;
	for(i=0, iter = tracker_device_factories.begin(); i<idx; iter++, i++);
	  return iter->first;

}	


