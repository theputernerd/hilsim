#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbTrajEntitySTLMap.cpp
    \brief Defines an entity class derived from vsbTrajEntity that has the 
	ability to retain a temporal state history
	This retention is acheived via STL maps.

	Pros:
	Data points can be inserted in random temporal order
	Distance between points can vary

	Cons:
	Access is relatively slow as each point is subject to binary search
	Adding new points is expensive as the STL map rebalances itself

 
   	\sa vsbTrajEntity.cpp

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#include <string>
#include <set>
#include <iostream>
#include <assert.h>

using namespace std;

#include "ssg.h"
#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbModelRoot.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"
#include "vsbInterpEuler.h"
#include "vsbTrajEntitySTLMap.h"


/////////////////////////////////////////////////////////////////

const std::string &vsbTrajEntitySTLMap::ClassName() const
{
	static string class_name = "vsbTrajEntitySTLMap";
	return class_name;
}


vsbTrajEntitySTLMap::vsbTrajEntitySTLMap(const string &name, bool postfixed):
vsbTrajEntity(name,postfixed)
{
	mTypeID = vsbTypeTrajEntityMap(); // initialise the type id
}

vsbTrajEntitySTLMap::~vsbTrajEntitySTLMap()
{

}

void vsbTrajEntitySTLMap::ClearTraj()
{
	mTraj.clear();
}

bool vsbTrajEntitySTLMap::InterpolateAt(float timeto)
{
	
	if (mLowerIter == mTraj.end())  // there is no data or iterators are invalid
    {
		return false;
	}

	if (mUpperIter == mTraj.end() ||    // At the end of the list
		!mInterpolate ||				// Interpolation is disabled
		timeto <= mLowerIter->first		// request time before iterator time segment
		)	             
	{			
		//cout << "Timeto: " << timeto << " Lower Iter: " << mLowerIter->first;	
		mCurrData = mLowerIter->second;
		return true;
	}

	if (timeto >= mUpperIter->first)  // request time after iterator time segment
	{
		mCurrData = mUpperIter->second;
		return true;
	}

	// Interpolate now
	float proportion;   // between 0 and 1.0


	proportion = (timeto - mLowerIter->first)/(mUpperIter->first - mLowerIter->first);
	mCurrData = mLowerIter->second.Interpolate(mUpperIter->second, proportion, mTrajVarDef.InterpStyle()); 

	return true;
}



void vsbTrajEntitySTLMap::SetCurrTime(float timeto)
{
//	int static ii;
//	if (ii++>2) return;

	if( mTraj.begin() == mTraj.end() )
		// There is no traj data in this entity yet.
		return;

	mLowerIter = mUpperIter = mTraj.upper_bound(timeto);

	// if timeto > last time in traj, upper_bound returns (end)
	// if timeto < first time in traj, upper_bound returns (begin)
	// otherwise returns the iterator with the next time within the
	// time interval enclosing the requested time, which must be
	// interpolated.

	if (mLowerIter != mTraj.end())
	{
		// create the interpolation interval ie, mLowerIter being the
		// next time before the requested time, and mUpperIter being
		// the next time above the requested time
		
		mLowerIter--;

		if (mLowerIter == mTraj.end())  // time corresponds to first point	
		{
			mLowerIter = mTraj.begin();  // set interval to be the first interval
			mUpperIter++ = mLowerIter;
		}
	} else								// requested time beyond the last time interval
	{
		mLowerIter-- = mTraj.end();		// set interval to be the last interval	
		mUpperIter = mLowerIter--;
	}


	if (!InterpolateAt(timeto))
	{
		mLowerIter = mTraj.begin();
		if (mLowerIter == mTraj.end()) // This condition arises if there is no data
		{
			mCurrTime = timeto;
			mTrajVarDef.UpdateInstanceData(mCurrData);
			return;
		}
		mCurrData = mLowerIter->second;
	}

	mCurrTime = timeto;

	// Set entity's variables @@@@@@@@@@@@@@@@@@@@@@
	
	mTrajVarDef.UpdateInstanceData(mCurrData);

	CheckBounds();
	
}

void vsbTrajEntitySTLMap::AdvanceTime(float deltaTime)
{
	float timeto = CurrTime() + deltaTime;

	if (mLowerIter == mTraj.end() || mUpperIter == mTraj.end())
	{
		SetCurrTime(timeto);
		return;
	} 
	
	if (timeto < mLowerIter->first)
	{
		mLowerIter--;
		mUpperIter--;
		AdvanceTime(deltaTime);
		return;
	} 

	if (timeto >= mUpperIter->first)
	{
		mLowerIter++;
		mUpperIter++;
		AdvanceTime(deltaTime);
		return;
	}
	
	if (!InterpolateAt(timeto))
	{
		mLowerIter = mTraj.begin();
		if (mLowerIter == mTraj.end())
			// no data
			return;
		mCurrData = mLowerIter->second;
	}

	mCurrTime = timeto;

	// Set entity's variables @@@@@@@@@@@@@@@@@@@@@@
	
	mTrajVarDef.UpdateInstanceData(mCurrData);

	CheckBounds();

}

bool vsbTrajEntitySTLMap::AddVar(const string &varname, double *addr, EInterpolationStyle interpStyle )
{
	if (mTraj.size()>0) return false;  // Mustn't be done whilst data is present in the trajectory
	return vsbTrajEntity::AddVar( varname, addr,  interpStyle );
}


bool vsbTrajEntitySTLMap::AddNewTime(float atTime, bool updateFromState)
{
	map<float, vsbInstanceData>::iterator found;

	mCurrTime = atTime;
	if (updateFromState) UpdateInstanceDataFromState();
/*	if (!HaveOrientation() && AutoOrientation())
	{
		if (mTraj.size()<1) return true;
		// generate automatic orientation
		//@@ TO BE IMPLEMENTED:
	}
*/

	found = mTraj.find(atTime);
	if (found != mTraj.end()) 
	{
		found->second = mCurrData; // If data for that time exists over-write it
	} else
		mTraj.insert(pair<float, vsbInstanceData>(atTime,mCurrData)); // insert the new data


	return true;

}


bool	vsbTrajEntitySTLMap::FirstInstanceData(vsbInstanceData &data)
{
	mSteppingIter = mTraj.begin();
	if (mSteppingIter == mTraj.end())
		return false;
	data = mSteppingIter->second;
	return true;
}

bool	vsbTrajEntitySTLMap::LastInstanceData(vsbInstanceData &data)
{
	mSteppingIter = mTraj.end();
	mSteppingIter--;
	if (mSteppingIter == mTraj.end())
		return false;
	data = mSteppingIter->second;
	return true;
}

bool	vsbTrajEntitySTLMap::NextInstanceData(vsbInstanceData &data)
{
	mSteppingIter++;
	if (mSteppingIter == mTraj.end())
		return false;
	data = mSteppingIter->second;
	return true;
}

bool	vsbTrajEntitySTLMap::PrevInstanceData(vsbInstanceData &data)
{
	mSteppingIter++;
	if (mSteppingIter == mTraj.end())
		return false;
	data = mSteppingIter->second;
	return true;
}