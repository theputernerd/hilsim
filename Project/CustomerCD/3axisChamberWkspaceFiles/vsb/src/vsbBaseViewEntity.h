////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseViewEntity.h
    \brief Defines the class vsbBaseViewEntity and itas auxillary classes
    
     The class vsbBaseEntity is the  abstract base class for all entities
	 in the 3D scene, providing the basic funtionality necessary to to be 
	 managed by the entity manager.
	
	\sa vsbBaseViewEntity.cpp

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////


#ifndef _vsbBaseViewEntity_H
#define _vsbBaseViewEntity_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _SET_
#include <set>
#define _SET_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _LIST_
#include <list>
#define _LIST_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbAttachmentPoint_H
#include "vsbAttachmentPoint.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif




#define _BV_BASE_ENTITY			0x00000001
inline int vsbTypeBaseEntity       () { return _BV_BASE_ENTITY ; }



/*! 
   \brief EOutOfRange behaviour defines what the entity does when the requested
   time-state is invalid, ie if it is requested to draw at a time that 
   exceedis its valid time ranges. 
   \note Range is not related to position but rather time

   \note Not used for the moment
*/
enum EOutOfRangeBehavior
{
	EOR_Default,			//!< The entity is drawn at the nearest limit
	EOR_Invisible,			//!< The entity is invisible when its is out of range
	EOR_Custom,				//!< User defined behaviour
	EOR_Delete,				//!< Delete the entity from the object manager 
	
};

/*! 
  \brief EEntityStatus defines an enumeration reflecting the status of an entity
  after constructor is called
  \sa vsbEntityException, vsbEntityManager::EntityStatus
*/
enum EEntityStatus			
{
	ES_OK,					    //!< Construction successful
	ES_NameClash,			    //!< Name clash during construction, already exists?
	ES_TableSaturated,		    //!< Hash tables cannot hold any more data
	ES_NoEntityManager,		    //!< No valid entity manager was supplied
	ES_EntityManagerAddFailure, //!< Failure attempting to add Entity into the vsbEntityManager list 
	ES_GeneralInitFailure,      //!< General initialisation failure    
};


/*! 
  \brief EEntityTeam defines an enumeration reflecting a notional team association
*/
enum EEntityTeam			
{
	ETeam_Blue,		
	ETeam_Red,			    
	ETeam_Yellow,		   
	ETeam_Green,		  
	ETeam_Magenta
};

enum ETrajDisplayStatus			
{
	ETrajectory_Off,		//!< Trajectory is never to be displayed
	ETrajectory_On,			//!< Trajectory to always be displayed if available   
	ETrajectory_Default		/*!< \brief Trajectory to be displayed if available and  
                                 default is on
                               
                                 /sa vsbEnvironmentOptions::mTrajectoryTraceOn
                            */
                            
};


/*!
  \class vsbEntityException
  \brief Exception class for reporting entity creation errors.
  \note These exceptions are thrown by the constructors of vsbBaseViewEntity derived classes
*/

class DLL_VSB_API vsbEntityException 
{
public:
	 //! Constructor
     vsbEntityException(EEntityStatus someStatus) { mStatus  = someStatus; };

	 /*! \brief Get the exception status

	 The status returned indicates the reason for the exception
	 /sa EEntityStatus
	 */
	 EEntityStatus	Status() const				  { return mStatus;};
private:

     EEntityStatus	mStatus;
} ;


class DLL_VSB_API vsbModelRoot;


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbBaseViewEntity
    \brief The base class from which all entities are derrived
    
     All entities in the 3D scene are derrived from vsbBaseViewEntity, which 
	 provides the basic funtionality necessary to to be managed by the entity 
	 manager.

  \sa vsbEntityManager

*/
/////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbBaseViewEntity
{
friend class DLL_VSB_API vsbEntityManager;
friend class DLL_VSB_API vsbModelManager;

public: //////////////////////// Public Interface ///////////////////////////////


					/*! \brief Contructor for BaseView entities
					    \param em Pointer to a valid Entity Manager
						\param name A unique logical name for the entity, OR the prefix name (see next param)
						\param postfixed A boolean indicating that a unique numeric value is to bb appended to 
							   the name specified above, making it unique

						<b>Default attribute settings are as follows:</b> 
							- mHaveOrientation	= false   
							- mHaveVelocity		= false		
							- mHaveAngularRates	= false
 							- mIsVisible		= false		
							- mIsActive			= false		
							- mIsMoving			= false	
							- mDoPostProcess	= false
							- mDeleteNextPass	= false
							- mOutOfRangeBehavior[] = EOR_Invisible, EOR_Invisible
							- mCurrTime			= 0
							- mMinTime			= - Effective infinite range
							- mMaxTime			= + Effective infinite range
							- mEntityPosXYZ		= <empty>
							- mOrientationHPR	= <empty>
							- mVelocityXYZ		= <empty>
							- mAngularRatesHPR	= <empty>

						<i>(to be set appropriately by derived classes)</i>
					*/
					vsbBaseViewEntity(/*vsbEntityManager *em,*/ const std::string &name, bool postfixed = false);


					/*! \brief Virtual destructor
					*/
	virtual			~vsbBaseViewEntity();


					/*! \brief Attach a 3D model to this entity
					\param modelName the logical name of the model to attach
					\param clear Indicates whether any previous models are to be detached

					The Attach Detach mechanism is closely coupled with the vsbModelManager
					which is responsible for dynamic loadign and sharing of 3D models.

					\sa vsbModelManager
					*/

					/*! \brief Get the string name of the class of entity
					*/
	virtual const std::string &ClassName() const;

	virtual bool	AttachModel(const std::string &modelName, bool clear = true);

					/*! \brief Detach a 3D model from this entity
					\param modelName the logical name of the model to detach
					
					The Attach Detach mechanism is closely coupled with the vsbModelManager
					which is responsible for dynamic loadign and sharing of 3D models.

					\sa vsbModelManager
					*/
	virtual bool	DetachModel(const std::string &modelName);

					/*! \brief Detach all 3D models from this entity
					*/
	virtual void	DetachAllModels();

					/*! \brief Get the logical name of the model attached to this entity 
					\param index The offset index of the model in question within the list
					of attached models for this entity
					*/
	virtual const std::string    &AttachedModelName(int index) ;

/** @name Attribute Enquire Methodss 
*/
//!
//@{

					/*! \brief Get the entity's out of range behavour
						\return EOutOfRangeBehavior
					*/	
	EOutOfRangeBehavior OutOfRangeBehavior(int rangeEnd) { return mOutOfRangeBehavior[rangeEnd]; };

					//! Get the entity's root scene graph node
	vsbModelRoot    *EntityRoot() {return mEntityRoot;};
 
					/*! \brief Get the status result after constructor is called
						\sa EEntityStatus
					*/
	EEntityStatus	EntityStatus() const				{return mEntityStatus; };

					/*! \brief Get the entity's name Entity's name
					  \note Can only be set in the constructor
					*/

	const std::string &  Name() const						{return mName; };

	const int		TypeId() const {return mTypeID; };


					/*! \brief Get the entity's unique entity handle. 

					The handle is generated by a hash func and functions as a fast lookup index
					It can be used to directly look up an entity in the entity list 
					\sa mHandle
					*/
	unsigned		Handle() const						{return mHandle;};	

					// The following Inquire functions are used to test the availability of
					// the corresponding data

					/*! \brief Test the avilability of orientation data for this entity
					\sa mHaveOrientation
					*/
	bool			HaveOrientation() const				{ return mHaveOrientation; }; 

					/*! \brief Test the avilability of velocity data for this entity
					\sa mHaveVelocity
					*/
	bool			HaveVelocity() const				{ return mHaveVelocity; }; 	

					/*! \brief Test the avilability of angular rate data for this entity
					\sa mHaveAngularRates
					*/
	bool			HaveAngularRates() const			{ return mHaveAngularRates; };	

					// Inquire entity status functions

					/*! \brief Test whether the entity is designated as visible
					\sa mIsVisible
					*/
	virtual bool	IsVisible() const					{ return mIsVisible; };	

					/*! \brief Test whether the entity is designated as active
					\sa mIsActive
					*/
	virtual bool	IsActive() const					{ return mIsActive; };

					/*! \brief Test whether the entity is designated as moving
					\sa mIsMoving
					*/
	virtual bool	IsMoving() const					{ return mIsMoving; };

					/*! \brief Set invisibility status
					\sa mIsVisible
					*/
	virtual void	IsVisible(bool trueFalse);

					/*! \brief Set activity status
					\sa mIsVisible
					*/
	void			IsActive(bool trueFalse);


					/*! \brief Get the entity's current time 
					*/
	float			CurrTime() const			{ return mCurrTime; }; 

					/*! \brief Get the entity's minimum valid time 
					\sa mMinTime, mMaxTime
					*/
	float			MinTime() const				{ return mMinTime;  };

					/*! \brief Get the entity's maximum valid time 
					\sa mMinTime, mMaxTime
					*/
	float			MaxTime() const				{ return mMaxTime;	};

	
					/*! \brief Get the entity's current position vector
					*/
	sgdVec3&			EntityPosXYZ()			{ return mEntityPosXYZ; };
	const sgdVec3&	EntityPosXYZ() const	{ return const_cast< vsbBaseViewEntity *>(this)->EntityPosXYZ(); }

					/*! \brief Get the entity's current orientation vector
					*/
	sgdVec3&			OrientationHPR()		{ return mOrientationHPR; };
	const sgdVec3&	OrientationHPR() const	{ return const_cast< vsbBaseViewEntity *>(this)->OrientationHPR(); }

					/*! \brief Get the entity's current velocity vector
					*/
	sgdVec3&			VelocityXYZ()			{ return mVelocityXYZ; };
	const sgdVec3&	VelocityXYZ() const		{ return const_cast< vsbBaseViewEntity *>(this)->VelocityXYZ(); }

                    /*! \brief Get the entity's current team association
					*/
    EEntityTeam     Team() const {return mTeam;}

                    /*! \brief Get the trajectory display status

                    If this entity has a historical trajectory, this setting
                    tells the entity whether it should be depicted.

                    /sa ETrajDisplayStatus
					*/
    ETrajDisplayStatus TrajDisplay() const { return mTrajDisplay;  };    

//@}


/** @name Attribute Set Functions 
*/
//@{

					/*! \brief Set the entity's out of range behavour
						\param behaviour The out required out of range behaviour
						\sa mOutOfRangeBehavior

						This setting decides how the entity responds to a request to update to
						a time instant that is out of its valid range. 

						if is set to EOR_UserBehavior, the TimeBoundsExceededFunc is called which
						implements an appropriate response behaviour
					*/
	void			OutOfRangeBehavior(int rangeEnd, EOutOfRangeBehavior behaviour)  {mOutOfRangeBehavior[rangeEnd] = behaviour;};	


					/*! \brief Set the entity's minumum valid time 
					\sa mMinTime, mMaxTime
					*/
	void			MinTime(float min) 			{ mMinTime = min; };

					/*! \brief Set the entity's maxiumum valid time 
					\sa mMinTime, mMaxTime
					*/
	void			MaxTime(float max) 			{ mMaxTime = max; };

					/*! \brief Set the Entity's valid time range
					\sa mMinTime, mMaxTime
					*/
	void			MinMaxTime(float min, float max) {mMinTime = min; mMaxTime = max;};

					/*! \brief Set the time range to effectively infinite
					\sa mMinTime, mMaxTime
					*/
	void			InfiniteTimeRange()			{ mMinTime = -1.0e-10f; mMaxTime = 1.0e10f;};


                    /*! \brief Set the entity's current team association
					*/
    void            Team(EEntityTeam team) {mTeam = team;};

                    /*! \brief Set the trajectory display status
					*/
    void            TrajDisplay(ETrajDisplayStatus trajStatus) {  mTrajDisplay = trajStatus;  };    

	bool			LimitedLifecycle() const { return mLimitedLifecycle; };
	void			LimitedLifecycle(bool onOrOff) { mLimitedLifecycle = onOrOff; };

	void			ForceInvisible(bool onOrOff) {mForceInvisible = onOrOff; };
	bool			ForceInvisible() const {return mForceInvisible;};

	bool			AttachmentPointName(int pos_id, std::string &pos_name);
	bool			AttachmentPointExists(int pos_id);
	bool			AttachmentPointExists(std::string &pos_name);
	bool			GetAttachmentPointPos(sgdVec3 pos, int pos_id);
	bool			GetAttachmentPointPos(sgdVec3 pos, std::string &pos_name);

	bool			IsATypeOf(int typeId);
	bool			IsA(int typeId);
	bool			IsThisType(vsbBaseViewEntity *entity);
	bool			IsATypeOfThis(vsbBaseViewEntity *entity);

    vsbBaseViewEntity	*Next()	const			{ return mpNext;  };	
	vsbBaseViewEntity	*Prev()	const			{ return mpPrev;  };

//@}
	

protected: /////////////////////// ACCESSED INTERNALLY //////////////////////////

	double			mCurrTime;			//!< The current instance time of the object
	float			mMinTime;			//!< Minimum time of this entity's activity
	float			mMaxTime;			//!< Minimum time of this entity's activity	

	sgdVec3			mEntityPosXYZ;		//!< The position of the object			| Must appear
	sgdVec3			mOrientationHPR;	//!< The orientation Heading Pitch Roll	| together

	sgdVec3			mVelocityXYZ;		//!< The linear velocity
	sgdVec3			mAngularRatesHPR;	//!< Rates of rotation 

	bool			mHaveOrientation;   //!< Flags indicating availability of Orientation
	bool			mHaveVelocity;		//!< Flags indicating availability of Velocitiy
	bool			mHaveAngularRates;	//!< Flags indicating availability of AngularRates

	bool			mIsVisible;			//!< Flag indicating whether entity is visible or not (st by user)
	bool			mIsActive;			//!< Flag indicating whether entity is active (set internally)
	bool			mIsMoving;			//!< Flag indicating whether entity is moving (set internally)
	bool			mForceInvisible;    //!< Flag to override and force entity to invisible

	bool			mDoPostProcess;      //!< Determines if post process is called 
 
	bool			mLimitedLifecycle;

	int				mHandle;			//!< Entity handle;
	std::string		mName;				//!< Entity name
	int				mTypeID;			//!< Entity  type ID

	bool			mDeleteNextPass;	//!< If set to true, the entity is marked to be deleted	

	EOutOfRangeBehavior mOutOfRangeBehavior[2]; //!< Out of range behavour enumeration

    EEntityTeam         mTeam;               //!< Team association

    ETrajDisplayStatus  mTrajDisplay;       //!< TrajectoryDisplayStatus
	

	EEntityStatus		mEntityStatus;		//!< The Entity's current activity status	

	vsbModelRoot		*mEntityRoot;		//!< The entitys root scene graph node

	sgdMat4				mOrientationMat;    //!< Matrix encapsulation entity orientation

	
					/*! \brief Mark the entity for deletion on the next update pass
					\note The entity is not deleted immediately as other entities updates
					may depend on the presensce of this one on this pass 
					*/
	void			DeleteNextPass(bool trueFalse) { mDeleteNextPass = trueFalse; };
	
					/*! \brief Get the deletNextPass status of the entity
					\sa DeleteNextPass
					*/
	bool			DeleteNextPass() { return mDeleteNextPass; };


					/*! \brief Tests whether the post process function is enabled
					
					The PostProcessFunc is called after the following steps in the Entity Manager..
						-# All the entities are updated wrt time. 
						-# All the entities marked for deletion are then deleted. 
						-# All the entities that have mDoPostProcess set have their PostProcessFunc called.

					\sa vsbEntityManager
					*/
	bool			DoPostProcess() const { return mDoPostProcess; };


					/*! \brief Set the flag indicating whether this entity's position changes
					*/
	virtual void	IsMoving(bool trueFalse)			{ mIsMoving = trueFalse;};

	// These functions, designed to be overriden to impart behaviour when object
	// instantaneous time is outside of the object's time span.
	//-----------------------------------------------------------

					/*! \brief Checks the entity's current time against its valid time range taking
						appropriate action as necessary.

						This function may be overridden however it is functional as it stands.
						It defines some predetermined behaviour to respond to the time bounds
						condition of the entity, whic is an enumeration of the type 
						EOutOfRangeBehavior. If the out of time range behaviour is set to
						EOR_UserBehavior, then the virtual function TimeBoundsExceededFunc is
						called to take apropriate action.

						\sa TimeBoundsExceededFunc
					*/
	virtual void	CheckBounds();

					/*! \brief Abstract function implementing user defined Out of time range behavouur
					*/
	virtual	void	TimeBoundsExceededFunc(int rangeEnd, bool trueFalse)	{};

	// Entity state update is done in two passes, update and post-update
	//------------------------------------------------------------------

					/*! \brief Sets the entity's current time and reflecting that time in its state
					\param timeto The time to set the entity at
					Set the entity's current time, and update any neccesary internal state 
					variables as well as perform any functions required by the particular derived
					class for the update
					*/
	virtual void	SetCurrTime(float timeto);

					/*! \brief Advances the entity's current time and reflecting that time in its state
					\param deltaTime The time to increment by

					Update the entity's current time, and update any neccesary internal state 
					variables as well as perform any functions required by the particular derived
					class for the update. Behaviour is similar to SetCurrTime.
					*/
	virtual void	AdvanceTime(float deltaTime);


					/*! \brief Update the scene graph transform for the model from the entity's state
					\note Behaviour generally does not need overiding, as long as mEntityPosXYZ and 
					mOrientationHPR are set in advance.
					*/
	virtual void	UpdateTransforms();


					/*! \brief Function that is called if post processing is enabled
					If mDoPostProcess is set to true, this function is called in the post 
					update pass.

					The derived PostProcessFunc is called after the following steps in the Entity 
					Manager..
						-# All the entities are updated wrt time. 
						-# All the entities marked for deletion are then deleted. 
						-# All the entities that have mDoPostProcess set have their PostProcessFunc called.

					\sa vsbEntityManager
					*/
	virtual void	PostProcessFunc();			//@@ uses entity manager
	

					/*! \brief Create a hash value from the entity name
					\param name Name std::string to hash
					\param maxSize The maximum value of the hash
					*/
	static int		HashValue(const std::string &name, int maxSize) ;

					/*! \brief Set the entity's unique handle ID
					*/
	void			Handle(int someHandle)		{mHandle = someHandle;};

					/*! |brief Convenience function that sets mHaveOrientation, mHaveVelocity and mHaveAngularRates
					Position is always presumed to be available, and by default velosity and angular rates are not 
					*/
	void			DataFields(bool orientation = true, bool velocity = false, bool angRates = false);

 

private: 
	//////////////////////////////////////////////////////////////////////////////////////////
	//                     Base class low level implementation members
	//////////////////////////////////////////////////////////////////////////////////////////   


	std::map<long,vsbEntityAttachmentPoint> mAttachmentPoints;



	vsbBaseViewEntity	*mpNext;			// list links
	vsbBaseViewEntity	*mpPrev;

	void            CopyAttachmentPoints(std::map<long,vsbAttachmentPoint> &attachment_points);
	void			DeleteAllAttachmentPoints();
	void			DeleteAttachmentPoint(std::string attachmentName);
	void			DeleteAttachmentPoint(long attachmentId);
	void			ClearAttachmentPointEvaluatedFlag();
	int				AttachmentCount() {return mAttachmentPoints.size();};
	
	void			ConstructorSetup(const std::string &name); // Constructor helper; common initialisations

	// hash look up table implementation - implemented to enable fast look up of vsbBaseViewEntity objects
	// by name. The object handle is effectively the hash value of the entity name (tho not strictly as
	// there is some hash value collision resolution performed


	void			Next(vsbBaseViewEntity *bv)	{ mpNext = bv; };	
	void			Prev(vsbBaseViewEntity *bv)	{ mpPrev = bv; };

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 

#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

	std::list<std::string>			mModelNames; //!< List of attached models by name

#    ifdef _MSC_VER
#    pragma warning( pop  ) 
#    endif


};




#endif