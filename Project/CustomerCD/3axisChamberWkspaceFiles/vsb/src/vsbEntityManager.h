////////////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbEntityManager.h
    \brief Entity manager is the class that maintains and updates the state of 
    entities in a scene.
    
    It is the interface between the user program, and the  scene graph.

    Entity manager implements a hashtable for fast look-up of entities by name
    The hash value of an entity's name is used as it's handle

    Entity manager also incorporates the EntityFactory for creating entities
    \sa vsbEntityManager.cpp
*/
////////////////////////////////////////////////////////////////////////////////
#ifndef _vsbEntityManager_H
#define _vsbEntityManager_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbModelManager_H
#include "vsbModelManager.h"
#endif

//! Brief Status of the entity manager's attempt to create of a new entity


enum EEntityManagerStatus           
{
    EM_OK,                  //!< Construction successful
    EM_NameClash,           //!< Name clash during construction, already exists?
    EM_TableSaturated,      //!< Hash tables cannot hold any more data
};

class DLL_VSB_API vsbEntityManager;
class DLL_VSB_API vsbViewportContext;


typedef void (*vsbEntityCreatedCallback) ( const char *msg ) ;
typedef void (*vsbEntityRemovedCallback) ( const char *msg ) ;

////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbBaseViewEntityFactory
    \brief  This class defines the pure abstract base factory object
    
    Used to derrive a factory for a specific entity.
*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbBaseViewEntityFactory
{
public:
    /*! \brief Constructor
       \param em Pointer to the entity manager that will host the new entity

       The entity is given a default unique name.
    */
	virtual vsbBaseViewEntity *Create() = 0;

    /*! \brief Constructor
        \param em Pointer to the entity manager that will host the new entity
        \param name The name of the entity
        \param postfixed Flag indicating that a unique integer should be postfixed to the entity name
    */
    virtual vsbBaseViewEntity *Create(const std::string &name, bool postfixed = false) = 0;
};


#ifdef _MSC_VER
#pragma warning( disable : 4101 ) // unreferenced local variable (exc)
#endif 


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class ViewEntityFactory
    \brief View entity factory template class
    
    This template class derives from vsbBaseViewEntityFactory providing a generalised
    entity creator for all possible entities 
*/
////////////////////////////////////////////////////////////////////////////////

template <class T> class DLL_VSB_API ViewEntityFactory : public vsbBaseViewEntityFactory
{
public:

    /*! \brief Constructor
       \param em Pointer to the entity manager that will host the new entity

       The entity is given a default unique name.
    */
	vsbBaseViewEntity *Create()
	{
		try 
		{
			return new T(typeid(T).name(), true);
		} catch (vsbEntityException exc)
		{
			return 0;
		}
	}

    /*! \brief Constructor
        \param em Pointer to the entity manager that will host the new entity
        \param name The name of the entity
        \param postfixed Flag indicating that a unique integer should be postfixed to the entity name
    */
    vsbBaseViewEntity *Create(const std::string &name, bool postfixed = false)
	{
		try 
		{
			return new T(name, postfixed);
		} catch (vsbEntityException exc)
		{
			return 0;
		}
	}

};

#ifdef _MSC_VER
#pragma warning( default : 4101 ) 
#endif 


/*! \brief Defines a macro for registering new entity classes.

    Registers a new entity with the vsbEntityManager's entity factory.
    Once an entity is registered, it may be created by any entity manager
    by simply identifying it by its class name. However, in the current
    implementation only one entity manager is allowed per application.
*/
#define REGISTER_VIEW_ENTITY_FACTORY(viewclass) \
	vsbEntityManager::RegisterViewEntityFactory(#viewclass, new ViewEntityFactory<viewclass>)



////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbEntityManager
    \brief Entity manager class
    
     The class vsbEntityManager is responsible for managing all the entities
     in the scene. Only one scene is allowed and consequently only one entity 
     manager in an application.

*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbEntityManager
{
friend class DLL_VSB_API vsbBaseViewEntity;
friend class DLL_VSB_API vsbViewportContext;
public:

	// Public classes for the world!! 

    /*! \brief Constructor
   
    */
    vsbEntityManager();

    //! Virtual destructor
    ~vsbEntityManager();

							//--------------------------------------------------------------
							// ViewEntity factory related functions
							//---------------------------------------------------------------

							/*!  \brief Register a View Entity factory
                             Function to register entity factories with the Entity Manager
							 The correct procedure is; 
                             \code RegisterViewEntityFactory("SomeEntity", new ViewEntityFactory<SomeEntityClass>)
                            */

    static bool				RegisterViewEntityFactory(const std::string &entityTypeName, vsbBaseViewEntityFactory *entityFactory); 

							/*! \brief De-register entity factory from the Entity Manager
                            \param entityTypeName The name of the entity
                            */
    static void				DeregisterViewEntityFactory(const std::string &entityTypeName);

                            /*! \brief De-register all entity factories from the Entity Manager
                            */
	static void				DeregisterAllViewEntityFactories();

							/*! \brief Create an entity of a specified type and name 

							 The entity type must have been registered with the Register
                            */

    vsbBaseViewEntity *		CreateViewEntity(const std::string &entityTypeName, const std::string &entityName); 

                            /*! \brief Create an entity of a specified type and name 

						    A default name is set by the factory. The entity type must have been 
                            registered with the Register
                            */    
    vsbBaseViewEntity *		CreateViewEntity(const std::string &entityTypeName); 

	
							/* register a call back function called when entity is added */
							void RegisterAddCallback(vsbEntityCreatedCallback pAddedFunc)		{mpEntityAddedFunc = pAddedFunc;};

							/* register a call back function called when entity is removed */
							void RegisterRemoveCallback(vsbEntityRemovedCallback pRemovedFunc)	{mpEntityRemovedFunc = pRemovedFunc;};

							//---------------------------------------------------------------

							/*!
                             \brief Update the entity's time instance to the specified time

							  SetCurrTime does a binary Search within the stored trajectory
							  and saves that traj point in an internal iterator

						      \note The SetCurrTime function should be called once at the beginning 
                              of animation and subsequent advancement should be done with AdvanceTime 
                              which is more efficient.
                                
                              \param timeTo The time instant to setthe entity to 
							  
                            */
    void                    SetCurrTime(float timeTo);

                            /*!
                             \brief Advance the entity's time instance by the specified time increment
						      
							  AdvanceTime uses the iterator as a starting point.

                              \param deltaTime The time to increment the intity's time instant by   
                            */
	void                    AdvanceTime(float deltaTime);

                            //! Return the current time instance of the entity
	float					CurrTime() { return mCurrTime; };

                            /*! \brief Notify of expected number of entities to prepare hash table in 
							 advance - ie. hashtable is resized appropriately
                             \param n The number of entities to expect
                            */
    void                    EntityCountHint(int n);
    
                            /*! \brief Enable/disable auto hash table resizing. 
                            
                            If the hashtable resizes handle id's of all Entities changes since handle id 
                            == unique hash value of entity's name. 
                            */
    void                    AutoHashtableResize(bool trueFalse) {mAutoHashResize = trueFalse;};

                            //! Enquire about the AutoHashtableResize status
    bool                    AutoHashtableResize()               {return mAutoHashResize;};

							//! Get the number of entities currently managed
    int                     EntityCount() const {return mEntityCount;};

                            
                            /*! \brief Look-up an entity by its handle id
                            \returns NULL if entity not found, a pointer to the entity therwise
                            */
	vsbBaseViewEntity *     LookupEntity(int handle) ;

                            /*! \brief Look-up an entity by its logical name
                             \returns NULL if entity not found, a pointer to the entity therwise
                            */
    vsbBaseViewEntity *     LookupEntity(const std::string &name) ;

							/*! \brief Look up an Entity's handle by its name in the hash table 
							 \returns The entity handle or the address of the next free cell if 
                             the entity was not found (given an address has been supplied
							 to store it in the nextFree index). 
                             \note This function performs a hashtable look-up. It updates the
                             mLookUpCollisions member with the number of hashtable collisions
                             resulting from the look-up
                            */

    int                     LookupEntityHandle(const std::string &name, int *nextFree=0) ;

                            //! Get the first entity in the entity list
	vsbBaseViewEntity *     Head() const { return mpHead; };

                            //! Get the collision count from the last look-up
    int						LookupCollisions() const {return mLookUpCollisions; };

							//! Function to seek and destroy BaseViewEntities by handle id
    void                    DestroyEntity(int entityHandle);

                            //! Function to seek and destroy BaseViewEntities by entity name
    void                    DestroyEntity(const std::string &entityName);

                            //!  Destroy all managed entities
    void                    DestroyAll();

	int						GenerateHandle(std::string entityName);

protected:

	// Protected members are for internal use, and for the use of vsbBaseViewEntity friend class

 				
							// The current size of the hash table 

                            //! Get the hashtable size
	int                     HashSize() {return msHashTableSizes[mHashTableSizeIndex];};

                            //! Add an entity into the entity manager
    EEntityManagerStatus    AddEntity(vsbBaseViewEntity *newEntity);

                            //! Remove an entity from the entity manager
	void					RemoveEntity(vsbBaseViewEntity *newEntity);
 
							//! Utility function to associate a vsbBaseViewEntity with a hash value 
    void                    SetEntityIdHandle(int entityHandle, vsbBaseViewEntity  *viewEntity);

							//! Utility function to clear free an vsbBaseViewEntity association for a hash value
    void                    ClearEntityIdHandle(int entityHandle);

						    //! Utility function to assign a handle id to an entity
	int						AssignEntityHandle(vsbBaseViewEntity *entity);




private:
	// Note the friend class vsbBaseViewEntity should not access the following members directly
	// but rather through the above protected member functions. These members are STRICTLY for
	// the internal use of Entity manager

    int                     mEntityCount;				//!< The number of entities currently managed

    vsbBaseViewEntity *     mpHead;					    //!< Points to the l-list of vsbBaseViewEntity derived objects

    int                     mHashSize;                  //!< Current size of hash table
    vsbBaseViewEntity **    mpHashTable;                //!< Hash table of pointers to vsbBaseViewEntity

    static int              msHashTableSizes[];         //!< Array of valid sizes a hash table can have
    static const int        msHashTableSizeIndexMax;    //!< The number of entries in sHashTableSizes
    int                     mHashTableSizeIndex;        //!< Index into above array defining curr hash table size 

    bool                    mAutoHashResize;            /*!< If True, hash table automatically grows at saturation
                                                          caveat: all entity handles change when this happens
                                                          so enable with caution    
                                                         */

    bool                    HashTableResize(bool force = false); /*!< Resize the hash table to the next biggest size
                                                         based on the sHashTableSizeIndex and the
                                                         sHashTableSizes[] array - behaviour subject to mAutoHashResize
                                                        */


	static int				mInstanceCount; //!< The number of instances of this object

	float					mCurrTime;  //!< the current time state
	
	int						mLookUpCollisions;  //!< Number of collisions occured looking up an entity

                           /*! \brief Update the scene graph transform for the model from the entity's state
					        \note Behaviour generally does not need overiding, as long as mEntityPosXYZ and 
					        mOrientationHPR are set in advance.
					        */

	vsbEntityCreatedCallback mpEntityAddedFunc;
	vsbEntityRemovedCallback mpEntityRemovedFunc;
	void					UpdateTransforms(); 

};

#endif
