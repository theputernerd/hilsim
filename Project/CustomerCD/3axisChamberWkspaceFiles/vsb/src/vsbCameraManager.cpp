///////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbCameraManager.cpp
    \brief Implements the class vsbCameraManager and itas auxillary classes
    
     The class vsbCameraManager is responsible for managing all the cameras
     in a vsbViewContext. 
	 
	\sa vsbCameraManager.h

    \author T. Gouthas
*/
///////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <string>
#include <map>
#include <assert.h>


#include "ssg.h"
#include "vsbEntityManager.h"
#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"
#include "vsbSimpleCamera.h"

using namespace std;
using namespace Parser;

map<string, vsbAbstractCameraCreator *> vsbCameraManager::mCameraCreators;


long  vsbCameraManager::DefaultCameraId()
{
    static long cam_id = vsbBaseCamera::HashValue(DefaultCameraName());
    return cam_id;
}

std::string&  vsbCameraManager::DefaultCameraName()
{
    static string cam_name = "DefaultCamera";
    return cam_name;
}

vsbCameraManager::vsbCameraManager()
{
	mCameraInstances.clear();
	assert(mCameraCreators.size());
	InsertDefaultCamera();
    mActivationMode = CM_MANUAL;
	mpCameraSchedule = new vsbCameraSchedule(this);
}


void vsbCameraManager::InsertDefaultCamera()
{
	if (FindCam("DefaultCamera")) return;
	mpActiveCamera = Create("vsbFollowCamera", DefaultCameraName());

}

vsbCameraManager::~vsbCameraManager()
{
	delete mpCameraSchedule;
	DeleteAllCameras();
}

bool vsbCameraManager::RegisterCameraCreator(const string &camClassName, vsbAbstractCameraCreator *camClassCreator)
{
	static int insertion_index = 0;
	camClassCreator->SetName(camClassName);
	camClassCreator->SetInsertionIndex(insertion_index++);

	pair<map<string, vsbAbstractCameraCreator *>::iterator, bool> res;

	res = mCameraCreators.insert(pair<string, vsbAbstractCameraCreator *>(camClassName, camClassCreator));
	return res.second;
}

void vsbCameraManager::DestroyCameraCreators()
{
	map<string, vsbAbstractCameraCreator *>::iterator iter; 
	for (iter = mCameraCreators.begin(); iter!= mCameraCreators.end(); iter++)
    {
       delete iter->second;   
    }
	mCameraCreators.clear();
}


vsbBaseCamera *vsbCameraManager::Create(const string &cammClassName, const string &instanceName)
{
	map<string, vsbAbstractCameraCreator *>::iterator p; 
	vsbBaseCamera *new_cam;

    p = mCameraCreators.find(cammClassName);
	


	if (p == mCameraCreators.end()) 
		return 0;


	vsbAbstractCameraCreator *cameraCreator = p->second;
	if (!cameraCreator) 
		return 0;


	if (instanceName!="")
	{
		vsbBaseCamera *found_cam = FindCam(instanceName);
		if (found_cam) 
			return 0; 
	}

	if (instanceName=="")
		new_cam = cameraCreator->Create(this);
	else
		new_cam = cameraCreator->Create(this, instanceName);

//	new_cam->ClassName(cammClassName);
	return new_cam;
}


int  vsbCameraManager::GetCameraFactoryClassCount()
{
    return mCameraCreators.size();
}

bool  vsbCameraManager::GetCameraFactoryClassName(int index, std::string &name)
{
    int i=0;
    map<std::string, vsbAbstractCameraCreator *>::iterator iter;
    for (iter = mCameraCreators.begin(); iter!= mCameraCreators.end(); iter++, i++)
    {
        if (i==index)
        {
             name = iter->first;   
             return true;
        }
    }
    return false;
}


int vsbCameraManager::GetCameraFactoryClassInsertionIndex(const std::string &name)
{
	map<std::string, vsbAbstractCameraCreator *>::iterator iter;
	iter = mCameraCreators.find(name);
	if (iter == mCameraCreators.end()) return -1;
	return iter->second->InsertionIndex();

}

vsbBaseCamera *vsbCameraManager::FindCam(const string &camName)
{
	long hash_value = vsbBaseCamera::HashValue(camName);
	return FindCam(hash_value);
}

vsbBaseCamera *vsbCameraManager::FindCam(long handle)
{
	if(mCameraInstances.empty()) 
		return 0;
	mCamIterator = mCameraInstances.find(handle);
	if (mCamIterator == mCameraInstances.end()) return 0;
	return mCamIterator->second;

}

vsbBaseCamera *vsbCameraManager::FindFirstCam()
{
	if(mCameraInstances.empty()) 
		return 0;
	mCamIterator = mCameraInstances.begin();
	return mCamIterator->second;
}

vsbBaseCamera *vsbCameraManager::FindLastCam()
{
	if(mCameraInstances.empty()) 
		return 0;
	mCamIterator = mCameraInstances.end();
	return --mCamIterator->second;
}

vsbBaseCamera *vsbCameraManager::FindNextCam()
{
	if(mCameraInstances.empty() || mCamIterator == mCameraInstances.end()) 
		return 0;
	if (++mCamIterator == mCameraInstances.end()) return 0;
	return mCamIterator->second;
}

vsbBaseCamera *vsbCameraManager::FindPrevCam()
{
	if(mCameraInstances.empty() || mCamIterator == mCameraInstances.end()) 
		return 0;
	if (--mCamIterator == mCameraInstances.end()) return 0;
	return mCamIterator->second;
}
void vsbCameraManager::RemoveIteratorCam()
{
	if (mCamIterator == mCameraInstances.end()) 
		return;
	if(mCameraInstances.empty()) 
		return;
	mCameraInstances.erase(mCamIterator);

}

void vsbCameraManager::DeleteAllCameras()
{
	vsbBaseCamera *cam;
	while ((cam =  FindFirstCam())!= 0)
	{
		delete cam;
		
	}
}

void vsbCameraManager::ActiveCam(vsbBaseCamera *cam) 
{
	//assert(cam != 0 && cam->mpCameraManager == this);
    if(cam == 0 || cam->mpCameraManager != this) return;
    if (!FindCam(cam->Handle())) return; 
	mpActiveCamera = cam;
};


int vsbCameraManager::Load(Parser::TLineInp *tli)
{
	mParseError.Clear();
	vsbCameraManagerParser cam_man(*this);

	DeleteAllCameras();

	int result = cam_man.Parse(tli);
	if (result)
	{
		mParseError.Set(*cam_man.ItsLastError());
		DeleteAllCameras();
		InsertDefaultCamera(); //@@
	}

    ActiveCam(FindFirstCam());
	return result;
};

int  vsbCameraManager::Write(FILE *f, int indent)
{
	vsbCameraManagerParser cam_man(*this);
	return cam_man.Write(f,indent);
}


void vsbCameraManager::UpdateActiveCam(double time)
{
	//@@ to be completed
	if (ActivationMode() == CM_SCHEDULE)
	{
		CameraSchedule().ActivateCamera(time);
	}
}

///////////////////////////////////////////////////////////

char *vsbCameraManagerParser::mpKeyWords[] =
{
	"CAMERA_MANAGER",
	"CAMERA",
    "CAMERA_ACTIVATION",
    "MANUAL",
    "SCHEDULE",
    "SCRIPT",
	0
};

enum {
	VSB_CAMPARSER_CAMERAMANAGER,
	VSB_CAMPARSER_CAMERA,
    VSB_CAMPARSER_CAMERA_ACTIVATION,
    VSB_CAMPARSER_MANUAL,
    VSB_CAMPARSER_SCHEDULE,
    VSB_CAMPARSER_SCRIPT,
};


char *vsbCameraManagerParser::mpErrorStrings[] =
{
	"",
	"Camera class does not exist or camera creation error",
    "Invalid activation mode; MANUAL, SCHEDULE or SCRIPT expected",
	0
};

enum {
	VSB_CAMPARSER_ERR_UNUSED,
	VSB_CAMPARSER_ERR_CREATING_CAM,
    VSB_CAMPARSER_ERR_INVALID_ACTIVATION_MODE,
};



vsbCameraManagerParser::vsbCameraManagerParser(vsbCameraManager &cm):
TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrCameraManager(cm) 
{
	mpOutput = new vsbParserHelper(*this);
}

vsbCameraManagerParser::~vsbCameraManagerParser()
{
	delete mpOutput;
}

int	 vsbCameraManagerParser::Parse(Parser::TLineInp *tli)
{
	return ParseSource(tli, true);
}

int  vsbCameraManagerParser::Write(FILE *f, int indent)
{
	mpOutput->SetFile(f);
	mpOutput->WriteSpaces(indent);
    mpOutput->WriteKeyWord(VSB_CAMPARSER_CAMERAMANAGER);
	mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;
	mpOutput->WriteNewline(indent);

    
	vsbBaseCamera *working_cam = mrCameraManager.FindFirstCam();
	while (working_cam)
	{
		mpOutput->WriteNewline(0);
		working_cam->Write(f, indent);
		working_cam = mrCameraManager.FindNextCam();
		mpOutput->WriteNewline(0);

	}

    mpOutput->WriteSpaces(indent);
    mpOutput->WriteKeyWord(VSB_CAMPARSER_CAMERA_ACTIVATION);
    mpOutput->WriteString(" = ");
    int modes[3] = {VSB_CAMPARSER_MANUAL, VSB_CAMPARSER_SCHEDULE, VSB_CAMPARSER_SCRIPT};
    mpOutput->WriteKeyWord(modes[(int)mrCameraManager.ActivationMode()]);

    mrCameraManager.CameraSchedule().Write(f, indent);

	indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);
	mpOutput->WriteNewline(indent);
	return 1;
}

void vsbCameraManagerParser::ParseFunc()
{
	char class_name[100];
	vsbBaseCamera *working_cam;

	GetToken(VSB_CAMPARSER_CAMERAMANAGER);
	GetToken(T_LBRACE);


	PeekToken();

	while (GetCurrTokenID() == VSB_CAMPARSER_CAMERA)
	{
		GetToken();
		GetToken(T_LBRACK);
		GetString(class_name);

		working_cam = mrCameraManager.Create(class_name);
		if (!working_cam)
			AbortParse(VSB_CAMPARSER_ERR_CREATING_CAM);

		GetToken(T_RBRACK);
		GetToken(T_LBRACE);

		if (working_cam->Parse(ParserLineInp()))
		{
			TParserError parseError(working_cam->ParseError());
			delete working_cam;
			AbortParse(parseError);
		}

		PeekToken();
	}

    GetToken(VSB_CAMPARSER_CAMERA_ACTIVATION);
    GetToken(T_EQ);
    int modes[] = {VSB_CAMPARSER_MANUAL, VSB_CAMPARSER_SCHEDULE, VSB_CAMPARSER_SCRIPT,-1};
    GetToken();
    int mode = KeywordTableSelection(modes);
    if (mode < 0) AbortParse(VSB_CAMPARSER_ERR_INVALID_ACTIVATION_MODE);
    mrCameraManager.ActivationMode((vsbCameraManager::EActivationMode)mode); 

    if (mrCameraManager.CameraSchedule().Parse(ParserLineInp()))
    {
        TParserError parseError(mrCameraManager.CameraSchedule().ParseError());
		AbortParse(parseError);
    }

	GetToken(T_RBRACE);


}
