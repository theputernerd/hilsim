//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSimpleCamera.h
    \brief Defines the class vsbSimpleCamera
    
     The class vsbSimpleCamera is derrived from vsbBaseCamera, 
     implementing  a simple static camera object. This camera object
     is the default camera of the vsbCameraManager.

	\sa vsbSimpleCamera.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbSimpleCamera_H
#define _vsbSimpleCamera_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#ifndef _vsbBaseCamera_H
#include "vsbBaseCamera.h"
#endif 

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

#ifndef _vsbLookAtTransform_H
#include "vsbLookAtTransform.h"
#endif

class DLL_VSB_API vsbSimpleCameraParser;

//////////////////////////////////////////////////////////////////
/*! 
	\class vsbSimpleCamera
	\brief Simple static camera. 
    
*/
/////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbSimpleCamera: public vsbBaseCamera,public vsbLookAtTransform
{
friend class DLL_VSB_API vsbSimpleCameraParser;
public:
    /*! \brief Constructor 

        Initialises the camera with its parent camera manager
        \param cm A pointer to a valid parent camera manager

        A name for the camera is automatically generated
    */
	vsbSimpleCamera(vsbCameraManager *cm);

     /*! \brief Constructor

        Initialises the camera with its parent camera manager, and a name for the camera
        \param cm A pointer to a valid parent camera manager
        \param name A name for the camera
        \param postfix A flag to indicate whether the name should be postfixed by a 
        unique camera number
    */    
	vsbSimpleCamera(vsbCameraManager *cm, const std::string &name, bool postfixed = false);

    //! Virtual Destructor 
	virtual ~vsbSimpleCamera();

	vsbSimpleCamera  &operator=(const vsbSimpleCamera  &from);

    virtual const std::string & ClassName() const; 

	virtual vsbBaseCamera *Clone();
    
    /*! \brief Set the cameras position and orientation
    \param x
    \param y
    \param z
    \param h  heading
    \param p  pitch
    \param r  roll
    */
	void PositionCamera(float x, float y, float z, float h = 0, float p = 0, float r=0);

	int  Write(FILE *f, int indent);
	int	 Parse(Parser::TLineInp *tli);

protected:

    /*! \brief Update camera matrix 
    
    This function generates the computed matrix, followed by the camera matrix based
	on the computed and relative geometry matrix, for the specified time
    \param time Time stampe for the next frame update

    \sa vsbBaseCamera::UpdateCameraMatrix
    */
	void UpdateCameraMatrix(float time);
private:
	void Setup();


};


class DLL_VSB_API vsbSimpleCameraParser: public Parser::TParseObjectBase
{
public:
	vsbSimpleCameraParser(vsbSimpleCamera &sc);
	virtual ~vsbSimpleCameraParser();

	int	 Parse(Parser::TLineInp *tli, bool skipHeader = false);
	int  Write(FILE *f, int indent);

private:
	bool mSkipHeader;
    vsbParserHelper *mpOutput;

	vsbSimpleCamera &mrSimpleCamera;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
	void ParseOnOffState(bool &onOrOffVar, int id);
	void WriteOnOff(bool onOrOff);
};


#endif