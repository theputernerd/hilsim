#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbViewportContext.cpp
    \brief Implements the vsbViewportContext class.

    The vsbViewportContext class is the interface between VSB,
    the OpenGL context and the user's visualisation system 
    applicatio code. The vsbViewportContext maps directly to a 
    view window in the application, ie; for each view the application 
    presents, there must be a corresponding vsbViewportContext object 
    (as	there should also be an OpenGL context)

    \sa vsbViewportContext.h

  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#include <string>
#include <map>
#include <set>
#include <iostream>
#include <assert.h>
using namespace std;

#include "ssg.h"

#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"
#include "vsbEnvironmentOptions.h"
#include "vsbRenderOptions.h"
#include "vsbModelManager.h"
#include "vsbEntityManager.h"
#include "vsbBaseCamera.h"
#include "vsbCameraManager.h"

#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbStateManager.h"



#include "ssg.h"
#include <GL/gl.h>
#include <GL/glu.h>
// Static initialisations

int						vsbViewportContext::mInstanceCount			= 0;
vsbRenderOptions				*vsbViewportContext::mspRenderOpts			= 0;
vsbEnvironmentOptions			*vsbViewportContext::mspEnvironmentOpts		= 0;
vsbEnvironmentManager			*vsbViewportContext::mspEnvironmentManager	= 0;
vsbEntityManager				*vsbViewportContext::mspEntityManager		= 0;
vsbModelManager					*vsbViewportContext::mspModelManager		= 0;
vsbChunkLODElevationManager		*vsbViewportContext::mspChunkLODElevationManager		= 0;
vsbStateManager					*vsbViewportContext::mspStateManager        = 0;
vsbExplosionFXDirector			*vsbViewportContext::mspExplosionFXDirector = 0;
vsbSmokeFXDirector				*vsbViewportContext::mspSmokeFXDirector = 0;
vsbHeadTrackerInterface			*vsbViewportContext::mspHeadTrackerInterface= 0;
 vsbCompiledGLShapeList			*vsbViewportContext::mspCompiledGLShapeList = 0;

void vsbViewportContext::Init()
{
	if (mspRenderOpts) 
		return;  
        // return if mspRenderOpts nonzero, as it implies we have already initalised

	mspRenderOpts			= new vsbRenderOptions;
	mspEnvironmentOpts		= new vsbEnvironmentOptions;
	mspEnvironmentManager	= new vsbEnvironmentManager(mspEnvironmentOpts, mspRenderOpts);
	mspModelManager			= new vsbModelManager; 
	mspChunkLODElevationManager = new vsbChunkLODElevationManager;
	mspEntityManager		= new vsbEntityManager;
    mspStateManager         = new vsbStateManager;
    mspExplosionFXDirector  = new vsbExplosionFXDirector;
	mspSmokeFXDirector      = new vsbSmokeFXDirector;
	mspHeadTrackerInterface = new vsbHeadTrackerInterface;
	mspCompiledGLShapeList  = new vsbCompiledGLShapeList;
}

void vsbViewportContext::Close()
{
	if (mInstanceCount > 0 || mspRenderOpts==0) 
		return;  
       // return if we currently have an instance of vsbViewportContext or havent been
       // initialised yet.

	if (mspEntityManager)		delete mspEntityManager;		mspEntityManager = 0;
	if (mspModelManager)		delete mspModelManager;			mspModelManager = 0;
	if (mspChunkLODElevationManager) delete mspChunkLODElevationManager; mspChunkLODElevationManager = 0;
    if (mspExplosionFXDirector) delete mspExplosionFXDirector;  mspExplosionFXDirector = 0;
	if (mspRenderOpts)			delete mspRenderOpts;			mspRenderOpts = 0;
	if (mspEnvironmentOpts)		delete mspEnvironmentOpts;		mspEnvironmentOpts = 0;
	if (mspEnvironmentManager)	delete mspEnvironmentManager;	mspEnvironmentManager = 0;
	if (mspStateManager)		delete mspStateManager;		    mspStateManager = 0;
	if (mspHeadTrackerInterface) delete mspHeadTrackerInterface; mspHeadTrackerInterface = 0;
	if (mspCompiledGLShapeList) delete mspCompiledGLShapeList; mspCompiledGLShapeList = 0;
	
}

set<vsbViewportContext *> vsbViewportContext::msContextList;

vsbViewportContext::vsbViewportContext()
{
	mInstanceCount++;  // Increment the reference count every time a new vsbViewportContext is created
	Init();            // Initialise the vsbViewpoertContext system if not done already 

    // create a camera manager for this vsbViewportContext
	mpCameraManager = new vsbCameraManager; 
	msContextList.insert(this);
};


vsbViewportContext::~vsbViewportContext()
{
	mInstanceCount--; //Decrement reference cound as vsbViewportContext instances are destroyed
	delete mpCameraManager; // delete the local mpCameraManager instance

	set<vsbViewportContext *>::iterator iter;
	iter = msContextList.find(this);
	if (iter!=msContextList.end())
	{
		msContextList.erase(iter);
	}
}

void vsbViewportContext::UpdateContext()
{
	set<vsbViewportContext *>::iterator iter;
	iter = msContextList.begin();
	while (iter != msContextList.end())
	{
		vsbCameraManager *cam_man = (*iter)->mpCameraManager;
		cam_man->UpdateActiveCam(mspEntityManager->CurrTime()); 
 		vsbBaseCamera *cam = cam_man->ActiveCam();
		iter++;
	}

	if (EnvironmentManager().EnvironmentOpts().ChunkLODTerrainOn())
	{
		iter = msContextList.begin();
		if (iter != msContextList.end())
		{
			EnvironmentManager().ChunkLODTerrain().SetViewportContext(*iter);
			EnvironmentManager().ChunkLODTerrain().UpdatePosition();
		}
	}

}

void vsbViewportContext::RedrawBuffer()
{
    assert(mspEnvironmentManager && mspEntityManager && mpCameraManager);
    // Ensure all the apropriate classes are instantiated
	
//	mpCameraManager->UpdateActiveCam(mspEntityManager->CurrTime()); 
 	vsbBaseCamera *cam = mpCameraManager->ActiveCam();
    // Find the currently active camera 

	if (cam)
	{
        float fovx, fovy;
        fovx = fovy = cam->FOV();
		if (mView_Aspect > 1.0)
			fovy /=  mView_Aspect;
		else
			fovx *= mView_Aspect;


        // Update the camera for the specified time
		cam->UpdateCameraMatrix(mspEntityManager->CurrTime()); 

        // Update the ssg viewing parameters FOV, and near/far clipping planes

		ssgSetFOV   ( fovx, fovy ) ;

		ssgSetNearFar ( cam->NearClip(), cam->FarClip());

        glViewport( 0, 0, (GLint) mView_W, (GLint) mView_H );
		
		if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
		{
			vsbViewportContext::EntityManager().UpdateTransforms();
			vsbViewportContext::ExplosionFXDirector().CameraCentricTranslation();

		}

		if (EnvironmentManager().EnvironmentOpts().ChunkLODTerrainOn())
		{
			EnvironmentManager().ChunkLODTerrain().SetViewportContext(this);
		}

		//if (EnvironmentManager().EnvironmentOpts().ChunkLODTerrainOn())
		//	EnvironmentManager().ChunkLODTerrain().Draw();

		mspEnvironmentManager->RedrawBuffer(cam->CameraMatrix());

		

	}
}

vsbBaseCamera &vsbViewportContext::ActiveCamera() const
{
	assert(mpCameraManager);

    // Find the currently active camera 
	vsbBaseCamera *cam = mpCameraManager->ActiveCam();

    assert(cam); // we MUST always have a camera (camera manager has at least one, 
                 // the default camera
	return *cam;
}


void	vsbViewportContext::ArcBallClick(EArcBallMode arcBallMode, sgVec2 pt)
{
    // Find the currently active camera 
	vsbBaseCamera *cam = mpCameraManager->ActiveCam();

    assert(cam); // we MUST always have a camera (camera manager has at least one, 
                 // the default camera

	sgdMat4 &matrix = cam->RelativeMatrix();

		//cjm temp settings
//	ArcBall().SetDir(/*cam->SphereReverse()&*/false);

	ArcBall().Click(arcBallMode, pt, &matrix);

}

void	vsbViewportContext::ArcBallDrag(sgVec2 dragPt)
{
	ArcBall().Drag(dragPt);
}


DLL_VSB_API void vsbToScaledVSBCoords(sgVec3 xyz)
{
	if (vsbViewportContext::IsInitialised() && vsbViewportContext::EnvironmentOpts().ScalingOn())
	{
		
		xyz[0] =   xyz[0]*vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[1] = - xyz[1]*vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[2] = - xyz[2]*vsbViewportContext::EnvironmentOpts().VerticalScale(); 
	} else
	{
		xyz[1] = - xyz[1]; 
		xyz[2] = - xyz[2]; 
	}
}

DLL_VSB_API void vsbToScaledVSBCoords(sgVec3 xyz, sgVec3 hpr)
{
	vsbToScaledVSBCoords(xyz);
   hpr[0] = - hpr[0] - 90;
}

DLL_VSB_API void vsbToScaledVSBCoords(sgdVec3 xyz)
{
	if (vsbViewportContext::IsInitialised() && vsbViewportContext::EnvironmentOpts().ScalingOn())
	{
		
		xyz[0] =   xyz[0]*vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[1] = - xyz[1]*vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[2] = - xyz[2]*vsbViewportContext::EnvironmentOpts().VerticalScale(); 
	} else
	{
		xyz[1] = - xyz[1]; 
		xyz[2] = - xyz[2]; 
	}
}

DLL_VSB_API void vsbToScaledVSBCoords(sgdVec3 xyz, sgdVec3 hpr)
{
	vsbToScaledVSBCoords(xyz);
   hpr[0] = - hpr[0] - 90;
}


DLL_VSB_API void vsbFromScaledVSBCoords(sgVec3 xyz)
{
	if (vsbViewportContext::IsInitialised() && vsbViewportContext::EnvironmentOpts().ScalingOn())
	{
		xyz[0] =   xyz[0]/vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[1] = - xyz[1]/vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[2] = - xyz[2]/vsbViewportContext::EnvironmentOpts().VerticalScale(); 
	} else
	{
		xyz[1] = - xyz[1]; 
		xyz[2] = - xyz[2]; 
	}
}

DLL_VSB_API void vsbFromScaledVSBCoords(sgVec3 xyz, sgVec3 hpr)
{
	vsbFromScaledVSBCoords(xyz);
    hpr[0] = - hpr[0] - 90;
}


DLL_VSB_API void vsbFromScaledVSBCoords(sgdVec3 xyz)
{
	if (vsbViewportContext::IsInitialised() && vsbViewportContext::EnvironmentOpts().ScalingOn())
	{
		xyz[0] =   xyz[0]/vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[1] = - xyz[1]/vsbViewportContext::EnvironmentOpts().HorizontalScale(); 
		xyz[2] = - xyz[2]/vsbViewportContext::EnvironmentOpts().VerticalScale(); 
	} else
	{
		xyz[1] = - xyz[1]; 
		xyz[2] = - xyz[2]; 
	}
}

DLL_VSB_API void vsbFromScaledVSBCoords(sgdVec3 xyz, sgdVec3 hpr)
{
	vsbFromScaledVSBCoords(xyz);
    hpr[0] = - hpr[0] - 90;
}


