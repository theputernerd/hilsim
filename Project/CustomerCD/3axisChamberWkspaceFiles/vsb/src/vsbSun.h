//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSun.h
    \brief Defines the class vsbSun

    This file defines a SkyDome class that is designed to track
    a camera on a flat earth. It provides realistic sky
    colouration given a specific son position and an 
    artificial curved horizon 

 	
	\sa vsbSun.cpp

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbSun_H
#define _vsbSun_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


//////////////////////////////////////////////////////////////
/*!
   \class vsbSun vsbSun.h 	
   \brief Manages and controls the state of the sun object
   \author T. Gouthas
*/ 

class DLL_VSB_API vsbSun {

    ssgTransform     *mpSunTransform;
    ssgSimpleState   *mpOrbState;
    ssgSimpleState   *mpHaloState;

    ssgColourArray   *mpColourArr;

    ssgVertexArray   *mpHaloVertArr;
    ssgTexCoordArray *mpHaloTextureArr;

	sgCoord           mSunPos;
	sgMat4            mSunXForm;

public:

    //! Constructor
    vsbSun( void );

    //! Destructor
    ~vsbSun( void );

    /*! \brief Builds the sun object with the specified size and the
	    and return its scene graph root
		\note The path to the directory containing the "halo.rgba" 
		texture is automatically generated internally. The file 
		"halo.rgba" must be successfully loaded before a sun image
		can be generated.
		\param sunSize Size of sun
		\returns ssgBranch; The root of the sun object scene graph
    */
    ssgBranch *Build( double sunSize );

    /*! \brief Repaint the sun colors based on current value of sunAngle in
     degrees and initialise its placement xfm relative to a camera
     
	 \param sunAngle The sun angle;
    	-	-0 degrees = high noon
    	-	-90 degrees = sun rise/set
    	-	-180 degrees = darkest midnight

    \param sunNorthDeclination Northern declination of the sun;
	    -   -23.5 summer solstice, 0 equinox, 23.5 winter solstice

    \param sunDist The distance from the current camera pos of the sun (not the 
		true distance, but an arbitrary far distance)
 
   \returns Always true

    */
    bool Repaint( double sunAngle, double sunNorthDeclination, double sunDist  );

    /*! \brief Reposition the sun given the specified camera position 

		Reposition the sun at the specified right ascension and
		declination, offset by our current position (p) so that it appears
		fixed at a great distance from the viewer.  Also add in an optional
		rotation (i.e. for the current time of day.)
		\param cameraPos The current position of the camera
		\returns Always true
	*/
    bool Reposition( const sgVec3 cameraPos);

	/*! \brief Gwt the position of the sun 
		\returns sgCoord
	*/
	sgCoord &GetSunPos() { return mSunPos; };
	
};


#endif // _vsbSun_H
