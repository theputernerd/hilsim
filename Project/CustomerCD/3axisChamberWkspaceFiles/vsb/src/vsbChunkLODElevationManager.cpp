////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbChunkLODElevationManager.cpp
    \brief Implements the class vsbChunkLODElevationManager and its auxillary classes
    
     The class vsbChunkLODElevationManager is responsible for managing and caching  
	 ChunkedLOD digital elevation models.
	 
	\sa vsbChunkLODElevationManager.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <set>

#ifdef _MSC_VER
#include <io.h>
#include <direct.h>
#endif

#if (defined(__unix__) || defined(unix))
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#endif

using namespace std;

#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

using namespace Parser;

#include "ssg.h"
#include "vsbSysPath.h"
#include "vsbUtilities.h"
#include "vsbViewportContext.h"

#include "vsbChunkLODElevationManager.h"


#define MINIMUM_CLOD_VERSION 1.0
#define WORK_DIR "elevations"
// The name of the index file containing ChunkedLOD configuration

const char *ChunkLODElevationIndexFile = "elevation.idx";

////////////////////////////////////////////////////////////////////////////////
//
//  LOCAL function definitions
//
////////////////////////////////////////////////////////////////////////////////


/*! \func vsbMakeChunkedLODElevationPath

 Composes an explicit, fully qualified path to the elevation model filename specified in 
 the fname parameter. This is based on the SysPath and the hard wired rule that 
 model paths in the model index file are implicitly relative to the "elevations" 
 subdirectory of the directory specified by the vsbGetSysPath() function.
*/

string vsbMakeChunkedLODElevationPath(string fname)
{
	string work_str = vsbGetSysPath()+ "/" WORK_DIR "/";
	vsbRemoveEnclosingSlashes(fname);
	work_str += fname;
	return work_str;
}

////////////////////////////////////////////////////////////////////////////////////////////////

vsbChunkLODElevationEntry::vsbChunkLODElevationEntry()
{
	sgMakeIdentMat4(mAlignmentMat);
	mTexelSize = 1.0;
	mMaxPixelError = 2.0;
	mMorph = true;
	mWireFrame = false;

	mLogicalName = mGeometryFileName = mTextureFileName;
}

vsbChunkLODElevationEntry::~vsbChunkLODElevationEntry()
{
}

vsbChunkLODElevationEntry::vsbChunkLODElevationEntry(const vsbChunkLODElevationEntry &from)
{
	*this = from;
}

vsbChunkLODElevationEntry &vsbChunkLODElevationEntry::operator=(const vsbChunkLODElevationEntry &from)
{
	mLogicalName = from.mLogicalName;
	mGeometryFileName = from.mGeometryFileName;
	mTextureFileName  = from.mTextureFileName;
	mTexelSize		  = from.mTexelSize;
	mMaxPixelError    = from.mMaxPixelError;
	mMorph			  =	from.mMorph;
	mWireFrame		  =	from.mWireFrame;
	sgCopyMat4(mAlignmentMat, from.mAlignmentMat);
	SetNeedUpdate();
	return *this;
}

void vsbChunkLODElevationEntry::CopyParameters(const vsbChunkLODElevationEntry &from)
{
	mTexelSize		  = from.mTexelSize;
	mMaxPixelError    = from.mMaxPixelError;
	mMorph			  =	from.mMorph;
	mWireFrame		  =	from.mWireFrame;
	sgCopyMat4(mAlignmentMat, from.mAlignmentMat);
	SetModified();
}


////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////


vsbChunkLODElevation::vsbChunkLODElevation()
{
	mNeedReload = false;
	mIsModified = false;
	mIsLoaded = false;
	mCurrent = "";
	mTextureQuadtree = NULL;
    mGeometry = NULL;



}

vsbChunkLODElevation::~vsbChunkLODElevation()
{
	if (mGeometry) delete mGeometry;
	if (mTextureQuadtree) delete mTextureQuadtree;

}

vsbChunkLODElevation::vsbChunkLODElevation(const std::string &logical_name)
{
	*this = logical_name;
}


vsbChunkLODElevation &vsbChunkLODElevation::operator = (const std::string &logical_name)
{
	map<std::string, vsbChunkLODElevationEntry>::iterator iter;
    iter = vsbViewportContext::ChunkLODElevationManager().mElevationList.find(logical_name);


	// failed to load
	if (iter == vsbViewportContext::ChunkLODElevationManager().mElevationList.end()) return *this;

	vsbChunkLODElevationEntry entry = iter->second;	

	*this = entry;

//	if (NeedUpdate()){MessageBeep(-1); Sleep(50);}


	return *this;
}

vsbChunkLODElevation &vsbChunkLODElevation::operator = (const vsbChunkLODElevationEntry &elevation_entry)
{
	vsbChunkLODElevationEntry::operator = (elevation_entry);
	mIsLoaded = false;
	mIsModified = false;
	SetNeedUpdate();
	return *this;
}


void  vsbChunkLODElevation::UpdatePosition()
{
	static vec3 cam_pos,center, extent;

	if (NeedUpdate() && mpVieportContext)
	{
//		MessageBeep(-1); Sleep(50);

		if (mCurrent != mLogicalName)
		{
			if (mTextureQuadtree)
				delete mTextureQuadtree;
			mTextureQuadtree = NULL;
			if (mGeometry)
				delete mGeometry;
            mGeometry = NULL;
		}
		mCurrent = mLogicalName; // used to check what kind of update to do

		mIsLoaded = true;
		if (!mTextureQuadtree)
		{
			if (tqt::is_tqt_file(mTextureFileName.c_str())) 
			{
				mTextureQuadtree = new tqt(mTextureFileName.c_str());
			} else
			{
				ulSetError(UL_WARNING,"Failed to load quadtree texture file: %s",mTextureFileName.c_str());
				mIsLoaded = false;
			}
		}

		if (!mGeometry && IsLoaded())
		{
			FILE *in = fopen(mGeometryFileName.c_str(), "rb");
			if (in == NULL) 
			{
				ulSetError(UL_WARNING,"Failed to load geometry file: %s",mGeometryFileName.c_str());
				mIsLoaded = false;
			} else
			{
				mGeometry = new lod_chunk_tree(in, mTextureQuadtree);
				
			}
		}
		

	}

	if (IsLoaded()) 
	{
		vsbBaseCamera *p_camera;		// Pointer to active camera for this context	
		bool cam_centric;				// flag indicating camera positioning mode
		sgdVec4 cam_pos_ssg;


		p_camera = mpVieportContext->CameraManager().ActiveCam();
		cam_centric = vsbViewportContext::RenderOpts().CameraCentricTransformsOn(); 
	
		
		sgdCopyVec4(cam_pos_ssg, p_camera->CameraMatrix()[3]);

		if (cam_centric)
			sgdSubVec3(cam_pos_ssg,vsbBaseCamera::CameraToOriginDelta());

		mGeometry->get_bounding_box(&center, &extent);
		cam_pos_ssg[1]-=center.x;
		cam_pos_ssg[0]-=center.z;

		cam_pos.set_xyz(-cam_pos_ssg[1],cam_pos_ssg[2],-cam_pos_ssg[0]);

		mGeometry->update(cam_pos);

		float pixerr = vsbViewportContext::EnvironmentOpts().ChunkLODPixelError();

		if (pixerr<1)
			vsbViewportContext::EnvironmentOpts().ChunkLODPixelError(mMaxPixelError);
		else
			mMaxPixelError = pixerr;


		mGeometry->set_parameters(mMaxPixelError, mTexelSize, mpVieportContext->ViewportWidth(), p_camera->FOV());
		mGeometry->set_use_loader_thread(true);

//		ulSetError(UL_WARNING,"%lf %lf %lf\n", cam_mat[3][0],cam_mat[3][1],cam_mat[3][2]);
	}

	mNeedReload = false;
}


static void set_view_matrix(matrix& m, double *mat)
// Applies the given matrix to the current GL matrix.
{
	
	for (int col = 0; col < 4; col++) 
	{
		vec3 v;
		v.set_xyz(-mat[col * 4 + 0],mat[col * 4 + 1],-mat[col * 4 + 2]);
		m.SetColumn(col,v);
	}
}

static void CopyMat4ToDouble (sgdMat4 dest, sgMat4 src)
{
	int i, j;
	for (i=0; i< 4; i++)
		for (j=0; j<4; j++)
			dest[i][j] = src[i][j];
}

void  vsbChunkLODElevation::Draw()
{
	if (!(IsLoaded()  && mpVieportContext)) return;

	static view_state		s_view;     // view frustrum (Ulrich CHUNKLOD code)
	static render_options	render_opt; // render options Ulrich CHUNKLOD code)

	sgdMat4 cam_mat;					// Camera matrix
	sgdMat4 dview_mat;
	vsbBaseCamera *p_camera;		// Pointer to active camera for this context	
	bool cam_centric;				// flag indicating camera positioning mode

	static plane_info	frustum_plane[6];

	p_camera = mpVieportContext->CameraManager().ActiveCam();

	float width				= mpVieportContext->ViewportWidth();
    float height			= mpVieportContext->ViewportWidth();

	if (!width || !height) return; // Not valid

	float aspect_ratio		= height/ width;
	float horizontal_fov	= (float) p_camera->FOV() * M_PI / 180.f;

	float vertical_fov		= (float) atanf(tanf(horizontal_fov / 2.f) * aspect_ratio) * 2;
	float nearz				= p_camera->NearClip();
	float farz				= p_camera->FarClip();

	
	float hfov_on_2 = horizontal_fov/2;
	float vfov_on_2 = vertical_fov/2;

	float cos_hfov_on_2 = cosf(hfov_on_2);
	float sin_hfov_on_2 = sinf(hfov_on_2);
	float cos_vfov_on_2 = cosf(vfov_on_2);
	float sin_vfov_on_2 = sinf(vfov_on_2);

 
	// Compute values for the frustum planes.  Normals point inwards towards the visible volume.
	
	frustum_plane[0].set(0, 0, 1, nearz);	// near.
	frustum_plane[1].set(0, 0, -1, -farz);	// far.
	frustum_plane[2].set(-cos_hfov_on_2, 0, sin_hfov_on_2, 0);	// left.
	frustum_plane[3].set(cos_hfov_on_2, 0, sin_hfov_on_2, 0);	// right.
	frustum_plane[4].set(0, -cos_vfov_on_2, sin_vfov_on_2, 0);	// top.
	frustum_plane[5].set(0, cos_vfov_on_2, sin_vfov_on_2, 0);	// botto

	cam_centric = vsbViewportContext::RenderOpts().CameraCentricTransformsOn(); //.EntityCentricCameraOn();
	
	sgdCopyMat4(cam_mat,p_camera->CameraMatrix());
	
	if (cam_centric)
		sgdSubVec3(cam_mat[3],vsbBaseCamera::CameraToOriginDelta());

	vec3	center, extent;
	mGeometry->get_bounding_box(&center, &extent);
	cam_mat[3][1]-=center.x;
	cam_mat[3][0]-=center.z;


/*
 
   //The following sequence within dashed line, is condensed in following code 
   // -------------------------start---------------------------------

	//  rotate the chunklod terrain around its up vector by 90 degrees
	//  ** swapping col 0 & 1 then negating new col 0 is equivalent to the following
    //	** sgdVec3 orient = {0, 0, 1};
    //	** sgdMakeRotMat4(drot_mat, 90,orient);
    //	** sgdPostMultMat4(cam_mat,drot_mat);

	for(int row=0;row<4; row++)
	{
		double tmp = cam_mat[row][1];
		cam_mat[row][1] = cam_mat[row][0];
		cam_mat[row][0]= -tmp;
	}

	
 	sgdTransposeNegateMat4 ( dview_mat, cam_mat );


	static sgdMat4 axisMatrix =
	{
		{  1.0f,  0.0f, 0.0f,  0.0f },
		{  0.0f,  0.0f, -1.0f, 0.0f },
		{  0.0f,  1.0f,  0.0f,  0.0f },
		{  0.0f,  0.0f,  0.0f,  1.0f }
	} ;
	
	sgdPostMultMat4(dview_mat, axisMatrix);


	
	
    //rotate 90 deg rotation about X
	//** swapping col 1 & 2 then negating col 1 equivalent to the following
	//** sgdVec3 axis2 = {1, 0, 0};
	//** sgdMakeRotMat4(drot_mat, 90,axis2);
	//** sgdPreMultMat4(dview_mat,drot_mat);
	
	static sgdMat4 axisMatrix3 =
	{
		{  1.0f,  0.0f, 0.0f,  0.0f },
		{  0.0f,  0.0f, 1.0f, 0.0f },
		{  0.0f,  -1.0f,  0.0f,  0.0f },
		{  0.0f,  0.0f,  0.0f,  1.0f }
	} ;

	sgdPreMultMat4(dview_mat, axisMatrix3);

  // -------------------------end---------------------------------

*/	
	dview_mat[0][0] = -cam_mat[0][1] ;
	dview_mat[1][0] = cam_mat[0][2] ;
	dview_mat[2][0] = -cam_mat[0][0] ;
	dview_mat[3][0] = - ( cam_mat[3][1]*cam_mat[0][1] + cam_mat[3][0]*cam_mat[0][0] + cam_mat[3][2]*cam_mat[0][2] );
	
	dview_mat[0][1] = -cam_mat[2][1] ;    
	dview_mat[1][1] = cam_mat[2][2] ;                                                    
	dview_mat[2][1] = -cam_mat[2][0] ;                                                      
   	dview_mat[3][1] = - ( cam_mat[3][1]*cam_mat[2][1] + cam_mat[3][0]*cam_mat[2][0] + cam_mat[3][2]*cam_mat[2][2] );
	
	dview_mat[0][2] = cam_mat[1][1] ;
	dview_mat[1][2] = -cam_mat[1][2] ;
	dview_mat[2][2] = cam_mat[1][0] ;
	dview_mat[3][2] =  ( cam_mat[3][1]*cam_mat[1][1] + cam_mat[3][0]*cam_mat[1][0] + cam_mat[3][2]*cam_mat[1][2] );

	dview_mat[0][3] = SGD_ZERO ;
	dview_mat[1][3] = SGD_ZERO ;                                                        
	dview_mat[2][3] = SGD_ZERO ;                                                        
	dview_mat[3][3] = SGD_ONE  ;


	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixd((double *)dview_mat);
		


	
    set_view_matrix(s_view.m_matrix, (double *)dview_mat);
	
	
	for (int i = 0; i < 6; i++) 
	{
		// Rotate the plane from view coords into world coords.  I'm pretty sure this is not a slick
		// way to do this :)
		plane_info&	tp = s_view.m_frustum[i];
		plane_info&	p = frustum_plane[i];
		
		s_view.m_matrix.ApplyInverseRotation(&tp.normal, p.normal);
		vec3	v;
		s_view.m_matrix.ApplyInverse(&v, p.normal * p.d);
		tp.d = v * tp.normal;
		
	}


	render_opt.show_box = false;
	render_opt.morph = mMorph;
	
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	// GL_MODULATE
		
	// Enable vertex array, and just leave it on.
	glEnableClientState(GL_VERTEX_ARRAY);
		
		// Set the wireframe state.
	if (vsbViewportContext::EnvironmentOpts().ChunkLODWireframeOn()) 
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	else 
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		
		
	glDisable(GL_LIGHTING);
	
	// Turn on back-face culling.
//	glFrontFace(GL_CCW);
	glEnable(GL_CULL_FACE);
	glEnable(GL_FOG);
	
	const float *tod_color = vsbViewportContext::EnvironmentManager().LightingParams().Diffuse();
	mGeometry->set_color(tod_color[0],tod_color[1],tod_color[2]);
	mGeometry->set_render_options(render_opt);
	mGeometry->render(s_view);
	
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glEnable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_GEN_S);
	glDisable(GL_TEXTURE_GEN_T);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);


}


bool  vsbChunkLODElevation::IsPointAboveGround(sgVec3 target)
{
	if (!IsLoaded())
		return true;   // Terrain not loaded, assume point above ground
	vec3 center, extent; 
	mGeometry->get_bounding_box(&center, &extent);


/*
	sgMat2 rot;
	sgVec3 axis2 = {0, 0, 1};
	sgVec3 dst;
	sgMakeRotMat4(rot, -90,axis2);
	sgXformPnt3(dst,rot);
*/	
	//vec3 pt(target[0]+center.x,target[2],-target[1]+center.z);

	vec3 pt(-target[1]+center.x,target[2],-target[0]+center.z);

	bool result =  mGeometry->is_above_terrain(&pt);
	return result;

}

////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbChunkLODElevationListParser: public TParseObjectBase, public vsbChunkLODElevationEntry
{
friend  class DLL_VSB_API vsbChunkLODElevationManager;

public:
    /*! \brief Constructor
        \param ptl A reference to the model manager
    */
	vsbChunkLODElevationListParser(vsbChunkLODElevationManager &pclodmanager);

    /*! \brief Parse a model index file
    */
	int ParseFile(const string &fname);

    /*! Get the currently active model source file

    A model file can recursively pull in other model files. This function returns the name
    of the actual file being parsed.
    */
	const string &CurrentActiveSourceFile() const {return mCurrentActiveSourceFile;};
private:

	static char     *mpsKeywords[];    //!< Index file grammar key words
	static char     *mpsErrors[];      //!< Index error strings 
	
	string          mCurrentActiveSourceFile; //!< Current file being parsed

	vsbChunkLODElevationManager &mElevationManager;  //!< Reference to the clod elevation manager

	void   ParseFunc();	                //!< Overidden virtual parse function (see parser)

/** @name Sub-Parsers
Private functions that parse different blocks of the model file grammar
*/
//@{
	void  ParseAllBlocks();
	void  ParseTerrainBlock();
	void  RecurseSubDirectories();

//@}



            /*! \brief Parses all the registered blocks that follow in the input stream

            While a peek into the input token stream reveals a registerd block keyword
            the parser for that block is invoked.
            */
	void    PerformParseBlocks();
    
            //! Tests whether all compulsory blocks have been parsed
	void    TestCompulsoryBlocksParsed();
            


#           ifdef _MSC_VER
#           pragma warning( push )
#           pragma warning( disable : 4251 ) // STL not exported warning
#           endif

	        set<string> mIncludedFiles; //!< used to keep a record of included files so they are included only once

			bool mRecursing;
			string mDefaultElevationPath;

#           ifdef _MSC_VER
#           pragma warning( pop )
#           endif

};




// Enumerations for error messages when parsing model index grammar




enum {
	CLOD_CHUNKLOD_TERRAIN_LIST,
	CLOD_VERSION,
	CLOD_INCLUDE,
	CLOD_TERRAIN_DEF,
	CLOD_GEOMETRY_FILE,
	CLOD_TEXTURE_FILE,
	CLOD_POSITION,
	CLOD_ORIENTATION,
	CLOD_PIXEL_ERROR,
	CLOD_TEXEL_SIZE,
};

char * vsbChunkLODElevationListParser::mpsKeywords[] = 
{
	"CHUNKLOD_TERRAIN_LIST",
	"VERSION",
	"INCLUDE",
	"TERRAIN_DEF",
	"GEOMETRY_FILE",
	"TEXTURE_FILE",
	"POSITION",
	"ORIENTATION",
	"PIXEL_ERROR",
	"TEXEL_SIZE",
	0
};

enum {
	CLOD_UNUSED,
	CLOD_INTERNAL_ERR,
	CLOD_FILE_ERR,
	CLOD_INCLUDE_FILE_ERR,
	CLOD_VERSION_OBSOLETE,
	CLOD_RECURSIVE_INCLUDE_ERROR,
	CLOD_INCLUDE_FILE_ERROR,
	CLOD_LOGICAL_NAME_REPEATED,
	CLOD_ORIENTATION_REPEATED,
	CLOD_POS_REPEATED,
	CLOD_TEXEL_SIZE_REPEATED,
	CLOD_PIXEL_ERROR_REPEATED,
};

char * vsbChunkLODElevationListParser::mpsErrors[] = 
{
	"",
	"Internal Error",
	"Unable to open chunked LOD terrain index file", 
	"Unable to open include file",
	"File version obsolete",
	"Includes not allowed in recursed sub-index files",
	"Model file name expected",
	"Logical name repeated",
	"Orientation adjustment repeated",
	"Position adjustment repeated",
	"Texel size setting repeated",
	"Maximum pixel error setting repeated",
	0
};



vsbChunkLODElevationListParser::vsbChunkLODElevationListParser(vsbChunkLODElevationManager &pclodmanager):
TParseObjectBase(mpsKeywords, mpsErrors),
	mRecursing(false),
mElevationManager(pclodmanager) 
{
}

int vsbChunkLODElevationListParser::ParseFile(const string &fname)
{

	TFileLineInp mInput;
	mElevationManager.mElevationList.clear();
	mIncludedFiles.clear();

	mCurrentActiveSourceFile = fname;

	if (!mInput.Open(fname.c_str()))
	{
		if (!mRecursing) 
		{
			try {
				RecurseSubDirectories();
			}
			catch(...)
			{
				return itsLastError->Id();
			}
			return 0;
		}
		else
		{
			mInput.Close();
			itsLastError->Set(CLOD_FILE_ERR,0,"",mpsErrors[CLOD_FILE_ERR]);
			return CLOD_FILE_ERR;
		}
	} 

	int errNum = ParseSource(&mInput, 1);
    mInput.Close();
	return errNum;
}


void vsbChunkLODElevationListParser::ParseFunc()
{
	ParseAllBlocks();
}

void vsbChunkLODElevationListParser::RecurseSubDirectories()
{
	mRecursing = true;
	vector<string> dirs;


	FindSubDirectories(WORK_DIR,dirs);
	vector<string>::iterator dir_iter;


	for(dir_iter = dirs.begin(); dir_iter!= dirs.end(); dir_iter++)
	{
		// do not read the current file again.

		if (*dir_iter == "." || *dir_iter == "..") continue;


		mDefaultElevationPath = *dir_iter;


		string sub_index_file = vsbGetSysPath()+ "/" WORK_DIR+"/"+mDefaultElevationPath.c_str()+"/" + ChunkLODElevationIndexFile;
		TFileLineInp sub_index_inp;
		
		if (!sub_index_inp.Open(sub_index_file.c_str())) 
			continue;
		string last_source	= mCurrentActiveSourceFile;
		TLineInp *last_line_input 	= itsLineInput;
		mCurrentActiveSourceFile = sub_index_file;

		int recurse_status = ParseSource(&sub_index_inp, 1);

		itsLineInput = last_line_input;

		if (recurse_status) AbortParse(*ItsLastError());
		mIncludedFiles.insert(mCurrentActiveSourceFile);
		mCurrentActiveSourceFile = last_source;

	}

}

void vsbChunkLODElevationListParser::ParseAllBlocks()
{
	double ver_num;
	string include_filename;
	GetToken(CLOD_CHUNKLOD_TERRAIN_LIST);
	GetToken(T_LBRACK);
	GetToken(CLOD_VERSION);
	ver_num = GetNumericValue();
	if (ver_num < MINIMUM_CLOD_VERSION)
		AbortParse(CLOD_VERSION_OBSOLETE);

	GetToken(T_RBRACK);

	if (!mRecursing)
	  mDefaultElevationPath = ".";

	PeekToken();
	while (GetCurrTokenID() == CLOD_INCLUDE)
	{
		if (mRecursing)
			AbortParse(CLOD_RECURSIVE_INCLUDE_ERROR);

		GetToken();
		GetToken(T_STRING);
		string include_filename = vsbMakeChunkedLODElevationPath(GetCurrTokenStr());
		if (mIncludedFiles.find(include_filename) == mIncludedFiles.end())
		{
			TFileLineInp include_input;

			if (!include_input.Open(include_filename.c_str()))
			{
				string error_msg = "Unable to open include file \"";
				error_msg += include_filename;
				error_msg += "\"";
				include_input.Close();
				AbortParse(CLOD_INCLUDE_FILE_ERROR, error_msg.c_str());
			}
			
			string last_source				= mCurrentActiveSourceFile;
			TLineInp *last_line_input		= itsLineInput;
			mCurrentActiveSourceFile		= include_filename;

			int include_status = ParseSource(&include_input, 1);

			itsLineInput = last_line_input;

			if (include_status) AbortParse(*ItsLastError());
			mIncludedFiles.insert(mCurrentActiveSourceFile);
			mCurrentActiveSourceFile = last_source;
		}
		PeekToken();
	}
	while( GetCurrTokenID() == CLOD_TERRAIN_DEF )
	{
		ParseTerrainBlock();
		PeekToken();
	}


	if (!mRecursing) RecurseSubDirectories();


}

void vsbChunkLODElevationListParser::ParseTerrainBlock()
{
	map<std::string, vsbChunkLODElevationEntry>::iterator iter;
	sgCoord pos;

	sgSetVec3(pos.xyz, 0, 0, 0);
	sgSetVec3(pos.hpr, 0, 0, 0);


	GetToken();
	GetToken(T_LBRACK);
	GetToken(T_STRING);
	mLogicalName = GetCurrTokenStr();

	iter = mElevationManager.mElevationList.find(mLogicalName);
	if (iter != mElevationManager.mElevationList.end()) 
			AbortParse(CLOD_LOGICAL_NAME_REPEATED);

	GetToken(T_RBRACK);
	GetToken(T_LBRACE);
	GetToken(CLOD_GEOMETRY_FILE);
	GetToken(T_EQ);
	GetToken(T_STRING);
	mGeometryFileName = GetCurrTokenStr();
	vsbRemoveEnclosingSlashes(mGeometryFileName);
	mGeometryFileName = vsbGetSysPath()+ "/" WORK_DIR+"/"+mDefaultElevationPath.c_str()+"/" + mGeometryFileName;

	GetToken(CLOD_TEXTURE_FILE);
	GetToken(T_EQ);
	GetToken(T_STRING);
	mTextureFileName = GetCurrTokenStr();
	vsbRemoveEnclosingSlashes(mTextureFileName);
	mTextureFileName = vsbGetSysPath()+ "/" WORK_DIR+"/"+mDefaultElevationPath.c_str()+"/" + mTextureFileName;


	mTexelSize = 1.0;
	mMaxPixelError = 4.0;
	mMorph = true;
	mWireFrame = false;


	PeekToken();
	bool pos_read = false;
	bool orient_read = false;
	bool pixerr_read = false;
	bool texsize_read = false; 
	while (GetCurrTokenID() == CLOD_POSITION 
		|| GetCurrTokenID() == CLOD_ORIENTATION
		|| GetCurrTokenID() == CLOD_PIXEL_ERROR
		|| GetCurrTokenID() == CLOD_TEXEL_SIZE)
	{
		switch(GetCurrTokenID())
		{
		case CLOD_TEXEL_SIZE:
			if (texsize_read) 
				AbortParse(CLOD_TEXEL_SIZE_REPEATED);
			GetToken();
			GetToken(T_EQ);
			mTexelSize = GetNumericValue();
			texsize_read = true;
			break;

		case CLOD_PIXEL_ERROR:
			if (pixerr_read) 
				AbortParse(CLOD_PIXEL_ERROR_REPEATED);
			GetToken();
			GetToken(T_EQ);
			mMaxPixelError = GetNumericValue();
			pixerr_read = true;
			break;

		case CLOD_POSITION:
			if (pos_read) 
				AbortParse(CLOD_POS_REPEATED);
			GetToken();
			GetToken(T_EQ);
			pos.xyz[0] = GetNumericValue();
			GetToken(T_COMMA);
			pos.xyz[1] = GetNumericValue();
			GetToken(T_COMMA);
			pos.xyz[2] = GetNumericValue();
			pos_read = true;
			break;

		case CLOD_ORIENTATION:
			if (orient_read) 
				AbortParse(CLOD_ORIENTATION_REPEATED);
			GetToken();
			GetToken(T_EQ);
			pos.hpr[0] = GetNumericValue();
			GetToken(T_COMMA);
			pos.hpr[1] = GetNumericValue();
			GetToken(T_COMMA);
			pos.hpr[2] = GetNumericValue();
			orient_read = true;
			break;

		}
		PeekToken();

	}

	vsbToScaledVSBCoords(pos) ;
	sgMakeCoordMat4(mAlignmentMat, &pos);

	GetToken(T_RBRACE);
	mElevationManager.mElevationList.insert(pair<string, vsbChunkLODElevationEntry>(mLogicalName, *this));


}

/////////////////////////////////////////////////////////////////////////////////////////

int vsbChunkLODElevationManager::mInstanceCount = 0;

vsbChunkLODElevationManager::vsbChunkLODElevationManager()
{
	assert(mInstanceCount++<1);
	mCount = 0;
	LoadIndex();
}

vsbChunkLODElevationManager::~vsbChunkLODElevationManager()
{
}

void vsbChunkLODElevationManager::LoadIndex()
{
	string clod_idxfile = vsbMakeChunkedLODElevationPath(ChunkLODElevationIndexFile);

	ulSetError(UL_DEBUG,"Loading chunked lod terrain index file: %s",clod_idxfile.c_str());

	vsbChunkLODElevationListParser parser(*this);
	int err_val = parser.ParseFile(clod_idxfile);
	if (err_val)
	{
		char buffer[400];
		sprintf(buffer,"While loading chunked lod terrain index file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			parser.CurrentActiveSourceFile().c_str(), 
			parser.ItsLastError()->Id(),
			parser.ItsLastError()->Desc(),
			parser.ItsLastError()->Pos(),
			parser.ItsLastError()->Line());
		ulSetError(UL_FATAL, buffer);
		return;
	}

	mCount = mElevationList.size();

}

const std::string *vsbChunkLODElevationManager::FindFirstName()
{
	vsbChunkLODElevationEntry *me = FindFirst();
	if (me) 
		return &me->LogicalName();
	else 
		return 0;
}

const std::string *vsbChunkLODElevationManager::FindNextName()
{
	vsbChunkLODElevationEntry *me = FindNext();
	if (me) 
		return &me->LogicalName();
	else 
		return 0;
}

vsbChunkLODElevationEntry *vsbChunkLODElevationManager::FindFirst()
{
	mElevationListIter = mElevationList.begin();
	if (mElevationListIter == mElevationList.end()) 
		return 0;
	return &mElevationListIter->second;
}

vsbChunkLODElevationEntry *vsbChunkLODElevationManager::FindNext()
{
	mElevationListIter++;
	if (mElevationListIter == mElevationList.end()) 
		return 0;
	return &mElevationListIter->second;
}

vsbChunkLODElevationEntry *	vsbChunkLODElevationManager::Find(const std::string &new_name)
{
	mElevationListIter = mElevationList.find(new_name);
	if (mElevationListIter == mElevationList.end()) 
		return 0;
	return &mElevationListIter->second;
}

////////////////////////////////////////////////////////////////////////////////
//
//  CLASS: vsbChunkLODElevationListParser -- Member Definitions
//
//  This class derrives an object from the TParseObjectBase class and defines 
//  a grammar for the chunked lod terrain index file.  It has the ability to parse a 
//  chunked lod terrain index file and populate the vsbChunkLODElevationManager's  
//  mElevationList with vsbChunkLODElevationEntry's that contain all the information 
//  necessary to load a chunked lod terrain 
////////////////////////////////////////////////////////////////////////////////