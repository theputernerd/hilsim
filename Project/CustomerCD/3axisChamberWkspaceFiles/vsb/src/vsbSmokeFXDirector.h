#ifndef _vsbSmokeFXDirector_H
#define _vsbSmokeFXDirector_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

/*
#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif
*/

#ifndef _vsbSmokeEmitter_H
#include "vsbSmokeEmitter.h"
#endif

// vsbSmokeFXDirector is designed to be a singleton in the application

class DLL_VSB_API vsbSmokeFXDirector
{
    friend class DLL_VSB_API vsbSmokeEmitter;
    friend class _vsbSmokeEmitterDefinitionParser;
    
public: 
    vsbSmokeFXDirector();
    virtual ~vsbSmokeFXDirector();
    
    vsbSmokeFXDirector(int max_puffs, bool pre_allocate = true);
    
    void SetMaxPuffs(int max_puffs);    // set the maximum number of puffs handled

	
    void Update(double time);		  // forward time stepping - create puffs from emitters/ remove dead ones
	
    void Regenerate(double time);     // when jumping (also sort wrt to time)
    

 	static bool IsInitialised(){ return msSmokeEmitterDefinitions.size() != 0; };


	vsbSmokeEmitter  *NewSmokeEmitter(std::string emitter_type);
	

	vsbSmokeEmitter *FindEmitterById(int id);

	void  DeleteEmitterById(int id);

    static void StateLoaderCallback(vsbStateManager *stateManager);
    
	std::list<vsbSmokeEmitter *> &SmokeEmitters() {return mEmitters;};

	std::map<std::string, vsbSmokeEmitterDefinition> &SmokeEmitterDefinitions()  {return msSmokeEmitterDefinitions;};

	void ClearAllEmitters();				// clear all emitters
	void ClearUnattachedEmitters();			// clear those emitters that are not attached to entities;
	void ClearFlaggedEmitters();			// clear entities that are flagged as deletable
	
	void ClearAttachedEmitters(int entity_id); // clear those emitters attached to said entity

	bool SaveFile(const std::string &name);
	const char *LoadFile(const std::string &filename);
    static void SaveSmokeEmitterDefinitions();
	
protected:
    int			                  mMaxPuffCount;   // The maximum number of smoke puffs to manage
    bool						  mPreallocatePuffs;
    static int					  mInstanceCount;
	ssgTransform                  *mTransform;
    
    std::list<vsbSmokeEmitter *>  mEmitters;
    std::list<vsbSmokeVtxTable *> mActivePuffs;
    std::list<vsbSmokeVtxTable *> mFreePuffs;
    
    static std::map<std::string, vsbSmokeEmitterDefinition> msSmokeEmitterDefinitions;
    
	void PreCreatePuffs(); // create a list of free puffs in advance
	void StopActiveExpiredPuffs(/*double time*/); // stop expired active puffs
	void PreallocPuffs(bool truefalse); // define whether to preallocate all puffs on callse to SetMaxPuffs
    
      void StopActivePuffs();					// stops all active puffs
	void ClearFreeList();

	static void LoadSmokeEmitterDefinitions(vsbStateManager *stateManager, 
		const std::string &smokeEmitterDefFile);

	
	
    static void ClearSmokeEmitterDefinitions();

	vsbSmokeVtxTable *NewSmokePuff();

	

};



/*! \func vsbMakeSmokeFXPath

 Composes an explicit, fully qualified path to the explosion effects filename 
 specified in the fname parameter. This is based on the SysPath and the hard 
 wired rule that explosion file paths in the explosions index file are 
 implicitly relative to the "effects/explosions" subdirectory of 
 the directory specified by the vsbGetSysPath() function.
*/

std::string vsbMakeSmokeFXPath(std::string fname);

class DLL_VSB_API vsbSmokeSeting
{
public:
	std::string mTypeName;
	std::string mEntityName;
	std::string mAttachmentName;
	bool		mAttached;
	double		mX, mY, mZ;
	double		mStartTime;
	double		mEndTime;
};

class DLL_VSB_API vsbSmokeFXListParser: public Parser::TParseObjectBase, public vsbSmokeSeting
{
public:
	vsbSmokeFXListParser(vsbSmokeFXDirector &efxd);
	virtual ~vsbSmokeFXListParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
	vsbParserHelper *mpOutput;
	std::vector<vsbSmokeSeting> mSmokeList;

	vsbSmokeFXDirector &mrSmokeFXDirector;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};

#endif //_vsbSmokeManager_H