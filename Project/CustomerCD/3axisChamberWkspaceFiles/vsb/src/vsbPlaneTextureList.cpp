//////////////////////////////////////////////////////////////////
/*! 
    \file vsbPlaneTextureList.cpp
    \brief Implements the class vsbPlaneTextureList
    
    The vsbPlaneTextureList class maintains a list of  texture names, 
    texture filenames and scales for all ground plane, or cloud texture 
    objects. 
  
    The texture names and scales are extracted from a file containing the tabulated 
    filename and scales. This file would reside in the directory along with the 
    texture files and act as an index into the textures.

    \sa vsbPlaneTextureList.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <assert.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

using namespace std;

#include "parser.h"
#include "parser_perror.h"
#include "parser_flinein.h"

using namespace Parser;

#include "ul.h"

#include "vsbPlaneTextureList.h"


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbPlaneTextureListParser
    \brief Parses a plane texture list index file and fills a 
    vsbPlaneTextureList object.

   
    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class vsbPlaneTextureListParser: public TParseObjectBase
{
public:

    /*! \brief Constructor

        Initialises the parser and selects a vsbPlaneTextureList object
        to load.
        \param ptl vsbPlaneTextureList to load from a texture list index file
    */
	vsbPlaneTextureListParser(vsbPlaneTextureList &ptl);

    /*! \brief Perform the parse for the given texture index file
        \returns 0 if successful, an error number otherwise.
        Error details including line number, and message string can
        be retreived from the TParserObjectBase member functions
        \sa TParserObjectBase
    */
	int ParseFile(const string &fname);
private:
	static char *mpsKeywords[];     //!< Keywords of the texture index file syntax
	static char *mpsErrors[];       //!< Error strings of the texture index file syntax
	vsbPlaneTextureList &mPlaneTextureList; //!< Reference to the vsbPlaneTextureList

	void   ParseFunc(); //!< Overloaded ParseFunc implementing the parser grammar
};

enum {
	TL_TERRAIN_TEXTURE_LIST,
	TL_VERSION,
};


// No user keywords or error messages
char * vsbPlaneTextureListParser::mpsKeywords[] = 
{
	"TERRAIN_TEXTURE_LIST",
	"VERSION",
	0
};

enum {
	TL_UNUSED,
	TL_TEXTURE_REF_NAME_EXPECTED,
	TL_TEXTURE_FILE_NAME_EXPECTED,
	TL_TEXTURE_SCALE_EXPECTED,
	TL_TEXTURE_REF_NAME_REPEATED,
	TL_FILE_ERR,
};

char * vsbPlaneTextureListParser::mpsErrors[] = 
{
	"",
	"Texture reference name expected",
	"Texture filename expected",
	"Texture scale expected",
	"Texture reference name repeated",
	"Unable to open terrain texture index file", 

	0
};



vsbPlaneTextureListParser::vsbPlaneTextureListParser(vsbPlaneTextureList &ptl):
TParseObjectBase(mpsKeywords, mpsErrors),
mPlaneTextureList(ptl) 
{
}

int vsbPlaneTextureListParser::ParseFile(const string &fname)
{
	TFileLineInp mInput;
	if (!mInput.Open(fname.c_str()))
	{
		mInput.Close();
        itsLastError->Set(TL_FILE_ERR,0,"",mpsErrors[TL_FILE_ERR]);
        return TL_FILE_ERR;
	} 

	int errNum = ParseSource(&mInput, 1);
    mInput.Close();
	return errNum;
}

void vsbPlaneTextureListParser::ParseFunc()
{
	double ver_num;
	float f_scale;
	string f_name, f_label;
	vsbPlaneTextureEntry *temp_texture_entry;
	
	mPlaneTextureList.Clear();

	map<string, vsbPlaneTextureEntry *>::iterator p; 
	pair<map<string, vsbPlaneTextureEntry *>::iterator, bool> res;

	GetToken(TL_TERRAIN_TEXTURE_LIST);
	GetToken(T_LBRACK);
	GetToken(TL_VERSION);
	ver_num = GetNumericValue();
	GetToken(T_RBRACK);
	PeekToken();
	while(GetCurrTokenID() != T_EOF)
	{
		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(TL_TEXTURE_REF_NAME_EXPECTED);
		f_label = GetCurrTokenStr() ;
		
		p = mPlaneTextureList.mListLabel.find(f_label);
		if (p != mPlaneTextureList.mListLabel.end()) 
			AbortParse(TL_TEXTURE_REF_NAME_REPEATED);

		GetToken();
		if (GetCurrTokenID()  != T_STRING)
			AbortParse(TL_TEXTURE_FILE_NAME_EXPECTED);
		f_name = GetCurrTokenStr();
			
		f_scale = (float) GetNumericValue(TL_TEXTURE_SCALE_EXPECTED);

		temp_texture_entry = new vsbPlaneTextureEntry(f_label, f_name, f_scale);
		
		res = mPlaneTextureList.mListLabel.insert(pair<string, vsbPlaneTextureEntry *>(f_label, temp_texture_entry));
		res = mPlaneTextureList.mListFilename.insert(pair<string, vsbPlaneTextureEntry *>(f_name, temp_texture_entry));
		temp_texture_entry->Index(mPlaneTextureList.mCount);
		mPlaneTextureList.mIndexArray[mPlaneTextureList.mCount] = temp_texture_entry; 
		mPlaneTextureList.mCount++;

		PeekToken();
	}
}

////////////////////////////////////////////////////////////////////////////////////


vsbPlaneTextureEntry::~vsbPlaneTextureEntry()
{
}

vsbPlaneTextureEntry::vsbPlaneTextureEntry()
{
	mScale = 0;
	mFname = "";
}

vsbPlaneTextureEntry::vsbPlaneTextureEntry(const string &label, const string &name, float scale)
{
	mScale = scale;
	mFname = name;
	mLabel = label;
}

vsbPlaneTextureEntry &vsbPlaneTextureEntry::operator =(const vsbPlaneTextureEntry & someGroundTexture)
{
	mScale = someGroundTexture.Scale();
	mFname = someGroundTexture.Fname();
	mLabel = someGroundTexture.Label();
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

vsbPlaneTextureList::vsbPlaneTextureList()
{
	mCount = 0;
}

vsbPlaneTextureList::vsbPlaneTextureList(const string &path) 
{
	mCount = 0;
	LoadList(path);
}

vsbPlaneTextureEntry *	vsbPlaneTextureList::FindByLabel(const string &descr) 
{
	mListLabelIter = mListLabel.find(descr);


	if (mListLabelIter == mListLabel.end()) 
		return 0;

	return mListLabelIter->second;

};

vsbPlaneTextureEntry *	vsbPlaneTextureList::FindByFilename(const string &fname) 
{
	map<string, vsbPlaneTextureEntry *>::iterator p; 

	p = mListFilename.find(fname);


	if (p == mListFilename.end()) 
		return 0;

	return FindByLabel(p->second->Label());
};


vsbPlaneTextureEntry *	vsbPlaneTextureList::FindFirst() 
{
	mListLabelIter = mListLabel.begin();
	if (mListLabelIter == mListLabel.end()) 
		return 0;
	return mListLabelIter->second;
}

vsbPlaneTextureEntry *	vsbPlaneTextureList::FindNext() 
{
	mListLabelIter++;
	if (mListLabelIter == mListLabel.end()) 
		return 0;
	return mListLabelIter->second;
}

int	vsbPlaneTextureList::FindIndexByLabel(const string &descr) 
{
	vsbPlaneTextureEntry *temp_texture = FindByLabel(descr);
	if (!temp_texture) return -1;
	return temp_texture->Index();
}

int	vsbPlaneTextureList::FindIndexByFilename(const string &fname)
{
	vsbPlaneTextureEntry *temp_texture = FindByFilename(fname);
	if (!temp_texture) return -1;
	return temp_texture->Index();
}
	
const vsbPlaneTextureEntry & vsbPlaneTextureList::operator[](int i) const
{
	static vsbPlaneTextureEntry dummy;

	if (i<0 || i>=mCount)
	{
		char buffer[400];
		sprintf(buffer,"In %s line %d, texture list entry was out of range. i = %d",
			__FILE__, __LINE__, i);
		ulSetError(UL_WARNING,buffer);
		return dummy;
	}

	return *mIndexArray[i];
}
///////////////////////////////////////////////////////////////////////////


vsbPlaneTextureList::~vsbPlaneTextureList()
{
   Clear();
}

void vsbPlaneTextureList::Clear()
{
	map<string, vsbPlaneTextureEntry *>::iterator p; 

	p = mListFilename.begin();


	while (p != mListFilename.end())
	{	
		delete p->second;
		p++;
	}
	mListFilename.clear();
	mListLabel.clear();
	mCount = 0;
}

void vsbPlaneTextureList::LoadList(const string &path)
{
	
    vsbPlaneTextureListParser parser(*this);
	int err_val = parser.ParseFile(path);
	ulSetError(UL_DEBUG,"Loading terrain texture index file: %s",path.c_str());
	if (err_val)
	{
		char buff[400];
		sprintf(buff,"While loading terrain texture index file: \"%s\"\n(%d) %s\noccurred at or near line;\n%03d: %s\n",
			path.c_str(), 
			parser.ItsLastError()->Id(),
			parser.ItsLastError()->Desc(),
			parser.ItsLastError()->Pos(),
			parser.ItsLastError()->Line());
		ulSetError(UL_FATAL, buff);
	}
	return;
}



