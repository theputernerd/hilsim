////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbEnvironmentManager.cpp
    \brief Implements the class vsbEnvironmentManager and its auxillary classes
    
     The class vsbEnvironmentManager is responsible for managing environmental
	 parameters and rendering the scene according to those parameters.

	\sa vsbEnvironmentManager.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#ifdef WIN32
#include <windows.h>
#endif

#include <iostream>
#include <string>
using namespace std;

//#include <stdio.h>

// Note: There is some const_cast'ing done as several constant parameters
//      to plib functions arent defined as const. Hack till such time as
//		plib parameters are updated appropriately;

#include "vsbSun.h"
#include "vsbLightingParams.h"
#include "vsbCloudLayer.h"
#include "vsbGroundPlane.h"
#include "vsbSkyDome.h"
#include "vsbEnvironmentManager.h"
#include "vsbViewportContext.h"

/*! \struct vsbEnvironmentData
\brief Private environment manager data object

Much of the data that could be defined in the vsbEnvironmentManager
class is kept local to the source file in the vsbEnvironmentData structure, 
the reason being to keep it hidden in order to keep the 
vsbEnvironmentManager header clean and to aid in encapsulation
*/
class vsbEnvironmentData
{
friend class vsbEnvironmentManager;
private:	
	vsbEnvironmentData();
	vsbEnvironmentOptions	  mDefaultEnvironmentOpts; //!< Default environment options object 
	vsbRenderOptions	      mDefaultRenderOpts; //!< Default render options object 
	sgCoord					  mCurrViewPos; //!< Cameras current position and orientation

	vsbSkyDome				  mSkyDome; //!< Sky dome object
	vsbSun					  mSun; //!< sun object

	ssgRoot					  mSkyRoot; //!< Root node for the skydome object
	ssgRoot					  mSunRoot; //!< Root node for the sun object

    vsbLightingParams		  mLighting; //!< Scene lighting params

	// Cloud Layer

	int						  mNumCloudLayers;					//!< Number of cloud layers
	vsbCloudLayer			  mCloudLayers[MAX_CLOUD_LAYERS];	//!< The list of cloud layers


	int						  mNumGroundPlanes;					//!< Number of ground planes
	vsbGroundPlane			  mGroundPlanes[MAX_GROUND_PLANES]; //!< The list of ground planes



	bool					  mHaveLightingUpdate; //!< Local flag to indicate new lighting parameters present

};
 

vsbEnvironmentData::vsbEnvironmentData()
{
	mNumCloudLayers = mNumGroundPlanes = 0; 
	mHaveLightingUpdate = true;
}


//! Global instance of vsbEnvironmentData
static vsbEnvironmentData *gsEnv;

vsbEnvironmentManager::vsbEnvironmentManager(vsbEnvironmentOptions *eo, vsbRenderOptions *ro)
{
	gsEnv = new vsbEnvironmentData;
    ssgSetFOV     ( 60.0f, 0.0f ) ;
    ssgSetNearFar ( 1.0f, 80000.0f ) ;

	mScene = new ssgRoot; 
	//mScene = (ssgRoot *) new vsbSortedRoot;
	mScene->ref();

	SetOpts(eo, ro);
	gsEnv->mLighting.Update();
	gsEnv->mSkyRoot.addKid(gsEnv->mSkyDome.Build());
	gsEnv->mSunRoot.addKid(gsEnv->mSun.Build(700));
}

vsbEnvironmentManager::~vsbEnvironmentManager()
{
	ssgDeRefDelete(mScene);
	delete gsEnv;
}

const vsbLightingParams &vsbEnvironmentManager::LightingParams() const
{
	return gsEnv->mLighting;
}

vsbEnvironmentOptions &vsbEnvironmentManager::EnvironmentOpts()
{
	return *mpEnvironmentOpts;
}

vsbRenderOptions &vsbEnvironmentManager::RenderOpts()
{
	return *mpRenderOpts;	
}

void vsbEnvironmentManager::EnvironmentOpts(vsbEnvironmentOptions *eo)
{
	if (eo == 0)
		mpEnvironmentOpts = &gsEnv->mDefaultEnvironmentOpts;
	else
		mpEnvironmentOpts = eo;

}

void vsbEnvironmentManager::RenderOpts(vsbRenderOptions *ro)
{
	if (ro == 0)
		mpRenderOpts = &gsEnv->mDefaultRenderOpts;
	else
		mpRenderOpts = ro;
}

void vsbEnvironmentManager::SetOpts(vsbEnvironmentOptions *eo, vsbRenderOptions *ro)
{
	EnvironmentOpts(eo);
	RenderOpts(ro);
}
	

#define CLOUD(X) (gsEnv->mCloudLayers[sort_idx[X]])

	///////////////////////////////////////////////////////////////////////////
	//
	//  DRAW CLOUDS
	//
	///////////////////////////////////////////////////////////////////////////

void vsbEnvironmentManager::DrawClouds()
{


	if (!mpEnvironmentOpts->CloudOn(*mpRenderOpts))
		return;


	if (mpRenderOpts->FogOn()) 
	{
		glEnable( GL_FOG );
		glHint ( GL_FOG_HINT, (GLenum) mpRenderOpts->FogHint()  );
		glFogfv( GL_FOG_COLOR, const_cast<float *>(gsEnv->mLighting.FogColour()) );
	} else
		glDisable( GL_FOG );


	glDepthMask(false);

	int i;
	
	// The current number of layers
	int curr_cloud_count = gsEnv->mNumCloudLayers; 
    int new_cloud_count;  
	int num_requiring_update;


	// The new number of cloud layers
	new_cloud_count = gsEnv->mNumCloudLayers = mpEnvironmentOpts->NumCloudLayers(); 
		
	if (new_cloud_count < curr_cloud_count)
	{
		// We have fewer clouds this pass than last, so Destroy excess clouds

		for (i = new_cloud_count; i < curr_cloud_count; i++)
			gsEnv->mCloudLayers[i].Destroy();
		num_requiring_update = new_cloud_count;

	} else if (new_cloud_count > curr_cloud_count)
	{	
		// We have more clouds this pass
		for (i = curr_cloud_count; i < new_cloud_count; i++)
        {
		   // copy the cloud parameters and build it
		   gsEnv->mCloudLayers[i] = *mpEnvironmentOpts->CloudLayer(i);

		   // If lighting's not going to be updated for this pass, force
		   // the new clouds to be repainted 

		   if (!gsEnv->mHaveLightingUpdate) 
			   gsEnv->mCloudLayers[i].Repaint(gsEnv->mLighting.AdjFogColour());
        }
		num_requiring_update = curr_cloud_count;
    } else
		num_requiring_update = curr_cloud_count;

	
	// update any parameters of existing clouds that have been modified 
	// this pass

	for (i=0; i < num_requiring_update; i++)
   		if (mpEnvironmentOpts->CloudLayer(i)->IsModified())
			gsEnv->mCloudLayers[i] = *mpEnvironmentOpts->CloudLayer(i);

	
    // update position and colours, if lighting changed, of all clouds

	for (i=0; i < new_cloud_count; i++)
    {
		// Update colours if necessary
		if (gsEnv->mHaveLightingUpdate)
			gsEnv->mCloudLayers[i].Repaint(gsEnv->mLighting.AdjFogColour());

		// Update positions
		gsEnv->mCloudLayers[i].Reposition(gsEnv->mCurrViewPos.xyz);
	}

	//--------------------------------------------------------------------
	//
	// Draw the clouds, noting that clouds below the camera are drawn in 
	// increasing order of altitude, clouds above the camera in decreasing
	// order of altitude, this is donme as transparency is not taken into
	// account by the z buffer so if you draw a cloud closer to the viewer
	// before a further one, even tho the further one should be visible
	// it wont be drawn because the closer one would have registered with
	// the z buffer
	//
	//--------------------------------------------------------------------


  	 const int *sort_idx = mpEnvironmentOpts->CloudSortIdx();

	 float slop   = 5.0;	// If we are closer than this to a cloud layer,
							//   don't draw clouds
	 int in_cloud = -1;		// Cloud we are in
	 float alt      =	gsEnv->mCurrViewPos.xyz[2];
	 int   pos      = 0;

    // check where we are relative to the cloud layers

    for (  i = 0; i < new_cloud_count; i++ ) 
	{
		float asl		= CLOUD(i).Alt()*vsbViewportContext::EnvironmentOpts().VerticalScale();
		float thickness = CLOUD(i).Thickness()*vsbViewportContext::EnvironmentOpts().VerticalScale();

		if ( alt < asl - slop ) 
		{
			pos++; // count the clouds below the camera
			// below cloud layer
		} else if ( alt < asl + thickness + slop ) 
		{
			// in cloud layer
			
			// bail now and don't draw any clouds
			in_cloud = i;
		} else {
			// above cloud layer
		}
    }

    // determine rendering order of the clouds and render then

	pos = new_cloud_count - pos;	
    if ( pos == 0 ) 
	{
		// we are below all the cloud layers, draw top to bottom
		for ( i = new_cloud_count - 1; i >= 0; --i ) 
			if ( i != in_cloud ) 
				CLOUD(i).Draw();

    } else if ( pos >= new_cloud_count ) 
	{
		// we are above all the cloud layers, draw bottom to top
		for ( i = 0; i < new_cloud_count; ++i ) 
			if ( i != in_cloud ) 
				CLOUD(i).Draw();
	
    } else 
	{
		// we are between cloud layers, draw lower layers bottom to
		// top and upper layers top to bottom
		for ( i = 0; i < pos; ++i ) 
			if ( i != in_cloud ) 
				CLOUD(i).Draw();
		
		for ( i = new_cloud_count - 1; i >= pos; --i ) 
			if ( i != in_cloud ) 
				CLOUD(i).Draw();

    }

	glDepthMask(true);
	
}
#undef CLOUD


#define GROUND_PLANE(X) (gsEnv->mGroundPlanes[sort_idx[X]])
void vsbEnvironmentManager::DrawGroundPlanes()
{
	int i;

	
//	glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ) ;

	///////////////////////////////////////////////////////////////////////////
	//
	//  DRAW CLOUDS
	//
	///////////////////////////////////////////////////////////////////////////
	
	// The current number of layers
	int curr_ground_plane_count = gsEnv->mNumGroundPlanes; 
	int num_requiring_update;


	// The new number of groundplane layers
	int new_ground_plane_count = gsEnv->mNumGroundPlanes = mpEnvironmentOpts->NumGroundPlanes(); 
		
	if (new_ground_plane_count < curr_ground_plane_count)
	{
		// We have fewer groundplanes this pass than last, so Destroy excess groundplanes

		for (i = new_ground_plane_count; i < curr_ground_plane_count; i++)
			gsEnv->mGroundPlanes[i].Destroy();

		num_requiring_update = new_ground_plane_count;

	} else if (new_ground_plane_count > curr_ground_plane_count)
	{	
		// We have more groundplanes this pass
		for (i = curr_ground_plane_count; i < new_ground_plane_count; i++)
        {
		   // copy the groundplane parameters and build it
		   gsEnv->mGroundPlanes[i] = *mpEnvironmentOpts->GroundPlane(i);

		   // If lighting's not going to be updated for this pass, force
		   // the new groundplanes to be repainted 

		   if (!gsEnv->mHaveLightingUpdate) 
			   gsEnv->mGroundPlanes[i].Repaint(gsEnv->mLighting.AdjFogColour());
        }

		num_requiring_update = curr_ground_plane_count;
    } else
		num_requiring_update = curr_ground_plane_count;

	// update any parameters of existing groundplanes that have been modified 
	// this pass

	for (i=0; i < num_requiring_update; i++)
   		if (mpEnvironmentOpts->GroundPlane(i)->IsModified())
			gsEnv->mGroundPlanes[i] = *mpEnvironmentOpts->GroundPlane(i);

	
    // update position and colours, if lighting changed, of all groundplanes

	for (i=0; i < new_ground_plane_count; i++)
    {
		// Update colours if necessary
		if (gsEnv->mHaveLightingUpdate)
			gsEnv->mGroundPlanes[i].Repaint(gsEnv->mLighting.AdjFogColour());

		// Update positions
		gsEnv->mGroundPlanes[i].Reposition(gsEnv->mCurrViewPos.xyz);
	}

	//--------------------------------------------------------------------
	//
	// Draw the groundplanes, noting that groundplanes below the camera are drawn in 
	// increasing order of altitude, groundplanes above the camera in decreasing
	// order of altitude, this is donme as transparency is not taken into
	// account by the z buffer so if you draw a groundplane closer to the viewer
	// before a further one, even tho the further one should be visible
	// it wont be drawn because the closer one would have registered with
	// the z buffer
	//
	//--------------------------------------------------------------------


  	 const int *sort_idx = mpEnvironmentOpts->GroundPlaneSortIdx();

	 float alt      =	gsEnv->mCurrViewPos.xyz[2];
	 int   pos      = 0;

    // check where we are relative to the groundplane layers

    for (  i = 0; i < new_ground_plane_count; i++ ) 
	{
		float asl		= GROUND_PLANE(i).Alt();

		if ( alt < asl ) 
		{
			pos++; // count the groundplanes below the camera
			// below groundplane layer
		} else {
			// above groundplane layer
			break;
		}
    }

    // determine rendering order of the groundplanes and render then

	pos = new_ground_plane_count - pos;	
    if ( pos == 0 ) 
	{
		// we are below all the groundplane layers, draw top to bottom
		for ( i = new_ground_plane_count - 1; i >= 0; --i ) 
				GROUND_PLANE(i).Draw();

    } else if ( pos >= new_ground_plane_count ) 
	{
		// we are above all the groundplane layers, draw bottom to top
		for ( i = 0; i < new_ground_plane_count; ++i ) 
				GROUND_PLANE(i).Draw();
	
    } else 
	{
		// we are between groundplane layers, draw lower layers bottom to
		// top and upper layers top to bottom
		for ( i = 0; i < pos; ++i ) 
				GROUND_PLANE(i).Draw();
		
		for ( i = new_ground_plane_count - 1; i >= pos; --i ) 
				GROUND_PLANE(i).Draw();

    }
	
}
#undef GROUND_PLANE

	   	
void vsbEnvironmentManager::PreDraw(const sgCoord &viewPos)
{

	gsEnv->mHaveLightingUpdate = false;
	float effective_visibility;

	sgCopyCoord(&gsEnv->mCurrViewPos, &viewPos);
	

	//---------------------------------------------------------------
	//
	// Setup weather visibility 
	//
	//---------------------------------------------------------------

	// visibility setting affect dome colouring so if weather is turned
	// off make sure efective visibility is for all i.a.p. infinite
	// if weather is on but fog is off, still colour the sky dome appropriately

	if (mpEnvironmentOpts->WeatherOn())
		effective_visibility = mpEnvironmentOpts->AtmosphericVisibility();
	else
		effective_visibility = 1.0e6f;


	//---------------------------------------------------------------
	//
	// Recalculate lighting and sky painting parameters if necessary
	//
	//---------------------------------------------------------------

	float tod_degrees = mpEnvironmentOpts->TimeOfDay()/24.0f * 360.0f; 
	float toy_degrees = mpEnvironmentOpts->TimeOfYear()/365.0 * 47 - 23.5;
	if (tod_degrees != gsEnv->mLighting.SunAngle() || toy_degrees != gsEnv->mLighting.SunDeclinationNorth())
	{
	   gsEnv->mHaveLightingUpdate = true;	 
	   gsEnv->mLighting.Update(tod_degrees, toy_degrees);
    }

	//---------------------------------------------------------------
	//
	// Draw plain Sky or Skydome as necessary
	//
	//---------------------------------------------------------------

	// Clear Viewport

	bool sky_dome_on = mpEnvironmentOpts->SkyDomeOn(*mpRenderOpts);
	const GLfloat	*clear_colour;

	if (sky_dome_on)
        clear_colour = gsEnv->mLighting.AdjFogColour();
	else
		clear_colour = gsEnv->mLighting.SkyColour();

	glClearColor(clear_colour[0],clear_colour[1],clear_colour[2],clear_colour[3]);
	glClear  ( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) ;

	// Set up preliminary sky drawing state

//	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	glDisable( GL_DEPTH_TEST );
    glDisable( GL_FOG );
    glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ) ;
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, (GLenum) mpRenderOpts->PerspectiveCorrectionHint()) ;

   	// Updatew SkyDome

	if (sky_dome_on)
    {
		gsEnv->mSkyDome.Reposition(viewPos.xyz);
		gsEnv->mSkyDome.Repaint(gsEnv->mLighting.SkyColour(), gsEnv->mLighting.AdjFogColour(),
							   tod_degrees, effective_visibility);


		// Draw Skydome 

		ssgCullAndDraw ( &gsEnv->mSkyRoot ) ;
	}

	
	//---------------------------------------------------------------
	//
	// Draw sun and set up sun-lighting
	//
	//---------------------------------------------------------------

	
	// Update SunPosition -- need to always update for the lighting parameter

	gsEnv->mSun.Repaint(tod_degrees, toy_degrees, 39999);
    gsEnv->mSun.Reposition(viewPos.xyz);


	// Draw the sun if sun drawing is enabled

	if (mpEnvironmentOpts->SunOn(*mpRenderOpts))
	   ssgCullAndDraw ( &gsEnv->mSunRoot ) ;


	glEnable( GL_LIGHTING );

	//glLightModelfv( GL_LIGHT_MODEL_AMBIENT, const_cast<float *>(gsEnv->mLighting.Ambient()));
	GLfloat black[4] = { 0.0, 0.0, 0.0, 1.0 };
    ssgGetLight ( 0 ) -> setPosition ( gsEnv->mSun.GetSunPos().xyz ) ;
    //ssgGetLight ( 0 ) -> setColour(GL_AMBIENT,black);
	ssgGetLight ( 0 ) -> setColour(GL_AMBIENT,const_cast<float *>(gsEnv->mLighting.Ambient()));
    ssgGetLight ( 0 ) -> setColour(GL_DIFFUSE,const_cast<float *>(gsEnv->mLighting.Diffuse()));
    ssgGetLight ( 0 ) -> setup();

	//---------------------------------------------------------------
	//
	// Setup weather  related fog
	//
	//---------------------------------------------------------------

	static GLfloat last_visibility = -1;

	if ( effective_visibility != last_visibility ) 
	{
		last_visibility = effective_visibility;

		GLfloat fog_exp_density;
		GLfloat fog_exp2_density;

		// for GL_FOG_EXP
		fog_exp_density = -log(0.01 / effective_visibility);

		// for GL_FOG_EXP2
		fog_exp2_density = sqrt( -log(0.01) ) / effective_visibility ;

		// Set correct opengl fog density
		glFogf (GL_FOG_DENSITY, fog_exp2_density);
		glFogi( GL_FOG_MODE, GL_EXP2 );

	
		glFogfv( GL_FOG_COLOR, const_cast<float *>(gsEnv->mLighting.FogColour()) );
	}

	if (mpRenderOpts->FogOn()) 
	{
		glEnable( GL_FOG );
		glHint ( GL_FOG_HINT, (GLenum) mpRenderOpts->FogHint()  );
		glFogfv( GL_FOG_COLOR, const_cast<float *>(gsEnv->mLighting.FogColour()) );
    } 

	glEnable( GL_DEPTH_TEST );

	// Draw ground plane/s

	if (mpEnvironmentOpts->GroundPlaneOn(*mpRenderOpts))
		DrawGroundPlanes();


    if (EnvironmentOpts().ChunkLODTerrainOn())
		ChunkLODTerrain().Draw();

	if (mpEnvironmentOpts->CloudPreDrawModeOn()) 
		DrawClouds();

}

void vsbEnvironmentManager::PostDraw()
{
	glEnable( GL_DEPTH_TEST );

	if (!mpEnvironmentOpts->CloudPreDrawModeOn()) 
		DrawClouds();
}

void vsbEnvironmentManager::ForceDefaultGLState()
{
  static ssgSimpleState default_state;
  static bool first = true;
  if (first)
  {	
	  default_state.enable( GL_TEXTURE_2D );
	  default_state.enable( GL_CULL_FACE );
	  default_state.enable( GL_COLOR_MATERIAL );
	  default_state.setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
	  default_state.setMaterial( GL_EMISSION, 0, 0, 0, 1 );
	  default_state.setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
	  default_state.disable( GL_BLEND );
	  default_state.disable( GL_ALPHA_TEST );
	  default_state.enable( GL_LIGHTING );
	  first = false;
  }
  default_state.force();

}


void vsbEnvironmentManager::ReshapeViewport ( int w, int h )
{
//  win_w = w; win_h = h;
  glViewport ( 0, 0, w, h ) ;
}

void vsbEnvironmentManager::RedrawBuffer(sgdCoord &campos)
{
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

	sgCoord cp;
	cp.xyz[0] = campos.xyz[0];
	cp.xyz[1] = campos.xyz[1];
	cp.xyz[2] = campos.xyz[2];
	cp.hpr[0] = campos.hpr[0];
	cp.hpr[1] = campos.hpr[1];
	cp.hpr[2] = campos.hpr[2];

  ssgSetCamera ( & cp ) ;

  PreDraw(cp);
 
  glEnable( GL_LIGHTING );
  glEnable ( GL_DEPTH_TEST ) ;

  if (mpRenderOpts->FogOn()) 
	{
		glEnable( GL_FOG );
		glHint ( GL_FOG_HINT, (GLenum) mpRenderOpts->FogHint()  );
		glFogfv( GL_FOG_COLOR, const_cast<float *>(gsEnv->mLighting.FogColour()) );
	} else
		glDisable( GL_FOG );

  //static int xx;
  //cout << "<<<<    " << xx++;
  if (mpRenderOpts->GLNormalizeOn())
	  glEnable(GL_NORMALIZE);
	  
  ssgCullAndDraw ( mScene ); 
  //cout << ">>>>>\n"; 	

  glDisable(GL_NORMALIZE);

  PostDraw();
}

void vsbEnvironmentManager::RedrawBuffer( sgdMat4 &cammat)
{
	sgdCoord camcoord;
    sgdSetCoord ( &camcoord, cammat );

    RedrawBuffer(camcoord);
}

void vsbEnvironmentManager::AddSceneChild(ssgEntity *entity)
{
	mScene->addKid( entity ); 
}


void	vsbEnvironmentManager::SelectChunkLODTerrain(const std::string &name)
{
	if (name!= mChunkLODElevation.LogicalName())
	{
		//MessageBeep(-1); Sleep(50);
		mChunkLODElevation = name;
		/*
		const vsbChunkLODElevationEntry *found = vsbViewportContext::ChunkLODElevationManager().Find(name);
		if (found)
		{
			mChunkLODElevation = *found;
		}
		*/

	}
}