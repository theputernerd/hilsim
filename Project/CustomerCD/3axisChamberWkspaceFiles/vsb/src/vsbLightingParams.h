//////////////////////////////////////////////////////////////////
/*! 
    \file vsbLightingParams.h
    \brief Defines the class vsbLightingParams  
	
	The vsbLightingParams class maintains a record of the current sun 
    angle and declination and can be used to determine the lighting and 
    colours of the sky sun and other atmospheric phenomena    

	\sa vsbLightingParams.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbLightingParams_H
#define _vsbLightingParams_H

#ifndef __cplusplus                                                          
# error This library requires C++
#endif                                   

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

//////////////////////////////////////////////////////////////////
/*! 
    \class vsbLightingParams
    \brief Define a structure containing the global lighting parameters

	The vsbLightingParams class maintains a record of the current sun 
    angle and declination and can be used to determine the lighting and 
    colours of the sky sun and other atmospheric phenomena    
*/
//////////////////////////////////////////////////////////////////


class DLL_VSB_API vsbLightingParams 
{

public:
	
	//! Default constructor
	vsbLightingParams( );

	//! Virtual destructor
    virtual ~vsbLightingParams( );

    /*! Update the vsbLightingParams's recorded sun state
        \param sunAngle The sun angle in degrees
        \param sunDeclinationN The sun's northern declination angle in degrees
    */
	void Update(float sunAngle, float sunDeclinationN);

	//! Update vsbLightingParam's internal lighting parameters based the current sun position
    void Update( void);

    //! Get the ambient scene light    
	const GLfloat *Ambient() const		{return mSceneAmbient;};

    //! Get the diffuse scene light 
	const GLfloat *Diffuse() const		{return mSceneDiffuse;};

    //! Get the background sky colour 
	const GLfloat *SkyColour() const	{return mSkyColour;};

    //! Get the fog colour
	const GLfloat *FogColour() const	{return mFogColour;};

    //! Get the fog colour adjusted for sunrise/sunset effects
	const GLfloat *AdjFogColour() const {return mAdjFogColour;};

    //! Get the sun angle               
    GLfloat        SunAngle() const				{ return (GLfloat) mSunAngle; }

    //! Get the sun northern declination angle
	GLfloat		   SunDeclinationNorth() const	{ return (GLfloat) mSunDeclinationNorth;};

    //! Set the sun angle
	void           SunAngle(float angle) 	    { mSunAngle = angle; };

    //! Set the sun northern declination angle
	void		   SunDeclinationNorth(float angle)  {mSunDeclinationNorth = angle;};

private:

    ///////////////////////////////////////////////////////////
    // position of the sun in various forms

    // the angle between the sun and the local horizontal (in degrees)

    float mSunAngle;               //!< The sun angle
	float mSunDeclinationNorth;    //!< The sun northern declination angle

        
    ///////////////////////////////////////////////////////////
    // Derived lighting values

    //! Ambient component
    GLfloat mSceneAmbient[4];

    //! Diffuse component
    GLfloat mSceneDiffuse[4];

    //! Fog color
    GLfloat mFogColour[4];

    //! Fog color adjusted for sunset effects
    GLfloat mAdjFogColour[4];

    //! Clear screen color
    GLfloat mSkyColour[4];

    //! Initialize lighting tables
    void Init( void );

    //! Calculate fog color adjusted for sunrise/sunset effects
    void AdjForSunriseOrSet( void );


};

// Global shared light parameter structure
extern vsbLightingParams gCurrLightParams;


#endif // _LIGHT_HXX
