//
/*
In the F18 hud we assume the 8 deg circle limiting velocitiy vector is 25 diameter
so we work out eye range from hud as  25/tan(8 deg). Solving for range, we get
177.88424305960521870576415359079
At 6 deg diameter = 18.6963

*/

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
using namespace std;

#ifdef WIN32
#include <windows.h>
#endif

#include "math.h"

#include<GL/gl.h>

#include "fnt.h"
#include "pu.h"
#include "vsbSysPath.h"
#include "vsbHUDComponents.h"
#include "vsbStrokedText.h"

#include "vsbViewportContext.h"
#include "vsbFollowCamera.h"

#include "tool_mathtools.h"


const float SMALL_FLOAT = 0.1f;

//	const double M_TO_FT = .28083989501; // This conversion is the wrong way!!
//	const double MPS_TO_KTS = 1.94384449244;

// static float CalcAngleToTarget( const sgVec3 targetPos, const vsbBaseCamera *pCam );


vsbHeadingHUD::vsbHeadingHUD():vsbHUDComponent()
{
    mpHeading = 0;
    mX = 30;
    mY = 85;
    mW = 40, mH = 4;
    mMajorTickDeg = 10;
    mMinorTickDeg = mMajorTickDeg/2;
    mAngleRange = 30;
}

const char * vsbHeadingHUD::Name() const 
{
  static char *name = "HeadingHUD";
  return name;
};    
  

void vsbHeadingHUD::SetRectangle(float x, float y, float w, float h)
{
    mX = x;  mY = y;   mW = w;    mH = h;
}

void vsbHeadingHUD::SetMarkings(float rangeDeg, float majorTickDeg)
{
    mAngleRange = rangeDeg;
    mMajorTickDeg = majorTickDeg;
    mMinorTickDeg = mMajorTickDeg/2;
}
  
void  vsbHeadingHUD::Draw()
{
	// Pre-Draw stuff.
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );

	
	// End-PreDraw stuff.


    float val = mpHeading?*mpHeading:0.0f;
    float val0 = val - mAngleRange/2 - SMALL_FLOAT;
    float val1 = val + mAngleRange/2 + SMALL_FLOAT;

    while (val0<0) 
    {
        val0+=360;
        val1+=360;
    }

    float startMajor = val0 - fmod(val0,mMajorTickDeg) + mMajorTickDeg;
    float startMinor = val0 - fmod(val0,mMinorTickDeg) + mMinorTickDeg;
    if (startMajor <= startMinor) startMinor+=mMinorTickDeg; 


    while(startMinor<=val1)
    {
        float xoffs = mX+mW*(startMinor-val0)/mAngleRange;
        glBegin(GL_LINE_STRIP);
        glVertex3f(xoffs, mY,  0);
        glVertex3f(xoffs,mY+mH/2, 0);
        glEnd();
        startMinor+=mMinorTickDeg;
    }

    mStrokedText.Scale(3);
    mStrokedText.Justify(JUST_MIDDLE, JUST_LOWER); 

    while(startMajor<=val1)
    {
        int displ_val = (int) startMajor;
        while (displ_val < 0) displ_val+=360;
        while (displ_val >=360) displ_val-=360;

        char buff[40];
        float xoffs = mX+mW*(startMajor-val0)/mAngleRange;
        glBegin(GL_LINE_STRIP);
        glVertex3f(xoffs, mY,  0);
        glVertex3f(xoffs,mY+ 0.8*mH, 0);
        glEnd();
        sprintf(buff,"%03d",displ_val);
        mStrokedText.DrawStr(buff,xoffs,mY+mH);

        startMajor+=mMajorTickDeg;
        
    }

  
    float carat = mH/3;
    glBegin(GL_LINE_STRIP);
    glVertex3f(mX+mW/2-carat/2, mY-carat,  0);
    glVertex3f(mX+mW/2, mY,  0);
    glVertex3f(mX+mW/2+carat/2, mY-carat,  0);
    glEnd();

	glPopMatrix();

}


///////////////////////////////


vsbAltAirspeedHUD::vsbAltAirspeedHUD():vsbHUDComponent()
{
    mX = 0;
    mY = 0;
    mW = 0;
    mH = 0;
    mBW = 0;
    mpAltitude = 0;
    mpAirspeed = 0;
}

const char * vsbAltAirspeedHUD::Name() const 
{
  static char *name = "AltAirspeedHUD";
  return name;
};    
  

void vsbAltAirspeedHUD::SetRectangle(float x, float y, float w, float h, float b_w)
{
    mX = x;  mY = y;   mW = w;    mH = h; mBW = b_w;
}


void vsbAltAirspeedHUD::Draw()
{
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


    char buff[40];
    int alt_low, alt_high;
    float text_w, text_h;

    glBegin(GL_LINE_STRIP);
    glVertex3f(mX, mY,  0);
    glVertex3f(mX, mY+mH,  0);
    glVertex3f(mX+mBW, mY+mH,  0);
    glVertex3f(mX+mBW, mY,  0);
    glVertex3f(mX, mY,  0);
    glEnd();

    glBegin(GL_LINE_STRIP);
    glVertex3f(mX+mW, mY,  0);
    glVertex3f(mX+mW, mY+mH,  0);
    glVertex3f(mX+mW-mBW, mY+mH,  0);
    glVertex3f(mX+mW-mBW, mY,  0);
    glVertex3f(mX+mW, mY,  0);
    glEnd();

    mStrokedText.Scale(3);
    mStrokedText.Justify(JUST_UPPER, JUST_LOWER);

    alt_high = (int)(mpAltitude?(*mpAltitude)*tool::constants::M_TO_FT:0);
    alt_low = abs(alt_high) % 1000;
    alt_high = alt_high/1000;
    sprintf(buff,"%03d",alt_low);
    mStrokedText.TextDimensions(buff, text_w, text_h);
    mStrokedText.DrawStr(buff,mX+mW-0.5,mY+0.5);

    mStrokedText.Scale(5);
    mStrokedText.Justify(JUST_UPPER, JUST_LOWER);
    sprintf(buff,"%0d",alt_high);
    mStrokedText.DrawStr(buff,mX+mW-text_w-0.8,mY+0.5);

//    sprintf(buff,"%0d",(int)(mpAirspeed?(*mpAirspeed)*tool::constants::MPS_TO_KTS:0));
    sprintf(buff,"%0d",(int)(mpAirspeed?*mpAirspeed:0));
    mStrokedText.DrawStr(buff,mX+mBW-0.5,mY+0.5);

	glPopMatrix();
}


///////////////////////////////


vsbAttitudeHUD::vsbAttitudeHUD():vsbHUDComponent()
{
    mX = 0;
    mY = 0;
    mpAlpha = 0;
    mpMach = 0;
    mpG = 0;
    mPeakG = 0;
}

const char * vsbAttitudeHUD::Name() const 
{
  static char *name = "AttitudeHUD";
  return name;
};  


void vsbAttitudeHUD::Draw()
{
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


    char buff[40];
    float text_w, text_h;

    mStrokedText.Scale(3);
    mStrokedText.Justify(JUST_UPPER, JUST_LOWER);
    mStrokedText.TextDimensions("X", text_w, text_h);
    text_h*=1.3f;

    mStrokedText.DrawStr("~",mX,mY);
    mStrokedText.DrawStr("M",mX,mY-text_h);
    mStrokedText.DrawStr("G",mX,mY-text_h*2);

    mStrokedText.Justify(JUST_LOWER, JUST_LOWER);

    sprintf(buff,"%0.1f",mpAlpha?*mpAlpha:0);
    mStrokedText.DrawStr(buff,mX+text_w*3,mY);

    sprintf(buff,"%0.2f",mpMach?*mpMach:0);
    mStrokedText.DrawStr(buff,mX+text_w*3,mY-text_h);

    sprintf(buff,"%0.1f",mpG?*mpG:0);
    mStrokedText.DrawStr(buff,mX+text_w*3,mY-text_h*2);

    if (mPeakG > 4.0)
    {
        sprintf(buff,"%0.1f",mPeakG);
        mStrokedText.DrawStr(buff,mX+text_w*3,mY-text_h*3);
    }
    
   // mStrokedText.DrawStr(buff,mX+mBW-0.5,mY+0.5);

	glPopMatrix();
}    


///////////////////////////////


vsbPitchLadderHUD::vsbPitchLadderHUD():vsbHUDComponent()
{
   mpWatermarkOn = 0;
   mpPitch = 0;
   mpRoll = 0;
   mpXCentre = 0; 
   mpYCentre = 0;
   mWaterMarkX = 0;
   mWaterMarkY = 0;
   mWidth = 60;
   mHeight = 95;
   mAngleRange = 20;
   mTickDeg = 5;
   mWaterMarkW = 5;
   mWaterMarkH = 2;
}

const char * vsbPitchLadderHUD::Name() const 
{
  static char *name = "PitchLadderHUD";
  return name;
};  

void  vsbPitchLadderHUD::SetMarkings(float rangeDeg, float tickDeg)
{
    mAngleRange = rangeDeg;
    mTickDeg = tickDeg;
}

void  vsbPitchLadderHUD::HookPitchLadder(double *pitchDeg, double *rollDeg, float *xCentre, float *yCentre, bool *watermark, bool *steepness)
{
    mpPitch = pitchDeg; 
    mpRoll = rollDeg; 
    mpXCentre = xCentre; 
    mpYCentre = yCentre;
    mpWatermarkOn = watermark;
    mLadderSteepnessOn = steepness;
};

void  vsbPitchLadderHUD::SetGeometryPos(float waterMarkX, float waterMarkY, float waterMarkW, float waterMarkH, float ladderWidth, float ladderHeight)
{
   mWaterMarkX = waterMarkX;
   mWaterMarkY = waterMarkY;
   mWaterMarkW = waterMarkW;
   mWaterMarkH = waterMarkH;
   mWidth = ladderWidth;
   mHeight = ladderHeight;
}


void vsbPitchLadderHUD::Draw()
{
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


    bool watermark_on;
    if (!mpWatermarkOn) 
        watermark_on = true;
    else
        watermark_on = *mpWatermarkOn;

    if (watermark_on)
    {
        glBegin(GL_LINE_STRIP);
        glVertex3f(mWaterMarkX-mWaterMarkW/2, mWaterMarkY,  0);
        glVertex3f(mWaterMarkX-mWaterMarkW/4, mWaterMarkY,  0);
        glVertex3f(mWaterMarkX-mWaterMarkW/8, mWaterMarkY-mWaterMarkH, 0);
        glVertex3f(mWaterMarkX, mWaterMarkY,  0);
        glVertex3f(mWaterMarkX+mWaterMarkW/8, mWaterMarkY-mWaterMarkH,  0);
        glVertex3f(mWaterMarkX+mWaterMarkW/4, mWaterMarkY,  0);
        glVertex3f(mWaterMarkX+mWaterMarkW/2, mWaterMarkY,  0);
        glEnd();

    }  
	
	glPushMatrix();

	float roll = mpRoll?*mpRoll:0;

	if (watermark_on)
		glTranslatef(mWaterMarkX, mWaterMarkY, 0.0f);
	else
		glTranslatef(*mpXCentre, *mpYCentre, 0.0f);

	glRotatef(roll, 0.0f, 0.0f, 1.0f);
	

	DrawLadder();

	glPopMatrix();

	//vsbDrawCircle(50,50, 25, 5);

	//vsbDrawCircle(mWaterMarkX, mWaterMarkY, 18.6963, 5);

	glPopMatrix();

}


void  vsbPitchLadderHUD::DrawLadder()
{
    int sign = 1;
    float pitch = ToRad(mpPitch?*mpPitch:0);
    float roll = ToRad(mpRoll?*mpRoll:0);
    

    float x = sgCos(pitch); float y = sgSin(pitch);
    if (x<0)
    {
        sign = -1;
        x = fabs(x);
        roll = roll+Pi;
    }

    float adjusted_angle = sgATan2(y,x);

    if (adjusted_angle >= TwoPi-PiOnTwo)
        adjusted_angle-= TwoPi;

    adjusted_angle = ToDeg(adjusted_angle);
   
    float val0 = adjusted_angle - mAngleRange/2;
    float val1 = adjusted_angle + mAngleRange/2;


    float angle_incr = val0 - fmod(val0,mTickDeg); 


	while(angle_incr <= val0) 
		angle_incr+=mTickDeg;

    while(angle_incr < val1)
    {
        DrawLadderStep(angle_incr, adjusted_angle, val0, val1);
        angle_incr+=mTickDeg;
    }

 



}

void vsbPitchLadderHUD::DrawLadderStep(float rung_angle_deg, float angle_deg, float val0, float val1)
{
	char buff[20];
	float yoffs = mHeight*(rung_angle_deg-val0)/mAngleRange - mHeight/2;
	float a_sixth_w = mWidth/6;
	float a_half_w = mWidth/2;
    float arm_w = mWidth/2-a_sixth_w;

    float dx;
    float dy;


	
	bool over_pitch = false;  // flag indicating that this rung is for angle on 
							  // other side of vertical which is represented upside-down
	bool pitch_down = false;  // flag indicating we have pitch down - used for rung stipple selection

	// Adjust for overpitch 
	if (rung_angle_deg > 90)
	{
		rung_angle_deg = 180 - rung_angle_deg;
		over_pitch = true;
	} else 
	if (rung_angle_deg < -90)
	{
		rung_angle_deg = -180 - rung_angle_deg;
		over_pitch = true;
	}
		
	static float text_w;
	static float text_h;
	static float text_offs = 0; // Pitch ladder rung annotation accomodation offset size

	if (text_offs == 0)  // Calculate once only as is unchangeing
	{
		mStrokedText.Scale(3);
		mStrokedText.TextDimensions("00", text_w, text_h); // two digit annotation
		text_offs = text_w * 1.5;						   // leave some extra space	
	}

    mStrokedText.Justify(JUST_UPPER, JUST_LOWER);		
	
	pitch_down = rung_angle_deg < 0;	// set the pitch down flag according to the angle	


	

	sprintf(buff,"%02d", int(fabs(rung_angle_deg)));

    if (mLadderSteepnessOn)
    {
        float angle_rad = ToRad(/*fabs(*/angle_deg/*)*/);
        dx = sgCos(angle_rad/2);
        dy = sgSin(angle_rad/2);
    } else
        dx = dy = 1.0f;

	if (rung_angle_deg > - SMALL_FLOAT && rung_angle_deg < SMALL_FLOAT)
	{
        float x_arm_length = arm_w*dx;
        float y_arm_length = arm_w*dy;
		// Left arm of ladder marking
		glBegin(GL_LINE_STRIP);
		glVertex3f(-a_half_w, yoffs-text_h,  0);
		glVertex3f(-a_half_w, yoffs,  0);
		//glVertex3f(-a_sixth_w, yoffs,  0);
        glVertex3f(-a_half_w+ x_arm_length, yoffs - y_arm_length, 0);
		glEnd();

        

		// Right arm of ladder marking
		glBegin(GL_LINE_STRIP);
		glVertex3f(a_half_w, yoffs-text_h,  0);
		glVertex3f(a_half_w, yoffs,  0);
        glVertex3f(a_half_w - x_arm_length, yoffs - y_arm_length, 0);
		//glVertex3f(a_sixth_w, yoffs,  0);
		glEnd();
	} else
	if (rung_angle_deg > 90 - SMALL_FLOAT && rung_angle_deg < 90 + SMALL_FLOAT)
	{
		// Draw vert up marker
         vsbDrawCircle(0, yoffs, 3.8f, 5);
         

	} else
	if (rung_angle_deg < -90 + SMALL_FLOAT && rung_angle_deg > -90 - SMALL_FLOAT)
	{
        // draw vert down marker
        vsbDrawCircle(0,yoffs, 4, 5);
		
        glBegin(GL_LINE_STRIP);
        glVertex3f(-2.4f, yoffs-2.4,  0);
        glVertex3f(2.4f, yoffs+2.4,  0);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glVertex3f(2.4f, yoffs-2.4,  0);
        glVertex3f(-2.4f, yoffs+2.4,  0);
        glEnd();
	}
	else 
	{
		float tail_offs[2] = {text_h, -text_h};
        float tail_dir[2] = {1.0, -1.0};
		int do_flip = int((pitch_down && !over_pitch) || (!pitch_down && over_pitch));

 
		mStrokedText.Scale(3);

 		// Left tail & label of ladder markng
		glBegin(GL_LINE_STRIP);
		glVertex3f(-a_half_w+text_offs, yoffs-tail_offs[do_flip],  0);
		glVertex3f(-a_half_w+text_offs, yoffs,  0);
		glEnd();

		mStrokedText.Justify(JUST_UPPER, (do_flip)?JUST_LOWER:JUST_UPPER);
		mStrokedText.DrawStr(buff,-a_half_w+text_w, yoffs, over_pitch);
		
		// Right tail & label of ladder markng

		glBegin(GL_LINE_STRIP);
		glVertex3f(a_half_w-text_offs, yoffs-tail_offs[do_flip],  0);
		glVertex3f(a_half_w-text_offs, yoffs,  0);
		glEnd();

		mStrokedText.Justify(JUST_LOWER, (do_flip)?JUST_LOWER:JUST_UPPER);
		mStrokedText.DrawStr(buff,a_half_w-text_w, yoffs, over_pitch);


		if (pitch_down)
		{
			glEnable(GL_LINE_STIPPLE);
			glLineStipple(4,0x5555);
		}

        
        float x_arm_length = (arm_w-text_offs)*dx;
        float y_arm_length = (arm_w-text_offs)*dy;

        // Left arm of ladder markng
		
		glBegin(GL_LINE_STRIP);
		glVertex3f(-a_half_w+text_offs, yoffs,  0);
 //       glVertex3f(-a_half_w+text_offs+x_arm_length, yoffs-y_arm_length*tail_dir[do_flip],  0);
		glVertex3f(-a_half_w+text_offs+x_arm_length, yoffs-y_arm_length*tail_dir[over_pitch],  0);
		glEnd();


		// Right arm of ladder marking
		glBegin(GL_LINE_STRIP);
		glVertex3f(a_half_w-text_offs, yoffs,  0);
//        glVertex3f(a_half_w-text_offs-x_arm_length, yoffs-y_arm_length*tail_dir[do_flip],  0);
		glVertex3f(a_half_w-text_offs-x_arm_length, yoffs-y_arm_length*tail_dir[over_pitch],  0);
		glEnd();

		glDisable(GL_LINE_STIPPLE);

	}



}


/////////////////////////////////////

vsbBankHUD::vsbBankHUD():vsbHUDComponent()
{
    mpBank = 0;
    mX = 0;
    mY = 0;
    mRadius = 50;
    mTickLen = 3;
}


const char * vsbBankHUD::Name() const 
{
  static char *name = "BankHUD";
  return name;
};  


void  vsbBankHUD::Draw()
{
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


    const float limit =  47;
    const float ticksize = 3;

    float angle;
    float c;
    float s;

    int i;
//    static float angles[] = {ToRad(5), ToRad(15) ,ToRad(30), ToRad(45)};
    static float angles[] = {5.0f, 15.0f, 30.0f, 45.0f };
    if (mpHUDManager == 0 || IsEnabled() == false) return;

//	glBegin(GL_LINE_STRIP);
//	glVertex3f(mX, mY+mRadius,  0);
//	glVertex3f(mX, mY+mRadius+mTickLen,  0);
//	glEnd();
    for (i=0; i<4; i++)
    {
//        angle = angles[i]-Pi/2;
        angle = angles[i]-90.0f;
        c = sgCos(angle);
        s = sgSin(angle);
        glBegin(GL_LINE_STRIP);
        glVertex3f(mX+c*mRadius, mY+s*mRadius,  0);
        glVertex3f(mX+c*(mRadius+mTickLen), mY+(mRadius+mTickLen)*s,  0);
        glEnd();
        glBegin(GL_LINE_STRIP);
        glVertex3f(mX-c*mRadius, mY+s*mRadius,  0);
        glVertex3f(mX-c*(mRadius+mTickLen), mY+(mRadius+mTickLen)*s,  0);
        glEnd();
    }
	glBegin(GL_LINE_STRIP);
    glVertex3f(mX, mY-mRadius,  0);
    glVertex3f(mX, mY-mRadius-mTickLen,  0);
    glEnd();

    angle = mpBank?*mpBank:0.0;
    while (angle > 180.0f) angle -=360.0f;
    while (angle < -180.0f) angle +=360.0f;
    if (angle < -limit) angle = -limit;
        else 
            if (angle > limit) angle = limit;

//    angle = ToRad(angle-90);
    angle = angle-90.0f;
    c = sgCos(angle);
    s = sgSin(angle);

    vsbDrawArrow(mX+c*mRadius,mY+s*mRadius, ToRad(angle)+Pi, ticksize);
    
	glPopMatrix();
}


///////////////////////////////////////////////////////////////////////////////
//
// Velocity Vector, HUD component
//

vsbVelocityVectorHUD::vsbVelocityVectorHUD():vsbHUDComponent()
{
//	mX = mY = 0;
	mVX = mVY = 0;
	mRadius = 25.0f;
//	mRadiusAngle = 10;
	mIconRadius = 2;

	mpHeading = mpPitch = mpRoll = 0;
	mpU = mpV = mpW = 0;

	mCaged = 1;

//    SetPos(0, 0, 10, 8); 
}

     
const char * vsbVelocityVectorHUD::Name() const 
{
  static char *name = "VelocityVector";
  return name;
};  


void vsbVelocityVectorHUD::HookVelocityVector(	
	double *headingDeg, double *pitchDeg, double *rollDeg, 
	double *u, double *v, double *w )
{
    mpHeading = headingDeg;
	mpPitch = pitchDeg;
	mpRoll = rollDeg;
	mpU = u;
	mpV = v;
	mpW = w;
}
/*
void vsbVelocityVectorHUD::SetPos(float x, float y, float r, float r_angle_deg) 
{
	mX = x; mY = y; 
	mRadius = r;
	mRadiusAngle= r_angle_deg;
    mEyeDistance = mRadius/tan(ToRad(mRadiusAngle));
}
*/
void vsbVelocityVectorHUD::GetVectorPos(float &x, float &y)
{
	x = mVX;
	y = mVY;
}

void vsbVelocityVectorHUD::DrawBody(float x, float y)
{
	vsbDrawCircle(x,y, mIconRadius, 5);
	DrawFins(x,y);
}

void vsbVelocityVectorHUD::DrawFins(float x, float y)
{
	glBegin(GL_LINE_STRIP);
    glVertex3f(x, y+mIconRadius,  0);
	glVertex3f(x, y+mIconRadius+mIconRadius,  0);
	glEnd();
	glBegin(GL_LINE_STRIP);
    glVertex3f(x-mIconRadius, y,  0);
	glVertex3f(x-mIconRadius*3, y,  0);
	glEnd();
	glBegin(GL_LINE_STRIP);
    glVertex3f(x+mIconRadius, y,  0);
	glVertex3f(x+mIconRadius*3, y,  0);
	glEnd();
}



void vsbVelocityVectorHUD::DrawIcon()
{
	if (mCaged)
	{
//		DrawBody(mX, mVY);
//		DrawFins(mVX, mVY);
		DrawBody( 0.0f, 0.0f );
		DrawFins( 0.0f, 0.0f );
	} else
//		DrawBody(mVX, mVY);
		DrawBody( 0.0f, 0.0f );
}



void vsbVelocityVectorHUD::Draw()
{
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );

	if( mCaged )
	{
		// Draw stationary watermark icon.
		DrawBody( 0.0f, 0.0f );
		DrawFins( 0.0f, 0.0f );
	}

	// OMG h@x. So many minus signs flipped. Oh well, it works...
	sgMat4 mat, inv;
	sgMakeCoordMat4( mat, 0.0f, 0.0f, 0.0f, *mpHeading + 90.0f, *mpPitch, -*mpRoll );

	sgTransposeNegateMat4( inv, mat );

	sgVec3 rotatedVec, vec;
	sgSetVec3( vec, *mpU, *mpV, *mpW );
	sgNormaliseVec3( vec );
	sgXformVec3( rotatedVec, vec, inv );

	sgVec3 straightVec;
	sgSetVec3( straightVec, 0.0f, -1.0f, 0.0f );

	// Calculate dot product.
	float dot = sgScalarProductVec3( rotatedVec, straightVec );
	float angle = acosf( dot ) ;

	if( angle > (2.0f * tool::constants::DEG_TO_RAD) )
	{
		// Draw ghost.
		bool bDrawVelocityVec = true;

		sgVec2 vec2d;
		sgSetVec2( vec2d, rotatedVec[0], rotatedVec[2] );
		sgScaleVec2( vec2d, 6.0f/8.0f );	// to make it less crappy.
		float length = sgLengthVec2( vec2d );
//		if( angle > (8.0f * tool::constants::DEG_TO_RAD) )
		//	should be 8, but this is all rough anyways.
		//	6 actually fits on our cockpit.
		float limit = sinf(6.0f * tool::constants::DEG_TO_RAD);
		if( length > limit )
		{
			// limit ghost to 8.0 degrees. - Er, actually radially limit the vector offset.
			sgNormaliseVec2( vec2d );
			sgScaleVec2( vec2d, limit );

			//	flash ghost.
/*
			DWORD a = timeGetTime();
			a += a + a/2;	// * 2.5
			float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
			// Convert to value between zero and 1.
			float remainder = fmod( asdf, 1.0f );
			// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
			if( remainder < 0.5f )
				bDrawVelocityVec = false;
*/
			int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 0.0f );
			if( odd & 0x01 )
				bDrawVelocityVec = false;

		}
		
		if( bDrawVelocityVec)
			DrawFins( -vec2d[0] * (1.0f/0.005f), -vec2d[1] * (1.0f/0.005f) );
	}

	glPopMatrix();
}


///////////////////////////////////////////////////////////////////////////////
//
// Lars, HUD component
//

vsbLarsHUD::vsbLarsHUD() :
	vsbHUDComponent()
{
    mX = mY = 0;
    mRadius = 0;
}

     
const char * vsbLarsHUD::Name() const 
{
  static char *name = "LarsHUD";
  return name;
};  

void  vsbLarsHUD::SetPos(float x, float y, float r)
{
	mX = x;
	mY = y;
    mRadius = r;
}

void vsbLarsHUD::HookLars( const eLarsMode *mode, const float *rMin, const float *rMax, const float *rNoEscape, const float *range )
{
	m_pMode = mode;
	m_pMin = rMin;
	m_pMax = rMax;
	m_pNoEscape = rNoEscape;
	m_pRange = range;
}

void vsbLarsHUD::HookSD( const float *SDx, const float *SDy )
{
	m_SDx = SDx;
	m_SDy = SDy;
}


void vsbLarsHUD::DrawOuterMarker(float angleDeg, bool diamond)
{
	float arrow_size = mRadius/10;

	glPushMatrix();
	glTranslatef(mX, mY, 0.0f);
	glRotatef(90-angleDeg, 0.0f, 0.0f, 1.0f);
	

    glBegin(GL_LINE_STRIP);
	glVertex3f(mRadius, 0,  0);
	glVertex3f(mRadius+arrow_size, -arrow_size,  0);
	if (diamond)
		glVertex3f(mRadius+arrow_size+arrow_size, 0,  0);
	glVertex3f(mRadius+arrow_size, +arrow_size,  0);
	glVertex3f(mRadius, 0,  0);
	glEnd();

	glPopMatrix();


}


void vsbLarsHUD::DrawRangeBar(float angleDeg)
{
  float incr = Pi/20;
  float angle = ToRad(angleDeg);
  float workangle;
  float sx, cx;
  glPushAttrib(GL_LINE_BIT); 

  glLineWidth(3.0);
  
  glBegin(GL_LINE_STRIP);
  for (float a = 0; a<angle; a+=incr)
  {
	  workangle = PiOnTwo - a;
	  glVertex3f(mX+mRadius*sgCos(workangle), mY+mRadius*sgSin(workangle),  0);
  }	
  workangle = PiOnTwo - angle;
  glVertex3f(mX+mRadius*sgCos(workangle), mY+mRadius*sgSin(workangle),  0);
  glEnd();
  glPopAttrib();
  
  workangle = PiOnTwo - angle;
  sx = sgSin(workangle);
  cx = sgCos(workangle);

  glBegin(GL_LINE_STRIP);
  glVertex3f(mX+mRadius*cx, mY+mRadius*sx,  0);
  glVertex3f(mX+mRadius*cx*0.8, mY+mRadius*sx*0.8,  0);
  glEnd();
  
}

void vsbLarsHUD::Draw()
{

    if (mpHUDManager == 0 || IsEnabled() == false) return;

	// This should be done one level up.
//	sgVec3 targetPos;
//	sgSetVec3( targetPos, m_targetState->x, m_targetState->y, m_targetState->z );
//	float angle = CalcAngleToTarget( targetPos, m_pCam );
//	if( angle > 70.0f )
//		return;


	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


	vsbDrawCircle(mX, mY, mRadius, 5);

	if( *m_pMin < *m_pMax )
	{
		// Draw the relative range markers.

		// Draw rMin marker.
		DrawOuterMarker(75);

		// Draw rMax marker.
		DrawOuterMarker(180);
		// Note: Diamond only drawn for sparrow 7H with LOFT mode selected.
//		DrawOuterMarker(205, true);

		// Draw range no escape marker.
		//	Is rNE guaranteed to be between rMin and rMax??
		float rNEangle = 75.0f + (180.0f - 75.0f) * (*m_pNoEscape - *m_pMin) / (*m_pMax - *m_pMin);
		DrawOuterMarker( rNEangle );

		float relativeRangeAngle;
		if( *m_pRange < *m_pMin )
		{
			relativeRangeAngle = 0.0f + ( 75 - 0.0f ) * ( *m_pRange - 0.0f ) / ( *m_pMin - 0.0f );			
		}
		else
		{
			relativeRangeAngle = 75.0f + (180.0f - 75.0f) * ( *m_pRange - *m_pMin ) / ( *m_pMax - *m_pMin );
			if( relativeRangeAngle > 360.0f )
				relativeRangeAngle = 360.0f;
		}

		DrawRangeBar( relativeRangeAngle );

	}

	// Draw steering Dot.
	// Hrm... how to limit the steering dot, when it is either inside the HMS
	//	or inside the cockpit HUD view? 
	glPointSize( 4.0f );
	glBegin( GL_POINTS );
	glVertex2f( *m_SDx * mRadius, *m_SDy * mRadius );
	glEnd();
	
	

	glPopMatrix();
}




///////////////////////////////////////////////////////////////////////////////
//
// Aspect Vector, HUD component
//

vsbAspectVectorHUD::vsbAspectVectorHUD()
{
	m_aspect = NULL;
	m_speed = NULL;
	m_length = 12.0f;
	m_radius = 13.0f;
}

const char * vsbAspectVectorHUD::Name() const 
{
  static char *name = "AspectVectorHUD";
  return name;
};    


void vsbAspectVectorHUD::Draw()
{
	// Pre-Draw stuff.
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );


	// Draw aspect vector.
	sgVec2 dir2d;
	dir2d[0] = cosf( *m_aspect ) * 0.0f + -sinf( *m_aspect ) * 1.0f;
	dir2d[1] = sinf( *m_aspect ) * 0.0f + cosf( *m_aspect ) * 1.0f;
	// sgNormaliseVec2( dir2d );

	// The length of the aspect vector is doubled if the target is going faster than
	//	Mach 1. Note: this isn't calibrated airspeed ( should it be? )
	float length = m_length;
	if( *m_speed > tool::constants::A0 )
		length *= 2.0f;

	sgVec2 start, end;
	sgCopyVec2( start, dir2d );
	sgCopyVec2( end, dir2d );
	sgScaleVec2( start, m_radius );
	sgScaleVec2( end, m_radius + length );

	glBegin( GL_LINES );

	// Draw body
	glVertex3f( start[0], start[1], 0.0f );
	glVertex3f( end[0], end[1], 0.0f );

	// Draw head
	glVertex3f( end[0], end[1], 0.0f );
	glVertex3f( end[0] - dir2d[0] + dir2d[1], end[1] - dir2d[1] - dir2d[0], 0.0f );
	
	glVertex3f( end[0], end[1], 0.0f );
	glVertex3f( end[0] - dir2d[0] - dir2d[1], end[1] - dir2d[1] + dir2d[0], 0.0f );

	glEnd();


	glPopMatrix();
}



///////////////////////////////////////////////////////////////////////////////
//
// Weapon Name , HUD component
//

vsbSelectedWeaponHUD::vsbSelectedWeaponHUD() :
	vsbHUDComponent()
{
	m_selectedWeapon[0] = '\0';
}

const char * vsbSelectedWeaponHUD::Name() const 
{
  static char *name = "SelectedWeaponHUD";
  return name;
};    


void vsbSelectedWeaponHUD::Draw()
{
	// Pre-Draw stuff.
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );

	mStrokedText.Scale( 4 );
	mStrokedText.Justify( JUST_UPPER, JUST_UPPER );
	mStrokedText.Hilight( true );
	mStrokedText.DrawStr( m_selectedWeapon, 0.0f, 0.0f );

	glPopMatrix();
}


///////////////////////////////////////////////////////////////////////////////
//
// Target Locator, HUD component
//

vsbTargetLocatorHUD::vsbTargetLocatorHUD() :
	vsbHUDComponent()
//	m_targetState( NULL )
{
}

const char * vsbTargetLocatorHUD::Name() const 
{
  static char *name = "TargetLocatorHUD";
  return name;
};    

void vsbTargetLocatorHUD::Draw()
{
	// Draw the target locator HUD.
    if (mpHUDManager == 0 || IsEnabled() == false) return;

	if( *m_bWithinRadar == 0.0f ) return;


	glPushMatrix();
	glScalef( m_sx, m_sy, 1.0f);
	glTranslatef( m_px, m_py, 0.0f );

	// target world position - attacker world position.
	//	The current error in tdbox position is due to the translation of the HUD on the cockpit.
	//	Grrr....
	sgdVec3 relativePos;
	sgdSubVec3( relativePos, *m_targetPos, *m_pos );
	relativePos[1] = -relativePos[1];
	relativePos[2] = -relativePos[2];

	sgdMat4 cm;
	sgdCopyMat4( cm, m_pCam->ComputedMatrix() ) ;
	cm[3][0] = cm[3][1] = cm[3][2] = 0.0;

	sgdMat4 invB;
	sgdTransposeNegateMat4( invB, cm );

	sgdVec3 newVec; 
	sgdXformVec3( newVec, relativePos, invB );

	float hyp1 = sqrtf(newVec[0]*newVec[0] + newVec[1]*newVec[1]);
	float hyp2 = sqrtf(newVec[2]*newVec[2] + newVec[1]*newVec[1]);
	if( (hyp1 != 0.0f) && (hyp2 != 0.0f) )
	{
		// Do all the drawing stuff.
		float xAngle = asinf( newVec[0] / hyp1 );
		float zAngle = asinf( newVec[2] / hyp2 );

		// Draw target designator box.
		sgVec2 tdBoxPos;
		tdBoxPos[0] = (1.0f/0.9f)*(1.0f/0.005f) * tanf( xAngle );
		tdBoxPos[1] = (1.0f/0.9f)*(1.0f/0.005f) * tanf( zAngle );

		bool bDrawTdBox = true;
		bool tdLimited = false;

		// Limits on the range of movement of the target designator box.
		const float top = 22.0f;
		const float bottom = -29.0f;
		const float left = -40.0f;
		const float right = 37.0f;

		// Limit TdBox to edge of hud.
		if( tdBoxPos[0] > right )
		{
			// Clip to HUD.
			tdBoxPos[1] *= ( right / tdBoxPos[0] );
			tdBoxPos[0] = right;
			tdLimited = true;
		}
		if( tdBoxPos[0] < left )
		{
			// Clip to HUD.
			tdBoxPos[1] *= ( left/tdBoxPos[0]);
			tdBoxPos[0] = left;
			tdLimited = true;
		}
		if( tdBoxPos[1] > top )
		{
			// Clip to HUD.
			tdBoxPos[0] *= ( top / tdBoxPos[1] );
			tdBoxPos[1] = top;
			tdLimited = true;
		}
		if( tdBoxPos[1] < bottom )
		{
			// Clip to HUD.
			tdBoxPos[0] *= ( bottom / tdBoxPos[1] );
			tdBoxPos[1] = bottom;
			tdLimited = true;
		}

		if(	tdLimited )
		{
			// Limit target box to the HUD limit.
/*
			DWORD a = timeGetTime();
			a += a + a/2;	// * 2.5
			float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
			// Convert to value between zero and 1.
			float remainder = fmod( asdf, 1.0f );
			// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
			if( remainder < 0.5f )
				bDrawTdBox = false;
*/
			int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
			if( odd & 0x01 )
				bDrawTdBox = false;

			// Draw Target Locator line.
			if( (tdBoxPos[0]*tdBoxPos[0] + tdBoxPos[1]*tdBoxPos[1]) > (1.0f*1.0f) )
			{
				sgVec2 dir;
				sgCopyVec2( dir, tdBoxPos );
				sgNormalizeVec2( dir );

				sgVec2 arrow;
				sgScaleVec2( arrow, dir, 10.0f );

				glBegin( GL_LINES );

				// Draw body.
				glVertex3f( 0.0f, 0.0f, 0.0f);
				glVertex3f( arrow[0], arrow[1], 0.0f );

				// Draw head
				glVertex3f( arrow[0], arrow[1], 0.0f );
				glVertex3f( arrow[0] - dir[0] + dir[1], arrow[1] - dir[1] - dir[0], 0.0f );
				
				glVertex3f( arrow[0], arrow[1], 0.0f );
				glVertex3f( arrow[0] - dir[0] - dir[1], arrow[1] - dir[1] + dir[0], 0.0f );

				glEnd();
			}
		}

		if( bDrawTdBox )
		{
			// Draw Target designator box.
			glBegin( GL_LINE_LOOP );

			glVertex3f( tdBoxPos[0] - 2.0f, tdBoxPos[1] - 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] - 2.0f, tdBoxPos[1] + 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] + 2.0f, tdBoxPos[1] + 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] + 2.0f, tdBoxPos[1] - 2.0f, 0.0f );

			glEnd();
		}

		// Draw Shoot Cue.
		// Draw the shoot cue.
		if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pMin) )
		{
			// Draw Breakaway X.

			//	we need Breakaway X to flash at 2.5 times per second.
			bool bBreakAway = true;
/*
			DWORD a = timeGetTime();
			a += a + a/2;	// * 2.5
			float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
			// Convert to value between zero and 1.
			float remainder = fmod( asdf, 1.0f );
			// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
			if( remainder < 0.5f )
				bBreakAway = false;
*/
			int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
			if( odd & 0x01 )
				bBreakAway = false;

			if( bBreakAway )
			{
				glBegin( GL_LINES );
				
				float x = 0.0f, y = -20.0f;
				float offset = 13.0f;

				glVertex3f( x + offset, y + offset, 0.0f );
				glVertex3f( x - offset, y - offset, 0.0f );

				glVertex3f( x - offset, y + offset, 0.0f );
				glVertex3f( x + offset, y - offset, 0.0f );

				glEnd();
			}

		}
		else if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pMax) )
		{
			bool bDrawShootCue = true;

			if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pNoEscape) )
			{
				//	we need ShootCue to flash at 2.5 times per second.
/*
				DWORD a = timeGetTime();
				a += a + a/2;	// * 2.5
				float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
				// Convert to value between zero and 1.
				float remainder = fmod( asdf, 1.0f );
				// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
				if( remainder < 0.5f )
					bDrawShootCue = false;
*/
				int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
				if( odd & 0x01 )
					bDrawShootCue = false;

			}

			if( bDrawShootCue )
			{
				mStrokedText.Scale( 4 );
				mStrokedText.Justify( JUST_UPPER, JUST_UPPER );
				mStrokedText.Hilight( true );
				mStrokedText.DrawStr( "IN LAR", tdBoxPos[0] + 7.0f , tdBoxPos[1] + 8.0f );
			}
		}

	}

	glPopMatrix();
}


///////////////////////////////////////////////////////////////////////////////
//
// Helmet Mounted Sight, HUD component
//
vsbHMSHUD::vsbHMSHUD() :
	vsbHUDComponent() ,
//	m_canShoot( 0 ),
	m_hmsRadius( 45.0f ),
	m_lastTime( -10000.0f ),
//	m_targetState( NULL ),
	m_pLarsHUD( NULL )
{
}

     
const char * vsbHMSHUD::Name() const 
{
  static char *name = "HMS";
  return name;
};  


void vsbHMSHUD::Draw()
{
	// Draw the helmet mounted sight HUD component.

    if (mpHUDManager == 0 || IsEnabled() == false) return;

	// Don't draw HMS hud if it is going to overlap cockpit HUD.
	sgdVec3 headOrientation;
//	vsbViewportContext::HeadTrackerInterface().GetAngles( headOrientation );
	sgdCopyVec3( headOrientation, m_pCam->HeadOrientation() );
	float magicNumber = 20.0f;
	if( (fabsf(headOrientation[0]) < 20.0f) && (fabsf(headOrientation[1]) < 15.0f) ) 
		return;

	// Move into "eye space"
	glPushMatrix();
	
	{
		glLoadIdentity();
		glScalef( 0.005f, 0.005f, 1.0f );
		glTranslatef( 0.0f, 0.0f, -1.0f);

		// Draw x-hair in middle of screen.
		glBegin(GL_LINE_STRIP);
		glVertex3f( 0.0f, 2.0f,  0.0f );
		glVertex3f( 0.0f, -2.0f, 0.0f );
		glEnd();

		glBegin(GL_LINE_STRIP);
		glVertex3f( 2.0f, 0.0f,  0.0f );
		glVertex3f( -2.0f, 0.0f, 0.0f );
		glEnd();

		// Draw outer circle.
		vsbDrawCircle( 0, 0.0f, m_hmsRadius );

		// Draw off-boresight circles.
		DrawAircraftRelativeCircle( 70.0f * ( tool::constants::PI / 180.0f ),
									0.5f * ( tool::constants::PI / 180.0f ),
									1.0f * ( tool::constants::PI / 180.0f ) );

		DrawAircraftRelativeCircle( 85.0f * ( tool::constants::PI / 180.0f ),
									5.0f * ( tool::constants::PI / 180.0f ),
									1.0f * ( tool::constants::PI / 180.0f ) );

		float px, py, sx, sy;

		// Draw heading
		m_pHeadingHUD->GetPosition( px, py );
		m_pHeadingHUD->GetScale( sx, sy );
		m_pHeadingHUD->SetPosition( 0.0f, 30.0f );
		m_pHeadingHUD->SetScale( 0.9f, 0.9f );
		m_pHeadingHUD->Draw();
		m_pHeadingHUD->SetPosition( px, py );
		m_pHeadingHUD->SetScale( sx, sy );

		// Draw altairspeed
		m_pAltAirspeedHUD->GetPosition( px, py );
		m_pAltAirspeedHUD->SetPosition( -43.0f, 22.0f );
		m_pAltAirspeedHUD->Draw();
		m_pAltAirspeedHUD->SetPosition( px, py );

		// Draw attitude
		m_pAttitudeHUD->GetPosition( px, py );
		m_pAttitudeHUD->SetPosition( -30.0f, 10.0f );
		m_pAttitudeHUD->Draw();
		m_pAttitudeHUD->SetPosition( px, py );

		// Draw Weapon name
		m_pSelectedWeaponHUD->GetPosition( px, py );
		m_pSelectedWeaponHUD->SetPosition( px, -35.0f );
		m_pSelectedWeaponHUD->Draw();
		m_pSelectedWeaponHUD->SetPosition( px, py );


//		if( m_targetState )
		if( *m_bHasTarget )
		{
			// If we have a target.

//			if( *m_pAngleToTarget < 70.0f * tool::constants::DEG_TO_RAD )
			if( *m_bWithinRadar == 1.0f )
			{
				// Target is within radar limits, Draw full HMS hud.

				// Draw the Lars display.
				if( m_pLarsHUD )
				{
					float sx, sy;

					m_pLarsHUD->GetScale( sx, sy );
					m_pLarsHUD->SetScale( 0.6f, 0.6f );
					m_pLarsHUD->Draw();
					m_pLarsHUD->SetScale( sx, sy );
				}

				// Draw aspect vector
				m_pAspectVectorHUD->GetScale( sx, sy );
				m_pAspectVectorHUD->SetScale( 0.6f, 0.6f );
				m_pAspectVectorHUD->Draw();
				m_pAspectVectorHUD->SetScale( sx, sy );

				// Draw the target box/shoot cue + target direction arrow.
				DrawTargetBox();
			}
//			else if( angle < 85.0f )
//			else if( *m_pAngleToTarget < 85.0f * tool::constants::DEG_TO_RAD )
			else if( *m_bWithinSeeker == 1.0f )
			{
				// Draw the seeker circle.
				
				DrawSeekerCircle();

			}
		}
	}

	glPopMatrix();
}


void vsbHMSHUD::DrawAircraftRelativeCircle( float angle, float segmentSize, float gapSize )
{
	// Draw off-boresite limit circle.

	//	In "relative to aircraft" space
	sgdVec3 headOrientation;
//	vsbViewportContext::HeadTrackerInterface().GetAngles( headOrientation );
	sgdCopyVec3( headOrientation, m_pCam->HeadOrientation() );

	sgMat4 headMatrix;
	sgMakeRotMat4  ( headMatrix , headOrientation[2], headOrientation[1], headOrientation[0] );

	float oldWidth;
	glGetFloatv( GL_LINE_WIDTH, &oldWidth );
	glLineWidth( oldWidth * 2.0f );

	glBegin( GL_LINES );

	// If 1.0f, is the edge of the screen at 30 degrees (60 degree fov)....
	float radius = tanf( angle ) * (1.0f/0.005f);

	sgVec3 start, end;

	start[0] = radius;
	start[1] = 0.0f; 
	start[2] = 1.0f * (1.0f/0.005f);
	end[2] = 1.0f * (1.0f/0.005f);
	

	float step = 0.0f;
	while( step < tool::constants::TWO_PI )
	{
		step += segmentSize;
	
		end[0] = cosf( step ) * radius;
		end[1] = sinf( step ) * radius;


		// Clip points to Hud Circle.

		// Find if line is inside hud circle.
		bool bStartInside = false;
		bool bEndInside = false;

		sgVec3 newVec; 
		sgXformVec3( newVec, start, headMatrix );
		float startx, starty, endx, endy;

		if( newVec[2] > 1.0f )
		{
			startx = (1.0f/0.005f) * newVec[0]/newVec[2];
			starty = (1.0f/0.005f) * newVec[1]/newVec[2];

			if( (startx*startx + starty*starty) < (m_hmsRadius * m_hmsRadius) )
				bStartInside = true;
		}

		sgXformVec3( newVec, end, headMatrix );
		if( newVec[2] > 1.0f )
		{
			endx = (1.0f/0.005f) * newVec[0]/newVec[2];
			endy = (1.0f/0.005f) * newVec[1]/newVec[2];

			if( (endx*endx + endy*endy) < (m_hmsRadius * m_hmsRadius) )
				bEndInside = true;
		}

		// Circle clipping code.
		if( (bStartInside && !bEndInside) || (!bStartInside && bEndInside) )
		{
			// One is inside, one is outside.

			// find collision points with the circle.

			//	Px = Ax + r(Bx - Ax)
			//->Px = a + rb;

			//	Py = Ay + r(By - Ay)
			//->Py = c + rd;	

			float a,b,c,d;

			a = startx;
			b = endx - startx;
			c = starty;
			d = endy - starty;

			//	sqr(a + rb) + sqr(c + rd) = sqr(45)
			//-> (sqr(b) + sqr(d))*sqr(r) + (a*b + c*d)*2r + (sqr(a) + sqr(c) - sqr(45) = 0
			//	A*sqr(r) + B*r + C

			double A = b*b + d*d;
			double B = 2*(a*b + c*d);
			double C = a*a + c*c - m_hmsRadius*m_hmsRadius;

			double discriminant = B*B - 4*A*C;

			if( discriminant > 0.0f )
			{
				double sqrtDiscriminant = sqrt( discriminant );

				// we have a 2 unique solutions
				double soln1 = ( -B + sqrtDiscriminant ) / (2*A);

				double P[2];
				P[0] = a + soln1*b;
				P[1] = c + soln1*d;

				// Test which solution lies on the line.
				double AB[2];
				double AC[2];

				AB[0] = endx - startx;
				AB[1] = endy - starty;

				AC[0] = P[0] - startx;
				AC[1] = P[1] - starty;

				double test;
				test = (AB[0]*AC[0] + AB[1]*AC[1]) / (AB[0]*AB[0] + AB[1]*AB[1]);

				if( (test > 0) && (test < 1) )
				{
					// This solution is the point on the circle.
					for(;0;);
				}
				else
				{
					// The other solution is the point on the circle.
					double soln2 = ( -B - sqrtDiscriminant ) / (2*A);
					P[0] = a + soln2*b;
					P[1] = c + soln2*d;
				}

				if( bStartInside )
				{
					endx = P[0];
					endy = P[1];
					bEndInside = true;
				}
				else
				{
					startx = P[0];
					starty = P[1];
					bStartInside = true;
				}

			}
		}

		if( bStartInside && bEndInside )
		{
			glVertex3f( startx, starty, 0.0f );
			glVertex3f( endx, endy, 0.0f );
		}

		step += gapSize;
		start[0] = cosf( step ) * radius;
		start[1] = sinf( step ) * radius;
	}

	glEnd();

	glLineWidth( oldWidth );

}


void vsbHMSHUD::DrawTargetBox()
{
	// Calculate angle target is from aircraft heading.

	// target world position - attacker world position.
	sgdVec3 relativePos;
	sgdSubVec3( relativePos, *m_targetPos, *m_pos );
	relativePos[1] = -relativePos[1];
	relativePos[2] = -relativePos[2];

	// We need to add in the cockpit offset in here somewhere.



	// Get camera's matrix.
	sgdMat4 tmp;
	sgdCopyMat4( tmp, m_pCam->CameraMatrix() );
	//	zero out translation component. Note: this may already be zero-ed by the
	//	"translate camera to origin" functionality of vsb.
	tmp[3][0] = 0.0f;	tmp[3][1] = 0.0f;	tmp[3][2] = 0.0f;

	// Inverse = matrix transpose for rotation matrices.
	sgdMat4 inv;
	sgdTransposeNegateMat4( inv, tmp );

	// Draw targetting box around target aircraft.
	sgdVec3 newVec;
	sgdXformVec3( newVec, relativePos, inv );

	// x is left-to-right
	// z is bottom-to-top
	// y is back-to-front.

	float hyp1 = sqrtf(newVec[0]*newVec[0] + newVec[1]*newVec[1]);
	float hyp2 = sqrtf(newVec[2]*newVec[2] + newVec[1]*newVec[1]);
	if( (hyp1 != 0.0f) && (hyp2 != 0.0f) )
	{
		// Do all the drawing stuff.
		float xAngle = asinf( newVec[0] / hyp1 );
		float zAngle = asinf( newVec[2] / hyp2 );

		// Draw target designator box.
		sgVec2 tdBoxPos;
		tdBoxPos[0] = (1.0f/0.005f) * tanf( xAngle );
		tdBoxPos[1] = (1.0f/0.005f) * tanf( zAngle );

		bool bDrawTdBox = true;

		if( (tdBoxPos[0]*tdBoxPos[0] + tdBoxPos[1]*tdBoxPos[1]) > (m_hmsRadius-1.0f)*(m_hmsRadius-1.0f) ) 
		{
			// Limit target box to the HUD limit.
			sgNormalizeVec2( tdBoxPos );
			sgScaleVec2( tdBoxPos, m_hmsRadius - 1.0f );
/*
			DWORD a = timeGetTime();
			a += a + a/2;	// * 2.5
			float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
			// Convert to value between zero and 1.
			float remainder = fmod( asdf, 1.0f );
			// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
			if( remainder < 0.5f )
				bDrawTdBox = false;
*/
			int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
			if( odd & 0x01 )
				bDrawTdBox = false;

			// Draw the target locator line (when target designator box is HUD limited).
			if( (tdBoxPos[0]*tdBoxPos[0] + tdBoxPos[1]*tdBoxPos[1]) > (1.0f*1.0f) )
			{
				// target locator line (next to lars).
				sgVec2 dir2d;
				dir2d[0] = newVec[0];
				dir2d[1] = newVec[2];
				sgNormaliseVec2( dir2d );

				sgVec2 arrow;
				sgCopyVec2( arrow, dir2d );
				sgScaleVec2( arrow, 8.0f );

				glBegin( GL_LINES );
				
				// Draw body of arrow.
				glVertex3f( 0.0f, 0.0f, 0.0f );
				glVertex3f( arrow[0], arrow[1], 0.0f );

				// Draw head
				glVertex3f( arrow[0], arrow[1], 0.0f );
				glVertex3f( arrow[0] - dir2d[0] + dir2d[1], arrow[1] - dir2d[1] - dir2d[0], 0.0f );
				
				glVertex3f( arrow[0], arrow[1], 0.0f );
				glVertex3f( arrow[0] - dir2d[0] - dir2d[1], arrow[1] - dir2d[1] + dir2d[0], 0.0f );

				glEnd();
			}

		}
		else 
		{
			// Within HUD limit.
			if( newVec[1] < 0.0f )
			{
				// Behind us within HUD limit.
				sgNormalizeVec2( tdBoxPos );
				sgScaleVec2( tdBoxPos, m_hmsRadius - 1.0f );
			}
		}

		// Draw the target box.
		if( bDrawTdBox )
		{
			glBegin( GL_LINE_LOOP );

			glVertex3f( tdBoxPos[0] - 2.0f, tdBoxPos[1] - 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] - 2.0f, tdBoxPos[1] + 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] + 2.0f, tdBoxPos[1] + 2.0f, 0.0f );
			glVertex3f( tdBoxPos[0] + 2.0f, tdBoxPos[1] - 2.0f, 0.0f );

			glEnd();
		}

		// Draw the shoot cue.
		if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pMin) )
		{
			// Draw Breakaway X.

			//	we need Breakaway X to flash at 2.5 times per second.
			bool bBreakAway = true;
/*
			DWORD a = timeGetTime();
			a += a + a/2;	// * 2.5
			float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
			// Convert to value between zero and 1.
			float remainder = fmod( asdf, 1.0f );
			// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
			if( remainder < 0.5f )
				bBreakAway = false;
*/
			int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
			if( odd & 0x01 )
				bBreakAway = false;

			if( bBreakAway )
			{
				glBegin( GL_LINES );
				
				float x = 0.0f, y = -20.0f;
				float offset = 9.0f;

				glVertex3f( x + offset, y + offset, 0.0f );
				glVertex3f( x - offset, y - offset, 0.0f );

				glVertex3f( x - offset, y + offset, 0.0f );
				glVertex3f( x + offset, y - offset, 0.0f );

				glEnd();
			}

		}
		else if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pMax) )
		{
			bool bDrawShootCue = true;

			if( *(m_pLarsHUD->m_pRange) < *(m_pLarsHUD->m_pNoEscape) )
			{
				//	we need ShootCue to flash at 2.5 times per second.
/*
				DWORD a = timeGetTime();
				a += a + a/2;	// * 2.5
				float asdf = ( (float)(a & 0xfffff) ) / 1000.0f;	// remove precision problems
				// Convert to value between zero and 1.
				float remainder = fmod( asdf, 1.0f );
				// Change 0.5 to bigger/smaller value between 0 and 1 to adjust dwell time.
				if( remainder < 0.5f )
					bDrawShootCue = false;
*/
				int odd = (int)( vsbViewportContext::EntityManager().CurrTime() * 5.0f );
				if( odd & 0x01 )
					bDrawShootCue = false;

			}

			if( bDrawShootCue )
			{
				mStrokedText.Scale( 4 );
				mStrokedText.Justify( JUST_UPPER, JUST_UPPER );
				mStrokedText.Hilight( true );
				mStrokedText.DrawStr( "IN LAR", tdBoxPos[0] + 7.0f , tdBoxPos[1] + 8.0f );
			}
		}

	}
	else
	{
		// Target is Right on top of us.
	}

}


void vsbHMSHUD::DrawSeekerCircle()
{
	// target world position - attacker world position.
	sgdVec3 relativePos;
	sgdSubVec3( relativePos, *m_targetPos, *m_pos );
	relativePos[1] = -relativePos[1];
	relativePos[2] = -relativePos[2];

	// Get camera's matrix.
	sgdMat4 tmp;
	sgdCopyMat4( tmp, m_pCam->CameraMatrix() );
	tmp[3][0] = 0.0f;	tmp[3][1] = 0.0f;	tmp[3][2] = 0.0f;

	// Inverse = matrix transpose for rotation matrices.
	sgdMat4 inv;
	sgdTransposeNegateMat4( inv, tmp );

	sgdVec3 newVec;
	sgdXformVec3( newVec, relativePos, inv );

	if( newVec[1] > 1.0f )
	{
		// Our target is "in-front" of our HMS.
		float xAngle = atanf( newVec[0] / newVec[1] );
		float zAngle = atanf( newVec[2] / newVec[1] );

		// Draw targetting box.
		if( ( fabs( xAngle ) < (30.0f * 3.141 / 180.0f) ) && ( fabs( zAngle ) < (30.0f * 3.141 / 180.0f) ) )
		{

			float x = (1.0f/0.005f) * newVec[0]/newVec[1];
			float y = (1.0f/0.005f) * newVec[2]/newVec[1];

			// Only draw target box & shoot cue if target is inside HMS circle.
			if( (x*x + y*y) < m_hmsRadius*m_hmsRadius )
			{
				// Draw seeker circle.
				sgVec2 start, end;
				float radius = 10.0f;
				float segmentSize = 5.0f * tool::constants::DEG_TO_RAD;
				float gapSize = 5.0f * tool::constants::DEG_TO_RAD;

				start[0] = x + radius;
				start[1] = y + 0.0f; 

				float step = 0.0f;

				float oldWidth;
				glGetFloatv( GL_LINE_WIDTH, &oldWidth );
				glLineWidth( oldWidth * 2.0f );


				glBegin( GL_LINES );

				while( step < tool::constants::TWO_PI )
				{
					step += segmentSize;

					if( step > tool::constants::TWO_PI )
						step = tool::constants::TWO_PI;

					end[0] = x + cosf( step ) * radius;
					end[1] = y + sinf( step ) * radius;

					glVertex3f( start[0], start[1], 0.0f );
					glVertex3f( end[0], end[1], 0.0f );

					step += gapSize;
					start[0] = x + cosf( step ) * radius;
					start[1] = y + sinf( step ) * radius;
				}

				glEnd();

				glLineWidth( oldWidth );
			}
		}
	}
}

