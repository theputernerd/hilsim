//////////////////////////////////////////////////////////////////
/*! 
    \file vsbSkyDome.h
    \brief Defines the class vsbSkyDome
 

	This file defines a sky dome object that is designed to track
    a camera on a flat earth. It provides realistic sky colouration 
	given a specific son position and an  artificial curved horizon
	
	\sa vsbSkyDome.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbSkyDome_H
#define _vsbSkyDome_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif                                   

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


///////////////////////////////////////////////////////////////////////
/*!
   \class vsbSkyDome vsbSkyDome.h 	
   \brief A skydome object 

   This class defines a dome sky object that is designed to track
   a camera on a flat earth. It provides realistic sky
   colouration given a specific son position and an 
   artificial curved horizon


   \author T. Gouthas
*/ 
///////////////////////////////////////////////////////////////////////


class DLL_VSB_API vsbSkyDome {
	int            mSkyDomeSlices;
	sgVec4         mBottomColor;

    ssgTransform   *mpDomeTransform;
    ssgSimpleState *mpDomeState;

    ssgVertexArray *mpCentreDiskVert;
    ssgColourArray *mpCentreDiskColour;

    ssgVertexArray *mpUpperRingVert;
    ssgColourArray *mpUpperRingColour;

    ssgVertexArray *mpMiddleRingVert;
    ssgColourArray *mpMiddleRingColour;

    ssgVertexArray *mpLowerRingVert;
    ssgColourArray *mpLowerRingColour;

	float mSunAngle;
public:

    //! Default Constructor
    vsbSkyDome( void );

	/*! \brief Constructor
		
		By default a skydome has 10 slices per layer, ie, 10 edges approximating its 
		circumference. Think of it as cake or pie slices. This constructor allows a
		different number of slices to use in constructing the skydome.
	    \param slices The number of slices (or facets) comprising the skydome's 
		circumference 
	*/
	vsbSkyDome( int slices );

    //! Virtual destructor
    virtual ~vsbSkyDome( void );

	/*! \brief Build and initialize the skyDome object and and return its scene graph root
		\returns ssgBranch; The root of the skyDome object scene graph
	*/
     ssgBranch *Build();

    /*! \brief Repaint the sky colors based on current value of sun_angle,
    sky, and fog colors. 
	
	This updates the color arrays for ssgVtxTable. Sun angle is in degrees relative 
	to verticle
      - 0 degrees = high noon
      - 90 degrees = sun rise/set
      - 180 degrees = darkest midnight

    \param sky_color The sky colour in RGB

	\param fog_color The fog colour in RGB

	\param sun_angle The sun angle;
    	-	-0 degrees = high noon
    	-	-90 degrees = sun rise/set
    	-	-180 degrees = darkest midnight

	\param vis The range of visibility in Metres

    \returns ssgBranch; The root of the skyDome object scene graph
    */
    bool Repaint( const sgVec3 sky_color, const sgVec3 fog_color, 
		          const double sun_angle, const double vis );

	/*! \brief Reposition the skyDome at the specified origin 
		\param pos position of the camers
		\returns Always true
	*/
    bool Reposition(const sgVec3 pos);

	/*! \brief Get the colour of the bottom ring of the skydome
		\returns sgVeg4, the RGBA values
		\remarks This is used for coloring the base of the skydome prior to terrain
		mapping
	*/
	sgVec4 &GetBottomColor() { return mBottomColor; };



};

#endif