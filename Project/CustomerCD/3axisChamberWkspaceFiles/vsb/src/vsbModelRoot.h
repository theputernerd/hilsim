#ifndef _vsbModelRoot_H
#define _vsbModelRoot_H

//////////////////////////////////////////////////////////////////
/*! 
    \file vsbModelRoot.h
    \brief Defines the class vsbModelRoot and its auxillary class 
	vsbSelectorInterface
	
	\sa vsbModelRoot.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


#define SSG_TYPE_MODELROOT     0x10000000 //!< ssg Type identifier

const int MAX_SELECTORS = 10;

class DLL_VSB_API vsbModelRoot;
class DLL_VSB_API vsbSelectorInterfaceNode;


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbSelectorInterface
    \brief An ssgSelector state management class

	Defines a list of ssgSelector states, and associated manipulation
	functions. The list consists of	selector state entries, which are 
	for all intents and purposes, merely tuples consisting of a string 
	label identifying the ssgSelector the state refers to, and a 16 bit 
	bitfield.

	ssgSelectors are scene graph switching branches which allow only 
	one of a number of sub trees to be drawn according to a selection 
	bitmap.

	\sa ssgSelector, vsbSelectorInterfaceNode
*/

//////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbSelectorInterface
{
protected:

	
public:

	/*! \brief Constructor
		\param mr reference to a  vsbModelRoot
	*/
	vsbSelectorInterface(vsbModelRoot &mr);

	/*! \brief Virtual destructor
	*/
	virtual ~vsbSelectorInterface();

	/*! \brief Register a new selector 

		A selector entry is created in the list and its default state bits are recorded.
		\param selectorName The name of the ssgSelector this entry refers to
		\param defaultVal The default state bitfield
        \note In this version of RegisterSelector, the logical selector name is assumed 
        to be the same as the actual selector name
	*/
    int  RegisterSelector(std::string actualSelectorName, int defaultVal = 0);	

	/*! \brief Register a new selector 

		A selector entry is created in the list and its default state bits are recorded.
		This version includes an logical name, which allows an alternative name to be used
		to reference a ssgSelector within a scene graph, which is different to the actual
		name of the ssgSelector itself.

		\param actualSelectorName The name of the ssgSelector this entry refers to. 
		\param selectorLogicalName The logical name of the ssgSelector this entry refers to
		\param defaultVal The default state bitfield
	*/
    int  RegisterSelector(std::string actualSelectorName, std::string selectorLogicalName, int defaultVal = 0);	

	/*! \brief Activate all the selector hooks

		Associates the selector entries in this class with the corresponding ssgSelector
		instances in the model pointed to by mpModelRoot, enabling the selector states
		managed here to be applied
		
		\sa vsbSelectorInterfaceNode::Hook()
	*/
	void ActivateSelectorHooks();

	/*! \brief De-activate all the selector hooks

		ssgSelectors must be unhooked from the vsbSelectorInterface before the model
		pointed to by mpModelRoot is deleted or changed, otherwise calls to Apply will
		subsequently reference undefined selectors resulting in unpredictable (and 
        probably catastrophic) behaviour.

		\sa ActivateSelectorHooks, vsbSelectorInterfaceNode::Unhook()
	*/
	void DeactivateSelectorHooks();

	/*! \brief Applies all the selector states to all the registered ssgSelectors
		\note Before applying the current selector states, the previous states are saved
		\sa RestoreSelectors()
	*/	
	void ApplySelectors();


	/*! \brief restore the selector states to the previously saved values
	*/	
	void RestoreSelectors();

	/*! \brief Delete all the selector state entries
	*/
	void DeleteSelectors() {mNumInterfaceNodes = 0;}
	
   

    /*! \brief Replaces the specified selectors state bits with new ones
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing th selectors new state bits
        \returns false if invalid selector 
    */
	bool ReplaceSelectorBits		(int selectorIndex, int opts);

    /*! \brief Set the specified selectors state bits by xor'ing with opts
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing th selector state bits to set
        \returns false if invalid selector 
    */
	bool SetSelectorBits			(int selectorIndex, int opts);

    /*! \brief Clear all the specified selectors state bits 
        \param selectorIndex Selector's index in the list
        \returns false if invalid selector 
    */
	bool ClearSelectorBits			(int selectorIndex);

    /*! \brief Clear the specified selectors state bits as specified in opts
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing the selector state bits to clear
        \returns false if invalid selector 
    */
	bool ClearSelectorBits			(int selectorIndex, int opts);

    /*! \brief Replaces the specified selectors "default" state bits with new ones
        \param selectorIndex Selector's new default index in the list
        \param opts Integer bitfield representing the selectors new default state bits
        \returns false if invalid selector 
    */
	bool ReplaceDefaultSelectorBits	(int selectorIndex, int opts);

     /*! \brief Set the specified selectors default state bits by xor'ing the new ones
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing th selectors new state
        \returns false if invalid selector 
    */
	bool SetDefaultSelectorBits		(int selectorIndex, int opts);

    /*! \brief Clear all of the specified selectors default state bits 
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing th selectors new state
        \returns false if invalid selector 
    */
	bool ClearDefaultSelectorBits	(int selectorIndex);

    /*! \brief Clear the specified selectors default state bits as specified in opts
        \param selectorIndex Selector's index in the list
        \param opts Integer bitfield representing th selector's default state bits to clear
        \returns false if invalid selector 
    */
	bool ClearDefaultSelectorBits	(int selectorIndex, int opts);

    /*! \brief Replaces the specified selectors state bits with new ones
        \param selectorName Selector's name
        \param opts Integer bitfield representing th selectors new state bits
        \returns false if invalid selector 
    */
    bool ReplaceSelectorBits		(const std::string & selectorLogicalName, int opts);

    /*! \brief Set the specified selectors state bits by xor'ing with opts
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing th selector state bits to set
        \returns false if invalid selector 
    */
    bool SetSelectorBits			(const std::string & selectorLogicalName, int opts);

    /*! \brief Clear all the specified selectors state bits 
        \param selectorLogicalName Selector's name
        \returns false if invalid selector 
    */
    bool ClearSelectorBits			(const std::string & selectorLogicalName);

    /*! \brief Clear the specified selectors state bits as specified in opts
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing the selector state bits to clear
        \returns false if invalid selector 
    */

    bool ClearSelectorBits			(const std::string & selectorLogicalName, int opts);

    /*! \brief Replaces the specified selectors "default" state bits with new ones
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing the selectors new default state bits
        \returns false if invalid selector 
    */
    bool ReplaceDefaultSelectorBits	(const std::string & selectorLogicalName, int opts);

    /*! \brief Set the specified selectors default state bits by xor'ing the new ones
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing th selectors new state
        \returns false if invalid selector 
    */
	bool SetDefaultSelectorBits		(const std::string & selectorLogicalName, int opts);

    /*! \brief Clear all of the specified selectors default state bits 
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing th selectors new state
        \returns false if invalid selector 
    */
	bool ClearDefaultSelectorBits	(const std::string & selectorLogicalName);

    /*! \brief Clear the specified selectors default state bits as specified in opts
        \param selectorLogicalName Selector's name
        \param opts Integer bitfield representing th selector's default state bits to clear
        \returns false if invalid selector 
    */
	bool ClearDefaultSelectorBits	(const std::string & selectorLogicalName, int opts);

	/*! \brief Set the specified selector to its default state
		\param selectorIndex The index of the selector to set to its default
	*/
	bool Default					(int selectorIndex);

	/*! \brief Set the specified selector to its default state
		\param selectorLogicalName The name of the selector to set to its default
	*/
	bool Default					(const std::string & selectorLogicalName);

	/*! \brief Get the state bitfield for the specified selector 
		\param selectorLogicalName Selector's name
	*/
	int  GetSelectorBits(int selectorIndex) const; 

	/*! \brief Get the state bitfield for the specified selector 
		\param selectorLogicalName The name of the selector 
	*/
	int  GetSelectorBits(const std::string & selectorLogicalName) const; 

	/*! \brief Get the default state bitfield for the specified selector 
		\param selectorIndex The index of the selector 
	*/
	int  GetDefaultSelectorBits(int selectorIndex) const; 

	/*! \brief Get the default state bitfield for the specified selector 
		\param selectorLogicalName The name of the selector 
	*/
	int  GetDefaultSelectorBits(const std::string & selectorLogicalName) const; 

	/*! \brief Get the name of the Selector based on its index
	*/
	const std::string &SelectorName(int idx) const;

	/*! \brief Get the number of selector interface nodes in the list
	*/
    int  NumInterfaceNodes() { return  mNumInterfaceNodes;};

	/*! \brief Set the number of selector interface nodes in the list
	*/
	void NumInterfaceNodes(int n) { mNumInterfaceNodes= n;};

private:
	int						 mNumInterfaceNodes;  //!< Number of selector interface nodes
	vsbModelRoot			 *mpModelRoot;		  //!< Model root node		
	vsbSelectorInterfaceNode *mpSelectorInterfaceNodes; //!< List of selector interface nodes


};


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbModelRoot
    \brief An ssgTransform derived class designed to be the root
	class of vsbEntity model scene graphs.
	
	The vsbModelRoot extends ssgTransform by adding a local transform
	(and associated member functiions) that the model is premultiplied 
	with for the purpose of allowing the model's default orientation 
	and centre to be adjusted externally to the model. It also adds a 
	Selector interface (and again, including related member functions) 
	to allow the control of ssgSelector based moving parts in, for 
	example, MS Flight Sim models or any models that have named ssgSelector
	based geometry alternatives. 
	
	The ssgSelector control extensions have a number of important capabilities, 
	but one major one is that this class stores	the state of the ssgSelectors 
	locally, and applies them to the model at draw time. 
	
	This allows the same instance of the model to be referenced by multiple 
	entities, and have the moving parts drawn in the correct state for the 
	respective entities that reference them. All that is required is that
	each reference of the model is attached to a unique vsbModelRoot parent.

	\sa vsbSelectorInterface, ssgSelector, ssgTransform

*/
//////////////////////////////////////////////////////////////////


class DLL_VSB_API vsbModelRoot : public ssgTransform
{

public:
                //! Inherited clone function
  virtual		ssgBase *clone ( int clone_flags = 0 ) ;
                
                //! Default constructor
				vsbModelRoot (void) ;

                //! Constructor        
			    vsbModelRoot ( sgCoord *c ) ;

                //!Virtual destructor
  virtual		~vsbModelRoot (void);

                //! Get model scaling factor
  float			getScale() const { return mScale;}; 

                //! Set model scaling factor
  void			setScale(float scale);

                /*! \brief Set the local model adjustment transform

                This transform is applied before the standard ssgTransform transform to
                adjust the models default orientation and centre of rotation.

                This version sets the local transform to a simple xyz translation
                */
  virtual void	setLocalTransform ( sgVec3 xyz ) ;
                
                /*! \brief Set the local model adjustment transform

                This transform is applied before the standard ssgTransform transform to
                adjust the models default orientation and centre of rotation.

                This version sets the local transform from a user supplied euler style
                sgCoord transform
                */
  virtual void	setLocalTransform ( sgCoord *xform ) ;

                /*! \brief Set the local model adjustment transform

                This transform is applied before the standard ssgTransform transform to
                adjust the models default orientation and centre of rotation.

                This version sets the local transform from a user supplied transform
                */
  virtual void	setLocalTransform ( sgMat4 xform ) ;

                /*! \brief Set the model transform
                    \sa ssgTransform
                */
  virtual void	setTransform ( sgVec3 xyz ) ;

                /*! \brief Set the model transform

                    This version sets the transform to a simple xyz translation
                    \sa ssgTransform
                */
  virtual void	setTransform ( sgCoord *xform ) ;

  virtual void	setTransform ( sgdCoord *xform ) ;

                /*! \brief Set the model transform

                    This version sets the transform from a user supplied euler style
                    sgCoord transform
                    \sa ssgTransform
                */
  virtual void	setTransform ( sgCoord *xform, float sx, float sy, float sz ) ;

                /*! \brief Set the model transform

                    This version sets the transform from a user supplied transform    
                    \sa ssgTransform
                */
  virtual void	setTransform ( sgMat4 xform ) ;


                /*! \brief recalculate bounding sphere

                Inherited from ssgTransform, is extended to calculate a scaled bounding
                sphere reflecting the scaling factor this transform implements
                */
  void			recalcBSphere (void);
  
                /* \sa ssgTransform */
  virtual const char *getTypeName(void) ;
            
                /* \sa ssgTransform */
  virtual int   load ( FILE *fd ) ;

                /* \sa ssgTransform */
  virtual int   save ( FILE *fd ) ;

                /* \brief Get a reference to this vsbModelRoot's vsbSelectorInterface
                */
  vsbSelectorInterface &Selectors() {return *mpSelectors;}; 

  void dirtyBSphere ();

protected:
  SGfloat		mScale;  //!< Scaling factor 
  sgMat4		mPreXfm; //!< The local transformation matrix
  vsbSelectorInterface *mpSelectors; //!< Selector interface instance

                /*! \brief Inherited and overridend copy_from function
                \sa ssgTransform
                */
  virtual void	copy_from ( vsbModelRoot *src, int clone_flags ) ;

  void scaleBSphere( float scale, ssgEntity *entity);
  void dirtyChildBSpheres(ssgEntity *entity);


 private:

  void	                    ApplySelectors(); //!< Apply all the selector settings
  void	                    RestoreSelectors();	//!< Restore prior selector settings

                             //!Cull - \see SSG documentation
  virtual void cull          ( sgFrustum *f, sgMat4 m, int test_needed ) ;
                             //!Intersection - \see SSG documentation
  virtual void isect         ( sgSphere  *s, sgMat4 m, int test_needed ) ;
                             //!Height off terrain - \see SSG documentation
  virtual void hot           ( sgVec3     s, sgMat4 m, int test_needed ) ;




};

#endif


/*

in SgSphere creat a Scale function which multiplies centre, and radius by a factor

  the factor is stored in the ssgBase as a static or in the ssgFile

  and vsbModelRoot::recalcBSphere sets that value but 

  ssgVtxTable  and ssgVTable use the Scale function to scale their primitive spheres 

*/