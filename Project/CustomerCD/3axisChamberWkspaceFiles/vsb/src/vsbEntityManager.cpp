////////////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbEntityManager.cpp
    \brief Entity manager is the class that maintains and updates the state of 
    entities in a scene.
    
    It is the interface between the user program, and the  scene graph.

    Entity manager implements a hashtable for fast look-up of entities by name
    The hash value of an entity's name is used as it's handle

    Entity manager also incorporates the EntityFactory for creating entities
    \sa vsbEntityManager.h
*/
////////////////////////////////////////////////////////////////////////////////
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <map>
#include <set>
#include <string>
#include <functional>
#include <assert.h>
#include <typeinfo.h>

using namespace std;

#include "ssg.h"
#include "vsbBaseViewEntity.h"
#include "vsbEntityManager.h"
#include "vsbModelManager.h"
#include "vsbEnvironmentManager.h"
#include "vsbViewportContext.h"

const int       vsbEntityManager::msHashTableSizeIndexMax = 2;

// A table of sizes (primes) of hash tables - this accomodates up to 10,000
// entities which is overly generous

int             vsbEntityManager::msHashTableSizes[]   = {941, 9973, 16333};

int             vsbEntityManager::mInstanceCount = 0;

static map<string, vsbBaseViewEntityFactory * > entity_factories;

//-------------------------------------------------------------
// Constructor
//-------------------------------------------------------------


vsbEntityManager::vsbEntityManager()
:mpHashTable(0),mpHead(0),mHashTableSizeIndex(0), mEntityCount(0),
mAutoHashResize(true)
{
	assert(mInstanceCount == 0);
	assert(vsbViewportContext::IsInitialised());
	mInstanceCount++;
    HashTableResize();
	int i = entity_factories.size();
	mpEntityAddedFunc = 0;
	mpEntityRemovedFunc = 0;
	mCurrTime = 0;
}

//-------------------------------------------------------------
// Destructor
//-------------------------------------------------------------

vsbEntityManager::~vsbEntityManager()
{
	DeregisterAllViewEntityFactories();
    DestroyAll();
    delete [] mpHashTable;
	mInstanceCount--;
}

//-------------------------------------------------------------
// HashTableResize()
// Function to resize the look up hash-table  to the next biggest size
// based on the mHashTableSizeIndex and the mHashTableSizes[] array.
//
// If there are already entities in an existing hash-table there is
// a serious repercussion;
//
// All the handles of the entities will be updated; This may or may
// not be an issue depending on how vsbEntityManager is used.
//
// By default automatic resizing of the hash-table is off, consequently
// calling this function whilst there are entities in the hash table has
// no efect, UNLESS the force parameter is TRUE. 
//-------------------------------------------------------------

bool vsbEntityManager::HashTableResize(bool force)
{

    if (!mpHashTable) // assign the hash table
    {
        // Table hasnt been assigned yet, reset hash size index to 
        // be safe and allocate it

        mHashSize = msHashTableSizes[mHashTableSizeIndex];
        mpHashTable = new vsbBaseViewEntity *[mHashSize];

    } else
    {

        if (mHashTableSizeIndex == msHashTableSizeIndexMax)
            return false;  // reached max hash table size

        // get the next largest hash table size and allocate it

        if (!force && !AutoHashtableResize()) return false;

        delete [] mpHashTable;
        mHashTableSizeIndex++;
        mHashSize = msHashTableSizes[mHashTableSizeIndex];

        mpHashTable = new vsbBaseViewEntity *[mHashSize];
    }

    // zero the hash table
    for (int i=0; i<mHashSize; i++) mpHashTable[i] = 0;
    mEntityCount = 0;

    // reassign all the entity handles

    vsbBaseViewEntity *head = mpHead;

    while (head)
    {
        if (AssignEntityHandle(head) <0 ) return false;
        head = head->Next();
    }
    return true;
}


//-------------------------------------------------------------
// EntityCountHint()
//-------------------------------------------------------------
// Notify of expected number of entities to prepare hash table in advance
// The intended use of this member function is to be called before any 
// entities are created, to pre-allocate a sufficiently large hash table,
// However it can be called at any time. It uses HashTableResize() and
// is consequently subject to the same usage rules.


void vsbEntityManager::EntityCountHint(int n)
{
    n*=2; // account for saturation maximum
    int new_size_index;
    for(int i = new_size_index = mHashTableSizeIndex; i < msHashTableSizeIndexMax; i++)
    {
        if (n > msHashTableSizes[i]) 
            new_size_index = i;
        else
            break;
    }

    if (new_size_index > mHashTableSizeIndex)
    {
        mHashTableSizeIndex =  new_size_index-1; // need to decrement as HashTableResize
        HashTableResize(true);                   // automatically increments - note we need to force
												 // resize in case AutoResize is disabled;	
    }
}


//-------------------------------------------------------------
// LookupEntity()
//-------------------------------------------------------------
// Return a pointer to an entity based on the string name supplied
// returning 0 if auch an object is not found

vsbBaseViewEntity  *vsbEntityManager::LookupEntity(const string &name)
{
    int handle = LookupEntityHandle(name);
    if (handle < 0) return 0;
    return 
        LookupEntity(handle);
}

//-------------------------------------------------------------
// LookupEntity()
//-------------------------------------------------------------
// Return a pointer to an entity based on the handle ID supplied
// returning 0 if auch an object is not found

vsbBaseViewEntity  *vsbEntityManager::LookupEntity(int handle)
{
    if (handle < 0 || handle >= mHashSize || mpHashTable[handle] == 0) 
        return 0;
    return 
        mpHashTable[handle];

}


int	vsbEntityManager::GenerateHandle(std::string entityName)
{
	return vsbBaseViewEntity::HashValue(entityName,HashSize());
}

//-------------------------------------------------------------
//LookupEntityHandle()
//-------------------------------------------------------------
// Look up an Entity hamdle by name in the hash table 
// returning the address of the next free cell if the
// entity was not found (given an address has been supplied
// to store it in the nextFree index). 

int vsbEntityManager::LookupEntityHandle(const string &name, int *nextFree) 
{
    // store the initial hash representation of the name

    int start_hash_val, hash_val;

	mLookUpCollisions = 0;

    start_hash_val = hash_val = vsbBaseViewEntity::HashValue(name,HashSize());
   
    do
    {
        
       if (mpHashTable[hash_val] == 0) 
       {
           // search resulted in empty cell being found. Return its index
           // in nextFree if an address is supplied, and indicate that no
           // entity was found by returning -1

           if (nextFree)
               *nextFree = hash_val;
           return -1;
       }
       
       else
       {
           if (mpHashTable[hash_val]->mName == name)
           {    
               // the search was successful
               // nextFree if an address is supplied, is set to an ivalid value (-1)
               // to indicate that the next free sell has no been searched for

               if (nextFree) *nextFree = -1;
               return hash_val;
           }
		   mLookUpCollisions++;

           // a matching entity was not found at this hash table cell so advance the 
           // hash val to the next cell, wrapping back to zero if the end of the hash
           // table is encountered

           if (++hash_val >= mHashSize) hash_val = 0;

           if (hash_val == start_hash_val)
           {
               // if this block is entered it means neither the Entity was found nor
               // were there any free cells in the hash-table -- a condition that should
               // never occur as the hash table should not be allowed to become saturated
               // ie approx > 60% full

               *nextFree = -1;
               return -1;
           }
            
       }
   } while (true);

}

EEntityManagerStatus vsbEntityManager::AddEntity(vsbBaseViewEntity *newEntity)
{
    assert (newEntity!=0); // should not occur, not this functions responsibility

    if (!mpHashTable) // assign the hash table
        if (!HashTableResize())
            return EM_TableSaturated;

	if (LookupEntity(newEntity->Name()))
			return EM_NameClash;
    

    // add this entitiy to the head of the static l-list of entities;
    newEntity->Prev(0);
    newEntity->Next(mpHead);

    // if there is an entity at the head, set its prev pointer
    if (mpHead) mpHead->mpPrev = newEntity;

    mpHead = newEntity;     
    
    
    if ( AssignEntityHandle(newEntity) < 0 ) 
        return EM_NameClash;

    if (mEntityCount > mHashSize/2)
    {
        // Hash table is 50% saturated - do not allow any more 
        // objects to be created
        if (!mAutoHashResize || !HashTableResize())
            return EM_TableSaturated;
 
    }
	if (mpEntityAddedFunc)
		(*mpEntityAddedFunc)(newEntity->Name().c_str());
    return EM_OK;
}


void vsbEntityManager::RemoveEntity(vsbBaseViewEntity *entity)
{
	if (!entity) return;

	if (entity->Next() == 0)
	{
		//we are at the tail
		if (mpHead == entity)
			// this is the sole entry - clear the entity l-list
			mpHead = 0;
		else if (entity->Prev() != 0)
			// there are entities before it
			entity->Prev()->Next(0);
			
	} else if (mpHead == entity)
	{
		// we are at the head
		entity->Next()->Prev(0);
		mpHead = entity->Next();

	} else 
	{
		// we are somewhere between the head and tail of the entity l-list
		entity->Next()->Prev( entity->Prev() );
		entity->Prev()->Next( entity->Next() );
	}

	if (entity->Handle() >= 0) ClearEntityIdHandle(entity->Handle());
	entity->Prev(0);
	entity->Next(0);
	if (mpEntityRemovedFunc)
		(*mpEntityRemovedFunc)(entity->Name().c_str());

}

void vsbEntityManager::DestroyEntity(int entityHandle)
{
    vsbBaseViewEntity * be = LookupEntity(entityHandle);
    if (be)
        delete be;
}

void vsbEntityManager::DestroyEntity(const string &entityName)
{
    vsbBaseViewEntity * be = LookupEntity(entityName);
    if (be)
        delete be;
}


void vsbEntityManager::DestroyAll()
{
    vsbBaseViewEntity  *next = mpHead;
    while (next)
    {
        delete next;
        next = mpHead;
    }
}


void vsbEntityManager::SetEntityIdHandle(int entityHandle, vsbBaseViewEntity  *viewEntity)
{
    assert(viewEntity && mpHashTable[entityHandle] == 0);
    mEntityCount++;
	viewEntity->Handle(entityHandle);
    mpHashTable[entityHandle] = viewEntity;
}

void vsbEntityManager::ClearEntityIdHandle(int entityHandle)
{
    assert(mpHashTable[entityHandle] != 0);
    mEntityCount--;
    mpHashTable[entityHandle] = 0;
}

//---------------------------------------------------------------------
// AssignEntityHandle()
// Assuming hash table was cleared but a resize, this entity's handle is 
// recalculated and its entry in the hash table is initialised

int vsbEntityManager::AssignEntityHandle(vsbBaseViewEntity * entity)
{
	if(entity == 0) 
		return -1;

	int next_avail;
	int index = LookupEntityHandle(entity->Name(), &next_avail);

	if (index >= 0)
		return -1; // this name is already in use. 

	SetEntityIdHandle(next_avail, entity);
	return next_avail;
}


void  vsbEntityManager::SetCurrTime(float timeto)
{
	vsbBaseViewEntity *iterator;
	vsbBaseViewEntity *curr_entity;

	//cout << "vsbEntityManager::SetCurrTime -- timeto: " << timeto << "\n";

	for (iterator = mpHead; iterator; iterator = iterator->Next())
	{
//		if 	(iterator->IsActive()) iterator->SetCurrTime(timeto);
		// Jim's h@x fix for the trajectory lines to disappear before the missile is active.
		//	Ask themie if this will mess things up.
		iterator->SetCurrTime(timeto);
	}

	for (iterator = mpHead; iterator; )
	{
		curr_entity = iterator;
		iterator = iterator->Next();
		if (curr_entity->DeleteNextPass()) delete(curr_entity);
	}

	for (iterator = mpHead; iterator; iterator = iterator->Next())
	{
		if 	(iterator->DoPostProcess() && iterator->IsActive()) iterator->PostProcessFunc();
	}


	UpdateTransforms();
	mCurrTime = timeto;
	vsbViewportContext::ExplosionFXDirector().AdvanceAll();
	vsbViewportContext::SmokeFXDirector().Update(mCurrTime);
}

void  vsbEntityManager::AdvanceTime(float deltaTime)
{
	vsbBaseViewEntity *iterator;
	vsbBaseViewEntity *curr_entity;

	for (iterator = mpHead; iterator; iterator = iterator->Next())
	{
		if 	(iterator->IsActive()) iterator->AdvanceTime(deltaTime);
	}

	for (iterator = mpHead; iterator; )
	{
		curr_entity = iterator;
		iterator = iterator->Next();
		if (curr_entity->DeleteNextPass()) delete(curr_entity);
	}

	 for (iterator = mpHead; iterator; iterator = iterator->Next())
	{
		if 	(iterator->DoPostProcess() && iterator->IsActive()) iterator->PostProcessFunc();
	}


	UpdateTransforms();
	mCurrTime+= deltaTime;
	vsbViewportContext::ExplosionFXDirector().AdvanceAll();
}


void vsbEntityManager::UpdateTransforms()
{
	vsbBaseViewEntity *iterator;
    for (iterator = mpHead; iterator; iterator = iterator->Next())
	{
		if 	(iterator->IsActive() && iterator->IsVisible()) iterator->UpdateTransforms();
	}
}

////////////////////////////////////////////////////////



bool vsbEntityManager::RegisterViewEntityFactory(const string &entityType, vsbBaseViewEntityFactory *entityFactory)
{
    pair <map<string, vsbBaseViewEntityFactory *>::iterator, bool> res;
	entity_factories.insert(pair<string, vsbBaseViewEntityFactory *>(entityType, entityFactory));
	return res.second;
}

vsbBaseViewEntity *vsbEntityManager::CreateViewEntity(const string &entityType, const string &entityName)
{
	map<string, vsbBaseViewEntityFactory *>::iterator p; 
    p = entity_factories.find(entityType);


	if (p == entity_factories.end()) 
		return 0;


	vsbBaseViewEntityFactory *entityBuilder = p->second;
	if (!entityBuilder) 
		return 0;


	if (entityName=="")
		return entityBuilder->Create();
	else
		return entityBuilder->Create(entityName);
}

vsbBaseViewEntity *vsbEntityManager::CreateViewEntity(const string &entityType )
{
	string entity_name = "";
	return CreateViewEntity(entityType, entity_name );
}


void vsbEntityManager::DeregisterViewEntityFactory(const string &entityType)
{
	map<string, vsbBaseViewEntityFactory *>::iterator p; 
    p = entity_factories.find(entityType);
	if (p == entity_factories.end()) return;
	vsbBaseViewEntityFactory *entityBuilder = p->second;
	entity_factories.erase(p);
	delete entityBuilder;
}

void vsbEntityManager::DeregisterAllViewEntityFactories()
{
    map<string, vsbBaseViewEntityFactory *>::iterator p; 
	
	int i = entity_factories.size();
	
	if (i==0) return;

   for (p = entity_factories.begin(); p != entity_factories.end();  )
	{
	    vsbBaseViewEntityFactory *entityBuilder = p->second;
		entity_factories.erase(p);
		delete entityBuilder;
		p = entity_factories.begin();

	}

}

