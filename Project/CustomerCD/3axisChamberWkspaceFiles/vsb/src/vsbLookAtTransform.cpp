//////////////////////////////////////////////////////////////////
/*! 
    \file vsbLookAtTransform.cpp
	\brief Implements a class that generates a matrix orienting the camera
	to look at a designated target

    A vsbLookAtTransform instance, given an initial, camera position and 
	orientation in space, and a target or list of targets, can 
	generate a matrix representing a look-at vector from the camera 
	to the highest priority target in terms of a viewpoint orientation 
	matrix
 

	\sa vsbLookAtTransform.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <string>
#include <vector>
#include <map>
#include <set>
#include <algorithm>
using namespace std;

#include "ssg.h"

#include "vsbLookAtTransform.h"
#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"

#include "vsbEntityManager.h"
#include "vsbBaseViewEntity.h"
#include "vsbViewportContext.h"

/* \if LOOKATTRANSFORM_PRIVATE */

//////////////////////////////////////////////////////////////////
/*! 
    \class _vsbLookAtTransform

	\brief Private encapsulation class for use by vsbLookAtTransform
    
    Encapsulates the private data of vsbLookAtTransform.

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

class _vsbLookAtTransform
{
friend class vsbLookAtTransform;
private:
	vsbCameraManager	*mpCameraManager; //!< Pointer to camera manager

                        //! Low lever MakeLookAtMatrix function
	void				MakeLookAtMatrix(sgdMat4 lookAt, const sgdCoord &camOrigin, const sgdVec3 lookatPoint);

                        //! List of target entity names
	vector<string>     mTargetEntityNames;


};


void _vsbLookAtTransform::MakeLookAtMatrix(sgdMat4 lookAt, const sgdCoord &camOrigin, const sgdVec3 lookatPoint)
{
	sgdVec3 target_vec;
	sgdMat4 src_rot;
	sgdVec3 deflt_up_vec = {0, 0, 1};
	sgdVec3 new_up_vec;
	sgdVec3 cam_hpr;
	sgdVec3 look_at_pt;


	// Find vector to target
	sgdCopyVec3(target_vec, lookatPoint);

    // adjust coordinates from RH to SSG

	vsbToScaledVSBCoords(target_vec);
/*
    target_vec[1] = - target_vec[1]; 
    target_vec[2] = - target_vec[2]; 
*/
	sgdCopyVec3(look_at_pt, target_vec);
	sgdSubVec3(target_vec, camOrigin.xyz);

	// determine if co-located camera & target - if so set target
	// vec to the original camera look vector, and create a bogus
	// look_at_pt down that vector
	if (sgdScalarProductVec3 ( target_vec, target_vec) < 1.0e-10)
	{
		sgdMat4 dst;
		sgdSetVec3(target_vec, 1, 0, 0);
		sgdMakeRotMat4(dst, camOrigin.hpr );
		sgdXformPnt3(target_vec, dst);
		sgdCopyVec3(look_at_pt, target_vec);
	}

	sgdNormalizeVec3(target_vec);

	// Make camera rot matrix
	sgdCopyVec3(cam_hpr, camOrigin.hpr);
	sgdMakeRotMat4 (src_rot, cam_hpr );

	// get new up vec, using camera rot matrix
	sgdXformPnt3(new_up_vec, deflt_up_vec, src_rot);

	if (sgdEqualVec3(new_up_vec, target_vec))
	{
		// target vec and up vec must not be same 
		// modify up vec by 10 deg pitch
		cam_hpr[1]+=1;
		sgdMakeRotMat4 (src_rot, cam_hpr);
		sgdXformPnt3(new_up_vec, deflt_up_vec, src_rot);

		// test for correctness
		assert(!sgdEqualVec3(new_up_vec, target_vec));
	}

	// make the new vector
//	sgMakeLookAtMat4 ( lookAt,  camOrigin.xyz, look_at_pt, new_up_vec ) ;

	{
		// Caveats:
		// 1) In order to compute the line of sight, the eye point must not be equal
		//    to the center point.
		// 2) The up vector must not be parallel to the line of sight from the eye
		//    to the center point.

		/* Compute the direction vectors */
		sgdVec3 x,y,z;

		/* Y vector = center - eye */
		sgdSubVec3 ( y, look_at_pt, camOrigin.xyz ) ;

		/* Z vector = up */
		sgdCopyVec3 ( z, new_up_vec ) ;

		/* X vector = Y cross Z */
		sgdVectorProductVec3 ( x, y, z ) ;

		/* Recompute Z = X cross Y */
		sgdVectorProductVec3 ( z, x, y ) ;

		/* Normalize everything */
		sgdNormaliseVec3 ( x ) ;
		sgdNormaliseVec3 ( y ) ;
		sgdNormaliseVec3 ( z ) ;

		/* Build the matrix */
		sgdSetVec4 ( lookAt[0], x[0], x[1], x[2], SG_ZERO ) ;
		sgdSetVec4 ( lookAt[1], y[0], y[1], y[2], SG_ZERO ) ;
		sgdSetVec4 ( lookAt[2], z[0], z[1], z[2], SG_ZERO ) ;
		sgdSetVec4 ( lookAt[3], camOrigin.xyz[0], camOrigin.xyz[1], camOrigin.xyz[2], SG_ONE ) ;

	}
	
}


/* \endif LOOKATTRANSFORM_PRIVATE */

/////////////////////////////////////////////////////

vsbLookAtTransform::vsbLookAtTransform(vsbCameraManager *cm)
{
	mpPrivateData = new _vsbLookAtTransform;
	mpPrivateData->mpCameraManager = cm;
	mpPrivateData->mTargetEntityNames.clear();
}

vsbLookAtTransform::~vsbLookAtTransform()
{
	delete mpPrivateData;
}

vsbLookAtTransform &vsbLookAtTransform::operator=(const vsbLookAtTransform &from)
{
	mpPrivateData->mTargetEntityNames.clear();
	mpPrivateData->mTargetEntityNames = from.mpPrivateData->mTargetEntityNames;
	return *this;
}

void vsbLookAtTransform::AddTargetEntity(const string &name, int position)
{
	if (position < 0 || 
		mpPrivateData->mTargetEntityNames.empty() ||
		position >= mpPrivateData->mTargetEntityNames.size())
		mpPrivateData->mTargetEntityNames.push_back(name);
	else
	{
		int i;
		vector<string>::iterator iter;
		for (i=0, iter = mpPrivateData->mTargetEntityNames.begin();
		     i<position; i++, iter++);
		mpPrivateData->mTargetEntityNames.insert(iter, name);
//		mpPrivateData->mTargetEntityNames.insert(position, name);
	}
}

const string& vsbLookAtTransform::GetTargetEntity(int position) const
{
	static string dummy = "";
	if (position < 0 || 
		mpPrivateData->mTargetEntityNames.empty() ||
		position >= mpPrivateData->mTargetEntityNames.size())
		return dummy;
	else
	{
		return mpPrivateData->mTargetEntityNames[position];
	}
}
void vsbLookAtTransform::RemoveTargetEntity(const string &name)
{
	vector<string>::iterator iter;
	iter = find(mpPrivateData->mTargetEntityNames.begin(),
				mpPrivateData->mTargetEntityNames.end(), name);
	if (iter != mpPrivateData->mTargetEntityNames.end())
		mpPrivateData->mTargetEntityNames.erase(iter);
}

void vsbLookAtTransform::RemoveTargetEntity(int position)
{
	int i;
	vector<string>::iterator iter;
	for (i=0, iter = mpPrivateData->mTargetEntityNames.begin();
	     i<position; i++, iter++);
	if (iter != mpPrivateData->mTargetEntityNames.end())
		mpPrivateData->mTargetEntityNames.erase(iter);
}

void vsbLookAtTransform::ClearTargetEntities()
{
	mpPrivateData->mTargetEntityNames.clear();
}

int  vsbLookAtTransform::CountTargetEntities()
{
    return mpPrivateData->mTargetEntityNames.size();
}

vsbBaseViewEntity *vsbLookAtTransform::FirstActiveTargetEntity()
{
	vsbBaseViewEntity *entity_to_lookat;
	vector<string>::iterator iter;

	for(iter = mpPrivateData->mTargetEntityNames.begin();
	    iter != mpPrivateData->mTargetEntityNames.end();
		iter++)
		{
			string &name = *iter;
			entity_to_lookat = vsbViewportContext::EntityManager().LookupEntity(name);
			if (!entity_to_lookat) continue;
			if (entity_to_lookat->IsActive()) return entity_to_lookat;
		}
	return 0;
}

vsbBaseViewEntity *vsbLookAtTransform::FirstVisibleTargetEntity()
{
	vsbBaseViewEntity *entity_to_lookat;
	vector<string>::iterator iter;

	for(iter = mpPrivateData->mTargetEntityNames.begin();
	    iter != mpPrivateData->mTargetEntityNames.end();
		iter++)
		{
			string &name = *iter;
			entity_to_lookat = vsbViewportContext::EntityManager().LookupEntity(name);
			if (!entity_to_lookat) continue;
			if (entity_to_lookat->IsVisible()) return entity_to_lookat;
		}
	return 0;
}


sgdMat4 &vsbLookAtTransform::MakeLookAtTargetMatrix(const sgdCoord &camOrigin)
{
	static sgdMat4 lookat_matrix;

	vsbBaseViewEntity *entity_to_lookat = FirstVisibleTargetEntity();
	if (entity_to_lookat)
	{
		mpPrivateData->MakeLookAtMatrix(lookat_matrix, camOrigin, entity_to_lookat->EntityPosXYZ());
	} else
	{
		sgdMakeCoordMat4(lookat_matrix, &camOrigin);
	}
	return lookat_matrix;



}



