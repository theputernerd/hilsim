#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

#include "sg.h"
#include "ssg.h"

#include "vsbCockpit.h"
#include "vsbRenderOptions.h"
#include "vsbSysPath.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentOptions.h"


vsbCockpit::vsbCockpit()
{
	mpCockpitRoot = new ssgRoot;
	mpCockpitXfm = new ssgTransform;
	mpCockpitRoot->addKid(mpCockpitXfm);
	mpCockpitRoot->ref();


	 mpColorArr   = 0;		//!< Cockpit colour array
     mpVertexArr  = 0;		//!< Cockpit vertex array
     mpTextureArr = 0;		//!< Cockpit texture array	
	 Build();
}

vsbCockpit::~vsbCockpit( void )
{
  ssgDeRefDelete(mpCockpitRoot);
}


void vsbCockpit::Reposition(sgCoord &coord)
{
	 mpCockpitXfm->setTransform ( &coord ) ;
}

void vsbCockpit::Reposition(sgMat4 mat4)
{
	 mpCockpitXfm->setTransform ( mat4 ) ;
}

void vsbCockpit::RepositionWorldFrame(float x, float y, float z, float psi, float the, float phi)
{
	sgCoord coord;
	coord.xyz[0] = x; coord.xyz[1] = y; coord.xyz[2] = z;
	coord.hpr[0] = psi; coord.hpr[0] = the; coord.hpr[0] = phi;

	vsbToScaledVSBCoords( coord.xyz, coord.hpr );

	
	mpCockpitXfm->setTransform ( &coord ) ;
}


void vsbCockpit::RepositionWorldFrame(sgVec3 pos, sgVec3 rot)
{
	sgCoord coord;
	sgCopyVec3(coord.xyz, pos);
	sgCopyVec3(coord.hpr, rot);
	vsbToScaledVSBCoords(coord.xyz, coord.hpr );
	mpCockpitXfm->setTransform ( &coord ) ;
}


//!		 the cloud layer
void vsbCockpit::Draw()
{
    glClear(GL_DEPTH_BUFFER_BIT);
	ssgSetNearFar (0.1f, 5.0f);
	ssgCullAndDraw( mpCockpitRoot );
}