#ifndef _vsbExplosionFXDirector_H
#define _vsbExplosionFXDirector_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _LIST_
#include <list>
#define _LIST_
#endif

#ifndef _VECTOR_
#include <vector>
#define _VECTOR_
#endif


#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbStateManager_H
#include "vsbStateManager.h"
#endif

#ifndef _vsbExplosion_H
#include "vsbExplosion.h"
#endif 

#ifndef parserH
#include "parser.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

class DLL_VSB_API vsbExplosionDefinition
{
	friend class DLL_VSB_API vsbExplosion;
public:
                            vsbExplosionDefinition(const vsbExplosionDefinition &src);
                            vsbExplosionDefinition(int frames, float delay,const std::string &textureFilename = "", bool preload = true);
                            vsbExplosionDefinition();

    vsbExplosionDefinition  &operator =  (const vsbExplosionDefinition &src) ;

    float                   FrameDelay() const {return mFrameDelay; } 
    int                     FrameCount() const {return mFrameCount; }
	bool					Preload() const {return mPreload; }
	bool					Loaded() const {return mLoaded; }
	void					Loaded(bool truefalse) { mLoaded = truefalse; }
    const   std::string &   TextureFilename() const {return mTextureFilename; }

private:
	std::string			   mTextureFilename; //! The filename name of the texture	
    float                  mFrameDelay; //!< The number pageswaps frames before the next animation frame
    int                    mFrameCount; //!< The total number of frames in the animation
	bool				   mPreload;    //!< Indicates whether the state (texture) for this explosion is to be pre-loaded
	bool				   mLoaded;	
};

class _vsbExplosionDefinitionParser;

class DLL_VSB_API vsbExplosionFXDirector
{
	friend class DLL_VSB_API vsbExplosion;
	friend class _vsbExplosionDefinitionParser;
public:
            vsbExplosionFXDirector();
    virtual ~vsbExplosionFXDirector();

    vsbExplosion *    Initiate(float x, float y, float z, float size, const std::string explosionTypeName = "");
    vsbExplosion *    Initiate(sgVec3 pos, float size, const std::string explosionTypeName = "");
    vsbExplosion *    Initiate(float x, float y, float z, float size, int explosionIndex);
    vsbExplosion *    Initiate(sgVec3 pos, float size, int explosionIndex);
	bool    Update(const std::string &explosionTypeName, vsbExplosion *expl);
    void    StopAll();   
	void    Stop(int id);
	void    Stop(vsbExplosion *expl);
	void    AdvanceAll();
	void	CameraCentricTranslation();
    
    static  void StateLoaderCallback(vsbStateManager *stateManager);

    std::string  DebugStatusString();

	static void LoadExplosionDefinitions(vsbStateManager *stateManager, const std::string &explosionIndexFile);
	static void ClearExplosionDefinitions();
	ssgSimpleState *GetState(const std::string &explTypeName);

	vsbExplosion *FindExplosionById(int id);

	std::list<vsbExplosion *> &ActiveExplosions(){ return mActiveExplosions;};
	static std::map<std::string, vsbExplosionDefinition> &ExplosionDefinitions() {return msExplosionDefinitions;}

	const char *LoadFile(const std::string &filename);
	bool SaveFile(const std::string &filename);



private:
    static int	mInstanceCount;
    void		FreeInactiveExplosions();



	vsbExplosion *GetFreeExplosion(int requestedFrameCount = 0); // Gets an explosion from the free-list
	void		  ClearFreeList();
	void		  CreateNewExplosion(); // Creates a new inactive explosion and inserts it into toe free list
	ssgSimpleState *GetState(std::map<std::string, vsbExplosionDefinition>::iterator &iter);

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 
#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

    std::list<vsbExplosion *> mActiveExplosions;
    std::list<vsbExplosion *> mFreeExplosions;
    static std::map<std::string, vsbExplosionDefinition> msExplosionDefinitions;

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

};

/*! \func vsbMakeExplosionFXPath

 Composes an explicit, fully qualified path to the explosion effects filename 
 specified in the fname parameter. This is based on the SysPath and the hard 
 wired rule that explosion file paths in the explosions index file are 
 implicitly relative to the "effects/explosions" subdirectory of 
 the directory specified by the vsbGetSysPath() function.
*/

std::string vsbMakeExplosionFXPath(std::string fname);

class DLL_VSB_API vsbExplosionSeting;

class DLL_VSB_API vsbExplosionFXListParser: public Parser::TParseObjectBase
{
public:
	vsbExplosionFXListParser(vsbExplosionFXDirector &efxd);
	virtual ~vsbExplosionFXListParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
	vsbParserHelper *mpOutput;
	std::vector<vsbExplosionSeting> mExplosionList;

	vsbExplosionFXDirector &mrExplosionFXDirector;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};


#endif