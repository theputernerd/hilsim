
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
using namespace std;

#ifdef WIN32
#include <windows.h>
#endif

#include<GL/gl.h>


#include "fnt.h"
#include "pu.h"
#include "vsbF18HUD.h"
#include "vsbF18Cockpit.h"

#include "vsbViewportContext.h"

#include "tool_mathtools.h"

vsbF18HUD::vsbF18HUD():vsbHUDFramework()
{
    mHaveVelocityVec = false;
    mHavePositionVec = false;
	mHaveAirSpeed = false;
	mHaveAttitude = false;
	mHaveSelectedWeapon = false;
	mHaveAspect = false;
	mHaveLars = false;
//	mHaveSD = false;
	mHaveTargetPosition = false;
	mHavePosition = false;
	m_bHaveHMS = false;

	mpCockpit = new vsbF18Cockpit;

    //! Add the heading HUD 
    mAltitude = 0;
    mAirspeed = 0;
    mAlpha    = 0;
    mMach     = 0;
    mG        = 0;
    mWatermarkOn = true;  
    mLadderSteepnessOn = true;
    mPitchLadderX = 0; 
    mPitchLadderY = 0; 

	m_larsMode = vsbLarsHUD::eASRAAM;
	m_rMin = 0.0f;
	m_rMax = 0.0f;
	m_rNoEscape = 0.0f;
	m_range = 0.0f;

    for (int i=0; i<3; i++)
    {    
       mXYZ[i] = mHPR[i] = mVel[i] = 0; 
    }

    m_pHeadingHUD = new vsbHeadingHUD;
    m_pHeadingHUD->IsEnabled(true);
    m_pHeadingHUD->SetRectangle(-30, 0, 60, 4);
    m_pHeadingHUD->SetMarkings(30, 10);
    m_pHeadingHUD->HookHeading(&mHPR[0]);
	m_pHeadingHUD->SetPosition( 0.0f, 24.0f );
	m_pHeadingHUD->SetScale( 0.7f, 0.7f );
    AddHUDComponent(m_pHeadingHUD);

    m_pAltAirspeedHUD = new vsbAltAirspeedHUD;  
    m_pAltAirspeedHUD->IsEnabled(true);
    m_pAltAirspeedHUD->SetRectangle(0, 0, 90, 6, 18);
    m_pAltAirspeedHUD->HookAltAirspeed( &mAltitude, &mAirspeed);
	m_pAltAirspeedHUD->SetPosition( -43.0f, 0.0f );
	m_pAltAirspeedHUD->SetScale( 0.7f, 0.7f );
    AddHUDComponent(m_pAltAirspeedHUD);

    m_pAttitudeHUD  = new vsbAttitudeHUD;
    m_pAttitudeHUD->IsEnabled(true);
//    m_pAttitudeHUD->SetPos( 0.0f, 0.0f );
    m_pAttitudeHUD->HookAttitude(&mAlpha, &mMach, &mG);
	m_pAttitudeHUD->SetPosition( -35.0f, -5.0f );
    AddHUDComponent(m_pAttitudeHUD);

    m_pVelocityVectorHUD  = new vsbVelocityVectorHUD;
    m_pVelocityVectorHUD->IsEnabled( true );
//	m_pVelocityVectorHUD->SetPos(0, 0, 25, 8 /*deg*/);
	m_pVelocityVectorHUD->SetPosition( 0.0f, -12.0f );
    m_pVelocityVectorHUD->HookVelocityVector(	&mHPR[0],&mHPR[1], &mHPR[2], &mVel[0], &mVel[1], &mVel[2] );
	m_pVelocityVectorHUD->SetScale( 0.7f, 0.7f );
	m_pVelocityVectorHUD->Caged( true );
    AddHUDComponent(m_pVelocityVectorHUD);

    m_pPitchLadderHUD = new vsbPitchLadderHUD;
    m_pPitchLadderHUD->IsEnabled(true);
    m_pPitchLadderHUD->HookPitchLadder(&mHPR[1], &mHPR[2], &mPitchLadderX, &mPitchLadderY, &mWatermarkOn, &mLadderSteepnessOn);
	mWatermarkOn = false;
	m_pPitchLadderHUD->SetGeometryPos(0, 0, 10, 3, 50, 62.25);
    m_pPitchLadderHUD->SetMarkings(20, 5);
//	m_pPitchLadderHUD->SetPosition( 0.0f , -4.0f);
	m_pPitchLadderHUD->SetPosition( 0.0f , -14.0f);
	m_pPitchLadderHUD->SetScale( 0.6f, 0.6f );

    AddHUDComponent(m_pPitchLadderHUD);

    m_pBankHUD = new vsbBankHUD;
    m_pBankHUD->IsEnabled(true);
    m_pBankHUD->HookBank(&mHPR[2]);
    m_pBankHUD->SetPos(0, 0, 60, 2);
	m_pBankHUD->SetPosition( 0.0f, 18.0f );
	m_pBankHUD->SetScale( 0.6f, 0.6f );
    AddHUDComponent(m_pBankHUD);

	// lars.
	m_pLarsHUD= new vsbLarsHUD;
    m_pLarsHUD->IsEnabled(true);
    m_pLarsHUD->SetPos(0, 0, 18.6963f);
	m_pLarsHUD->SetScale( 0.6f, 0.6f );
    m_pLarsHUD->SetPosition( 0.0f, 0.0f );
	m_pLarsHUD->HookLars( &m_larsMode, &m_rMin, &m_rMax, &m_rNoEscape, &m_range );
	m_pLarsHUD->HookSD( &m_SDx, &m_SDy );
    AddHUDComponent(m_pLarsHUD);


	// Aspect Vector.
	m_pAspectVectorHUD = new vsbAspectVectorHUD;
	m_pAspectVectorHUD->IsEnabled( true );
//	m_pAspectVectorHUD->HookAspectVector( &m_aspect, &m_speed );
	m_pAspectVectorHUD->HookAspectVector( &m_aspect, &mAirspeed );
	m_pAspectVectorHUD->SetScale( 0.6f, 0.6f );
	m_pAspectVectorHUD->SetRadius( 18.6963f );	// same as Lars radius.
	AddHUDComponent( m_pAspectVectorHUD );

	
	// SelectedWeapon
	m_pSelectedWeaponHUD = new vsbSelectedWeaponHUD;
	m_pSelectedWeaponHUD->IsEnabled( true );
	m_pSelectedWeaponHUD->SetPosition( 2.0f, -25.0f );
	m_pSelectedWeaponHUD->SetScale( 0.7f, 0.7f );
	m_pSelectedWeaponHUD->SetSelectedWeapon( "AS" );
	AddHUDComponent( m_pSelectedWeaponHUD );


	// target locator
	m_pTargetLocator = new vsbTargetLocatorHUD;
	m_pTargetLocator->IsEnabled( true );
	m_pTargetLocator->HookLars( m_pLarsHUD );
	m_pTargetLocator->HookPos( &m_Pos );
	m_pTargetLocator->HookTargetPos( &m_targetPos );
	m_pTargetLocator->HookWithinRadar( &m_bWithinRadar );
    AddHUDComponent( m_pTargetLocator );


	// HMS
	m_pHMSHUD = new vsbHMSHUD;
	m_pHMSHUD->IsEnabled( false );
	m_pHMSHUD->HookLars( m_pLarsHUD );
	m_pHMSHUD->HookAspectVector( m_pAspectVectorHUD );
	m_pHMSHUD->HookHeading( m_pHeadingHUD );
	m_pHMSHUD->HookAltAirSpeed( m_pAltAirspeedHUD );
	m_pHMSHUD->HookAttitude( m_pAttitudeHUD );
	m_pHMSHUD->HookWeaponName( m_pSelectedWeaponHUD );

	m_pHMSHUD->HookHasTarget( &m_bHasTarget );
	m_pHMSHUD->HookPos( &m_Pos );
	m_pHMSHUD->HookTargetPos( &m_targetPos );
	m_pHMSHUD->HookWithinRadar( &m_bWithinRadar );
	m_pHMSHUD->HookWithinSeeker( &m_bWithinSeeker );

    AddHUDComponent(m_pHMSHUD);


    SetLogicalDimensions(80, 53);
    PixelMargin(25);

	// Gah!! This makes the target designator box incorrect.
	//	Note sure it is needed anyways.
//	SetPosition( -1.5f, -6.0f );
	SetScale ( 0.9f, 0.9f );
}

vsbF18HUD::~vsbF18HUD()
{
	delete mpCockpit;
}

void vsbF18HUD::DrawHud(vsbCameraManager *cammgr)
{
//	m_pVelocityVectorHUD->IsEnabled(false);
//	m_pAltAirspeedHUD->IsEnabled(mHaveVelocityVec);

	m_pHMSHUD->SetCam( cammgr->ActiveCam() );
	m_pTargetLocator->SetCam( cammgr->ActiveCam() );

	// Set all HUD's to Enable.					// Fields Required for HUD component.
	m_pHeadingHUD->IsEnabled( true );			//  orientation
	m_pAltAirspeedHUD->IsEnabled( false );		//	Position, airspeed/velocity.
	m_pAttitudeHUD->IsEnabled( false );			//	Attitude data.
	m_pVelocityVectorHUD->IsEnabled( false );	//	orientation, velocity
	m_pPitchLadderHUD->IsEnabled( true );		//	orientation
	m_pBankHUD->IsEnabled( true );				//	orientation (roll)
	m_pLarsHUD->IsEnabled( false );				//	Lars data.
	m_pAspectVectorHUD->IsEnabled( false );		//	Aspect data.
	m_pSelectedWeaponHUD->IsEnabled( false );	//	Selected Weapon	
	m_pTargetLocator->IsEnabled( false );		//	Target Position
	m_pHMSHUD->IsEnabled( false );				//	ON/OFF + HeadTracker driver working?.


	// Disable HUD's depending on data set previous frame.
	//	Orientation is always assumed to exist.

	// Set Airspeed from Velocity vector if not explicity set
	//	Note: It might be explicitly set from calibrated airspeed calculations.
	if( (!mHaveAirSpeed) && mHaveVelocityVec)
	{
		// convert from mps_to_kts
		mAirspeed = sgdLengthVec3( mVel ) * 1.94384449244;
		mHaveAirSpeed = true;
	}

	// AltAirspeed
	if( mHavePositionVec && mHaveAirSpeed )
		m_pAltAirspeedHUD->IsEnabled( true );

	// Attitude
	if( (!mHaveAttitude) && mHaveAirSpeed )
	{
		// Set our attitude data.

		// convert from kts_to_mps, then to mach (divide by speed of sound at sea level).
		float mach = mAirspeed * 0.514444444444 / 340.294;

		// calculate angle of attack, from velocity vector and orientation.

		// Transform mVel into body space.
		//	Or this might be inverted.
		double bodyVel[3];
		tool::transform321( mVel[0], mVel[1], mVel[2], mHPR[0] * tool::constants::DEG_TO_RAD,
														mHPR[1] * tool::constants::DEG_TO_RAD,
														mHPR[2] * tool::constants::DEG_TO_RAD,
														&bodyVel[0], &bodyVel[1], &bodyVel[2] );	
		// Calculate angle of attack.
		// convert to tan2 or whatever after this is working.
		float aoa;
		if( fabs( bodyVel[0] ) > 0.00001f )
			aoa = atanf( bodyVel[2] / bodyVel[0] ) * tool::constants::RAD_TO_DEG;
		else
		{
			// x is not much, hence we want to avoid divide by zero.
			if( fabs( bodyVel[2] ) < 0.00001f )
			{
				aoa = 0.0f;
			}
			else
			{
				if( bodyVel[2] < 0.0f )
					aoa = -90.0f;
				else
					aoa = 90.0f;
			}

		}

//		SetAttitudeData( mach, 0.0f, 0.0f );
		SetAttitudeData( mach, aoa, 0.0f );
		mHaveAttitude = true;
	}

	if( mHaveAttitude )
		m_pAttitudeHUD->IsEnabled( true );

	// Velocity Vector
	if( mHaveVelocityVec )
		m_pVelocityVectorHUD->IsEnabled( true );

	// Lars
	if( mHaveLars && mHaveLars )
		m_pLarsHUD->IsEnabled( true );

	// Aspect Vector
	if( mHaveAspect && mHaveAirSpeed )
		m_pAspectVectorHUD->IsEnabled( true );

	// Selected Weapon
	if( mHaveSelectedWeapon )
		m_pSelectedWeaponHUD->IsEnabled( true );

	// Target Locator
	if( mHaveTargetPosition && mHavePosition )
		m_pTargetLocator->IsEnabled( true );

	// HMS
	if( m_bHaveHMS )	// && mHavePosition??
		m_pHMSHUD->IsEnabled( true );

    vsbHUDFramework::DrawHud(cammgr);


	// This isn't actually a good way to do this, because a frame can be needed to be drawn
	//	Multiple times at the same time-step. (if the screen is overdrawn for instance, or minimizing
	//	the screen whilst paused).
	// Perhaps if the traj Entities hold all this data, the HUD can read from the entity the fields
	//	it needs and so will rectify this problem.
    mHaveVelocityVec = false;
    mHavePositionVec = false;

	mHaveAirSpeed = false;
	mHaveAttitude = false;
	mHaveLars = false;
	mHaveAspect = false;
	mHaveSelectedWeapon = false;
	mHavePosition = false;
	mHaveTargetPosition = false;

	m_bHaveHMS = false;
}


void vsbF18HUD::SetAirSpeed( const float airSpeed )
{ 
	mAirspeed = airSpeed;

	mHaveAirSpeed = true;
}


void vsbF18HUD::SetAttitudeData( const float mach, const float alpha, const float G )
{ 
	mMach = mach;
	mAlpha = alpha;
	mG = G;

	mHaveAttitude = true;
}


void vsbF18HUD::SetLarsData( const float rMin, const float rMax, const float rNoEscape, const float range )
{
	m_rMin = rMin;
	m_rMax = rMax;
	m_rNoEscape = rNoEscape;
	m_range = range;

	mHaveLars = true;
}


void vsbF18HUD::SetSteeringDotData( const float SDx, const float SDy )
{
	m_SDx = SDx;
	m_SDy = SDy;

//	mHaveSD = true;
}


void vsbF18HUD::SetAspectVector( const float aspect )
{ 
	m_aspect = aspect;

	mHaveAspect = true;
}

void vsbF18HUD::SetPosData( const sgdVec3 &pos )
{
	SetPosData( pos[0], pos[1], pos[2] );
}


void vsbF18HUD::SetPosData( const double x, const double y, const double z )
{
	sgdSetVec3( m_Pos, x, y, z );

	mHavePosition = true;
}


void vsbF18HUD::SetTargetPosData( const sgdVec3 &targetPos )
{
	SetTargetPosData( targetPos[0], targetPos[1], targetPos[2] );
}


void vsbF18HUD::SetTargetPosData( const double x, const double y, const double z )
{
	sgdSetVec3( m_targetPos, x, y, z );

	mHaveTargetPosition = true;
}


void vsbF18HUD::SetSelectedWeapon( const char *name )
{
	m_pSelectedWeaponHUD->SetSelectedWeapon( name );

	mHaveSelectedWeapon = true;
}
