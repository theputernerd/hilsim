//////////////////////////////////////////////////////////////////
/*! 
    \file vsbPlaneTextureList.h
    \brief Defines the class vsbPlaneTextureList
    
    The vsbPlaneTextureList class maintains a list of  texture names, 
    texture filenames and scales for all ground plane, or cloud texture 
    objects. 
  
    The texture names and scales are extracted from a file containing the tabulated 
    filename and scales. This file would reside in the directory along with the 
    texture files and act as an index into the textures.

    \sa vsbPlaneTextureList.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbPlaneTextureList_H
#define _vsbPlaneTextureList_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif


const int MAX_TERRAIN_TEXTURES = 500;

class DLL_VSB_API vsbPlaneTextureList;
class DLL_VSB_API vsbPlaneTextureListParser;

 
//////////////////////////////////////////////////////////////////
/*! 
    \class vsbPlaneTextureEntry
    \brief Auxillary class for the vsbPlaneTextureList class
  
    This class is a single entry in the vsbPlaneTextureList object, and 
    contains the file name and default scale for the given texture. Scale 
    meaning the real world size in metres, one side of the square the 
    texture represents.

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbPlaneTextureEntry
{
	friend class vsbPlaneTextureList;
	friend class vsbPlaneTextureListParser;
protected:
							// ----- Constructors -- all private to restrict instantiation
							//						 privelleges to vsbPlaneTextureList;

                            //! Default constructor
							vsbPlaneTextureEntry();

                            /*! \brief Constructors with filename and scale initialisation
                                \param label The string name the texture will be referenced by
                                \param filename The name of the texture file
                                \param scale The real world length one side of the square texture map represents
                            */
                            vsbPlaneTextureEntry(const std::string &label, const std::string &filename, float scale);
	
public:


                            //! Virtual destructor
	virtual					~vsbPlaneTextureEntry();

                            //! Assignment Operator 
	vsbPlaneTextureEntry &	operator =(const vsbPlaneTextureEntry &someGroundTexture);

							// Accessor functions

                            //! Get the texture scale
	float					Scale() const					{return mScale;};
                            
                            //! Set the texture scale
	void					Scale(float someScale)			{mScale = someScale;};

                            //! Get the texture filename
	const std::string &			Fname()	const					{return mFname;};

                            //! Set the texture filename
	void					Fname(const std::string &someName)	{mFname = someName;};

                            //! Get the texture label name
	const std::string &			Label()	const					{return mLabel;};

                            //! Set the texture label name
	void					Label(const std::string &someLabel)	{mLabel = someLabel;};

private:
	float					mScale;  //!< Scale of texture (in Metres)



	int						mIndex;  //!< Index of the texture ie, offset in the texture list

	int						Index() {return mIndex;};
	void					Index(int i) {mIndex = i;};

	std::string				mLabel;  //!< The name the texture is internally known as
	std::string				mFname;  //!< Texture file name (no path information please) 

};


//////////////////////////////////////////////////////////////////
/*! 
    \class vsbPlaneTextureList
    \brief Maintains a list of texture names, texture filenames and scales for all 
    ground plane, or cloud texture objects. 
  
    The texture names and scales are extracted from a file containing the tabulated 
    filename and scales. This file would reside in the directory along with the 
    texture files and act as an index into the textures.
   
    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbPlaneTextureList
{
friend vsbPlaneTextureListParser;
public:

							//! Default constructor
							vsbPlaneTextureList();

							/*! \brief Constructor with texture index file name parameter.

							   This constructor loads the texture list from the texture
							   index file.
                            */
							vsbPlaneTextureList(const std::string &path);

							//! Virtual Destructor
	virtual					~vsbPlaneTextureList();

							/*! \brief Overloaded [] operator to return an indexed vsbPlaneTextureEntry
                                \param i The index of the vsbPlaneTextureEntry in the list
                                \returns reference to internally held vsbPlaneTextureEntry
                             */
	const vsbPlaneTextureEntry &	operator[](int i) const;

							/*! \brief Refresh the texture list from the specified texture 
                                index file
                                \param path The fully qualified index file path 
                                \returns reference to internally held vsbPlaneTextureEntry
                            */

	void					LoadList(const std::string &path);

                            //! Get the number of texture list entries available
	int						Count() const {return mCount;};
					
                            /*! \brief Find a texture entry by label name
                                \params descr Texture label name    
                                \returns pointer to locally held vsbPlaneTextureEntry 
                                NULL if there are no matching texture entries in the list
                            */
	vsbPlaneTextureEntry *	FindByLabel(const std::string &descr);
        
                            /*! \brief Find a texture entry by texture file name
                                \params fname Texture file name 
                                \returns pointer to locally held vsbPlaneTextureEntry 
                                NULL if there are no matching texture entries in the list
                            */
	vsbPlaneTextureEntry *	FindByFilename(const std::string &fname);

                            /*! \brief Find the first texture entry in the list
                                \param  fname Texture file name
                                \returns pointer to locally held vsbPlaneTextureEntry 
                                NULL if there are no texture entries in the list
                            */
	vsbPlaneTextureEntry *	FindFirst();

                            /*! \brief Find the next texture entry
                                \returns pointer to locally held vsbPlaneTextureEntry 
                                NULL if there are no more texture entries in the list
                            */
	vsbPlaneTextureEntry *	FindNext();

                            /*! \brief Find the index of the texture entry matching the 
                                 label name
                                \param fname Texture label name
                                \returns index, or -1 if texture entry not found
                            */
    int						FindIndexByLabel(const std::string &descr) ;

                            /*! \brief Find the index of the texture entry matching the 
                                label name
                                \param fname Texture file name
                                \returns index, or -1 if texture entry not found
                            */
	int						FindIndexByFilename(const std::string &fname);	
	
							//! Clear the texture entry list	
	void					Clear();

private:

	int						mCount;	//!< Number of entries used

                            


    vsbPlaneTextureEntry *	mIndexArray[MAX_TERRAIN_TEXTURES]; //!< List containing texture entries

// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 
#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

	std::string				mPath;			//!< Path of texture index file

	std::map<std::string, vsbPlaneTextureEntry *> mListFilename;  //!< Map allowing search by texture filenames
	std::map<std::string, vsbPlaneTextureEntry *> mListLabel;     //!< Map allowing search by texture label names

	std::map<std::string, vsbPlaneTextureEntry *>::iterator mListLabelIter; //!< Iterator pointing to last found texture entry

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

};


#endif