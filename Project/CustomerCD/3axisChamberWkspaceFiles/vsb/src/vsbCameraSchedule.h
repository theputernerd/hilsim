///////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbCameraSchedule.h
    \brief 
	 


    \author T. Gouthas
*/
///////////////////////////////////////////////////////////////////////////
#ifndef _vsbCameraSchedule_H
#define _vsbCameraSchedule_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _VECTOR_
#include <vector>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef parserH
#include "parser.h"
#endif

#ifndef parser_perrorH
#include "parser_perror.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

class DLL_VSB_API vsbBaseCamera;
class DLL_VSB_API vsbCameraManager;
class DLL_VSB_API vsbCameraSchedule;
class DLL_VSB_API vsbCameraScheduleParser;

class DLL_VSB_API vsbCameraScheduleEntry
{
friend class DLL_VSB_API vsbCameraSchedule;

public:
    vsbCameraScheduleEntry();
    vsbCameraScheduleEntry(vsbCameraSchedule *camSchedule);
    vsbCameraScheduleEntry(const vsbCameraScheduleEntry& from);

    vsbCameraScheduleEntry& operator =(const vsbCameraScheduleEntry& from);
    bool operator==(const vsbCameraScheduleEntry& from);
    bool operator>(const vsbCameraScheduleEntry& from);
    bool operator<(const vsbCameraScheduleEntry& from);

    void   Clear();

    // Schedule initialisation

    void Set(long camID, double time);
	void Set(const std::string &camName, double time);
	void Set(const vsbBaseCamera *camera, double time);
    void Set(double time);

    // Schedule inquire

    double ActivationTime() const {return mActivationTime;};
    long  CamId() const { return mCameraId;};
    std::string CameraName();

    // Schedule ID

    int ScheduleId() const {return mScheduleId;};
    void ScheduleId(int id) {mScheduleId = id;};

private:
    int                     mScheduleId;  
	long                    mCameraId;
	double                  mActivationTime;	
    vsbCameraSchedule      *mpCamSchedule;
};

//////////////////////////////////////////////////

class DLL_VSB_API vsbCameraSchedule
{
	friend class DLL_VSB_API vsbCameraManager;
    friend class DLL_VSB_API vsbCameraScheduleParser;
public:

	vsbCameraSchedule(vsbCameraManager *camManager);

    void ClearSchedule();
	// return a reference to 
	vsbCameraScheduleEntry* operator[](int scheduleId); 

    vsbCameraScheduleEntry* FindById(int scheduleId); 

	void InsertSchedule(long camID, double time);
	void InsertSchedule(const std::string &camName, double time);
	void InsertSchedule(const vsbBaseCamera *camera, double time);

    void DeleteSchedule(int scheduleId);
    void DeleteAllSchedules();

    int  ScheduleCout() const;
	void VerifyCameras(bool cull=false); // search camera manager and remove missing cameras
  
	void Sort(); 

	long SelectCamera(double timeSec);
	void ActivateCamera(double timeSec);

	
    vsbCameraManager &CamManager() const {return *mpCamManager;};
    int FindFreeId();

    int  Write(FILE *f, int indent);
    int	 Parse(Parser::TLineInp *tli);
    Parser::TParserError    &ParseError() {return mParseError;};

private:
	std::vector<vsbCameraScheduleEntry> mScheduleList;
    vsbCameraManager *mpCamManager;
    Parser::TParserError	mParseError;

    
};


class DLL_VSB_API vsbCameraScheduleParser: public Parser::TParseObjectBase
{
public:
	vsbCameraScheduleParser(vsbCameraSchedule &cs);
	virtual ~vsbCameraScheduleParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
    vsbParserHelper *mpOutput;

	vsbCameraSchedule &mrCameraSchedule;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};

#endif