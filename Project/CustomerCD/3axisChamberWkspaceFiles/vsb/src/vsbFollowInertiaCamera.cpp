//////////////////////////////////////////////////////////////////
/*! 
    \file vsbFollowInertiaCamera.h
    \brief Defines a follow camera with inertia class vsbFollowInertiaCamera
    
     The class vsbFollowInertiaCamera is derrived from vsbFollowCamera, 
     as well as vsbLookAtTransform, implementing  a versatile follow 
     camera object that has inertia. 

	\sa vsbFollowInertiaCamera.cpp

    \author D. Fletcher
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include "vsbFollowInertiaCamera.h"

#include <iostream>
#include <string>
#include <map>
#include <set>

#include "ssg.h"

#include "vsbCameraManager.h"
#include "vsbBaseCamera.h"
#include "vsbFollowCamera.h"

#include "vsbEntityManager.h"
#include "vsbBaseViewEntity.h"
#include "vsbViewportContext.h"
#include "vsbModelRoot.h"

#include "parser_perror.h"

#include "tool_mathtools.h"
#include "mars_vector4.h"

using namespace std;
using namespace Parser;

void vsbFollowInertiaCamera::Setup()
{
	d_APosGain = 1;
	d_AAngleGain = 2000;
	d_AOmegaN = 2.0 * tool::constants::DEG_TO_RAD;
	d_AZeta = 1.2;

	d_integ = new Mars::TIntegrator(this);
	d_lastUpdateTime = 0.0;
}


vsbFollowInertiaCamera::vsbFollowInertiaCamera(vsbCameraManager *cm, const string &name, bool postfixed) : 
vsbFollowCamera(cm, name, postfixed)
{
	Setup();
}

const std::string & vsbFollowInertiaCamera::ClassName() const
{
    static string class_name = "vsbFollowInertiaCamera";
    return class_name;
} 
 

vsbFollowInertiaCamera::vsbFollowInertiaCamera(vsbCameraManager *cm) : 
vsbFollowCamera(cm,"vsbFollowInertiaCamera",true)
{
	Setup();
}

vsbFollowInertiaCamera::~vsbFollowInertiaCamera()
{
	delete d_integ;
}

vsbBaseCamera *vsbFollowInertiaCamera::Clone() 
{
	string name = Name();
	name+="-";
	vsbFollowInertiaCamera *newcam = new vsbFollowInertiaCamera(mpCameraManager, name, true);
	*newcam = *this;
	return newcam;
}

void vsbFollowInertiaCamera::UpdateCameraMatrix(float time)
{
	// use the inherited FollowCam to generate a demanded camera matrix
	vsbFollowCamera::UpdateCameraMatrix(time);

	// copy the CamMatrix to the demand variables
	sgVec3 cam_trans;
	sgCopyVec3(cam_trans,vsbBaseCamera::mCamMatrix[3]);
	d_camPosCmdE = Mars::TVector3(cam_trans[SGD_X], cam_trans[SGD_Y], cam_trans[SGD_Z]);

	sgQuat quat;
	sgMatrixToQuat(quat, vsbBaseCamera::mCamMatrix);
	d_camQuatCmdE = Mars::TQuaternion(quat[SGD_W], quat[SGD_X], quat[SGD_Y], quat[SGD_Z]);

	// we only want lagged behaviour if we're travelling in time, otherwise we'll
	// leave the camera matrix as-is without treating it as a demand - setting
	// the lagged-achieved to the demand so that once we do start travelling,
	// we won't have the start-up transient to contend with
	// Side-effect of this is that camera actual snaps to camera demand at end of
	// run/simulation.
	if (time == d_lastUpdateTime)
	{
		d_camPosE = d_camPosCmdE;
		d_camVelE = Mars::TVector3();
		d_camQuatE = d_camQuatCmdE;
		d_camAngVelE = Mars::TVector3();
	}
	else
	{
		using namespace Mars::VI;

		// run the integrator to model the 2nd order response
		// TIME IS ALLOWED TO GO BACKWARDS
		double dt = time - d_lastUpdateTime;
		d_integ->RungeKutta2(d_lastUpdateTime, dt);

		// bring time forward
		d_lastUpdateTime = time;

		// copy the response to the CamMatrix
		sgQuat quat;
		sgSetQuat(quat, d_camQuatE[E0], d_camQuatE[E1], d_camQuatE[E2], d_camQuatE[E3]);
		sgQuatToMatrix(vsbBaseCamera::mCamMatrix, quat);
		vsbBaseCamera::mCamMatrix[3][SGD_X] = d_camPosE[X];
		vsbBaseCamera::mCamMatrix[3][SGD_Y] = d_camPosE[Y];
		vsbBaseCamera::mCamMatrix[3][SGD_Z] = d_camPosE[Z];

		// if camera-centric transforms are on, extra manipulation is needed
		// to lag that too...
		// TBD
	}


}


void vsbFollowInertiaCamera::CalcDerivsAndDirectOutputs(const double &t)
{
	using namespace Mars::VI;

	// calculate the second-order response...

	// calculate the achieved position double derivative (ie acceleration)
	Mars::TVector3 posError = d_APosGain * (d_camPosCmdE - d_camPosE);

	Mars::TVector3 camAccE =
		d_AOmegaN*d_AOmegaN*posError-
		2.0*d_AZeta*d_AOmegaN*d_camVelE;

	d_integ->RegForIntegrate(camAccE, &d_camVelE);
	d_integ->RegForIntegrate(d_camVelE, &d_camPosE);

	// calculate the achieved quaternion double derivative (ie angular acceleration)
	Mars::TQuaternion quatError = d_camQuatCmdE*d_camQuatE.inverse();
	Mars::TVector4 eVectAndPhi = quatError.calcEVectAndPhi();
	Mars::TVector3 angError(eVectAndPhi[X], eVectAndPhi[Y], eVectAndPhi[Z]);
	angError = d_AAngleGain * eVectAndPhi[OTHER] * angError;

	Mars::TVector3 camAngAccE =
		d_AOmegaN*d_AOmegaN*angError-
		2.0*d_AZeta*d_AOmegaN*d_camAngVelE;

	d_integ->RegForIntegrate(camAngAccE, &d_camAngVelE);

	// calculate quaternion rates from angular velocity
	Mars::TQuaternion quatRatesE = 
		0.5 * Mars::TQuaternion(0.0, d_camAngVelE) * d_camQuatE;

	d_integ->RegForIntegrate(quatRatesE, &d_camQuatE);
}


void vsbFollowInertiaCamera::TransformIntegOutputs(const double &t)
{
	d_camQuatE.normalise();
}


vsbFollowInertiaCamera  &vsbFollowInertiaCamera::operator=(const vsbFollowInertiaCamera  &from)
{
	if (&from == this)
		return *this;

    vsbFollowCamera::operator=(from);

	d_APosGain = from.d_APosGain;
	d_AAngleGain = from.d_AAngleGain;
	d_AOmegaN = from.d_AOmegaN;
	d_AZeta = from.d_AZeta;
	d_camPosE = from.d_camPosE;
	d_camVelE = from.d_camVelE;
	d_camQuatE = from.d_camQuatE;
	d_camAngVelE = from.d_camAngVelE;
	d_lastUpdateTime = from.d_lastUpdateTime;

	return *this;
}

int  vsbFollowInertiaCamera::Write(FILE *f, int indent)
{
    vsbFollowInertiaCameraParser cam_parser(*this);
    return cam_parser.Write(f,indent);
}

int	 vsbFollowInertiaCamera::Parse(Parser::TLineInp *tli)
{
	mParseError.Clear();
	vsbFollowInertiaCameraParser cam_parser(*this);
	int result = cam_parser.Parse(tli, true );
	if (result)
	{
		mParseError.Set(*cam_parser.ItsLastError());
	}
	return result;
}

////////////////////////////////////////////////////////////

char *vsbFollowInertiaCameraParser::mpKeyWords[] =
{
	"CAMERA",
	"ATTRIBUTES",
	"POSGAIN",
	"ANGLEGAIN",
	"OMEGAN",
    "ZETA",

	0
};

enum {
	VSB_FICAM_CAMERA,
	VSB_FICAM_ATTRIBUTES,
	VSB_FICAM_POSGAIN,
	VSB_FICAM_ANGLEGAIN,
	VSB_FICAM_OMEGAN,
    VSB_FICAM_ZETA,

};


char *vsbFollowInertiaCameraParser::mpErrorStrings[] =
{
	"",
	0
};

enum {
	VSB_FICAM_ERR_UNUSED,
};

vsbFollowInertiaCameraParser::vsbFollowInertiaCameraParser(vsbFollowInertiaCamera &fc):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrFollowInertiaCamera(fc) 
{
      mpOutput = new vsbParserHelper(*this);
}

vsbFollowInertiaCameraParser::~vsbFollowInertiaCameraParser()
{
      delete mpOutput;
}

int	 vsbFollowInertiaCameraParser::Parse(TLineInp *tli, bool skipHeader)
{
	mSkipHeader = skipHeader;
	return ParseSource(tli, true);
}

int vsbFollowInertiaCameraParser::Write(FILE *file, int indent)
{
	mpOutput->SetFile(file);
	if (file==0) return 0;

	mpOutput->WriteSpaces(indent);
    mpOutput->WriteKeyWord(VSB_FICAM_CAMERA);
	mpOutput->WriteKeyWord(T_LBRACK);
	mpOutput->WriteName(mrFollowInertiaCamera.ClassName());
	mpOutput->WriteKeyWord(T_RBRACK);

    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;

    vsbFollowCameraParser follow_camera_parser(mrFollowInertiaCamera);
    follow_camera_parser.Write(file, indent);

    mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_FICAM_ATTRIBUTES);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_LBRACE);
    indent+=3;

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FICAM_POSGAIN);
	mpOutput->WriteSpaces(4);
    mpOutput->WriteValue(mrFollowInertiaCamera.d_APosGain);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FICAM_ANGLEGAIN);
	mpOutput->WriteSpaces(4);
    mpOutput->WriteValue(mrFollowInertiaCamera.d_AAngleGain);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FICAM_OMEGAN);
	mpOutput->WriteSpaces(4);
    mpOutput->WriteValue(mrFollowInertiaCamera.d_AOmegaN);

    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(VSB_FICAM_ZETA);
	mpOutput->WriteSpaces(4);
    mpOutput->WriteValue(mrFollowInertiaCamera.d_AZeta);

    indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);

    indent-=3;
    mpOutput->WriteNewline(indent);
    mpOutput->WriteKeyWord(T_RBRACE);
    return 1;

}

void vsbFollowInertiaCameraParser::ParseFunc()
{
	vsbFollowCameraParser follow_camera_parser(mrFollowInertiaCamera);

	char class_name[100];

	if (!mSkipHeader)
	{
		GetToken(VSB_FICAM_CAMERA);
		GetToken(T_LBRACK);
		GetString(class_name);
		GetToken(T_RBRACK);
		GetToken(T_LBRACE);
	}

	int status = follow_camera_parser.Parse(ParserLineInp());
	if (status)
	{
		AbortParse(*follow_camera_parser.ItsLastError());
	}

	GetToken(VSB_FICAM_ATTRIBUTES);
    GetToken(T_LBRACE);

	GetToken(VSB_FICAM_POSGAIN);
	GetToken(T_EQ);
	GetToken(T_REAL);
	mrFollowInertiaCamera.d_APosGain = GetCurrToken().value.rval;

	GetToken(VSB_FICAM_ANGLEGAIN);
	GetToken(T_EQ);
	GetToken(T_REAL);
	mrFollowInertiaCamera.d_AAngleGain = GetCurrToken().value.rval;

	GetToken(VSB_FICAM_OMEGAN);
	GetToken(T_EQ);
	GetToken(T_REAL);
	mrFollowInertiaCamera.d_AOmegaN = GetCurrToken().value.rval;

	GetToken(VSB_FICAM_ZETA);
	GetToken(T_EQ);
	GetToken(T_REAL);
	mrFollowInertiaCamera.d_AZeta = GetCurrToken().value.rval;

	GetToken(T_RBRACE);

	GetToken(T_RBRACE);
}	

