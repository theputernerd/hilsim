#ifndef vsbCylinder_H
#define vsbCylinder_H
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbHemisphere.h


  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#include <GL\gl.h>
#include <GL\glu.h>

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef vsbShapeDList_H
#include "vsbShapeDList.h"
#endif


class DLL_VSB_API vsbCylinder: public vsbCompiledGLShape
{
public:
	vsbCylinder();
	virtual ~vsbCylinder();
	virtual void Create();
	virtual void GetBSphere(sgSphere &bsphere, float scale, float length);
	virtual void Scale(float size, float length) const;
	
protected:

	
};




#endif