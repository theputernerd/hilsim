#ifndef vsbShapeDList_H
#define vsbShapeDList_H
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbShapeDList.h


  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif


class DLL_VSB_API vsbCompiledGLShape
{
public:
	vsbCompiledGLShape();
	virtual ~vsbCompiledGLShape();
	virtual void Create();
	void   Draw(float size = 1.0f, float length = 2.0f) const;
	bool   IsCreated() const {return Id()!=0;}
	int    Id() const {return m_Id;} 
	virtual void Scale(float size, float length) const = 0;
	// For a given scale factor, get object bounding sphere
	virtual void  GetBSphere(sgSphere &bsphere, float scale, float length) = 0;
	
protected:
	void   Free();	
	int    m_Id;
};


class DLL_VSB_API vsbCompiledGLShapeList
{
public:

	vsbCompiledGLShapeList();
	virtual ~vsbCompiledGLShapeList();

	static void RegisterShape(std::string shapeName, vsbCompiledGLShape *shapeDef);
	static void ClearAllShapes();

	int Id(std::string shapeName);
    void Draw(std::string shapeName, float scale = 1.0f,  float length = 1.0f);
	bool   IsCreated(std::string shapeName);
	bool   GetBSphere(sgSphere &bsphere, std::string shapeName, float scale, float length) ;
protected: 
	static std::map<std::string,vsbCompiledGLShape *> m_GLShapeList;
};		

#define REGISTER_COMPILED_GL_SHAPE(shape)\
	vsbCompiledGLShapeList::RegisterShape(#shape, (vsbCompiledGLShape *) new shape);
   	
#define CLEAR_COMPILED_GL_SHAPES()\
	vsbCompiledGLShapeList::ClearAllShapes();



//////////////

#define _VSB_TYPE_COMPILEDSHAPE         0x80000000
inline int vsbTypeCompiledShape  () { return _VSB_TYPE_COMPILEDSHAPE  | ssgTypeLeaf () ; }

class DLL_VSB_API vsbCompiledShape : public ssgLeaf
{
	std::string mShapeName;	

	float mScale;  // radius
	float mLength; // length/height
	bool  mWireFrame;

	ssgSimpleState *mState;
	sgVec4 mColour;

protected:
  virtual void draw_geometry () ;
  virtual void copy_from ( vsbCompiledShape *src, int clone_flags ) ;
public:

  virtual ssgBase *clone ( int clone_flags = 0 ) ;
  vsbCompiledShape (void) ;
  virtual ~vsbCompiledShape (void) ;

  float Scale() { return mScale; }
  void  Scale(float scale) {mScale = scale; dirtyBSphere (); }; 
  float LengthScale() { return mLength; }
  void  LengthScale(float len) {mLength = len; dirtyBSphere (); }; 

  void  SetColour(float r, float g, float b, float alpha = 0);
  void  SetColour(sgVec4 colour);
  void  SetAlpha(float alpha);

  void  ShapeName(std::string shapeName) {mShapeName = shapeName;dirtyBSphere (); };


  virtual void drawHighlight ( sgVec4 colour ) ;
  virtual void drawHighlight ( sgVec4 colour, int i )  ;
  virtual void pick ( int baseName ) ;


  virtual void setPrimitiveType ( GLenum ty ) { gltype = ty ; }
  virtual GLenum getPrimitiveType () { return gltype ; }

  virtual int getNumVertices  () { return 0 ; }
  virtual int getNumNormals   () { return 0 ; }
  virtual int getNumColours   () { return 0 ; }
  virtual int getNumTexCoords () { return 0 ; }

  virtual float *getVertex   ( int i ) {return 0;};
  virtual float *getNormal   ( int i ) {return 0;} ;
  virtual float *getColour   ( int i ) {return 0;} ;
  virtual float *getTexCoord ( int i ) {return 0;} ;

  virtual int  getNumTriangles () {return 0;} ;
  virtual void getTriangle ( int n, short *v1, short *v2, short *v3 ) {} ;
  virtual int  getNumLines () {return 0;} ;
  virtual void getLine ( int n, short *v1, short *v2 ) {} ;

  virtual void transform ( const sgMat4 m )  ;

  virtual void recalcBSphere () ;

  virtual const char *getTypeName(void) ;
  virtual void print ( FILE *fd = stderr, char *indent = "", int how_much = 2 ) ;
  virtual int load ( FILE *fd ) ;
  virtual int save ( FILE *fd ) ;

  virtual void isect_triangles ( sgSphere *s, sgMat4 m, int test_needed )  ;
  virtual void hot_triangles   ( sgVec3    s, sgMat4 m, int test_needed )  ;
  virtual void los_triangles   ( sgVec3    s, sgMat4 m, int test_needed );

  virtual void draw  ()  ;
};

#endif