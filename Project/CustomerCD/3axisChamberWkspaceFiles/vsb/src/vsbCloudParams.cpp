//////////////////////////////////////////////////////////////////
/*! 
    \file vsbCloudParams.cpp
    \brief Implements the class vsbCloudParams
 
	This file implements the vsbCloudParams class which encapsulates all the 
    information necessary for the construction and manipulation of cloud 
    layers, however it does not define any cloud implementation methods.
 	
	\sa vsbCloudParams.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#include "vsbCloudParams.h"

using namespace std;
using namespace Parser;


char *vsbCloudParamsParser::mpKeyWords[] =
{
	"CLOUD",
	"ALTITUDE",
	"LENGTH",
	"SCALE",
	"THICKNESS",
	"TRANSITION",
	"TEXTURE_INDEX",
	0
};

enum {
	VSB_CP_CLOUD,
	VSB_CP_ALTITUDE,
	VSB_CP_LENGTH,
	VSB_CP_SCALE,
	VSB_CP_THICKNESS,
	VSB_CP_TRANSITION,
	VSB_CP_TEXTURE_INDEX
};

char *vsbCloudParamsParser::mpErrorStrings[] =
{
	"",
	0
};

vsbCloudParamsParser::vsbCloudParamsParser(vsbCloudParams &cp):
  TParseObjectBase(mpKeyWords, mpErrorStrings),
  mrCloudParams(cp)
{
  	  mpOutput = new vsbParserHelper(*this);
}

vsbCloudParamsParser::~vsbCloudParamsParser()
{
	delete mpOutput;
}

int	 vsbCloudParamsParser::Parse(TLineInp *tli)
{
	return ParseSource(tli, true);
}

int  vsbCloudParamsParser::Write(FILE *f, int indent)
{
	mpOutput->SetFile(f);
	if (f==0) return 0;

	vsbCloudParams &working_cloud_params = mrCloudParams;

	mpOutput->WriteSpaces(indent);
	mpOutput->WriteKeyWord(VSB_CP_CLOUD);
	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(T_LBRACE);
	indent+=3;

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_TEXTURE_INDEX);
	mpOutput->WriteValue((int) working_cloud_params.CloudType());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_ALTITUDE);
	mpOutput->WriteValue((int) working_cloud_params.Alt());

	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_SCALE);
	mpOutput->WriteValue((int) working_cloud_params.Scale());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_LENGTH);
	mpOutput->WriteValue((int) working_cloud_params.Length());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_THICKNESS);
	mpOutput->WriteValue((int) working_cloud_params.Thickness());


	mpOutput->WriteNewline(indent);
	mpOutput->WriteKeyWord(VSB_CP_TRANSITION);
	mpOutput->WriteValue((int) working_cloud_params.Transition());


	indent-=3;
	mpOutput->WriteNewline(indent); 
	mpOutput->WriteKeyWord(T_RBRACE);


	return 1;
}

void vsbCloudParamsParser::ParseFunc()
{
	vsbCloudParams working_cloud_params;
	GetToken(VSB_CP_CLOUD);
	GetToken(T_LBRACE);

	GetToken(VSB_CP_TEXTURE_INDEX);
	GetToken(T_EQ);
	working_cloud_params.CloudType((CloudTypeEnum)GetIntValue());

	GetToken(VSB_CP_ALTITUDE);
	GetToken(T_EQ);
	working_cloud_params.Alt(GetNumericValue());

	GetToken(VSB_CP_SCALE);
	GetToken(T_EQ);
	working_cloud_params.Scale(GetNumericValue());

	GetToken(VSB_CP_LENGTH);
	GetToken(T_EQ);
	working_cloud_params.Length(GetNumericValue());

	GetToken(VSB_CP_THICKNESS);
	GetToken(T_EQ);
	working_cloud_params.Thickness(GetNumericValue());

	GetToken(VSB_CP_TRANSITION);
	GetToken(T_EQ);
	working_cloud_params.Transition(GetNumericValue());

	GetToken(T_RBRACE);

	mrCloudParams = working_cloud_params;

}

//////////////////////////////////////////////////////////////

#define DEFLT_CLOUD_SCALE_FACTOR 8000.0f //!< The default scale of a cloud texture map in metres

vsbCloudParams::vsbCloudParams()
{
	SetDefault();
};

void vsbCloudParams::SetDefault()
{
	mAlt = 2600.0f;
	mThickness = 200;
	mTransition = 50;
	mLength = 80000;
	mCloudType = CLOUD_MOSTLY_CLOUDY;
	mScale = DEFLT_CLOUD_SCALE_FACTOR;
	mModified = true;
    mNeedRebuild = true;
};

vsbCloudParams::vsbCloudParams(vsbCloudParams &cp)
{
	*this = cp;
};

vsbCloudParams &vsbCloudParams::operator = (const vsbCloudParams& cp)
{
	Alt(cp.Alt());
	Thickness(cp.Thickness());
	Transition(cp.Transition());
	Length(cp.Length());
	Scale(cp.Scale());
	CloudType(cp.CloudType());
	//mModified = true;
    //mNeedRebuild = true;
	return *this;
}

bool  vsbCloudParams::operator == (const vsbCloudParams& cp) const
{
	return	mAlt == cp.Alt() &&
			mThickness == cp.Thickness() &&
			mTransition == cp.Transition() &&
			mLength == cp.Length() &&
			mScale == cp.Scale() &&
			mCloudType == cp.CloudType();
}

void vsbCloudParams::Set(CloudTypeEnum cloudType, double alt, double thickness, double transition)
{
	mAlt = (float) alt;
	mThickness = (float) thickness;
	mTransition = (float) transition;
    if (mCloudType != cloudType) 
        mNeedRebuild = true;
	mCloudType = cloudType;
	mModified = true;
 }
