#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbShapeDlist.cpp
    \brief Implements compiled OpenGL shapes

  \author T. Gouthas
*/
/////////////////////////////////////////////////////////////////

#include <string>
#include <map>

#include <assert.h>
#include <limits.h>

using namespace std;

#include "ssg.h"
#include "vsbShapeDList.h"
#include "vsbViewPortContext.h"

vsbCompiledGLShape::vsbCompiledGLShape()
{
	m_Id = 0;
}

vsbCompiledGLShape::~vsbCompiledGLShape()
{
	Free();
}

void vsbCompiledGLShape::Create()
{
	if (IsCreated()) return;
	m_Id=glGenLists(1);
	
//		char s[30];
//	sprintf(s,"%d",m_Id);
//	MessageBox(0,s,s,MB_APPLMODAL | MB_OK);
}

void vsbCompiledGLShape::Draw(float scale, float length) const
{	
	if ( !IsCreated())
		 return;
	if (glIsList(m_Id))
	{
		glDepthMask(false);
	
		if (!vsbViewportContext::RenderOpts().GLNormalizeOn())
			glEnable(GL_NORMALIZE);
		glPushMatrix();
		Scale(scale, length);
		glCallList(m_Id);
		glPopMatrix();
		if (!vsbViewportContext::RenderOpts().GLNormalizeOn())
			glDisable(GL_NORMALIZE);


		glDepthMask(true);


/*
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();


		
		glDisable      ( GL_LIGHTING   ) ;
		glDisable      ( GL_TEXTURE_2D ) ;
		glDisable      ( GL_CULL_FACE  ) ;
		glDisable      ( GL_DEPTH_TEST );
		glDisable      ( GL_LIGHTING   );
		glDisable      ( GL_FOG        );
		glEnable       ( GL_BLEND      );
		glBlendFunc    (GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		glAlphaFunc(GL_GEQUAL, 0.0625);
		glEnable(GL_ALPHA_TEST);
		glLineWidth(5.0);
		glColor4f(1,0,0,1);
		
		glBegin(GL_LINES );
		glVertex3f(-2, 0, 0);
		glVertex3f(2, 0, 0);
		glEnd();
		glBegin(GL_LINES );
		glVertex3f(0, -2, 0);
		glVertex3f(0, 2, 0);
		glEnd();
		glBegin(GL_LINES );
		glVertex3f(-2,0, -2);
		glVertex3f(2,0, -2);
		glEnd();
		
		glPopMatrix();
*/
		
	}
}



void vsbCompiledGLShape::Free()
{
	if (!IsCreated()) return;
	if (glIsList(m_Id))
		glDeleteLists(m_Id,1);
	m_Id = 0;
}


//////////////////////////////////////////////////////

map<string, vsbCompiledGLShape *> vsbCompiledGLShapeList::m_GLShapeList;

vsbCompiledGLShapeList::vsbCompiledGLShapeList()
{
}

vsbCompiledGLShapeList::~vsbCompiledGLShapeList()
{
}

void vsbCompiledGLShapeList::ClearAllShapes()
{
	map<string,vsbCompiledGLShape *>::iterator iter;
	for (iter = m_GLShapeList.begin(); iter!= m_GLShapeList.end(); iter++)
	{
		delete (iter->second);
	}
	m_GLShapeList.clear();

}

int vsbCompiledGLShapeList::Id(std::string shapeName) 
{
	
	map<string,vsbCompiledGLShape *>::iterator iter;
	iter = m_GLShapeList.find(shapeName);
	if (iter == m_GLShapeList.end()) 
		return 0;

	return 
		iter->second->Id();
}

void vsbCompiledGLShapeList::RegisterShape(std::string shapeName, vsbCompiledGLShape *shapeDef)
{
	map<string,vsbCompiledGLShape *>::iterator iter;
	iter = m_GLShapeList.find(shapeName);
	assert(iter == m_GLShapeList.end() /*must be unique shape*/); 
	m_GLShapeList.insert(pair<string,vsbCompiledGLShape *>(shapeName, shapeDef));
	shapeDef->Create();
}

void vsbCompiledGLShapeList::Draw(std::string shapeName, float scale, float length) 
{
	std::map<std::string,vsbCompiledGLShape *>::iterator iter;
	iter = m_GLShapeList.find(shapeName);
	if (iter == m_GLShapeList.end())  
		return;
	iter->second->Draw(scale, length);
}



bool vsbCompiledGLShapeList::IsCreated(std::string shapeName) 
{
	std::map<std::string,vsbCompiledGLShape *>::iterator iter;
	iter = m_GLShapeList.find(shapeName);
	if (iter == m_GLShapeList.end())  
		return false;
	return iter->second->IsCreated();
}



bool vsbCompiledGLShapeList::GetBSphere(sgSphere &bsphere, std::string shapeName, 
										float scale, float length) 
{
	std::map<std::string,vsbCompiledGLShape *>::iterator iter;
	iter = m_GLShapeList.find(shapeName);
	if (iter == m_GLShapeList.end())  
		return false;

	iter->second->GetBSphere(bsphere,  scale, length);
	return true;
}
//////////////////////////////////////////////////////////////////////////

vsbCompiledShape::vsbCompiledShape (void)
{
	mShapeName = "";
	mLength = mScale = 1.0f;
	type = ssgTypeVtxTable () ;
	gltype = GL_POINTS ;
	sgSetVec4(mColour,0.9f, 0.0f, 0.0f, 0.5f);
	mWireFrame = false;

	mState = new ssgSimpleState;

	mState->setShadeModel( GL_SMOOTH );
	mState->enable( GL_LIGHTING ) ;
	mState->disable ( GL_COLOR_MATERIAL ) ;
    mState->disable( GL_TEXTURE_2D );
	mState->enable ( GL_CULL_FACE) ;
  
	
	mState->setShininess  (15);

    mState->enable( GL_BLEND );
    mState->enable( GL_ALPHA_TEST );
	mState->setAlphaClamp( 0.01f );
	setState (mState);
	
	

	
}

vsbCompiledShape::~vsbCompiledShape (void)
{
}

void vsbCompiledShape::draw_geometry () 
{
	vsbViewportContext::CompiledGLShapeList().Draw(mShapeName, mScale, mLength);
}

void vsbCompiledShape::copy_from ( vsbCompiledShape *src, int clone_flags ) 
{
  ssgLeaf::copy_from ( src, clone_flags ) ;
  gltype = src -> getPrimitiveType () ;
  mShapeName = src->mShapeName;
  mScale = src->mScale;
  mLength = src->mLength;
  mWireFrame = src->mWireFrame;
  recalcBSphere () ;
}

ssgBase *vsbCompiledShape::clone ( int clone_flags )
{
  vsbCompiledShape *b = new vsbCompiledShape ;
  b -> copy_from ( this, clone_flags ) ;
  return b ;
}

void vsbCompiledShape::drawHighlight ( sgVec4 colour )
{
	// Not implemented for this type
}

void vsbCompiledShape::drawHighlight ( sgVec4 colour, int i )
{
	// Not implemented for this type
}

void vsbCompiledShape::pick ( int baseName ) 
{
	// Not implemented for this type
}

void vsbCompiledShape::transform ( const sgMat4 m )  
{
	// Not implemented for this type
}


void vsbCompiledShape::recalcBSphere ()
{
	vsbViewportContext::CompiledGLShapeList().GetBSphere(bsphere, mShapeName, mScale, mLength);
}

const char *vsbCompiledShape::getTypeName(void) 
{
	return "vsbCompiledShape";
}

void vsbCompiledShape::print ( FILE *fd , char *indent, int how_much  ) 
{
	// Not implemented for this type
}

int vsbCompiledShape::load ( FILE *fd ) 
{
	// Not implemented for this type
	return 0;
}

int vsbCompiledShape::save ( FILE *fd ) 
{
	// Not implemented for this type
	return 0;
}



void vsbCompiledShape::isect_triangles ( sgSphere *s, sgMat4 m, int test_needed ) 
{
	// Not implemented for this type
}

void vsbCompiledShape::hot_triangles   ( sgVec3    s, sgMat4 m, int test_needed ) 
{
	// Not implemented for this type
}

void vsbCompiledShape::los_triangles   ( sgVec3    s, sgMat4 m, int test_needed ) 
{
	// Not implemented for this type
}

void vsbCompiledShape::draw  () 
{
	
	if ( ! preDraw () )
		return ;
	
	if ( hasState () ) 
	{
		ssgSimpleState *state = (ssgSimpleState *)getState ();
		const float* env_diffuse = vsbViewportContext::EnvironmentManager().LightingParams().Diffuse();

		state->setMaterial(GL_AMBIENT, mColour[0]/2, mColour[1]/2, mColour[2]/2, mColour[3]);
		state->setMaterial(GL_DIFFUSE, mColour[0], mColour[1], mColour[2], mColour[3]);
		state ->setMaterial( GL_SPECULAR, env_diffuse[0], env_diffuse[1], env_diffuse[2], mColour[3] );	
 		if(mColour[3]<0.99)
			state->setTranslucent();
		else 
			state->setOpaque();

		state -> apply () ;
		if (mWireFrame) 
		{
			glColor3f(mColour[0], mColour[1], mColour[2]);
			glDisable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
		}
	}

	
	draw_geometry () ;
	if (mWireFrame) 
	{
			glEnable(GL_LIGHTING);
			glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	}
	
	if ( postDrawCB != NULL ) (*postDrawCB)(this) ;
}	


void vsbCompiledShape::SetColour(float r, float g, float b, float alpha)
{
	mColour[0];
}

void vsbCompiledShape::SetColour(sgVec4 colour)
{
}

void vsbCompiledShape::SetAlpha(float alpha)
{
}