//////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseCamera.h
    \brief Defines the class vsbBaseCamera
    
     The class vsbBaseCamera is the pure abstract base class for all 
	 cameras, providing the basic funtionality to be managed by the 
	 camera manager.
	
	\sa vsbBaseCamera.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
#ifndef _vsbBaseCamera_H
#define _vsbBaseCamera_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef parserH
#include "parser.h"
#endif

#ifndef parser_perrorH
#include "parser_perror.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif


class DLL_VSB_API vsbCameraManager;
class DLL_VSB_API vsbViewportContext;
class DLL_VSB_API vsbBaseCameraParser;

//////////////////////////////////////////////////////////////////
/*! 
	\class vsbBaseCamera
	\brief Pure abstract base class for all cameras, providing the basic 
    funtionality to be managed by the camera manager.

  The key abstract member function is UpdateCameraMatrix() which 
  updates the cameras position orientation and field of view at a
  particular time, and captures its geometry in the mCamMatrix matrix
  which is subsequent accessed by the camera manager via the CamMatrix()
  member function.

  Prior to calling UpdateMatrix() the camera manager passes the local
  relative matrix, which may be used by UpdateCameraMatrix() to modify the 
  final mCamMatrix; the relative matrix is a matrix that represents
  some external translations and/or rotations. If mRelativeGeometryEnabled
  is false, it is expected that the relative matrix is ignored in the
  derrived UpdateCameraMatrix() functions. This would be applied to strict 
  views for example missile seeker views which MUST look at the scene as 
  prescribed by its computational model, and not allowed to be modified by 
  the user interactively in the GUI.

  The one source pf relative camera matrices is the vsbArcBall which converts
  mouse clicks and drags as 3D manipulation instructions centering around the
  concept of a virtual ball at the centre of the viewport.
 
*/
//////////////////////////////////////////////////////////////////

enum vsbHudMode
{
	HUD_FIXED,
	HUD_GIMBALLED,
	HUD_COCKPIT,
};

class DLL_VSB_API vsbBaseCamera
{

friend class DLL_VSB_API vsbCameraManager;
friend class DLL_VSB_API vsbViewportContext;
friend class DLL_VSB_API vsbBaseCameraParser;

public:
                    /*! \brief Constructor

                        Initialises the camera with its parent camera manager, and a name for the camera
                        \param cm A pointer to a valid parent camera manager
                        \param name A name for the camera
                        \param postfix A flag to indicate whether the name should be postfixed by a 
                        unique camera number
                    */
                    vsbBaseCamera(vsbCameraManager *cm, const std::string &name, bool postfixed = false);

                    /*! \brief Virtual destructor
                    */
	virtual			~vsbBaseCamera();

	vsbBaseCamera  &operator=(const vsbBaseCamera  &from); 

	virtual vsbBaseCamera *Clone() = 0;

                    /*! \brief Get a reference to the name of the camera 
                    */
    const std::string &  Name() const						{ return mName; };

	                   /*! \brief Rename of the camera 
                    */
    bool  Rename(const std::string &newname);

	                 /*! \brief Get a reference to the class name of the camera 
                    */
    virtual const std::string & ClassName() const; // { return mClassName; };
  
                
                    /*! \brief Get the camera's unique handle id 
                    */
	long			Handle() const						{ return mHandle;};

                    /*! \brief Get the final composite camera matrix

					Gets a reference to the camera instance's mCamMatrix

					\sa mCamMatrix
                    */
	sgdMat4 &		CameraMatrix() { return mCamMatrix; };
	const sgdMat4 &	CameraMatrix() const { return mCamMatrix; };

                     /*! \brief Get the final computed camera matrix

					Gets a reference to the camera instance's mComputedMatrix

					\sa mComputedMatrix
                    */
	sgdMat4 &		ComputedMatrix() { return mComputedMatrix; };
	const sgdMat4 &	ComputedMatrix() const { return mComputedMatrix; };

                    /*! \brief Get the relative matrix

  
					Gets a reference to the camera instance's mRelativeGeometryMatrix

					\sa mRelativeGeometryMatrix, vsbArcBall
                    */
	sgdMat4 &		RelativeMatrix()					{ return mRelativeGeometryMatrix; };

                    /*! \brief Set the relative matrix
					\sa mRelativeGeometryMatrix
                    */
	void			RelativeMatrix(const sgdMat4 mat); 

                    /*! \brief Set the computed matrix
					\sa mComputedMatrix
                    */
	void			ComputedMatrix(const sgdMat4 mat); 

                    /*! \brief Set the computed matrix
					\sa mComputedMatrix
                    */
	void			ComputedMatrix(const sgdCoord &coord); 

                    /*! \brief Set the relative matrix
					\sa mRelativeGeometryMatrix
                    */
	void			RelativeMatrix(const sgdCoord &coord); 

                    /*! \brief Set the composite camera matrix
					\sa mCamMatrix
                    */
	void			CameraMatrix(const sgdMat4 mat); 

                    /*! \brief Set the composite camera matrix
					\sa mCamMatrix
                    */
	void			CameraMatrix(const sgdCoord &coord); 

                    /*! \brief Reports whether relative geometry is enabled
                       
                        Ie whether the relative camera matrix is premultiplied to the 
                        computed matrix, ie, it generally can be used enable or disable 
                        interactive user's view-point adjustments.
						
						The relative matrix is what is updated by the vsbArcBall manipulator
						via the gui. If the view is not allowed to be modified by the users
						GUI interactions, mRelativeGeometryEnabled should be made false.
						
                    */
	bool			RelativeGeom()						{ return mRelativeGeometryEnabled; };


					/*! \brief Enable or disable relative camera geometry

					Controls whether the relative camera matrix is premultiplied to the computed matrix,
					ie, it generally can be used enable or disable interactive user view-point adjustments
					*/

	void			RelativeGeom(bool trueFalse)	{ mRelativeGeometryEnabled = trueFalse; };

                    /*! \brief Set the camera's field of view in degrees

					The field of view applies both to the vertical and horizontal
					planes.

                    */
	void			FOV(float f)		{mFOV = f;};

                    /*! \brief Sets the near clip plane

					The near clip range affects the depth buffer resolution
					close to the far clip plane quite dramatically (since the 
					Z buffer is exponential in that more bits are allocated 
					for near resolution as opposed to far) so one must endeavour
					to set the near clip plane as far from the eye viewpoint as 
					possible.. 

                    */
	void			NearClip(float n)	{mNearClip = n;};

	                    /*! \brief Set the camera's far clip plane
                    */
	void			FarClip(float f)	{mFarClip = f;}; // Dont want changed casually
	
                    /*! \brief Get the cameras field of view
                    */
	float			FOV()				{return mFOV;};
            
                    /*! \brief Get the camera's near clip plane
                    */
	float			NearClip()			{return mNearClip;};

                    /*! \brief Get the camera's far clip plane
                    */
	float			FarClip()			{return mFarClip;};

	virtual int     Write(FILE *f, int indent) = 0;

	virtual int		Parse(Parser::TLineInp *tli) = 0;
	Parser::TParserError    &ParseError() {return mParseError;};

	vsbCameraManager * CameraManager() {return mpCameraManager;};

    void RenameCamera(const std::string &name);

    				/*! Internal function for determining an ID for a given camera*/
    static long		HashValue(const std::string &name) ;

	static sgdVec3  &HeadOrientation(){return mHeadOrientation;}
	static sgdVec3  &CameraToOriginDelta(){return mCameraToOriginDelta;}


	vsbHudMode		HudMode() const { return mHudMode; }; 
	void			HudMode(vsbHudMode hm) {mHudMode = hm; };

	bool			HeadTrackingEnabled(){return mHeadTrackingEnabled;};
	void			HeadTrackingEnabled(bool onOff){mHeadTrackingEnabled = onOff;};


protected:




	long			mHandle;					//!< Camera handle
    std::string		mName;						//!< Camera name
	vsbCameraManager *mpCameraManager;		    //!< Pointer to camera manager
	sgdMat4			mCamMatrix;					//!< The final camera matrix used by OpenGL
	sgdMat4			mComputedMatrix;			//!< The matrix provided by the "Update" function
	sgdMat4			mRelativeGeometryMatrix;	//!< The local relative matrix that is premultiplied to the computed matrix.. 

	float			mFOV;                       //!< The field of view
    float           mNearClip;                  //!< The near clip plane
    float           mFarClip;                   //!< The far clip plane

	bool			mRelativeGeometryEnabled;		/*!< determines if the relative geometry
													     matrix is applied to the Computed matrix
                                                    */

	bool			mHeadTrackingEnabled;

	static sgdVec3	mHeadOrientation;
	static sgdVec3	mCameraToOriginDelta;

	vsbHudMode      mHudMode;
	

	Parser::TParserError	mParseError;

	/*! \brief Update camera matrix 
    
    This function generates the computed matrix, followed by the camera matrix based
	on the computed and relative geometry matrix, for the specified time
    \param time Time stampe for the next frame update
    */
	virtual void	UpdateCameraMatrix(float time) {;}
	
private: 

                    /*! 
                        Initialise support function for constructors
                    */
    void			ConstructorSetup(const std::string &name);



};


class DLL_VSB_API vsbBaseCameraParser: public Parser::TParseObjectBase
{
public:
	vsbBaseCameraParser(vsbBaseCamera &bc);
	virtual ~vsbBaseCameraParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
    vsbParserHelper *mpOutput;

	vsbBaseCamera &mrBaseCamera;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
	void ParseOnOffState(bool &onOrOffVar, int id);
	void WriteOnOff(bool onOrOff);
};

#endif

