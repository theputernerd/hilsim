#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#include <string>
using namespace std;
#include "ssg.h"

#include "vsbViewportContext.h"
#include "vsbSysPath.h"
#include "vsbExplosion.h"
#include "vsbUtilities.h"
#include "vsbExplosionFXDirector.h"


static void vsbSetExplosionScale(float size, ssgVertexArray *vertices)
{
	int i, j;
	static float unit_cutout[4][3] =
	{	{-1, 0, -1},
		{1, 0, -1},
		{-1, 0, 1},
		{1, 0, 1}
	};

	if (!vertices) return;

	for(i=0; i<4; i++)
	{
		float *v = vertices->get( i );
		for (j=0; j<3; j++)
			v[j] = unit_cutout[i][j]*size;
	}
}


static int explosionPreDraw(ssgEntity *)
{
  glDepthMask(false);
  return 1;
}

static int explosionPostDraw(ssgEntity *)
{
   glDepthMask(true);
   return 1;
}


int vsbExplosion::msIdCounter = 0;

float vsbExplosion::msTextureUnitCoord[4][2] =
{	{0,  0},
	{1,  0},
	{0, 1},
	{1,  1}
};


void vsbExplosion::CreateStructure()
{

	int i;

	mpLeafGeometrySets = 0;
	mpTextureCoordSets = 0;
	mpState = 0;
	mCount = 0;
	mSqrtCount = 0;
	mListSize = 0;
    mSize = 0;
	sgdSetVec3(mPos,0,0,0);
	mId = msIdCounter++;
	mRunning = false;

	mpCutout = new ssgCutout(TRUE);

	sgVec3 vertex;
	sgVec4 colour;

	// Set up vertex colour array - shared by all leafs

	mpColourArray = new ssgColourArray(4);
	mpColourArray->ref();
    sgSetVec4( colour, 1.0f, 1.0f, 1.0f, 1.0f );
	for (i=0; i<4; i++)
		mpColourArray->add( colour );

	// Set up vertices - shared by all leafs

	mpVertices = new ssgVertexArray(4);
	mpVertices->ref();
	for (i=0; i<4; i++)	mpVertices->add(vertex);
	vsbSetExplosionScale(1, mpVertices); 

	//mpTimedSelector = new ssgTimedSelector(256);
	mpSelector = new ssgSelector(256);

	//mpTimedSelector->setMode(SSG_ANIM_ONESHOT);
	

	//mpCutout->addKid(mpTimedSelector);
	mpCutout->addKid(mpSelector);
	this->addKid(mpCutout);

}


bool vsbExplosion::SetType(const std::string &type_name)
{
	return SetType(type_name.c_str()); 
};

bool vsbExplosion::SetType(const char * type_name) 
{
	return vsbViewportContext::ExplosionFXDirector().Update(type_name, this);

};
	

void vsbExplosion::Initialise(const std::string &explTypeName, int sqrt_count, float scale, float duration)
{
    ssgSimpleState *expl_state = vsbViewportContext::ExplosionFXDirector().GetState(explTypeName);
	if (!expl_state) return ;
	Initialise(expl_state, sqrt_count, scale, duration);
	mTypeName = explTypeName;
}


void vsbExplosion::Initialise(ssgSimpleState *expl_state, int sqrt_count, float scale, float duration)
{
	int i, j;

    // If explosion size is unchanged dont need to do much other than replace the 
    // state, update the duration and scale
    if (sqrt_count == mSqrtCount)
    {
        for (i=0; i<mCount; i++)
	    {
			mpLeafGeometrySets[i]->setState( expl_state );
        }
        mpState = expl_state;

        SetFrameDuration(duration);
        if (scale != mSize) SetScale(scale);

        return;
    } 

	//mpTimedSelector->removeAllKids();
	mpSelector->removeAllKids();

	mCount = sqrt_count*sqrt_count;
	mSqrtCount = sqrt_count;
	mpState = expl_state;

	if (mCount > mListSize)
	{
		if (mpLeafGeometrySets) delete[] mpLeafGeometrySets;
		if (mpTextureCoordSets) delete[] mpTextureCoordSets;
		mListSize = mCount;
		
		// Set up mCount texture and geometry arrays unique for each leaf

		mpTextureCoordSets = new ssgTexCoordArray * [mCount];
		mpLeafGeometrySets = new ssgLeaf *[mCount];	
	}	
	
	float text_tile_len = float(1.0/mSqrtCount);

	for(i=0; i<mCount; i++)
	{
		sgVec2 text_coords;
		float  horiz = float((i % mSqrtCount) * text_tile_len);
		float vert  = float((i / mSqrtCount) * text_tile_len);
		mpTextureCoordSets[i] = new ssgTexCoordArray;
		for (j=0; j<4;j++)
		{
			sgSetVec2(text_coords,
				horiz + msTextureUnitCoord[j][0] * text_tile_len,
				vert + msTextureUnitCoord[j][1] * text_tile_len);
			mpTextureCoordSets[i]->add(text_coords);
			
		}
	}

	for (i=0; i<mCount; i++)
	{
		mpLeafGeometrySets[i] = new  ssgVtxTable ( GL_TRIANGLE_STRIP, mpVertices, 0, mpTextureCoordSets[i], mpColourArray );
		mpLeafGeometrySets[i]->setCallback( SSG_CALLBACK_PREDRAW, explosionPreDraw );
		mpLeafGeometrySets[i]->setCallback( SSG_CALLBACK_POSTDRAW, explosionPostDraw );
		mpLeafGeometrySets[i]->setState( mpState );
		//mpTimedSelector->addKid(mpLeafGeometrySets[i]);
		mpSelector->addKid(mpLeafGeometrySets[i]);
	}

	//mpTimedSelector->setLimits(0,mCount-1);
	SetFrameDuration(duration);
	SetScale(scale);

}



vsbExplosion::vsbExplosion():ssgTransform()
{
	CreateStructure();
}


/*
vsbExplosion::vsbExplosion(ssgSimpleState *expl_state, int sqrt_count):ssgTransform()
{
	int i, j;

	CreateStructure();

	mCount = sqrt_count*sqrt_count;
	mSqrtCount = sqrt_count;
	ssgSimpleState *state = (expl_state)?expl_state:MakeState();
	Initialise(state, sqrt_count, 1, 5);
}
*/

vsbExplosion::~vsbExplosion()
{
	ssgDeRefDelete(mpColourArray);
	ssgDeRefDelete(mpVertices);
}

void vsbExplosion::SetScale(float scale)
{
	int i;
    mSize = scale;
	vsbSetExplosionScale(scale, mpVertices);
	for (i=0; i<mCount; i++)
		mpLeafGeometrySets[i]->dirtyBSphere  ();
}

void vsbExplosion::SetFrameDuration(float ti)
{
	mFrameDuration = ti;
	//mpTimedSelector->setDuration(ti);
}


void  vsbExplosion::SetPosition( float x, float y, float z, float scale) 
{
	sgdSetVec3(mPos,x,y,z);
	vsbToScaledVSBCoords(mPos);

	sgVec3 p;
	p[0] = mPos[0];
	p[1] = mPos[1];
	p[2] = mPos[2];
	setTransform(p);

	if (scale>0) SetScale(scale);
}



void  vsbExplosion::GetPosition( float &x, float &y, float &z) 
{
	sgdVec3 pos;
	sgdCopyVec3(pos, mPos);
	vsbFromScaledVSBCoords(pos);
	x = pos[0]; y = pos[1]; z = pos[2];

}

void	vsbExplosion::CameraCentricTranslation()
{
	sgdVec3 adjustedPos;
	sgdCopyVec3(adjustedPos, mPos);
//	vsbToScaledVSBCoords(adjustedPos);
	sgdAddVec3(adjustedPos,vsbBaseCamera::CameraToOriginDelta());

	sgVec3 ap;
	ap[0] = adjustedPos[0];
	ap[1] = adjustedPos[1];
	ap[2] = adjustedPos[2];
	setTransform(ap);

} 

ssgSimpleState *vsbExplosion::MakeState(const char *explFname)
{

    string	texture_path = vsbMakeExplosionFXPath(explFname ? explFname: "explode1.rgba");


	ssgSimpleState *state = new ssgSimpleState();
    state->setTexture( const_cast<char *>(texture_path.c_str()),TRUE,TRUE,FALSE );
    state->setShadeModel( GL_SMOOTH );
    state->disable( GL_LIGHTING );
    state->disable( GL_CULL_FACE );
    state->enable( GL_TEXTURE_2D );
    state->enable( GL_COLOR_MATERIAL );
    state->setColourMaterial( GL_AMBIENT_AND_DIFFUSE );
    state->setMaterial( GL_EMISSION, 0, 0, 0, 1 );
    state->setMaterial( GL_SPECULAR, 0, 0, 0, 1 );
    state->enable( GL_BLEND );
    state->enable( GL_ALPHA_TEST );
	state->setAlphaClamp( 0.001f );
	state->setTranslucent();
    return state;
}

void vsbExplosion::StartAnimation(float initialTime) 
{
	//mInitialTime = vsbViewportContext::EntityManager().CurrTime();
	mInitialTime = initialTime;
	mpSelector->selectStep(0);
	mRunning = true;
	
	//mpTimedSelector->control ( SSG_ANIM_START );
};

void vsbExplosion::StopAnimation()  
{
	mRunning = false;
	//mpTimedSelector->control ( SSG_ANIM_STOP );
};


bool vsbExplosion::IsRunning() 
{ 
	return mRunning;
	//return mpTimedSelector->getControl() != SSG_ANIM_STOP; 
};

void vsbExplosion::AdvanceFrame()
{
	float currtime = vsbViewportContext::EntityManager().CurrTime();
	if (currtime < mInitialTime)
		//StopAnimation();
		mpSelector->selectStep(255); // hide the cutout
	else
		if (currtime > mInitialTime + (mCount+1)*mFrameDuration)
			//StopAnimation();
			mpSelector->selectStep(255); // hide the cutout
		else
			mpSelector->selectStep((currtime - mInitialTime)/mFrameDuration);


}
