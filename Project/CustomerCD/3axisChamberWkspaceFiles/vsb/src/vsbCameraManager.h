///////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbCameraManager.h
    \brief Defines the class vsbCameraManager and itas auxillary classes
    
     The class vsbCameraManager is responsible for managing all the cameras
     in a vsbViewContext. 
	 
	\sa vsbCameraManager.cpp

    \author T. Gouthas
*/
///////////////////////////////////////////////////////////////////////////
#ifndef _vsbCameraManager_H
#define _vsbCameraManager_H


#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef parserH
#include "parser.h"
#endif

#ifndef parser_perrorH
#include "parser_perror.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif

#ifndef _vsbCameraSchedule_H
#include "vsbCameraSchedule.h"
#endif

#ifndef _vsbBaseCamera_H
#include "vsbBaseCamera.h"
#endif

/*
EXPIMP_TEMPLATE template class DLL_VSB_API std::char_traits<char>;
EXPIMP_TEMPLATE template class DLL_VSB_API std::allocator<char>;
*/

//EXPIMP_TEMPLATE template class DLL_VSB_API std::basic_string<char, struct std::char_traits<char>,std::allocator<char> >;



//-----------------------------------------------------------------------
//! Defines an enumeration reflecting the status of camera after an operation on it

enum ECameraStatus			
{
	EC_OK,					//!< Construction successful
	EC_NoCameraManager,     //!< The camera did not have a valid camera manager reference
	EC_NameExists,          //!< A camera with the same name already exists
	EC_ListInsertFailure,   //!< New camera could not be inserted into the camera list
	EC_GeneralInitFailure,  //!< Unspecified initialisation error

};

class DLL_VSB_API vsbCameraManager;

////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbCameraException
    \brief This class defines the exception thrown by the camera manager when
    trying to create a camera 
*/
////////////////////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbCameraException 
{
public:

     /*! \brief Constructor
     \param err_id The camera error status
     \param someString A textual description elaborating on the exception reason
     */
    vsbCameraException(ECameraStatus err_id, const std::string &someString = "") 
										{ mId  = err_id; mDescription = someString; };

     //! Get the camera status enumeration
	 ECameraStatus	Id() const			{ return mId;};

     //! Get the description string
     const std::string &	Description() const	{ return mDescription;};

private:
	
     ECameraStatus	mId;
     std::string	mDescription;
};


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbAbstractCameraCreator
    \brief  This class defines the pure abstract base creator object
    
    Used to derrive a creator for a specific camera.
*/
////////////////////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbAbstractCameraCreator
{
	friend class vsbCameraManager;
public:
    /*! \brief Constructor
        \param cm Pointer to the camera manager that will host this camera

        The camera is given a default unique name.
    */
	virtual vsbBaseCamera *Create(vsbCameraManager *cm) = 0;

    /*! \brief Constructor
        \param cm Pointer to the camera manager that will host this camera
        \param name The name of the camera
        \param postfixed Flag indicating that a unique integer should be postfixed to the camera name
    */
    virtual vsbBaseCamera *Create(vsbCameraManager *cm, const std::string &name, bool postfixed = false) = 0;
	const std::string &Name() const {return m_CameraName;};
	int   InsertionIndex() const { return m_InsetionIndex; }
private:

	void SetName(const std::string &name) {m_CameraName = name;};
	void SetInsertionIndex(int idx) {m_InsetionIndex = idx;};

	int m_InsetionIndex;
	std::string m_CameraName;
};


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class CameraCreator
    \brief Camera creator template class
    
    This template class derives from vsbAbstractCamera providing a generalised
    camera creator for all possible cameras 
*/
////////////////////////////////////////////////////////////////////////////////
template <class T> class DLL_VSB_API CameraCreator : public vsbAbstractCameraCreator
{
public:

    /*! \brief Constructor
        \param cm Pointer to the camera manager that will host this camera
        The camera is given a default unique name.

        If the camera creation fails, 0 is returned;    
    */
	vsbBaseCamera *Create(vsbCameraManager *cm)
	{
		try 
		{
			return new T(cm, typeid(T).name()+9, true);
		} catch (vsbCameraException exc)
		{
			return 0;
		}
	}

    /*! \brief Constructor
        \param cm Pointer to the camera manager that will host this camera
        \param name The name of the camera
        \param postfixed Flag indicating that a unique integer should be postfixed to the camera name

        If the camera creation fails, 0 is returned;   
    */
    vsbBaseCamera *Create(vsbCameraManager *cm, const std::string &name, bool postfixed = false)
	{
		try 
		{
			return new T(cm, name, postfixed);
		} catch (vsbCameraException exc)
		{
			return 0;
		}
	}

};


class DLL_VSB_API vsbBaseCamera;


////////////////////////////////////////////////////////////////////////////////
/*! 
    \class vsbCameraManager
    \brief Camera manager class
    
     The class vsbCameraManager is responsible for managing all the cameras
     in a vsbViewContext.

    \note
    The camera manager always has a camera active at any given time. To that
    end a default camera is created at construction time.


*/
////////////////////////////////////////////////////////////////////////////////

#ifdef EXPIMP_TEMPLATE
//EXPIMP_TEMPLATE template class DLL_VSB_API std::map<std::string, vsbAbstractCameraCreator *>;
//EXPIMP_TEMPLATE template class DLL_VSB_API std::map<long, vsbBaseCamera *>;
#endif 

class DLL_VSB_API vsbCameraManager
{
	friend class DLL_VSB_API vsbBaseCamera;
private:

#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif
    
    //! List of camera creators corresponding to registered camera classes
    static std::map<std::string, vsbAbstractCameraCreator *> mCameraCreators;

    //! List of camera instances managed by this instance of vsbCameraManager
    std::map<long, vsbBaseCamera *> mCameraInstances;

#    ifdef _MSC_VER
#    pragma warning( pop ) // restor error warning level
#    endif

public:

	enum EActivationMode {
		CM_MANUAL,
		CM_SCHEDULE,
		CM_SCRIPT            /* Not implemented yet */
	};

    /*! \brief Constructor
    \param em A pointer to a valid vsbEntityManager.
    */
	vsbCameraManager();

    //! Virtual destructor
	virtual ~vsbCameraManager();

    /*! \brief Register a camera creator for a new camera class with the camera
        manager's camera factory.
        \param camClassName The name of the camera class that will be used to create it with 
        \param camClassCreator The creator object for the camera
        \note This function should not be used directly but rather through the
        macro function REGISTER_CAMERA_FACTORY
        As it is a static function, no vsbCameraManager instance is necessary and
        cameras registered can be created by any instance of vsbCameraManager
    */
    static bool RegisterCameraCreator(const std::string &camClassName, vsbAbstractCameraCreator *camClassCreator);

	static void DestroyCameraCreators();
    /*! \brief Camera creation function

    This function can be used to create an instance of a camera that has been 
    registered with a creator.
    \param cammClassName The name of the camera class to create
    \param instanceName  The cameras instance name
    \note If an empty instance name is provided, one is generated automatically
    */
    vsbBaseCamera *Create(const std::string &cammClassName, const std::string &instanceName = "");


	/*! \brief Camera loader

    This function can be used to load an instance of a camera checking 
    it for validity. 
	
	\note The camera loaded is not inserted into the camera list

	*/


	//@@@
	int Load(Parser::TLineInp *tli);

	Parser::TParserError & LastParseError() {return	mParseError;};



/** @name Camera retreival members 
Functions for getting a pointer to managed cameras
*/
//@{
    vsbBaseCamera *FindCam(const std::string &camName);//!< Find camera by instance name
	vsbBaseCamera *FindCam(long handle); //!< Find camera by ID
	vsbBaseCamera *FindFirstCam();        //!< First camera in the list 
	vsbBaseCamera *FindLastCam();         //!< Last camera in the lest
	vsbBaseCamera *FindNextCam();         //!< Next camera in the list  
	vsbBaseCamera *FindPrevCam();         //!< Previous camera in the list
    vsbBaseCamera *ActiveCam() {return mpActiveCamera; }; //!< Get currently active camera
//@}


	vsbCameraSchedule &CameraSchedule() {return  *mpCameraSchedule;};
	EActivationMode  ActivationMode() const {return mActivationMode;};
	void ActivationMode(EActivationMode mode) {mActivationMode = mode;};

	// Updates the active cam based on time and camera selection mode
	void UpdateActiveCam(double time);

    /*! \brief Change the currently active camera

    The camera supplied must be available in this camera manager
    */
	void		ActiveCam(vsbBaseCamera *cam); 
	
    //! Get the entity manager for this camera manager
//	vsbEntityManager *EntityManager(){return mpEntityManager;};
	
    static int  GetCameraFactoryClassCount();
    static bool GetCameraFactoryClassName(int index, std::string &name);
	static int  GetCameraFactoryClassInsertionIndex(const std::string &name);


	int  Write(FILE *f, int indent);


    static long          DefaultCameraId();
    static std::string&  DefaultCameraName();

	void ResetCameraManager() {DeleteAllCameras(); InsertDefaultCamera();}

private:

	void InsertDefaultCamera();

	void RemoveIteratorCam();       //!< Remove the camera iterator
	void DeleteAllCameras();        //!< Delete all managed camera instances

	EActivationMode     mActivationMode;

	vsbCameraSchedule   *mpCameraSchedule;		//!< Camera activation schedule

	vsbBaseCamera		*mpActiveCamera;        //!< Currently active camera
	Parser::TParserError	mParseError;        //!< The error returned by the camera loader 


// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958 
#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

    std::map<long, vsbBaseCamera *>::iterator mCamIterator; //! Camera iterator

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

};


class DLL_VSB_API vsbCameraManagerParser: public Parser::TParseObjectBase
{
public:
	vsbCameraManagerParser(vsbCameraManager &cm);
	virtual ~vsbCameraManagerParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
    vsbParserHelper *mpOutput;

	vsbCameraManager &mrCameraManager;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};


/*! \brief Defines a macro for registering new camera classes.

    Registers a new camera with the static camera-factory part of the camera 
    manager class, that is shared by all instances of the camera manager.
    Once a camera is registered, it may be created by any camera manager
    by simply identifying it by its class name.
*/
#define REGISTER_CAMERA_FACTORY(camclass) \
	vsbCameraManager::RegisterCameraCreator(#camclass, new CameraCreator<camclass>)


#endif