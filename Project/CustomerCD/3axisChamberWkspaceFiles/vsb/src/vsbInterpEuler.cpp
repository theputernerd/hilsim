//////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseCamera.cpp
    \brief Implements the class vsbBaseCamera
    
     The class vsbBaseCamera is the pure abstract base class for all 
	 cameras, providing the basic funtionality to be managed by the 
	 camera manager.
	
	\sa vsbBaseCamera.h

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 


#include <iostream>
#include <string>
#include <map>
#include <assert.h>

#include "vsbInterpEuler.h"
using namespace std;


void NormalizeQuat(float *q)
{
    int i;
    float mag;

    mag = FastSqrt(q[0]*q[0] + q[1]*q[1] + q[2]*q[2] + q[3]*q[3]);
    for (i = 0; i < 4; i++) q[i] /= mag;
}


void EulerToQuat(float rotz, float roty, float rotx, float *quat)
{
	float radX,radY,radZ,tempx,tempy,tempz;
  	float cosx,cosy,cosz,sinx,siny,sinz,cosc,coss,sinc,sins;

	// convert angles to radians	
	radX =  (rotx * 3.14159) / (360.0 / 2.0);
	radY =  (roty * 3.14159) / (360.0 / 2.0);	
	radZ =  (rotz * 3.14159) / (360.0 / 2.0);
	// half angles	
	tempx = radX * 0.5;	
	tempy = radY * 0.5;	
	tempz = radZ * 0.5;
	cosx = FastCos(tempx);	
	cosy = FastCos(tempy);	
	cosz = FastCos(tempz);
	sinx = FastSin(tempx);	
	siny = FastSin(tempy);	
	sinz = FastSin(tempz);

	cosc = cosx * cosz;	coss = cosx * sinz;	
	sinc = sinx * cosz;	sins = sinx * sinz;
	quat[SG_X] = (cosy * sinc) - (siny * coss);	
	quat[SG_Y] = (cosy * sins) + (siny * cosc);
	quat[SG_Z] = (cosy * coss) - (siny * sinc);	
	quat[SG_W] = (cosy * cosc) + (siny * sins);

	return;




	float rx, ry, rz, tx,ty,tz,cx,cy,cz,sx,sy,sz,/*cc,*/cs,sc/*,ss*/;

	// FIRST STEP, CONVERT ANGLES TO RADIANS
	rx =  (rotx * 3.14159f) / 180.0f;
	ry =  (roty * 3.14159f) / 180.0f;
	rz =  (rotz * 3.14159f) / 180.0f;
	// GET THE HALF ANGLES
	tx = rx * 0.5f;
	ty = ry * 0.5f;
	tz = rz * 0.5f;

	cx = FastCos(tx);	// Why isn't this one FastCos? ask themie.
	cy = FastCos(ty);
	cz = FastCos(tz);
	sx = FastSin(tx);
	sy = FastSin(ty);
	sz = FastSin(tz);

	float cxcz = cx * cz;
	cs = cx * sz;
	sc = sx * cz;
	float sxsz = sx * sz;

	quat[0] = (cy * sc) - (sy * cs);
	quat[1] = (cy * sxsz) + (sy * cxcz);
	quat[2] = (cy * cs) - (sy * sc);

	quat[3] = (cy * cxcz) + (sy * sxsz);

//zyx
	
	quat[SG_X] = cx * sy * sz + sx * cy * cz;
	quat[SG_Y] = cx * sy * cz - sx * cy * sz;
	quat[SG_Z] = cx * cy * sz + sx * sy * cz;
	quat[SG_W] = cx * cy * cz - sx *sy *sz;

//xyz
	
	quat[SG_X] = sx * cy * cz - cx * sy * sz;
	quat[SG_Y] = cx * sy * cz + sx * cy * sz;
	quat[SG_Z] = cx * cy * sz - sx * sy * cz;
	quat[SG_W] = cx * cy * cz + sx *sy *sz;

	
	quat[SG_X] = sx * cy * cz - cx * sy * sz;
	quat[SG_Y] = sx * cy * sz + cx * sy * cz;
	quat[SG_Z] = cx * cy * sz - sx * sy * cz;
	
	quat[SG_W] = cx * cy * cz + sx *sy *sz;
	// INSURE THE QUATERNION IS NORMALIZED

	// PROBABLY NOT NECESSARY IN MOST CASES
	NormalizeQuat(quat);
}


void MyEulerToQuat(sgQuat quat, float h, float p,  float r)
{
  sgMat4 m;
  /*SGfloat*/double ch, sh, cp, sp, cr, sr, srsp, crsp, srcp ;

  if ( h == SG_ZERO )
  {
    ch = SGD_ONE ;
    sh = SGD_ZERO ;
  }
  else
  {
    sh = /*(SGfloat)*/ FastSin( h * SG_DEGREES_TO_RADIANS ) ;
    ch = /*(SGfloat)*/ FastCos( h * SG_DEGREES_TO_RADIANS ) ;
  }

  if ( p == SG_ZERO )
  {
    cp = SGD_ONE ;
    sp = SGD_ZERO ;
  }
  else
  {
    sp = /*(SGfloat)*/ FastSin( p * SG_DEGREES_TO_RADIANS ) ;
    cp = /*(SGfloat)*/ FastCos( p * SG_DEGREES_TO_RADIANS ) ;
  }

  if ( r == SG_ZERO )
  {
    cr   = SGD_ONE ;
    sr   = SGD_ZERO ;
    srsp = SGD_ZERO ;
    srcp = SGD_ZERO ;
    crsp = sp ;
  }
  else
  {
    sr   = /*(SGfloat)*/ FastSin( r * SG_DEGREES_TO_RADIANS ) ;
    cr   = /*(SGfloat)*/ FastCos( r * SG_DEGREES_TO_RADIANS ) ;
    srsp = sr * sp ;
    crsp = cr * sp ;
    srcp = sr * cp ;
  }

  m[0][0] = (SGfloat)(  ch * cr - sh * srsp ) ;
  m[1][0] = (SGfloat)( -sh * cp ) ;
  m[2][0] = (SGfloat)(  sr * ch + sh * crsp ) ;

  m[0][1] = (SGfloat)( cr * sh + srsp * ch ) ;
  m[1][1] = (SGfloat)( ch * cp ) ;
  m[2][1] = (SGfloat)( sr * sh - crsp * ch ) ;

  m[0][2] = (SGfloat)( -srcp ) ;
  m[1][2] = (SGfloat)(  sp ) ;
  m[2][2] = (SGfloat)(  cr * cp ) ;


  SGfloat tr, s, q[4] ;
  int   i, j, k ;

  int nxt[3] = {1, 2, 0};

  tr = m[0][0] + m[1][1] + m[2][2];

  // check the diagonal
  if (tr > SG_ZERO )
  {
    s = (SGfloat) FastSqrt (tr + SG_ONE);
    quat[SG_W] = s / SG_TWO;
    s = SG_HALF / s;
    quat[SG_X] = (m[1][2] - m[2][1]) * s;
    quat[SG_Y] = (m[2][0] - m[0][2]) * s;
    quat[SG_Z] = (m[0][1] - m[1][0]) * s;
  }
  else
  {		
    // diagonal is negative
   	i = 0;
    if (m[1][1] > m[0][0]) i = 1;
    if (m[2][2] > m[i][i]) i = 2;
    j = nxt[i];
    k = nxt[j];
    s = (SGfloat) FastSqrt ((m[i][i] - (m[j][j] + m[k][k])) + SG_ONE);
    q[i] = s * SG_HALF;
            
    if (s != SG_ZERO) s = SG_HALF / s;

    q[3] = (m[j][k] - m[k][j]) * s;
    q[j] = (m[i][j] + m[j][i]) * s;
    q[k] = (m[i][k] + m[k][i]) * s;

    quat[SG_X] = q[0];
    quat[SG_Y] = q[1];
    quat[SG_Z] = q[2];
    quat[SG_W] = q[3];
  }
}


void MyQuatToRotmatrix(sgMat4 m, float *quaternion)
{

		// calculate coefficients
	float x2 = quaternion[0] + quaternion[0];
	float y2 = quaternion[1] + quaternion[1];
	float z2 = quaternion[2] + quaternion[2];
	float xx = quaternion[0] * x2;
	float xy = quaternion[0] * y2;
	float xz = quaternion[0] * z2;
	float yy = quaternion[1] * y2;
	float yz = quaternion[1] * z2;
	float zz = quaternion[2] * z2;
	float wx = quaternion[3] * x2;
	float wy = quaternion[3] * y2;
	float wz = quaternion[3] * z2;

	#define M(x,y) m[x][y]
	
	M(0,0) = 1.0f - (yy + zz);
	M(0,1) = xy + wz;
	M(0,2) = xz - wy;
	M(0,3) = 0.0f;

	M(1,0) = xy - wz;
	M(1,1) = 1.0f - (xx + zz);
	M(1,2) = yz + wx;
	M(1,3) = 0.0f;
	
	M(2,0) = xz + wy;
	M(2,1) = yz - wx;
	M(2,2) = 1.0f - (xx + yy);
	M(2,3) = 0.0f;
	
	M(3,0) = 0.0f;
	M(3,1) = 0.0f;
	M(3,2) = 0.0f;
	M(3,3) = 1.0f;
	
	#undef M

}



// To be improved
// currently not eworking in sg
//           sgEulerToQuat
//           sgQuatToMatrix
//           sgQuatToEuler
void  vsbInterpEuler(sgVec3 res, sgVec3 from_hpr, sgVec3 to_hpr,float t)
{
	sgQuat to;
	sgQuat from;
	sgQuat interp;
	sgCoord crd;
	sgMat4 interp_mat;

	MyEulerToQuat(from, from_hpr[0], from_hpr[1], from_hpr[2]);
	MyEulerToQuat(to,to_hpr[0], to_hpr[1], to_hpr[2]);
	sgSlerpQuat(interp, from,to, t);
	MyQuatToRotmatrix (interp_mat, interp ); 
	sgSetCoord(&crd, interp_mat);
	sgCopyVec3(res, crd.hpr);
}

void vsbInterpEuler(sgdVec3 res, sgdVec3 from, sgdVec3 to,float t)
{
	// Convert to use doubles.
	sgVec3 resf, fromf, tof;

	for( int i = 0; i < 3 ; ++i )
	{
		resf[i] = res[i];
		fromf[i] = from[i];
		tof[i] = to[i];
	}

	vsbInterpEuler( resf, fromf, tof, t );

	for( i = 0; i < 3 ; ++i )
	{
		res[i] = resf[i];
		from[i] = fromf[i];
		to[i] = tof[i];
	}
}


