#ifndef _vsbHUDTools_H
#define _vsbHUDTools_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

class DLL_VSB_API vsbRGB { 
private:
	GLfloat mRed;
    GLfloat mGreen;
	GLfloat mBlue;
	GLfloat mAlpha;
	GLfloat *mpTriple;
public:	
	vsbRGB();
	vsbRGB(vsbRGB &copy);
	vsbRGB(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0f);
	vsbRGB & operator==(const vsbRGB &copy);

	GLfloat  Red()				{return mRed;};
	void     Red(GLfloat r)		{mRed = r;};
	GLfloat  Green()			{return mGreen;};
	void     Green(GLfloat g)	{mGreen = g;};
	GLfloat  Blue()				{return mBlue;};
	void     Blue(GLfloat b)	{mBlue = b;};
	GLfloat  Alpha()			{return mAlpha;};
	void     Alpha(GLfloat a)	{mAlpha = a;};

	GLfloat *Triple()				{return mpTriple;};
	void     Triple(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0f) 
					{mRed = r; mGreen = g; mBlue = b; mAlpha = a;};

	void     RGBScale(GLfloat r, GLfloat g, GLfloat b, GLfloat a = 1.0f) {mRed *= r; mGreen *= g; mBlue *= b; mAlpha *= a;};
	void     RGBScale(GLfloat s) {mRed *= s; mGreen *= s; mBlue *= s;};

	void	Apply();
	void	Apply(float scaled);
};

const float Pi = 3.1415931f;
const float TwoPi = 2*3.1415931f;
const float PiOnTwo = 3.1415931f/2;
const float PiOnFour = 3.1415931f/4;

inline float ToDeg(float r){ return r/Pi*180.0; };
inline float ToRad(float d){  return d/180.0*Pi; };

void vsbDrawArrow(float x,float y,float angle_rad,float len);
void vsbDrawCircle(float x,float y,float radius, int resolution = 5);
void vsbDrawArc(float x,float y,float radius, float start_angle_rad, float end_angle_rad);
void vsbMakeEarthToBodyMat(sgMat4 m, float h, float p, float r);

#endif