////////////////////////////////////////////////////////////////////////
/*! 
    \file vsbBaseViewEntity.cpp
    \brief Defines the class vsbBaseViewEntity and itas auxillary classes
    
     The class vsbBaseEntity is the  abstract base class for all entities
	 in the 3D scene, providing the basic funtionality necessary to to be 
	 managed by the entity manager.
	
	\sa vsbBaseViewEntity.h

    \author T. Gouthas
*/
////////////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 


#include <assert.h>

#include "ssg.h"



#include "vsbAttachmentPoint.h"


#include "tool_hashstr.h"

using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////
//  vsbAttachmentPoint
////////////////////////////////////////////////////////////////////////////////////////////


vsbAttachmentPoint::vsbAttachmentPoint()
{
	mName = "";
	mId = MakeId(mName);
	sgdSetVec3(mPoint,0,0,0);
}

vsbAttachmentPoint::vsbAttachmentPoint(const std::string &name, sgdVec3 pt)
{
	Set(name, pt);
}

vsbAttachmentPoint::~vsbAttachmentPoint()
{
}

void vsbAttachmentPoint::Set(const std::string &name, sgdVec3 pt)
{	
	mName = name;
    mId = MakeId(mName);
    sgdCopyVec3(mPoint,pt);
}


vsbAttachmentPoint & vsbAttachmentPoint::operator =(const vsbAttachmentPoint &from)
{
	mName = from.mName;
	mId = from.mId;
	sgdCopyVec3(mPoint,from.mPoint);
	return *this;
}


int	vsbAttachmentPoint::MakeId(const std::string &name)
{
	return tool::HashString(name.c_str(), 16333);
}

/////////////////////////////////////////////////////////////////////////////////////

void vsbEntityAttachmentPoint::Set(const std::string &name, sgdVec3 pt, bool evaluated )
{
	vsbAttachmentPoint::Set(name,pt);
	mIsEvaluated = evaluated;
}

vsbEntityAttachmentPoint &	vsbEntityAttachmentPoint::operator =(const vsbAttachmentPoint &from)
{
	*((vsbAttachmentPoint *)this) = from;
	mIsEvaluated = false;
	mTemplateAttachmentPoint = &from;
	return *this;
}

vsbEntityAttachmentPoint &	vsbEntityAttachmentPoint::operator =(const vsbEntityAttachmentPoint &from)
{
	*((vsbAttachmentPoint *)this) = from;
	mIsEvaluated = from.mIsEvaluated;
	mTemplateAttachmentPoint = from.mTemplateAttachmentPoint;
	return *this;
}

void vsbEntityAttachmentPoint::Transform(sgdMat4 mat)
{
	double t0 = TemplatePoint()[0];
	double t1 = TemplatePoint()[1];
	double t2 = TemplatePoint()[2];
	
	
	
	mPoint[0] = t0 * mat[ 0 ][ 0 ] +
		t1 * mat[ 1 ][ 0 ] +
		t2 * mat[ 2 ][ 0 ] ;
	
	mPoint[1] = t0 * mat[ 0 ][ 1 ] +
		t1 * mat[ 1 ][ 1 ] +
		t2 * mat[ 2 ][ 1 ];
	
	mPoint[2] = t0 * mat[ 0 ][ 2 ] +
		t1 * mat[ 1 ][ 2 ] +
		t2 * mat[ 2 ][ 2 ] ;

	mIsEvaluated = true;
}

void vsbEntityAttachmentPoint::Transform(sgMat4 mat)
{
	double t0 = TemplatePoint()[0];
	double t1 = TemplatePoint()[1];
	double t2 = TemplatePoint()[2];
	
	
	
	mPoint[0] = t0 * mat[ 0 ][ 0 ] +
		t1 * mat[ 1 ][ 0 ] +
		t2 * mat[ 2 ][ 0 ] ;
	
	mPoint[1] = t0 * mat[ 0 ][ 1 ] +
		t1 * mat[ 1 ][ 1 ] +
		t2 * mat[ 2 ][ 1 ];
	
	mPoint[2] = t0 * mat[ 0 ][ 2 ] +
		t1 * mat[ 1 ][ 2 ] +
		t2 * mat[ 2 ][ 2 ] ;
	

	mIsEvaluated = true;
}