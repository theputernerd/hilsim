#ifndef _vsbTrajEntitySTLVector_H
#define _vsbTrajEntitySTLVector_H

//////////////////////////////////////////////////////////////////
/*! 
    \file vsbTrajEntitySTLVector.h
    \brief Defines an entity class derived from vsbTrajEntity that has the 
	ability to retain a temporal state history	This retention is acheived via 
	STL vectors.

	Pros:
	Access is relatively fast as each point can be found by offset
	Adding new points is fast but is subject to the following cons.

	Cons:
	Data points must be inserted in increasing and equidistant time steps
	
 
   	\sa vsbTrajEntitySTLVector.cpp

  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#include "vsbBaseViewEntity.h"


#ifdef _MSC_VER
	#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _MAP_
#include <map>
#define _MAP_
#endif

#ifndef _VECTOR_
#include <vector>
#define _VECTOR_
#endif


#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbTrajEntity_H
#include "vsbTrajEntity.h"
#endif


#define _BV_TRAJ_ENTITY_VECTOR     0x0008
inline int vsbTypeTrajEntityVector   () { return _BV_TRAJ_ENTITY_VECTOR | vsbTypeTrajEntity() ; }




// Cannot be exported - for DLL internal use only - see MSDN
//
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958  

typedef std::vector<vsbInstanceData>           vsbInstanceDataVector;    //!< Time referenced map of vsbInstanceData, ie, Trajectory
typedef std::vector<vsbInstanceData>::iterator vsbInstanceDataVectorIter; //!< Iterator for Trajectories

/////////////////////////////////////////////////////////////////
/*! 
    \class vsbTrajEntitySTLVector 

    \brief Defines an vsbTrajEntity derived class that maintains a 
    trajectory history

     
    The vsbTrajEntitySTLVector class is designed to maintain, along with its current 
    state data, (as every vsbBaseEntity derived class does)  
    a state data (trajectory) history. Any time referenced state variables
    can be stored as part of the trajectory history.
    
    This enables entities to, for example, load a future state history on 
    instantiation and step through it, or to store instantaneous state as 
    each time step elapses to facilitate a replay capability or the capability 
    to provide history information for trajectory plots or any other history 
    aware behaviour.

*/
////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbTrajEntitySTLVector : public  vsbTrajEntity
{
public:

                        /*! \brief Constructor

                        Initialises the vsbTrajEntitySTLVector object.
                        \param name A name for the vsbTrajEntitySTLVector object
                        \param postfixed If this parameter is set to true, the object name is
                        constructed from the provided name postfixed with a unique integer value
				         */
                        vsbTrajEntitySTLVector(const std::string &name, bool postfixed = false);

                        //*! Virtual destructor
	virtual				~vsbTrajEntitySTLVector();

						/*! \brief Get the string name of the class of entity
						*/
	virtual const std::string &	ClassName() const;


                        //! Clear the trajectory history for this entity
    virtual void		ClearTraj();

                        /*! \brief Set the entity's state for the specified time from history data
                        if available.

                        For the required time instant, trajectory data is looked up from th mTraj
                        member, and if  mInterpolate is set to true, the data is interpolated for
                        the specified time, otherwise the trajectory data for the lower time bound 
                        is used. This data is then used to update the current state of the vsbTrajEntity.

                        The CheckBounds member is called to determine what to do when the requested
                        time is beyond the time range specified for this entity.
                        \sa also MinTime, MaxTime, MinMaxTime, InfiniteTimeRange found in vsbBaseViewEntity.

                        \param timeto The time at which to set the entity's state
                        */
    void				SetCurrTime(float timeto);

                        /*! \brief Advance the entity's state for the specified time increment if 
                        history data is available. Same interpolation rules apply as in <I>SetCurrTime</I>.
                
                        For the required time increment advance from the current time, trajectory data is 
                        looked up from the mTraj member, and if mInterpolate is set to true, the data is 
                        interpolated for the new calculated current time, otherwise the trajectory data for 
                        the lower time bound is used.  This data is then used to update the current state of 
                        the vsbTrajEntity.

                        The CheckBounds member is called to determine what to do when the requested
                        time is beyond the time range specified for this entity.
                        \sa MinTime, MaxTime, MinMaxTime, InfiniteTimeRange found in vsbBaseViewEntity.

                        \param timeto The time increment for which to advance the entity's state
                        */
	void				AdvanceTime(float deltaTime);

						/*! \brief Add a new vsbInstanceData entry for a specified time instant 
						into the trajectory
						
						Creates a new vsbInstanceData entry for the time specified by <I>atTime</I> and 
						inserts it into the trajectory <I>mTraj</I>. If the specified time already exists 
						in the trajectory, it is overwritten. Otherwise it is inserted int the apropriate 
						position according to its time.

						If the <I>updateFromState</I> parameter is true, then the new vsbInstanceData entry
						is initialised from the current state of the entity, which happens via the 
						<I>mpVarAddress</I> members of the vsbTrajVar object <I>mTrajVarDef</I>.

						\param atTime The time stamp of the new entity state (vsbInstanceData) to insert 
						into the trajectory

						\param updateFromState If set to true, the entity state inserted into the trajectory 
						is initialised with the entity's current state parameters, otherwise the new entity 
						state remains un-initialised.

						 \sa UpdateInstanceDataFromState
						*/
	bool				AddNewTime(float atTime, bool updateFromState = false);
	

						/*! \brief Add a new state variable to be included in mTraj's vsbInstanceData entries.
						
						Defines a new time dependant variable to be tracked in the vsbInstanceData entries of
						mTraj. This variable is registered with mTrajVarDef so that it new vsbInstanceData
						instances contain it. 
						
						\note Adding new variables is illegal if there is already data in mTraj since this 
						would result in an inconsistent set of variables in the mTraj entries. It is essential 
						that all mTrajVar's vsbInstanceData entries have the same set of variables.

						\param varname The name of the variable
						\param addr  The address of the location where the value may be retreived from.
						This address is intended for automatic update of the variable value in the trajectory
						and will usually be a pointer to a member variable in the vsbTrajEntity derived class.
						\returns  true if variable added successfully successfully

						/sa The <I>AddVar</I> member function of vsbTrajVar, UpdateInstanceData and RefreshFromInstanceData 
						*/
    bool				AddVar(const std::string &varname, double *addr = 0, EInterpolationStyle interpStyle = INTERP_NONE);

						/*! \brief Get a reference to the trajectory data for this entity
						\returns A reference to vsbInstanceDataMap
                        \note CANNOT BE EXPORTED OUTSIDE OF DLL
						*/

	virtual bool		FirstInstanceData(vsbInstanceData &data);
	virtual bool		LastInstanceData(vsbInstanceData &data);
	virtual bool		NextInstanceData(vsbInstanceData &data);
	virtual bool		PrevInstanceData(vsbInstanceData &data);

	void				ReserveSpace(int count);
	void				ResizeIncrement(int count) {mVectorSizeIncrease = count;};

protected:


// Following members cannot be exported outside of DLL
// -- strictly for DLL internal use only - see MSDN
//See;
//HOWTO: Exporting STL Components Inside & Outside of a Class
//Last reviewed: January 19, 1998
//Article ID: Q168958  

#    ifdef _MSC_VER
#    pragma warning( push )
#    pragma warning( disable : 4251 ) // STL not exported warning
#    endif

	vsbInstanceData		mLastData;

	vsbInstanceDataVector  mTraj;				//!< The trajectory data, a map of type <time,vsbInstanceData>
	vsbInstanceDataVectorIter mLowerIter;			//!< Trajectory iterator
	vsbInstanceDataVectorIter mUpperIter;			//!< Trajectory iterator
	vsbInstanceDataVectorIter mSteppingIter;	

	float mTimeStepSize;

	int	  mReserveVectorSize;
	int   mVectorSizeIncrease;

#    ifdef _MSC_VER
#    pragma warning( pop )
#    endif

private:

};	


#endif