//////////////////////////////////////////////////////////////////
/*! 
    \file vsbGroundPlaneParams.h
    \brief Defines the class vsbGroundPlaneParams
 
	This file defines the vsbGroundPlaneParams class which encapsulates 
    all the information necessary for the construction and manipulation 
    of flat textured ground planes (terrains), however it does not define 
    any ground plane implementation methods.
 	
	\sa vsbGroundPlaneParams.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////


#ifndef _vsbGroundPlaneParams_H
#define _vsbGroundPlaneParams_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef parserH
#include "parser.h"
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _vsbPlaneTextureList_H
#include "vsbPlaneTextureList.h"
#endif

#ifndef _vsbParserHelper_H
#include "vsbParserHelper.h"
#endif


typedef enum 
{
    vsbTEST_ONLY,
    vsbTEST_AND_WRITE,
    vsbNO_TEST_NO_WRITE
} vsbDepthTestStyle;


#define MAX_GROUND_PLANES 2 /*!< The maximum number of ground planes allowed, 
                                 ie one ground and one ocean bed if required */

class DLL_VSB_API vsbGroundPlane;
class DLL_VSB_API vsbGroundPlaneParser;

//////////////////////////////////////////////////////////////////
/*! 
    \class vsbGroundPlaneParams
    \brief Ground plane  parameters class
 
	The vsbGroundPlaneParams class encapsulates all the information necessary 
    for the construction and manipulation of flat textured ground planes 
    (terrains), however it does not define any ground plane implementation methods.
    
    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

class DLL_VSB_API vsbGroundPlaneParams
{
	friend class DLL_VSB_API vsbGroundPlane;
	friend class DLL_VSB_API vsbGroundPlaneParser;

private:

	float				mAlt;			 //!< Altitude of the terrain 
	float				mLength;		 //!< Cloud length in M from one horizon to the next	
	int                 mGroundTextureIdx;//!< Ground plane texture ID 
	float				mScale;		     //!< A facor by which planes are stretched
	vsbDepthTestStyle	mDepthTest;      //!< Depth testing enable flag

	bool				mModified;       //!< Flag indicating change in any of the parameters
    bool				mNeedRebuild;    //!< flag indicating change in any of the parameters

	static int                  mInstanceCount;    //!< Count of the number of instances
	static vsbPlaneTextureList *mPlaneTextureList; //!< List containing info on all available ground plane texture  
	static ssgSimpleState	**msGroundPlaneState;   //!< Ground plane ssgSimpleState

public:

    /*! \brief Gets the number number of available ground plane textures

    The number of available textures is looked up in the static texture list
    mPlaneTextureList which is shared by all ground planes.
    */
	static int MaxPlaneTextures(); 

	//-------- Standard member functions

    //! Constructor
	vsbGroundPlaneParams();

    //! Copy constructor
	vsbGroundPlaneParams(vsbGroundPlaneParams &cp);

    //! Virtual destructor
	virtual ~vsbGroundPlaneParams();

    //! Assignment operator
	vsbGroundPlaneParams	&operator = (const vsbGroundPlaneParams& cp);

    //! Comparison operator 
	bool					operator == (const vsbGroundPlaneParams& cp) const;

    //! Comparison operator
	bool					operator != (const vsbGroundPlaneParams& cp) const 
												{return !(*this == cp);};

	//-------- Class members

						//! Set the default cloud parameters (see cpp file for details)
	void				SetDefault();

						// Enquire & Set accessor functions

                        /*! \brief Convenience function to set primary ground plane parameters
                            \param textureIdx The index of the texture applied to this ground plane   
                            \param alt Altitude of the ground plane above sea level
                            \param depthTest Flag to enable depth testing while drawing
                          */
	void				Set(int textureIdx, double alt = 0.0f, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);

                        /*! \brief Convenience function to set primary ground plane parameters
                            \param textureLabel The reference name of the texture applied to this ground plane   
                            \param alt Altitude of the ground plane above sea level
                            \param depthTest Flag to enable depth testing while drawing
                          */

    void				Set(const std::string &textureName, double alt = 0.0f, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);

                        /*! \brief Convenience function to set primary ground plane parameters
                            \param textureLabel The reference name of the texture applied to this ground plane   
                            \param alt Altitude of the ground plane above sea level
                            \param depthTest Flag to enable depth testing while drawing
                          */
	void				Set(const char *textureName, double alt = 0.0f, vsbDepthTestStyle depthTest = vsbTEST_AND_WRITE);

                        //! Set ground plane altitude above sea level  
	void				Alt(double alt)			{ mAlt = (float) alt; mModified = true; };

                        //! Get ground plane altitude above sea level
	float				Alt() const			    { return mAlt; };	

                        //! Set ground plane length
	void				Length(double len)		{ mLength = (float) len; mModified = true; };
                        
                        //! Get ground plane length
	float				Length() const			{ return mLength; };	

                        //! Set ground plane texture scale (in M)
	void				Scale(double s)		    { mScale = (float) s; mModified = true; };

                         //! Get ground plane texture scale (in M)
	float				Scale() const		    { return mScale; };	

                         //! Set OpenGL depth testing status
	void				DepthTest(vsbDepthTestStyle style)	{ mDepthTest = style; mModified = true; };
            
                        //! Get OpenGL depth testing status
	vsbDepthTestStyle	DepthTest() const		{ return mDepthTest; };	
        
                        //! Get the default scale of the selected ground texture
	float				DefaultScale();

                        //! Set ground plane texture by index
	void				GroundTextureIdx(int textureIdx) {  mNeedRebuild = (mGroundTextureIdx != textureIdx);
															mGroundTextureIdx = textureIdx; 
                                                            mModified = true; };

                        //! Get selected ground plane texture index                                    
    int                 GroundTextureIdx() const		{ return mGroundTextureIdx; } ;

                        //!Get a reference to the texture list
    static vsbPlaneTextureList &	TextureList() ;


                        //! Get the parameter modification status of the ground plane
	bool				IsModified() const				{ return mModified; };

						//! Get the rebuild status of the ground plane
	bool				NeedRebuild() const				{ return mNeedRebuild; };

	void			    NeedRebuild(bool trueFalse) { mNeedRebuild = trueFalse; 
													  if (mNeedRebuild) mModified = true;};

protected:
                        //! Clear the parameter modification status of the ground plane
	void				ClearModified() 				{ mNeedRebuild = mModified = false; };
};


class DLL_VSB_API vsbGroundPlaneParamsParser: public Parser::TParseObjectBase
{
public:
	vsbGroundPlaneParamsParser(vsbGroundPlaneParams &gp);
	virtual ~vsbGroundPlaneParamsParser();

	int	 Parse(Parser::TLineInp *tli);
	int  Write(FILE *f, int indent);

private:
	vsbParserHelper *mpOutput;

	vsbGroundPlaneParams &mrGroundPlaneParams;
	static char *mpKeyWords[];
	static char *mpErrorStrings[];
    virtual void ParseFunc();
};


#endif