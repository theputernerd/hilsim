
#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <assert.h>


#include "ssg.h"
#include "vsbViewportContext.h"

using namespace std;

#include "vsbSmokeVtxTable.h"

void vsbSmokeVtxTable::recalcBSphere ()
{
	return;
}

 sgSphere *vsbSmokeVtxTable::getBSphere ()
{
	 mCurrAge = vsbViewportContext::EntityManager().CurrTime() - mStartTime;
	 
	if (mCurrAge < 0 || mCurrAge > mMaxLife)
	{
		mExpired = true;

	} else
	{	
		mExpired = false;

		sgdSetVec3(mInstantaneousPos,mPos[0],-mPos[1],-mPos[2]);	
		
		sgdVec3 adjust;
		sgdSetVec3(adjust, mVel[0]*mCurrAge,-mVel[1]*mCurrAge,-mVel[2]*mCurrAge);
		sgdAddVec3(mInstantaneousPos,adjust);

		if (vsbViewportContext::RenderOpts().CameraCentricTransformsOn())
			sgdAddVec3(mInstantaneousPos,vsbBaseCamera::CameraToOriginDelta());

		
		bsphere.setCenter(mInstantaneousPos[0],mInstantaneousPos[1],mInstantaneousPos[2]);
		bsphere.setRadius(mStartSize+mExpansionRate*mCurrAge);
	}
	return &bsphere;
}

void vsbSmokeVtxTable::copy_from ( vsbSmokeVtxTable *src, int clone_flags )
{
    ssgVtxTable::copy_from ( src, clone_flags ) ;

	sgdCopyVec3(mVel,src->mVel);
	sgdCopyVec3(mPos,src->mPos);
				
	
	mExpansionRate = src->mExpansionRate;				
	
	mMaxLife = src->mMaxLife;					
	mStartTime = src->mStartTime;						
	mStartSize = src->mStartSize;					
	
	mExpired = src->mExpired;	
	
	mCurrAge = src->mCurrAge;							
	
	
}

ssgBase *vsbSmokeVtxTable::clone ( int clone_flags )
{
    vsbSmokeVtxTable *b = new vsbSmokeVtxTable ;
    b -> copy_from ( this, clone_flags ) ;
	b->mDecayMode = mDecayMode;
    return b ;
}

vsbSmokeVtxTable::vsbSmokeVtxTable ():ssgVtxTable()
{
	mExpired = false;
	mDecayMode = DECAY_LINEAR;
	
    gltype = GL_TRIANGLE_STRIP;
    type = ssgTypeVtxTable () ;
    vertices  =  new ssgVertexArray   () ;
    normals   =  new ssgNormalArray   () ;
    texcoords =  new ssgTexCoordArray () ;
    colours   =  new ssgColourArray   () ;
	
    vertices  -> ref () ;
    normals   -> ref () ;
    texcoords -> ref () ;
    colours   -> ref () ;
	
    recalcBSphere () ; 
	setCallback( SSG_CALLBACK_PREDRAW, NULL );
}

vsbSmokeVtxTable:: vsbSmokeVtxTable (ssgVertexArray	*shd_vtx , float initsize) 
{
	setCallback( SSG_CALLBACK_PREDRAW, NULL );
	mStartSize = initsize;
	mExpired = false;
	mDecayMode = DECAY_LINEAR;

	
    gltype = GL_TRIANGLE_STRIP;
    type = ssgTypeVtxTable () ;
    vertices  = (shd_vtx!=NULL) ? shd_vtx : new ssgVertexArray   () ;
    normals   =  new ssgNormalArray   () ;
    texcoords =  new ssgTexCoordArray () ;
    colours   =  new ssgColourArray   () ;
	
    vertices  -> ref () ;
    normals   -> ref () ;
    texcoords -> ref () ;
    colours   -> ref () ;
	
    recalcBSphere () ;
}

vsbSmokeVtxTable::~vsbSmokeVtxTable ()
{
} 



void vsbSmokeVtxTable::draw_geometry ()
{

				
	// If particle is expired, dont draw it
	if (mExpired) return;

    float alpha;
    GLfloat modelView[16];
    sgVec3 A,B,C,D;
    sgVec3 right,up;

	float currtime = vsbViewportContext::EntityManager().CurrTime();

	//


	switch(mDecayMode)
	{
	case DECAY_SQUARE:
		
		alpha =  (float)(mCurrAge/mMaxLife);
		alpha= 1 - alpha*alpha;
		
		break;
		
	case DECAY_CUBE:
		
		alpha =  (float)(mCurrAge/mMaxLife);
		alpha= 1 - alpha*alpha*alpha;
		break;
		
	case DECAY_INVERSESQUARE:
		
		alpha =  1-(float)(mCurrAge/mMaxLife);
		alpha*=alpha;

		break;

	case DECAY_INVERSECUBE:
		
		alpha =  1-(float)(mCurrAge/mMaxLife);
		alpha= alpha*alpha*alpha;
		break;
 
	case DECAY_LINEAR:
		alpha =  1 -((float)(mCurrAge/mMaxLife));
				
		break;
	case DECAY_MASSCONSERVATION:
		alpha = mStartSize/(mStartSize + mExpansionRate*mCurrAge);

		alpha = alpha*alpha;
		break;
	}
	
    glDepthMask(GL_FALSE);


    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
	

    /* the principle is to have mAcc right and up verctor 
	to determine how the points of the quadri should be placed
	orthoganly to the view, paralel to the screen.
	This alogo is inspired from the Cutout in Plib 
	and from the cutout in racer code */
	
    /* get the matrix */
    glGetFloatv(GL_MODELVIEW_MATRIX,modelView);
	
    /* get the up and right vector from the matrice view */
    up[0] = modelView[1];
    up[1] = modelView[5];
    up[2] = modelView[9];
	
    right[0] = modelView[0];
    right[1] = modelView[4];
    right[2] = modelView[8];
	
    /* compute the coordinates of the 
	four points of the quadri. */
	
    /* up and right points    left and up */
    C[0] = right[0]+up[0];    D[0] = -right[0]+up[0];
    C[1] = right[1]+up[1];    D[1] = -right[1]+up[1];
    C[2] = right[2]+up[2];    D[2] = -right[2]+up[2];
    
    /* down and up            right and down */
    A[0] = -right[0]-up[0];   B[0] = right[0]-up[0];
    A[1] = -right[1]-up[1];   B[1] = right[1]-up[1];
    A[2] = -right[2]-up[2];   B[2] = right[2]-up[2];
    
	if (mAmbientLit)
	{
		// time of day ambient colouration
		const GLfloat* adj_fog_color = vsbViewportContext::EnvironmentManager().LightingParams().AdjFogColour();
		glColor4f(adj_fog_color[0],adj_fog_color[1],adj_fog_color[2],alpha);
	}
	else
		// force to specified RGB colouration
		glColor4f(mColor[0],mColor[1],mColor[2],alpha); 


    /* the computed coordinates are translated from the smoke position
	with the x,y,z speed*/

	//@@@@@ In here add adjustments for camera centric coordinates here

	double size = bsphere.getRadius();
	sgVec3 centre;
	bsphere.getCenter(centre);



    glBegin ( gltype ) ;

    glTexCoord2f(0,0);
    glVertex3f(centre[0]+size*A[0],centre[1]+size*A[1], centre[2]+size*A[2]);
    glTexCoord2f(0,1);
    glVertex3f(centre[0]+size*B[0],centre[1]+size*B[1], centre[2]+size*B[2]);
    glTexCoord2f(1,0);
    glVertex3f(centre[0]+size*D[0],centre[1]+size*D[1], centre[2]+size*D[2]);
    glTexCoord2f(1,1);
    glVertex3f(centre[0]+size*C[0],centre[1]+size*C[1], centre[2]+size*C[2]);
    glEnd () ;



    glDepthMask(GL_TRUE);
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
}