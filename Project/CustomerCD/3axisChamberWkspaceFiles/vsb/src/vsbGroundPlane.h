//////////////////////////////////////////////////////////////////
/*! 
    \file vsbGroundPlane.h
    \brief Defines the class vsbGroundPlane
 
	This file defines a vsbGroundPlane class, which manages the
	the construction, manipulation and drawing of a ground plane. It builds 
	an ssgRoot node with all the apropriate children including textures, 
	and ground plane geometry. It further provides functions to allow the 
	positioning the ground plane according to the next frame's requirements.
	
	\sa vsbGroundPlane.cpp

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifndef _vsbGroundPlane_H_
#define _vsbGroundPlane_H_

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 

#ifndef _STRING_
#include <string>
#define _STRING_
#endif

#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif

#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

#ifndef _vsbGroundPlaneParams_H
#include "vsbGroundPlaneParams.h"
#endif

class DLL_VSB_API vsbGroundPlaneParamsParser;

//////////////////////////////////////////////////////////////////
/*! 
    \class vsbGroundPlane
    \brief Manages texture mapped ground planes
 
	Manages the construction, manipulation and drawing of a ground plane. 
    It builds an ssgRoot node with all the apropriate children including textures, 
	and ground plane geometry. It further provides functions to allow the 
	positioning the ground plane according to the next frame's requirements.

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////
class DLL_VSB_API vsbGroundPlane : public vsbGroundPlaneParams 
{

private:

					 
    ssgRoot          *mpGroundPlaneRoot;  //<! The root node of the vsbGroundPlane

					 	
    ssgTransform     *mpGroundPlaneXfm;  //<! The vsbGroundPlane's transformation

    ssgColourArray   *mpColorArr;       //!< vsbGroundPlane colour array 
    ssgVertexArray   *mpVertexArr;      //!< vsbGroundPlane vertex array 
    ssgTexCoordArray *mpTextureArr;     //!< vsbGroundPlane texture array 

					   /* \brief Storage for texture coordinate origin

					   Texture coordinate origin, critical for the "moving" effect 
                       by altering texture coordinates
					  */
	
	sgVec2            mInitialTextureCoords[6];
	
public:

    //! Constructor
    vsbGroundPlane( void );

    //! Destructor
    virtual ~vsbGroundPlane( void );

	/*! \brief Comparison operator 

       Allows testing for equality of any specified ground plane's parameters with 
       current ground plane's parameters
    */
	bool operator == (const vsbGroundPlaneParams &cp); 

    /*! \brief Assignment operator
	
     Assigns a new set of ground plane parameters to the ground plane instance,
     rebuilding it immediately after assignment, and clearing the modified flag
     once it has been rebuilt
	*/

	vsbGroundPlane &operator = (const vsbGroundPlaneParams &cp); 

	//! destroy the ground planegeometry 
	void Destroy();

    /*! \brief Build the ground plane object
    
     Building includes constructing the geometry, and mapping the texture
    */
	void Build();

    /*! \brief Build the ground plane object
    
     Building includes constructing the geometry, and mapping the texture
     as above but overrides the previously set values with new ones
     \sa Build()
     */
    void Build( double mSize, double asl,  int terrainTextureIdx );

	/*! \brief Update the geometry parameters fot the gorund plane.

	If this ground plane is already built, but the texture has not been changed,
	updates the layer's texture coordinates otherwise build the ground plane from
	scratch
	*/
	void UpdateGeometry();

	//! Test to see whether cloud geometry has been built
	bool IsBuilt() const { return mpGroundPlaneRoot != 0; };

    /*!\brief Reposition the ground plane 
	  Reposition the ground plane to be centred directly over
	  the vertical line through the specified position (which
	  should be the current camera position) and compensate 
      for this movement in the texture coordinates.
	  \param camerapos The camera position
	*/
    bool Reposition( const sgVec3 camerapos );

    /*! \brief Repaint the ground plane colors 
	
     Repaint the ground plane colors based on current value of sun_angle,
     sky, and fog colors. This updates the color arrays for
     ssgVtxTable. 
	 \param fog_color Determines the colour of the cloud
	 which should blend into the skydomes fog_colour.
	*/
    bool Repaint( const sgVec3 fog_colour ) ;

    //! draw the ground plane 
    void Draw();

private:

    //! Get the ground plane texture filename
    const std::string &GroundPlaneTextureFileName(int terrainTextureIdx);

    //! Utility function to create ground plane states
	ssgSimpleState *MakeGroundPlaneState(int terrainTextureIdx);

};



#endif // _vsbGroundPlane_H_
