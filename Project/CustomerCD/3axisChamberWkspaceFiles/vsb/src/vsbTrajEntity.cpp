#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 
//////////////////////////////////////////////////////////////////
/*! 
    \file vsbTrajEntity.cpp
    \brief Implements an entity class vsbTrajEntity that has the ability
    to retain a temporal state history
    
    This file implements several helper classes and the primary class
    vsbTrajEntity class designed to maintain, along with its current 
    state data, (as every vsbBaseEntity derived class does)  
    a state data (trajectory) history. Any time referenced state variables
    can be stored as part of the trajectory history.
    
    This enables entities to, for example, load a future state history on 
    instantiation and step through it, or to store instantaneous state as 
    each time step elapses to facilitate a replay capability or the capability 
    to provide history information for trajectory plots or any other history 
    aware behaviour.

	\sa vsbTrajEntity.h
 
  \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#include <string>
#include <set>
#include <iostream>
#include <assert.h>

using namespace std;

#include "ssg.h"
#include "vsbBaseViewEntity.h"
#include "vsbTrajEntity.h"
#include "vsbModelRoot.h"
#include "vsbViewportContext.h"
#include "vsbEnvironmentManager.h"
#include "vsbInterpEuler.h"

int vsbInstanceData::msDefaultAllocationSize = 12;

vsbInstanceData::vsbInstanceData()
{
	mNumElements = msDefaultAllocationSize;
	mpData = new double[mNumElements];
	memset(mpData, 0, sizeof(double)*mNumElements);
}

vsbInstanceData::vsbInstanceData(const vsbInstanceData &someCopy)
{
	mNumElements = someCopy.mNumElements;
	mpData = new double[mNumElements];
	memcpy(mpData, someCopy.mpData, sizeof(double)*mNumElements);
}


vsbInstanceData::vsbInstanceData(int nElements)
{
	mNumElements = nElements;
	mpData = new double[mNumElements];
	memset(mpData, 0, sizeof(double)*mNumElements);
}


vsbInstanceData::~vsbInstanceData()
{
	delete [] mpData;
}


void vsbInstanceData::Resize(int nElements)
{
	double *old_data = mpData;

	mpData = new double[nElements];

	int copy_element_count = (nElements >= mNumElements)?mNumElements:nElements;
	
	memcpy(mpData, old_data, copy_element_count * sizeof(double));

	if (nElements > mNumElements)
	{
		memset(&mpData[mNumElements], 0, (nElements-mNumElements) * sizeof(double));
	}

	delete [] old_data;
	mNumElements = nElements;
}



vsbInstanceData &vsbInstanceData::operator = (const vsbInstanceData &someCopy)
{
	if (mNumElements!=someCopy.mNumElements)
	{
		if (mpData) delete [] mpData;

		mNumElements = someCopy.mNumElements;	
		mpData = new double[mNumElements];
	}
	memcpy(mpData, someCopy.mpData, sizeof(double)*mNumElements);
	return *this;
}


double &vsbInstanceData::operator [](int idx)
{
	assert(idx >=0);
	assert(idx <= mNumElements);
	return mpData[idx];
}


// Normalise a quaternion



vsbInstanceData vsbInstanceData::Interpolate( vsbInstanceData &inst, float proportion, const EInterpolationStyle *interpStyle)
{
	assert (inst.mNumElements == mNumElements);
//	assert (proportion >= 0 && proportion <= 1.0);

	
	vsbInstanceData idat(mNumElements);
	for(int i=0; i<mNumElements; i++)
	{
		if (  interpStyle==0 || interpStyle[i] == INTERP_NONE  || proportion == 0.0f)
			idat[i] = this->mpData[i];
		else if (proportion == 1.0f)
			idat[i] = inst[i];
		else if (interpStyle[i] == INTERP_LINEAR)
			idat[i] = this->mpData[i] + proportion * (inst[i] - this->mpData[i]);
		else /*INTERP_SLERP*/
		{
			// We expect at least two more entries which we **assume** are
			// of INTERP_SLERP interpolation type as well and are the correct euler component angles.

			assert(i+2 < mNumElements); 
			vsbInterpEuler(&idat.mpData[i], &this->mpData[i], &inst.mpData[i], proportion);

			i+=2;
		}
	}
	return idat;
}

//////////////////////////////////////////////////////////////////////////

vsbTrajVar::vsbTrajVar():mVarCount(0)
{
	for (int i = 0; i<MAX_TRAJ_VARS; i++)
	{
		mInterpStyle[i] = INTERP_NONE;
		mpVarAddress[i] = NULL;
	}

}

vsbTrajVar::~vsbTrajVar()
{
}

bool vsbTrajVar::AddVar(const string &varname,double *addr, EInterpolationStyle interpStyle)
{

	if (Index(varname) >= 0) return false;	
							// shouldnt ever insert duplicat var names
							// ** If used correctly, should never occur

	if (mVarCount >= MAX_TRAJ_VARS) return false;
							// maximum number of variable fields exceeded

	mpVarAddress[mVarCount] = addr;
	mInterpStyle[mVarCount] = interpStyle;
	mVars[mVarCount++] = varname;


	return true;
}

void vsbTrajVar::UpdateInstanceData(vsbInstanceData &inst)
{
	for (int i=0; i<mVarCount; i++)
		if (mpVarAddress[i]) *mpVarAddress[i] = inst[i];
	
}

void vsbTrajVar::RefreshFromInstanceData(vsbInstanceData &inst) const
{
	for (int i=0; i<mVarCount; i++)
		if (mpVarAddress[i]) inst[i] = *mpVarAddress[i] ;
	
}

int	vsbTrajVar::Index(const string &index_str)
{
	for (int i=0; i<mVarCount; i++)
		if (!stricmp(mVars[i].c_str(),index_str.c_str())) return i;

	return -1; // not found
}


void vsbTrajVar::Clear()
{
	mVarCount = 0;
}


double &vsbTrajVar::Var(const string &varname, vsbInstanceData &inst)
{
	return inst[Index(varname)];
	
}

double &vsbTrajVar::Var(int id, vsbInstanceData &inst)
{
	return inst[id];
}

/////////////////////////////////////////////////////////////////

const std::string &vsbTrajEntity::ClassName() const
{
	static string class_name = "vsbTrajEntity";
	return class_name;
}


vsbTrajEntity::vsbTrajEntity(const string &name, bool postfixed):
vsbBaseViewEntity(name,postfixed)
{
	mTypeID = vsbTypeTrajEntity(); // initialise the type id

	AddVar("T", &mCurrTime, INTERP_LINEAR);
	AddVar("X", &mEntityPosXYZ[0], INTERP_LINEAR);
	AddVar("Y", &mEntityPosXYZ[1], INTERP_LINEAR);
	AddVar("Z", &mEntityPosXYZ[2], INTERP_LINEAR);
	AddVar("H", &mOrientationHPR[0], INTERP_SLERP);
	AddVar("P", &mOrientationHPR[1], INTERP_SLERP);
	AddVar("R", &mOrientationHPR[2], INTERP_SLERP);
	DataFields(/*orientation*/true, /*velocity*/ false, /*angRates*/ false);

/*
	if (mEntityRoot!= 0) 
	{
		mEntityRoot->Selectors().RegisterSelector("GEAR", 0);
		mEntityRoot->Selectors().RegisterSelector("FLAPS", 2 );
		mEntityRoot->Selectors().RegisterSelector("PROP", 2 );
	}
*/
	mpTrajLeaf = new vsbTrajLeaf(this);
	mpTrajLeaf->ref();
	vsbViewportContext::EnvironmentManager().SceneRoot()->addKid(mpTrajLeaf);
}

vsbTrajEntity::~vsbTrajEntity()
{
	vsbViewportContext::EnvironmentManager().SceneRoot()->removeKid(mpTrajLeaf);
	ssgDeRefDelete(mpTrajLeaf);
}



bool vsbTrajEntity::AddVar(const string &varname, double *addr, EInterpolationStyle interpStyle )
{
	// Jim's "fix".
	mCurrData.Resize( mCurrData.Size() + 1 );

	return mTrajVarDef.AddVar(varname, addr, interpStyle);
}


void vsbTrajEntity::UpdateInstanceDataFromState()
{
	mTrajVarDef.RefreshFromInstanceData(mCurrData); 
}

void vsbTrajEntity::UpdateStateFromInstanceData()
{
	mTrajVarDef.UpdateInstanceData(mCurrData); 
}


int	vsbTrajEntity::FindVarIdx(const string &varname)
{
	return mTrajVarDef.Index(varname);
}

double &	vsbTrajEntity::operator[](int idx)
{
	static double tmp;
	tmp = 0;
	if (idx >= 0 && idx < mTrajVarDef.Count()) 
	{
		return mCurrData[idx];
	}

	return tmp;
} 

double &	vsbTrajEntity::operator[](const string &varname)
{
	static double tmp;
	tmp = 0;
	int idx = mTrajVarDef.Index(varname);
	if (idx >= 0 && idx < mTrajVarDef.Count()) 
	{
		return mCurrData[idx];
	}

	return tmp;
}
