#ifndef _vsbSmokeVtxTable_H
#define _vsbSmokeVtxTable_H

#ifndef __cplusplus                                                          
# error This file requires C++
#endif 


#ifndef _INCLUDED_SSG_H_
#include "ssg.h"
#endif


#ifndef _vsbWin32DLL_H
#include "vsbWin32DLL.h"
#endif

//class ssgVtxTable;

enum EDecayMode {DECAY_LINEAR, DECAY_SQUARE, DECAY_INVERSESQUARE, DECAY_CUBE, DECAY_INVERSECUBE,  DECAY_MASSCONSERVATION};


class DLL_VSB_API vsbSmokeVtxTable : public ssgVtxTable
{
    friend class DLL_VSB_API vsbSmokeEmitterDefinition;
	friend class DLL_VSB_API vsbSmokeFXDirector;

protected:
	virtual void copy_from ( vsbSmokeVtxTable *src, int clone_flags ) ;
	void recalcBSphere ();
	
public:
	

	virtual ssgBase *clone ( int clone_flags = 0 ) ;
	vsbSmokeVtxTable () ;
	vsbSmokeVtxTable (ssgVertexArray	*shd_vtx , float initsize);
	virtual ~vsbSmokeVtxTable (void);	

	bool IsExpired() const {return mExpired;}
	void StartSize(double s){mStartSize = s; recalcBSphere () ;};

	virtual sgSphere *getBSphere ();

	void SetPos(double x, double y, double z ){sgdSetVec3(mPos,x,y,z);}
	void SetPos(sgdVec3 xyz ){sgdCopyVec3(mPos,xyz);}

protected:	
	
	sgdVec3 mPos;
	sgdVec3 mVel;						// velocity vector of smoke puff

	double mMaxLife;					// maximum life in seconds of smoke puff
	double mStartTime;					// life starting time	
	double mStartSize;					// starting size
	double mExpansionRate;				// expansion factor per second of smoke puff

	bool   mExpired;				    // flag indicating whether particle is expired
	EDecayMode   mDecayMode;			// if true decay is inverse square, otherwise linear
	bool   mAmbientLit;
	sgVec3 mColor;


	// calculated dynamically?
	double mCurrAge;					// age of smoke puff in seconds
	void draw_geometry();
private:

	sgdVec3 mInstantaneousPos;

};


#endif


