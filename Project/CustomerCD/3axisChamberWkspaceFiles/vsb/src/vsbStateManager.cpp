//////////////////////////////////////////////////////////////////
/*! 
    \file vsbStateManager.cpp
    \brief Implements a class for managing shared ssgSimpleState's
 

	This file implements a cache of sorts which you can use to 
    load states and look-up and subsequently reference them as required 
    
    It also implements the ability to register a number of callback
    functions which can be called to create/load new states. 
	

    \author T. Gouthas
*/
//////////////////////////////////////////////////////////////////

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif

#include "vsbStateManager.h"

using namespace std;

vsbStateManager::vsbStateManager()
{
	mStateCache.clear();
	mLoaderList.clear();
}

vsbStateManager::~vsbStateManager()
{
	void ClearStates();
	void ClearLoaders();
}

vsbStateManager::InsertStatus 
vsbStateManager::Insert(const string &stateName, ssgSimpleState *someState)
{
	
	if (!someState)
		return EStateManagerFailed;
	pair<map<string, ssgSimpleState *>::iterator, bool> res;
	map<string, ssgSimpleState *>::iterator iter;

	res = mStateCache.insert(pair<string, ssgSimpleState *>(stateName,someState));
	if (res.second) 
	{
		someState->ref();
		return EStateManagerOK;
	} else
		return EStateManagerExists;
}

void vsbStateManager::Delete(const std::string &stateName)
{
	map<string, ssgSimpleState *>::iterator iter;
	iter = mStateCache.find(stateName);
	if (iter != mStateCache.end())
	{
		ssgDeRefDelete(iter->second);
		mStateCache.erase(iter);
	}
}

void vsbStateManager::ClearStates()
{
	map<string, ssgSimpleState *>::iterator iter;
	for (iter  = mStateCache.begin(); iter!= mStateCache.end(); iter++)
	{
		ssgDeRefDelete(iter->second);
	}
	mStateCache.clear();
}

ssgSimpleState *vsbStateManager::Find(const string &stateName) 
{
	
	map<string, ssgSimpleState *>::iterator iter;
	iter = mStateCache.find(stateName);
	if (iter == mStateCache.end()) return 0;
	return iter->second;
}

ssgSimpleState *vsbStateManager::FindByTextureFilename(const string &textureFileName)
{
    map<string, ssgSimpleState *>::iterator iter;

/*	iter = mStateCache.find(textureFileName);
	if (iter != mStateCache.end())
		return 0;
	else return 
		iter->second;
*/

    for(iter = mStateCache.begin(); iter != mStateCache.end(); iter++)
    {
        if (textureFileName == iter->second->getTextureFilename()) return iter->second;
    }
    return 0;

}


void vsbStateManager::ClearLoaders()
{
	mLoaderList.clear();
}

vsbStateManager::InsertStatus vsbStateManager::AddLoader(vsbStateManagerLoader loader)
{
	if (!loader) 
		return EStateManagerFailed;

	pair<set<vsbStateManagerLoader>::iterator, bool> res;

	set<vsbStateManagerLoader>::iterator iter;

	res = mLoaderList.insert(loader);
	if (res.second) 
		return EStateManagerOK;
	else
		return EStateManagerExists;
}

void vsbStateManager::RemoveLoader(vsbStateManagerLoader loader)
{
	set<vsbStateManagerLoader>::iterator iter;
	iter = mLoaderList.find(loader);
	if (iter == mLoaderList.end()) return;
	mLoaderList.erase(iter);
}

void vsbStateManager::CallLoaders()
{
	set<vsbStateManagerLoader>::iterator iter;
	for (iter = mLoaderList.begin(); iter != mLoaderList.end(); iter++)
	{
       // vsbStateManagerLoader loader = *iter;
		(**iter)(this);
	}
}