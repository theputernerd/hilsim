//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:57:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_filetree.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_filetree.h/.cpp

        Class implementing recursive data storage of a directory listing
*/
//---------------------------------------------------------------------------
#ifdef _MSC_VER
	#pragma warning (disable : 4786) // identifier truncated in debug info
#endif

#include "tool_filetree.h"

#include "tool_bincodec.h"
#include "tool_fileutils.h"
#include <iostream>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


class TFileTree::Privates
{
  public:
    Privates() {}
    Privates(const TFileTree::Privates& in)
      : d_subDirs(in.d_subDirs),
        d_files(in.d_files)
    {}

    std::list<TFileTree> d_subDirs;
    std::list<TFileData> d_files;
};
//---------------------------------------------------------------------------


TFileTree::TFileTree(const std::string& dirPath)
  : d_this(new Privates)
{
    setDirPath(dirPath);
}
//---------------------------------------------------------------------------


TFileTree::TFileTree(std::istream& is)
  : d_this(new Privates)
{
    read(is);
};
//---------------------------------------------------------------------------


TFileTree::TFileTree(const TFileTree& in)
  : d_dirPath(in.d_dirPath),
    d_alias(in.d_alias),
    d_isFiltered(in.d_isFiltered),
    d_dirData(in.d_dirData),
    d_this(new Privates(*in.d_this))
{
}
//---------------------------------------------------------------------------


TFileTree::~TFileTree()
{
    delete d_this;
}
//---------------------------------------------------------------------------


TFileTree& TFileTree::operator=(const TFileTree& in)
{
    if (&in == this)
        return *this;

    d_dirPath = in.d_dirPath;
    d_alias = in.d_alias;
    d_isFiltered = in.d_isFiltered;
    d_dirData = in.d_dirData;
    d_this->d_subDirs = in.d_this->d_subDirs;
    d_this->d_files = in.d_this->d_files;

    return *this;
}
//---------------------------------------------------------------------------


const std::list<TFileTree>& TFileTree::getSubDirs() const
{
    return d_this->d_subDirs;
}
//---------------------------------------------------------------------------


const std::list<TFileData>& TFileTree::getFiles() const
{
    return d_this->d_files;
}
//---------------------------------------------------------------------------


void TFileTree::setDirPath(const std::string& dirPath)
{
    clear();

    d_dirPath = dirPath;
    d_alias = tool::extractFileName(dirPath);
    d_isFiltered = false;

    std::list<TFileData> contents = tool::getFileDataList(dirPath, "*");

    for (std::list<TFileData>::iterator i = contents.begin();
         i != contents.end(); ++i)
    {
        if (i->isDirectory())
        {
            d_this->d_subDirs.push_back(TFileTree(dirPath + "/" + i->getFileName()));
        }
        else
        {
            d_this->d_files.push_back(*i);
        }
    }
}
//---------------------------------------------------------------------------

void TFileTree::setFilter(const std::list<std::string>& filterList)
{
    // filter the sub dirs
    for (std::list<TFileTree>::iterator it = d_this->d_subDirs.begin();
         it != d_this->d_subDirs.end(); ++it)
    {
        it->setFilter(filterList);
    }

    // filter the files in this dir
    for (std::list<TFileData>::iterator id = d_this->d_files.begin();
         id != d_this->d_files.end(); ++id)
    {
        bool filter = false;

        for (std::list<std::string>::const_iterator j = filterList.begin();
             j != filterList.end(); ++j)
        {
            filter |= tool::wildcardMatch(id->getFileName(), *j);
        }

        id->setFiltered(filter);
    }
}
//---------------------------------------------------------------------------

void TFileTree::clear()
{
    d_dirPath = ""; // MSVC Compat
    d_alias = "";// MSVC Compat
    d_dirData.clear();
    d_this->d_subDirs.clear();
    d_this->d_files.clear();
}
//---------------------------------------------------------------------------

std::istream& TFileTree::read(std::istream& is)
{
    double version = TBinCodec<double>(is);

    if (version != 1.0)
        throw std::runtime_error("Unsupported version of File Tree");

    clear();

    size_t size = TBinCodec<size_t>(is);
    char* buffer = new char[size+1];
    is.get(buffer, size);
    buffer[size] = '\0';
    d_dirPath = buffer;
    delete[] buffer;
    size = TBinCodec<size_t>(is);
    buffer = new char[size+1];
    is.get(buffer, size);
    buffer[size] = '\0';
    d_alias = buffer;
    delete[] buffer;
    d_isFiltered = TBinCodec<bool>(is).val();
    is >> d_dirData;
    size = TBinCodec<size_t>(is);
    size_t i; // MSVC Compat
    for (i = 0; i < size; ++i)
    {
        d_this->d_subDirs.push_back(TFileTree(is));
    }
    size = TBinCodec<size_t>(is);
    for (i = 0; i < size; ++i)
    {
        d_this->d_files.push_back(TFileData(is));
    }

    return is;
}
//---------------------------------------------------------------------------


std::ostream& TFileTree::write(std::ostream& os) const
{
    os << TBinCodec<double>(1.0);

    os << TBinCodec<size_t>(d_dirPath.size());
    os << d_dirPath.c_str();
    os << TBinCodec<size_t>(d_alias.size());
    os << d_alias.c_str();
    os << TBinCodec<bool>(d_isFiltered);
    os << d_dirData;
    os << TBinCodec<size_t>(d_this->d_subDirs.size());
    for (std::list<TFileTree>::const_iterator it = d_this->d_subDirs.begin();
         it != d_this->d_subDirs.end(); ++it)
    {
        os << *it;
    }
    os << TBinCodec<size_t>(d_this->d_files.size());
    for (std::list<TFileData>::const_iterator id = d_this->d_files.begin();
         id != d_this->d_files.end(); ++id)
    {
        os << *id;
    }

    return os;
}
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------

} // namespace tool


