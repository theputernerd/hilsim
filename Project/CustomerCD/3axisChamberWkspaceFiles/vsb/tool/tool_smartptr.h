//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.4 $
//  $RCSfile: tool_smartptr.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_smartptr.h/.cpp

        Adapted from smartptr.h by Sandu Turcan, idlsoft@hotmail.com

    Usage:

    // ---------------------
    // 1. In a program block
    // ---------------------
    SmartPtr<TMyObject> ptr1(new TMyObject); // creates object 1
    SmartPtr<TMyObject> ptr2(new TMyObject); // creates object 2
    ptr1 = ptr2;             // destroys object 1
    ptr2 = 0;
    ptr1 = new TMyObject;    // creates object 3, destroys object 2
    ptr1->methodcall(...);
    TMyObject o1;
    // ptr1 = &o1;  // DON'T ! only memory allocated by new operator should be used
    TMyObject *o2 = new TMyObject;
    ptr1 = o2;
    //ptr2 = o2;  // DON'T ! unless TMyObject implements IRefCount
                 // try to use ptr1 = ptr2 instead, it's always safe;

    // ---------------------
    // 2. in a function call
    // ---------------------
    void f(TMyObject *o) {...}
    ...
    SmartPtr<TMyObject> ptr(new TMyObject)
    f(ptr);

    // --------------------
    // 3. As a return value
    // --------------------
    SmartPtr<TMyObject> f()
    {
        SmartPtr<TMyObject> ptr(new TMyObject);
        return ptr;
    }

*/
//---------------------------------------------------------------------------
#ifndef tool_smartptrH
#define tool_smartptrH

namespace tool {
//---------------------------------------------------------------------------

// forward declaration
template <class T> class SmartPtr;
//---------------------------------------------------------------------------

// IRefCount
// is an interface for reference counting
// Classes can inherit this interface themselves,
// or SmartPtr will provide its internal implementation of IRefCount
template <class T> class IRefCount {
    friend class SmartPtr<T>;
    protected:
    virtual void __IncRefCount() = 0;
    virtual void __DecRefCount() = 0;
    virtual T * GetPtr() const = 0;
};
//---------------------------------------------------------------------------

// IRefCountImpl
// is a standart implementation of IRefCount
// To use it just derive your class from it:
// class TMyObject : public IRefCountImpl<TMyObject> { ... };
// Reminder: implementing IRefCount is optional but it would reduce
// memory fragmentation.
template <class T> class IRefCountImpl : public IRefCount<T> {
    private:
    int __m_counter;
    protected:
    virtual void __IncRefCount()
    {
        __m_counter++;
    }
    virtual void __DecRefCount()
    {
        __m_counter--;
        if(__m_counter<=0)
        {
            __DestroyRef();
        }
    }
    virtual T * GetPtr() const
    {
        return ((T *)this);
    }
    virtual void __DestroyRef()
    {
        if(GetPtr()!=0)
            delete GetPtr();
    }
    protected:
    IRefCountImpl()
    {
        __m_counter = 0;
    }
};
//---------------------------------------------------------------------------

template <class T> class SmartPtr
{
  public:
    SmartPtr()
    {
        __m_refcount = 0;
    }
    SmartPtr(T * ptr)
    {
        __m_refcount = 0;
        __Assign(ptr);
    }
    SmartPtr(const SmartPtr &sp)
    {
        __m_refcount = 0;
        __Assign(sp.__m_refcount);
    }
    virtual ~SmartPtr()
    {
        __Assign((IRefCount<T> *)0);
    }

    // get the contained pointer, not really needed but...
    T *GetPtr() const
    {
        if(__m_refcount==0) return 0;
        return __m_refcount->GetPtr();
    }

    // assign another smart pointer
    SmartPtr & operator = (const SmartPtr &sp) {__Assign(sp.__m_refcount); return *this;}
    // assign pointer or 0
    SmartPtr & operator = (T * ptr) {__Assign(ptr); return *this;}
    // to access members of T
    T * operator ->()
    {
#ifdef _ASSERT
        _ASSERT(GetPtr()!=0);
#endif
        return GetPtr();
    }

    // to get const access to members of T
    const T * operator ->() const
    {
#ifdef _ASSERT
        _ASSERT(GetPtr()!=0);
#endif
        return GetPtr();
    }

    // conversion to T* (for function calls)
    operator T* () const
    {
        return GetPtr();
    }

    // conversion to const T* (for const function calls)
    operator const T* () const
    {
        return GetPtr();
    }

    // utilities
    bool operator !()
    {
        return GetPtr()==0;
    }
    bool operator ==(const SmartPtr &sp)
    {
        return GetPtr()==sp.GetPtr();
    }
    bool operator !=(const SmartPtr &sp)
    {
        return GetPtr()!=sp.GetPtr();
    }

  private:
    IRefCount<T> *__m_refcount;

    /////////////////////////////////////////
    // __RefCounter
    // An internal implementation of IRefCount
    // for classes that don't implement it
    // SmartPtr will automatically choose between its internal and
    // class' implementation of IRefCount
    class __RefCounter : public IRefCountImpl<T> {
        private:
        T *__m_ptr;
        protected:
        virtual T * GetPtr() const
        {
            return __m_ptr;
        }
        virtual void __DestroyRef() {delete this;}
        public:
        __RefCounter(T *ptr)
        {
            __m_ptr = ptr;
        }
        virtual ~__RefCounter()
        {
            IRefCountImpl<T>::__DestroyRef();
        }
    };
    // this method is called if T does not implement IRefCount
    void __Assign(void *ptr)
    {
        if(ptr==0)
            __Assign((IRefCount<T> *)0);
        else
        {
            __Assign(new __RefCounter(static_cast<T *>(ptr)));
        }
    }
    // this method is picked over __Assign(void *ptr)
    // if T implements IRefCount.
    // This allows some memory usage optimization
    void __Assign(IRefCount<T> *refcount)
    {
        if(refcount!=0) refcount->__IncRefCount();
        IRefCount<T> *oldref = __m_refcount;
        __m_refcount = refcount;
        if(oldref!=0) oldref->__DecRefCount();
    }
};
//---------------------------------------------------------------------------

}; // namespace tool

#endif
