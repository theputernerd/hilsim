//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.16 $
//  $RCSfile: tool_matrix.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_matrix.h/.cpp

          Basic_Matrix<T>
          Matrix    - typedef Basic_Matrix<double>

      2D matrix template providing some useful numeric operations.

      Original concept taken from Stroustrup, 'The C++ Programming Language'
          Note: bug fix in body of 'column' methods from text.

      inverse, determinant & matrix * matrix operations from Nick Luckman

      matrix * valarray operators not yet complete

      Author - Duncan Fletcher
*/
//---------------------------------------------------------------------------
#ifndef tool_matrixCPP
#define tool_matrixCPP

#include "tool_matrix.h"
#include "tool_mathtools.h"
//#ifndef numericSTL
//    #include <numeric>
//    #define numericSTL
//#endif
#ifndef stdexceptSTL
    #include <stdexcept>
    #define stdexceptSTL
#endif
#ifndef limitsSTL
    #include <limits>
    #define limitsSTL
#endif

#pragma hdrstop
//---------------------------------------------------------------------------

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif

namespace tool {

//export template<typename T>
//Basic_Matrix<T>::Basic_Matrix(size_t x, size_t y)
//  : v(x*y), d1(x), d2(y)
//{
//}
//---------------------------------------------------------------------------

export template<typename T>
T mul(const CSlice_iter<T>& v1,const std::valarray<T>& v2)
{
    if (v1.size() != v2.size())
        throw std::runtime_error("Dimension mismatch in matrix-row * vector operation");
    T res = 0;
    for (size_t i = 0; i < v1.size(); i++)
        res += (v1[i])*(v2[i]);
    return res;
}
//---------------------------------------------------------------------------

export template<typename T>
std::valarray<T> operator*(const Basic_Matrix<T>& m, const std::valarray<T>& v)
{
    if (m.dim2() != v.size())
        throw std::runtime_error("Dimension mismatch in matrix * vector operation");
    std::valarray<T> res(m.dim1());
    for (size_t i = 0; i < m.dim1(); i++)
        res[i] = mul(m.rowIter(i), v);
    return res;
}
//---------------------------------------------------------------------------


// assignment operators

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator=(const Basic_Matrix<T>& in)
{
    if (this == &in)
        return *this;

    if (dim1() != in.dim1() || dim2() != in.dim2())
        resize(in.dim1(), in.dim2());

    v = in.v;

    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator=(const std::valarray<T>& vin)
{
    if (&v == &vin)
        return *this;

    if (v.size() != vin.size())
        throw std::runtime_error("Dimension mismatch in valarray assignment to matrix");

    v = vin;

    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator=(const T& x)
{
    v = x;
    return *this;
}
//---------------------------------------------------------------------------


// operator-assign with scalar args

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator*=(const T& d)
{
    v*=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator/=(const T& d)
{
    v/=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator%=(const T& d)
{
    v%=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator+=(const T& d)
{
    v+=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator-=(const T& d)
{
    v-=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator^=(const T& d)
{
    v^=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator&=(const T& d)
{
    v&=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator|=(const T& d)
{
    v|=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator<<=(const T& d)
{
    v<<=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator>>=(const T& d)
{
    v>>=d;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator+=(const Basic_Matrix<T>& rhs)
{
    if (dim1() != rhs.dim1() || dim2() != rhs.dim2())
        throw std::runtime_error("Dimension mismatch in matrix + matrix operation");

    v += rhs.v;

    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator-=(const Basic_Matrix<T>& rhs)
{
    if (dim1() != rhs.dim1() || dim2() != rhs.dim2())
        throw std::runtime_error("Dimension mismatch in matrix + matrix operation");

    v -= rhs.v;

    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::operator*=(const Basic_Matrix<T>& rhs)
{
    // 1x1 matrices are interpretted here as scalars to ensure that the
    // recursive determinant and inverse algorithms work.

	// get info from another matrix instead of matrix operation result
	size_t rhs_r = rhs.dim1();
	size_t rhs_c = rhs.dim2();

	// get this matrix info and set up memory for results
	size_t lhs_r = dim1();
	size_t lhs_c = dim2();

    // determine dimensions of result
    size_t res_r, res_c;
	if(lhs_r == 1 && lhs_c == 1)
	{
		res_r = rhs_r;
		res_c = rhs_c;
	}
    else if(rhs_r == 1 && rhs_c == 1)
    {
		res_r = lhs_r;
		res_c = lhs_c;
	}
    else
    {
		res_r = lhs_r;
        res_c = rhs_c;
	}

    // calculate the result
	Basic_Matrix<T> result(0.0, res_r, res_c);
	if(lhs_r == 1 && lhs_c == 1)
	    // LHS is a scalar
		// do the matrix multiplication and store in result
        result = v[0]*rhs;
    else if(rhs_r == 1 && rhs_c == 1)
    	// RHS is a scalar
		// do the matrix multiplication and store in result
        result = (*this)*rhs.v[0];
    else
    {
    	// both matricies are non-scalar
		// ensure matricies are compatable
		if(lhs_c != rhs_r)
            throw std::runtime_error("Dimension mismatch in matrix * matrix operation");

		// do the matrix multiplication and store in result
		for(size_t i = 0;  i < lhs_r; ++i)
			for(size_t j = 0; j < rhs_c; ++j)
			{
				for(size_t k = 0; k < lhs_c; ++k)
					result[i][j] += (*this)[i][k]*rhs[k][j];
			}
	}

	return *this=result;
}
//---------------------------------------------------------------------------

// Unary operators

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::operator+()
{
    v = +v;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::operator-()
{
    v = -v;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::operator~()
{
    v = ~v;
    return *this;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::operator!()
{
    v = !v;
    return *this;
}
//---------------------------------------------------------------------------

// other operations

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::transpose() const
{
    Basic_Matrix<T> result(dim2(), dim1());

    for (size_t i = 0; i < dim1(); ++i)
        for (size_t j = 0; j < dim2(); j++)
            result[j][i] = (*this)[i][j];

    return result;
}
//---------------------------------------------------------------------------

export template<typename T>
Basic_Matrix<T> Basic_Matrix<T>::inverse() const
{
    // get this matrix info
	size_t r = dim1();
	size_t c = dim2();

    // look for non-sqare matrix
    if(r != c)
        throw std::runtime_error("Cannot get determinant of a non-square matrix");

    Basic_Matrix<T> result(r, c);

    if(r == 1)
    {
        // deal with 1x1 matrix
        if(v[0] == 0)
            result[0][0] = std::numeric_limits<T>::max();
        else
            result[0][0] = 1.0/v[0];
    }
    else if(r == 2)
    {
        // deal with 2x2 matrix
        double det = determinant();

        double invDet;
        if(det == 0)
            invDet = std::numeric_limits<T>::max();
        else
            invDet = 1.0/det;

        result[0][0] = invDet*(*this)[1][1];
        result[0][1] = -invDet*(*this)[0][1];
        result[1][0] = -invDet*(*this)[1][0];
        result[1][1] = invDet*(*this)[0][0];
    }
    else
    {
        // deal with larger than 2x2 matricies
        Basic_Matrix<T> m11(2,2);
        Basic_Matrix<T> m12(2,c-2);
        Basic_Matrix<T> m21(r-2,2);
        Basic_Matrix<T> m22(r-2,c-2);
        Basic_Matrix<T> t11(2,2);
        Basic_Matrix<T> t12(2,c-2);
        Basic_Matrix<T> t21(r-2,2);
        Basic_Matrix<T> t22(r-2,c-2);

        // break up this matrix into component matricies
        size_t i, j; // MSVC Compat
        for(i = 0; i < r; ++i)
            for(j = 0; j < c; ++j)
            {
                if(i<2 && j<2) // do m11
                    m11[i][j] = (*this)[i][j];
                if(i<2 && j>1) // do m12
                    m12[i][j-2] = (*this)[i][j];
                if(i>1 && j<2) // do m21
                    m21[i-2][j] = (*this)[i][j];
                if(i>1 && j>1) // do m22
                    m22[i-2][j-2] = (*this)[i][j];
            }

        // do the inverse transformation
        t11 = m11.inverse();
        t22 = (m22-m21*t11*m12).inverse();
        t12 = t11*m12*t22*-1.0;
        t21 = t22*m21*t11*-1.0;
        t11 = t11 + t11*m12*t22*m21*t11;

        // construct the result from the components
        for(i = 0; i < r; ++i)
            for(j = 0; j < c; ++j)
            {
                if(i<2 && j<2) // do t11
                    result[i][j] = t11[i][j];
                if(i<2 && j>1) // do t12
                    result[i][j] = t12[i][j-2];
                if(i>1 && j<2) // do t21
                    result[i][j] = t21[i-2][j];
                if(i>1 && j>1) // do t22
                    result[i][j] = t22[i-2][j-2];
            }
    }

    return result;
}
//---------------------------------------------------------------------------

export template<typename T>
T Basic_Matrix<T>::determinant() const
{
    // get this matrix info
	size_t r = dim1();
	size_t c = dim2();

    // look for non-sqare matrix
    if(r != c)
        throw std::runtime_error("Cannot get determinant of a non-square matrix");

    if(r == 1)
    {
        // deal with 1x1 matrix
        return (*this)[0][0];
    }
    else if(r == 2)
    {
        // deal with 2x2 matrix
        return (*this)[0][0]*(*this)[1][1] - (*this)[0][1]*(*this)[1][0];
    }
    else if(r == 3)
    {
        // deal with 3x3 matrix
        return
            (*this)[0][0]*(*this)[1][1]*(*this)[2][2]
           -(*this)[0][0]*(*this)[2][1]*(*this)[1][2]
           +(*this)[1][0]*(*this)[2][1]*(*this)[0][2]
           -(*this)[1][0]*(*this)[0][1]*(*this)[2][2]
           +(*this)[2][0]*(*this)[0][1]*(*this)[1][2]
           -(*this)[2][0]*(*this)[1][1]*(*this)[0][2];
    }
    else
    {
        // deal with larger than 3x3 matricies
        tool::Basic_Matrix<T> m11(2,2);
        tool::Basic_Matrix<T> m12(2,c-2);
        tool::Basic_Matrix<T> m21(r-2,2);
        tool::Basic_Matrix<T> m22(r-2,c-2);

        // break up this matrix into component matricies
        for(size_t i = 0; i < r; ++i)
            for(size_t j = 0; j < c; ++j)
            {
                if(i<2 && j<2) // do m11
                    m11[i][j] = (*this)[i][j];
                if(i<2 && j>1) // do m12
                    m12[i][j-2] = (*this)[i][j];
                if(i>1 && j<2) // do m21
                    m21[i-2][j] = (*this)[i][j];
                if(i>1 && j>1) // do m22
                    m22[i-2][j-2] = (*this)[i][j];
            }

        return m11.determinant() * (m21*m11.inverse()*m12).determinant();
    }
}
//---------------------------------------------------------------------------


export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::setLeadingDiagonal(const T& val)
{
    size_t minDim = tool::min(d1, d2);
    for (size_t i = 0; i < minDim; ++i)
        (*this)[i][i] = val;

    return *this;
}
//---------------------------------------------------------------------------


// method supporting Cramer's rule

export template<typename T>
Basic_Matrix<T>& Basic_Matrix<T>::setcolumn(size_t i, const CSlice_iter<T>& col)
{
    if (dim2() != col.size())
        throw std::runtime_error("Dimension mismatch in setcolumn call on Matrix");

    for (size_t row = 0; row < dim2(); ++row)
        this->operator[](row)[i] = col[row];

    return *this;
}
//---------------------------------------------------------------------------

}; // namespace tool

#if (_MSC_VER <= 1200)
	// MSVC 6.0 & earlier
	#undef export
#endif

#endif // tool_matrixCPP

