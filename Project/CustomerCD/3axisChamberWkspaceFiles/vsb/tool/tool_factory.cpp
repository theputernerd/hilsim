//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.4 $
//  $RCSfile: tool_factory.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_factory.h/.cpp
      Template implementation of a standard Factory design pattern.   */
//---------------------------------------------------------------------------
#ifndef tool_factoryCPP
#define tool_factoryCPP

#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_factory.h"

#include "tool_functors.h"
#include <stdexcept>
#include <algorithm>

#pragma hdrstop
//---------------------------------------------------------------------------
namespace tool
{

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif
//---------------------------------------------------------------------------

export template<typename BaseClass>
CreatorBase<BaseClass>::~CreatorBase()
{}
//---------------------------------------------------------------------------


export template<typename BaseClass>
void Factory<BaseClass>::addCreator(tool::SmartPtr< CreatorBase<BaseClass> >& creator)
{
    d_creators[creator->getName()] = creator;
}
//---------------------------------------------------------------------------


export template<typename BaseClass>
BaseClass* Factory<BaseClass>::createClass(const std::string& name) const
{
    CreatorsMap::const_iterator creator = d_creators.find(name);
    if (creator != d_creators.end())
        return creator->second.GetPtr()->create();
    else
        return 0;
}
//---------------------------------------------------------------------------


export template<typename BaseClass>
std::list<std::string> Factory<BaseClass>::getClassList() const
{
    std::list<std::string> results;
    std::transform(
        d_creators.begin(), d_creators.end(),
        std::back_inserter(results), tool::ConstFirst());

    return results;
}
//---------------------------------------------------------------------------


////////////////////////////////////////////////
// One-constructor parameter variant
////////////////////////////////////////////////


export template<typename BaseClass, typename InitParam1Type>
CreatorBase1<BaseClass, InitParam1Type>::~CreatorBase1()
{}
//---------------------------------------------------------------------------


export template<typename BaseClass, typename InitParam1Type>
void Factory1<BaseClass, InitParam1Type>::addCreator(
        tool::SmartPtr< CreatorBase1<BaseClass, InitParam1Type> >& creator)
{
    d_creators[creator->getName()] = creator;
}
//---------------------------------------------------------------------------


export template<typename BaseClass, typename InitParam1Type>
BaseClass* Factory1<BaseClass, InitParam1Type>::createClass(
        const std::string& name, const InitParam1Type& in) const
{
    CreatorsMap::const_iterator creator = d_creators.find(name);
    if (creator != d_creators.end())
        return creator->second.GetPtr()->create(in);
    else
        return 0;
}
//---------------------------------------------------------------------------


export template<typename BaseClass, typename InitParam1Type>
std::list<std::string> Factory1<BaseClass, InitParam1Type>::getClassList() const
{
    std::list<std::string> results;
    std::transform(
        d_creators.begin(), d_creators.end(),
        std::back_inserter(results), tool::ConstFirst());

    return results;
}
//---------------------------------------------------------------------------


} // namespace tool
//---------------------------------------------------------------------------

// Test declaration
namespace {

#ifdef ENABLE_BUILTIN_TESTS
class TFredBase
{
  public:
    virtual ~TFredBase() {}
    virtual bool dummy() { return true; }
};
class TFredJunior : public TFredBase {};

class Tester
{
  public:
    Tester()
    {
        tool::Factory<TFredBase> fredFactory;
        fredFactory.addCreator(
            tool::SmartPtr< tool::CreatorBase<TFredBase> >(
                new tool::Creator<TFredJunior, TFredBase>("Junior")));

        TFredBase* basePtr = fredFactory.createClass("Junior");
        TFredJunior* ptr = dynamic_cast<TFredJunior*>(basePtr); // should succeed
        if (!ptr)
            throw std::runtime_error("Built-in test of tool::Factory<> failed.");

        std::list<std::string> list = fredFactory.getClassList();
    }
};
#ifdef _DEBUG
Tester test;
#endif // _DEBUG

// Test of one parameter alternate...

class TFredBase1
{
  public:
    virtual ~TFredBase1() {}
    virtual bool dummy() { return true; }
};
class TFredJunior1 : public TFredBase1
{
    int d_init;
  public:
    TFredJunior1(const int& init)
      : d_init(init) {}
};

class Tester1
{
  public:
    Tester1()
    {
        tool::Factory1<TFredBase1, int> fredFactory;
        fredFactory.addCreator(
            tool::SmartPtr< tool::CreatorBase1<TFredBase1, int> >(
                new tool::Creator1<TFredJunior1, TFredBase1, int>("Junior")));

        TFredBase1* basePtr = fredFactory.createClass("Junior", 456);
        TFredJunior1* ptr = dynamic_cast<TFredJunior1*>(basePtr); // should succeed
        if (!ptr)
            throw std::runtime_error("Built-in test of tool::Factory1<> failed.");

        std::list<std::string> list = fredFactory.getClassList();
    }
};
#ifdef _DEBUG
Tester1 test1;
#endif // _DEBUG
#endif // ENABLE_BUILTIN_TESTS

} // unnamed namespace
//---------------------------------------------------------------------------

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#undef export
#endif

#endif // tool_factoryCPP

