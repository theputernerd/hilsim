//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.6 $
//  $RCSfile: tool_atmos.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_atmos.h/.cpp
      Standard Earth Atmosphere properties.

    Use:
        To use an atmosphere model, immediately after including tool_atmos.h,
        use a typedef to select the atmosphere model you wish to use, eg:

            #include "tool_atmos.h"
            typedef tool::TAtmosphereUS76 TAtmosphere; // use US76 atmosphere

        Then call the methods directly. For example, to get temperature, use
            TAtmosphere::Temperature(altitude);
        where altitude is the number of positive metres above sea level

        Since all methods of these classes are static, and their constructors
        are not implemented, you can not instantiate the class.
*/
//---------------------------------------------------------------------------
#include "tool_atmos.h"

#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif
#ifndef __MATH_H
    #include <cmath>
    #define __MATH_H
#endif

using namespace std; // MSVC Compat

namespace tool {

////////////////////////////////////////////////////////////////////////////////
// ESDU77 Atmosphere model
////////////////////////////////////////////////////////////////////////////////

// Atmosphere is divided into 7 layers, results valid up to 80km
// H = base altitude
// T = base temperature
// L = temperature gradient
// P = base pressure
// J & K = constants for pressure computation

// Need to initialise the data members H,T,L,P,J,K
const double TAtmosphereESDU77::H[7]={    0.0,   11e3,   20e3,   32e3,    47e3,    51e3,    71e3};
const double TAtmosphereESDU77::T[7]={ 288.15, 216.65, 216.65, 228.65,  270.65,  270.65,  214.65};
const double TAtmosphereESDU77::bL[7]={-6.5e-3,    0.0, 1.0e-3, 2.8e-3,     0.0, -2.8e-3, -2.0e-3};
const double TAtmosphereESDU77::bP[7]={1.013250e5,2.263204e4,5.474879e3,8.680160e2, 1.109058e2,   66.938530,3.956392};
const double TAtmosphereESDU77::J[7]={-5.25580, 0.0, 34.16322, 12.20115, 0.0, -12.20115, -17.08161};
const double TAtmosphereESDU77::K[7]={ 0.0, 1.576885e-4, 0.0, 0.0, 1.262266e-4, 0.0, 0.0};

const double TAtmosphereESDU77::Po = 1.01325e5;    // Pressure at sea level (N/m^2)
const double TAtmosphereESDU77::Ro = 1.225;	      // Density at sea level (kg/m^3)
const double TAtmosphereESDU77::Rgas = 8.31432e3;	// Universal gas constant   N m/kmol K
const double TAtmosphereESDU77::M0 = 28.96442;     // Mean molecular mass      kg/kmol

int TAtmosphereESDU77::LookUpH(const double &h)
{
	int i=2;
	if (h < H[1])
		return 0;
	else if (h >= H[6])
		return 6;
	else
		while (h >= H[i]) i++;
	return (i-1);
}

double TAtmosphereESDU77::Temperature(const double &height)
{
	double h = limit(fabs(height),0.0,80000.0);
	int i = LookUpH(h);
	return (T[i] + bL[i]*(h-H[i]));
}

double TAtmosphereESDU77::Pressure(const double &height)
{
	double h = limit(fabs(height),0.0,80000.0);
	int i = LookUpH(h);
	double temp = T[i] + bL[i]*(h-H[i]);

	if (bL[i])
		return (pow(T[i]/temp, J[i]) * bP[i]);
	else
		return (exp((H[i]-h)*K[i]) * bP[i]);
}

double TAtmosphereESDU77::SpeedOfSound(const double &height)
{
	double temp = Temperature(height);
	return (sqrt(1.4*Rgas*temp/M0));
}

double TAtmosphereESDU77::Density(const double &height)
{
	double temp = Temperature(height);
	double press = Pressure(height);

	return ((press*M0)/(Rgas*temp));
}

////////////////////////////////////////////////////////////////////////////////
// US76 Atmosphere model
////////////////////////////////////////////////////////////////////////////////

// constants used in calculations
const double TAtmosphereUS76::ae = 6.37816e6;
const double TAtmosphereUS76::GE = 3.986005e14;
const double TAtmosphereUS76::gmr = 0.034163947;
const double TAtmosphereUS76::T0 = 288.15;

// tables used to perform calculations
const double TAtmosphereUS76::Height_Table[9]=
{ 0,11000,20000,32000,47000,51000,71000,84852,90000 };
const double TAtmosphereUS76::Temperature_Table[9]=
{ 288.15,216.65,216.65,228.65,270.65,270.65,214.65,186.946,186.946 };
const double TAtmosphereUS76::Pressure_Table[9]=
{ 1,0.2233611,0.05403295,8.5666784e-3,1.0945601e-3,6.6063531e-4,3.9046834e-5,3.68501e-6,0 };
const double TAtmosphereUS76::LapseRate_Table[9]=
{ -0.0065,0,0.001,0.0028,0,-0.0028,-0.002,0,0 };

// LookupHeight() - Calculates the geopotential from the height, 
// then uses that to lookup which table entries should be used for
// subsequent calculations, returning the index.
// Also returns (via dh), the difference between the geopotential
// and the height in the table at the returned index.
int TAtmosphereUS76::LookupHeight(const double &height, double &dh)
{
	// calculate geopotential
	double R = height + ae;
	double geopotential = height*ae/R;

	// find the index in the height table of the largest
	// value smaller than or equal to the geopotential
	int i;
	for (i=8; i>=0; i--)
	{
		if (Height_Table[i]<=geopotential)
		{
			dh = geopotential-Height_Table[i];
			return i;
		}
	}
	// less than first entry, return 0
	dh = geopotential-Height_Table[0];
	return 0;
}

double TAtmosphereUS76::Temperature(const double &height)
{
	double dh;
	int Index=LookupHeight(height,dh);
	double lri=LapseRate_Table[Index];

	if (lri==0.0)
		return Temperature_Table[Index];
	else
		return Temperature_Table[Index] + dh*(lri + 1e-6);
}

double TAtmosphereUS76::Pressure(const double &height)
{
	double dh;
	int Index=LookupHeight(height,dh);
	double lri=LapseRate_Table[Index];

	if (lri==0.0)
	{
		return 101325*Pressure_Table[Index]*exp(-gmr*dh/Temperature_Table[Index]);
	}
	else
	{
		double Temp = Temperature_Table[Index] + dh*(lri + 1e-6);
		return 101325*Pressure_Table[Index]*pow(Temperature_Table[Index]/Temp,gmr/(lri + 1e-6));
	}
}

double TAtmosphereUS76::SpeedOfSound(const double &height)
{
	return 20.047*sqrt(Temperature(height));
}

double TAtmosphereUS76::Density(const double &height)
{
	return 1.225*T0*Pressure(height)/(101325*Temperature(height));
}

double TAtmosphereUS76::Gravity(const double &height)
{
	double R = height + ae;
	return GE/(R*R);
}

}; // namespace tool

