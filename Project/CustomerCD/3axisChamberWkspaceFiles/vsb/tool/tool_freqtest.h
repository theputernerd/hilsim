//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_freqtest.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_freqtest.h/.cpp
        Utility class for calculating fundamental properties of a sine wave.
*/
//---------------------------------------------------------------------------
#ifndef tool_freqtestH
#define tool_freqtestH

#ifndef tool_arraytemplateH
    #include "tool_arraytemplate.h"
#endif
#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
//---------------------------------------------------------------------------

namespace tool {

class TFrequencyTest
{
  public:
	TFrequencyTest(const double& frequencyHz);

    // change settings
	void setFrequencyHz(const double& f) { d_sineFreq = f*tool::constants::TWO_PI; }
	void setAmplitudeRelativeAccuracy(const double& aa) { d_ampRelAcc=aa; }
	void setPhaseRelativeAccuracyDeg(const double& pha) { d_phaseRelAcc=pha; }
	void setBiasRelativeAccuracy(const double& ba) { d_biasRelAcc=ba; }

    // provide point for possible storage
	bool addDataPoint(const double& time, const double& val);

    // last estimates
	double getAmplitude() const {return d_estAmplitude;}
	double getPhaseDeg() const {return d_estPhaseDeg;}
	double getDCBias() const {return d_estBias;}

    // error info about last estimates
    bool isAccuracyReached() const { return d_stopFlag; }
    std::string getErrorString() const { return d_errorString; }

    // get cumulative stats of estimates
	double getAveAmplitude() const
    { if (d_count > 0)
        return d_sumAmplitude / d_count;
      else
        return d_estAmplitude;
    }
	double getAvePhaseDeg() const
    { if (d_count > 0)
        return d_sumPhaseDeg / d_count;
      else
        return d_estPhaseDeg;
    }
	double getAveDCBias() const
    { if(d_count > 0)
        return d_sumBias / d_count;
      else
        return d_estBias;
    }
    int getPointCount() const { return d_count; }

    // reset functions
	void reset();
    void resetStats();

  private:

	void estimateSineValues();

	bool d_stopFlag;
	int d_count;
	double d_sineFreq;
	double d_ampRelAcc;
	double d_phaseRelAcc;
	double d_biasRelAcc;
	double d_prevEstAmplitude;
	double d_prevEstPhaseDeg;
	double d_prevEstBias;
	double d_estAmplitude;
	double d_estPhaseDeg;
	double d_estBias;
	double d_sumAmplitude;
	double d_sumPhaseDeg;
	double d_sumBias;
    tool::TArrayTemplate<double, 3> d_times;
    tool::TArrayTemplate<double, 3> d_values;

    std::string d_errorString;
};
//---------------------------------------------------------------------------

} // namespace tool

#endif
