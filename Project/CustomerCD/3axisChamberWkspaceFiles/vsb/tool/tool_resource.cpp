//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.2 $
//  $RCSfile: tool_resource.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_resource.h/.cpp

      Implements the "Resource" design pattern which performs reference
      counting on itself, deleting itself when the count reaches zero.
      The design pattern is for classes that require such behaviour to
      inherit publicly from this one, so that the behaviour is inherited.
      Users of this reference counted class or its derived classes must
      call ref() and unRef() to allow the class to keep track of what is
      referencing it.

*/
//---------------------------------------------------------------------------
#include "tool_resource.h"
//---------------------------------------------------------------------------

namespace tool {

#ifdef TOOL_ENABLE_DELAYED_RESOURCE_DELETION
short Resource::m_countToDelete  = 0;
Resource *Resource::m_pRsrcToDelete[MaxEntries] = {0};
#endif

//---------------------------------------------------------------------------

Resource::~Resource()
 { assert(m_refcount == 0); }

//---------------------------------------------------------------------------
} // namespace tool
