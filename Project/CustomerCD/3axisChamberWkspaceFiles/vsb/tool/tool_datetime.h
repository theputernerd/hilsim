//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:50:25 $
//  $Revision: 1.1 $
//  $RCSfile: tool_datetime.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_datatime.h/.cpp

      Of the four or five "standard" date/time implementations available,
      none suit our purpose of being easy to use, convertable to a nicely
      formatted string and being compatible with Window's data/time, etc.

      This date/time implementation is a C++ wrapper around the standard C
      'tm' structure.
*/
//---------------------------------------------------------------------------
#ifndef tool_datetimeH
#define tool_datetimeH

#include <time.h>
#include <iosfwd>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


class TDateTime
{
  public:
    TDateTime();
    ~TDateTime();

    void setDateTime(const tm& in) { d_tm = in; d_millisec = 0; }
    // Is Daylight Savinds in Effect? (this option doesn't do much in this impl)
    void setDST(bool in) { d_tm.tm_isdst = in; }
    // Year from 0 AD
    void setYear(int yearAD) { d_tm.tm_year = yearAD - 1900; }
    // Month where Jan=1, Feb=2, etc
    void setMonth(int month) { d_tm.tm_mon = month - 1; }
    // Day of month (1..31)
    void setDay(int day) { d_tm.tm_mday = day; }
    // Hours as per 24 hour clock
    void setHour(int hour) { d_tm.tm_hour = hour; }
    // Minutes
    void setMinutes(int mins) { d_tm.tm_min = mins; }
    // Seconds
    void setSeconds(int secs) { d_tm.tm_sec = secs; }
    // Milliseconds
    void setMilliseconds(int millisec) { d_millisec = millisec; }

    void setDate(int year, int month, int day)
    { setYear(year); setMonth(month); setDay(day); }
    void setTime(int hour, int mins, int secs, int millisec = 0)
    { setHour(hour); setMinutes(mins);
      setSeconds(secs); setMilliseconds(millisec); }

    const tm& gettm() const { return d_tm; }
    bool getDST() const { return !!d_tm.tm_isdst; }
    int getYear() const { return d_tm.tm_year + 1900; }
    int getMonth() const { return d_tm.tm_mon + 1; }
    int getDay() const { return d_tm.tm_mday; }
    int getHour() const { return d_tm.tm_hour; }
    int getMinutes() const { return d_tm.tm_min; }
    int getSeconds() const { return d_tm.tm_sec; }
    int getMilliseconds() const { return d_millisec; }

    // get day of year (0..365)
    int getDayOfYear() { mktime(&d_tm); return d_tm.tm_yday; }
    // get day of week (0..6 where Sunday = 0)
    int getDayOfWeek() { mktime(&d_tm); return d_tm.tm_wday; }

    std::istream& readBin(std::istream& is);
    std::ostream& writeBin(std::ostream& os) const;

  private:

    tm d_tm;
    // For reference, the following are the fields of tm...
    //  int tm_sec;   /* Seconds */
    //  int tm_min;   /* Minutes */
    //  int tm_hour;  /* Hour (0--23) */
    //  int tm_mday;  /* Day of month (1--31) */
    //  int tm_mon;   /* Month (0--11) */
    //  int tm_year;  /* Year (calendar year minus 1900) */
    //  int tm_wday;  /* Weekday (0--6; Sunday = 0) */
    //  int tm_yday;  /* Day of year (0--365) */
    //  int tm_isdst; /* 0 if daylight savings time is not in effect) */

    int d_millisec;
};
//---------------------------------------------------------------------------

// Non-member operators

bool operator < (const TDateTime& lhs, const TDateTime& rhs);
//---------------------------------------------------------------------------
/*
inline std::istream& operator>>(std::istream& is, TDateTime& dt)
{ return dt.read(is); }
inline std::ostream& operator<<(std::ostream& os, const TDateTime& dt)
{ return dt.write(os); }
*/
//---------------------------------------------------------------------------

} // namespace tool

#endif
