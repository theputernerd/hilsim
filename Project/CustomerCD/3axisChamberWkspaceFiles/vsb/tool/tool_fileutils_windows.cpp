//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:56:43 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fileutils_windows.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fileutils_windows.cpp

      Class containing Windows-specific methods for tool_fileutils.h
*/
//---------------------------------------------------------------------------
#ifdef _MSC_VER
	#pragma warning (disable : 4786) // identifier truncated in debug info
#endif

#include "tool_fileutils.h"

#include "tool_strtools.h"
#include <stdexcept>
#include <windows.h>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


std::list<tool::TFileData> getFileDataList(
        const std::string& basePath,
        const std::string& searchPattern)
{
    if (basePath.size() + searchPattern.size() > MAX_PATH-1)
        throw std::runtime_error("Search path exceeds maximum path length");

    if (basePath.size() == 0)
        return std::list<tool::TFileData>();

    //attach the search string
    std::string searchPath;
    if (basePath[basePath.size()-1] == '\\')
    {
        if (searchPattern[0] != '\\')
            searchPath = basePath + searchPattern;
        else
            searchPath = basePath + (searchPattern.c_str()+1);
    }
    else
    {
        if (searchPattern[0] == '\\')
            searchPath = basePath + searchPattern;
        else
            searchPath = basePath + '\\' + searchPattern;
    }

    std::list<tool::TFileData> result;
    struct _WIN32_FIND_DATAA fileData;
    bool searchSuccessful = true;

    HANDLE handle = FindFirstFile(searchPath.c_str(), &fileData);
    if(handle == INVALID_HANDLE_VALUE)
        return result;  //no file found

    while (searchSuccessful)
    {
        //add the file data to the list
        if (strcmp(fileData.cFileName, ".") && strcmp(fileData.cFileName, ".."))
            result.push_back(fileData);
        searchSuccessful = !!FindNextFile(handle, &fileData);
    }

    FindClose(handle);

    return result;
}
//---------------------------------------------------------------------------


bool copyFileToLoc(const std::string& sourceFile, const std::string& destPath)
{
    return CopyFile(
        sourceFile.c_str(),
        (destPath + "/" + extractFileName(sourceFile)).c_str(),
        FALSE) != 0;
}
//---------------------------------------------------------------------------


} // namespace tool



