//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:34:36 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_integ2nd.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_integ2nd.h/.cpp
      Template implementation of a 2nd Order Digital Integrator.

          y       1       dt   (1 - z^-1)
         ---  =  ---  =  --- * ----------
          x       s       2    (1 + z^-1)

*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_integ2ndCPP
#define tool_digitalfilter_integ2ndCPP

#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_digitalfilter_integ2nd.h"

#include <cmath>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInteg2ndOrder<T>::reset(
            const double& time,
            const T& inValue,
            const T& outValue
            )
{
    d_inValue = inValue;
    d_inTime = time;
    d_lastInValue = d_inValue;
    d_lastInTime = d_inTime;

//    d_lastTimeStep = 0.0;

    d_outValue = outValue;
    d_outTime = time;
    d_lastOutValue = d_outValue;
    d_lastOutTime = d_outTime;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInteg2ndOrder<T>::inputValue(
			const double& time,
			const T& inValue
            )
{
    // time must not go backwards (can stand still though)
    if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Integ2ndOrder Filter: Time travelling backwards in filter.");

    // record the new input with its timestamp
    d_inTime = time;
    d_inValue = inValue;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInteg2ndOrder<T>::calculateOutput(const double& time)
{
    // time must not go backwards (can stand still though)
	if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Integ2ndOrder Filter: Time travelling backwards in filter.");

    // this filter can't push forward in time, so we can't calculate
    // an output at a time greater than the last input...
    if (time > d_inTime)
        throw std::logic_error("Integ2ndOrder Filter: Request to calculate output at time greater than last input");

    // are the previous outputs and inputs to be copied off or overwritten?
    if (time > d_outTime && d_inTime > d_outTime)
    {
        // filter has moved on in time, need to copy off the
        // previous input and output values
        d_lastInValue = d_inValue;
        d_lastInTime = d_inTime;
        d_lastOutValue = d_outValue;
        d_lastOutTime = d_outTime;
    }

    // do filter calculations for time of input, ignoring the time that
    // was passed to calculate call because it was early tested to be an
    // error if its not the same as d_inTime...
    double timestep = d_inTime - d_lastOutTime;

    // Other than for the very the first pass of the filter,
    // timestep will always be greater than zero, because the previous
    // input & output values are not updated until a new input is
    // received (as opposed to the 'current' output which is given a new value
    // every time calculate is called).  This ensures that previous
    // outputs and inputs are always kept in the past.

    if (timestep == 0.0)
        // first pass of the filter, don't change anything (because initial
        // values were set in the reset function)
        return;

    d_outValue =
        0.5 * timestep * (d_inValue + d_lastInValue) + d_lastOutValue;
    d_outTime = d_inTime;
}
//---------------------------------------------------------------------------


} // end namespace tool
//---------------------------------------------------------------------------

//#define ENABLE_BUILTIN_TESTS
#ifdef ENABLE_BUILTIN_TESTS

#include "tool_arraytemplate.h"
#include "../marsbase/mars_vector3.h"
// Test declaration
namespace {

void compileTestFunc()
{
    tool::DigitalFilterInteg2ndOrder<double> filter1;
    filter1.reset(0.0, 0.0, 0.0);
    filter1.inputValue(1.0, 3.0);
    filter1.calculateOutput(1.0);
    double val = filter1.outputValue();

    typedef tool::TArrayTemplate<double, 2> Vector2;
    tool::DigitalFilterInteg2ndOrder<Vector2> filter2;
    filter2.reset(
            0.0, Vector2(), Vector2());
    Vector2 inVal;
    inVal[0] = 3.0;
    inVal[1] = 4.0;
    filter2.inputValue(1.0, inVal);
    filter2.calculateOutput(1.0);
    Vector2 val2 = filter2.outputValue();

    tool::DigitalFilterInteg2ndOrder<Mars::TVector3> filter3;
    filter3.reset(
            0.0, Mars::TVector3(), Mars::TVector3());
    filter3.inputValue(1.0, Mars::TVector3(3.0, 4.0, 5.0));
    filter3.calculateOutput(1.0);
    Mars::TVector3 val3 = filter3.outputValue();
}


} // unnamed namespace
//---------------------------------------------------------------------------
#endif // ENABLE_BUILTIN_TESTS

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#undef export
#endif

#endif // tool_digitalfilter_integ2ndCPP
