//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_freqtest.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_freqtest.h/.cpp
        Utility class for calculating fundamental properties of a sine wave.
*/
//---------------------------------------------------------------------------
#include "tool_freqtest.h"

#include <cmath>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TFrequencyTest::TFrequencyTest(const double& frequencyHz)
{
	reset();
    setFrequencyHz(frequencyHz);
}
//---------------------------------------------------------------------------


void TFrequencyTest::reset()
{
	d_stopFlag = false;
	d_sineFreq = 0.0;
	d_ampRelAcc = 0.001;
	d_phaseRelAcc = 0.01;
	d_biasRelAcc = 0.001;
	d_prevEstAmplitude = 0.0;
	d_prevEstPhaseDeg = 0.0;
	d_prevEstBias = 0.0;
	d_estAmplitude = 0.0;
	d_estPhaseDeg = 0.0;
	d_estBias = 0.0;
	for (int i = 0; i <= 2; ++i)
	{
        d_times[i] = -1.0e100;
        d_values[i] = 0.0;
	}

    d_errorString.resize(0);

    resetStats();
}
//---------------------------------------------------------------------------


void TFrequencyTest::resetStats()
{
	d_count = 0;
    d_sumAmplitude = 0.0;
	d_sumPhaseDeg = 0.0;
	d_sumBias = 0.0;
}
//---------------------------------------------------------------------------
    

bool TFrequencyTest::addDataPoint(const double& t, const double& val)
{
	// Test for decreasing time
	if (t < d_times[2])
	{
        throw std::runtime_error("Frequency Test: Data point time is decreasing");
	}
	else if (t == d_times[2])
	{
		// overwrite existing value
		d_times[2] = val;

        if (d_times[0] > 0)
            this->estimateSineValues();
	}
	else if (t > d_times[2] + tool::constants::TWO_PI / (11.0 * d_sineFreq))
	{
        // Accept only 11 points per cycle to ensure reasonable spacing...

		//Shift previous data points in buffer
		d_times[0] = d_times[1];
		d_times[1] = d_times[2];
		d_times[2] = t;
		d_values[0] = d_values[1];
		d_values[1] = d_values[2];
		d_values[2] = val;


        if (d_times[0] > 0)
            this->estimateSineValues();
	}

	return d_stopFlag;
}
//---------------------------------------------------------------------------

void TFrequencyTest::estimateSineValues()
{
    using namespace std; // MSVC Compat

	d_prevEstAmplitude = d_estAmplitude;
	d_prevEstPhaseDeg = d_estPhaseDeg;
	d_prevEstBias = d_estBias;

    // Solve the equation "val = aSin(wt + phi) + b"
    // with three unknowns, so we need three points

	// Evaluate values from last 3 data points
	// val0 = aSin(wt0)Cos(phi) + aCos(wt0)Sin(phi) + b
	// val1 = aSin(wt1)Cos(phi) + aCos(wt1)Sin(phi) + b
	// val2 = aSin(wt2)Cos(phi) + aCos(wt2)Sin(phi) + b

	// Eliminate b
	double aphi2_0 = d_values[2] - d_values[0];
	double aphi2_1 = d_values[2] - d_values[1];
	// Eliminate aCos(phi)
	double wt0 = d_sineFreq*d_times[0];
	double wt1 = d_sineFreq*d_times[1];
	double wt2 = d_sineFreq*d_times[2];
	double sinwt2_0 = sin(wt2) - sin(wt0);
	double sinwt2_1 = sin(wt2) - sin(wt1);
	double coswt2_0 = cos(wt2) - cos(wt0);
	double coswt2_1 = cos(wt2) - cos(wt1);
	double asinphi =
        (sinwt2_0*aphi2_1  - sinwt2_1*aphi2_0) /
        (sinwt2_0*coswt2_1 - sinwt2_1*coswt2_0);
	// Eliminate aSin(phi)
	double acosphi =
        (coswt2_0*aphi2_1  - coswt2_1*aphi2_0) /
        (coswt2_0*sinwt2_1 - coswt2_1*sinwt2_0);
	// Eliminate a
	double phir = atan2(asinphi,acosphi);
	d_estPhaseDeg = phir*tool::constants::RAD_TO_DEG;
	// Find a
	d_estAmplitude = sqrt(asinphi*asinphi + acosphi*acosphi);
	// Find b
	d_estBias = d_values[2] - d_estAmplitude*sin(wt2 + phir);

    // record stats
    ++d_count;
    d_sumAmplitude += d_estAmplitude;
	d_sumPhaseDeg += d_estPhaseDeg;
	d_sumBias += d_estBias;

	// Check for data settling
	bool ampOK =
        fabs(d_estAmplitude - d_prevEstAmplitude) <=
        fabs(d_ampRelAcc * d_estAmplitude);
	bool phaseOK =
        fabs(d_estPhaseDeg - d_prevEstPhaseDeg) <=
        fabs(d_phaseRelAcc * d_estPhaseDeg);
	bool biasOK =
        fabs(d_estBias - d_prevEstBias) <=
        fabs(d_biasRelAcc * d_estAmplitude);

	if (ampOK && phaseOK && biasOK)
	{
		d_stopFlag = true;
        d_errorString.resize(0);
	}
    else
    {
        d_stopFlag = false;
        d_errorString = (ampOK ? "" : "Inaccurate:Amplitude;");
        d_errorString += (phaseOK ? "" : "Phase;");
        d_errorString += (biasOK ? "" : "Bias;");
    }
}
//---------------------------------------------------------------------------

} // namespace tool
//---------------------------------------------------------------------------

