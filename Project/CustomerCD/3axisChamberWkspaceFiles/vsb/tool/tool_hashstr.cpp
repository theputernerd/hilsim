//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.10 $
//  $RCSfile: tool_hashstr.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_hashstr.h/.cpp
        Implements a general purpose string hashing function.
*/
//---------------------------------------------------------------------------
#include "tool_hashstr.h"

#ifndef localeSTL
    #include <locale>
    #define localeSTL
#endif
#ifndef __STDLIB_H
	#include <cstdlib>
	#define __STDLIB_H
#endif
#ifndef tool_strtoolsH
    #include "tool_strtools.h"
#endif

#pragma hdrstop

namespace tool {

// Note:  hashPrime must be prime for best performance in a hastable
//        look up application
//
// Oprimal hash table size for a given hashPrime is at least 2*hashPrime-1
// where hashPrime is greater than or equal to the expected number of entries
// in the hash table;
//
// Some example candidate primes are;
//  1001, 1511, 1999, 2297

_PREFIXFUNC long HashString(const char* s, long hashPrime, int ignoreCase )
{
    using namespace std; // MSVC compat so other compilers can find toupper

	long    h = 0L;
	while (*s)
	{
// use the standard to upper, rather than the slower tool::ToUpper
//	  h = (h<<2) ^ ((ignoreCase)?*s++:ToUpper(*s++));
	  h = (h<<2) ^ ((ignoreCase) ? *s++ : toupper(*s++));
	}
   h = h & 0x7FFFFFFFL;
	return (h % hashPrime);
}

}; // namespace tool
