//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.9 $
//  $RCSfile: tool_slice_iter.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_slice_iter.h/.cpp

          Slice_iter<T>
          CSlice_iter<T>

      Iterator template class for slices of a valarray.  Used primarily to
      access elements of tool::Basic_Matrix<T>

      Original concept taken from Stroustrup, 'The C++ Programming Language'

      Author - Duncan Fletcher

*/
//---------------------------------------------------------------------------
#ifndef tool_slice_iterH
#define tool_slice_iterH

#ifndef valarraySTL
    #include <valarray>
    #define valarraySTL
#endif

namespace tool {

// forward declaration
template<class T> class CSlice_iter;


template<class T> class Slice_iter {
    std::valarray<T>* v;
    std::slice s;
    size_t curr; // index of current element

    T& ref(size_t i) const { return (*v)[s.start()+i*s.stride()]; }

    // CSlice_iter needs access to privates in order to create a copy of this
    friend class CSlice_iter<T>;

  public:
    Slice_iter(std::valarray<T>* vv, std::slice ss) :v(vv), s(ss), curr(0) {}

    size_t size() const { return s.size(); }

    Slice_iter end()
    {
        Slice_iter t = *this;
        t.curr = s.size()   // index of last-plus-one element
        return t;
    }

    Slice_iter& operator++() {curr++; return *this; }
    Slice_iter operator++(int) {Slice_iter t=*this; curr++; return t; }

    T& operator[] (size_t i) { return ref(curr=i); }
    T& operator*() { return ref(curr); }

    friend bool operator==(const Slice_iter<T>& p, const Slice_iter<T>&q);
    friend bool operator!=(const Slice_iter<T>& p, const Slice_iter<T>&q);
    friend bool operator<(const Slice_iter<T>& p, const Slice_iter<T>&q);
};
//---------------------------------------------------------------------------

// Non-member operators...

template<class T>
inline bool operator==(const Slice_iter<T>& p, const Slice_iter<T>&q)
{ return p.curr==q.curr && p.s.stride() == q.s.stride() && p.s.start()==q.s.start(); }

template<class T>
inline bool operator!=(const Slice_iter<T>& p, const Slice_iter<T>&q)
    { return !(p==q); }

template<class T>
inline bool operator<(const Slice_iter<T>& p, const Slice_iter<T>&q)
{ return p.curr<q.curr && p.s.stride() == q.s.stride() && p.s.start()==q.s.start(); }
//---------------------------------------------------------------------------


template<class T> class CSlice_iter {
    std::valarray<T>* v;
    std::slice s;
    mutable size_t curr; // index of current element

    const T& ref(size_t i) const { return (*v)[s.start()+i*s.stride()]; }
  public:
    CSlice_iter(const std::valarray<T>* vv, std::slice ss) :s(ss), curr(0)
        { v = const_cast<std::valarray<T>*>(vv); }
        // const_cast should be okay here because member function of the
        // template protect it.
    CSlice_iter(const Slice_iter<T>& si) :s(si.s), curr(si.curr)
        { v = const_cast<std::valarray<T>*>(si.v); }

    size_t size() const { return s.size(); }

    CSlice_iter end()
    {
        CSlice_iter t = *this;
        t.curr = s.size()   // index of last-plus-one element
        return t;
    }

    CSlice_iter& operator++() const {curr++; return *this; }
    CSlice_iter operator++(int) const {Slice_iter t=*this; curr++; return t; }

    const T& operator[] (size_t i) const { return ref(curr=i); }
    const T& operator*() const { return ref(curr); }

    friend bool operator==(const CSlice_iter<T>& p, const CSlice_iter<T>&q);
    friend bool operator!=(const CSlice_iter<T>& p, const CSlice_iter<T>&q);
    friend bool operator<(const CSlice_iter<T>& p, const CSlice_iter<T>&q);
};
//---------------------------------------------------------------------------

// Non-member operators...

template<class T>
inline bool operator==(const CSlice_iter<T>& p, const CSlice_iter<T>&q)
{ return p.curr==q.curr && p.s.stride() == q.s.stride() && p.s.start()==q.s.start(); }

template<class T>
inline bool operator!=(const CSlice_iter<T>& p, const CSlice_iter<T>&q)
{ return !(p==q); }

template<class T>
inline bool operator<(const CSlice_iter<T>& p, const CSlice_iter<T>&q)
{ return p.curr<q.curr && p.s.stride() == q.s.stride() && p.s.start()==q.s.start(); }
//---------------------------------------------------------------------------

} // namespace tool

//---------------------------------------------------------------------------
#endif
