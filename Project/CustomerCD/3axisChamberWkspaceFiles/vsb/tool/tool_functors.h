//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_functors.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_functors.h/.cpp
        Collection of useful generic funtion objects (functors).
*/
//---------------------------------------------------------------------------
#ifndef tool_functorsH
#define tool_functorsH

namespace tool {
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Access pair::first functor object
struct First
{
    template<typename T>
    T::first_type& operator()(T& pair) const
    {
        return pair.first;
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Const access pair::first functor object
struct ConstFirst
{
    template<typename T>
    const T::first_type& operator()(const T& pair) const
    {
        return pair.first;
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Access pair::second functor object
struct Second
{
    template<typename T>
    T::second_type& operator()(T& pair) const
    {
        return pair.second;
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Const access pair::second functor object
struct ConstSecond
{
    template<typename T>
    const T::second_type& operator()(const T& pair) const
    {
        return pair.second;
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Pointer deletion functor DeleteObject.
// Usage:
//   std::for_each(mycontainer.begin(), mycontainer.end(),
//          tool::DeleteObject());
//
struct DeleteObject
{
    template<typename T>
    void operator()(const T* ptr) const
    {
      if (!ptr)
        delete ptr;
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////////////////////////////////////
// Second-of-pair Pointer deletion functor DeleteSecondObject.
// Usage:
//   std::for_each(mycontainer.begin(), mycontainer.end(),
//          tool::DeleteObject());
//
struct DeleteSecondObject
{
    template<typename T>
    void operator()(const T& in) const
    {
      if (!in.second)
        delete in.second;
    }
};
//---------------------------------------------------------------------------

} // namespace tool

#endif