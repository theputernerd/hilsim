//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/24 04:32:30 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fpucontrol_windows.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fpucontrol_windows.cpp

      Provides a utility class to manipulate the FPU control word on x86 CPUs.

	  Note that this file contains the implementation of the tool::TFPUControl
	  class for Windows, however this is something of a generalization. It
	  contains assembly statements in getControlWord() and setControlWord()
	  which work under Visual C++ and Borland C++, but which may need to be
	  changed for other compilers under Windows, eg cygwin. Since these 
	  methods operate on the x86 FPU control word, they could also feasibly
	  be used on other non-Windows x86-based platforms, eg x86-Linux, 
	  x86-FreeBSD, NetWare, etc, possibly only requiring a change to the
	  assembly statements to be compatible with the specific compilers. Only
	  on a platform which doesn't run on an x86 CPU would this file actually
	  require significant changes to provide the same functionality.

          Original author - Saxon Druce, Block Software
*/
//---------------------------------------------------------------------------

// The bits of the x86 FPU control word are as follows:

//  0: Invalid operation exception
//  1: Denormalised operand exception
//  2: Divide by zero exception
//  3: Overflow exception
//  4: Underflow exception
//  5: Precision exception
//  6: Reserved
//  7: Reserved
//  8: Precision control
//  9: Precision control
// 10: Rounding control
// 11: Rounding control
// 12: Infinity control
// 13: Reserved
// 14: Reserved
// 15: Reserved

/*************************************************************************/
// Include files
/*************************************************************************/

#include "tool_fpucontrol.h"

#include <stdexcept>

/*************************************************************************/
// Local types
/*************************************************************************/

/*************************************************************************/
// Functions
/*************************************************************************/

namespace tool
{

/**************************************************************************
*
*	function:	TFPUControl::getControlWord()
*
*	desc:		Returns the current FPU control word.
*
*	notes:		
*
**************************************************************************/

//static short temp;

short TFPUControl::getControlWord()
{
	// Get the control word - note this assembly construct works on
	// Visual C++ and Borland C++, but may need to be changed for other
	// compilers.

	// Note that this variable is static because for some reason,
	// Borland C has trouble reading the value into a variable allocated
	// on the stack when compiled with release settings (works fine
	// with debug settings).

	static short controlWord;
	__asm
	{
		fstcw controlWord
	}

	// return it
	return controlWord;
}

/**************************************************************************
*
*	function:	TFPUControl::setControlWord()
*
*	desc:		Sets the FPU control word to the provided value.
*
*	notes:		
*
**************************************************************************/

void TFPUControl::setControlWord(short controlWord)
{
	// set the control word - note this assembly construct works on 
	// Visual C++ and Borland C++, but may need to be changed for other
	// compilers
	__asm
	{
		fldcw controlWord
	}
}

/**************************************************************************
*
*	function:	TFPUControl::getPrecisionMode()
*
*	desc:		Returns the current precision mode of the FPU.
*
*	notes:		
*
**************************************************************************/

TFPUControl::EPrecisionMode TFPUControl::getPrecisionMode()
{
	// get control word
	short controlWord = getControlWord();

	// extract precision mode bits
	controlWord >>= 8;
	controlWord &= 3;

	// return appropriate value
	switch (controlWord)
	{
		case 0:
			return eSingle;
		case 1:
			// this value is reserved
			throw std::runtime_error("Invalid precision mode");
		case 2:
			return eDouble;
		case 3:
			return eExtended;
		default:
			// this should never happen
			throw std::runtime_error("Invalid precision mode");
	}
}

/**************************************************************************
*
*	function:	TFPUControl::setPrecisionMode()
*
*	desc:		Sets the precision mode of the FPU to the provided value.
*
*	notes:		
*
**************************************************************************/

void TFPUControl::setPrecisionMode(EPrecisionMode precisionMode)
{
	// work out the precision mode bitfield from the desired setting
	int bitField;
	switch (precisionMode)
	{
		case eSingle:
			bitField = 0;
			break;
		case eDouble:
			bitField = 2 << 8;
			break;
		case eExtended:
			bitField = 3 << 8;
			break;
		default:
			throw std::runtime_error("Invalid precision mode");
	}

	// get current control word
	short controlWord = getControlWord();

	// clear precision bits
	controlWord &= ~(3 << 8);

	// set new desired bits
	controlWord |= bitField;

	// set new control word
	setControlWord(controlWord);
}

/**************************************************************************
*
*	function:	TFPUControl::getRoundingMode()
*
*	desc:		Returns the current rounding mode of the FPU.
*
*	notes:		
*
**************************************************************************/

TFPUControl::ERoundingMode TFPUControl::getRoundingMode()
{
	// get control word
	short controlWord = getControlWord();

	// extract rounding mode bits
	controlWord >>= 10;
	controlWord &= 3;

	// return appropriate value
	switch (controlWord)
	{
		case 0:
			return eNearest;
		case 1:
			return eDown;
		case 2:
			return eUp;
		case 3:
			return eTruncate;
		default:
			// this should never happen
			throw std::runtime_error("Invalid rounding mode");
	}
}

/**************************************************************************
*
*	function:	TFPUControl::setRoundingMode()
*
*	desc:		Sets the rounding mode of the FPU to the provided value.
*
*	notes:		
*
**************************************************************************/

void TFPUControl::setRoundingMode(ERoundingMode roundingMode)
{
	// work out the rounding mode bitfield from the desired setting
	int bitField;
	switch (roundingMode)
	{
		case eNearest:
			bitField = 0;
			break;
		case eDown:
			bitField = 1 << 10;
			break;
		case eUp:
			bitField = 2 << 10;
			break;
		case eTruncate:
			bitField = 3 << 10;
			break;
		default:
			throw std::runtime_error("Invalid rounding mode");
	}

	// get current control word
	short controlWord = getControlWord();

	// clear rounding bits
	controlWord &= ~(3 << 10);

	// set new desired bits
	controlWord |= bitField;

	// set new control word
	setControlWord(controlWord);
}

/**************************************************************************
*
*	function:	TFPUControl::getEnabledExceptions()
*
*	desc:		Returns which FPU exceptions are currently enabled, as a 
*				bitwise combination of EException values.
*
*	notes:		
*
**************************************************************************/

int TFPUControl::getEnabledExceptions()
{
	// get current control word
	short controlWord = getControlWord();

	// The bits in the control word are a *mask*, which means if a bit is
	// set, the exception is *disabled*. In contrast, the value returned 
	// by this function has a bit set when the exception is *enabled*. So,
	// invert the control word.
	controlWord = ~controlWord;

	// get just the bottom 6 bits (ie the exception ones)
	controlWord &= 0x3f;

	// return the result, since the values of the enums match the bits
	// in the control word
	return controlWord;
}

/**************************************************************************
*
*	function:	TFPUControl::setEnabledExceptions()
*
*	desc:		Sets which exceptions should be enabled, by providing a 
*				bitwise combination of EException values. Any exception 
*				set in the argument will be turned on, any exception 
*				clear will be turned off.
*
*	notes:		
*
**************************************************************************/

void TFPUControl::setEnabledExceptions(int exceptions)
{
	// The bits in the control word are a *mask*, which means if a bit is
	// set, the exception is *disabled*. In contrast, the value passed 
	// to this function has a bit set when the exception is *enabled*. So,
	// invert the desired setting to get the bitfield we want.
	int bitField = ~exceptions;

	// get just the bottom 6 bits (ie the exception ones)
	bitField &= 0x3f;

	// get the current control word
	short controlWord = getControlWord();

	// clear current exception bits
	controlWord &= ~0x3f;

	// set new desired bits
	controlWord |= bitField;

	// set new control word
	setControlWord(controlWord);
}

/*************************************************************************/

} // tool namespace

/*************************************************************************/

