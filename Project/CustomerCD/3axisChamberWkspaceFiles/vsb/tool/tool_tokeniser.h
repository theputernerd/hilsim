//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_tokeniser.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_tokeniser.h/.cpp

        A simple tokeniser, or Lexical Analyser to chop raw characters from
        a stream into tokens.

        Adapted from the class 'lexer' from the 2172 course, 1999,
        University of Ottawa

        Adapted by Duncan Fletcher

*/
//---------------------------------------------------------------------------
#ifndef tool_tokeniserH
#define tool_tokeniserH

#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef iosfwdSTL
    #include <iosfwd>
    #define iosfwdSTL
#endif
#ifndef stdexceptSTL
    #include <stdexcept>
    #define stdexceptSTL
#endif
//---------------------------------------------------------------------------

namespace tool {

// Token Types (Examples)
//    eTokenNumeric:    0, 1, 3.455
//    eTokenIdentifier: hello, _a, cow2, ___, _bimbo_
//    eTokenString:     "hello world", 'hello world' and
//                       `hello world`
//    eTokenSpecial:    +, -, <=, !, @, ...
//    eTokenEOF:        no token read yet, or end of file reached
//
enum ETokenType {
    eTokenNumeric,
    eTokenIdentifier,
    eTokenString,
    eTokenSpecial,
    eTokenWhitespace,
    eTokenEOF
    };

//! This error object is thrown whenever a tokeniser error occurs
class tokeniser_error : public std::runtime_error
{
  public:
     tokeniser_error(int lineNumber,const std::string& message);
     int getLineNumber() const { return d_lineNumber; }
     virtual const char* what() const throw();
  private:
     int d_lineNumber;   //!< line number error detected on
     std::string d_message;
};

/*! \brief simple Tokeniser to break up an ascii stream

  Token types

        o unsigned real numbers in decimal notation
          (like 0.5, 434.232, 3.14, NOT .5)

        o unsigned integers
          (like 2, 0, 122233)

        o identifiers, first letter alphabetic or _
         subsequent letters are alphanumreic or _
          (like a, a1, _a, _, __a, hello)

        o special tokens
           +,  -,  *,  /,  %
          &&, ||, !,
           &,  |,
           (,  ),  [,  ],  {,  }
           =, ==, <=, >=,  <,  >,
           .,  @,  \,  ~,  ?,  $, ;, ,, :

        o every character following # (sharp or hash) until the
          end of the line is treated as a comment and discarded

        o quoted character sequences may include spaces and
          punctuation characters, but they cannot span multiple
          lines (line "hello world", 'how are you?' `life sucks`)

        o (char*)0 is returned if the Enf Of File (EOF)
          condition occured

        o other characters or combinations of charactres throw
          a lexer exception

        o corrupt stream forces an exception thrown
*/
class TTokeniser
{
  public:
    //! '#' is the comment character, but general comments can be disabled
    TTokeniser(const bool& commentsAllowed);
    ~TTokeniser();

    /*! \brief Read the next token from the stream and return it.

        If a token was putBack, then that token is returned instead.
        On EOF an exception is thrown */
    std::string nextToken(std::istream& is);

    /*! \brief Same as basic nextToken, but throw an exception if the token
        read is not of the expected type.  Unlike above nextToken, this
        does not throw an exception on EOF if tokenType == eTokenEOF*/
    std::string nextToken(std::istream& is, const ETokenType& tokenType);

    /*! \brief Read the token, but rather than returning it, throw an
        exception if it is not the same as the given token.  Will throw
        an exception on EOF */
    void nextToken(std::istream& is, const std::string& testToken);

    /*! \brief Look at the token, but do not remove it from the stream.
        If the next token is EOF, "" will be returned.  Updates d_type
        to peeked token */
    std::string peekToken(std::istream& is);

    /*! \brief removes all whitespace from the head of the stream */
    void eatWhitespace(std::istream& is);

    /*! \brief skips all characters until a '\n' is reached */
    void skipLine(std::istream& is);

    /*! \brief Reads a possibly signed numeric value and throws an exception
        if it is not such a token. Can handle exponentials */
    double readNumeric(std::istream& is);

    //! return the type of the last read token
    ETokenType getType() const { return d_type; }

    //! return the last read token
    std::string getLastToken() const { return d_token; }

    /*! \brief Put the current token back so it is the next token returned
        rather than whatever is next in the stream */
    void putBack();

    //! Set the line number, useful if the stream read by the tokeniser changes
    void setLineNumber(const int& lineNumber) { d_lineno = lineNumber; };

    //! return the current line number
    int getLineNumber() const { return d_lineno; }

    //! true if the argument is an unsigned numeric (real or integer) - no exponents
    static bool isNumeric(const std::string& token);

  private:

    /*! \brief Performs the real read, returning "" if EOF is found */
    std::string getToken(std::istream& is);

    std::string d_token; //!< token buffer
    ETokenType d_type; //!< type of last read token

    int d_lineno;  //!< current line number

    mutable bool d_inBuffer;

    const bool d_commentsAllowed;

    // a TTokeniser should not be passed by value
    // nor it should be assigned
    TTokeniser& operator=(const TTokeniser&); // not implemented
    TTokeniser(const TTokeniser&); // not implemented
};
//---------------------------------------------------------------------------

} // namespace tool

#endif
