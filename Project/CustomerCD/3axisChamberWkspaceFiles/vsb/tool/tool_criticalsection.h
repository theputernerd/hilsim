//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:49:54 $
//  $Revision: 1.3 $
//  $RCSfile: tool_criticalsection.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*!  \file
     Interface declaration for tool::TCriticalSection and tool::TAcquire

     Currently these critical section classes are implemented for
     Microsoft Windows and also ACE (the Adaptive Communication Environment).

     Note that <windows.h> or <ace/synch.h> must be included before this
     header for correct operation.

     Note also that behaviour is undefined for the case where different
     libraries/sections of a project use different implementations of
     TCriticalSection (ie if some parts use windows, while others use ace).
*/
//---------------------------------------------------------------------------
/*! \class tool::TCriticalSection
       \brief Wrapper class for the critical section semaphore.  Needs to be
              aquired to get a lock.  Wrapped to provide easy initialisation
              and cleanup.
*/
//---------------------------------------------------------------------------
/*! \class tool::TAcquire
       \brief Class that aquires a critical section in construction and releases
              it when deleted/destroyed.  Wrapped to provide easy acquisition
              and release of a critical section.  Wrapped for both native
              Windows critical sections and ACE's own multi-platform wrapper of
              Thread_Mutex's.
*/
//---------------------------------------------------------------------------
#ifndef tool_criticalsectionH
#define tool_criticalsectionH

namespace tool {
//---------------------------------------------------------------------------

#if defined(ACE_HAS_THREADS)
// ACE version
class TCriticalSection
{
    //! ACE critical section object that we're wrapping
    ACE_Thread_Mutex cs;
  public:
    // ACE_Thread_Mutex takes care of its own initialisation and cleanup.

    //! Acquire lock and block if necessary
    void acquire() {cs.acquire();}
    //! Release lock
    void release() {cs.release();}
};
#elif defined (WINVER)
// Windows version
class TCriticalSection
{
    //! Windows critical section object that we're wrapping
    CRITICAL_SECTION cs;
  public:
    //! Initialise Window's critical section on construction of the class
    TCriticalSection()
        { InitializeCriticalSection(&cs); }
    //! Delete Window's critical section on destruction of the class
    ~TCriticalSection()
        { DeleteCriticalSection(&cs); }

    //! Acquire lock and block if necessary
    void acquire() {EnterCriticalSection(&cs);}
    //! Release lock
    void release() {LeaveCriticalSection(&cs);}

  private:
    TCriticalSection(const TCriticalSection& in); // not implemented
    TCriticalSection& operator =(const TCriticalSection& in); // not implemented
};
#else
// other "error" version
#ifndef SPECIALSUPPRESSDUETOCPP
#include <stdexcept>
class TCriticalSection
{
  public:
    void acquire()
    { throw std::logic_error("TCriticalSection used without first #including OS dependent files, or used with unsupported operating system"); }
    void release()
    { throw std::logic_error("TCriticalSection used without first #including OS dependent files, or used with unsupported operating system"); }
};
#endif
#endif
//---------------------------------------------------------------------------

#ifndef SPECIALSUPPRESSDUETOCPP
class TAcquire
{
    //! Stored pointer to the critical section we have acquired.
    TCriticalSection* cs;
  public:
    //! Acquire the critical section on instantiation of this class - Constructor will return when acquisition is successful.
    TAcquire(TCriticalSection* in)
        { cs = in;
          cs->acquire(); }
    //! Release the critical section automatically when the object is destroyed.
    ~TAcquire()
        { cs->release(); }

  private:
    TAcquire(const TAcquire& in); // not implemented
    TAcquire& operator=(const TAcquire& in); // not implemented

};
#endif
//---------------------------------------------------------------------------

} // namespace tool

//---------------------------------------------------------------------------
#endif
