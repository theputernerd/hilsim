//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.17 $
//  $RCSfile: tool_matrix.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_matrix.h/.cpp

          Basic_Matrix<T>
          Matrix    - typedef Basic_Matrix<double>

      2D matrix template providing some useful numeric operations.

      Original concept taken from Stroustrup, 'The C++ Programming Language'
          Note: bug fix in body of 'column' methods from text.

      inverse, determinant & matrix * matrix operations from Nick Luckman

      matrix * valarray operators not yet complete

      Author - Duncan Fletcher
*/
//---------------------------------------------------------------------------
#ifndef tool_matrixH
#define tool_matrixH

// just in case, undef...
#undef min
#undef max
#ifndef valarraySTL
    #include <valarray>
    #define valarraySTL
#endif
#ifndef tool_slice_iterH
    #include "tool_slice_iter.h"
#endif
//---------------------------------------------------------------------------

namespace tool {

template<typename T>
class Basic_Matrix
{
  public:
    //! \name Constructors
    //@{
    //! Since assignment operator will resize, this default constructor is acceptable.
    Basic_Matrix()
      : v(0), d1(0), d2(0) {}
    //! Copy Constructor
    Basic_Matrix(const Basic_Matrix& in)
      : v(in.v), d1(in.d1), d2(in.d2) {}
    //! Initial values of the rows x cols matrix are zero
    Basic_Matrix(size_t rows, size_t cols)
      : v(0.0, rows*cols), d1(rows), d2(cols) {}
    //! Initial values of the rows x cols matrix are filled with given value
    Basic_Matrix(const T& value, size_t rows, size_t cols)
      : v(value, rows*cols), d1(rows), d2(cols) {}
    /*! \brief Initial values of the rows x cols matrix are filled from
        array of values.  And access violation will likely occur if there are
        not rows x cols values in the array */
    Basic_Matrix(const T* value, size_t rows, size_t cols)
      : v(value, rows*cols), d1(rows), d2(cols) {}
    //@}

    //! \name Assignment Operators
    //@{
    //! Will do a proper resize if necessary
    Basic_Matrix<T>& operator=(const Basic_Matrix<T>& in);
    /*! \brief Will check that rows*cols == vin.size and throw an exception if
         that is not true */
    Basic_Matrix<T>& operator=(const std::valarray<T>& vin);
    //! Fills all elements of the matrix with the given value
    Basic_Matrix<T>& operator=(const T& x);
    //@}

    //! \name Dimension Query methods
    //@{
    //! Number of elements contained in the matrix
    size_t size() const { return d1*d2; }
    //! Number of rows in the matrix
    size_t dim1() const { return d1; }
    //! Number of columns in the matrix
    size_t dim2() const { return d2; }
    //@}

    //! Resize looses all data held in the matrix
    void resize(size_t rows, size_t cols)
        { v.resize(rows*cols, 0.0); d1 = rows; d2 = cols; }

    //! \name Accessor methods
    /*! Iterators can only be used for accessing and manipulating
        individual elements of the matrix.  Using these functions to
        copy a row or column for example will only copy the iterator,
        not the data */
    //@{
    Slice_iter<T> rowIter(size_t i);
    CSlice_iter<T> rowIter(size_t i) const;

    Slice_iter<T> colIter(size_t i);
    CSlice_iter<T> colIter(size_t i) const;

    //! Returns a row iterator, which can then be indexed to get a value.
    Slice_iter<T> operator[](size_t i) { return rowIter(i); }
    CSlice_iter<T> operator[](size_t i) const { return rowIter(i); }
    //@}

    //! \name Operator-Assign operations
    //@{
    Basic_Matrix<T>& operator*=(const T& val);
    Basic_Matrix<T>& operator/=(const T& val);
    Basic_Matrix<T>& operator%=(const T& val);
    Basic_Matrix<T>& operator+=(const T& val);
    Basic_Matrix<T>& operator-=(const T& val);
    Basic_Matrix<T>& operator^=(const T& val);
    Basic_Matrix<T>& operator&=(const T& val);
    Basic_Matrix<T>& operator|=(const T& val);
    Basic_Matrix<T>& operator<<=(const T& val);
    Basic_Matrix<T>& operator>>=(const T& val);
	Basic_Matrix<T>& operator+=(const Basic_Matrix<T>& rhs);
	Basic_Matrix<T>& operator-=(const Basic_Matrix<T>& rhs);
	Basic_Matrix<T>& operator*=(const Basic_Matrix<T>& rhs);
    //@}

    //! \name Unary operations
    //@{
    Basic_Matrix<T> operator+();
    Basic_Matrix<T> operator-();
    Basic_Matrix<T> operator~();
    Basic_Matrix<T> operator!();
    //@}

    //! \name Other operations
    //@{
    Basic_Matrix<T> transpose() const;
    //! Should successfully inverse any square matrix
    Basic_Matrix<T> inverse() const;
    //! Should successfully calculate the determinant of any square matrix
    T determinant() const;
    //@}

    //! Set leading-diagonal elements of this matrix to val and return *this
    Basic_Matrix<T>& setLeadingDiagonal(const T& val);

    //! Method to support using Cramer's rule
    Basic_Matrix<T>& setcolumn(size_t i, const CSlice_iter<T>& col);

    //! Underlying valarray
    const std::valarray<T>& array() const { return v; }

  private:
    std::valarray<T> v; //!< matrix's data
    size_t d1; //!< number of rows
    size_t d2; //!< number of columns
};
//---------------------------------------------------------------------------

// Non-member operators
template<typename T>
std::valarray<T> operator*(const Basic_Matrix<T>& m, const std::valarray<T>& v);

template<typename T>
inline Basic_Matrix<T> operator+(const Basic_Matrix<T>& mat, const T& scalar)
{   Basic_Matrix<T> r = mat;
    return r+=scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator-(const Basic_Matrix<T>& mat, const T& scalar)
{   Basic_Matrix<T> r = mat;
    return r-=scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator*(const Basic_Matrix<T>& mat, const T& scalar)
{   Basic_Matrix<T> r = mat;
    return r*=scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator*(const T& scalar, const Basic_Matrix<T>& mat)
{   Basic_Matrix<T> r = mat;
    return r*=scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator+(const T& scalar, const Basic_Matrix<T>& mat)
{   Basic_Matrix<T> r = mat;
    return r+=scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator-(const T& scalar, const Basic_Matrix<T>& mat)
{   Basic_Matrix<T> r = -mat;
    return r-= -scalar; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator+(const Basic_Matrix<T>& lhs, const Basic_Matrix<T>& rhs)
{   Basic_Matrix<T> r = lhs;
    return r+=rhs; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator-(const Basic_Matrix<T>& lhs, const Basic_Matrix<T>& rhs)
{   Basic_Matrix<T> r = lhs;
    return r-=rhs; }
//---------------------------------------------------------------------------

template<typename T>
inline Basic_Matrix<T> operator*(const Basic_Matrix<T>& lhs, const Basic_Matrix<T>& rhs)
{   Basic_Matrix<T> r = lhs;
    return r*=rhs; }
//---------------------------------------------------------------------------

// inline bodies of member functions
template<typename T>
inline Slice_iter<T> Basic_Matrix<T>::rowIter(size_t i)
    { return Slice_iter<T>(&v, std::slice(i, d2,d1)); }

template<typename T>
inline CSlice_iter<T> Basic_Matrix<T>::rowIter(size_t i) const
    { return CSlice_iter<T>(&v, std::slice(i, d2,d1)); }

template<typename T>
inline Slice_iter<T> Basic_Matrix<T>::colIter(size_t i)
    { return Slice_iter<T>(&v, std::slice(i*d1,d1,1)); }

template<typename T>
inline CSlice_iter<T> Basic_Matrix<T>::colIter(size_t i) const
    { return CSlice_iter<T>(&v, std::slice(i*d1,d1,1)); }
//---------------------------------------------------------------------------

//! Commonly used template instance
typedef Basic_Matrix<double> Matrix;

}; // namespace tool
//---------------------------------------------------------------------------

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_matrix.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_matrix.cpp"
    #endif
#endif

//---------------------------------------------------------------------------

#endif
