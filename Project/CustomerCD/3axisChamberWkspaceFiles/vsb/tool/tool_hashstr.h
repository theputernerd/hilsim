//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_hashstr.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_hashstr.h/.cpp
        Implements a general purpose string hashing function.
*/
//---------------------------------------------------------------------------
#ifndef tool_hashstrH
#define tool_hashstrH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace tool {

// Note:  hashPrime must be prime for best performance;
//
// Oprimal hash table size for a given hashPrime is at least 2*hashPrime-1
// where hashPrime is greater than or equal to the expected number of entries
// in the hash table;
//
// Some example candidate primes are;
//  1001, 1511, 1999, 2297

_PREFIXFUNC long HashString(const char* s, long hashPrime, int ignoreCase = 1);

}; // namespace tool

#endif


