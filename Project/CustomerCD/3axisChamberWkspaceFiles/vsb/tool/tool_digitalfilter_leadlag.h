//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:33:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_leadlag.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_leadlag.h/.cpp
      Template implementation of a Lead-Lag Digital Filter.

      General purpose lead/lag transfer function,
          y       1 + T1 s       where  T1 = lead_time
         ---  =   --------              T2 = lag_time
          x       1 + T2 s

*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_leadlagH
#define tool_digitalfilter_leadlagH
//---------------------------------------------------------------------------

namespace tool {

template <typename T>
class DigitalFilterLeadLag
{
  public:

    DigitalFilterLeadLag() { reset(0.0, T()); }
    ~DigitalFilterLeadLag() {}

    void reset(
            const double& time,
            const T& inValue
            );

    void setTimeConstants(
            const double& rLeadTime,
            const double& rLagTime
            );

    void inputValue(
            const double& time,
            const T& inValue
            );

    void calculateOutput(const double& time);

    const T& outputValue() const { return d_outValue; }

  private:

    double d_leadTime;
    double d_lagTime;
    double d_lastLeadTime;
    double d_lastLagTime;

    T d_inValue;
    double d_inTime;
    T d_lastInValue;
    double d_lastInTime;

    double d_lastTimeStep;

    T d_outValue;
    double d_outTime;
    T d_lastOutValue;
    double d_lastOutTime;

    double d_a;
    double d_b;
    double d_invGain; // gain is stored in inversed form for efficiency
};
//---------------------------------------------------------------------------

}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_leadlag.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_leadlag.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
