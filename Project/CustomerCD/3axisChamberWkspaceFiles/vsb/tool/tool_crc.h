//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.6 $
//  $RCSfile: tool_crc.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_crc.h/.cpp
        Calculate Cyclic Redundancy of a text string.
        Useful tool for determining if files have been modified.
*/
//---------------------------------------------------------------------------
#ifndef tool_crcH
#define tool_crcH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace tool {

class _PREFIXCLASS TCheckSum {
public:
   TCheckSum();
   void Clear();
   unsigned int Update(const char *buff);
   unsigned int Get();
   const char *GetStr();

private:
   unsigned int itsCRC;

};

// Generate a serial key based on a name and organisation string
_PREFIXCLASS const char *NameOrganisationSerialKey(const char *name, const char *organisation, const char *privateKey="");

// Generate a serial key based on a name and organisation string
_PREFIXCLASS const char *NameSerialKey(const char *name, const char *privateKey="");

}; // namespace tool

#endif
