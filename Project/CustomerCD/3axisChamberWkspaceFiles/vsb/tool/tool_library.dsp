# Microsoft Developer Studio Project File - Name="tool_library" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=tool_library - Win32 Debug And Profile
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "tool_library.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "tool_library.mak" CFG="tool_library - Win32 Debug And Profile"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "tool_library - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "tool_library - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "tool_library - Win32 Debug And Profile" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "tool_library - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
MTL=midl.exe
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GR /GX /O2 /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "USE_TOOL_WINDOWS_IMPL" /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "tool_library - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
MTL=midl.exe
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /Gi /GR /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "USE_TOOL_WINDOWS_IMPL" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "tool_library - Win32 Debug And Profile"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "tool_library___Win32_Debug_And_Profile"
# PROP BASE Intermediate_Dir "tool_library___Win32_Debug_And_Profile"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug_And_Profile"
# PROP Intermediate_Dir "Debug_And_Profile"
# PROP Target_Dir ""
MTL=midl.exe
# ADD BASE CPP /nologo /Zp16 /MDd /W3 /Gm /GR /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /Zp16 /MDd /W3 /Gm /GR /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "USE_TOOL_WINDOWS_IMPL" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "tool_library - Win32 Release"
# Name "tool_library - Win32 Debug"
# Name "tool_library - Win32 Debug And Profile"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\tool_arraytemplate.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_atmos.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_bincodec.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_cmconsole.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_compress.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_crc.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_criticalsection.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_datetime.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_ab.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_abg.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_derivative.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_integ2nd.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_inv2ndlag.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_leadlag.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_ratelimiter.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_factory.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_filedata.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_filetree.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_fileutils.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_fpucontrol.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_freqtest.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_functors.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_hashstr.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_lookuptable.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_mathtools.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_matrix.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_mimecodec.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_randnum.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_registry.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_resource.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_sequentiallogic.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_slice_iter.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_smartptr.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_strtools.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_table.cpp
# End Source File
# Begin Source File

SOURCE=.\tool_tokeniser.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\tool_arraytemplate.h
# End Source File
# Begin Source File

SOURCE=.\tool_atmos.h
# End Source File
# Begin Source File

SOURCE=.\tool_bincodec.h
# End Source File
# Begin Source File

SOURCE=.\tool_cmconsole.h
# End Source File
# Begin Source File

SOURCE=.\tool_compress.h
# End Source File
# Begin Source File

SOURCE=.\tool_crc.h
# End Source File
# Begin Source File

SOURCE=.\tool_criticalsection.h
# End Source File
# Begin Source File

SOURCE=.\tool_datetime.h
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_ab.h
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_abg.h
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_integ2nd.h
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_inv2ndlag.h
# End Source File
# Begin Source File

SOURCE=.\tool_digitalfilter_leadlag.h
# End Source File
# Begin Source File

SOURCE=.\tool_factory.h
# End Source File
# Begin Source File

SOURCE=.\tool_filedata.h
# End Source File
# Begin Source File

SOURCE=.\tool_filetree.h
# End Source File
# Begin Source File

SOURCE=.\tool_fileutils.h
# End Source File
# Begin Source File

SOURCE=.\tool_fpucontrol.h
# End Source File
# Begin Source File

SOURCE=.\tool_freqtest.h
# End Source File
# Begin Source File

SOURCE=.\tool_functors.h
# End Source File
# Begin Source File

SOURCE=.\tool_hashstr.h
# End Source File
# Begin Source File

SOURCE=.\tool_lookuptable.h
# End Source File
# Begin Source File

SOURCE=.\tool_mathtools.h
# End Source File
# Begin Source File

SOURCE=.\tool_matrix.h
# End Source File
# Begin Source File

SOURCE=.\tool_mimecodec.h
# End Source File
# Begin Source File

SOURCE=.\tool_prefixdec.h
# End Source File
# Begin Source File

SOURCE=.\tool_randnum.h
# End Source File
# Begin Source File

SOURCE=.\tool_registry.h
# End Source File
# Begin Source File

SOURCE=.\tool_resource.h
# End Source File
# Begin Source File

SOURCE=.\tool_sequentiallogic.h
# End Source File
# Begin Source File

SOURCE=.\tool_slice_iter.h
# End Source File
# Begin Source File

SOURCE=.\tool_smartptr.h
# End Source File
# Begin Source File

SOURCE=.\tool_strtools.h
# End Source File
# Begin Source File

SOURCE=.\tool_table.h
# End Source File
# Begin Source File

SOURCE=.\tool_tokeniser.h
# End Source File
# End Group
# End Target
# End Project
