//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_prefixdec.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_prefixdec.h

      Parser library's Class modifiers are necessary to allow one set of 
      sources for the creation of both Static and Dynamic Link libraries
*/
//---------------------------------------------------------------------------
#ifndef tool_prefixdecH
#define tool_prefixdecH

#ifdef _MSC_VER // Microsoft Visual C++ DLL pre-amble

#	if defined(DLL_PARSERTOOLS_EXPORTS)
#		define _PREFIXCLASS __declspec(dllexport)
#		define _PREFIXFUNC __declspec(dllexport)
#	elif defined(DLL_PARSERTOOLS_IMPORTS)
#		define _PREFIXCLASS __declspec(dllimport)
#		define _PREFIXFUNC __declspec(dllimport)
#	else
#		define _PREFIXCLASS 
#		define _PREFIXFUNC 
#	endif

#else  // Borland C++ DLL pre-amble
	
#	if defined(DLL_PARSERTOOLS_EXPORTS)
#		define _PREFIXCLASS __export
#		define _PREFIXFUNC __export
#	elif defined(DLL_PARSERTOOLS_IMPORTS)
#		define _PREFIXCLASS __import
#		define _PREFIXFUNC __import
#	else
#		define _PREFIXCLASS 
#		define _PREFIXFUNC 
#	endif

#endif

#endif


