//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_compress.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_compress.h/.cpp
        Implements a simple run length string compression system.  The idea
        is to minimize the cost in memory when we want to store a module's
        source code.  So this provides a method to easily compress the data
        and then store it in the compressed form.
*/
//---------------------------------------------------------------------------
#ifndef tool_compressH
#define tool_compressH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace tool {

class TCompressedLine;

class  _PREFIXCLASS TCompressBuffer
{
  public:
    TCompressBuffer();
    virtual ~TCompressBuffer();
    TCompressBuffer& operator =(TCompressBuffer& aBuffer);

    void        Clear();
    int         AddString(const char *str);
    void        ResetIterator();
    int         AdvanceIterator();
    const char *UncompressCurrLine();
    const char *UncompressLine(int index);
    int         CurrLineLength();
    int         LineCount() { return itsLineCount; }
    int         CharCount() { return itsTotalCharCount; }

  private:
    TCompressedLine *itsFirstLine, *itsLastLine, *itsLineIterator;
    int              itsLineCount;
    int              itsTotalCharCount;
};

_PREFIXFUNC int RLEncode(char *inp, char* outp);
_PREFIXFUNC int RLDecode(char *inp, char* outp);

}; // tool

#endif
