//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_bincodec.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_bincodec.h/.cpp

    Binary Coder-Decoder template for binary stream IO.  Allows complex types
    to be written and read without loss of accuracy.
    File MUST be opened in binary mode (see below)

    Usage example:

    // open file in binary mode
    std::ofstream oStore("Store.dst", std::ios_base::binary);

    TBinCodec<double> PosEX = 10.0;     // initialises BinCodec to a double, 10.0
    oStore.write(PosEX, PosEX.size());  // writes a char array representing
                                        // the double to the file.
     // or the new way
    double PosEX = 10.0;
    oStore << TBinCodec<double>(PosEX); // writes the data in a single line of source
     // or
    oStore << TBinIO_double(PosEX); // using the typedef defined down the bottom of this file

    oStore.close;

    std::ifstream iStore("Store.dst", std::ios_base::binary);
    iStore.read(PosEX, PosEX.size();   // reads a char array representing
                                       // a double from the file
    std::cout << static_cast<double>(PosEX);  // displays the double on the console.

    // or the new way
    double newPos = TBinCodec<double>(iStore); // reads the data in a single line of source
    // or
    newPos = TBinIO_double(iStore);  // using the typedef defined down the bottom of this file
    std::cout << newPos;
*/
//---------------------------------------------------------------------------
#ifndef tool_bincodecH
#define tool_bincodecH

#include <iostream>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

template <class T> class TBinCodec
{
    union u{
        T    data;
        char bytes[sizeof(T)];
    } d_bits;
  public:
    TBinCodec() {}
    TBinCodec(const T& in)
    {  d_bits.data = in; }
    TBinCodec(std::istream& is)
    { is.read((*this), size()); }

    const unsigned int size() const
        { return sizeof(T); }

    TBinCodec& operator =(const T& in)
    {   d_bits.data = in;
        return *this; }

    operator T() const
        { return d_bits.data; }

    const T& val() const
        { return d_bits.data; }

    operator char*()
        {return d_bits.bytes; }

    const char* bytes() const
        {return d_bits.bytes; }

};
//---------------------------------------------------------------------------

} // namespace tool

// Non-member operators

template <typename T>
inline std::ostream& operator << (std::ostream& os, const tool::TBinCodec<T>& v)
{
    os.write(v.bytes(), v.size());
    return os;
}

template <typename T>
inline std::istream& operator >> (std::istream& is, const tool::TBinCodec<T>& v)
{
    is.read(v, v.size());
    return is;
}
//---------------------------------------------------------------------------


namespace tool {

// some useful typedefs for binary I/O of built-in types.
typedef TBinCodec<int>      TBinIO_int;
typedef TBinCodec<double>   TBinIO_double;
typedef TBinCodec<char>     TBinIO_char;

} // namespace tool
//---------------------------------------------------------------------------


#endif