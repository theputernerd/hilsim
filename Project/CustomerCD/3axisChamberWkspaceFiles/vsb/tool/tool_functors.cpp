//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:05:12 $
//  $Revision: 1.1 $
//  $RCSfile: tool_functors.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_functors.h/.cpp
        Collection of useful generic funtion objects (functors).
*/
//---------------------------------------------------------------------------
#include "tool_functors.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool

