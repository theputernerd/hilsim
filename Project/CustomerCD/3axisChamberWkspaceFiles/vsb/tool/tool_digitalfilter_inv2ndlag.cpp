//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_inv2ndlag.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_inv2ndlag.h/.cpp
      Template implementation of an Inverse 2nd Order Lag Estimator.

      Estimates what input to a 2nd Order Lag filter would have been,
      given the 2nd order response.  Inverts the general purpose
      2nd Order Lag transfer function,

          y              w�          where  w = natural frequency
         ---  =   ----------------          d = damping ratio
          x        s� + 2dws + w�

      to get time-domain inverse function,

        x(t)  =  y''(t) + 2dw.y'(t) + w�.y(t)
                -----------------------------
                             w�

      Uses DigitalFilterDerivative to obtain necessary estimates of
      derivatives to evaluate the time domain inverse function.
*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_inv2ndlagCPP
#define tool_digitalfilter_inv2ndlagCPP

#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_digitalfilter_inv2ndlag.h"

#include <cmath>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInv2ndOrderLag<T>::reset(
            const double& time,
            const T& inValue
            )
{
    d_inValue = inValue;
    d_inTime = time;

    d_outValue = inValue;
    d_outTime = time;

    d_dot.reset(time, inValue);
    d_dotdot.reset(time, d_dot.outputValue());

    d_twoOmegaD = 0.0;
    d_omegaSq = 0.0;
    d_invOmegaSq = 0.0;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInv2ndOrderLag<T>::setParameters(
            const double& rNaturalFrequency,  // rad/s
            const double& rDamping
            )
{
    d_twoOmegaD = 2.0 * rNaturalFrequency * rDamping;
    d_omegaSq = tool::sq(rNaturalFrequency);
    if (d_omegaSq != 0.0)
        d_invOmegaSq = 1.0 / d_omegaSq;
    else
        d_invOmegaSq = 0.0;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInv2ndOrderLag<T>::inputValue(
			const double& time,
			const T& inValue
            )
{
    // time must not go backwards (can stand still though)
    if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Inv 2nd Order Lag: Time travelling backwards in filter.");

    // record the new input with its timestamp
    d_inValue = inValue;
    d_inTime = time;
    d_dot.inputValue(time, inValue);
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterInv2ndOrderLag<T>::calculateOutput(const double& time)
{
    // time must not go backwards (can stand still though)
	if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Inv 2nd Order Lag Filter: Time travelling backwards in filter.");

    // this filter can't push forward in time, so we can't calculate
    // an output at a time greater than the last input...
    if (time > d_inTime)
        throw std::logic_error("Inv 2nd Order Lag Filter: Request to calculate output at time greater than last input");

    // Calculate derivatives
    d_dot.calculateOutput(time);
    d_dotdot.inputValue(time, d_dot.outputValue());
    d_dotdot.calculateOutput(time);

    // Calculate output.
    d_outValue =
        d_invOmegaSq *
            (d_dotdot.outputValue() + d_twoOmegaD * d_dot.outputValue() +
             d_omegaSq * d_inValue);
}
//---------------------------------------------------------------------------


} // end namespace tool
//---------------------------------------------------------------------------

//#define ENABLE_BUILTIN_TESTS
#ifdef ENABLE_BUILTIN_TESTS

#include "tool_arraytemplate.h"
#include "../marsbase/mars_vector3.h"
// Test declaration
namespace {

void compileTestFunc()
{
    tool::DigitalFilterInv2ndOrderLag<double> filter1;
    filter1.reset(0.0, 0.0);
    filter1.setParameters(2.0, 2.0);
    filter1.inputValue(1.0, 3.0);
    filter1.calculateOutput(1.0);
    double val = filter1.outputValue();

    typedef tool::TArrayTemplate<double, 2> Vector2;
    tool::DigitalFilterInv2ndOrderLag<Vector2> filter2;
    filter2.reset(
            0.0, Vector2());
    filter2.setParameters(2.0, 2.0);
    Vector2 inVal;
    inVal[0] = 3.0;
    inVal[1] = 4.0;
    filter2.inputValue(1.0, inVal);
    filter2.calculateOutput(1.0);
    Vector2 val2 = filter2.outputValue();

    tool::DigitalFilterInv2ndOrderLag<Mars::TVector3> filter3;
    filter3.reset(
            0.0, Mars::TVector3());
    filter3.setParameters(2.0, 2.0);
    filter3.inputValue(1.0, Mars::TVector3(3.0, 4.0, 5.0));
    filter3.calculateOutput(1.0);
    Mars::TVector3 val3 = filter3.outputValue();
}


} // unnamed namespace
//---------------------------------------------------------------------------
#endif // ENABLE_BUILTIN_TESTS

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#undef export
#endif

#endif // tool_digitalfilter_leadlagCPP
