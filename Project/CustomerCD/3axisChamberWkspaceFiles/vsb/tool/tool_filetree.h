//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:57:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_filetree.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_filetree.h/.cpp

        Class implementing recursive data storage of a directory listing
*/
//---------------------------------------------------------------------------
#ifndef tool_filetreeH
#define tool_filetreeH

#ifndef tool_filedataH
    #include "tool_filedata.h"
#endif
#include <iosfwd>
#include <string>
#include <list>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

class TFileTree
{
  public:
    TFileTree(const std::string& dirPath);
    TFileTree(std::istream& is);
    TFileTree(const TFileTree& in);
    ~TFileTree();

    TFileTree& operator=(const TFileTree& in);

    // The DirPath should be the full absolute path for this folder
    std::string getDirPath() const { return d_dirPath; }
    // The Alias is alternate name for this folder - without the full path
    std::string getAlias() const { return d_alias; }
    // Get the basic data about this folder itself.
    const TFileData& getDirData() const { return d_dirData; }
    // Get the list of sub-directories of this directory
    const std::list<TFileTree>& getSubDirs() const;
    // Get the files that are in this directory
    const std::list<TFileData>& getFiles() const;

    // The DirPath should be the full absolute path for this folder
    void setDirPath(const std::string& dirPath);
    // The Alias is alternate name for this folder - without the full path
    void setAlias(const std::string& in) { d_alias = in; }
    // Calling setFilter resets whatever current filtering there is.  Filters
    // only files.
    void setFilter(const std::list<std::string>& filterList);
    // Clears the class out completely...
    void clear();

    // This filter setting is for this folder and is independent of the
    // file filter setFilter()
    bool isFiltered() const { return d_isFiltered; }
    void setFiltered(bool in) { d_isFiltered = in; }

    // Read and Write the current state of the class.  As yet there is
    // no neat 'refresh' system for the class to pick up changes to the file
    // system.
    std::istream& read(std::istream& is);
    std::ostream& write(std::ostream& os) const;

  private:

    std::string d_dirPath;
    std::string d_alias;
    bool d_isFiltered;

    TFileData d_dirData;

    // Due to MSVC's unusual parsing system, it won't let you have a list
    // of TFileTree objects declared in the TFileTree class (though it should
    // be valid to do so because other compilers allow it).  So anyway, we
    // have put the sub directories and files of this tree in an internal
    // delegate class...
    class Privates;
    Privates* d_this;
};
//---------------------------------------------------------------------------

// Non-member operators...

inline std::istream& operator>>(std::istream& is, TFileTree& ft)
{ return ft.read(is); }
inline std::ostream& operator<<(std::ostream& os, const TFileTree& ft)
{ return ft.write(os); }
//---------------------------------------------------------------------------


} // namespace tool
//---------------------------------------------------------------------------

#endif
