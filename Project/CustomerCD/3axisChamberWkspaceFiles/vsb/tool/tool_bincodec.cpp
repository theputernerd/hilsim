//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:48:54 $
//  $Revision: 1.1 $
//  $RCSfile: tool_bincodec.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_bincodec.h/.cpp

    Binary Coder-Decoder template for binary stream IO.  Allows complex types
    to be written and read without loss of accuracy.
*/
//---------------------------------------------------------------------------
#include "tool_bincodec.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool
