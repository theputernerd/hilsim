//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_slice_iter.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_slice_iter.h/.cpp

          Slice_iter<T>
          CSlice_iter<T>

      Iterator template class for slices of a valarray.  Used primarily to
      access elements of tool::Basic_Matrix<T>

      Original concept taken from Stroustrup, 'The C++ Programming Language'

      Author - Duncan Fletcher

*/
//---------------------------------------------------------------------------
#include "tool_slice_iter.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool

