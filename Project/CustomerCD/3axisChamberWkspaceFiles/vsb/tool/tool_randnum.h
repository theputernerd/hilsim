//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.4 $
//  $RCSfile: tool_randnum.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_randnum.h/.cpp

        Deterministic pseudo random number generator
*/
//---------------------------------------------------------------------------
#ifndef tool_randnumH
#define tool_randnumH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace tool {

class _PREFIXCLASS TRandNumGen
{
public:
	TRandNumGen();
	void Seed(long aSeed);
    void ClockSeed(); // attempt to get a slightly random seed using the clock

	long NextNum();

    double NextUniformNum(
        const double& lower = 0.0,
        const double& upper = 1.0
        );
    double NextNormalNum(
        const double& mean = 0.0,
        const double& stddev = 1.0
        );

private:
	long itsSeed;
};
//---------------------------------------------------------------------------

} // namespace tool

#endif
