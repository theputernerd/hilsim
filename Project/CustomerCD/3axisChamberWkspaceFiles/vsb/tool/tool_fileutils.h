//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:56:43 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fileutils.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fileutils.h/.cpp

      Collection of general filename utilities, plus some OS-dependent
      File System functions (file system searches, etc)

      **Note that a number of these functions are C++ adaptions of Borland's
          SysUtil functions - mainly the "extract" functions.

      The Windows implementation is activated by the conditional define
      USE_TOOL_WINDOWS_IMPL appearing in the project settings

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
#ifndef tool_fileutilsH
#define tool_fileutilsH

#ifndef tool_filedataH
    #include "tool_filedata.h"
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef listSTL
    #include <list>
    #define listSTL
#endif
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

// Filename extraction routines...
//   (many translated from Borland's VCL SysUtils library)
//   Trailing / or \ characters are removed from the paths.

std::string extractFileDrive(const std::string& FileName);
std::string extractFilePath(const std::string& FileName);
std::string extractFileName(const std::string& FileName);
std::string extractFileExt(const std::string& FileName);
std::string extractFilePathNoDrive(const std::string& Filename);

/* Convert a fully qualified path name into a relative path name.
   The DestName parameter specifies file name (including path) to
   be converted.  BaseName is the fully qualified name of the base
   directory to which the returned path name should be relative.
   BaseName may or may not include a file name, but it must include
   the final path delimiter.
*/
std::string extractRelativePath(
        const std::string& BaseName,
        const std::string& DestName);

// Wildcard match handling * and ? characters
bool wildcardMatch(const std::string& filename, const std::string& pattern);
//---------------------------------------------------------------------------

// Directory search/list routines

std::list<std::string> getFileList(
        const std::string& basePath,
        const std::string& searchPattern);

std::list<std::string> getFileList(
        const std::string& basePath,
        const std::string& searchPattern,
        const std::list<std::string>& excludeList);

std::list<tool::TFileData> getFileDataList(
        const std::string& basePath,
        const std::string& searchPattern);
//---------------------------------------------------------------------------

// file copy - assumes that destPath does not have a trailing '/' or '\'
bool copyFileToLoc(const std::string& sourceFile, const std::string& destPath);
//---------------------------------------------------------------------------

} // namespace tool

#endif
