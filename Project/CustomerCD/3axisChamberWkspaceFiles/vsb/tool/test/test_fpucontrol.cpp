/**************************************************************************
*
*	test_fpucontrol.cpp
*
*	Main source file for the FPU control test programme
*
*	Author: Saxon Druce, Block Software
*
*	Copyright (c) 2003
*
**************************************************************************/

/*************************************************************************/
// Include files
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdexcept>

#include "tool_fpucontrol.h"

/*************************************************************************/
// Defines
/*************************************************************************/

/*************************************************************************/
// Local enums
/*************************************************************************/

/*************************************************************************/
// Local types
/*************************************************************************/

/*************************************************************************/
// Static function prototypes
/*************************************************************************/

/*************************************************************************/
// Global variables
/*************************************************************************/

/*************************************************************************/
// Static variables
/*************************************************************************/

/*************************************************************************/
// Functions
/*************************************************************************/

/**************************************************************************
*
*	function:	main()
*
*	desc:		Programme entry point
*
*	notes:		
*
**************************************************************************/

void main(void)
{
	try
	{
		printf("\n");

		// get initial control word
		short initialControlWord = tool::TFPUControl::getControlWord();
		printf("Initial control word: %04x\n", initialControlWord);

		// set to 0x027f (Visual C default)
		tool::TFPUControl::setControlWord(0x027f);
		printf("VC default control word: %04x\n", tool::TFPUControl::getControlWord());
		if (tool::TFPUControl::getControlWord() != 0x027f)
			throw std::runtime_error("Unable to set FPU control word to VC default");

		// set to 0x1372 (Borland C default)
		tool::TFPUControl::setControlWord(0x1372);
		printf("BC default control word: %04x\n", tool::TFPUControl::getControlWord());
		if (tool::TFPUControl::getControlWord() != 0x1372)
			throw std::runtime_error("Unable to set FPU control word to BC default");

		// restore to original
		tool::TFPUControl::setControlWord(initialControlWord);
		printf("Restored control word: %04x\n", tool::TFPUControl::getControlWord());
		if (tool::TFPUControl::getControlWord() != initialControlWord)
			throw std::runtime_error("Unable to restore FPU control word to initial value");

		printf("\n");

		// default precision mode
		switch (tool::TFPUControl::getPrecisionMode())
		{
			case tool::TFPUControl::eSingle:
				printf("Default precision mode: single\n");
				break;
			case tool::TFPUControl::eDouble:
				printf("Default precision mode: double\n");
				break;
			case tool::TFPUControl::eExtended:
				printf("Default precision mode: extended\n");
				break;
			default:
				throw std::runtime_error("Invalid precision mode");
		}

		// set to single and check
		tool::TFPUControl::setPrecisionMode(tool::TFPUControl::eSingle);
		if (tool::TFPUControl::getPrecisionMode() != tool::TFPUControl::eSingle)
			throw std::runtime_error("Unable to set FPU into single precision mode");

		// set to double and check
		tool::TFPUControl::setPrecisionMode(tool::TFPUControl::eDouble);
		if (tool::TFPUControl::getPrecisionMode() != tool::TFPUControl::eDouble)
			throw std::runtime_error("Unable to set FPU into double precision mode");

		// set to extended and check
		tool::TFPUControl::setPrecisionMode(tool::TFPUControl::eExtended);
		if (tool::TFPUControl::getPrecisionMode() != tool::TFPUControl::eExtended)
			throw std::runtime_error("Unable to set FPU into extended precision mode");

		printf("\n");

		// default rounding mode
		switch (tool::TFPUControl::getRoundingMode())
		{
			case tool::TFPUControl::eNearest:
				printf("Default rounding mode: nearest\n");
				break;
			case tool::TFPUControl::eDown:
				printf("Default rounding mode: down\n");
				break;
			case tool::TFPUControl::eUp:
				printf("Default rounding mode: up\n");
				break;
			case tool::TFPUControl::eTruncate:
				printf("Default rounding mode: truncate\n");
				break;
			default:
				throw std::runtime_error("Invalid rounding mode");
		}

		// set to nearest and check
		tool::TFPUControl::setRoundingMode(tool::TFPUControl::eNearest);
		if (tool::TFPUControl::getRoundingMode() != tool::TFPUControl::eNearest)
			throw std::runtime_error("Unable to set FPU into round to nearest mode");

		// set to down and check
		tool::TFPUControl::setRoundingMode(tool::TFPUControl::eDown);
		if (tool::TFPUControl::getRoundingMode() != tool::TFPUControl::eDown)
			throw std::runtime_error("Unable to set FPU into round down mode");

		// set to up and check
		tool::TFPUControl::setRoundingMode(tool::TFPUControl::eUp);
		if (tool::TFPUControl::getRoundingMode() != tool::TFPUControl::eUp)
			throw std::runtime_error("Unable to set FPU into round up mode");

		// set to truncate and check
		tool::TFPUControl::setRoundingMode(tool::TFPUControl::eTruncate);
		if (tool::TFPUControl::getRoundingMode() != tool::TFPUControl::eTruncate)
			throw std::runtime_error("Unable to set FPU into truncate mode");

		printf("\n");

		// get default exceptions
		int defaultExceptions = tool::TFPUControl::getEnabledExceptions();
		// print out which ones are enabled
		printf("Default enabled exceptions:\n");
		if (defaultExceptions & tool::TFPUControl::eInvalidOperation)
			printf("  Invalid operation\n");
		if (defaultExceptions & tool::TFPUControl::eDenormalisedOperand)
			printf("  Denormalised operand\n");
		if (defaultExceptions & tool::TFPUControl::eDivideByZero)
			printf("  Divide by zero\n");
		if (defaultExceptions & tool::TFPUControl::eOverflow)
			printf("  Overflow\n");
		if (defaultExceptions & tool::TFPUControl::eUnderflow)
			printf("  Underflow\n");
		if (defaultExceptions & tool::TFPUControl::ePrecision)
			printf("  Precision\n");

		// set invalid operation exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::eInvalidOperation);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::eInvalidOperation)
			throw std::runtime_error("Unable to turn on invalid operation exception");

		// set denormalised operand exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::eDenormalisedOperand);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::eDenormalisedOperand)
			throw std::runtime_error("Unable to turn on denormalised operand exception");

		// set divide by zero exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::eDivideByZero);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::eDivideByZero)
			throw std::runtime_error("Unable to turn on divide by zero exception");

		// set overflow exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::eOverflow);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::eOverflow)
			throw std::runtime_error("Unable to turn on overflow exception");

		// set undeflow exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::eUnderflow);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::eUnderflow)
			throw std::runtime_error("Unable to turn on underflow exception");

		// set precision exception and check
		tool::TFPUControl::setEnabledExceptions(tool::TFPUControl::ePrecision);
		if (tool::TFPUControl::getEnabledExceptions() != tool::TFPUControl::ePrecision)
			throw std::runtime_error("Unable to turn on precision exception");

		printf("\nDone, no errors\n\n");
	}
	catch (std::runtime_error &e)
	{
		printf("\nAn exception occurred: %s\n\n", e.what());
	}
}

/*************************************************************************/
