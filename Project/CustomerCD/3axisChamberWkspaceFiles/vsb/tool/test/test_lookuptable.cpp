//---------------------------------------------------------------------------
#include "tool_lookuptable.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#pragma hdrstop

//---------------------------------------------------------------------------

#pragma argsused
int main(int argc, char* argv[])
{
    std::ifstream iFile("../sample_lookup_table.lota");

    try {
        tool::TLookupTable table(iFile, 4);

        std::cout << "Successful read\n";

        double Y;
        table.getY(1.0)(5.0)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        table.getY(1.1)(5.0)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        table.getY(1.0)(5.5)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        table.getY(1.0)(5.0)(8.0)(10.5).into(Y, false);
        std::cout << Y << '\n';
        table.getY(4.0)(7.0)(9.0)(11.0).into(Y, false);
        std::cout << Y << '\n';
        std::cout << '\n';
        table.getY(1.0)(89.0)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        table.getY(1.0)(-89.0)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        table.getY(1.0)(-5.0)(8.0)(10.0).into(Y, false);
        std::cout << Y << '\n';
        std::cout << '\n';

        std::cout << table.getY(10.0)(5.0)(8.0)(10.0).into(Y, false) << "  ";
        std::cout << Y << '\n';
        std::cout << table.getY(10.0)(5.0)(8.0)(10.0).into(Y, true) << "  ";
        std::cout << Y << '\n';

        std::ofstream oFile("test_output.lota");
        oFile << table;
        oFile.close();

        // The following test should throw an exception
//        std::vector<std::string> names(4, "test");
//        names[1] = "6fred";
//        table.setBreakpointNames(names);

        // The following test should throw an exception
//        table.setTableName("fr/ed");
    }
    catch (std::exception& e)
    {
        std::cout << "Error: " << e.what() << '\n';
    }

    char c;
    std::cin >> c;

    return 0;
}
//---------------------------------------------------------------------------
