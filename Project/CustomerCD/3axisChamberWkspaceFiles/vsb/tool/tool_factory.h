//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_factory.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_factory.h/.cpp
      Template implementation of a standard Factory design pattern.

  Usage:

  NORMAL USE:

    tool::Factory<TFredBase> fredFactory;
    fredFactory.addCreator(
        tool::SmartPtr< tool::CreatorBase<TFredBase> >(
            new tool::Creator<TFredJunior, TFredBase>("Junior")));

    TFredBase* basePtr = fredFactory.createClass("Junior");
    TFredJunior* ptr = dynamic_cast<TFredJunior*>(basePtr); // should succeed

  AUTO-REGISTER USE:

    1. Create a single NON TEMPLATE function in CPP somewhere that looks
       like this...

        tool::Factory<TFredBase>& getStaticFredFactory()
        {
            static tool::Factory<TFredBase> fredFactory;
            return fredFactory;
        }

    2. Somewhere in every CPP file that declares a derived class, like fred
       junior, put this...

       namespace {
       tool::AutoFactoryRegister<TFredJunior, TFredBase>
            autoRegisterJunior(getStaticFredFactory(), "Junior");
       } // unnamed namespace


    ALTERNATE VERSION
        An alternate version of the factory, tool::Factory1, is provided
        along with alternates of the other necessary classes.  This is for
        use with classes that require a single parameter in their constructor.
        In other words, tool::Factory1<>::create takes both a name string and
        another parameter to be used in the construction of the subject class.
*/
//---------------------------------------------------------------------------
#ifndef tool_factoryH
#define tool_factoryH
//---------------------------------------------------------------------------

#include "tool_smartptr.h"
#include <string>
#include <map>
#include <list>
//---------------------------------------------------------------------------

namespace tool
{

template<typename BaseClass>
class CreatorBase
{
  public:
    CreatorBase(const std::string& name)
      : d_name(name) {}
    virtual ~CreatorBase();

    virtual BaseClass* create() const = 0;

    std::string getName() const { return d_name; }

  private:
    std::string d_name;
};
//---------------------------------------------------------------------------


template<typename T, typename BaseClass>
class Creator : public CreatorBase<BaseClass>
{
  public:
    Creator(const std::string& name)
      : CreatorBase<BaseClass>(name)
    {
        #if defined(_DEBUG) && defined(ENABLE_BUILTIN_TESTS)
        // test that cast will succeed
        T t;
        BaseClass* ptr = &t;
        #endif
    }
    virtual ~Creator() {};

    virtual BaseClass* create() const { return new T; }

};
//---------------------------------------------------------------------------


template<typename BaseClass>
class Factory
{
  public:
    Factory() {}
    ~Factory() {}

    void addCreator(tool::SmartPtr< CreatorBase<BaseClass> >& creator);

    BaseClass* createClass(const std::string& name) const;

    std::list<std::string> getClassList() const;

  private:

    typedef std::map<std::string, tool::SmartPtr< CreatorBase<BaseClass> > > CreatorsMap;
    CreatorsMap d_creators;

    Factory(const Factory& in); // not implemented
    Factory& operator =(const Factory& in); // not implemented
};
//---------------------------------------------------------------------------


template<typename T, typename BaseClass>
class AutoFactoryRegister
{
  public:
    AutoFactoryRegister(Factory<BaseClass>& factory, const std::string& name)
    {
        factory.addCreator(
            tool::SmartPtr< tool::CreatorBase<BaseClass> >(
                new tool::Creator<T, BaseClass>(name)));
    }
};
//---------------------------------------------------------------------------

////////////////////////////////////////////////
// One-constructor parameter variant
////////////////////////////////////////////////

template<typename BaseClass, typename InitParam1Type>
class CreatorBase1
{
  public:
    CreatorBase1(const std::string& name)
      : d_name(name) {}
    virtual ~CreatorBase1();

    virtual BaseClass* create(const InitParam1Type& in) const = 0;

    std::string getName() const { return d_name; }

  private:
    std::string d_name;
};
//---------------------------------------------------------------------------


template<typename T, typename BaseClass, typename InitParam1Type>
class Creator1 : public CreatorBase1<BaseClass, InitParam1Type>
{
  public:
    Creator1(const std::string& name)
      : CreatorBase1<BaseClass, InitParam1Type>(name)
    {
        #if defined(_DEBUG) && defined(ENABLE_BUILTIN_TESTS)
        // test that cast will succeed (ie T is a subclass of BaseClass)
        InitParam1Type temp;
        T t(temp);
        BaseClass* ptr = &t;
        #endif
    }
    virtual ~Creator1() {};

    virtual BaseClass* create(const InitParam1Type& in) const
        { return new T(in); }

};
//---------------------------------------------------------------------------


template<typename BaseClass, typename InitParam1Type>
class Factory1
{
  public:
    Factory1() {}
    ~Factory1() {}

    void addCreator(
            tool::SmartPtr< CreatorBase1<BaseClass, InitParam1Type> >& creator);

    BaseClass* createClass(
            const std::string& name, const InitParam1Type& in) const;

    std::list<std::string> getClassList() const;

  private:

    typedef std::map<
                std::string,
                tool::SmartPtr< CreatorBase1<BaseClass, InitParam1Type> >
                > CreatorsMap;
    CreatorsMap d_creators;

    Factory1(const Factory1& in); // not implemented
    Factory1& operator =(const Factory1& in); // not implemented
};
//---------------------------------------------------------------------------


template<typename T, typename BaseClass, typename InitParam1Type>
class AutoFactoryRegister1
{
  public:
    AutoFactoryRegister1(
            Factory1<BaseClass, InitParam1Type>& factory,
            const std::string& name)
    {
        factory.addCreator(
            tool::SmartPtr< tool::CreatorBase1<BaseClass, InitParam1Type> >(
                new tool::Creator1<T, BaseClass, InitParam1Type>(name)));
    }
};
//---------------------------------------------------------------------------


} // namespace tool
//---------------------------------------------------------------------------

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_factory.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_factory.cpp"
    #endif
#endif

//---------------------------------------------------------------------------

#endif
