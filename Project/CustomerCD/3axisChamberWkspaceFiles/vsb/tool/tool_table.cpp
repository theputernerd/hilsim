//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.9 $
//  $RCSfile: tool_table.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_table.h/.cpp

        Scalar, 1D, 2D, 3D or 4D Data tables for linear interpolation

        Independent variables are referred to as Xn variables (n=0,..,4)
        Dependent variable is referred to as Y

        This class has been superceded by tool_lookuptable.h/.cpp - do not
        use this class for any new projects.

*/
//---------------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_table.h"

// tool_mathtools contains binary search functions for data lookup
#include "tool_mathtools.h"

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

// Constructor & Destructor
TTableData::TTableData()
{
  mDimension = 0;
}
//---------------------------------------------------------------------------

// Clear data
void TTableData::Clear()
{
	for (int i=0; i<4; i++)
		mX[i].clear();

	mY.clear();

	mDimension = 0;
}
//---------------------------------------------------------------------------


// Set table dimensions - number of independent variables
// Minimum is 0, maximum is 4
void TTableData::SetDimensions(int dim)
{
	if (dim < 0 || dim > 4) return;

	mDimension = dim;

	// Clear higher dimension X lists
	for (int i=dim-1; i<4; i++)
		mX[i].clear();
}
//---------------------------------------------------------------------------

int TTableData::GetDimensions()
{
	return mDimension;
}
//---------------------------------------------------------------------------

// Set independent X variable data
// "dim" is the dimension number of the independent variable
// "dim" must be less than or equal to the table dimensions
bool TTableData::SetXData(int dim, const std::vector<double>& xData)
{
	if (dim < 1 || dim > mDimension)
		return false;
	else
	{
		mX[dim-1] = xData;
		return true;
	}
}
//---------------------------------------------------------------------------

// Set dependent Y variable data
// Y data must be sequentially ordered starting from highest X dimension
// eg. for three dimensions, with x1 size = l, x2 size = m, x3 size = n
//     x3_0, x2_0, x1_0,..,x1_l
//     x3_0, x2_1, x1_0,..,x1_l
//     ...
//     x3_0, x2_m, x1_0,..,x1_l
//     x3_1, x2_0, x1_0,...x1_l
//     ...
//     x3_n, x2_0, x1_0,..,x1_l
//     ...
//     x3_n, x2_m, x1_0,..,x1_l
// Y data size must be the product of the X dimension sizes
// Returns true if size is correct, otherwise false
bool TTableData::SetYData(const std::vector<double>& yData)
{
	// Check Y data size
	size_t xProduct=1;

	// Calculate product of X sizes
	for (int i=0; i<mDimension; i++)
		xProduct *= mX[i].size();

	if (yData.size() != xProduct) return false;

	mY = yData;

	return true;
}
//---------------------------------------------------------------------------

// Get size of table in X dimension
// "dim" is the dimension number of the independent variable
int TTableData::GetXSize(int dim)
{
	if (dim < 1 || dim > mDimension) return 0;
	else return mX[dim-1].size();
}
//---------------------------------------------------------------------------

// Check the size of the Y data is consistent with the dimension and
// sizes of the independent X variable arrays
// Y data size must be the product of the X dimension sizes
// Returns true if size is correct, otherwise false
bool TTableData::CheckYSize()
{
	size_t xProduct=1;

	// Calculate product of X sizes
	for (int i=0; i<mDimension; i++)
		xProduct *= mX[i].size();

	// return true if mY size is equal to product of X sizes
	return (mY.size() == xProduct);
}
//---------------------------------------------------------------------------

// Get independent variable value
// "dim" is the dimension number of the independent variable
// "dim" must be less than or equal to the table dimensions
// "index" is the zero-based index of the independent variable value
double TTableData::GetXValue(int dim, int index)
{
	if (dim < 1 || dim > mDimension) return 0;

	// limit index to X variable size
	index = limit(index,0,int(mX[dim-1].size()-1));
	return mX[dim-1][index];
}
//---------------------------------------------------------------------------

// Get dependent variable values
// "xn" is the index of the nth dimension
double TTableData::GetYValue(int x1, int x2, int x3, int x4)
{
	// limit indices to X variable sizes
	x1 = limit(x1,0,int(mX[0].size()-1));
	x2 = limit(x2,0,int(mX[1].size()-1));
	x3 = limit(x3,0,int(mX[2].size()-1));
	x4 = limit(x4,0,int(mX[3].size()-1));

	size_t yIndex = x1 + x2*mX[0].size() + x3*(mX[0].size()*mX[1].size()) + x4*(mX[0].size()*mX[1].size()*mX[2].size());

	if (yIndex < mY.size())
		return mY[yIndex];
	else return 0;
}
//---------------------------------------------------------------------------

// Check that independent variables are monotonically increasing
// Returns true if all X variables are increasing, otherwise false
bool TTableData::IsOrdered()
{
	// Nothing to do if table is zero dimension
	if (mDimension == 0) return true;

	// Check each X variable array
	for (int i=0; i<mDimension; i++)
	{
		// If only one element, nothing to check ,so start check from index=1
		for (size_t i=1; i<mX[i].size(); i++)
			if (mX[i][i] < mX[i][i-1]) return false;
	}

	return true;
}
//---------------------------------------------------------------------------

// Interpolate y data based on independent variable values
// Returns true if all x values are within table bounds
// Returns false if any x values are outside table bounds
// If x is outside table bounds, y data is extrapolated as
// constant boundary values
bool TTableData::Interpolate(double &yVal, double x1, double x2, double x3, double x4)
{
	// Nothing to do for zero dimension
	if (mDimension == 0)
	{
		if (mY.size()>0) yVal = mY[0];
		return true;
	}

	// X values
	double x[4] = {x1, x2, x3, x4};

	// Bounding breakpoints in X arrays
	int lo[4], hi[4];

	// Offsets between bounding breakpoints
	double d[4];

	// fInside stays true if all x values are within table bounds
	bool fInside = true;

	// Find break points in X variable arrays
	for (int i=0; i<mDimension; i++)
	{
		if (!binarySearch(x[i],mX[i],&lo[i],&hi[i],&d[i])) fInside = false;
	}

	// Perform linear interpolation
	switch (mDimension)
	{
		// One dimensional table
	case 1:	yVal = mY[lo[0]] + d[0]*(mY[hi[0]] - mY[lo[0]]);
					break;

		case 2:	// Two dimensional table
			{
				int size1 = mX[0].size();	// size of dimension 1

				int i1 = lo[1]*size1 + lo[0];
				int i2 = lo[1]*size1 + hi[0];
				double y1 = mY[i1] + d[0]*(mY[i2] - mY[i1]);

				int i3 = hi[1]*size1 + lo[0];
				int i4 = hi[1]*size1 + hi[0];
				double y2 = mY[i3] + d[0]*(mY[i4] - mY[i3]);

				yVal = y1 + d[1]*(y2 - y1);
				break;
			}

		case 3:	// Three dimensional table
			{
				int size1 = mX[0].size();
				int size12 = mX[0].size() * mX[1].size();

				int i1 = lo[2]*size12 + lo[1]*size1 + lo[0];
				int i2 = lo[2]*size12 + lo[1]*size1 + hi[0];
				double y1 = mY[i1] + d[0]*(mY[i2] - mY[i1]);

				int i3 = lo[2]*size12 + hi[1]*size1 + lo[0];
				int i4 = lo[2]*size12 + hi[1]*size1 + hi[0];
				double y2 = mY[i3] + d[0]*(mY[i4] - mY[i3]);

				double y12 = y1 + d[1]*(y2 - y1);

				int i5 = hi[2]*size12 + lo[1]*size1 + lo[0];
				int i6 = hi[2]*size12 + lo[1]*size1 + hi[0];
				double y3 = mY[i5] + d[0]*(mY[i6] - mY[i5]);

				int i7 = hi[2]*size12 + hi[1]*size1 + lo[0];
				int i8 = hi[2]*size12 + hi[1]*size1 + hi[0];
				double y4 = mY[i7] + d[0]*(mY[i8] - mY[i7]);

				double y34 = y3 + d[1]*(y4 - y3);

				yVal = y12 + d[2]*(y34 - y12);

				break;
			}


		case 4:	// Four dimensional table
			{
				int size1 = mX[0].size();
				int size12 = mX[0].size() * mX[1].size();
				int size123 = mX[0].size() * mX[1].size() * mX[2].size();

				int i1 = lo[3]*size123 + lo[2]*size12 + lo[1]*size1 + lo[0];
				int i2 = lo[3]*size123 + lo[2]*size12 + lo[1]*size1 + hi[0];
				double y1 = mY[i1] + d[0]*(mY[i2] - mY[i1]);

				int i3 = lo[3]*size123 + lo[2]*size12 + hi[1]*size1 + lo[0];
				int i4 = lo[3]*size123 + lo[2]*size12 + hi[1]*size1 + hi[0];
				double y2 = mY[i3] + d[0]*(mY[i4] - mY[i3]);

				double y12 = y1 + d[1]*(y2 - y1);

				int i5 = lo[3]*size123 + hi[2]*size12 + lo[1]*size1 + lo[0];
				int i6 = lo[3]*size123 + hi[2]*size12 + lo[1]*size1 + hi[0];
				double y3 = mY[i5] + d[0]*(mY[i6] - mY[i5]);

				int i7 = lo[3]*size123 + hi[2]*size12 + hi[1]*size1 + lo[0];
				int i8 = lo[3]*size123 + hi[2]*size12 + hi[1]*size1 + hi[0];
				double y4 = mY[i7] + d[0]*(mY[i8] - mY[i7]);

				double y34 = y3 + d[1]*(y4 - y3);

				double yLo = y12 + d[2]*(y34 - y12);


				i1 = hi[3]*size123 + lo[2]*size12 + lo[1]*size1 + lo[0];
				i2 = hi[3]*size123 + lo[2]*size12 + lo[1]*size1 + hi[0];
				y1 = mY[i1] + d[0]*(mY[i2] - mY[i1]);

				i3 = hi[3]*size123 + lo[2]*size12 + hi[1]*size1 + lo[0];
				i4 = hi[3]*size123 + lo[2]*size12 + hi[1]*size1 + hi[0];
				y2 = mY[i3] + d[0]*(mY[i4] - mY[i3]);

				y12 = y1 + d[1]*(y2 - y1);

				i5 = hi[3]*size123 + hi[2]*size12 + lo[1]*size1 + lo[0];
				i6 = hi[3]*size123 + hi[2]*size12 + lo[1]*size1 + hi[0];
				y3 = mY[i5] + d[0]*(mY[i6] - mY[i5]);

				i7 = hi[3]*size123 + hi[2]*size12 + hi[1]*size1 + lo[0];
				i8 = hi[3]*size123 + hi[2]*size12 + hi[1]*size1 + hi[0];
				y4 = mY[i7] + d[0]*(mY[i8] - mY[i7]);

				y34 = y3 + d[1]*(y4 - y3);

				double yHi = y12 + d[2]*(y34 - y12);

				yVal = yLo + d[2]*(yHi - yLo);

				break;
			}

		default: yVal = 0;	// should never occur
	}

	return fInside;
}
//---------------------------------------------------------------------------


// Invert table with respect to one of the independent variables
// Y data must be monotonically increasing with respect to the specified X variable
// "dim" is the dimension number of the independent variable
// ("dim" must be less than or equal to the table dimensions)
// Returns false if unable to invert table, otherwise true
// Currently only implemented for 1 and 2 dimension tables
bool TTableData::Invert(int dim)
{
	if (dim < 1 || dim > mDimension) return false;
	if (mDimension == 0) return false;

	// Ensure that X variable array is monotonically increasing

	// If only one element, nothing to check ,so start check from index=1
	for (size_t i=1; i<mX[dim-1].size(); i++)
		if (mX[dim-1][i] < mX[dim-1][i-1]) return false;


	switch(mDimension)
	{
		case 1:	// 1D table, simply swap X and Y data
			{
				std::vector<double> temp = mY;
				mY = mX[0];
				mX[0] = temp;

				// Check that new X data is monotonically increasing
				for (size_t i=1; i<mX[0].size(); i++)
					if (mX[0][i] < mX[0][i-1]) return false;

				return true;
			}

		case 2:
			{
				int xi = dim-1;	// index of the specified X dimension

				// Find Min and Max values of Y data
				double yMin = mY[0], yMax = mY[0];
				size_t i; // MSVC Compat
				for (i=1; i<mY.size(); i++)
				{
				  if (mY[i] < yMin) yMin = mY[i];
				  else if (mY[i] > yMax) yMax = mY[i];
				}

				// Calculate Y break points that will form new X array
				// Breakpoints are equally spaced between min and max Y values
				// The new X array will have the same number of breakpoints as the old one
				double delta = (yMax - yMin)/(mX[xi].size()-1);

				// New X array
				std::vector<double> newX(mX[xi].size());
				for (i=0; i<mX[xi].size(); i++)
				{
					double val = yMin + delta*i;
					newX[i] = val;
				}

				// Temp array to hold single row/column of old Y data
				std::vector<double> yTemp(mX[xi].size());

				int size1 = mX[0].size();
				int size2 = mX[1].size();

				if (dim == 1)
				{
					// For each X2 value, pull out a column of Y values
					for (int j=0; j<size2; j++)
					{
						// Construct temp y array from one row of y data for Binary search
						int i; // MSVC Compat
						for (i=0; i<size1; i++)
							yTemp[i] = mY[j*size1 + i];

						// Interpolate new Y values from old X array, using new X breakpoints
						// and extracted row of old Y data
						for (i=0; i<size1; i++)
						{
							int i1, i2;
							double d;
							binarySearch(newX[i], yTemp, &i1, &i2, &d);
							mY[j*size1 + i] = mX[0][i1] + d*(mX[0][i2] - mX[0][i1]);
						}
					}

					// Replace old X array with new X data
					mX[0] = newX;

					return true;
				}
				else	// dim == 2
				{
					// For each X1 value, pull out a row of Y values
					for (int i=0; i<size1; i++)
					{
						// Construct temp y array from one row of y data for Binary search
						int j; // MSVC Compat
						for (j=0; j<size2; j++)
							yTemp[j] = mY[j*size1 + i];

						// Interpolate new Y values from old X array, using new X breakpoints
						// and extracted row of old Y data
						for (j=0; j<size2; j++)
						{
							int j1, j2;
							double d;
							binarySearch(newX[j], yTemp, &j1, &j2, &d);
							mY[j*size1 + i] = mX[1][j1] + d*(mX[1][j2] - mX[1][j1]);
						}
					}

					// Replace old X array with new X data
					mX[1] = newX;

					return true;
				}
			}

		case 3:	// not implemented
			{
				return false;
			}

		case 4:	// not implemented
			{
				return false;
			}
	}

	return true;
}
//---------------------------------------------------------------------------


}; // namespace tool
