//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:52:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_filedata_windows.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_filedata_windows.h/.cpp

      Class containing Windows-specific methods for tool_filedata.h -
      specifically those that pertain to _WIN32_FIND_DATA
*/
//---------------------------------------------------------------------------
#include "tool_filedata.h"

#include <windows.h>

#pragma hdrstop
//---------------------------------------------------------------------------


//---------------------------------------------------------------------------


namespace tool {
//---------------------------------------------------------------------------


TFileData::TFileData(const _WIN32_FIND_DATAA& fileData)
{
    d_FileAttributes = convertWindowsFileAttributes(fileData.dwFileAttributes);

    SYSTEMTIME sysTime;
    FileTimeToSystemTime(&fileData.ftCreationTime, &sysTime);
    d_CreationTime = convertWindowSystemDateTime(sysTime);
    FileTimeToSystemTime(&fileData.ftLastAccessTime, &sysTime);
    d_LastAccessTime = convertWindowSystemDateTime(sysTime);
    FileTimeToSystemTime(&fileData.ftLastWriteTime, &sysTime);

    d_LastWriteTime = convertWindowSystemDateTime(sysTime);
    d_FileSize.LowPart = fileData.nFileSizeLow;
    d_FileSize.HighPart = fileData.nFileSizeHigh;
    d_FileName = fileData.cFileName;
    d_AlternateFileName = fileData.cAlternateFileName;
    d_isFiltered = false;
}
//---------------------------------------------------------------------------


TFileData& TFileData::operator=(const _WIN32_FIND_DATAA& fileData)
{
    d_FileAttributes = convertWindowsFileAttributes(fileData.dwFileAttributes);

    SYSTEMTIME sysTime;
    FileTimeToSystemTime(&fileData.ftCreationTime, &sysTime);
    d_CreationTime = convertWindowSystemDateTime(sysTime);
    FileTimeToSystemTime(&fileData.ftLastAccessTime, &sysTime);
    d_LastAccessTime = convertWindowSystemDateTime(sysTime);
    FileTimeToSystemTime(&fileData.ftLastWriteTime, &sysTime);
    d_LastWriteTime = convertWindowSystemDateTime(sysTime);

    d_FileSize.LowPart = fileData.nFileSizeLow;
    d_FileSize.HighPart = fileData.nFileSizeHigh;
    d_FileName = fileData.cFileName;
    d_AlternateFileName = fileData.cAlternateFileName;
    d_isFiltered = false;

    return *this;
}
//---------------------------------------------------------------------------


unsigned long TFileData::convertWindowsFileAttributes(unsigned long fileAttrs)
{
    unsigned long result = 0;
    if (fileAttrs & FILE_ATTRIBUTE_DIRECTORY)
        result += tool::TFileData::eFileAttributeDirectory;
    if (fileAttrs & FILE_ATTRIBUTE_READONLY)
        result += tool::TFileData::eFileAttributeReadOnly;
    if (fileAttrs & FILE_ATTRIBUTE_HIDDEN)
        result += tool::TFileData::eFileAttributeHidden;
    if (fileAttrs & FILE_ATTRIBUTE_SYSTEM)
        result += tool::TFileData::eFileAttributeSystem;
    if (fileAttrs & FILE_ATTRIBUTE_ARCHIVE)
        result += tool::TFileData::eFileAttributeArchive;
    if (fileAttrs & FILE_ATTRIBUTE_COMPRESSED)
        result += tool::TFileData::eFileAttributeCompressed;

    return result;
}
//---------------------------------------------------------------------------


TDateTime TFileData::convertWindowSystemDateTime(const _SYSTEMTIME& sysTime)
{
    TDateTime result;
    result.setDate(sysTime.wYear, sysTime.wMonth, sysTime.wDay);
    result.setTime(
        sysTime.wHour, sysTime.wMinute, sysTime.wSecond, sysTime.wMilliseconds);
    return result;
}
//---------------------------------------------------------------------------

} // namespace tool
