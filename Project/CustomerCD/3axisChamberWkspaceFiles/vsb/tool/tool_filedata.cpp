//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:52:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_filedata.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_filedata.h/.cpp

      Class containing file information, abstracted away from operating
      system specifics.

      Constructors and assignment operators exist to take _WIN32_FIND_DATA
      but there will only be an implementation for these if the conditional
      define USE_TOOL_WINDOWS_IMPL has been defined in the project settings.

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
/*
    This class's function bodies consists of two types - those that are
    OS independent, and those that require specific OS knowledge.
    The OS independent ones are defined here, while the others are
    in seperate CPP's that get #include'd according to conditional defines.
*/
//---------------------------------------------------------------------------
#include "tool_filedata.h"

#include "tool_bincodec.h"
#include <iostream>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

// Get OS-dependent functions...
#if defined(USE_TOOL_WINDOWS_IMPL)
    // currently Windows is the only OS implemented for this class
    #include "tool_filedata_windows.cpp"
#endif

namespace tool {
//---------------------------------------------------------------------------


TFileData::~TFileData()
{
}
//---------------------------------------------------------------------------


TFileData::TFileData(const TFileData& in)
{
    d_FileAttributes = in.d_FileAttributes;
    d_CreationTime = in.d_CreationTime;
    d_LastAccessTime = in.d_LastAccessTime;
    d_LastWriteTime = in.d_LastWriteTime;
    d_FileSize = in.d_FileSize;
    d_FileName = in.d_FileName;
    d_AlternateFileName = in.d_AlternateFileName;
    d_isFiltered = in.d_isFiltered;
}
//---------------------------------------------------------------------------


TFileData& TFileData::operator=(const TFileData& in)
{
    if (&in == this)
        return *this;

    d_FileAttributes = in.d_FileAttributes;
    d_CreationTime = in.d_CreationTime;
    d_LastAccessTime = in.d_LastAccessTime;
    d_LastWriteTime = in.d_LastWriteTime;
    d_FileSize = in.d_FileSize;
    d_FileName = in.d_FileName;
    d_AlternateFileName = in.d_AlternateFileName;
    d_isFiltered = in.d_isFiltered;

    return *this;
}
//---------------------------------------------------------------------------


std::istream& TFileData::read(std::istream& is)
{
    double version = TBinCodec<double>(is);

    if (version != 1.0 && version != 1.1)
        throw std::runtime_error("Unsupported version of File Data");

    d_FileAttributes = TBinCodec<unsigned long>(is);
    if (version <= 1.0)
    {
        // converting the date-time from 0.1 micro seconds since 1 Jan 1601 to
        // number of seconds since 1 Jan 1970 is just too tricky to contemplate!
        // Since nothing important has got data stored in version 1.0, we'll
        // just lose the date...
        LargeInteger lint;
        lint.LowPart = TBinCodec<unsigned long>(is);
        lint.HighPart = TBinCodec<unsigned long>(is);
        d_CreationTime = TDateTime();
        lint.LowPart = TBinCodec<unsigned long>(is);
        lint.HighPart = TBinCodec<unsigned long>(is);
        d_LastAccessTime = TDateTime();
        lint.LowPart = TBinCodec<unsigned long>(is);
        lint.HighPart = TBinCodec<unsigned long>(is);
        d_LastWriteTime = TDateTime();
    }
    else
    {
        d_CreationTime.readBin(is);
        d_LastAccessTime.readBin(is);
        d_LastWriteTime.readBin(is);
    }
    d_FileSize.LowPart = TBinCodec<unsigned long>(is);
    d_FileSize.HighPart = TBinCodec<long>(is);
    size_t size = TBinCodec<size_t>(is);
    char* buffer = new char[size+1];
    is.get(buffer, size);
    buffer[size] = '\0';
    d_FileName = buffer;
    size = TBinCodec<size_t>(is);
    is.get(buffer, size);
    buffer[size] = '\0';
    d_AlternateFileName = buffer;

    d_isFiltered = false;

    delete[] buffer;

    return is;
}
//---------------------------------------------------------------------------


std::ostream& TFileData::write(std::ostream& os) const
{
    os << TBinCodec<double>(1.1);

    os << TBinCodec<unsigned long>(d_FileAttributes);
    d_CreationTime.writeBin(os);
    d_LastAccessTime.writeBin(os);
    d_LastWriteTime.writeBin(os);
    os << TBinCodec<unsigned long>(d_FileSize.LowPart);
    os << TBinCodec<long>(d_FileSize.HighPart);
    os << TBinCodec<size_t>(d_FileName.size());
    os << d_FileName.c_str();
    os << TBinCodec<size_t>(d_AlternateFileName.size());
    os << d_AlternateFileName.c_str();

    // filtered state not written since it would probably need
    // to be re-evaluated anyway.

    return os;
}
//---------------------------------------------------------------------------


void TFileData::clear()
{
    d_FileAttributes = 0;
    d_CreationTime = TDateTime();
    d_LastAccessTime = TDateTime();
    d_LastWriteTime = TDateTime();
    d_FileSize.LowPart = 0;
    d_FileSize.HighPart = 0;
    d_FileName = ""; // MSVC Compat
    d_AlternateFileName = ""; // MSVC Compat
    d_isFiltered = false;
}
//---------------------------------------------------------------------------

} // namespace tool


