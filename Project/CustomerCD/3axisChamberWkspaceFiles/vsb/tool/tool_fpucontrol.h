//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/24 04:32:30 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fpucontrol.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fpucontrol.h

      Provides a utility class to manipulate the FPU control word on x86 CPUs.

          Original author - Saxon Druce, Block Software
*/
//---------------------------------------------------------------------------

#ifndef tool_fpucontrolH
#define tool_fpucontrolH

/*************************************************************************/
// Include files
/*************************************************************************/

/*************************************************************************/
// Public types
/*************************************************************************/

namespace tool
{

// TFPUControl class - provides functions for manipulating the FPU 
// control word. Since all methods are static and the constructor is 
// not implemented, you should call the methods directly instead of 
// instantiating the class.
class TFPUControl
{
public:

	// getControlWord() - Returns the current FPU control word.
	static short getControlWord();

	// setControlWord() - Sets the FPU control word to the provided
	// value.
	static void setControlWord(short controlWord);

	// possible FPU precision modes
	enum EPrecisionMode
	{
		eSingle,	// 24 bit mantissa, 32 bit total, like 'float'
		eDouble,	// 53 bit mantissa, 64 bit total, like 'double'
		eExtended	// 64 bit mantissa, 80 bit total
	};

	// getPrecisionMode() - Returns the current precision mode of the FPU.
	static EPrecisionMode getPrecisionMode();

	// setPrecisionMode() - Sets the precision mode of the FPU to the 
	// provided value.
	static void setPrecisionMode(EPrecisionMode precisionMode);

	// possible FPU rounding modes
	enum ERoundingMode
	{
		eNearest,	// nearest
		eDown,		// towards negative infinity
		eUp,		// towards positive infinity
		eTruncate	// towards zero
	};

	// getRoundingMode() - Returns the current rounding mode of the FPU.
	static ERoundingMode getRoundingMode();

	// setRoundingMode() - Sets the rounding mode of the FPU to the 
	// provided value.
	static void setRoundingMode(ERoundingMode roundingMode);

	// possible FPU exceptions
	enum EException
	{
		eInvalidOperation = 1,
		eDenormalisedOperand = 2,
		eDivideByZero = 4,
		eOverflow = 8,
		eUnderflow = 16,
		ePrecision = 32
	};

	// getEnabledExceptions() - Returns which FPU exceptions are currently
	// enabled, as a bitwise combination of EException values.
	static int getEnabledExceptions();

	// setEnabledExceptions() - Sets which exceptions should be enabled,
	// by providing a bitwise combination of EException values. Any
	// exception set in the argument will be turned on, any exception
	// clear will be turned off.
	static void setEnabledExceptions(int exceptions);

	// For example, to turn on divide by zero exceptions, and to turn off
	// all other exceptions, use:
	//
	//   setEnabledExceptions(eDivideByZero);
	//
	// To turn on divide by zero exceptions, and leave other exception
	// settings as they are, use:
	// 
	//   int exceptions = getEnabledExceptions();
	//   exceptions |= eDivideByZero;
	//   setEnabledExceptions(exceptions);
	//
	// The default exception setting for Visual C++ is to have all 
	// exceptions off, ie:
	//
	//   setEnabledExceptions(0);
	//
	// The default exception setting for Borland C++ is to have invalid
	// operation, divide by zero and overflow on, with all others off, ie:
	//
	//   setEnabledExceptions(eInvalidOperation | eDivideByZero | eOverflow);

private:

	// Constructor (not implemented)
	TFPUControl();
};

} // tool namespace

/*************************************************************************/

#endif // tool_fpucontrolH
