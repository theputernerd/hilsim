//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.2 $
//  $RCSfile: tool_arraytemplate.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_arraytemplate.h/.cpp
      Implements a template dimensioned array.                        */
//---------------------------------------------------------------------------
#include "tool_arraytemplate.h"

#ifdef __BORLANDC__
	#pragma hdrstop
#endif
