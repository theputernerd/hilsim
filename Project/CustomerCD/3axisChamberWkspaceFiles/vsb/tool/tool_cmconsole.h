//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_cmconsole.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_cmconsole.h/.cpp
      Implements a console base class and specific implementations of a
      command line console class and file console class                    */
//---------------------------------------------------------------------------
#ifndef tool_cmconsoleH
#define tool_cmconsoleH

#ifndef tool_prefixdecH
    #include "tool_prefixdec.h"
#endif

namespace tool {

////////////////////////////////////////////
//
// Console base class
//
////////////////////////////////////////////

class _PREFIXCLASS  TConsole
{
public:
  TConsole();
  virtual ~TConsole();
  virtual bool IsOpen();
  virtual void Flush();
  virtual void Reset();
  virtual void Settings(int streamId, int optId);

  void Print(char *format,...);

private:

  char *buff;
  int   buffLen;
  int   buffContentsLen;
protected:
  virtual void DisplayString(const char *s) = 0;
};

////////////////////////////////////////////
//
// Command Line console class
//
// Use with DOS/UNIX based command line applications
//  
////////////////////////////////////////////

class _PREFIXCLASS TCommandLineConsole : public TConsole
{
public:
	TCommandLineConsole():TConsole(){};
	bool IsOpen();
private:
	void DisplayString(const char *str);
};

////////////////////////////////////////////
//
// File console class
//
// Writes console output to file
//
////////////////////////////////////////////

class _PREFIXCLASS TFileConsole : public TConsole
{
public:
	TFileConsole();
	virtual ~TFileConsole();

	bool Open(const char *filename);
	void Close();

	bool IsOpen();

private:

	void DisplayString(const char *str);

    // data members hidden to make it possible to compile this
    // header without including all the file stuff.
    class Privates;
    Privates* m_this;
};

}; // namespace tool

#endif

