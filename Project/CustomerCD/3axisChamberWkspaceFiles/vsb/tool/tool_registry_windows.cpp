//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:53:59 $
//  $Revision: 1.1 $
//  $RCSfile: tool_registry_windows.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_registry_windows.cpp

      Class containing Windows-specific methods for tool_registry.h
*/
//---------------------------------------------------------------------------
#ifdef _MSC_VER
	#pragma warning (disable : 4786) // identifier truncated in debug info
#endif

#include "tool_registry.h"
#include <stdexcept>
#include <windows.h>
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

void TRegistry::ReallyCloseKey(void*& key)
{
    void* tempKey = convertRootKeyToWindows(key);
    RegCloseKey(reinterpret_cast<HKEY>(tempKey));
    key = convertRootKeyFromWindows(tempKey);
}
//---------------------------------------------------------------------------

void TRegistry::ReallyCloseKey(ERootKey& key)
{
    void* tempKey = reinterpret_cast<void*>(key);
    ReallyCloseKey(tempKey);
    key = ERootKey(reinterpret_cast<int>(tempKey));
}
//---------------------------------------------------------------------------

void TRegistry::ReallyFlushKey(void*& key)
{
    void* tempKey = convertRootKeyToWindows(key);
    RegFlushKey(reinterpret_cast<HKEY>(tempKey));
    key = convertRootKeyFromWindows(tempKey);
}
//---------------------------------------------------------------------------

void TRegistry::ReallyFlushKey(ERootKey& key)
{
    void* tempKey = reinterpret_cast<void*>(key);
    ReallyFlushKey(tempKey);
    key = ERootKey(reinterpret_cast<int>(tempKey));
}
//---------------------------------------------------------------------------

void* TRegistry::convertRootKeyToWindows(void* key)
{
    if (key == reinterpret_cast<void*>(eRootKeyClassesRoot))
        return HKEY_CLASSES_ROOT;
    else if (key == reinterpret_cast<void*>(eRootKeyLocalMachine))
        return HKEY_LOCAL_MACHINE;
    else if (key == reinterpret_cast<void*>(eRootKeyCurrentUser))
        return HKEY_CURRENT_USER;
    else if (key == reinterpret_cast<void*>(eRootKeyUsers))
        return HKEY_USERS;
    else
        return key;
}
//---------------------------------------------------------------------------

void* TRegistry::convertRootKeyFromWindows(void*& key)
{
    if (key == HKEY_CLASSES_ROOT)
        return reinterpret_cast<void*>(eRootKeyClassesRoot);
    else if (key == HKEY_LOCAL_MACHINE)
        return reinterpret_cast<void*>(eRootKeyLocalMachine);
    else if (key == HKEY_CURRENT_USER)
        return reinterpret_cast<void*>(eRootKeyCurrentUser);
    else if (key == HKEY_USERS)
        return reinterpret_cast<void*>(eRootKeyUsers);
    else
        return key;
}
//---------------------------------------------------------------------------

unsigned int TRegistry::convertKeyAccessToWindows(unsigned int access)
{
    unsigned int result = 0;

    if (access & eKeyAccessQueryValue)
        result += KEY_QUERY_VALUE;
    if (access & eKeyAccessSetValue)
        result += KEY_SET_VALUE;
    if (access & eKeyAccessCreateSubKey)
        result += KEY_CREATE_SUB_KEY;
    if (access & eKeyAccessEnumerateSubKeys)
        result += KEY_ENUMERATE_SUB_KEYS;
    if (access & eKeyAccessNotify)
        result += KEY_NOTIFY;
    if (access & eKeyAccessCreateLink)
        result += KEY_CREATE_LINK;

    return result;
}
//---------------------------------------------------------------------------

unsigned int TRegistry::convertKeyAccessFromWindows(unsigned int access)
{
    unsigned int result = 0;

    if (access & KEY_QUERY_VALUE)
        result += eKeyAccessQueryValue;
    if (access & KEY_SET_VALUE)
        result += eKeyAccessSetValue;
    if (access & KEY_CREATE_SUB_KEY)
        result += eKeyAccessCreateSubKey;
    if (access & KEY_ENUMERATE_SUB_KEYS)
        result += eKeyAccessEnumerateSubKeys;
    if (access & KEY_NOTIFY)
        result += eKeyAccessNotify;
    if (access & KEY_CREATE_LINK)
        result += eKeyAccessCreateLink;

    return result;
}
//---------------------------------------------------------------------------

TRegDataType DataTypeToRegData(unsigned long Value)
{
    switch (Value) {
        case REG_SZ:
            return rdString;
        case REG_EXPAND_SZ:
            return rdExpandString;
        case REG_DWORD:
            return rdInteger;
        case REG_BINARY:
            return rdBinary;
        default:
            return rdUnknown;
        }
}
//---------------------------------------------------------------------------

unsigned long RegDataToDataType(TRegDataType Value)
{
    switch (Value) {
        case rdString:
            return REG_SZ;
        case rdExpandString:
            return REG_EXPAND_SZ;
        case rdInteger:
            return REG_DWORD;
        case rdBinary:
            return REG_BINARY;
        default:
            return REG_NONE;
        }
}
//---------------------------------------------------------------------------


int TRegistry::GetData(const std::string& Name, unsigned char* Buffer, unsigned long BufSize, TRegDataType &RegData)
{
    unsigned long DataType = REG_NONE;
    void* tempKey = convertRootKeyToWindows(GetCurrentKey());
    if (RegQueryValueEx(
            reinterpret_cast<HKEY>(tempKey), Name.c_str(), 0, &DataType,
            Buffer, &BufSize) != ERROR_SUCCESS)
        throw std::runtime_error(("Registry Exception:RegGetDataFailed:" +Name).c_str());

    RegData = DataTypeToRegData(DataType);
    return BufSize;
}
//---------------------------------------------------------------------------


void* TRegistry::GetKey(const std::string& Key)
{
    std::string S = Key;
    bool Relative = IsRelative(S);
    if (!Relative)
        S.erase(0, 1);
    void* result = 0;
    RegOpenKeyEx(
        reinterpret_cast<HKEY>(convertRootKeyToWindows(GetBaseKey(Relative))),
        S.c_str(),
        0,
        convertKeyAccessToWindows(d_Access),
        reinterpret_cast<HKEY*>(&result));
    return convertRootKeyFromWindows(result);
}
//---------------------------------------------------------------------------

void TRegistry::PutData(const std::string& Name, const unsigned char* Buffer, unsigned long BufSize, TRegDataType RegData)
{
    unsigned long DataType = RegDataToDataType(RegData);
    if (RegSetValueEx(
            reinterpret_cast<HKEY>(convertRootKeyToWindows(GetCurrentKey())),
            Name.c_str(), 0, DataType,
            Buffer, BufSize) != ERROR_SUCCESS)
        throw std::runtime_error(("Registry Exception:RegSetDataFailed:" +Name).c_str());
}
//---------------------------------------------------------------------------


bool TRegistry::GetDataInfo(const std::string& ValueName, TRegDataInfo &Value)
{
  unsigned long DataType;

  Value.RegData = TRegDataType(0);
  Value.DataSize = 0;

  bool Result = RegQueryValueEx(
    reinterpret_cast<HKEY>(convertRootKeyToWindows(GetCurrentKey())),
    ValueName.c_str(), 0,
    &DataType, 0, &Value.DataSize) == ERROR_SUCCESS;

  Value.RegData = DataTypeToRegData(DataType);

  return Result;
}
//---------------------------------------------------------------------------


bool TRegistry::GetKeyInfo(TRegKeyInfo &Value)
{
    Value = TRegKeyInfo();
    FILETIME ft;
    bool result = RegQueryInfoKey(
            reinterpret_cast<HKEY>(convertRootKeyToWindows(GetCurrentKey())),
            0, 0, 0,
            &(Value.NumSubKeys),
            &(Value.MaxSubKeyLen), 0,
            &(Value.NumValues),
            &(Value.MaxValueLen),
            &(Value.MaxDataLen), 0,
            &ft) == ERROR_SUCCESS;
    SYSTEMTIME sysTime;
    FileTimeToSystemTime(&ft, &sysTime);
    Value.FileTime.setDate(sysTime.wYear, sysTime.wMonth, sysTime.wDay);
    Value.FileTime.setTime(
        sysTime.wHour, sysTime.wMinute, sysTime.wSecond, sysTime.wMilliseconds);
    return result;
}
//---------------------------------------------------------------------------

void TRegistry::GetKeyNames(tool::TStrings& Strings)
{
    Strings.clear();
    TRegKeyInfo Info;
    if (GetKeyInfo(Info))
    {
        char* buffer = new char[Info.MaxSubKeyLen+1];
        for (unsigned int i = 0; i < Info.NumSubKeys; ++i)
        {
            unsigned long len = Info.MaxSubKeyLen+1;
            RegEnumKeyEx(
                reinterpret_cast<HKEY>(convertRootKeyToWindows(GetCurrentKey())),
                i, buffer, &len, 0, 0, 0, 0);
            Strings.push_back(buffer);
        }
    }
}
//---------------------------------------------------------------------------

void TRegistry::GetValueNames(tool::TStrings& Strings)
{
    Strings.clear();
    TRegKeyInfo Info;
    if (GetKeyInfo(Info))
    {
        char* buffer = new char[Info.MaxValueLen+1];
        for (unsigned int i = 0; i < Info.NumValues; ++i)
        {
            unsigned long len = Info.MaxValueLen+1;
            RegEnumKeyEx(
                reinterpret_cast<HKEY>(convertRootKeyToWindows(GetCurrentKey())),
                i, buffer, &len, 0, 0, 0, 0);
            Strings.push_back(buffer);
        }
    }
}
//---------------------------------------------------------------------------


bool TRegistry::OpenKey(const std::string& Key, bool CanCreate)
{
    bool Result;
    void* TempKey;
    std::string S;
    unsigned long Disposition;
    bool Relative;

    S = Key;
    Relative = IsRelative(S);

    if (!Relative)
        S.erase(0,1);
    TempKey = 0;
    if (!CanCreate || S=="")
        Result = RegOpenKeyEx(
            reinterpret_cast<HKEY>(convertRootKeyToWindows(GetBaseKey(Relative))),
            S.c_str(), 0,
            convertKeyAccessToWindows(d_Access),
            reinterpret_cast<HKEY*>(&TempKey)) == ERROR_SUCCESS;
    else
        Result = RegCreateKeyEx(
            reinterpret_cast<HKEY>(convertRootKeyToWindows(GetBaseKey(Relative))),
            S.c_str(), 0, 0,
            REG_OPTION_NON_VOLATILE,
            convertKeyAccessToWindows(d_Access), 0,
            reinterpret_cast<HKEY*>(&TempKey), &Disposition)
                == ERROR_SUCCESS;
    if (Result)
    {
        if ((GetCurrentKey() != 0) && Relative)
            S = d_CurrentPath + "\\" + S;
        ChangeKey(convertRootKeyFromWindows(TempKey), S);
    }

    return Result;
}
//---------------------------------------------------------------------------


void* TRegistry::GetBaseKey(bool Relative)
{
  if ((GetCurrentKey() == 0) || !Relative)
    return reinterpret_cast<void*>(GetRootKey());
  else
    return GetCurrentKey();
}
//---------------------------------------------------------------------------

bool TRegistry::KeyExists(const std::string& Key)
{
    void* TempKey = GetKey(Key);
    if (TempKey !=0)
        ReallyCloseKey(TempKey);
    return TempKey != 0;
}
//---------------------------------------------------------------------------


} // namespace tool
