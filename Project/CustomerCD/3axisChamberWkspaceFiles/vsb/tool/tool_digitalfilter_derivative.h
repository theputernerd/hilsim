//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_derivative.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_derivative.h/.cpp
      Template implementation of a simple derivative estimator.
      Uses finite difference method of estimation,

           x'(t) =  x(t) - x(t-dt)
                   ----------------
                         dt
*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_derivativeH
#define tool_digitalfilter_derivativeH
//---------------------------------------------------------------------------

namespace tool {

template <typename T>
class DigitalFilterDerivative
{
  public:

    DigitalFilterDerivative() { reset(0.0, T()); }
    ~DigitalFilterDerivative() {}

    void reset(
            const double& time,
            const T& inValue
            );

    void inputValue(
            const double& time,
            const T& inValue
            );

    void calculateOutput(const double& time);

    const T& outputValue() const { return d_outValue; }

  private:

    T d_inValue;
    double d_inTime;
    T d_lastInValue;
    double d_lastInTime;

    T d_outValue;
    double d_outTime;
};
//---------------------------------------------------------------------------

}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_derivative.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_derivative.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
