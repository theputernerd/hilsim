//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:05:12 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_ab.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_ab.h/.cpp
      Template implementation of an Alpha-Beta Estimator Digital Filter.   */
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_abCPP
#define tool_digitalfilter_abCPP

#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_digitalfilter_ab.h"

#include <cmath>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterAlphaBeta<T>::reset(
            const double& time,
            const T& val,
			const T& valDot
            )
{
    d_bandwidth = 0.0;
    d_lastBandwidth = d_bandwidth;

    d_measValue = val;
    d_measTime = time;

    d_lastTimeStep = 0.0;

    d_estValue = val;
    d_estValueDot = valDot;
    d_estTime = time;

    d_lastEstValue = d_estValue;
    d_lastEstValueDot = d_estValueDot;
    d_lastEstTime = d_estTime;

    d_alpha = 0.0;
    d_beta = 0.0;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterAlphaBeta<T>::setBandwidth(const double& rBandwidth)
{
	if (rBandwidth < 0.0)
		d_bandwidth = 0.0;
	else
		d_bandwidth = rBandwidth;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterAlphaBeta<T>::inputMeasValue(
			const double& time,
			const T& measuredValue
            )
{
    // time must not go backwards (can stand still though)
    if (time < d_estTime || time < d_measTime)
        throw std::logic_error("AlphaBeta Filter: Time travelling backwards in filter.");

    // record the new measurement with its timestamp
    d_measTime = time;
    d_measValue = measuredValue;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterAlphaBeta<T>::calculateEstimates(const double& time)
{
    // time must not go backwards (can stand still though)
	if (time < d_estTime || time < d_measTime)
        throw std::logic_error("AlphaBeta Filter: Time travelling backwards in filter.");

    // are the previous estimates to be copied off or overwritten?
    if (time > d_estTime && d_measTime > d_estTime)
    {
        // estimator has moved on in time, need to copy off the
        // previous estimated values
        d_lastEstValue = d_estValue;
        d_lastEstValueDot = d_estValueDot;
        d_lastEstTime = d_estTime;
    }

    // do filter calculations for time of measurement, ignoring
    // the time that was passed to calculate call for the moment
    double timestep = d_measTime - d_lastEstTime;

    // timestep will always be greater than zero, because the previous
    // estimates are not updated until a new measurement is received (as
    // opposed to the 'current' estimates which are given new values
    // every time calculate is called).  This ensures that previous
    // estimates are always kept in the past.

    // Prediction (up to time of measurement)
    T predictedValue = d_lastEstValue + timestep * d_lastEstValueDot;
    T predictedValueDot = d_lastEstValueDot;

    if (d_bandwidth == 0.0 || timestep == 0.0)
    {
        // Bandwidth may be zero on the first pass, but timestep should
        // only be zero if there is a fault in the programme somewhere.
        // We catch it anyway to be certain that a divide by zero does
        // not occur.
        d_estValue = predictedValue;
        d_estValueDot = predictedValueDot;
        d_estTime = d_measTime;
    }
    else
    {
        // check if gains need recalculating
        if (timestep != d_lastTimeStep ||
            d_bandwidth != d_lastBandwidth)
        {
            using namespace std; // MSVC Compat

            // recalculate gains
            d_alpha = 2.0 * timestep / (2.0*timestep + sqrt(2.0)/d_bandwidth);
            d_beta  = d_alpha * d_alpha / (2.0 - d_alpha);

            d_lastTimeStep = timestep;
            d_bandwidth = d_lastBandwidth;
        }

        // Correction
        T valError = d_measValue - predictedValue;
        d_estValue = predictedValue + d_alpha * valError;
        d_estValueDot = predictedValueDot + (d_beta / timestep) * valError;
        d_estTime = d_measTime;
    }

    // Estimate has so far been calculated for measured time.  We need
    // to push it forward to the time given as this function's parameter...
    if (time > d_estTime)
    {
        double pushStep = time - d_estTime;
        d_estValue = d_estValue + pushStep * d_estValueDot;
        d_estTime = time;
    }
}
//---------------------------------------------------------------------------


} // end namespace tool
//---------------------------------------------------------------------------

//#define ENABLE_BUILTIN_TESTS
#ifdef ENABLE_BUILTIN_TESTS

#include "tool_arraytemplate.h"
#include "../marsbase/mars_vector3.h"
// Test declaration
namespace {

void compileTestFunc()
{
    tool::DigitalFilterAlphaBeta<double> filter1;
    filter1.reset(0.0, 0.0, 0.0);
    filter1.setBandwidth(2.0);
    filter1.inputMeasValue(1.0, 3.0);
    filter1.calculateEstimates(1.0);
    double val = filter1.outputEstValue();
    double valDot = filter1.outputEstValueDot();

    typedef tool::TArrayTemplate<double, 2> Vec2;
    tool::DigitalFilterAlphaBeta<Vec2> filter2;
    filter2.reset(
            0.0, Vec2(), Vec2());
    filter2.setBandwidth(2.0);
    Vec2 measVal;
    measVal[0] = 3.0;
    measVal[1] = 4.0;
    filter2.inputMeasValue(1.0, measVal);
    filter2.calculateEstimates(1.0);
    Vec2 val2 = filter2.outputEstValue();
    Vec2 val2Dot = filter2.outputEstValueDot();

    tool::DigitalFilterAlphaBeta<Mars::TVector3> filter3;
    filter3.reset(
            0.0, Mars::TVector3(), Mars::TVector3());
    filter3.setBandwidth(2.0);
    filter3.inputMeasValue(1.0, Mars::TVector3(3.0, 4.0, 5.0));
    filter3.calculateEstimates(1.0);
    Mars::TVector3 val3 = filter3.outputEstValue();
    Mars::TVector3 val3Dot = filter3.outputEstValueDot();
}


} // unnamed namespace
//---------------------------------------------------------------------------
#endif // ENABLE_BUILTIN_TESTS

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#undef export
#endif

#endif // tool_digitalfilter_abCPP
