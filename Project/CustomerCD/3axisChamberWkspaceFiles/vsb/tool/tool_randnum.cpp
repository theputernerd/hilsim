//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.8 $
//  $RCSfile: tool_randnum.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_randnum.h/.cpp

        Deterministic pseudo random number generator
*/
//---------------------------------------------------------------------------
#include "tool_randnum.h"

#ifndef __MATH_H
    #include <cmath>
    #define __MATH_H
#endif

#undef max
#undef min

#ifndef limitsSTL
    #include <limits>
    #define limitsSTL
#endif
#ifndef __TIME_H
    #include <ctime>
    #define __TIME_H
#endif

#ifdef RANDNUMGEN_TEST_DRIVER
#include <iostream>
#endif

// do this again to make sure...
#undef max
#undef min

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

TRandNumGen::TRandNumGen():itsSeed(1L)
{
}
//---------------------------------------------------------------------------

void TRandNumGen::Seed(long aSeed)
{
    itsSeed = aSeed;
}
//---------------------------------------------------------------------------

void TRandNumGen::ClockSeed()
{
    using namespace std; // MSVC Compat
    itsSeed = time(0) % 12938;  // modulo about 3 1/2 hours.
}
//---------------------------------------------------------------------------

long TRandNumGen::NextNum()
{
    register long x, hi, lo, t;

    /*
    * Compute x[n + 1] = (7^5 * x[n]) mod (2^31 - 1).
    * From "Random number generators: good ones are hard to find",
    * Park and Miller, Communications of the ACM, vol. 31, no. 10,
    * October 1988, p. 1195.
    */
    x = itsSeed;
    hi = x / 127773L;
    lo = x % 127773L;
    t = 16807 * lo - 2836 * hi;
    if (t <= 0)
        t += 0x7fffffffL;
    itsSeed = t;
    return (t);
}
//---------------------------------------------------------------------------

double TRandNumGen::NextUniformNum(
        const double& lower,
        const double& upper
        )
{
    double diff;
    double min;
    if (upper > lower)
    {
        diff = upper - lower;
        min = lower;
    }
    else
    {
        diff = lower - upper;
        min = upper;
    }

    return double(NextNum())
        / std::numeric_limits<long>::max() * diff + min;
}
//---------------------------------------------------------------------------

double TRandNumGen::NextNormalNum(
        const double& mean,
        const double& stddev
        )
{
    using namespace std; // MSVC Compat
/*
    // the sum of twelve random numbers with a mean of zero also
    // has a mean of zero and a stddev equal to the width of the
    // uniform distribution (coz the stddev of a uniform distribution
    // is 1/12 of its width).
    double sum = 0;
    for (int i = 0; i < 12; i++)
        sum+= NextUniformNum(-0.5, 0.5);
    return sum*stddev + mean;
*/

    // uses Box-Muller(1958) transform - faster than the above version,
    // probably less clean.
    double r1 = NextUniformNum(0.000001, 1.0);
        // the >0.0 lower bound is to avoid log(0), but will slightly skew the result.
    double r2 = NextUniformNum(0.0, 1.0);
    return stddev*sqrt(-2.0*log(r1))*cos(3.14159265358979323846*2*r2) + mean;

}
//---------------------------------------------------------------------------

} //namespace tool
//---------------------------------------------------------------------------


#ifdef RANDNUMGEN_TEST_DRIVER
int main(int argc, char* argv[])
{
    tool::TRandNumGen rand;

    std::cout << "Gaussian (Normal) distribution, mean 0.0, stddev 1.0\n";
    for (int n = 100; n <= 1000000; n*=100)
    {
        double Ex = 0;
        double Ex2 = 0;

        for (int i = 1; i <= n; i++)
        {
            double num = rand.NextNormalNum();
            Ex += num;
            Ex2 += (num*num);
        }

        double mean = Ex/n;
        double stddev = sqrt(fabs(Ex2/n - mean*mean));
        std::cout << "n: " << n << "  Ex: " << Ex << "  Ex2: " << Ex2 << '\n';
        std::cout << "mean: " << mean << "  stddev: " << stddev << '\n';
    }

    std::cout << "\nUniform distribution, mean 0.5, stddev 0.28867, lb 0.0, ub 1.0\n";
    for (int n = 100; n <= 1000000; n*=100)
    {
        double Ex = 0;
        double Ex2 = 0;

        for (int i = 1; i <= n; i++)
        {
            double num = rand.NextUniformNum(0.0, 1.0);
            Ex += num;
            Ex2 += (num*num);
        }

        double mean = Ex/n;
        double stddev = sqrt(fabs(Ex2/n - mean*mean));
        std::cout << "n: " << n << "  Ex: " << Ex << "  Ex2: " << Ex2 << '\n';
        std::cout << "mean: " << mean << "  stddev: " << stddev << '\n';
    }

    char c;
    std::cin >> c;

    return 0;
}
#endif
//---------------------------------------------------------------------------

