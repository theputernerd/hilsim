//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_sequentiallogic.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_sequentiallogic.h/.cpp
        Collection of useful sequential logic classes.
        Currently includes rising and falling edge detectors.
*/
//---------------------------------------------------------------------------
#ifndef tool_sequentiallogicH
#define tool_sequentiallogicH

namespace tool {
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// Rising Edge Detector
class LogicRisingEdgeDetector
{
  public:
    LogicRisingEdgeDetector() { reset(0.0, false); }

    void reset(const double& time, const bool& state);

    bool operator()(const double& time, const bool& state);

  private:
    bool d_currentState;
    double d_currentTime;
    bool d_lastState;
};
//---------------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// Falling Edge Detector
class LogicFallingEdgeDetector
{
  public:
    LogicFallingEdgeDetector() { reset(0.0, false); }

    void reset(const double& time, const bool& state);

    bool operator()(const double& time, const bool& state);

  private:
    bool d_currentState;
    double d_currentTime;
    bool d_lastState;
};
//---------------------------------------------------------------------------


} // namespace tool

#endif