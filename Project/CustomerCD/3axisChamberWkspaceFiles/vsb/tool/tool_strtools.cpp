//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_strtools.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_strtools.h/.cpp

        String functions
*/
//---------------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_strtools.h"

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


// Converts character c to upper case
int ToUpper(int c)
{
	int upC;

	// convert to upper case
	if (c >= 'a' && c <= 'z')
		upC = c - ('a' - 'A');
	else
		upC = c;

	return upC;
}
//---------------------------------------------------------------------------


// Converts character c to lower case
int ToLower(int c)
{
	int lowC;
	
	// convert to lower case
	if (c >= 'A' && c <= 'Z')
		lowC = c - ('A' - 'a');
	else
		lowC = c;

	return lowC;
}
//---------------------------------------------------------------------------


// Converts string s to upper case
std::string ToUpper(std::string s)
{
    size_t len = s.size();
    std::string result;
    result.resize(len);
    for (size_t i = 0; i < len; ++i)
        result[i] = tool::ToUpper(s[i]);

    return result;
}
//---------------------------------------------------------------------------


// Converts string s to lower case
std::string ToLower(std::string s)
{
    size_t len = s.size();
    std::string result;
    result.resize(len);
    for (size_t i = 0; i < len; ++i)
        result[i] = tool::ToLower(s[i]);

    return result;
}
//---------------------------------------------------------------------------

// String comparison, not case sensitive
// Returns -1 if string1 is less than string2
// Returns 0 if string1 is equal to string2
// Returns +1 if string1 is greater than string2
int StrCmpNoCase(const char *string1, const char *string2)
{
	const char *str1 = string1;
	const char *str2 = string2;

	char ch1,ch2;
	size_t len = strlen(string1);
	if (len > strlen(string2)) len = strlen(string2);

	for (size_t i=0; i<len; i++)
	{
		// convert to lower case
		ch1 = ToLower(str1[i]);
		ch2 = ToLower(str2[i]);

		if (ch1 < ch2) return -1;
		if (ch1 > ch2) return 1;
	}

	if (strlen(string1) < strlen(string2)) return -1;
	if (strlen(string1) > strlen(string2)) return 1;

	return 0;
}
//---------------------------------------------------------------------------


// String comparison, case sensitive
// Returns -1 if string1 is less than string2
// Returns 0 if string1 is equal to string2
// Returns +1 if string1 is greater than string2
int StrCmpCase(const char *string1, const char *string2)
{
	const char *str1 = string1;
	const char *str2 = string2;

	char ch1,ch2;
	size_t len = strlen(string1);
	if (len > strlen(string2)) len = strlen(string2);

	for (size_t i=0; i<len; i++)
	{
		ch1 = str1[i];
		ch2 = str2[i];

		if (ch1 < ch2) return -1;
		if (ch1 > ch2) return 1;
	}

	if (strlen(string1) < strlen(string2)) return -1;
	if (strlen(string1) > strlen(string2)) return 1;

	return 0;
}
//---------------------------------------------------------------------------


std::list<std::string> splitString(
        const std::string& str,
        const std::string& seper)
{
    std::list<std::string> result;

    size_t seperLen = seper.size();
    size_t oldPos = 0;
    size_t pos;
    do
    {
        pos = str.find(seper, oldPos);
        std::string foundStr = str.substr(oldPos, pos-(oldPos));
        if (foundStr.size() > 0)
            result.push_back(foundStr);
        oldPos = pos+seperLen;
    } while (pos != str.npos);

    return result;
}
//---------------------------------------------------------------------------


std::string spliceString(
        const std::list<std::string>& strList,
        const std::string& seper)
{
    std::string result;

    for (std::list<std::string>::const_iterator i = strList.begin();
         i != strList.end(); ++i)
    {
        result += *i + seper;
    }

    // erase the final seper string
    if (result.size() > 0)
        result.erase(result.size() - seper.size());

    return result;
}
//---------------------------------------------------------------------------


}; // namespace tool


