//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_ratelimiter.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_ratelimiter.h/.cpp
      Template implementation of a rate limiter using a simple
      derivative estimator
*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_ratelimiterH
#define tool_digitalfilter_ratelimiterH
//---------------------------------------------------------------------------

#ifndef tool_digitalfilter_derivativeH
    #include "tool_digitalfilter_derivative.h"
#endif

namespace tool {
//---------------------------------------------------------------------------

template <typename T>
class DigitalFilterRateLimiter
{
  public:

    DigitalFilterRateLimiter() { reset(0.0, T()); }
    ~DigitalFilterRateLimiter() {}

    void setRateLimits(
            const T& lowerBound,
            const T& upperBound
            );

    void reset(
            const double& time,
            const T& inValue
            );

    void inputValue(
            const double& time,
            const T& inValue
            );

    void calculateOutput(const double& time);

    const T& outputValue() const { return d_outValue; }

  private:

    T d_lowerRateLimit;
    T d_upperRateLimit;

    T d_inValue;
    double d_inTime;
    T d_lastInValue;
    double d_lastInTime;

    T d_outValue;
    double d_outTime;

    tool::DigitalFilterDerivative<T> d_deriv;
};
//---------------------------------------------------------------------------


}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_ratelimiter.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_ratelimiter.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
