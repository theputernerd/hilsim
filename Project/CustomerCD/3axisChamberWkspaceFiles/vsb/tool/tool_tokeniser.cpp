//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_tokeniser.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_tokeniser.h/.cpp

        A simple tokeniser, or Lexical Analyser to chop raw characters from
        a stream into tokens.

        Adapted from the class 'lexer' from the 2172 course, 1999,
        University of Ottawa

        Adapted by Duncan Fletcher

*/
//---------------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_tokeniser.h"

#if defined(__GNUG__)
  #include <strstream.h>
#else
  #include <strstream>
#endif

#include <cstdlib>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

// tokeniser_error functions...

tokeniser_error::tokeniser_error(int lineNumber,const std::string& message)
 : std::runtime_error(message), d_lineNumber(lineNumber)
{
    char buffer[100];
	using namespace std; // MSVC Compat
    itoa(d_lineNumber, buffer, 10);
    d_message = std::string("(") + buffer + ") " + message;
}
//---------------------------------------------------------------------------

const char* tokeniser_error::what() const throw()
{
    return d_message.c_str();
}
//---------------------------------------------------------------------------

// TTokeniser functions...

// constructor
TTokeniser::TTokeniser(const bool& commentsAllowed)
  : d_token(),
    d_type(eTokenEOF),
    d_lineno(1),
    d_inBuffer(false),
    d_commentsAllowed(commentsAllowed)
{
}
//---------------------------------------------------------------------------


// destructor
TTokeniser::~TTokeniser()
{

}
//---------------------------------------------------------------------------


std::string TTokeniser::nextToken(std::istream& is)
{
    getToken(is);

    if (d_type == eTokenEOF)
        throw tool::tokeniser_error(d_lineno,"unexpected end of file");

    return d_token;
}
//---------------------------------------------------------------------------


std::string TTokeniser::nextToken(std::istream& is, const ETokenType& tokenType)
{
    getToken(is);

    if (d_type != tokenType)
        throw tool::tokeniser_error(d_lineno,"type mismatch");

    return d_token;
}
//---------------------------------------------------------------------------


void TTokeniser::nextToken(std::istream& is, const std::string& testToken)
{
    getToken(is);

    if (d_type == eTokenEOF)
        throw tool::tokeniser_error(d_lineno,"unexpected end of file");

    if (d_token != testToken)
    {
        std::ostrstream se;
        se << '\'' << testToken << '\''
           << " expected, "
           << '\'' << d_token << "\' found" << '\0';
        throw tool::tokeniser_error(d_lineno, se.str());
    }
}
//---------------------------------------------------------------------------


// peek at token, but do not remove it from the stream
std::string TTokeniser::peekToken(std::istream& is)
{
    getToken(is);
    putBack();
    return d_token;
}
//---------------------------------------------------------------------------

void TTokeniser::eatWhitespace(std::istream& is)
{
    char c; // char last read

    bool nonWhitespaceFound = false;

    while(!nonWhitespaceFound && (c = is.get()) != EOF)
    {
        switch(c)
        {
            // "white spaces"
            case '\n':  // new line
            case ' ':   // space
            case '\t':  // tab
            case '\r':  // carriage return
                if (c=='\n')
                    d_lineno++;
                break;
            default:
                is.putback(c);
                nonWhitespaceFound = true;
        }
    }
}
//---------------------------------------------------------------------------


void TTokeniser::skipLine(std::istream& is)
{
    char c;
    while ((c = is.get()) != EOF)
    {
        if (c == '\n')
        {
            d_lineno++;
            break;
        }
    }
}
//---------------------------------------------------------------------------

// reads a possibly signed numeric value and throws
// an exception if it is not
// CAREFUL, THERE SHOULD BE NO WHITE SPACE BETWEEN
// THE - (DASH) AND THE NUMBER
double TTokeniser::readNumeric(std::istream& is)
{
    std::string totalToken = getToken(is);

    if (d_token == "+" || d_token =="-")
    {
        totalToken += nextToken(is, eTokenNumeric);
    }
    else if (d_type != eTokenNumeric)
        throw tool::tokeniser_error(d_lineno,"numeric expected");

    char c;
    is.get(c);
    if (c == 'e' || c == 'E')
    {
        totalToken += "e" + getToken(is);
        if (d_token == "+" || d_token =="-")
        {
            std::string exponent = nextToken(is, eTokenNumeric);
            if (exponent.find('.') != exponent.npos)
                throw tool::tokeniser_error(d_lineno,"numeric expected");
            totalToken += exponent;
        }
        else if (d_type != eTokenNumeric)
            throw tool::tokeniser_error(d_lineno,"numeric expected");
    }
    else
        is.putback(c);

	using namespace std; // MSVC Compat
    return atof(totalToken.c_str());
}
//---------------------------------------------------------------------------


// put s back, so the next token returned by
// next_token is s
void TTokeniser::putBack()
{
    d_inBuffer = true;
}
//---------------------------------------------------------------------------


// true if token is numeric (real or integer)
bool TTokeniser::isNumeric(const std::string& token)
{
    bool decpoint_found = false;

    int k = token.size();

    if (k == 0)
        return false;

    if (token[0] < '0' || token[0] > '9')
        return false;

    for(int i = 1; i < k; i++)
    {
        if (token[i] == '.')
        {
            if (decpoint_found)
                return false;
            decpoint_found = true;
        }
        else if (token[i] < '0' || token[i] > '9')
        {
            return false;
        }
    }

    return true;
}
//---------------------------------------------------------------------------


std::string TTokeniser::getToken(std::istream& is)
{
    if (d_inBuffer)
    {
        // if a token was put back, return that
        d_inBuffer = false;
        return d_token;
    }

    char c; // char last read
    d_token = ""; // MSVC doesn't suppport clear()

    int tokenSize = 0;

    // THE UGLY PIECE OF CODE BELOW IS A HANDCODED
    // STATE MACHINE
    while((c = is.get()) != EOF)
    {
        switch(c)
        {
            // "white spaces"
            case '\n':  // new line
            case ' ':   // space
            case '\t':  // tab
            case '\v':  // vertical tab
            case '\r':  // carriage return
            case '\f':  // form feed
                if (tokenSize==0)
                {
                    if (c=='\n')
                        d_lineno++;
                    continue;
                }
                else
                {
                    // it is a token separator
                    is.putback(c);
                    return d_token;
                }
            // quotes
            case '\'':
            case '\"':
            case '`':
                {
                    if (tokenSize!=0)
                    {
                        is.putback(c);
                        return d_token;
                    }

                    char quote = c;

                    while((c = is.get()) != EOF)
                    {
                        if (c == quote)
                        {
                            d_type = eTokenString;
                            return d_token;
                        }
                        else if (c == '\n')
                        {
                            d_lineno++;
                            throw tool::tokeniser_error(
                                d_lineno,
                                "String cannot span multiple lines");
                        }
                        else if (c == '\\')
                        {
                            // escape sequences: \n \t \b \\ \0 \r
                            c = is.get();
                            switch(c)
                            {
                                case 'n':
                                    c = '\n';
                                    break;
                                case 't':
                                    c = '\t';
                                    break;
                                case 'b':
                                    c = '\b';
                                    break;
                                case '\\':
                                case '\"':
                                case '\'':
                                    // themselves
                                    break;
                                case '0':
                                    c = '\0';
                                    break;
                                case 'r':
                                    c = '\r';
                                    break;
                                default:
                                    throw tool::tokeniser_error(
                                        d_lineno,
                                        "Unknown quoted character in string");
                            }
                        }

                        d_token += c;
                        ++tokenSize;
                    }

                    throw tool::tokeniser_error(
                        d_lineno,
                        "Unterminated quoted string");
                }

            // comment, discard!
            case '#':
                if (d_commentsAllowed)
                {
                    while ((c = is.get()) != EOF)
                    {
                        if (c == '\n')
                        {
                            d_lineno++;
                            break;
                        }
                    }
                    continue;
                } // else treat '#' as a special character...
            // special tokens
            case '+':
            case '-':
            case '/':
            case '%':
            case '$':
            case '*':
            case '(':
            case ')':
            case ',':
            case '^':
            case '~':
            case ';':
            case ':':
            case '.':
            case '\\':
            case '@':
            case '?':
            case '[':
            case ']':
            case '{':
            case '}':
                if (tokenSize == 0)
                {
                    d_token += c;
                    d_type = eTokenSpecial;
                }
                else
                {
                    is.putback(c);
                }
                return d_token;
            // if & or | occur in pairs, return the
            // pair  as one token
            case '&':
            case '|':
                char c2;
                if (tokenSize == 0)
                {
                    d_token += c;
                    d_type = eTokenSpecial;
                    if ((c2 = is.get()) == c)
                    {
                        d_token += c;
                    }
                    else
                    {
                        is.putback(c2);
                    }
                }
                else
                {
                    is.putback(c);
                }
                return d_token;
            // if the following are followed by =,
            // return the two characters as one token
            case '!':
            case '=':
            case '<':
            case '>':
                if (tokenSize == 0)
                {
                    d_token += c;
                    d_type = eTokenSpecial;
                    if ((c = is.get()) == '=')
                    {
                        d_token += c;
                    }
                    else
                    {
                        is.putback(c);
                    }
                }
                else
                {
                    is.putback(c);
                }
                return d_token;
            // not special
            default:
                // numeric value
                if (c >= '0' && c <= '9')
                {
                    if (tokenSize == 0)
                    {
                        d_type = eTokenNumeric;
                        bool dot_found = false;
                        d_token += c;
                        ++tokenSize;
                        while((c = is.get()) != EOF)
                        {
                            if (c >= '0' && c <= '9')
                            {
                                d_token += c;
                                ++tokenSize;
                            }
                            else if (c == '.')
                            {
                                if (!dot_found)
                                {
                                    d_token += c;
                                    ++tokenSize;
                                    dot_found = true;
                                }
                                else
                                {
                                    // second dot
                                    is.putback(c);
                                    return d_token;
                                }
                            }
                            else
                            {
                                is.putback(c);
                                return d_token;
                            }
                        } // while !EOF
                        // read numeral
                        is.putback(c);
                        return d_token;
                    }
                    else
                    {
                        // tokenSize != 0
                        d_token += c;
                        ++tokenSize;
                    }
                }
                else if ( (c >= 'a' && c <= 'z') ||
                          (c >= 'A' && c <= 'Z') ||
                          c == '_' )
                {
                    // not a digit, but valid
                    d_type = eTokenIdentifier;
                    d_token += c;
                    ++tokenSize;
                }
                else
                {
                    throw tool::tokeniser_error(
                        d_lineno,
                        "invalid character");
                }
        } // switch
    } // while

    if (tokenSize!=0)
    {
       return d_token;
    }

    // tokenSize == 0, END OF FILE MUST HAVE OCCURED
    d_type = eTokenEOF;
    return ""; // EOF
}
//---------------------------------------------------------------------------


} // namespace tool

