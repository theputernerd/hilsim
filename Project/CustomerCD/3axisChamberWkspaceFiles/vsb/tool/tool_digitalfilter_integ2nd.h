//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:34:36 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_integ2nd.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_integ2nd.h/.cpp
      Template implementation of a 2nd Order Digital Integrator.

          y       1       dt   (1 - z^-1)
         ---  =  ---  =  --- * ----------
          x       s       2    (1 + z^-1)

*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_integ2ndH
#define tool_digitalfilter_integ2ndH
//---------------------------------------------------------------------------

namespace tool {

template <typename T>
class DigitalFilterInteg2ndOrder
{
  public:

    DigitalFilterInteg2ndOrder() { reset(0.0, T(), T()); }
    ~DigitalFilterInteg2ndOrder() {}

    void reset(
            const double& time,
            const T& inValue,
            const T& outValue
            );

    void inputValue(
            const double& time,
            const T& inValue
            );

    void calculateOutput(const double& time);

    const T& outputValue() const { return d_outValue; }

  private:

    T d_inValue;
    double d_inTime;
    T d_lastInValue;
    double d_lastInTime;

//    double d_lastTimeStep;

    T d_outValue;
    double d_outTime;
    T d_lastOutValue;
    double d_lastOutTime;
};
//---------------------------------------------------------------------------

}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_integ2nd.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_integ2nd.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
