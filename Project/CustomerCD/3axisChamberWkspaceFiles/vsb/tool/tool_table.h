//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_table.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_table.h/.cpp

        Scalar, 1D, 2D, 3D or 4D Data tables for linear interpolation

        Independent variables are referred to as Xn variables (n=0,..,4)
        Dependent variable is referred to as Y

        This class has been superceded by tool_lookuptable.h/.cpp - do not
        use this class for any new projects.

*/
//---------------------------------------------------------------------------
#ifndef tool_tableH
#define tool_tableH

#include <vector>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

class TTableData
{
  public:
	TTableData();

	void Clear();

	// Set table dimensions - number of independent variables
	// Minimum is 0, maximum is 4
	void SetDimensions(int dim);
	int GetDimensions();

	// Set independent X variable data
	// "dim" is the dimension number of the independent variable
	// "dim" must be between 1 and the table dimension
	// eg. if table is 3D, dim can be 1,2 or 3
	// Returns false if dim is out of valid range
	bool SetXData(int dim, const std::vector<double>& xData);

	// Set dependent Y variable data
	// Y data must be sequentially ordered starting from highest X dimension
	// eg. for three dimensions, with x1 size = l, x2 size = m, x3 size = n
	//     x3_0, x2_0, x1_0,..,x1_l
	//     x3_0, x2_1, x1_0,..,x1_l
	//     ...
	//     x3_0, x2_m, x1_0,..,x1_l
	//     x3_1, x2_0, x1_0,...x1_l
	//     ...
	//     x3_n, x2_0, x1_0,..,x1_l
	//     ...
	//     x3_n, x2_m, x1_0,..,x1_l
	// Y data size must be the product of the X dimension sizes
	// Returns true if size is correct, otherwise false
	bool SetYData(const std::vector<double>& yData);

	// Get size of table in X dimension
	// "dim" is the dimension number of the independent variable
	int GetXSize(int dim);

	// Check the size of the Y data is consistent with the dimension and
	// sizes of the independent X variable arrays
	// Y data size must be the product of the X dimension sizes
	// Returns true if size is correct, otherwise false
	bool CheckYSize();

	// Get independent variable value
	// "dim" is the dimension number of the independent variable
	// "dim" must be less than or equal to the table dimensions
	// "dim" must be in the range 1 to 4
	// "index" is the zero-based index of the independent variable value
	double GetXValue(int dim, int index);

	// Get dependent variable values
	// "xn" is the index of the nth dimension
	// eg. table of zero dim, GetYValue() returns constant value
	// eg. table of two dim, GetYValue(2,3) returns data value at position x1=2, x2=3
	double GetYValue(int x1=0, int x2=0, int x3=0, int x4=0);

	// Check that independent variables are monotonically increasing
	// Returns true if all X variables are increasing, otherwise false
	bool IsOrdered();

	// Interpolate y data based on independent variable values
	// Returns true if all x values are within table bounds
	// Returns false if any x values are outside table bounds
	// If x is outside table bounds, y data is extrapolated as
	// constant boundary values
	bool Interpolate(double &yVal, double x1=0, double x2=0, double x3=0, double x4=0);

	// Invert table with respect to one of the independent variables
	// Y data must be monotonically increasing with respect to the specified X variable
	// "dim" is the dimension number of the independent variable
	// ("dim" must be less than or equal to the table dimensions)
	// Returns false if unable to invert table, otherwise true
	// Currently only implemented for 1 and 2 dimension tables
	bool Invert(int dim);

  private:

	// 1D arrays to hold independent variable data
	std::vector<double> mX[4];

	// Array of dependent data
	std::vector<double> mY;

	// Table dimensions
   int mDimension;
};
//---------------------------------------------------------------------------

}; // namespace tool

#endif


