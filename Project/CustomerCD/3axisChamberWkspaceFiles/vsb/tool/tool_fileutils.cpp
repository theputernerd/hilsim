//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:56:43 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fileutils.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fileutils.h/.cpp

      Collection of general filename utilities, plus some OS-dependent
      File System functions (file system searches, etc)

      **Note that a number of these functions are C++ adaptions of Borland's
          SysUtil functions - mainly the "extract" functions.

      The Windows implementations are activated by the conditional define
      USE_TOOL_WINDOWS_IMPL appearing in the project settings

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
/*
    These functions consists of two types - those that are OS independent,
    and those that require specific OS knowledge.
    The OS independent ones are defined here, while the others are
    in seperate CPP's that get #include'd according to conditional defines.
*/
//---------------------------------------------------------------------------
#ifdef _MSC_VER
	#pragma warning (disable : 4786) // identifier truncated in debug info
#endif

#include "tool_fileutils.h"

#include "tool_strtools.h"
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

// Get OS-dependent functions...
#if defined(USE_TOOL_WINDOWS_IMPL)
    // currently Windows is the only OS implemented for this class
    #include "tool_fileutils_windows.cpp"
#endif
//---------------------------------------------------------------------------

// forward declarations
namespace {

bool wildcardMatch_i(const std::string& filename, const std::string& pattern);

bool queryExclude(
        const std::string &filename,
        const std::list<std::string> &excludeList);

char* Next(char*& Lead);
//---------------------------------------------------------------------------

// The following is specific to Windows only in that '\' characters can
// legally appear in some Unix filenames, and that the windows Drive
// delimiter is actually a path-list seperator in Unix.  Depending
// on how heavily this set of functions get used though, they should work
// pretty well on non-Windows OS's.
const char PathDelim = '\\';
const char PathDelimAlt = '/';
const char DriveDelim = ':';  // '' in unix
//const char PathSep = ';';   // ':' in unix

} // unnamed namespace
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


std::string extractFileDrive(const std::string& FileName)
{
    std::string Result;

    if (FileName.size() >= 2 && FileName[1] == DriveDelim)
        Result = FileName.substr(0, 2);
    else if (FileName.size() >= 2 &&
             (FileName[0] == PathDelim || FileName[0] == PathDelimAlt) &&
             (FileName[1] == PathDelim || FileName[1] == PathDelimAlt))
    {
        int j = 0;
        size_t i = 3;
        while (i < FileName.size() && j < 2)
        {
            if (FileName[i-1] == PathDelim || FileName[i-1] == PathDelimAlt)
                ++j;
            if (j < 2)
                ++i;
        }
        if (FileName[i-1] == PathDelim || FileName[i-1] == PathDelimAlt)
            --i;
        Result = FileName.substr(0, i);
    }
    else
        Result = "";

    return Result;
}
//---------------------------------------------------------------------------


std::string extractFilePath(const std::string& FileName)
{
    int lastpath = FileName.find_last_of(PathDelim, FileName.npos);
    if (lastpath == FileName.npos)
        lastpath = -1;
    int otherLastpath = FileName.find_last_of(PathDelimAlt, FileName.npos);
    if (otherLastpath == FileName.npos)
        otherLastpath = -1;
    lastpath = lastpath >= otherLastpath ? lastpath : otherLastpath;
    int lastdrive = FileName.find_last_of(DriveDelim, FileName.npos);
    if (lastdrive == FileName.npos)
        lastdrive = -1;
    return FileName.substr(
            0,
            (lastpath > lastdrive ? lastpath : lastdrive) + 1);
}
//---------------------------------------------------------------------------


std::string extractFileName(const std::string& FileName)
{
    int lastpath = FileName.find_last_of(PathDelim, FileName.npos);
    if (lastpath == FileName.npos)
        lastpath = -1;
    int otherLastpath = FileName.find_last_of(PathDelimAlt, FileName.npos);
    if (otherLastpath == FileName.npos)
        otherLastpath = -1;
    lastpath = lastpath >= otherLastpath ? lastpath : otherLastpath;
    int lastdrive = FileName.find_last_of(DriveDelim, FileName.npos);
    if (lastdrive == FileName.npos)
        lastdrive = -1;
    return FileName.substr(
            (lastpath > lastdrive ? lastpath : lastdrive)+1,
            FileName.npos);
}
//---------------------------------------------------------------------------


std::string extractFileExt(const std::string& FileName)
{
    // This implementation is getting rather inefficient.  Might
    // be better off switching this to use the "lastDelimiter" util
    // concept like Borland does at some point.
    int lastpath = FileName.find_last_of(PathDelim, FileName.npos);
    if (lastpath == FileName.npos)
        lastpath = -1;
    int otherLastpath = FileName.find_last_of(PathDelimAlt, FileName.npos);
    if (otherLastpath == FileName.npos)
        otherLastpath = -1;
    lastpath = lastpath >= otherLastpath ? lastpath : otherLastpath;
    int lastdelim = lastpath;
    int lastdrive = FileName.find_last_of(DriveDelim, FileName.npos);
    if (lastdrive == FileName.npos)
        lastdrive = -1;
    if (lastdrive > lastdelim)
        lastdelim = lastdrive;
    int lastdot = FileName.find_last_of('.', FileName.npos);
    if (lastdot == FileName.npos)
        lastdot = -1;
    if (lastdot > lastdelim)
        lastdelim = lastdot;

    if (FileName[lastdelim] == '.')
        return FileName.substr(lastdelim, FileName.npos);
    else
        return "";
}
//---------------------------------------------------------------------------


std::string extractRelativePath(
        const std::string& BaseName,
        const std::string& DestName)
{
    char* BasePath;
    char* DestPath;
    char* BaseLead;
    char* DestLead;
    char* BasePtr;
    char* DestPtr;


    if (!tool::StrCmpNoCase(
            extractFileDrive(BaseName).c_str(),
            extractFileDrive(DestName).c_str()))
    {
        std::string Result;
        BasePath = strdup(extractFilePathNoDrive(BaseName).c_str());
        DestPath = strdup(extractFilePathNoDrive(DestName).c_str());
        BaseLead = BasePath;
        BasePtr = Next(BaseLead);
        DestLead = DestPath;
        DestPtr = Next(DestLead);
        while (BasePtr != 0 &&
               DestPtr != 0 &&
               !tool::StrCmpNoCase(BasePtr, DestPtr))
        {
            BasePtr = Next(BaseLead);
            DestPtr = Next(DestLead);
        }
        Result = "";
        while (BaseLead != 0)
        {
            Result = Result + ".." + PathDelim;
            Next(BaseLead);
        }
        if (DestPtr != 0 && *DestPtr != '\0')
            Result = Result + DestPtr + PathDelim;
        if (DestLead != 0)
            Result = Result + DestLead;     // destlead already has a trailing backslash
        Result = Result + extractFileName(DestName);

        free(BasePath);
        free(DestPath);

        return Result;
    }
    else
        return DestName;
}
//---------------------------------------------------------------------------


std::string extractFilePathNoDrive(const std::string& FileName)
{
    std::string result = tool::extractFilePath(FileName);
    return result.substr(tool::extractFileDrive(FileName).length(), result.npos);
}
//---------------------------------------------------------------------------


bool wildcardMatch(const std::string& filename, const std::string& pattern)
{
    return wildcardMatch_i(tool::ToUpper(filename), tool::ToUpper(pattern));
}
//---------------------------------------------------------------------------


std::list<std::string> getFileList(
        const std::string& basePath,
        const std::string& searchPattern)
{
    return getFileList(basePath, searchPattern, std::list<std::string>());
}
//---------------------------------------------------------------------------


std::list<std::string> getFileList(
        const std::string& basePath,
        const std::string& searchPattern,
        const std::list<std::string>& excludeList)
{
    std::list<tool::TFileData> fileData =
        getFileDataList(basePath, searchPattern);

    std::list<std::string> result;

    for(std::list<tool::TFileData>::iterator i = fileData.begin();
        i != fileData.end(); ++i)
    {
        // check to see if this file is on the exclude list...
        if (!queryExclude(i->getFileName(), excludeList))
        {
            //add the filename to the list
            result.push_back(i->getFileName());
        }
    }

    return result;
}
//---------------------------------------------------------------------------


} // namespace tool

namespace { // unnamed

bool wildcardMatch_i(const std::string& filename, const std::string& pattern)
{
    // get a few basic cases out of the way..
    switch (pattern.size())
    {
        case 0:
            // only a match if the filename is also empty
            return filename.size() == 0;
        case 1:
            if (pattern[0] == '*')
                // we have the 'any' match
                return true;

        // don't check for anything else here, let it go on to the more complex
        // cases
    }

    // find the first wildcard character
    unsigned int firstStar = pattern.find_first_of('*');
    if (firstStar == pattern.npos)
        firstStar = -1;
    unsigned int firstQuest = pattern.find_first_of('*');
    if (firstQuest == pattern.npos)
        firstQuest = -1;

    unsigned int firstWild = firstStar < firstQuest ? firstStar : firstQuest;

    if (firstWild == -1)
        // no wildcard char, so we must have exact match
        return filename == pattern;
    else if (firstWild == 0)
    {
        // first char in pattern is a wildcard, so we need to skip
        // that then look for the match in the filename...
        if (pattern[firstWild] == '?')
        {
            // single char to match
            if (filename.size() == 1)
                // filename is only a single char
                return true;
            else
                // check the remainder
                return wildcardMatch_i(filename.substr(1), pattern.substr(1));
        }
        else
        {
            // '*' to match, so we need to search for the next tame part
            unsigned int nextStar = pattern.find('*', firstWild+1);
            if (nextStar == pattern.npos)
                nextStar = -1;
            unsigned int nextQuest = pattern.find('*', firstWild+1);
            if (nextQuest == pattern.npos)
                nextQuest = -1;

            unsigned int nextWild = nextStar < nextQuest ? nextStar : nextQuest;

            // get the tame part
            std::string patPart =
                    pattern.substr(firstWild+1, nextWild-(firstWild+1));

            if (patPart.size() == 0)
            {
                std::string newPattern = pattern;
                if (pattern[1] == '?')
                {
                    // switch the wildcards
                    newPattern[0] = '?';
                    newPattern[1] = '*';
                    return wildcardMatch_i(filename, newPattern);
                }
                else
                {
                    // some sadistic bastard put two '*' characters side by side
                    // erase the excess wildcard and try again
                    newPattern.erase(nextWild, 1);
                    return wildcardMatch_i(filename, newPattern);
                }
            }
            else
            {
                unsigned int matchStart = filename.find(patPart, 0);

                if (matchStart == filename.npos)
                    return false;
                else
                    return wildcardMatch_i(
                                filename.substr(matchStart+patPart.size()),
                                pattern.substr(1+patPart.size()));
            }
        }
    }
    else
    {
        // first part of pattern is tame (not wild), so lets get it
        // and check to see if it matches
        std::string patPart = pattern.substr(0, firstWild);

        if (patPart.size() < filename.size())
        {
            // there is more to the filename than the tame part...
            if (patPart == filename.substr(0, firstWild))
                // if the tame part is a match, check the rest of the name
                return wildcardMatch_i(
                        filename.substr(firstWild),
                        pattern.substr(firstWild));
            else
                // tame part doesn't match, we're done
                return false;
        }
        else if (patPart.size() == filename.size())
        {
            // since tame part and filename are the same length...
            if (pattern[firstWild] == '*' && pattern.size() == firstWild+1)
                // only have a match if '*' is the rest of the pattern
                return true;
            else
                return false;
        }
        else
            // tame part is larger than filename
            return false;
    }
}
//---------------------------------------------------------------------------


bool queryExclude(
        const std::string &filename,
        const std::list<std::string> &excludeList)
{
    if (excludeList.size() == 0)
        return false;

    // this implementation is about as inefficient as it gets - mainly
    // because we want to do a case-insensitive search, so we can't use
    // a sorted list.

    std::list<std::string>::const_iterator itr = excludeList.begin();
    bool result = false;
    while (itr != excludeList.end())
    {
        if (!stricmp(filename.c_str(), itr->c_str()))
        {
            result = true;
            break;
        }
        itr++;
    }

    return result;
}
//---------------------------------------------------------------------------


char* Next(char*& Lead)
{
    char* Result = Lead;

    if (Result == 0)
        return 0;
    while (Lead != 0 && *Lead != PathDelim && *Lead != PathDelimAlt)
    {
        ++Lead;
        if (*Lead == '\0')
            Lead = 0;
    }

    if (Lead != 0)
    {
        *Lead = '\0';
        ++Lead;
    }

    return Result;
}
//---------------------------------------------------------------------------

} // unnamed namespace


