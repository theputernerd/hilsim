//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_compress.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_compress.h/.cpp
        Implements a simple run length string compression system.  The idea
        is to minimize the cost in memory when we want to store a module's
        source code.  So this provides a method to easily compress the data
        and then store it in the compressed form.
*/
//---------------------------------------------------------------------------
#include "tool_compress.h"

#ifndef __STDLIB_H
    #include <cstdlib>
    #define __STDLIB_H
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif

namespace tool {

static const int WORKBUFFLEN = 512;

static char workCharBuff[WORKBUFFLEN+1];   /* compression working buffer */


class _PREFIXCLASS TCompressedLine
{
public:
    TCompressedLine();
	TCompressedLine(int len);
    ~TCompressedLine();

	TCompressedLine *itsNext;
	int  itsRLE_buffLength;
	int  itsRLE_origLength;
	char *itsRLE_buff;

 private:
    TCompressedLine(const TCompressedLine& in); // not implemented
    TCompressedLine& operator =(const TCompressedLine& in); // not implemented
};

TCompressedLine::TCompressedLine()
{
    itsRLE_buff = new char[2000];
	itsRLE_buff[0] = 0;
	itsRLE_origLength = 0;
	itsRLE_buffLength = 0;
	itsNext =0;
}

TCompressedLine::TCompressedLine(int len)
{
    itsRLE_buff = new char[len];
	itsRLE_buff[0] = 0;
	itsRLE_origLength = 0;
	itsRLE_buffLength = 0;
	itsNext =0;


}

TCompressedLine::~TCompressedLine()
{
    delete[] itsRLE_buff;
}


int RLEncode(char *inp, char* outp)
{
  int         comprLength = 0;   /* Length of compressed string         */
  int         runLength   = 0;   /* Run length of a repeated character  */

  char       *cptr = outp;       /* Pointer into working buffer         */

  char        lastc       = 0;
  char        c;
  char        RLCode;            /* Encoded run length                  */

  while((c=*inp++)!=0)
  {
	 /* ensure our character is in the < 127 ascii range */

	 c &= (0xFF>>1);

	 /*  If we have a new character or our maximum run length      *
	  *  has been reached finish encoding the last one, shunt it   *
	  *  off to the buffer and start encoding the next one         */

	 if (c!=lastc || runLength==127)
	 {
		if (runLength==1)
		{

		  /* The last character had a R.L. of one so we store it  *
			*  un-modified                                         */

		  comprLength++;
		  *cptr++ = lastc;

		  /* Begin counting the run length of the new character   */

		  runLength=1;
		  lastc=c;

		} else if (runLength>1)
		{
		  comprLength+=2;           /* We have a two character code */
		  RLCode = (unsigned char) (runLength | (1<<7));
		  *cptr++ = RLCode;         /* Store run length code        */
		  *cptr++ = lastc;          /* Store the character          */

		  /* Begin counting the run length of the new character   */

		  runLength=1;
		  lastc=c;

		} else
		{
		  /* This must be the first character in the string */
		  /* as the runLength is zero only at the beginning */

		  lastc=c;
		  runLength=1;
		}

	 } else
	 {
		/* Character not changed, increment it's run length */
		runLength++;
	 }
  }
  if (runLength==1)
  {
	 comprLength++;
	 *cptr++ = lastc;
  } else if (runLength>1)
  {
	 comprLength+=2;
	 RLCode = (unsigned char) (runLength | (1<<7));
	 *cptr++ = RLCode;
	 *cptr++ = lastc;

  }

  /* Add a terminating NULL to the compressed string */
  comprLength++;
  *cptr++ = 0;
  return comprLength;
}

int RLDecode(char *inp, char* outp)
{
  char          c;         /* Current character                       */
  unsigned      runLength; /* Current character's run length          */
  int           i,j;       /* General purpose looping variables       */


  j = 0;

  while ((c=*inp++)!=0 && j < 511)
  {
	 if (c & (1<<7))     /* Is this character an encoded run length? */
	 {
		/* Get the run length and the character it applies to */

		runLength = (unsigned) (c & (0xFF>>1));
		c = *inp++;

		/* Expand the character to the output file */

		for (i=0;i < (int)runLength && j < 511; i++)
		  outp[j++] = c;

	 } else
	 {
		/* We have a standard character, just write it out */
		outp[j++] = c;
	 }
  }
  outp[j] = 0;
  /* update the position ptr to point to the start of the next line *
	* in the buffer                                                  */
  return j;
}

TCompressBuffer::TCompressBuffer():itsLastLine(0),itsFirstLine(0),
					  itsLineIterator(0),itsLineCount(0), itsTotalCharCount(0)
{
  workCharBuff[0] = 0;
}

void TCompressBuffer::Clear()
{
  if (itsLineCount==0) return;
  itsTotalCharCount = 0;
  TCompressedLine *currLine = 0;
  itsLineIterator = itsFirstLine;
  do
  {
	  currLine = itsLineIterator;
	  itsLineIterator = currLine->itsNext;
	  //free(currLine);
	  delete currLine;
  } while (itsLineIterator);
  itsLineCount=0;
}

TCompressBuffer::~TCompressBuffer()
{
  Clear();
}

int TCompressBuffer::AddString(const char *str)
{
  int         comprLength = 0;   /* Length of compressed string         */
  int         runLength   = 0;   /* Run length of a repeated character  */

  char       *cptr;              /* Pointer into working buffer         */

  char        lastc       = 0;
  char        c;
  char        RLCode;            /* Encoded run length                  */
  int         origlen = 0;

  TCompressedLine *newCompressedLine;
  if (strlen(str) >= WORKBUFFLEN) return 0;

  if (!str)
	 workCharBuff[0] = 0;
  else
  {
	  origlen = strlen(str);
	  itsTotalCharCount+= origlen;

	  /* Initialise for compression */

	  cptr    = workCharBuff;

	  while((c=*str++)!=0)
	  {
		 /* ensure our character is in the < 127 ascii range */

		 c &= (0xFF>>1);

		 /*  If we have a new character or our maximum run length      *
		  *  has been reached finish encoding the last one, shunt it   *
		  *  off to the buffer and start encoding the next one         */

		 if (c!=lastc || runLength==127)
		 {
			if (runLength==1)
			{

			  /* The last character had a R.L. of one so we store it  *
				*  un-modified                                         */

			  comprLength++;
			  *cptr++ = lastc;

			  /* Begin counting the run length of the new character   */

			  runLength=1;
			  lastc=c;

			} else if (runLength>1)
			{
			  comprLength+=2;           /* We have a two character code */
			  RLCode = (unsigned char) (runLength | (1<<7));
			  *cptr++ = RLCode;         /* Store run length code        */
			  *cptr++ = lastc;          /* Store the character          */

			  /* Begin counting the run length of the new character   */

			  runLength=1;
			  lastc=c;

			} else
			{
			  /* This must be the first character in the string */
			  /* as the runLength is zero only at the beginning */

			  lastc=c;
			  runLength=1;
			}

		 } else
		 {
			/* Character not changed, increment it's run length */

			runLength++;
		 }
	  }

	  /* store the last character from the input string */

	  if (runLength==1)
	  {
		 comprLength++;
		 *cptr++ = lastc;
	  } else if (runLength>1)
	  {
		 comprLength+=2;
		 RLCode = (unsigned char) (runLength | (1<<7));
		 *cptr++ = RLCode;
		 *cptr++ = lastc;

	  }

	  /* Add a terminating NULL to the compressed string */

	  *cptr++=0;
	  comprLength++;

	  /* Check to see that we haven't overflowed our compression *
		* buffer                                                  */

	  if (comprLength  >= WORKBUFFLEN) return 0;

	  /* Append the compressed string to our compression buffer  *
		* and update all relevant variables                       */
  }

  //newCompressedLine = (TCompressedLine*)
  //	 calloc(1,sizeof(TCompressedLine) + 1 + strlen(workCharBuff));
  newCompressedLine = new TCompressedLine(1 + strlen(workCharBuff));  
  if (!newCompressedLine) return 0;

  strcat(newCompressedLine->itsRLE_buff,workCharBuff);
  newCompressedLine->itsNext = 0;
  if (!itsFirstLine || itsLineCount==0)
  {
	 itsFirstLine = itsLastLine = newCompressedLine;
	 itsLineCount = 1;
  } else {
	 itsLastLine->itsNext = newCompressedLine;
	 itsLastLine = newCompressedLine;
	 itsLineCount++;
  }

  newCompressedLine->itsRLE_buffLength = strlen(workCharBuff);
  newCompressedLine->itsRLE_origLength = origlen;
  return 1;
}

void TCompressBuffer::ResetIterator()
{
  if (itsLineCount)
	 itsLineIterator = itsFirstLine;
  else
	 itsLineIterator = 0;
}

int TCompressBuffer::AdvanceIterator()
{
  if (itsLineCount)
	 if (itsLineIterator->itsNext)
	 {
		itsLineIterator = itsLineIterator->itsNext;
		return 1;
	 } else
	 {
		itsLineIterator = 0;
		return 0;
	 }
  else
  {
	 itsLineIterator = 0;
	 return 0;
  }
}

const char *TCompressBuffer::UncompressLine(int index)
{
  TCompressedLine *iterator;
  workCharBuff[0] = 0;
  if (index>=0 && index<itsLineCount)
  {
	 iterator = itsLineIterator;
	 ResetIterator();
	 for (int i=0; i<index; i++) AdvanceIterator();
	 UncompressCurrLine();
	 itsLineIterator = iterator;
  }
  return  workCharBuff;

}

const char *TCompressBuffer::UncompressCurrLine()
{
  char         *cptr;      /* Working pointer into compression buffer */
  char          c;         /* Current character                       */
  unsigned      runLength; /* Current character's run length          */
  unsigned      i,j;       /* General purpose looping variables       */

  if (!itsLineIterator) return 0;

  j = 0;

  cptr=itsLineIterator->itsRLE_buff;

  while ((c=*cptr++)!=0 && j < 511)
  {
	 if (c & (1<<7))     /* Is this character an encoded run length? */
	 {
		/* Get the run length and the character it applies to */

		runLength = (unsigned) (c & (0xFF>>1));
		c = *cptr++;

		/* Expand the character to the output file */

		for (i=0;i < runLength && j < 511; i++)
		  workCharBuff[j++] = c;

	 } else
	 {
		/* We have a standard character, just write it out */
		workCharBuff[j++] = c;
	 }
	 if (c=='\n')
	 {
		/* We're done with this line when we reach a new-line character */
		goto leave;
	 }
  }
leave:

  workCharBuff[j++] = 0;
  /* update the position ptr to point to the start of the next line *
	* in the buffer                                                  */
  return workCharBuff;
}


int TCompressBuffer::CurrLineLength()
{
	if (!itsLineIterator) return 0;
	return itsLineIterator->itsRLE_origLength;

}

TCompressBuffer& TCompressBuffer::operator =(TCompressBuffer& aBuffer)
{
  Clear();
  char *workCharBuff = new char[WORKBUFFLEN+1];
  aBuffer.ResetIterator();
  do
  {
	 strcpy(workCharBuff,aBuffer.UncompressCurrLine());
	 AddString(workCharBuff);
  } while (aBuffer.AdvanceIterator());
  delete[] workCharBuff;
  return *this;
}

}; // namespace tool

//#define __TEST
#ifdef __TEST
void main()
{
	TCompressBuffer *tc = new TCompressBuffer;
	tc->AddString("Hello This     is line 1! Wheeeeeeeeeeee");
	tc->AddString("Testing 123");
	tc->AddString("MODULE \"Guidance Law\"");
	tc->AddString("BEGIN");
	tc->AddString("  FOR i:= 3 to 100 DO");
	tc->AddString("  BEGIN");
	tc->AddString("    PRINTF \"Testing\"");
	tc->AddString("  END");
	tc->AddString("END");


	{
	  tc->ResetIterator();
	  do
	  {
		  printf("<%s> is %d was %d\n",tc->UncompressCurrLine(),
		  tc->itsLineIterator->itsRLE_buffLength, tc->CurrLineLength());
	  } while (tc->AdvanceIterator());
	}

}

#endif



