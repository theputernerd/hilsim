//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.19 $
//  $RCSfile: tool_mathtools.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_mathtools.h/.cpp
      Useful mathematical functions.
      Transformations, physical constants and conversion constants
*/
//---------------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_mathtools.h"

#ifndef __MATH_H
    #include <cmath>
    #define __MATH_H
#endif

#pragma hdrstop
//---------------------------------------------------------------------------

using namespace std;  // MSVC Compat

namespace tool {

//---------------------------------------------------------------------------
// Physical Constants
const double constants::G = 9.80665;                // standard gravity, m/s^2
const double constants::PI = 3.14159265358979323846;        // pi
const double constants::TWO_PI = 2*constants::PI;           // 2*pi
const double constants::HALF_PI = 1.57079632679489661923;   // pi/2

const double constants::SQRT2 = 1.41421356237309504880;     // square root of 2

const double constants::T0 = 288.15;    // air temperature, sea level, deg K
const double constants::R0 = 1.225;     // air density, sea level, kg/m^3
const double constants::P0 = 1.01325e5; // air pressure, sea level, kg/m^2
const double constants::A0 = 340.294;   // speed of sound, sea level, m/s

const double constants::K = 1.38e-23;   // Boltzman's Constant
const double constants::C = 2.99792e+08;// Speed of light, m/s
//---------------------------------------------------------------------------
// Conversions
// Angles
const double constants::RAD_TO_DEG = 57.2957795131;     // radians <=> degrees
const double constants::DEG_TO_RAD = 1.74532925199e-2;
const double constants::RAD_TO_MILS = 1018.59163578;    // radians <=> mils
const double constants::MILS_TO_RAD = 9.8174770425e-4;
// Acceleration
const double constants::G_TO_MPS2 = 9.80665;            // m/s^2 <=> g's
const double constants::MPS2_TO_G = 0.101971621298;

// Length
const double constants::FT_TO_M = 0.3048;               // feet <=> metres
const double constants::M_TO_FT = 3.28083989501;
const double constants::KFT_TO_M = 304.8;               // kilofeet <=> metres
const double constants::M_TO_KFT = 3.28083989501e-3;
const double constants::NMI_TO_M = 1852.0;              // nautical miles <=> metres
const double constants::M_TO_NMI = 5.39956803456e-4;
const double constants::MI_TO_M = 1609.344;             // miles <=> metres
const double constants::M_TO_MI = 6.21371192237e-4;
const double constants::KM_TO_M = 1000.0;               // kilometres <=> metres
const double constants::M_TO_KM = 0.001;

// Speed
const double constants::KTS_TO_MPS = 0.514444444444;    // knots <=> m/s
const double constants::MPS_TO_KTS = 1.94384449244;
const double constants::KPH_TO_MPS = 0.277777777778;    // km/h <=> m/s
const double constants::MPS_TO_KPH = 3.6;

// Mass
const double constants::LB_TO_KG = 0.45359237;          // pounds <=> kilograms
const double constants::KG_TO_LB = 2.20462262185;

// Force
const double constants::LBF_TO_N = 4.44822161526;       // pound-force <=> Newtons
const double constants::N_TO_LBF = 0.2248089431;

// Time
const double constants::SEC_TO_MIN = 1.66666666667e-2;  // minutes <=> seconds
const double constants::MIN_TO_SEC = 60.0;
const double constants::SEC_TO_MILLISEC = 1000;
//---------------------------------------------------------------------------

// Temperature
double constants::CEL_TO_FAH(const double x) // deg Celcius <=> deg Fahrenheit
 { return ((x) * 9/5 + 32.0); }
double constants::FAH_TO_CEL(const double x)
 { return (((x) - 32.0)*5/9); }

double constants::CEL_TO_KEL(const double x) // deg Celcius <=> Kelvin
 { return ((x) + 273.15); }
double constants::KEL_TO_CEL(const double x)
 { return ((x) - 273.15); }
//---------------------------------------------------------------------------

// EUCLID2, return the Euclidean length of x and y
double euclid2(const double x, const double y)
    { return sqrt(sq(x) + sq(y)); };

// EUCLID3, return the Euclidean length of x, y and z
double euclid3(const double x, const double y, const double z)
    { return sqrt(sq(x) + sq(y) + sq(z)); };

// Round, rounds off value up or down
double round(const double x)
{
   double abs_x = fabs(x);
   double floor_x = floor(abs_x);
   if ((abs_x - floor_x) < 0.5) return floor_x*sign(x);
   else return ceil(abs_x)*sign(x);
}

// Round value of x up or down to specified precision
// eg. precision of 1 will round off to nearest integer
//     precision of 0.01 will round off to 2 decimal places
double roundTo(double x, double precision)
{
   if (precision <= 0) return x;
   else
      return round(x/precision)*precision;
}
//---------------------------------------------------------------------------

// PiMod, returns x equivalent angle in range -PI..+PI radians
double piMod(const double x)
{
   double y;

   y = fmod(x,constants::TWO_PI);
   if (y > constants::PI) y -= constants::TWO_PI;
   else if (y < -constants::PI) y+= constants::TWO_PI;

   return y;
}

// These inverse trig functions limit the argument to +/- 1 for safety
double aCos(const double x)
{
  return acos(limit(x,-1.0,1.0));
}

double aSin(const double x)
{
  return asin(limit(x,-1.0,1.0));
}

// ATan2, checks for x=y=0 argument before calling atan2, if x=y=0 return is 0
double aTan2(const double x, const double y)
{
   if (x==0.0 && y==0.0) return 0.0;
   else return atan2(x,y);
}

// SphericalToCartesian,
// Converts Spherical Coordinates r,psi,theta to Cartesian Coordinates x,y,z
void sphericalToCartesian(
        const double r,
        const double psi, const double theta,
        double *x,double *y,double *z
        )
{
	double cos_theta = cos(theta);
	*x = r*cos_theta*cos(psi);
	*y = r*cos_theta*sin(psi);
	*z = -r*sin(theta);
}

// CartesianToSpherical,
// Converts Cartesian Coordinates x,y,z to Spherical Coordinates r,psi,theta
void cartesianToSpherical(
        const double x, const double y, const double z,
        double *r, double *psi, double *theta
        )
{
   *r = euclid3(x,y,z);
   *psi = aTan2(y,x);
   *theta = aTan2(-z,euclid2(x,y));
}
//---------------------------------------------------------------------------

// Transform32, Euler rotations about axis 3 (Z) and axis 2 (Y)
// Vector (x1,y1,z1) in, vector (x2,y2,z2) out
void transform32(
        const double x1, const double y1, const double z1,
        const double psi, const double theta,
        double *x2, double *y2, double *z2
        )
{
	double cos_psi = cos(psi);
	double sin_psi = sin(psi);

	double cos_theta = cos(theta);
	double sin_theta = sin(theta);

	*x2 =  cos_theta*cos_psi*x1 + cos_theta*sin_psi*y1 - sin_theta*z1;
	*y2 =           -sin_psi*x1 +           cos_psi*y1;
	*z2 =  sin_theta*cos_psi*x1 + sin_theta*sin_psi*y1 + cos_theta*z1;
}

// InvTransform32, Inverse Transformation of above
void invTransform32(
        const double x1, const double y1, const double z1,
        const double psi, const double theta,
        double *x2, double *y2, double *z2
        )
{
	double cos_psi = cos(psi);
   double sin_psi = sin(psi);

   double cos_theta = cos(theta);
   double sin_theta = sin(theta);

   *x2 =  cos_theta*cos_psi*x1 - sin_psi*y1 + sin_theta*cos_psi*z1;
   *y2 =  cos_theta*sin_psi*x1 + cos_psi*y1 + sin_theta*sin_psi*z1;
   *z2 =         -sin_theta*x1              +         cos_theta*z1;
}


// Transform321, Euler rotations about axis 3 (Z), axis 2 (Y) and axis 1 (X)
// Vector (x1,y1,z1) in, vector (x2,y2,z2) out
void transform321(
        const double x1, const double y1, const double z1,
        const double psi, const double theta, const double phi,
        double *x2, double *y2, double *z2
        )
{
   double cos_psi = cos(psi);
   double sin_psi = sin(psi);

   double cos_theta = cos(theta);
   double sin_theta = sin(theta);

   double cos_phi = cos(phi);
   double sin_phi = sin(phi);

	(*x2) = (cos_theta*cos_psi)                            * x1 +
           (cos_theta*sin_psi)                            * y1 +
           (-sin_theta)                                   * z1;

   (*y2) = (-cos_phi*sin_psi + sin_phi*sin_theta*cos_psi) * x1 +
           (cos_phi*cos_psi + sin_phi*sin_theta*sin_psi)  * y1 +
           (sin_phi*cos_theta)                            * z1;

   (*z2) = (sin_phi*sin_psi + cos_phi*sin_theta*cos_psi)  * x1 +
           (-sin_phi*cos_psi + cos_phi*sin_theta*sin_psi) * y1 +
           (cos_phi*cos_theta)                            * z1;
}


// InvTransform321, Inverse Transformation of above
void invTransform321(
        const double x1, const double y1, const double z1,
        const double psi, const double theta, const double phi,
        double *x2, double *y2, double *z2
        )
{
   double cos_psi = cos(psi);
   double sin_psi = sin(psi);

   double cos_theta = cos(theta);
   double sin_theta = sin(theta);

   double cos_phi = cos(phi);
   double sin_phi = sin(phi);

   (*x2) = (cos_theta*cos_psi)                            * x1 +
           (-cos_phi*sin_psi + sin_phi*sin_theta*cos_psi) * y1 +
           (sin_phi*sin_psi + cos_phi*sin_theta*cos_psi)  * z1;

   (*y2) = (cos_theta*sin_psi)                            * x1 +
           (cos_phi*cos_psi + sin_phi*sin_theta*sin_psi)  * y1 +
           (-sin_phi*cos_psi + cos_phi*sin_theta*sin_psi) * z1;

   (*z2) = (-sin_theta)                                   * x1 +
           (sin_phi*cos_theta)                            * y1 +
           (cos_phi*cos_theta)                            * z1;
}
//---------------------------------------------------------------------------


// RotOrthogRateToEuler321()
// Converts orthogonal rates in the rotated frame to equivalent 321 Euler rates
void rotOrthogRateToEuler321(
        const double p, const double q, const double r,
        const double theta, const double phi,
        double *psidt, double *thetadt, double *phidt
        )
{
   double cos_phi = cos(phi);
   double sin_phi = sin(phi);
   double temp = (sin_phi*q + cos_phi*r);

   *psidt   = temp/nonzero(cos(theta));
   *thetadt = cos_phi*q - sin_phi*r;
   *phidt   = p + tan(theta)*temp;
}


// RefOrthogRateToEuler321()
// Converts orthogonal rates in the reference frame (E) to equivalent 321 Euler rates
void refOrthogRateToEuler321(
        const double pE, const double qE, const double rE,
        const double psi, const double theta,
        double *psidt, double *thetadt, double *phidt
        )
{
   double cos_psi = cos(psi);
   double sin_psi = sin(psi);
   double temp = (cos_psi*pE + sin_psi*qE);

   *psidt   =  tan(theta)*temp + rE;
   *thetadt = -sin_psi*pE + cos_psi*qE;
   *phidt   =  temp/nonzero(cos(theta));
}


// EulerRate321ToOrthogRot()
// Converts 321 Euler rates to orthogonal rates in the rotated frame
void eulerRate321ToOrthogRot(
        const double psidt, const double thetadt, const double phidt,
        const double theta, const double phi,
        double *p, double *q, double *r
        )
{
   double cos_theta = cos(theta);
   double cos_phi = cos(phi);
   double sin_phi = sin(phi);

   *p =       -sin(theta) * psidt  +  phidt;
   *q = sin_phi*cos_theta * psidt  +  cos_phi * thetadt;
   *r = cos_phi*cos_theta * psidt  -  sin_phi * thetadt;
}


// EulerRate321ToOrthogRef()
// Converts 321 Euler rates to orthogonal rates in the reference frame (E)
void eulerRate321ToOrthogRef(
        const double psidt, const double thetadt, const double phidt,
        const double psi, const double theta,
        double *pE, double *qE, double *rE
        )
{
   double cos_psi = cos(psi);
   double sin_psi = sin(psi);
   double cos_theta = cos(theta);

   *pE = -sin_psi * thetadt  +  cos_psi*cos_theta * phidt;
   *qE =  cos_psi * thetadt  +  sin_psi*cos_theta * phidt;
   *rE =              psidt  -  sin(theta) * phidt;
}
//---------------------------------------------------------------------------

// quaternionToEuler321()
// Calculates Euler angles, 321 rotation sequence, from the input quaternion
void quaternionToEuler321(double e0, double e1, double e2, double e3,
                                 double* psi,double* theta,double* phi)
{
   *psi = aTan2(2*(e1*e2+e0*e3), e0*e0+e1*e1-e2*e2-e3*e3);
   *theta = aSin(2*(e0*e2-e1*e3));
   *phi = aTan2(2*(e2*e3+e0*e1), e0*e0-e1*e1-e2*e2+e3*e3);
}

// euler321ToQuaternion()
// Calculates quaternion from input Euler angles, 321 rotation sequence
void euler321ToQuaternion(double psi,double theta,double phi,
                          double* e0, double* e1, double* e2, double* e3)
{
   double cos_psi =   cos(0.5*psi);
   double sin_psi =   sin(0.5*psi);
   double cos_theta = cos(0.5*theta);
   double sin_theta = sin(0.5*theta);
   double cos_phi =   cos(0.5*phi);
   double sin_phi =   sin(0.5*phi);

   *e0 =  cos_psi*cos_theta*cos_phi + sin_psi*sin_theta*sin_phi;
   *e1 =  cos_psi*cos_theta*sin_phi - sin_psi*sin_theta*cos_phi;
   *e2 =  cos_psi*sin_theta*cos_phi + sin_psi*cos_theta*sin_phi;
   *e3 = -cos_psi*sin_theta*sin_phi + sin_psi*cos_theta*cos_phi;
}

//convert quaternion to direction cosine matrix
void quaternionToMatrix(const double& e0,const double& e1,
                        const double& e2,const double& e3,
                        double& l1,double& l2,double& l3,
                        double& m1,double& m2,double& m3,
                        double& n1,double& n2,double& n3)
{
   l1 = e0*e0 + e1*e1 - e2*e2 - e3*e3;
   m1 = 2 * (e1*e2 + e0*e3);
   n1 = 2 * (e1*e3 - e0*e2);

   l2 = 2 * (e1*e2 - e0*e3);
   m2 = e0*e0 - e1*e1 + e2*e2 - e3*e3;
   n2 = 2 * (e2*e3 + e0*e1);

   l3 = 2 * (e0*e2 + e1*e3);
   m3 = 2 * (e2*e3 - e0*e1);
   n3 = e0*e0 - e1*e1 - e2*e2 + e3*e3;
}
//---------------------------------------------------------------------------

// Binary search of vector v of length n with independent variable x.
// il is bounds x on lower side and im bounds x on upper side with d
// being the location of x between il and im.
bool binarySearch(
        const double x,
        const double* v,
        const int n,
        int *il, int *im, double *d)
{
	int high, low, mid;

	if( x <= v[0])
	{
		*il = *im = 0;
		*d = 0.0;
		return (x == v[0]);	// false if x < v[0], outside array bounds
	}
	if( x >= v[n-1])
	{
		*il = *im = n - 1;
		*d = 0.;
		return (x == v[n-1]);	// false if x > v[0], outside array bounds
	}
	low = 0;
	high = n - 1;
	while(low <= high)
	{
		mid = (low + high) / 2;
		if(x < v[mid]) high = mid - 1;
		else if (x > v[mid]) low = mid + 1;
		else
		{
			*il = mid;
			*im = mid + 1;
			*d = ( x - v[*il]) / ( v[*im] - v[*il]);
			return true;
		}
	}
	*il = high;
	*im = high + 1;
	*d = ( x- v[*il]) / ( v[*im] - v[*il]);
	return true;
}
// --- std::valarray variant
bool binarySearch(
        const double x,
        const std::valarray<double>& v,
        int *il, int *im, double *d)
{
	int high, low, mid;
    unsigned int n = v.size();

	if( x <= v[0])
	{
		*il = *im = 0;
		*d = 0.0;
		return (x == v[0]);	// false if x < v[0], outside array bounds
	}
	if( x >= v[n-1])
	{
		*il = *im = n - 1;
		*d = 0.;
		return (x == v[n-1]);	// false if x > v[0], outside array bounds
	}
	low = 0;
	high = n - 1;
	while(low <= high)
	{
		mid = (low + high) / 2;
		if(x < v[mid]) high = mid - 1;
		else if (x > v[mid]) low = mid + 1;
		else
		{
			*il = mid;
			*im = mid + 1;
			*d = ( x - v[*il]) / ( v[*im] - v[*il]);
			return true;
		}
	}
	*il = high;
	*im = high + 1;
	*d = ( x- v[*il]) / ( v[*im] - v[*il]);
	return true;
}
// --- std::vector variant
bool binarySearch(
        const double x,
        const std::vector<double>& v,
        int *il, int *im, double *d)
{
	int high, low, mid;
    unsigned int n = v.size();

	if( x <= v[0])
	{
		*il = *im = 0;
		*d = 0.0;
		return (x == v[0]);	// false if x < v[0], outside array bounds
	}
	if( x >= v[n-1])
	{
		*il = *im = n - 1;
		*d = 0.;
		return (x == v[n-1]);	// false if x > v[0], outside array bounds
	}
	low = 0;
	high = n - 1;
	while(low <= high)
	{
		mid = (low + high) / 2;
		if(x < v[mid]) high = mid - 1;
		else if (x > v[mid]) low = mid + 1;
		else
		{
			*il = mid;
			*im = mid + 1;
			*d = ( x - v[*il]) / ( v[*im] - v[*il]);
			return true;
		}
	}
	*il = high;
	*im = high + 1;
	*d = ( x- v[*il]) / ( v[*im] - v[*il]);
	return true;
}
//---------------------------------------------------------------------------

// One dimensional interpolation.
// xArray is the independent variable array.
// yArray is the dependent variable array.
// n is the size of xArray and yArray (they should be the same size!).
// x is the value of the independent variable to be interpolated.
// Return value is the interpolated dependent variable from yArray.
double interpolate1D(
        const double x, const int n,
        const double* xArray, const double* yArray
        )
{
   double d;
   int i1,i2;

  	binarySearch(x, xArray, n, &i1, &i2, &d);
	return (d * ( yArray[i2] - yArray[i1]) + yArray[i1]);
}
double interpolate1D(
        const double x,
        const std::valarray<double>& xArray,
        const std::valarray<double>& yArray
        )
{
   double d;
   int i1,i2;

   binarySearch(x, xArray, &i1, &i2, &d);
   return (d * ( yArray[i2] - yArray[i1]) + yArray[i1]);
}
double interpolate1D(
        const double x,
        const std::vector<double>& xArray,
        const std::vector<double>& yArray
        )
{
   double d;
   int i1,i2;

   binarySearch(x, xArray, &i1, &i2, &d);
   return (d * ( yArray[i2] - yArray[i1]) + yArray[i1]);
}
//---------------------------------------------------------------------------


// Two dimensional interpolation.
// x1Array is the first independent variable array.
// x2Array is the second independent variable array.
// yArray is the dependent variable array.
// n1 is the size of x1Array.
// n2 is the size of x2Array.
// yArray has the length n1*n2. Data in yArray is arranged as n2 sets of n1 values.
// x1 is the value of the first independent variable to be interpolated.
// x2 is the value of the second independent variable to be interpolated.
// Return value is the interpolated dependent variable from yArray.
double Interpolate2D(
        const double x1, const int n1,
        const double* x1Array,
        const double x2, const int n2,
        const double* x2Array, const double* yArray
        )
{
   //
   //    Rows are dimension 1, columns are dimension 2
   //
   //           0  1  2  3  4
   //        cols ->
   //     rows
   //  0   |    0  1  2  3  4      y indicies
   //  1   v    5  6  7  8  9
   //  2       10 11 12 13 14



	int i1row, i2row, i1col, i2col;
	double drow,dcol;
	double y1, y2, y3, y4, yp1, yp2;

	binarySearch(x1, x1Array, n1, &i1row, &i2row, &drow);
	binarySearch(x2, x2Array, n2, &i1col, &i2col, &dcol);

	y1 = yArray[i1row * n2 + i1col];
	y2 = yArray[i1row * n2 + i2col];
	y3 = yArray[i2row * n2 + i1col];
	y4 = yArray[i2row * n2 + i2col];

	yp1 = drow * (y3 - y1) + y1;
	yp2 = drow * (y4 - y2) + y2;

	return dcol * (yp2 - yp1) + yp1;
}
//---------------------------------------------------------------------------

}; // namespace tool


