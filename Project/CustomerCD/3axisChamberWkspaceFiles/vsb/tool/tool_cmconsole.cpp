//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.8 $
//  $RCSfile: tool_cmconsole.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_cmconsole.h/.cpp
      Implements a console base class and a specific implementation of a
      command line console class                                           */
//---------------------------------------------------------------------------
#include "tool_cmconsole.h"

#ifndef __STDLIB_H
    #include <cstdlib>
    #define __STDLIB_H
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef __STDARG_H
    #include <cstdarg>
    #define __STDARG_H
#endif
#ifndef __STDIO_H
    #include <cstdio>
    #define __STDIO_H
#endif
#ifndef __ASSERT_H
    #include <cassert>
    #define __ASSERT_H
#endif

namespace tool {

//using namespace std;

namespace {
const int BUFF_CHUNK = 512;
} // unnamed namespace

void TConsole::Print(char *format,...)
{
	using namespace std; // MSVC Compat

	char tempBuffer[BUFF_CHUNK];
	int  tempBufferLen;
	if (!IsOpen()) return;
	va_list ap;
	va_start(ap,format);
	vsprintf(tempBuffer, format, ap);
	va_end(ap);
	tempBufferLen = strlen(tempBuffer);
	if (tempBufferLen)
	{
	  if (!buffLen)
	  {
		 buffLen = BUFF_CHUNK;
		 buffContentsLen = 0;
		 buff = (char *) calloc(buffLen+1,1);
		 assert(buff);
	  } else if (tempBufferLen + 1 + buffContentsLen > buffLen)
	  {
		  buffLen = (((tempBufferLen + 1 + buffContentsLen) % BUFF_CHUNK)+1)*BUFF_CHUNK;
		  buff = (char *)realloc(buff,buffLen+1);
		  assert(buff);
	  }
	  strcat(buff,tempBuffer);
	  buffContentsLen += tempBufferLen;
	}
}

TConsole::TConsole():buffLen(0),buff(0),buffContentsLen(0)
{
}

TConsole::~TConsole()
{
	if (buff) free(buff);
}

void TConsole::Flush()
{
	if (!buffContentsLen || !IsOpen()) return;
	DisplayString(buff);
	Reset();
}

void TConsole::Reset()
{
  buffContentsLen = 0;
  if (buff) buff[0]=0;
}

/*
void TConsole::DisplayString(const char *str)
{
}
*/

bool TConsole::IsOpen()
{
  return false;
}

void TConsole::Settings(int streamId, int optId)
{
}

/////////////////////////////////////////////////////
//
// TCommandLineConsole
//
//////////////////////////////////////////////////////


bool TCommandLineConsole::IsOpen()
{
  return true;
}

void TCommandLineConsole::DisplayString(const char *str)
{
  using namespace std;
  printf("%s\n",str);
}

/////////////////////////////////////////////////////
//
// TFileConsole
//
//////////////////////////////////////////////////////

using namespace std;

class _PREFIXCLASS TFileConsole::Privates
{
  public:
	FILE *m_pF;
};


TFileConsole::TFileConsole():TConsole()
{
    m_this = new Privates();
	m_this->m_pF = 0;
}

TFileConsole::~TFileConsole()
{
	Close();
    delete m_this;
}

bool TFileConsole::Open(const char *filename)
{
  if (IsOpen())
	  Close();

  m_this->m_pF = fopen(filename,"wt");

  return m_this->m_pF!= 0;
}

void TFileConsole::Close()
{
  if (IsOpen())
  {
	  fclose(m_this->m_pF);
	  m_this->m_pF= 0;
  }
}

bool TFileConsole::IsOpen()
{
    return m_this->m_pF != 0;
}

void TFileConsole::DisplayString(const char *str)
{
	if (IsOpen())
	{
		fprintf(m_this->m_pF,"%s\n",str);
	}
}


} // namespace tool
