//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.2 $
//  $RCSfile: tool_smartptr.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_smartptr.h/.cpp

        Adapted from smartptr.h by Sandu Turcan, idlsoft@hotmail.com
*/
//---------------------------------------------------------------------------
#include "tool_smartptr.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool

