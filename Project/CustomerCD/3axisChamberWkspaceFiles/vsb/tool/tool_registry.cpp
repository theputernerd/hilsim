//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:53:59 $
//  $Revision: 1.1 $
//  $RCSfile: tool_registry.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_registry.h/.cpp

      TRegistry
        - implements a wrapper for a limited set of Windows Registry
          function calls.
        - Header is standard C++ code in order to support many platforms
          by allowing selection of alternate implementations.

      ** Note **  This is originally a C++ adaption of the Borland Delphi
      TRegistry class.

      The Windows implementation is activated by the conditional define
      USE_TOOL_WINDOWS_IMPL appearing in the project settings

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
/*
    This class's function bodies consists of two types - those that are
    OS independent, and those that require specific OS knowledge.
    The OS independent ones are defined here, while the others are
    in seperate CPP's that get #include'd according to conditional defines.
*/
//---------------------------------------------------------------------------
#ifdef _MSC_VER
	#pragma warning (disable : 4786) // identifier truncated in debug info
#endif

#include "tool_registry.h"
#include <stdexcept>
#pragma hdrstop
//---------------------------------------------------------------------------

// Get OS-dependent functions...
#if defined(USE_TOOL_WINDOWS_IMPL)
    // currently Windows is the only OS implemented for this class
    #include "tool_registry_windows.cpp"
#endif
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


TRegistry::TRegistry(void)
{
    d_RootKey = eRootKeyCurrentUser;
    d_Access = eKeyAccessAll;
    d_LazyWrite = true;
	d_CurrentKey = 0;
    d_CloseRootKey = false;
}
//---------------------------------------------------------------------------

TRegistry::TRegistry(unsigned AAccess)
{
    TRegistry();
    d_Access = AAccess;
}
//---------------------------------------------------------------------------

TRegistry::~TRegistry(void)
{
    CloseKey();
}
//---------------------------------------------------------------------------


bool TRegistry::IsRelative(const std::string& Value)
{
    return !((Value!="") && (Value[0] == '\\'));
}
//---------------------------------------------------------------------------


void TRegistry::ChangeKey(void* Value, const std::string& Path)
{
  CloseKey();
  d_CurrentKey = Value;
  d_CurrentPath = Path;
}
//---------------------------------------------------------------------------
/*
void TRegistry::SetCurrentKey(const ERootKey& Value)
{
    d_CurrentKey = Value;
}
//---------------------------------------------------------------------------
*/
void TRegistry::SetRootKey(const ERootKey& Value)
{
    if (GetRootKey() != Value)
    {
        if (d_CloseRootKey)
        {
            ReallyCloseKey(GetRootKey());
            d_CloseRootKey = false;
        }
        d_RootKey = Value;
        CloseKey();
    }
}
//---------------------------------------------------------------------------

void TRegistry::CloseKey(void)
{
    if (GetCurrentKey() !=0)
    {
        if (GetLazyWrite())
            ReallyCloseKey(GetCurrentKey());
        else
            ReallyFlushKey(GetCurrentKey());
        d_CurrentKey = 0;
        d_CurrentPath.resize(0);  // coz MSVC doesn't do clear!
    }
}
//---------------------------------------------------------------------------

//bool TRegistry::CreateKey(const std::string& Key);
//bool TRegistry::DeleteKey(const std::string& Key);
//bool TRegistry::DeleteValue(const std::string& Name);

int TRegistry::GetDataSize(const std::string& ValueName)
{
  TRegDataInfo Info;

  if (GetDataInfo(ValueName, Info))
    return Info.DataSize;
  else
    return -1;
}
//---------------------------------------------------------------------------

TRegDataType TRegistry::GetDataType(const std::string& ValueName)
{
    TRegDataInfo Info;
    if (GetDataInfo(ValueName, Info))
        return Info.RegData;
    else
        return rdUnknown;
}
//---------------------------------------------------------------------------

bool TRegistry::HasSubKeys(void)
{
    TRegKeyInfo Info;
    return GetKeyInfo(Info) && (Info.NumSubKeys > 0);
}
//---------------------------------------------------------------------------

//bool TRegistry::LoadKey(const std::string& Key, const std::string& FileName);
//void TRegistry::MoveKey(const std::string& OldName, const std::string& NewName, bool Delete);
//bool TRegistry::OpenKeyReadOnly(const std::string& Key);

int TRegistry::ReadBinaryData(const std::string& Name, unsigned char* Buffer, int BufSize)
{
    TRegDataType RegData;
    TRegDataInfo Info;

    if (GetDataInfo(Name, Info))
    {
        int size = Info.DataSize;
        RegData = Info.RegData;

        if ((RegData == rdBinary || RegData == rdUnknown) && size <= BufSize)
            GetData(Name, Buffer, size, RegData);
        else
            throw std::runtime_error(("Read Error:InvalidRegType:" + Name).c_str());

        return size;
    }
    else
        return 0;
}
//---------------------------------------------------------------------------

//double TRegistry::ReadCurrency(const std::string& Name);
//bool TRegistry::ReadBool(const std::string& Name);
//tm TRegistry::ReadDate(const std::string& Name);
//tm TRegistry::ReadDateTime(const std::string& Name);
//double TRegistry::ReadFloat(const std::string& Name);

int TRegistry::ReadInteger(const std::string& Name)
{
    TRegDataType RegData;
    int result;
    GetData(Name, reinterpret_cast<unsigned char*>(&result), sizeof(int), RegData);
    if (RegData != rdInteger)
        throw std::runtime_error(("Read Error:InvalidRegType:" + Name).c_str());
    return result;
}
//---------------------------------------------------------------------------

std::string TRegistry::ReadString(const std::string& Name)
{
    TRegDataType RegData;

    int Len = GetDataSize(Name);
    if (Len > 0)
    {
        char* result = new char[Len+1];
        GetData(Name, reinterpret_cast<unsigned char*>(result), Len, RegData);

        if (!(RegData == rdString || RegData == rdExpandString))
            throw std::runtime_error(("Read Error:InvalidRegType:" + Name).c_str());

        std::string sResult = result;
        delete[] result;
        return sResult;
    }
    else
        return "";

}
//---------------------------------------------------------------------------

std::string TRegistry::QuickReadString(
        const ERootKey& rootKey,
        const std::string& keyName,
        const std::string& valueName)
{
    SetRootKey(rootKey);
    if (!OpenKey(keyName, false))
        return "";
    return ReadString(valueName);
}
//---------------------------------------------------------------------------

//tm TRegistry::ReadTime(const std::string& Name);
//bool TRegistry::RegistryConnect(const std::string& UNCName);
//void TRegistry::RenameValue(const std::string& OldName, const std::string& NewName);
//bool TRegistry::ReplaceKey(const std::string& Key, const std::string& FileName, const std::string& BackUpFileName);
//bool TRegistry::RestoreKey(const std::string& Key, const std::string& FileName);
//bool TRegistry::SaveKey(const std::string& Key, const std::string& FileName);
//bool TRegistry::UnLoadKey(const std::string& Key);

bool TRegistry::ValueExists(const std::string& Name)
{
    TRegDataInfo Info;
    return GetDataInfo(Name, Info);
}
//---------------------------------------------------------------------------

void TRegistry::WriteBinaryData(const std::string& Name, unsigned char* Buffer, int BufSize)
{
    PutData(Name, Buffer, BufSize, rdBinary);
}
//---------------------------------------------------------------------------

//void TRegistry::WriteCurrency(const std::string& Name, double Value);
//void TRegistry::WriteBool(const std::string& Name, bool Value);
//void TRegistry::WriteDate(const std::string& Name, const tm& Value);
//void TRegistry::WriteDateTime(const std::string& Name, const tm& Value);

void TRegistry::WriteFloat(const std::string& Name, double Value)
{
    PutData(Name, reinterpret_cast<unsigned char*>(&Value), sizeof(double), rdBinary);
}
//---------------------------------------------------------------------------

void TRegistry::WriteInteger(const std::string& Name, int Value)
{
    PutData(Name, reinterpret_cast<unsigned char*>(&Value), sizeof(int), rdInteger);
}
//---------------------------------------------------------------------------

void TRegistry::WriteString(const std::string& Name, const std::string& Value)
{
    PutData(Name, reinterpret_cast<const unsigned char*>(Value.c_str()), Value.length()+1, rdString);
}
//---------------------------------------------------------------------------

void TRegistry::WriteExpandString(const std::string& Name, const std::string& Value)
{
    PutData(Name, reinterpret_cast<const unsigned char*>(Value.c_str()), Value.length()+1, rdExpandString);
}
//---------------------------------------------------------------------------

//void TRegistry::WriteTime(const std::string& Name, const tm& Value);

} // namespace tool


