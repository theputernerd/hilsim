//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.3 $
//  $RCSfile: tool_mimecodec.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_mimecodec.h/.cpp

  Template - TMimeCodec<>

  Coder-Decoder template for MIME-encoding binary data into an ascii stream.
  Allows complex types to be written and read without loss of accuracy.
  Note that this is not a fully compliant MIME Base64 implementation
  but a loose interpretation of section 5.2 of RFC 1521.  ie the output
  of this routine is probably not actually MIME compliant and is therefore
  only guarunteed to be recoverable by this same routine.

  Usage example:

    // open file in binary mode
    std::ofstream oStore("Store.dst");

    TMimeCodec<double> PosEX = 10.0;  // initialises BinCodec to a double, 10.0
    oStore.write(PosEX, PosEX.size());  // writes a char array representing
                                        // the double to the file.
    // or the new way
    double PosEX = 10.0;
    oStore << TMimeCodec<double>(PosEX); // writes the data in a single line of source
    // or
    oStore << TMimeIO_double(PosEX); // using the typedef defined down the bottom of this file

    oStore.close;

    std::ifstream iStore("Store.dst");
    iStore.read(PosEX, PosEX.size();  // reads a char array representing
                                      // a double from the file
    std::cout << static_cast<double>(PosEX);  // displays the double on the console.

    // or the new way
    double newPos = TMimeCodec<double>(iStore);  // reads the data in a single line of source
    // or
    newPos = TMimeIO_double(iStore);  // using the typedef defined down the bottom of this file
    std::cout << newPos;
*/
//---------------------------------------------------------------------------
#ifndef tool_mimecodecH
#define tool_mimecodecH

#include <iostream>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

template <class T> class TMimeCodec
{
    union u{
        T    data;
        char bytes[sizeof(T)];
    } d_bits;

  public:
    TMimeCodec() {}
    TMimeCodec(const T& in)
    {  d_bits.data = in; }

    TMimeCodec(std::istream& is)
    { read(is); }

    TMimeCodec& operator =(const T& in)
    {   d_bits.data = in;
        return *this; }

    operator T() const
        { return d_bits.data; }

    const T& val() const
        { return d_bits.data; }

    std::ostream& write(std::ostream& os) const
    {
        const int groups = sizeof(T)/3;
        const int rem = sizeof(T) - 3*groups;

        int offset = 0;
        for (int i = 0; i < groups; ++i)
        {
            os << binToChar[(d_bits.bytes[offset] >> 2) & 0x3F];
            os << binToChar[((d_bits.bytes[offset] << 4) & 0x30) | ((d_bits.bytes[offset+1] >> 4) & 0x0F)];
            os << binToChar[((d_bits.bytes[offset+1] << 2) & 0x3C) | ((d_bits.bytes[offset+2] >> 6) & 0x03)];
            os << binToChar[(d_bits.bytes[offset+2] & 0x3F)];
            offset +=3;
        }

        if (rem == 1)
        {
            os << binToChar[(d_bits.bytes[offset] >> 2) & 0x3F];
            os << binToChar[((d_bits.bytes[offset] << 4) & 0x30)];
            os << "==";
        }
        else if (rem == 2)
        {
            os << binToChar[(d_bits.bytes[offset] >> 2) & 0x3F];
            os << binToChar[((d_bits.bytes[offset] << 4) & 0x30) | ((d_bits.bytes[offset+1] >> 4) & 0x0F)];
            os << binToChar[((d_bits.bytes[offset+1] << 2) & 0x3C)];
            os << '=';
        }

        return os;
    }

    std::istream& read(std::istream& is)
    {
        const int groups = sizeof(T)/3;
        const int rem = sizeof(T) - 3*groups;

        int offset = 0;
        for (int i = 0; i < groups; ++i)
        {
            unsigned char c4[4];
            is.read(c4, 4);

            d_bits.bytes[offset] = (charToBin[c4[0]]  << 2) | ((charToBin[c4[1]] >> 4) & 0x03);
            d_bits.bytes[offset+1] = (charToBin[c4[1]] << 4) | ((charToBin[c4[2]] >> 2) & 0x0F);
            d_bits.bytes[offset+2] = (charToBin[c4[2]] << 6) | (charToBin[c4[3]] & 0x3F);
            offset +=3;
        }

        if (rem == 1)
        {
            unsigned char c4[4];
            is.read(c4, 4);

            d_bits.bytes[offset] = (charToBin[c4[0]]  << 2) | ((charToBin[c4[1]] >> 4) & 0x03);
        }
        else if (rem == 2)
        {
            unsigned char c4[4];
            is.read(c4, 4);

            d_bits.bytes[offset] = (charToBin[c4[0]]  << 2) | ((charToBin[c4[1]] >> 4) & 0x03);
            d_bits.bytes[offset+1] = (charToBin[c4[1]] << 4) | ((charToBin[c4[2]] >> 2) & 0x0F);
        }

        return is;
    }

  private:

    // mapping of binary values to the Base64 characters
    static const unsigned char binToChar[];
    // mapping of the Base64 characters (ASCII positioned) to
    // the binary values
    static const unsigned char charToBin[];
};
//---------------------------------------------------------------------------

} // namespace tool
//---------------------------------------------------------------------------

template <typename T>
inline std::ostream& operator << (std::ostream& os, const tool::TMimeCodec<T>& v)
{ return v.write(os); }
//---------------------------------------------------------------------------

template <typename T>
inline std::istream& operator >> (std::istream& is, const tool::TMimeCodec<T>& v)
{ return v.read(is); }
//---------------------------------------------------------------------------


namespace tool {

// some useful typedefs for I/O of built-in types.
typedef TMimeCodec<int>      TMimeIO_int;
typedef TMimeCodec<double>   TMimeIO_double;
typedef TMimeCodec<char>     TMimeIO_char;

} // namespace tool
//---------------------------------------------------------------------------

// mapping of binary values to the Base64 characters
template <typename T> const unsigned char tool::TMimeCodec<T>::binToChar[64] =
 {  'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'
 };
//---------------------------------------------------------------------------

// mapping of the Base64 characters (ASCII positioned) to
// the binary values
template <typename T> const unsigned char tool::TMimeCodec<T>::charToBin[256] =
 {  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  0
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  1
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  2
    0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //  3
    0,  0,  0, 62,  0,  0,  0, 63, 52, 53,  //  4
   54, 55, 56, 57, 58, 59, 60, 61,  0,  0,  //  5
    0,  0,  0,  0,  0,  0,  1,  2,  3,  4,  //  6
    5,  6,  7,  8,  9, 10, 11, 12, 13, 14,  //  7
   15, 16, 17, 18, 19, 20, 21, 22, 23, 24,  //  8
   25,  0,  0,  0,  0,  0,  0, 26, 27, 28,  //  9
   29, 30, 31, 32, 33, 34, 35, 36, 37, 38,  // 10
   39, 40, 41, 42, 43, 44, 45, 46, 47, 48,  // 11
   49, 50, 51,  0,  0,  0,  0,  0           // 12
 };
//---------------------------------------------------------------------------


#endif