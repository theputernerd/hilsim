//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_resource.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_resource.h/.cpp

      Implements the "Resource" design pattern which performs reference
      counting on itself, deleting itself when the count reaches zero.
      The design pattern is for classes that require such behaviour to
      inherit publicly from this one, so that the behaviour is inherited.
      Users of this reference counted class or its derived classes must
      call ref() and unRef() to allow the class to keep track of what is
      referencing it.

*/
//---------------------------------------------------------------------------
#ifndef	tool_resourceH
#define tool_resourceH

#include <assert.h>

//#define TOOL_ENABLE_DELAYED_RESOURCE_DELETION

namespace tool {

class Resource
{
  public:
	Resource() : m_refcount(0) {}

	void ref()
        { m_refcount++;	}
	void unRef()
	{
        assert(m_refcount > 0);
        if (--m_refcount == 0)
        #ifdef TOOL_ENABLE_DELAYED_RESOURCE_DELETION
            addDelete(this);
        #else
            delete this;
        #endif
	}

    #ifdef TOOL_ENABLE_DELAYED_RESOURCE_DELETION
	static void flush()
	{
		for (int i = 0; i < m_countToDelete; i++)
			delete m_pRsrcToDelete[i];
		m_countToDelete = 0;
	}
    #endif

  protected:
	virtual ~Resource();  // if this is private, it seems that destructors
                          // if child classes won't compile.

    // it is occasionally useful for the derived class to know how many
    // references its got.
    short getRefCount() { return m_refcount; }

  private:
	short m_refcount;
    #ifdef TOOL_ENABLE_DELAYED_RESOURCE_DELETION
	enum { MaxEntries = 100	};
	static Resource* m_pRsrcToDelete[MaxEntries];
	static short m_countToDelete;

	void addDelete(Resource* pRes)
    {
        using namespace std; // MSVC Compat
		if (m_countToDelete == MaxEntries) {
			cout << "Overflow" << endl;
			delete pRes; //don't delay the deletion any longer
		}
		else {
			m_pRsrcToDelete[m_countToDelete++] = pRes;
		}
	}
    #endif
};

} // namespace tool
#endif
