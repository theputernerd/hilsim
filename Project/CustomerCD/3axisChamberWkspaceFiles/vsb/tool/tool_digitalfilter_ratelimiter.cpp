//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_ratelimiter.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_ratelimiter.h/.cpp
      Template implementation of a rate limiter using a simple
      derivative estimator
*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_ratelimiterCPP
#define tool_digitalfilter_ratelimiterCPP

#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_digitalfilter_ratelimiter.h"

#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif
#include <cmath>
#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#define export  // MSVC doesn't yet support the "export" keyword
    // NOTE MATCHING UNDEF AT BOTTOM OF FILE
#endif
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterRateLimiter<T>::setRateLimits(
            const T& lowerBound,
            const T& upperBound
            )
{
    d_lowerRateLimit = lowerBound;
    d_upperRateLimit = upperBound;
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterRateLimiter<T>::reset(
            const double& time,
            const T& inValue
            )
{
    d_inValue = inValue;
    d_inTime = time;
    d_lastInValue = d_inValue;
    d_lastInTime = d_inTime;

    d_outValue = T();
    d_outTime = time;

    d_deriv.reset(time, inValue);
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterRateLimiter<T>::inputValue(
			const double& time,
			const T& inValue
            )
{
    // time must not go backwards (can stand still though)
    if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Derivative Filter: Time travelling backwards in filter.");

    // record the new input with its timestamp
    d_inTime = time;
    d_inValue = inValue;

    d_deriv.inputValue(time, inValue);
}
//---------------------------------------------------------------------------


export template<typename T>
void DigitalFilterRateLimiter<T>::calculateOutput(const double& time)
{
    // time must not go backwards (can stand still though)
	if (time < d_outTime || time < d_inTime)
        throw std::logic_error("Derivative Filter: Time travelling backwards in filter.");

    // this filter can't push forward in time, so we can't calculate
    // an output at a time greater than the last input...
    if (time > d_inTime)
        throw std::logic_error("Derivative Filter: Request to calculate output at time greater than last input");

    // are the previous inputs to be copied off or overwritten?
    if (time > d_outTime && d_inTime > d_outTime)
    {
        // filter has moved on in time, need to copy off the
        // previous input values
        d_lastInValue = d_inValue;
        d_lastInTime = d_inTime;
    }

    // do filter calculations for time of input, ignoring the time that
    // was passed to calculate call because it was early tested to be an
    // error if its not the same as d_inTime...
    double timestep = d_inTime - d_lastInTime;

    // Other than for the very the first pass of the filter,
    // timestep will always be greater than zero, because the previous
    // input & output values are not updated until a new input is
    // received (as opposed to the 'current' output which is given a new value
    // every time calculate is called).  This ensures that previous
    // outputs and inputs are always kept in the past.

    if (timestep == 0.0)
        // first pass of the filter, don't change anything (because initial
        // values were set in the reset function)
        return;

    d_deriv.calculateOutput(time);

    d_outValue =
        d_lastInValue +
        timestep *
        tool::limit(d_deriv.outputValue(), d_lowerRateLimit, d_upperRateLimit);

    d_outTime = d_inTime;
}
//---------------------------------------------------------------------------


} // end namespace tool
//---------------------------------------------------------------------------

//#define ENABLE_BUILTIN_TESTS
#ifdef ENABLE_BUILTIN_TESTS

#include "tool_arraytemplate.h"
#include "../marsbase/mars_vector3.h"
// Test declaration
namespace {

void compileTestFunc()
{
    tool::DigitalFilterRateLimiter<double> filter1;
    filter1.setRateLimits(-3.0, 3.0);
    filter1.reset(0.0, 0.0);
    filter1.inputValue(1.0, 3.0);
    filter1.calculateOutput(1.0);
    double val = filter1.outputValue();

    typedef tool::TArrayTemplate<double, 2> Vector2;
    tool::DigitalFilterRateLimiter<Vector2> filter2;
    Vector2 limit2;
    limit2[0] = 3.0;
    limit2[1] = 4.0;
    filter2.setRateLimits(-limit2, limit2);
    filter2.reset(
            0.0, Vector2());
    Vector2 inVal;
    inVal[0] = 3.0;
    inVal[1] = 4.0;
    filter2.inputValue(1.0, inVal);
    filter2.calculateOutput(1.0);
    Vector2 val2 = filter2.outputValue();

    tool::DigitalFilterRateLimiter<Mars::TVector3> filter3;
    Mars::TVector3 limit3(3.0,4.0,5.0);
    filter3.setRateLimits(-limit3, limit3);
    filter3.reset(
            0.0, Mars::TVector3());
    filter3.inputValue(1.0, Mars::TVector3(3.0, 4.0, 5.0));
    filter3.calculateOutput(1.0);
    Mars::TVector3 val3 = filter3.outputValue();
}


} // unnamed namespace
//---------------------------------------------------------------------------
#endif // ENABLE_BUILTIN_TESTS

#if (_MSC_VER <= 1300)
	// MSVC 7.0 & earlier
	#undef export
#endif

#endif // tool_digitalfilter_ratelimiterCPP
