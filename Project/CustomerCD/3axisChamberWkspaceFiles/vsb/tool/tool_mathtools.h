//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.27 $
//  $RCSfile: tool_mathtools.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_mathtools.h/.cpp
      Useful mathematical functions.
      Transformations, physical constants and conversion constants
*/
//---------------------------------------------------------------------------
#ifndef tool_mathtoolsH
#define tool_mathtoolsH

// min and max are defined as macro functions in Visual C++ 6.0, "windef.h"
// NOMINMAX is Visual C++ specific and prevents these macros from being defined.
// Undefine macros here anyway and further onwe implement min and max as
// template functions for stricter type checking

#define NOMINMAX
#undef min
#undef max

#ifndef valarraySTL
    #include <valarray>
    #define valarraySTL
#endif
#ifndef vectorSTL
    #include <vector>
    #define vectorSTL
#endif

namespace tool {

namespace constants {
    // Physical Constants
    extern const double G;          // standard gravity, m/s^2
    extern const double PI;         // pi
    extern const double TWO_PI;     // 2*pi
    extern const double HALF_PI;    // pi/2

    extern const double SQRT2;      // square root of 2

    extern const double T0;         // air temperature, sea level, deg K
    extern const double R0;         // air density, sea level, kg/m^3
    extern const double P0;         // air pressure, sea level, kg/m^2
    extern const double A0;         // speed of sound, sea level, m/s

    extern const double K;          // Boltzman's Constant
    extern const double C;          // Speed of light, m/s

    // Unit Conversion Constants
    // Simply multiply by the appropriate constant

    // Angles
    extern const double RAD_TO_DEG; // radians <=> degrees
    extern const double DEG_TO_RAD;
    extern const double RAD_TO_MILS;// radians <=> mils
    extern const double MILS_TO_RAD;

    // Acceleration
    extern const double G_TO_MPS2;  // m/s^2 <=> g's
    extern const double MPS2_TO_G;

    // Length
    extern const double FT_TO_M;    // feet <=> metres
    extern const double M_TO_FT;
    extern const double KFT_TO_M;   // kilofeet <=> metres
    extern const double M_TO_KFT;
    extern const double NMI_TO_M;   // nautical miles <=> metres
    extern const double M_TO_NMI;
    extern const double MI_TO_M;    // miles <=> metres
    extern const double M_TO_MI;
    extern const double KM_TO_M;    // kilometres <=> metres
    extern const double M_TO_KM;

    // Speed
    extern const double KTS_TO_MPS; // knots <=> m/s
    extern const double MPS_TO_KTS;
    extern const double KPH_TO_MPS; // km/h <=> m/s
    extern const double MPS_TO_KPH;

    // Mass
    extern const double LB_TO_KG;   // pounds <=> kilograms
    extern const double KG_TO_LB;

    // Force
    extern const double LBF_TO_N;   // pound-force <=> Newtons
    extern const double N_TO_LBF;

    // Time
    extern const double SEC_TO_MIN; // minutes <=> seconds
    extern const double MIN_TO_SEC;
    extern const double SEC_TO_MILLISEC;

    // Temperature
    double CEL_TO_FAH(const double x); // deg Celcius <=> deg Fahrenheit
    double FAH_TO_CEL(const double x);

    double CEL_TO_KEL(const double x); // deg Celcius <=> kelvins
    double KEL_TO_CEL(const double x);
}; // namespace constants
//---------------------------------------------------------------------------

// Mathematical functions implemented as templates

// sq, return the square of x
template <class T> inline T sq(const T& x)
    { return x*x; }

// MIN, return the minimum of x and y
template <class T> inline T min(const T& x, const T& y)
    { return (x < y) ? x : y; }

// MAX, return the maximum of x and y
template <class T> inline T max(const T& x, const T& y)
    { return (x > y) ? x : y; }

// SIGN, return -1 if negative x, otherwise +1
template <class T> inline T sign(const T& x)
    { return (x < 0) ? -1 : 1; }

// LIMIT, restrict value x to lower limit min and upper limit max
template <class T> inline T limit(const T& arg, const T& min, const T& max)
    { return (arg < min) ? min : (arg > max) ? max : arg; }

// LIMIT, restrict value x to +/- lim
template <class T> inline T limit(const T& arg, const T& lim)
    { return (arg < -lim) ? -lim : (arg > lim) ? lim : arg; }

// RANGE, return 1 if x is inside range defined by y and z, 0 otherwise
template <class T> inline T range(const T& x, const T& y, const T& z)
    { return (x < y) ? 0 : (x > z) ? 0 : 1; }

// NONZERO, return 1e-10 if x is zero, otherwise x
template <class T> inline T nonzero(const T& x)
    { return (x == 0) ? 1e-10 : x; }

// Absolute value of x, x can be integer or real
template <class T> inline T abs(const T& x)
    { return (x < 0) ? -x : x; }
//---------------------------------------------------------------------------

// Mathematical functions implemented as normal functions
// The implementations are given in the file "mathtools.cpp"

// EUCLID2, return the Euclidean length of x and y
double euclid2(const double x, const double y);

// EUCLID3, return the Euclidean length of x, y and z
double euclid3(const double x, const double y, const double z);

// Round value of x up or down to nearest integer
double round(const double x);

// Round value of x up or down to specified precision
// eg. precision of 1 will round off to nearest integer
//     precision of 0.01 will round off to 2 decimal places
double roundTo(const double x, const double precision);
//---------------------------------------------------------------------------

// Trigonometry

// PiMod, returns x equivalent angle in range -PI..+PI radians
double piMod(const double x);

// These inverse trig functions limit the argument to +/- 1 for safety
double aCos(const double x);
double aSin(const double x);

// ATan2, checks for x=y=0 argument before calling atan2, if x=y=0 return is 0
double aTan2(const double x, const double y);
//---------------------------------------------------------------------------

// Vector Represenation

// SphericalToCartesian,
// Converts Spherical Coordinates r,psi,theta to Cartesian Coordinates x,y,z
void sphericalToCartesian(
        const double r,
        const double psi, const double theta,
        double *x,double *y,double *z
        );

// CartesianToSpherical,
// Converts Cartesian Coordinates x,y,z to Spherical Coordinates r,psi,theta
void cartesianToSpherical(
        const double x, const double y, const double z,
        double *r, double *psi, double *theta
        );
//---------------------------------------------------------------------------

// Euler Transformations

// Transform32, Euler rotations about axis 3 (Z) and axis 2 (Y)
// Vector (x1,y1,z1) in, vector (x2,y2,z2) out
void transform32(
        const double x1, const double y1, const double z1,
        const double psi, const double theta,
        double *x2, double *y2, double *z2
        );

// InvTransform32, Inverse Transformation of above
void invTransform32(
        const double x1, const double y1, const double z1,
        const double psi, const double theta,
        double *x2, double *y2, double *z2
        );

// Transform321, Euler rotations about axis 3 (Z), axis 2 (Y) and axis 1 (X)
// Vector (x1,y1,z1) in, vector (x2,y2,z2) out
void transform321(
        const double x1, const double y1, const double z1,
        const double psi, const double theta, const double phi,
        double *x2, double *y2, double *z2
        );

// Transform321, Euler rotations about axis 3 (Z), axis 2 (Y) and axis 1 (X)
// Vector (x1,y1,z1) in, vector (x2,y2,z2) out
void transform321(
        const double x1, const double y1, const double z1,
        const double psi, const double theta, const double phi,
        double *x2, double *y2, double *z2
        );

// InvTransform321, Inverse Transformation of above
void invTransform321(
        const double x1, const double y1, const double z1,
        const double psi, const double theta, const double phi,
        double *x2, double *y2, double *z2
        );

// RotOrthogRateToEuler321()
// Converts orthogonal rates in the rotated frame to equivalent 321 Euler rates
void rotOrthogRateToEuler321(
        const double p, const double q, const double r,
        const double theta, const double phi,
        double *psidt, double *thetadt, double *phidt
        );

// RefOrthogRateToEuler321()
// Converts orthogonal rates in the reference frame (E) to equivalent 321 Euler rates
void refOrthogRateToEuler321(
        const double pE, const double qE, const double rE,
        const double psi, const double theta,
        double *psidt, double *thetadt, double *phidt
        );

// EulerRate321ToOrthogRot()
// Converts 321 Euler rates to orthogonal rates in the rotated frame
void eulerRate321ToOrthogRot(
        const double psidt, const double thetadt, const double phidt,
        const double theta, const double phi,
        double *p, double *q, double *r
        );

// EulerRate321ToOrthogRef()
// Converts 321 Euler rates to orthogonal rates in the reference frame (E)
void eulerRate321ToOrthogRef(
        const double psidt, const double thetadt, const double phidt,
        const double psi, const double theta,
        double *pE, double *qE, double *rE
        );

// quaternionToEuler321()
// Calculates Euler angles, 321 rotation sequence, from the input quaternion
void quaternionToEuler321(double e0, double e1, double e2, double e3,
                                 double* psi,double* theta,double* phi);

// euler321ToQuaternion()
// Calculates quaternion from input Euler angles, 321 rotation sequence
void euler321ToQuaternion(double psi,double theta,double phi,
                          double* e0, double* e1, double* e2, double* e3);

//convert quaternion to direction cosine matrix
void quaternionToMatrix(const double& e0,const double& e1,
                        const double& e2,const double& e3,
                        double& l1,double& l2,double& l3,
                        double& m1,double& m2,double& m3,
                        double& n1,double& n2,double& n3);
//---------------------------------------------------------------------------

// Binary Search functions to find location of a numeric value in an array
// These are used by interpolation routines below and are also useful for
// customised interpolation routines.
// Return is false if x is outside array bounds, otherwise true

// --- double array variant
bool binarySearch(const double x,const double* v,const int n,int *il, int *im, double *d);

// --- std::valarray variant
bool binarySearch(const double x,const std::valarray<double>& v,int *il, int *im, double *d);

// --- std::vector variant
bool binarySearch(const double x,const std::vector<double>& v,int *il, int *im, double *d);
//---------------------------------------------------------------------------

// Interpolation Routines

// 1-D Interpolation
// xArray is the independent variable array.
// yArray is the dependent variable array.
// n is the size of xArray and yArray (they should be the same size!).
// x is the value of the independent variable to be interpolated.
// Return value is the interpolated dependent variable from yArray.
double interpolate1D(
        const double x, const int n,
        const double* xArray, const double* yArray
        );
// --- std::valarray<double> variant....
double interpolate1D(
        const double x,
        const std::valarray<double>& xArray,
        const std::valarray<double>& yArray
        );
// --- std::vector<double> variant....
double interpolate1D(
        const double x,
        const std::vector<double>& xArray,
        const std::vector<double>& yArray
        );
//---------------------------------------------------------------------------

// 2-D Interpolation
// x1Array is the first independent variable array.
// x2Array is the second independent variable array.
// yArray is the dependent variable array.
// n1 is the size of x1Array.
// n2 is the size of x2Array.
// yArray has the length n1*n2. Data in yArray is arranged as n2 sets of n1 values.
// x1 is the value of the first independent variable to be interpolated.
// x2 is the value of the second independent variable to be interpolated.
// Return value is the interpolated dependent variable from yArray.
double interpolate2D(
        const double x1, const int n1,
        const double* x1Array,
        const double x2, const int n2,
        const double* x2Array, const double* yArray
        );
//---------------------------------------------------------------------------

}; // namespace tool

#endif
