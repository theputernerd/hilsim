//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:53:59 $
//  $Revision: 1.1 $
//  $RCSfile: tool_registry.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_registry.h/.cpp

      TRegistry
        - implements a wrapper for a limited set of Windows Registry
          function calls.
        - Header is standard C++ code in order to support many platforms
          by allowing selection of alternate implementations.

      ** Note **  This is originally a C++ adaption of the Borland Delphi
      TRegistry class.

      The Windows implementation is activated by the conditional define
      USE_TOOL_WINDOWS_IMPL appearing in the project settings

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
#ifndef tool_registryH
#define tool_registryH
//---------------------------------------------------------------------------

#ifndef tool_datetimeH
    #include "tool_datetime.h"
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef listSTL
    #include <list>
    #define listSTL
#endif
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------


struct TRegKeyInfo
{
	unsigned long NumSubKeys;
	unsigned long MaxSubKeyLen;
	unsigned long NumValues;
	unsigned long MaxValueLen;
	unsigned long MaxDataLen;
	TDateTime FileTime;

    TRegKeyInfo()
     :  NumSubKeys(0), MaxSubKeyLen(0), NumValues(0),
        MaxValueLen(0), MaxDataLen(0)
    { }
};
//---------------------------------------------------------------------------

enum TRegDataType { rdUnknown, rdString, rdExpandString, rdInteger, rdBinary };

// Root key enumeration
enum ERootKey {
        eRootKeyClassesRoot = 0x80000000,
        eRootKeyLocalMachine = 0x80000001,
        eRootKeyCurrentUser = 0x80000002,
        eRootKeyUsers = 0x80000003
        };

// Access enumeration
enum EKeyAccess {
        eKeyAccessQueryValue = 0x0001,
        eKeyAccessSetValue = 0x0002,
        eKeyAccessCreateSubKey = 0x0004,
        eKeyAccessEnumerateSubKeys = 0x0008,
        eKeyAccessNotify = 0x0010,
        eKeyAccessCreateLink = 0x0020,
        eKeyAccessRead = 0x0019,
        eKeyAccessWrite = 0x0006,
        eKeyAccessExecute = 0x0019,
        eKeyAccessAll = 0x003F,
        eKeyAccessDUMMYDONOTUSE = 0xFFFF
        };

typedef std::list<std::string> TStrings;
//---------------------------------------------------------------------------

struct TRegDataInfo
{
	TRegDataType RegData;
	unsigned long DataSize;
} ;
//---------------------------------------------------------------------------


class TRegistry
{
  public:
	TRegistry(void)/* overload */;
	TRegistry(unsigned AAccess)/* overload */;
	virtual ~TRegistry(void);

  // accessor/manipulators...
	void*& GetCurrentKey() { return d_CurrentKey; }
	std::string GetCurrentPath() { return d_CurrentPath; }
	bool GetLazyWrite() { return d_LazyWrite; }
	void SetLazyWrite(bool in) { d_LazyWrite = in; }
	ERootKey& GetRootKey() { return d_RootKey; }
	void SetRootKey(const ERootKey& Value);
	unsigned GetAccess() { return d_Access; }
	void SetAccess(unsigned in) { d_Access = in; }

  // key manipulations...
	bool CreateKey(const std::string& Key);
	bool OpenKey(const std::string& Key, bool CanCreate);
	bool OpenKeyReadOnly(const std::string& Key);
	void CloseKey(void);
	void MoveKey(const std::string& OldName, const std::string& NewName, bool Delete);
	bool ReplaceKey(const std::string& Key, const std::string& FileName, const std::string& BackUpFileName);
	bool RestoreKey(const std::string& Key, const std::string& FileName);
	bool LoadKey(const std::string& Key, const std::string& FileName);
	bool SaveKey(const std::string& Key, const std::string& FileName);
	bool UnLoadKey(const std::string& Key);
	bool DeleteKey(const std::string& Key);
	bool RegistryConnect(const std::string& UNCName);

	void RenameValue(const std::string& OldName, const std::string& NewName);

  // key and value-type queries...
	bool GetDataInfo(const std::string& ValueName, TRegDataInfo &Value);
	int GetDataSize(const std::string& ValueName);
	TRegDataType GetDataType(const std::string& ValueName);
	bool GetKeyInfo(TRegKeyInfo &Value);
	void GetKeyNames(tool::TStrings& Strings);
	void GetValueNames(tool::TStrings& Strings);
	bool HasSubKeys(void);
	bool KeyExists(const std::string& Key);
	bool ValueExists(const std::string& Name);
	bool DeleteValue(const std::string& Name);

  // read functions...
	double ReadCurrency(const std::string& Name);
	int ReadBinaryData(const std::string& Name, unsigned char* Buffer, int BufSize);
	bool ReadBool(const std::string& Name);
	tm ReadDate(const std::string& Name);
	tm ReadDateTime(const std::string& Name);
	double ReadFloat(const std::string& Name);
	int ReadInteger(const std::string& Name);
	std::string ReadString(const std::string& Name);
    std::string QuickReadString(
            const ERootKey& rootKey,
            const std::string& keyName,
            const std::string& valueName
            );
     // note - the QuickRead function changes the current key of this object.
	tm ReadTime(const std::string& Name);

  // write functions...
	void WriteCurrency(const std::string& Name, double Value);
	void WriteBinaryData(const std::string& Name, unsigned char* Buffer, int BufSize);
	void WriteBool(const std::string& Name, bool Value);
	void WriteDate(const std::string& Name, const tm& Value);
	void WriteDateTime(const std::string& Name, const tm& Value);
	void WriteFloat(const std::string& Name, double Value);
	void WriteInteger(const std::string& Name, int Value);
	void WriteString(const std::string& Name, const std::string& Value);
	void WriteExpandString(const std::string& Name, const std::string& Value);
	void WriteTime(const std::string& Name, const tm& Value);

  private:
	void* d_CurrentKey;
	ERootKey d_RootKey;
	bool d_LazyWrite;
	std::string d_CurrentPath;
	bool d_CloseRootKey;
	unsigned d_Access;

    bool IsRelative(const std::string& Value);

    // The following methods are only implemented by some OS-specific
    // implementations, but must appear here to give these access to the
    // private declarations.
    void ReallyCloseKey(void*& key);
    void ReallyCloseKey(ERootKey& key);
    void ReallyFlushKey(void*& key);
    void ReallyFlushKey(ERootKey& key);
    void* convertRootKeyToWindows(void* key);
    void* convertRootKeyFromWindows(void*& key);
    unsigned int convertKeyAccessToWindows(unsigned int access);
    unsigned int convertKeyAccessFromWindows(unsigned int access);

  protected:
	void ChangeKey(void* Value, const std::string& Path);
	void* GetBaseKey(bool Relative);
	int GetData(const std::string& Name, unsigned char* Buffer, unsigned long BufSize, TRegDataType &RegData);
	void* GetKey(const std::string& Key);
	void PutData(const std::string& Name, const unsigned char* Buffer, unsigned long BufSize, TRegDataType RegData);
//	void SetCurrentKey(const ERootKey& Value);

};

//---------------------------------------------------------------------------

} // namespace tool

#endif
