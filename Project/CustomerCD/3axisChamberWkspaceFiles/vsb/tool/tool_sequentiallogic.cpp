//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_sequentiallogic.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_sequentiallogic.h/.cpp
        Collection of useful sequential logic classes.
        Currently includes rising and falling edge detectors.
*/
//---------------------------------------------------------------------------
#include "tool_sequentiallogic.h"

#include <stdexcept>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// Rising Edge Detector
void LogicRisingEdgeDetector::reset(const double& time, const bool& state)
{
    d_currentState = state;
    d_currentTime = time;
    d_lastState = d_currentState;
}
//---------------------------------------------------------------------------


bool LogicRisingEdgeDetector::operator()(const double& time, const bool& state)
{
    if (time > d_currentTime)
    {
        // time has moved on, shuffle down the history...
        d_lastState = d_currentState;
        d_currentState = state;
        d_currentTime = time;

        // return the test
        return !d_lastState && state;
    }
    else if (time == d_currentTime)
    {
        // check if this class is being used correctly and if so return the
        // test...
        if (state != d_currentState)
            throw std::logic_error("Sequential Logic Error: input state changed without time changing");
        else
            return !d_lastState && state;
    }
    else
        throw std::logic_error("Sequential Logic Error: Time travelling backwards!");
}
//---------------------------------------------------------------------------


/////////////////////////////////////////////////////////////////////////////
// Falling Edge Detector
void LogicFallingEdgeDetector::reset(const double& time, const bool& state)
{
    d_currentState = state;
    d_currentTime = time;
    d_lastState = d_currentState;
}
//---------------------------------------------------------------------------


bool LogicFallingEdgeDetector::operator()(const double& time, const bool& state)
{
    if (time > d_currentTime)
    {
        // time has moved on, shuffle down the history...
        d_lastState = d_currentState;
        d_currentState = state;
        d_currentTime = time;

        // return the test
        return d_lastState && !state;
    }
    else if (time == d_currentTime)
    {
        // check if this class is being used correctly and if so return the
        // test...
        if (state != d_currentState)
            throw std::logic_error("Sequential Logic Error: input state changed without time changing");
        else
            return d_lastState && !state;
    }
    else
        throw std::logic_error("Sequential Logic Error: Time travelling backwards!");
}
//---------------------------------------------------------------------------



} // namespace tool

