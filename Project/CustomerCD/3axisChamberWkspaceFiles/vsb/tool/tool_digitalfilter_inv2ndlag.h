//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:44:26 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_inv2ndlag.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_inv2ndlag.h/.cpp
      Template implementation of an Inverse 2nd Order Lag Estimator.

      Estimates what input to a 2nd Order Lag filter would have been,
      given the 2nd order response.  Inverts the general purpose
      2nd Order Lag transfer function,

          y              w�          where  w = natural frequency
         ---  =   ----------------          d = damping ratio
          x        s� + 2dws + w�

      to get time-domain inverse function,

        x(t)  =  y''(t) + 2dw.y'(t) + w�.y(t)
                -----------------------------
                             w�

      Uses DigitalFilterDerivative to obtain necessary estimates of
      derivatives to evaluate the time domain inverse function.
*/
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_inv2ndlagH
#define tool_digitalfilter_inv2ndlagH
//---------------------------------------------------------------------------

#include "tool_digitalfilter_derivative.h"
//---------------------------------------------------------------------------

namespace tool {

template <typename T>
class DigitalFilterInv2ndOrderLag
{
  public:

    DigitalFilterInv2ndOrderLag() { reset(0.0, T()); }
    ~DigitalFilterInv2ndOrderLag() {}

    void reset(
            const double& time,
            const T& inValue
            );

    void setParameters(
            const double& rNaturalFrequency,  // rad/s
            const double& rDamping
            );

    void inputValue(
            const double& time,
            const T& inValue
            );

    void calculateOutput(const double& time);

    const T& outputValue() const { return d_outValue; }

  private:

    T d_inValue;
    double d_inTime;

    T d_outValue;
    double d_outTime;

    tool::DigitalFilterDerivative<T> d_dot;
    tool::DigitalFilterDerivative<T> d_dotdot;

    double d_twoOmegaD;
    double d_omegaSq;
    double d_invOmegaSq;  // stored for efficiency
};
//---------------------------------------------------------------------------

}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_inv2ndlag.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_inv2ndlag.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
