//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.8 $
//  $RCSfile: tool_lookuptable.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_lookuptable.h/.cpp

      nD Data Tables based on the "lota" file format - see sample lota file
      in tool directory.

      Supports repeating intervals of symmetry in the table on any axis,
      including the optional negation of the lookup result if the number
      of reflections about the value of symmetry was odd.
      Symmetry typically used for aero tables symmetric in roll angle.

          Original author - Duncan Fletcher
*/
//---------------------------------------------------------------------------
#ifndef tool_lookuptableH
#define tool_lookuptableH

#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif
#ifndef vectorSTL
    #include <vector>
    #define vectorSTL
#endif
#ifndef dequeSTL
    #include <deque>
    #define dequeSTL
#endif
#ifndef iosfwdSTL
    #include <iosfwd>
    #define iosfwdSTL
#endif
#ifndef tool_smartptrH
    #include "tool_smartptr.h"
#endif
//---------------------------------------------------------------------------

namespace tool {

/*! \brief nD lookup table class that loads from a stream and performs
            queries on the loaded table

    Loads tables from the custom designed "lota" format (ascii only required)
*/
class TLookupTable
{
  public:
    //! \name Constructors and Destructor
    //@{
    //! Loads table from stream without caring how many dimensions the table has
    TLookupTable(std::istream& is);
    //! Loads table from stream throwing an exception if the table has the wrong dimensions
    TLookupTable(std::istream& is, const int& expectedDimensions);
    /*! \brief Populate table from supplied data.  Does not populate names, etc

        Note that storage for the yData is in X1 first, X2 second, etc so
        that for example the first 3 elements in the yData array correspond
        to the first 3 X1 values for the first X2..Xn values.  This is
        consistent with Fortran ordering, rather than C as might be expected.
    */
    TLookupTable(
        const std::vector<const double*>& breakpoints,
        const std::vector<int>& breakpointVectorLengths,
        const double* yData);
    ~TLookupTable();
    //@}

    // forward declaration
    class TLookupMeme;

    //! \name Information Accessors
    //@{
    std::string getTableName() const { return d_tableName; }
    std::string getVariantTag() const { return d_variantTag; }
    std::string getTableComment() const { return d_tableComment; }
    int         getTableDimensions() const { return d_tableDimensions; }
    std::vector<std::string> getBreakpointNames() const { return d_breakpointNames; }
    std::vector<std::vector<double> > getBreakpoints() const { return d_breakpoints; }
    std::deque<bool> getBreakpointSymmetryFlags() const { return d_breakpointSymmFlags; }
    std::deque<bool> getBreakpointNegOddSymmetryFlags() const { return d_breakpointNegOddSymmFlags; }
    std::vector<double> getBreakpointSymmetryValues() const { return d_breakpointSymmVals; }
    //@}

    //! \name Query Methods
    //@{
    //! Usage example (3D table): cn_table.getY(300.0)(2.3)(0.4).into(&cn, false)
    TLookupMeme getY(const double& X1) const;
        // declared inline below because parser couldn't find meme constructor

    /*! \brief Long-hand query method.  Actually used by the TLookupMeme helper
        class.

        Query is performed using a binary search of the breakpoint vectors.

        If inputs vector does not have the same dimension as the table, a
        std::runtime_error is thrown.

        Default for values outside of bounds of table is zero-order
        extrapolation, however first order extrapolation is an option.

        Result value is returned through the Y parameter.

        \return  Boolean return value is true if requested Y value was within
                 the bounds of the table, false if it was calculated by
                 extrapolating outside the bounds of the table.
    */
    bool getY(
        double& Y,
        const std::vector<double>& inputs,
        const bool& extrapFirstOrder = false) const;
    //@}

    //! \name Information Manipulators
    //@{
    //! Table name must be a well formed identifier.
    void setTableName(const std::string& name);
    void setVariantTag(const std::string& variant) { d_variantTag = variant; }
    void setTableComment(const std::string& cm) { d_tableComment = cm; }

    /*! \brief Sets the breakpoints and names then invalidates the table data.

        Breakpoint names must be well formed identifiers

        Lookups will give undefined results after calling this function.
        Will throw an exception if the number of breakpoint vectors and the
        number of breakpoint names does not match.*/
    void setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints,
        const std::vector<std::string>& breakpointNames);

    /*! \brief Sets the breakpoints then invalidates the table data.

        Lookups will give undefined results after calling this function since
        the table is resized to correspond to the supplied breakpoint vector
        dimensions.
        Will throw an exception if the number of breakpoint vectors and the
        number of existing breakpoint names does not match.*/
    void setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints);

    /*! \brief Sets the breakpoints, names and symmetry information then
               invalidates the table data.

        Breakpoint names must be well formed identifiers

        Lookups will give undefined results after calling this function.
        Will throw an exception if the number of breakpoint vectors and the
        number of breakpoint names, symmetry flags and symmetry values
        does not match.

        Note that the NegOddSymmetryFlags determines whether
        the result will be negated if the index is reflected about the
        value of symmetry an odd number of times. */
    void setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints,
        const std::vector<std::string>& breakpointNames,
        const std::deque<bool>& breakpointSymmetryFlags,
        const std::deque<bool>& breakpointNegOddSymmetryFlags,
        const std::vector<double>& breakpointSymmetryValues);

    /*! \brief Sets the breakpoint names.

        Breakpoint names must be well formed identifiers

        Will throw an exception if the length of the vector
		is not the same as the existing number of breakpoints.*/
    void setBreakpointNames(
        const std::vector<std::string>& breakpointNames);

    /*! \brief Sets the breakpoint symmetry information

        Will throw an exception if the length of the vectors
		are not the same as the existing number of breakpoints.

        Note that the NegOddSymmetryFlags determines whether
        the result will be negated if the index is reflected about the
        value of symmetry an odd number of times. */
    void setBreakpointSymmetry(
        const std::deque<bool>& breakpointSymmetryFlags,
        const std::deque<bool>& breakpointNegOddSymmetryFlags,
        const std::vector<double>& breakpointSymmetryValues);
    //@}

    /*! \brief Set method for table's main data.

        \param yData Must be an array of some sort.  Use of a template means
                     that theoretically any type that supports operator[] can
                     be used. e.g. std::vector<double>, double*, etc.
                     Storage is in X1 first, X2 second, etc so that for example
                     the first 3 elements in the yData array correspond to the
                     first 3 X1 values for the first X2..Xn values.

        If setTableDimensions has not been called to properly set up the table
        prior to calling this function, the results are undefined.  Hopefully
        an exception will be thrown, but there is no guarantee.
    */
    template<typename T> void resetYData(const T& yData)
    {  try {
            // copy in the data
            for (int i = 0; i < d_subTableSizes[d_tableDimensions-1]; ++i)
                d_data[i] = yData[i];
        } catch (...)
        { throw std::runtime_error("Error copying in data. Probable array bounds error."); }
    }

    //! Method to give access to the raw Y data for use in Simulink, etc.
    const std::vector<double>& getAllYData() const { return d_data; }

    //! Writes out the current data and breakpoints to a stream in the "lota" format
    void write(std::ostream& os) const;

  private:

    //! Used recursively to calculate Y
    double calculateY(
        const std::vector<int>& leftHandIndices,
        const std::vector<double>& multipliers,
        const int dimensionIndex,
        const int offset) const;

    //! Parse method for "lota" format (see sample_lookup_table.lota)
    void read(std::istream& is);

    // header & breakpoint data
    std::string d_tableName;
    std::string d_variantTag;
    std::string d_tableComment;
    int         d_tableDimensions;
    std::vector<std::string> d_breakpointNames;
    std::vector<int> d_subTableSizes;
    std::vector<std::vector<double> > d_breakpoints;
    std::deque<bool> d_breakpointSymmFlags;
    std::deque<bool> d_breakpointNegOddSymmFlags;
    std::vector<double> d_breakpointSymmVals;

    // actual table data
    std::vector<double> d_data;

    // these commented out to allow copied - all members support that
    //TLookupTable(const TLookupTable& in); // not implemented
    //TLookupTable& operator=(const TLookupTable& in); // not implemented
};
//---------------------------------------------------------------------------

// non member operator
std::ostream& operator<<(std::ostream& os, const TLookupTable& table);
//---------------------------------------------------------------------------

//! Intermediate class to provide nD lookup cleanly for the user
class tool::TLookupTable::TLookupMeme
{
  public:
    //! Method for adding X values for further dimensions
    TLookupMeme& operator()(const double& Xn)
    {
        d_inputs.push_back(Xn);
        return *this;
    }

    /*! \brief Method for triggering the actual lookup.

        Default for values outside of bounds of table is zero-order
        extrapolation, however first order extrapolation is an option.

        Result value is returned through the Y parameter.

        \return  Boolean return value is true if requested Y value was within
                 the bounds of the table, false if it was calculated by
                 extrapolating outside the bounds of the table.
    */
    bool into(double& Y, const bool& extrapFirstOrder = false) const
    {
        return d_table.getY(Y, d_inputs, extrapFirstOrder);
    }

  private:
    /*  Constructor is private so that users don't construct TLookupMeme's
        themselves, and hence the relationship between the table and the meme
        can change in the future without affecting users.
    */
    //! X value for first dimension recorded in constructor
    TLookupMeme(const tool::TLookupTable& table, const double& X1)
      : d_table(table)
    {
        d_inputs.reserve(d_table.getTableDimensions());
        d_inputs.push_back(X1);
    }

    friend tool::TLookupTable;

    const TLookupTable& d_table;
    std::vector<double> d_inputs;

    TLookupMeme(); // not implemented (just to be sure)
    TLookupMeme(const TLookupMeme& in); // not implemented
    TLookupMeme& operator =(const TLookupMeme& in); // not implemented
};
//---------------------------------------------------------------------------

// Inlined member function
inline TLookupTable::TLookupMeme TLookupTable::getY(const double& X1) const
{
    return TLookupMeme(*this, X1);
}
//---------------------------------------------------------------------------

// Lookup table handle
typedef tool::SmartPtr<TLookupTable> TLookupTableHandle;

} // namespace tool

//---------------------------------------------------------------------------
#endif
