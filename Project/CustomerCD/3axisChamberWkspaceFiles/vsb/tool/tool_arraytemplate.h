//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_arraytemplate.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_arraytemplate.h/.cpp
      Implements a template dimensioned array.                        */
//---------------------------------------------------------------------------
#ifndef tool_arraytemplateH
#define tool_arraytemplateH

#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif
#ifndef valarraySTL
    #include <valarray>
    #define valarraySTL
#endif
#ifndef vectorSTL
    #include <vector>
    #define vectorSTL
#endif
#ifndef stdexceptSTL
    #include <stdexcept>
    #define stdexceptSTL
#endif
//---------------------------------------------------------------------------

namespace tool {

template<class T, int n> struct TArrayTemplate
{
	T data[n];

    // default constructor
    TArrayTemplate()
    {
        for (int i = 0; i < n; ++i)
            data[i] = T();
    }

    TArrayTemplate(const T& in)
    {
        for (int i = 0; i < n; ++i)
            data[i] = in;
    }

    // copy constructor
    TArrayTemplate(const TArrayTemplate& in)
    {
        for (int i = 0; i < n; ++i)
            data[i] = in.data[i];
    }

    // assignment operators
    TArrayTemplate& operator=(const TArrayTemplate& rhs)
    {
        if (this == &rhs)
            return *this;

        for (int i = 0; i < n; ++i)
            data[i] = rhs.data[i];

        return *this;
    }

    TArrayTemplate& operator=(const std::valarray<T>& rhs)
    {
        if (n != rhs.size())
            throw std::range_error("Bad valarray size in conversion to TArrayTemplate.");

        for (int i = 0; i < n; ++i)
            data[i] = rhs[i];

        return *this;
    }

    TArrayTemplate& operator=(const std::vector<T>& rhs)
    {
        if (n != rhs.size())
            throw std::range_error("Bad vector size in conversion to TArrayTemplate.");

        for (int i = 0; i < n; ++i)
            data[i] = rhs[i];

        return *this;
    }

    // destructor
    ~TArrayTemplate() {}

    T& operator[](const int i)
    {
        return data[i];
    }

    const T& operator[](const int i) const
    {
        return data[i];
    }

    T& at(const int i)
    {
        if (i >= 0 && i < n)
            return data[i];
        else
            throw std::out_of_range("TArrayTemplate access out of range");
    }

    const T& at(const int i) const
    {
        if (i >= 0 && i < n)
            return data[i];
        else
            throw std::out_of_range("TArrayTemplate access out of range");
    }

    // Member operators
    TArrayTemplate<T, n> operator -() const
    {
        TArrayTemplate<T, n> result;
        for (int i = 0; i < n; ++i)
            result[i] = -data[i];
        return result;
    }

    TArrayTemplate<T, n> limited(
            const TArrayTemplate<T, n>& lowerLimit,
            const TArrayTemplate<T, n>& upperLimit) const
    {
        TArrayTemplate<T, n> result;
        for (int i = 0; i < n; ++i)
            result[i] =
                tool::limit(data[i], lowerLimit[i], upperLimit[i]);
        return result;
    }

};
//---------------------------------------------------------------------------

// template specialisations...
template <typename T, int n>
inline tool::TArrayTemplate<T, n> limit(
            const tool::TArrayTemplate<T, n>& arg,
            const tool::TArrayTemplate<T, n>& min,
            const tool::TArrayTemplate<T, n>& max)
{ return arg.limited(min, max); }

template <typename T, int n>
inline tool::TArrayTemplate<T, n> limit(
            const tool::TArrayTemplate<T, n>& arg,
            const tool::TArrayTemplate<T, n>& lim)
{ return arg.limited(lim); }
//---------------------------------------------------------------------------

// Non-member operators

#if defined(_MSC_VER) && (_MSC_VER <= 1200)
	#pragma warning ( disable : 4800 )  // forcing int to bool
        // only necessary to disable this because MSVC can't handle
        // the specialisation we've put in place to fix it.
    #define WARNING_4800_DISABLED_HEREIN
#endif

// add each of the elements of the arrays, returning a new array
template<typename T, int n>
TArrayTemplate<T, n> operator+(const TArrayTemplate<T, n> &lhs, const TArrayTemplate<T, n> &rhs)
{
	TArrayTemplate<T, n> temp;
	int i;
	for (i=0; i<n; i++)
		temp[i] = lhs[i]+rhs[i];
	return temp;
}

// subtract each of the elements of the arrays, returning a new array
template<typename T, int n>
TArrayTemplate<T, n> operator-(const TArrayTemplate<T, n> &lhs, const TArrayTemplate<T, n> &rhs)
{
	TArrayTemplate<T, n> temp;
	int i;
	for (i=0; i<n; i++)
		temp[i]=lhs[i]-rhs[i];
	return temp;
}

// multiply each of the elements of the array by a scalar, returning a new array
template<typename T, int n>
TArrayTemplate<T, n> operator*(const double scalar, const TArrayTemplate<T, n> &rhs)
{
	TArrayTemplate<T, n> temp;
	int i;
	for (i=0; i<n; i++)
		temp[i]=scalar*rhs[i];
	return temp;
}

#ifdef WARNING_4800_DISABLED_HEREIN
	#pragma warning ( default : 4800 )  // forcing int to bool
    #undef WARNING_4800_DISABLED_HEREIN
#endif
//---------------------------------------------------------------------------

// Specialisation...
// In case the TArrayTemplate class is ever used with booleans, we need a
// modicum of protection...

#if !defined(_MSC_VER) || (_MSC_VER > 1200)
    // MSVC 6.0 can't handle this kind of specialisation, dammit.

template<int n>
TArrayTemplate<bool, n>
operator+(const TArrayTemplate<bool, n> &lhs, const TArrayTemplate<bool, n> &rhs)
{
    throw std::logic_error("Nonsense Operation: Attempt to add two booleans in tool::TArrayTemplate");
}

template<int n>
TArrayTemplate<bool, n>
operator-(const TArrayTemplate<bool, n> &lhs, const TArrayTemplate<bool, n> &rhs)
{
    throw std::logic_error("Nonsense Operation: Attempt to subtract two booleans in tool::TArrayTemplate");
}

template<int n>
TArrayTemplate<bool, n>
operator*(const double scalar, const TArrayTemplate<bool, n> &rhs)
{
    throw std::logic_error("Nonsense Operation: Attempt to scale a boolean in tool::TArrayTemplate");
}
//---------------------------------------------------------------------------

#endif

}; // namespace tool

#endif
