//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.5 $
//  $RCSfile: tool_atmos.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_atmos.h/.cpp
      Standard Earth Atmosphere properties.

    Use:
        To use an atmosphere model, immediately after including tool_atmos.h,
        use a typedef to select the atmosphere model you wish to use, eg:

            #include "tool_atmos.h"
            typedef tool::TAtmosphereUS76 TAtmosphere; // use US76 atmosphere

        Then call the methods directly. For example, to get temperature, use
            TAtmosphere::Temperature(altitude);
        where altitude is the number of positive metres above sea level

        Since all methods of these classes are static, and their constructors
        are not implemented, you can not instantiate the class.
*/
//---------------------------------------------------------------------------
#ifndef tool_atmosH
#define tool_atmosH

namespace tool {

////////////////////////////////////////////////////////////////////////////////
// ESDU77 Atmosphere model
////////////////////////////////////////////////////////////////////////////////

//  This model is taken from the ESDU Item Number 77021
//  "Properties of a Standard Atmosphere"
//  valid for altitudes between 0.0km (sea-level) and 80.0km

class TAtmosphereESDU77 {
    public:

        // <height> metres, -ve & +ve numbers treated as the same height
        // Values greater than 80000m are limited to 80000m
        static double Temperature(const double &height); // in Kelvin
        static double Pressure(const double &height);
        static double SpeedOfSound(const double &height);
        static double Density(const double &height);

    private:
        TAtmosphereESDU77();        // Constructor NOT IMPLEMENTED

        static const double Po;     // Pressure at sea level (N/m^2)
        static const double Ro;	    // Density at sea level (kg/m^3)

        static const double Rgas;   // universal gas constant N m/kmol K
        static const double M0;     // mean molecular mass  kg/kmol

        // Atmosphere is divided into 7 layers, results valid up to 80km
        // H = base altitude
        // T = base temperature
        // L = temperature gradient
        // P = base pressure
        // J & K = constants for pressure computation

        static const double H[7];
        static const double T[7];
        static const double bL[7];
        static const double bP[7];
        static const double J[7];
        static const double K[7];

        static int  LookUpH(const double &h);
};

////////////////////////////////////////////////////////////////////////////////
// US76 Atmosphere model
////////////////////////////////////////////////////////////////////////////////

//  Based on US Standard Atmosphere 1976
//  Valid from sea-level (0km) to 100km.

class TAtmosphereUS76 {
    public:

        // <height> is metres above sea level.
        static double Temperature(const double &height); // in Kelvin
        static double Pressure(const double &height);
        static double SpeedOfSound(const double &height);
        static double Density(const double &height);
        static double Gravity(const double &height);

    private:
        TAtmosphereUS76();          // Constructor NOT IMPLEMENTED

		// constants used in calculations
		static const double TAtmosphereUS76::ae;
		static const double TAtmosphereUS76::GE;
		static const double TAtmosphereUS76::gmr;
		static const double TAtmosphereUS76::T0;

		// tables used to perform calculations
		static const double TAtmosphereUS76::Height_Table[9];
		static const double TAtmosphereUS76::Temperature_Table[9];
		static const double TAtmosphereUS76::Pressure_Table[9];
		static const double TAtmosphereUS76::LapseRate_Table[9];

        static int LookupHeight(const double &height, double &dh);

};

}; // namespace tool

#endif
