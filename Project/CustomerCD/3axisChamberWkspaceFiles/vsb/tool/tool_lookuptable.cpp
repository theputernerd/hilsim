//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.11 $
//  $RCSfile: tool_lookuptable.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_lookuptable.h/.cpp

      nD Data Tables based on the "lota" file format - see sample lota file
      in tool directory.

      Supports repeating intervals of symmetry in the table on any axis,
      including the optional negation of the lookup result if the number
      of reflections about the value of symmetry was odd.
      Symmetry typically used for aero tables symmetric in roll angle.

          Original author - Duncan Fletcher
*/
//---------------------------------------------------------------------------
#if defined(_MSC_VER)
	#pragma warning ( disable : 4786 )  // identifier truncated in the debug information
#endif

#include "tool_lookuptable.h"

#ifndef tool_tokeniserH
    #include "tool_tokeniser.h"
#endif
#ifndef tool_mathtoolsH
    #include "tool_mathtools.h"
#endif
#ifndef iostreamSTL
    #include <iostream>
    #define iostreamSTL
#endif
#ifndef iomanipSTL
    #include <iomanip>
    #define iomanipSTL
#endif
#ifndef stdexceptSTL
    #include <stdexcept>
    #define stdexceptSTL
#endif
#ifndef algorithmSTL
	#include <algorithm>
	#define algorithmSTL
#endif
#ifndef cmathSTL
	#include <cmath>
	#define cmathSTL
#endif
#ifndef localeSTL
    #include <locale>
    #define localeSTL
#endif

#pragma hdrstop
//---------------------------------------------------------------------------

//#define PARANOID_CHECK

// forward declaration
namespace internal { // MSVC Compat - unnamed namespace doesn't work!!!
//! Returns true if string is a well-formed identifier, otherwise false.
bool isWellFormed(const std::string& ident);
int binarySearch(const std::vector<double> &xData, const double &x);
};
//---------------------------------------------------------------------------


namespace tool {
//---------------------------------------------------------------------------

TLookupTable::TLookupTable(std::istream& is)
{
    read(is);
}
//---------------------------------------------------------------------------


TLookupTable::TLookupTable(std::istream& is, const int& expectedDimensions)
{
    read(is);

    if (this->getTableDimensions() != expectedDimensions)
        throw std::runtime_error("Table loaded of unexpected dimensions");
}
//---------------------------------------------------------------------------


TLookupTable::TLookupTable(
        const std::vector<const double*>& breakpoints,
        const std::vector<int>& breakpointVectorLengths,
        const double* yData)
  : d_tableName(), d_variantTag(), d_tableComment(),  // empty defaults
    d_tableDimensions(breakpoints.size()),
    d_breakpointNames(breakpoints.size()), // empty defaults
    d_subTableSizes(breakpoints.size()),
    d_breakpoints(breakpoints.size()),
    d_breakpointSymmFlags(breakpoints.size()),
    d_breakpointSymmVals(breakpoints.size())
{
    if (breakpoints.size() != breakpointVectorLengths.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    for (int i = 0; i < d_tableDimensions; ++i)
    {
        // check we have at least 2 Xn entries - TLookupTable is designed to
        // predicate this
        if (breakpointVectorLengths[i] < 2)
            throw std::runtime_error("Error while reading from memory: less than 2 entries in a breakpoint vector");

        // Check that the data in Xn is monotonically increasing, ie
        // so that we can search through it.  Copy the data in as we
        // traverse
        d_breakpoints[i].resize(breakpointVectorLengths[i]);
        for (int j = 0; j < breakpointVectorLengths[i] - 1; ++j)
        {
            if (breakpoints[i][j] >= breakpoints[i][j+1])
                throw std::runtime_error("Error while reading from memory: breakpoint data not monotonically increasing");

            // copy in the breakpoints - would be CPU time consuming to wrap
            // this in an exception handler, but its likely that the breakpoints
            // vector is too small to trigger an access violation anyway
            d_breakpoints[i][j] = breakpoints[i][j];
        }
        // copy over the last one that the above loop didn't get
        d_breakpoints[i][breakpointVectorLengths[i]-1] =
                breakpoints[i][breakpointVectorLengths[i]-1];

        // fill out d_subTableSizes
        if (i == 0)
            d_subTableSizes[i] = breakpointVectorLengths[i];
        else
            d_subTableSizes[i] = d_subTableSizes[i - 1] * breakpointVectorLengths[i];
    }

	// resize member yData to its new size
	d_data.resize(d_subTableSizes[d_tableDimensions-1]);
	// copy in the data
    this->resetYData(yData);
}
//---------------------------------------------------------------------------


TLookupTable::~TLookupTable()
{

}
//---------------------------------------------------------------------------


void TLookupTable::setTableName(const std::string& name)
{
    // check that name is well formed.
    if (!internal::isWellFormed(name))
        throw std::runtime_error("Table name must be a well-formed identifer");

    d_tableName = name;
}
//---------------------------------------------------------------------------


void TLookupTable::setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints,
        const std::vector<std::string>& breakpointNames)
{
    if (breakpointNames.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    if (d_breakpointSymmFlags.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    if (int(breakpointNames.size()) !=
        std::count_if(breakpointNames.begin(), breakpointNames.end(), internal::isWellFormed))
    {
        throw std::runtime_error("Breakpoint names must well-formed identifiers");
    }

    d_breakpointNames = breakpointNames;

    setTableDimensions(breakpoints);
}
//---------------------------------------------------------------------------


void TLookupTable::setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints)
{
    if (d_breakpointNames.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    if (d_breakpointSymmFlags.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    d_tableDimensions = breakpoints.size();

    // do some checks on the breakpoint numbers
    for (int i = 0; i < d_tableDimensions; ++i)
    {
        // check we have at least 2 Xn entries - TLookupTable is designed to
        // predicate this
        if (breakpoints[i].size() < 2)
            throw std::runtime_error("Error while reading from memory: less than 2 entries in a breakpoint vector");

        // Check that the data in Xn is monotonically increasing, ie
        // so that we can search through it.  Copy the data in as we
        // traverse
        for (size_t j = 0; j < breakpoints[i].size() - 1; ++j)
        {
            if (breakpoints[i][j] >= breakpoints[i][j+1])
                throw std::runtime_error("Error while reading from memory: breakpoint data not monotonically increasing");

        }

        // fill out d_subTableSizes
        if (i == 0)
            d_subTableSizes[i] = breakpoints[i].size();
        else
            d_subTableSizes[i] = d_subTableSizes[i - 1] * breakpoints[i].size();
    }

    // copy over the breakpoint values
    d_breakpoints = breakpoints;

	// resize member yData to its new size
	d_data.resize(d_subTableSizes[d_tableDimensions-1]);
}
//---------------------------------------------------------------------------


void TLookupTable::setTableDimensions(
        const std::vector<std::vector<double> >& breakpoints,
        const std::vector<std::string>& breakpointNames,
        const std::deque<bool>& breakpointSymmetryFlags,
        const std::deque<bool>& breakpointNegOddSymmetryFlags,
        const std::vector<double>& breakpointSymmetryValues)
{
    if (breakpointSymmetryFlags.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");
    if (breakpointNegOddSymmetryFlags.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");
    if (breakpointSymmetryValues.size() != breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    // check that breakpoints are correct for symmetric dimensions
    for (size_t i = 0; i < breakpoints.size(); ++i)
    {
        if (breakpointSymmetryFlags[i])
        {
            if (breakpoints[i][0] != 0.0)
                throw std::runtime_error("Smallest breakpoint value must be zero for \"Symmetry\" breakpoints");

            if (breakpointSymmetryValues[i] <
                    breakpoints[i][breakpoints[i].size() - 1])
                throw std::runtime_error("Largest breakpoint value greater than value of symmetry for \"Symmetry\" breakpoints");
        }
        else
        {
            if (breakpointNegOddSymmetryFlags[i])
                throw std::runtime_error("Breakpoint vector cannot be negative odd-symmetric if it is not symmetric at all");
        }
    }

    d_breakpointSymmFlags = breakpointSymmetryFlags;
    d_breakpointNegOddSymmFlags = breakpointNegOddSymmetryFlags;
    d_breakpointSymmVals = breakpointSymmetryValues;

    setTableDimensions(breakpoints, breakpointNames);
}
//---------------------------------------------------------------------------


void TLookupTable::setBreakpointNames(
    const std::vector<std::string>& breakpointNames)
{
	if (d_breakpointNames.size() != breakpointNames.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    if (int(breakpointNames.size()) !=
        std::count_if(breakpointNames.begin(), breakpointNames.end(), internal::isWellFormed))
    {
        throw std::runtime_error("Breakpoint names must well-formed identifiers");
    }

	d_breakpointNames = breakpointNames;
}
//---------------------------------------------------------------------------


void TLookupTable::setBreakpointSymmetry(
    const std::deque<bool>& breakpointSymmetryFlags,
    const std::deque<bool>& breakpointNegOddSymmetryFlags,
    const std::vector<double>& breakpointSymmetryValues)
{
    if (breakpointSymmetryFlags.size() != d_breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");
    if (breakpointNegOddSymmetryFlags.size() != d_breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");
    if (breakpointSymmetryValues.size() != d_breakpoints.size())
        throw std::runtime_error("Mismatch in table dimensions in construction of TLookupTable");

    // check that breakpoints are correct for symmetric dimensions
    for (size_t i = 0; i < d_breakpoints.size(); ++i)
    {
        if (breakpointSymmetryFlags[i])
        {
            if (d_breakpoints[i][0] != 0.0)
                throw std::runtime_error("Smallest breakpoint value must be zero for \"Symmetry\" breakpoints");

            if (breakpointSymmetryValues[i] <
                    d_breakpoints[i][d_breakpoints[i].size() - 1])
                throw std::runtime_error("Largest breakpoint value greater than value of symmetry for \"Symmetry\" breakpoints");
        }
        else
        {
            if (breakpointNegOddSymmetryFlags[i])
                throw std::runtime_error("Breakpoint vector cannot be negative odd-symmetric if it is not symmetric at all");
        }
    }

    d_breakpointSymmFlags = breakpointSymmetryFlags;
    d_breakpointNegOddSymmFlags = breakpointNegOddSymmetryFlags;
    d_breakpointSymmVals = breakpointSymmetryValues;
}
//---------------------------------------------------------------------------

bool TLookupTable::getY(
        double& Y,
        const std::vector<double>& inputs,
        const bool& extrapFirstOrder) const
{
    bool extrapolated = false;

    // find the breakpoint to the left of the desired entry and the
    // multipliers for interpolation/extrapolation...
    std::vector<int> indices(d_tableDimensions);
    std::vector<double> multipliers(d_tableDimensions);
    double signModifier = 1.0;
    for (int i = 0; i < d_tableDimensions; ++i)
    {
        using namespace std; // MSVC Compat

        double input = inputs[i];

        // check to see if symmetry compensation is necessary
        if (d_breakpointSymmFlags[i])
        {
            double rem = fmod(input, d_breakpointSymmVals[i]);
            int times = (input - rem) / d_breakpointSymmVals[i];
            if (tool::abs(times % 2) == 1)
            {
                // odd
                input = d_breakpointSymmVals[i] - fabs(rem);
                // check to see if we should negate this lookup result
                // due to both being reflected an odd number of times
                // and that the value being looked up is out-of-plane
                // (typically) meaning that its sign changes when
                // reflected.
                if (d_breakpointNegOddSymmFlags[i])
                    signModifier = -signModifier;
            }
            else
                // even
                input = fabs(rem);
        }

        int index = internal::binarySearch(d_breakpoints[i], input);
        double multiplier =
                (input - d_breakpoints[i][index]) /
                (d_breakpoints[i][index+1] - d_breakpoints[i][index]);

        // assess for extrapolation...
        // depending on which direction the extrapolation is to take place,
        // the multiplier may be less than zero or greater than one.  So,
        // depending on whether or not first-order extrapolation is desired,
        // we may have to limit the multiplier
        if (multiplier < 0.0 || multiplier > 1.0)
        {
            extrapolated = true;
            if (!extrapFirstOrder)
                multiplier = tool::limit(multiplier, 0.0, 1.0);
        }

        indices[i] = index;
        multipliers[i] = multiplier;
    }

    // calculate the Y value, including modifying its sign if required.
    Y = signModifier * calculateY(indices, multipliers, d_tableDimensions-1, 0);

    return extrapolated;
}
//---------------------------------------------------------------------------


inline double TLookupTable::calculateY(
        const std::vector<int>& leftHandIndices,
        const std::vector<double>& multipliers,
        const int dimensionIndex,
        const int offset) const
{
    if (dimensionIndex == 0)
    {
        #ifdef PARANOID_CHECK
        double leftPoint = d_data.at(offset+leftHandIndices[0]);
        double rightPoint = d_data.at(offset+leftHandIndices[0]+1);
        double result = leftPoint + (rightPoint - leftPoint) * multipliers[0];
        return result;
        #else
        return  d_data[offset+leftHandIndices[0]] +
                    (d_data[offset+leftHandIndices[0]+1] -
                    d_data[offset+leftHandIndices[0]]) * multipliers[0];
        #endif
    }
    else
    {
        int newoffset = offset +
                leftHandIndices[dimensionIndex]*
                d_subTableSizes[dimensionIndex-1];
        double leftPoint =
            calculateY(
                leftHandIndices,
                multipliers,
                dimensionIndex-1,
                newoffset);
        double rightPoint =
            calculateY(
                leftHandIndices,
                multipliers,
                dimensionIndex-1,
                newoffset + d_subTableSizes[dimensionIndex-1]
                );
        return leftPoint +
               (rightPoint - leftPoint) * multipliers[dimensionIndex];
    }
}
//---------------------------------------------------------------------------


void TLookupTable::read(std::istream& is)
{
    tool::TTokeniser tokeniser(false); // comments not allowed

    // read header...
    // read name
    tokeniser.nextToken(is, "NAME");
    tokeniser.nextToken(is, ":");
    this->d_tableName = tokeniser.nextToken(is, eTokenIdentifier);
    if (!internal::isWellFormed(d_tableName))
        throw tool::tokeniser_error(tokeniser.getLineNumber(), "Table name must be a well-formed identifier");
    tokeniser.nextToken(is, ";");

    // read version - optional
    std::string temptoken = tokeniser.nextToken(is, eTokenIdentifier);
    if (temptoken == "VARIANT")
    {
        tokeniser.nextToken(is, ":");
        tokeniser.eatWhitespace(is);
        char buffer[16384];
        is.get(buffer, 16383, ';');
        this->d_variantTag = buffer;
        if (d_variantTag.find('\n') != std::string::npos ||
            d_variantTag.find('/') != std::string::npos)
            throw tool::tokeniser_error(tokeniser.getLineNumber(), "Badly formed version string");
        tokeniser.nextToken(is, ";");
    }
    else
    {
        tokeniser.putBack();
        this->d_variantTag = "";
    }

    // read comment - optional
    temptoken = tokeniser.nextToken(is, eTokenIdentifier);
    if (temptoken == "COMMENT")
    {
        tokeniser.nextToken(is, ":");
        tokeniser.eatWhitespace(is);
        char buffer[16384];
        is.get(buffer, 16383, ';');
        this->d_tableComment = buffer;
        tokeniser.nextToken(is, ";");
        // Correct the line number in the tokeniser for the newlines in the comment
        int newlines = std::count(d_tableComment.begin(), d_tableComment.end(), '\n');
        tokeniser.setLineNumber(tokeniser.getLineNumber()+newlines);
    }
    else
    {
        this->d_tableComment = "";
        tokeniser.putBack();
    }

    // read dimensions
    tokeniser.nextToken(is, "DIMENSIONS");
    tokeniser.nextToken(is, ":");
    this->d_tableDimensions = tokeniser.readNumeric(is);

    // read breakpoints
    this->d_breakpointNames.clear();
    this->d_breakpointNames.reserve(d_tableDimensions);
    this->d_breakpoints.clear();
    this->d_breakpoints.reserve(d_tableDimensions);
    this->d_subTableSizes.clear();
    this->d_subTableSizes.reserve(d_tableDimensions);
    this->d_breakpointSymmFlags.clear();
    this->d_breakpointSymmVals.clear();
    for (int i = 0; i < d_tableDimensions; ++i)
    {
        // Get the "X1", "X2", etc
        tokeniser.eatWhitespace(is);
        char c;
        is.get(c);
        if (c != 'X')
            throw std::runtime_error("Invalid breakpoint vector label");

        // get the number of the breakpoint
        int breakpointVectorNumber = tokeniser.readNumeric(is);
        if (breakpointVectorNumber != i+1)
            throw std::runtime_error("Invalid breakpoint number");
        tokeniser.nextToken(is, ":");

        // Get the breakpoint name
        this->d_breakpointNames.push_back(tokeniser.nextToken(is, eTokenIdentifier));
        if (!internal::isWellFormed(d_breakpointNames[i]))
            throw tool::tokeniser_error(tokeniser.getLineNumber(), "Breakpoint names must be a well-formed identifiers");
        tokeniser.nextToken(is, ";");

        // Get the breakpoints vector size
        tokeniser.nextToken(is, "BREAKPOINTS");
        tokeniser.nextToken(is, "(");
        int breakpointVectorSize = tokeniser.readNumeric(is);

        // check for symmetry settings
        std::string tempToken = tokeniser.nextToken(is, eTokenSpecial);
        if (tempToken == ",")
        {
            // this breakpoint is for symmetric data
            tokeniser.nextToken(is, "Symmetry");
            tokeniser.nextToken(is, ":");
            this->d_breakpointSymmFlags.push_back(true);
            this->d_breakpointSymmVals.push_back(tokeniser.readNumeric(is));
            // check for negate-if-odd-symmetric flag...
            std::string tempToken = tokeniser.nextToken(is, eTokenSpecial);
            if (tempToken == ",")
            {
                // should have flag...
                tokeniser.nextToken(is, "NegateOdd");
                this->d_breakpointNegOddSymmFlags.push_back(true);
                tokeniser.nextToken(is, ")");
            }
            else if (tempToken == ")")
            {
                this->d_breakpointNegOddSymmFlags.push_back(false);
            }
            else
            {
                throw tool::tokeniser_error(tokeniser.getLineNumber(), "Expected \')\' or \',\'");
            }
        }
        else if (tempToken == ")")
        {
            // this is a normal breakpoint
            this->d_breakpointSymmFlags.push_back(false);
            this->d_breakpointNegOddSymmFlags.push_back(false);
            this->d_breakpointSymmVals.push_back(0.0);
        }
        else
        {
            throw tool::tokeniser_error(tokeniser.getLineNumber(), "Expected \')\' or \',\'");
        }
        tokeniser.nextToken(is, ":");

        // we count on there being at least two entries in each breakpoint
        // vector, so check for that...
        if (breakpointVectorSize < 2)
            throw std::runtime_error("For efficiency reasons, TLookupTable insists that there be at least 2 breakpoints per dimension");

        // update d_subTableSizes
        if (i == 0)
            this->d_subTableSizes.push_back(breakpointVectorSize);
        else
            this->d_subTableSizes.push_back(d_subTableSizes[i-1] * breakpointVectorSize);

        // Get the breakpoints
        this->d_breakpoints.resize(d_breakpoints.size() +1 );
        this->d_breakpoints[i].reserve(breakpointVectorSize);
        for (int j = 0; j < breakpointVectorSize; ++j)
        {
            // if too few breakpoint numbers appear, the error will be
            // thrown at this point
            this->d_breakpoints[i].push_back(tokeniser.readNumeric(is));
        }
        // if too many breakpoint numbers appear, the error will be thrown
        // the next time round the loop, or when the data starts to be read

        // if symmetry, check the smallest and largest breakpoints
        if (d_breakpointSymmFlags[i])
        {
            if (d_breakpoints[i][0] != 0.0)
                throw tool::tokeniser_error(tokeniser.getLineNumber(), "Smallest breakpoint value must be zero for \"Symmetry\" breakpoints");

            if (d_breakpointSymmVals[i] <
                    d_breakpoints[i][breakpointVectorSize - 1])
                throw tool::tokeniser_error(tokeniser.getLineNumber(), "Largest breakpoint value greater than value of symmetry");
        }
    }

    // allocate space for the lookup table
    this->d_data.resize(d_subTableSizes[d_tableDimensions-1]);

    // prepare a vector to help check the breakpoint values on each subtable
    std::vector<size_t> breakpointTracker(this->d_tableDimensions, 0);

    // Calculate some useful subtable dimensions
    int itemsPerSubTable =
            d_tableDimensions > 1 ? d_subTableSizes[1] : d_subTableSizes[0];
    int numSubTables = d_data.size() / itemsPerSubTable;

    #ifdef PARANOID_CHECK
    int lastEntry = -1;
    #endif
    // Get the table data
    for (int subtab = 0; subtab < numSubTables; ++subtab)
    {
        // only read sub-table headers if we have more than two dimensions,
        // ie there is more than one sub table
        if (d_tableDimensions > 2)
        {
			// Get the sub-table header
			for (int breakNum = 2; breakNum < d_tableDimensions; ++breakNum)
			{
				tokeniser.eatWhitespace(is);
				char c;
				is.get(c);
				if (c != 'X')
					// not strictly a tokeniser error, but it will do to get a line
					// number for the error.
					throw tool::tokeniser_error(
							tokeniser.getLineNumber(),
							"Invalid breakpoint label");

				// get the number of the breakpoint
				int breakpointNumber = tokeniser.readNumeric(is);
				if (breakpointNumber != breakNum+1)
					// not strictly a tokeniser error, but it will do to get a line
					// number for the error.
					throw tool::tokeniser_error(
							tokeniser.getLineNumber(),
							"Invalid breakpoint number");
				tokeniser.nextToken(is, "=");

				// get the value of the breakpoint for checking
				double breakpointVal = tokeniser.readNumeric(is);
				if (breakpointVal != d_breakpoints[breakNum][breakpointTracker[breakNum]])
					// not strictly a tokeniser error, but it will do to get a line
					// number for the error.
					throw tool::tokeniser_error(
							tokeniser.getLineNumber(),
							"Invalid breakpoint value on sub-table");

				if (breakNum < d_tableDimensions - 1)
					tokeniser.nextToken(is, ",");
			}

			// advance the breakpoint tracker.
			++(breakpointTracker[2]);

			// roll over the lower order breakpoint overflow (from X3 on)
			for (int i = 2; i < d_tableDimensions-1; ++i)
			{
				if (breakpointTracker[i] >= d_breakpoints[i].size())
				{
					++(breakpointTracker[i+1]);
					breakpointTracker[i] = 0;
				}
			}
		}

        // get the sub-table data
        int offset = subtab * itemsPerSubTable;
        for (int itemNum = 0; itemNum < itemsPerSubTable; ++ itemNum)
        {
            #ifdef PARANOID_CHECK
            this->d_data.at(offset + itemNum) = tokeniser.readNumeric(is);
            if (offset+itemNum != ++lastEntry)
                throw std::logic_error("read algorithm stuffed in TLookupTable");
            #else
            this->d_data[offset + itemNum] = tokeniser.readNumeric(is);
            #endif
        }
    }
}
//---------------------------------------------------------------------------


void TLookupTable::write(std::ostream& os) const
{
	// parsing a table with an empty name fails, so we shouldn't allow
	// writing of a table with an empty name either
	if (d_tableName.empty())
		throw std::runtime_error("Unable to write table with empty name");

    os << "NAME: " << d_tableName << ";\n";
    if (!d_variantTag.empty())
        os << "VARIANT: " << d_variantTag << ";\n";
    if (!d_tableComment.empty())
        os << "COMMENT: " << d_tableComment << ";\n";
    os << "DIMENSIONS: " << double(d_tableDimensions) << '\n';
        // for some reason STLPORT crashes in release mode when
        // outputting integers!

    for (int i = 0; i < d_tableDimensions; ++i)
    {
		// parsing a table with an empty breakpoint name fails,
		// so we shouldn't allow writing of a table with an empty
		// breakpoint name either
		if (d_breakpointNames[i].empty())
			throw std::runtime_error("Unable to write table with empty breakpoint names");

        os << 'X' << double(i+1) << ": " << d_breakpointNames[i] << ";\n";
        os << "    BREAKPOINTS(" << double(d_breakpoints[i].size());
        if (d_breakpointSymmFlags[i])
        {
            os << ",Symmetry:" << std::setw(4) << std::showpoint
               << d_breakpointSymmVals[i] << std::noshowpoint;

            if (d_breakpointNegOddSymmFlags[i])
            {
                os << ",NegateOdd";
            }
        }
        os << "):";
        for (size_t j = 0; j < d_breakpoints[i].size(); ++j)
            os << ' ' << std::setw(4) << std::showpoint
               << d_breakpoints[i][j] << std::noshowpoint;
        os << '\n';
    }

    os << '\n';

    // prepare a vector to help write out the breakpoint values on each subtable
    std::vector<size_t> breakpointTracker(this->d_tableDimensions, 0);

    // Calculate some useful subtable dimensions
    int itemsPerSubTable =
            d_tableDimensions > 1 ? d_subTableSizes[1] : d_subTableSizes[0];
    int numSubTables = d_data.size() / itemsPerSubTable;

    // write the table data
    for (int subtab = 0; subtab < numSubTables; ++subtab)
    {
        // only write sub-table headers if we have more than two dimensions,
        // ie there is more than one sub table
        if (d_tableDimensions > 2)
        {
            // write the sub-table header
            for (int breakNum = 2; breakNum < d_tableDimensions; ++breakNum)
            {
                os << 'X' << double(breakNum+1) << " = " << std::showpoint;
                os << d_breakpoints[breakNum][breakpointTracker[breakNum]];
                if (breakNum < d_tableDimensions - 1)
                    os << std::noshowpoint << ", ";
            }
            os << '\n';

            // advance the breakpoint tracker.
            ++(breakpointTracker[2]);

            // roll over the lower order breakpoint overflow (from X3 on)
            for (int i = 2; i < d_tableDimensions-1; ++i)
            {
                if (breakpointTracker[i] >= d_breakpoints[i].size())
                {
                    ++(breakpointTracker[i+1]);
                    breakpointTracker[i] = 0;
                }
            }
        }

        // write the sub-table data
        int offset = subtab * itemsPerSubTable;
        size_t colCount = 0;
        for (int itemNum = 0; itemNum < itemsPerSubTable; ++ itemNum)
        {
            os << ' ' << std::setw(8) << std::showpoint << std::showpos
               << d_data[offset + itemNum];
            if (++colCount == d_breakpoints[0].size())
            {
                os << '\n';
                colCount = 0;
            }
        }

        os << std::noshowpos << std::noshowpoint;
    }

}
//---------------------------------------------------------------------------


std::ostream& operator<<(std::ostream& os, const TLookupTable& table)
{
    table.write(os);
    return os;
}
//---------------------------------------------------------------------------


} // namespace tool
//---------------------------------------------------------------------------

namespace internal {

bool isWellFormed(const std::string& ident)
{
    using namespace std; // MSVC compat
    for (size_t i = 0; i < ident.size(); ++i)
    {
        if (!(isalnum(ident[i]) ||
              '_' == ident[i]))
        {
            return false;
        }

        if (i==0 && isdigit(ident[i]))
            return false;
    }

    return true;
}
//---------------------------------------------------------------------------


// binarySearch() - uses a binary search algorithm to find the
// index into the xData such that xData[i] <= x < xData[i+1].
// If x is less than xData[0],
//     then 0 is returned.
// If x is greater than xData[xData.size()-1],
//     then xData.size()-2 (second-last element) is returned.
// Otherwise the discovered index is returned.
// In any case, the returned value can be used for
// an interpolation calculation (interpolating between data
// points i and i+1). Further assessment would be required
// to determine if an extrapolation was necessary.  One way is
// to calculate (x - xData[i]) / (xData[i+1] - xData[i]) and
// assess the result.
// Search assumes xData is sorted ascending.
int binarySearch(const std::vector<double> &xData, const double &x)
{
	if (x<xData[0])
		// before start of array
		return 0;
	else if (x>=xData[xData.size()-1])
		// after end of array
		return xData.size()-2;
	else
	{
		int start=0;
		int end=xData.size()-2;
		for (;;)
		{
			int mid=(start+end)/2;
			if (x>=xData[mid] && x<xData[mid+1])
			{
				// mid is the index we want
				 return mid;
			}
			else if (x<xData[mid])
			{
				// the index we're after is before mid
				end=mid-1;
			}
			else
			{
				// the index we're after is after mid
				start=mid+1;
			}
		}
	}
}
//---------------------------------------------------------------------------


} // internal namespace
