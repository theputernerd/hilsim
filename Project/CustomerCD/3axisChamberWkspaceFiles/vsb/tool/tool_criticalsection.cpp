//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:49:54 $
//  $Revision: 1.2 $
//  $RCSfile: tool_criticalsection.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*! \file
    Empty counterpart to tool_criticalsection.h
*/
//---------------------------------------------------------------------------
#define SPECIALSUPPRESSDUETOCPP
#include "tool_criticalsection.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool

