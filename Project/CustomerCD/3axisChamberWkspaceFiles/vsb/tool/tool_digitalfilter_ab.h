//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Crown Copyright UK 2003
//    Dstl, Ministry of Defence
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:05:12 $
//  $Revision: 1.1 $
//  $RCSfile: tool_digitalfilter_ab.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_digitalfilter_ab.h/.cpp
      Template implementation of an Alpha-Beta Estimator Digital Filter.   */
//---------------------------------------------------------------------------
#ifndef tool_digitalfilter_abH
#define tool_digitalfilter_abH
//---------------------------------------------------------------------------

namespace tool {

template <typename T>
class DigitalFilterAlphaBeta
{
  public:
  
    DigitalFilterAlphaBeta() { reset(0.0, T(), T()); }
    ~DigitalFilterAlphaBeta() {}

    void reset(
            const double& time,
            const T& val,
            const T& valDot
            );

    void setBandwidth(const double& rBandwidth);

    void inputMeasValue(
            const double& time,
            const T& measuredValue
            );

    void calculateEstimates(const double& time);

    const T& outputEstValue() const { return d_estValue; }
    const T& outputEstValueDot() const { return d_estValueDot; }

  private:

    double d_bandwidth;
    double d_lastBandwidth;

    T d_measValue;
    double d_measTime;

    double d_lastTimeStep;

    T d_estValue;
    T d_estValueDot;
    double d_estTime;

    T d_lastEstValue;
    T d_lastEstValueDot;
    double d_lastEstTime;

    double d_alpha;
    double d_beta;
};
//---------------------------------------------------------------------------

}; // namespace tool

// "export" requires a template repository which is a new enough C++
// concept that it isn't implemented in the compilers yet.  BCB6 and
// VC++7 (.Net) not yet checked.
#if defined(__BORLANDC__) && !defined(_MSC_VER)
    #if ( __TURBOC__ >= 0x550 )
    // Borland C++ Builder 5.0 (BCB)
        // no template repository, so
        #include "tool_digitalfilter_ab.cpp"
    #endif
#endif
#if defined(_MSC_VER) && !defined(__BORLANDC__)
    #if (_MSC_VER >= 1100)
    // MSVC 6.0
        // no template repository, so
        #include "tool_digitalfilter_ab.cpp"
    #endif
#endif
//---------------------------------------------------------------------------

#endif
