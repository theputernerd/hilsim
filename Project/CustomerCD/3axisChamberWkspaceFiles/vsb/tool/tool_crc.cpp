//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.7 $
//  $RCSfile: tool_crc.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_crc.h/.cpp
        Calculate Cyclic Redundancy of a text string.
        Useful tool for determining if files have been modified.
*/
//---------------------------------------------------------------------------
#include "tool_crc.h"

#ifndef __STDIO_H
    #include <cstdio>
    #define __STDIO_H
#endif
#ifndef stringSTL
    #include <string>
    #define stringSTL
#endif

using namespace std;
namespace tool {

/* lookup table */
static unsigned int crc16_table[16]={ 0x0000, 0xCC01, 0xD801, 0x1400,
                               0xF001, 0x3C00, 0x2800, 0xE401,
                               0xA001, 0x6C00, 0x7800, 0xB401,
                               0x5000, 0x9C01, 0x8801, 0x4400 };

static char crcStr[5];

TCheckSum::TCheckSum():itsCRC(0)
{
}


void TCheckSum::Clear()
{
  itsCRC = 0;
}

unsigned int TCheckSum::Get()
{
  return itsCRC;
};

const char *TCheckSum::GetStr()
{
  sprintf(crcStr,"%04X",itsCRC);
  return crcStr;
};

unsigned int TCheckSum::Update(const char *buff)
{
  int buffLen;      /* length of input string buffer */
  int i;            /* general variable */
  unsigned int r1;  /* temp */
  if (!buff) return  itsCRC;
  if ((buffLen = strlen(buff)) == 0) return itsCRC;
  for(i=0; i<buffLen; i++, buff++)
  {
     /* lower 4 bits */
     r1=crc16_table[itsCRC & 0xF];
     itsCRC=(itsCRC >> 4) & 0x0FFF;
     itsCRC=itsCRC ^ r1 ^ crc16_table[*buff & 0xF];

     /* upper 4 bits */
     r1=crc16_table[itsCRC & 0xF];
     itsCRC=(itsCRC >> 4) & 0x0FFF;
     itsCRC=itsCRC ^ r1 ^ crc16_table[(*buff >> 4) & 0xF];
  }
  return itsCRC;
}


_PREFIXCLASS const char *NameOrganisationSerialKey(const char *name, const char *organisation, const char *privateKey)
{
	size_t i;
	TCheckSum crc;
	static string result;
	string src;
	string temp;
	src = name;
	src += organisation;
	src += privateKey;
	
	// Standardize case (alternating upper, lower)
	// makes input case insensitive, but injects more dynamic range 
	// of characters
	for(i=0; i<src.size();i++)
		src[i] = (i%2)?toupper(src[i]):tolower(src[i]);


	crc.Update(src.c_str());
	result = crc.GetStr();
	result += "-";

	temp = src;
	size_t size = src.size();
	for(i=0; i<size; i++)
	{
		temp[i] = src[size-1-i];
	}

	crc.Update(temp.c_str());
	result += crc.GetStr();
	result += "-";

	char ch;
    for ( i=0; i<size-2; i+=2)
    {  
      ch = temp[i];
      temp[i] = temp[i+1];
	  temp[i+1] = ch;
   }

   crc.Update(temp.c_str());
   result += crc.GetStr();

   return result.c_str();
}

_PREFIXCLASS const char *NameSerialKey(const char *name, const char *privateKey)
{
	return NameOrganisationSerialKey(name,"",privateKey);
}


}; // namespace tool
