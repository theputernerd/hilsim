//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:50:25 $
//  $Revision: 1.1 $
//  $RCSfile: tool_datetime.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_datatime.h/.cpp

      Of the four or five "standard" date/time implementations available,
      none suit our purpose of being easy to use, convertable to a nicely
      formatted string and being compatible with Window's data/time, etc.

      This date/time implementation is a C++ wrapper around the standard C
      'tm' structure.
*/
//---------------------------------------------------------------------------
#include "tool_datetime.h"

#include "tool_bincodec.h"
#include <iostream>

#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

TDateTime::TDateTime()
{
    d_tm.tm_sec = 0;   /* Seconds */
    d_tm.tm_min = 0;   /* Minutes */
    d_tm.tm_hour = 0;  /* Hour (0--23) */
    d_tm.tm_mday = 0;  /* Day of month (1--31) */
    d_tm.tm_mon = 0;   /* Month (0--11) */
    d_tm.tm_year = 0;  /* Year (calendar year minus 1900) */
    d_tm.tm_wday = 0;  /* Weekday (0--6 = 0; Sunday = 0) */
    d_tm.tm_yday = 0;  /* Day of year (0--365) */
    d_tm.tm_isdst = 0; /* 0 if daylight savings time is not in effect) */
    d_millisec = 0;
}
//---------------------------------------------------------------------------


TDateTime::~TDateTime()
{

}
//---------------------------------------------------------------------------


std::istream& TDateTime::readBin(std::istream& is)
{
    double version = TBinCodec<double>(is);

    if (version != 1.0)
        throw std::runtime_error("Unsupported version of TDateTime data");

    d_tm = TBinCodec<tm>(is);
    d_millisec = TBinCodec<int>(is);

    return is;
}
//---------------------------------------------------------------------------


std::ostream& TDateTime::writeBin(std::ostream& os) const
{
    os << TBinCodec<double>(1.0);

    os << TBinCodec<tm>(d_tm);
    os << TBinCodec<int>(d_millisec);

    return os;
}
//---------------------------------------------------------------------------


bool operator < (const TDateTime& lhs, const TDateTime& rhs)
{
    if (lhs.getYear() < rhs.getYear())
        return true;
    else if (rhs.getYear() < lhs.getYear())
        return false;
    else
    {
        // Year equal
        if (lhs.getMonth() < rhs.getMonth())
            return true;
        else if (rhs.getMonth() < lhs.getMonth())
            return false;
        else
        {
            // Month equal
            if (lhs.getDay() < rhs.getDay())
                return true;
            else if (rhs.getDay() < lhs.getDay())
                return false;
            else
            {
                // Day equal
                if (lhs.getHour() < rhs.getHour())
                    return true;
                else if (rhs.getHour() < lhs.getHour())
                    return false;
                else
                {
                    // Hour equal
                    if (lhs.getMinutes() < rhs.getMinutes())
                        return true;
                    else if (rhs.getMinutes() < lhs.getMinutes())
                        return false;
                    else
                    {
                        // Minutes equal
                        if (lhs.getSeconds() < rhs.getSeconds())
                            return true;
                        else if (rhs.getSeconds() < lhs.getSeconds())
                            return false;
                        else
                        {
                            // Seconds equal
                            if (lhs.getMilliseconds() < rhs.getMilliseconds())
                                return true;
                            else if (rhs.getMilliseconds() < lhs.getMilliseconds())
                                return false;
                            else
                            {
                                // Milliseconds equal
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }
}
//---------------------------------------------------------------------------


} // namespace tool
