//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:52:44 $
//  $Revision: 1.1 $
//  $RCSfile: tool_filedata.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_filedata.h/.cpp

      Class containing file information, abstracted away from operating
      system specifics.

      Constructors and assignment operators exist to take _WIN32_FIND_DATA
      but there will only be an implementation for these if the conditional
      define USE_TOOL_WINDOWS_IMPL has been defined in the project settings.

      Currently, there are no alternate implementations available, but this
      class has been designed to extend to other operating system
      implementations.
*/
//---------------------------------------------------------------------------
#ifndef tool_filedataH
#define tool_filedataH

#ifndef tool_datetimeH
    #include "tool_datetime.h"
#endif
#include <string>
#include <iosfwd>
//---------------------------------------------------------------------------

// forward declarations
struct _WIN32_FIND_DATAA;
struct _SYSTEMTIME;
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

typedef TDateTime FileTime;

// 64 bit type
struct LargeInteger
{
    unsigned long LowPart;
    long HighPart;
};

//---------------------------------------------------------------------------

class TFileData
{
  public:
    TFileData() { clear(); }
    TFileData(const _WIN32_FIND_DATAA& fileData);
    TFileData(std::istream& is) { read(is); }
    ~TFileData();

    TFileData(const TFileData& in);
    TFileData& operator=(const TFileData& in);
    TFileData& operator=(const _WIN32_FIND_DATAA& fileData);

    std::string getFileName() const { return d_FileName; }
    std::string getShortFileName() const { return d_AlternateFileName; }

    LargeInteger getFileSize() const { return d_FileSize; }

    FileTime getCreationTime() const { return d_CreationTime; }
    FileTime getLastAccessTime() const { return d_LastAccessTime; }
    FileTime getLastWriteTime() const { return d_LastWriteTime; }

    bool isFile() const { return !isDirectory(); }
    bool isDirectory() const { return !!(d_FileAttributes & eFileAttributeDirectory); }
    bool isReadOnly() const { return !!(d_FileAttributes & eFileAttributeReadOnly); }
    bool isHidden() const { return !!(d_FileAttributes & eFileAttributeHidden); }
    bool isSystem() const { return !!(d_FileAttributes & eFileAttributeSystem); }
    bool isArchiveFlag() const { return !!(d_FileAttributes & eFileAttributeArchive); }
    bool isCompressed() const { return !!(d_FileAttributes & eFileAttributeCompressed); }

    bool isFiltered() const { return d_isFiltered; }
    void setFiltered(bool in) { d_isFiltered = in; }

    std::istream& read(std::istream& is);
    std::ostream& write(std::ostream& os) const;

    void clear();

  private:
    unsigned long d_FileAttributes;
    FileTime d_CreationTime;
    FileTime d_LastAccessTime;
    FileTime d_LastWriteTime;
    LargeInteger d_FileSize;
    std::string d_FileName;
    std::string d_AlternateFileName;
    bool d_isFiltered;

    enum {
        eFileAttributeDirectory = 1,
        eFileAttributeReadOnly = 2,
        eFileAttributeHidden = 4,
        eFileAttributeSystem = 8,
        eFileAttributeArchive = 16,
        eFileAttributeCompressed = 32
        };

    // The following methods are only implemented by some OS-specific
    // implementations, but must appear here to give these access to the
    // private declarations.
    unsigned long convertWindowsFileAttributes(unsigned long fileAttrs);
    TDateTime convertWindowSystemDateTime(const _SYSTEMTIME& sysTime);

};
//---------------------------------------------------------------------------

// non-member functions
inline bool lessThanCreationTime(const TFileData& lhs, const TFileData& rhs)
{ return lhs.getCreationTime() < rhs.getCreationTime(); }

inline bool lessThanLastAccessTime(const TFileData& lhs, const TFileData& rhs)
{ return lhs.getLastAccessTime() < rhs.getLastAccessTime(); }

inline bool lessThanLastWriteTime(const TFileData& lhs, const TFileData& rhs)
{ return lhs.getLastWriteTime() < rhs.getLastWriteTime(); }

inline bool lessThanFileName(const TFileData& lhs, const TFileData& rhs)
{
    if (lhs.isDirectory())
    {
        if (rhs.isDirectory())
            return lhs.getFileName() < rhs.getFileName();
        else
            return true;
    }
    else
    {
        if (rhs.isDirectory())
            return false;
        else
            return lhs.getFileName() < rhs.getFileName();
    }
}

inline bool lessThanSize(const TFileData& lhs, const TFileData& rhs)
{
    if (lhs.getFileSize().HighPart < rhs.getFileSize().HighPart)
        return true;
    else if (lhs.getFileSize().HighPart == rhs.getFileSize().HighPart)
        return lhs.getFileSize().LowPart < rhs.getFileSize().LowPart;
    else
        return false;
}

//---------------------------------------------------------------------------

inline std::istream& operator>>(std::istream& is, TFileData& fd)
{ return fd.read(is); }
inline std::ostream& operator<<(std::ostream& os, const TFileData& fd)
{ return fd.write(os); }

//---------------------------------------------------------------------------
} // namespace tool

#endif
