//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 07:04:47 $
//  $Revision: 1.4 $
//  $RCSfile: tool_strtools.h,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_strtools.h/.cpp

        String functions
*/
//---------------------------------------------------------------------------
#ifndef tool_strtoolsH
#define tool_strtoolsH

#include <string>
#include <list>
//---------------------------------------------------------------------------

namespace tool {
//---------------------------------------------------------------------------

// Converts character c to upper case
int ToUpper(int c);

// Converts character c to lower case
int ToLower(int c);

// Converts string s to upper case
std::string ToUpper(std::string s);

// Converts string s to lower case
std::string ToLower(std::string s);

// String comparison, case sensitive
// Returns -1 if string1 is less than string2
// Returns 0 if string1 is equal to string2
// Returns +1 if string1 is greater than string2
int StrCmpNoCase(const char *string1, const char *string2);

// String comparison, case sensitive
// Returns -1 if string1 is less than string2
// Returns 0 if string1 is equal to string2
// Returns +1 if string1 is greater than string2
int StrCmpCase(const char *string1, const char *string2);

// Breaks the input string apart based on the given seperating
// string
std::list<std::string> splitString(
        const std::string& str,
        const std::string& seper);

// Splices all of the given string pieces into a single string seperated
// by the given seperating string
std::string spliceString(
        const std::list<std::string>& strList,
        const std::string& seper);
//---------------------------------------------------------------------------


}; // namespace tool

#endif
