//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/24 04:32:30 $
//  $Revision: 1.1 $
//  $RCSfile: tool_fpucontrol.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_fpucontrol.cpp

      Provides a utility class to manipulate the FPU control word on x86 CPUs.

          Original author - Saxon Druce, Block Software
*/
//---------------------------------------------------------------------------

#include "tool_fpucontrol.h"

#pragma hdrstop

// Get OS-dependent functions...
#if defined(USE_TOOL_WINDOWS_IMPL)
	// currently Windows is the only OS implemented for this class
	#include "tool_fpucontrol_windows.cpp"
#endif

/*************************************************************************/
