//---------------------------------------------------------------------------
//
//         @@                        @@
//        @@                        @@
//      @@@@@@                     @@
//      @@                        @@
//     @@       @@@@     @@@@   @@
//    @@      @@  @@   @@  @@  @@
//    @@  @@  @@  @@   @@  @@  @@
//     @@@     @@@      @@@     @@@@
//
//    tool - Utility Toolkit
//    Copyright Commonwealth of Australia, 1999-2003
//    Defence Science and Technology Organisation
//    Weapons Systems Division.
//
//---------------------------------------------------------------------------
//  $Author: robinsom $
//  $Date: 2003/03/21 06:48:54 $
//  $Revision: 1.1 $
//  $RCSfile: tool_mimecodec.cpp,v $
//  $Name:  $
//---------------------------------------------------------------------------
/*  tool_mimecodec.h/.cpp

  Template - TMimeCodec<>

  Coder-Decoder template for MIME-encoding binary data into an ascii stream.
  Allows complex types to be written and read without loss of accuracy.
  Note that this is not a fully compliant MIME Base64 implementation
  but a loose interpretation of section 5.2 of RFC 1521.  ie the output
  of this routine is probably not actually MIME compliant and is therefore
  only guarunteed to be recoverable by this same routine.
*/
//---------------------------------------------------------------------------
#include "tool_mimecodec.h"
#pragma hdrstop
//---------------------------------------------------------------------------

namespace tool {

} // namespace tool
