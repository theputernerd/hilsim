# Microsoft Developer Studio Project File - Name="VSB DLL Static" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=VSB DLL Static - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "VSB DLL Static.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "VSB DLL Static.mak" CFG="VSB DLL Static - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "VSB DLL Static - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "VSB DLL Static - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "VSB DLL Static - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../Bin/RELEASE STATIC"
# PROP Intermediate_Dir "RELEASE STATIC"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
MTL=midl.exe
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "PLIBDLL_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /I "../plib/src/fnt" /I "../plib/src/pui" /I "../plib/src/sg" /I "../plib/src/ssg" /I "../plib/src/util" /I "../plib/src/js" /I "../plib/src/ssgAux" /I "../plib/src/Win32" /I "../parser/src" /I "../parser/src/tools" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "DLL_PARSERTOOLS_IMPORTS" /D "DLL_PLIB_IMPORTS" /YX /FD /c
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "VSB DLL Static - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "VSB_Static___Win32_Debug"
# PROP BASE Intermediate_Dir "VSB_Static___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../Bin/DEBUG DLL"
# PROP Intermediate_Dir "DEBUG DLL"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
MTL=midl.exe
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "PLIBDLL_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../plib/src/fnt" /I "../plib/src/pui" /I "../plib/src/sg" /I "../plib/src/ssg" /I "../plib/src/util" /I "../plib/src/js" /I "../plib/src/ssgAux" /I "../plib/src/Win32" /I "../parser/src" /I "../parser/src/tools" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /D "DLL_PARSERTOOLS_IMPORTS" /D "DLL_PLIB_IMPORTS" /YX /FD /GZ /c
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "VSB DLL Static - Win32 Release"
# Name "VSB DLL Static - Win32 Debug"
# Begin Group "HUD"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbF18HUD.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbF18HUD.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDComponents.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDComponents.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDFramework.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDFramework.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDTools.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDTools.h
# End Source File
# End Group
# Begin Group "Entity"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbBaseViewEntity.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseViewEntity.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEntityManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEntityManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbFlightPath.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbFlightPath.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSkyDome.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajEntity.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajEntity.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajLeaf.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajLeaf.h
# End Source File
# End Group
# Begin Group "Camera"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbArcBall.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbArcBall.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseCamera.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbCameraManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCameraManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbFollowCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbFollowCamera.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlane.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSimpleCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSimpleCamera.h
# End Source File
# End Group
# Begin Group "Misc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbCloudLayer.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudLayer.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentOptions.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentOptions.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosion.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosion.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosionFXDirector.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosionFXDirector.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlane.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlaneParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlaneParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbLightingParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbLightingParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbLookAtTransform.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbLookAtTransform.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelRoot.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelRoot.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbPlaneTextureList.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbPlaneTextureList.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbRenderOptions.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbRenderOptions.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSkyDome.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbStateManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbStateManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbStrokedText.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbStrokedText.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSun.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSun.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSysPath.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSysPath.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSystemInit.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSystemInit.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbUtilities.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbUtilities.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbViewportContext.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbViewportContext.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbWin32DLL.h
# End Source File
# End Group
# End Target
# End Project
