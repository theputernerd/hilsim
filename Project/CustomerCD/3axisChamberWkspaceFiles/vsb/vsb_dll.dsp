# Microsoft Developer Studio Project File - Name="vsb_dll" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=vsb_dll - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "vsb_dll.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "vsb_dll.mak" CFG="vsb_dll - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "vsb_dll - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "vsb_dll - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "vsb_dll - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../Bin/RELEASE"
# PROP Intermediate_Dir "RELEASE_DLL"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "VSBDLL_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "../" /I "../plib/src/fnt" /I "../plib/src/pui" /I "../plib/src/sg" /I "../plib/src/ssg" /I "../plib/src/util" /I "../plib/src/js" /I "../plib/src/ssgAux" /I "../plib/src/Win32" /I "parser" /I "tool" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DLL_VSB_EXPORTS" /D "DLL_PLIB_IMPORTS" /D "DLL_PARSERTOOLS_IMPORTS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib glu32.lib glut32.lib OpenGl32.lib winmm.lib /nologo /dll /machine:I386 /out:"Release\vsb_dll.dll"

!ELSEIF  "$(CFG)" == "vsb_dll - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "VSB_DLL___Win32_Debug"
# PROP BASE Intermediate_Dir "VSB_DLL___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../Bin/DEBUG"
# PROP Intermediate_Dir "DEBUG_DLL"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "VSBDLL_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../" /I "../plib/src/fnt" /I "../plib/src/pui" /I "../plib/src/sg" /I "../plib/src/ssg" /I "../plib/src/util" /I "../plib/src/js" /I "../plib/src/ssgAux" /I "../plib/src/Win32" /I "parser" /I "tool" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DLL_VSB_EXPORTS" /D "DLL_PLIB_IMPORTS" /D "DLL_PARSERTOOLS_IMPORTS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib glu32.lib glut32.lib OpenGl32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"Debug\vsb_dll.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "vsb_dll - Win32 Release"
# Name "vsb_dll - Win32 Debug"
# Begin Group "HUD"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbF18HUD.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbF18HUD.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDComponents.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDComponents.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDFramework.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDFramework.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDTools.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbHUDTools.h
# End Source File
# End Group
# Begin Group "Camera"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbBaseCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseCamera.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbCameraManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCameraManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbFollowCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbFollowCamera.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSimpleCamera.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSimpleCamera.h
# End Source File
# End Group
# Begin Group "misc"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\vsbArcBall.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbArcBall.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseViewEntity.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbBaseViewEntity.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudLayer.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudLayer.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbCloudParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEntityManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEntityManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentOptions.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbEnvironmentOptions.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosion.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosion.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosionFXDirector.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbExplosionFXDirector.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbFlightPath.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbFlightPath.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlane.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlane.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlaneParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbGroundPlaneParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbLightingParams.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbLightingParams.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbLookAtTransform.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbLookAtTransform.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelRoot.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbModelRoot.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbPlaneTextureList.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbPlaneTextureList.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbRenderOptions.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbRenderOptions.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSkyDome.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSkyDome.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbStateManager.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbStateManager.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbStrokedText.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbStrokedText.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSun.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSun.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSysPath.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSysPath.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbSystemInit.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbSystemInit.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajEntity.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajEntity.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajLeaf.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbTrajLeaf.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbUtilities.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbUtilities.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbViewportContext.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbViewportContext.h
# End Source File
# Begin Source File

SOURCE=.\src\vsbWin32DLL.cpp
# End Source File
# Begin Source File

SOURCE=.\src\vsbWin32DLL.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
