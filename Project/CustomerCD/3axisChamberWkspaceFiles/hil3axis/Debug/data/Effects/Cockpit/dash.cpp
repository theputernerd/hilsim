/*
This file was produced by 3D Exploration Plugin: CPP Export filter.

3D Exploration

Copyright (c) 1999-2000 X Dimension Software

WWW         http://www.xdsoft.com/explorer/
eMail       info@xdsoft.com
*/
#include <windows.h>
#include <GL\gl.h>
#include <GL\glu.h>

// 4 Verticies
// 2 Triangles

static BYTE face_indicies[2][3] = {
// 1F06
	{0,1,2 }, {0,2,3 }
};
static GLfloat vertices [4][3] = {
{-0.31683f,0.475058f,0.259354f},{-0.316368f,0.304284f,-0.311039f},{0.33707f,0.316875f,-0.311533f},
{0.33707f,0.475059f,0.25829f}
};
GLint Gen3DObjectList()
{
 int i;
 int j;

 GLint lid=glGenLists(1);
   glNewList(lid, GL_COMPILE);

    glBegin (GL_TRIANGLES);
      for(i=0;i<sizeof(face_indicies)/sizeof(face_indicies[0]);i++)
       {
       for(j=0;j<3;j++)
        {
          int vi=face_indicies[i][j];
           glVertex3f (vertices[vi][0],vertices[vi][1],vertices[vi][2]);
        }
       }
    glEnd ();

   glEndList();
   return lid;
};
