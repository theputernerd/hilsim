/*
*\brief MotionTable.cpp 
*
*Written by J.West
*
*Based on code written by Themie Gouthas
*
*/
#include "MotionTable.h"
#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 


#include "ssgLocal.h"
#include <string>
#include "GL\gl.h"
#include "GL\glut.h"
#include <stdio.h>

const char *table::getTypeName(void) { return "3Axistable"; }

void table::copy_from ( table *src, int clone_flags )
{
	ssgBranch::copy_from ( src, clone_flags ) ;
}

ssgBase *table::clone ( int clone_flags )
{
	table *b = new table ;
	b -> copy_from ( this, clone_flags ) ;
	return b ;
}


table::table (void)
{	m_antennaOffset=0;
	m_Initialised = 0;
	type = ssgTypeBranch () ;
}

table::~table (void)
{
	delete(m_Xfm);
	delete(this);

}


int table::load ( FILE *fd )
{
	return ssgBranch::load(fd) ;
}

int table::save ( FILE *fd )
{
	return ssgBranch::save(fd) ;
}

void table::Initialise(const char *model_path)
{
  m_Initialised = true;
  ssgTransform *heightAdjust = new ssgTransform; 
  this->addKid(heightAdjust);
  m_Xfm[0] = heightAdjust;					
  m_Xfm[5] = new ssgTransform; m_Xfm[0]->addKid(m_Xfm[5]);		// chamber stuff
  m_Xfm[3] = new ssgTransform; m_Xfm[0]->addKid(m_Xfm[3]);		// outer axis
  m_Xfm[2] = new ssgTransform; m_Xfm[3]->addKid(m_Xfm[2]);		// centre axis
  m_Xfm[1] = new ssgTransform; m_Xfm[2]->addKid(m_Xfm[1]);		// inner axis
  m_Xfm[4] = new ssgTransform; m_Xfm[1]->addKid(m_Xfm[4]);		//seeker COO
 
  std::string qualified_model_name;
  
  for (int i=0; i<6; i++)
  {//The 3ds files are loaded in here
		qualified_model_name = model_path;
		qualified_model_name += '\\';
		qualified_model_name += "images";
		qualified_model_name += '\\';
		qualified_model_name += "motiontable";
		qualified_model_name += '0'+i;
		qualified_model_name += ".3ds";
		ssgEntity *entity = ssgLoad3ds(qualified_model_name.c_str());
		if (!entity) printf("failed to load \"%s\"\n",qualified_model_name.c_str());
		m_Xfm[i]->addKid(entity);
  }
       
    //down here will normalise the cone and then scale it to the appropriate size.
    //or dont use a .3ds cone and draw it with VSB
}


/*
*Sets all angles of the table at once.
*
*/
void table::setTableAngle(const double angle1, const double angle2,const double angle3)
{
	sgMat4 rot_mat1,rot_mat2,rot_mat3;
	sgVec3 axes[4] = {{0,0,0},{0,1,0},{0,0,1},{1,0,0}};
	
	
	sgMakeRotMat4( rot_mat1, (float) angle1, axes[1]) ; //takes a 3x4 matrix and makes it into a
	sgMakeRotMat4( rot_mat2, (float) angle2, axes[2]) ; 
	sgMakeRotMat4( rot_mat3, (float) angle3, axes[3]) ; 
	((ssgTransform*)m_Xfm[1])->setTransform(rot_mat1);
	((ssgTransform*)m_Xfm[2])->setTransform(rot_mat2);
	((ssgTransform*)m_Xfm[3])->setTransform(rot_mat3);

	glutPostRedisplay () ;
	
}
/*sets the antennaOffset
*/
void table::setAntennaOffset(const double antennaOffset)
{	m_antennaOffset=antennaOffset;
	
}
	
/*
*Sets the direction of the seeker's antenna
*
*If the antenna origin is offset 
*
*/
void table::setAntenna(const double psi,const double theta)
{	
	sgMat4 rot_mat,rot_mat1,rot,rot1,translate;
	sgVec3 axes[4] = {{0,0,0},{0,1,0},{0,0,1},{1,0,0}};
	
	
	sgMakeTransMat4(translate, 0, m_antennaOffset,0);

	sgMakeRotMat4( rot_mat, (float) psi, axes[2]) ; 
	
	sgMakeRotMat4( rot_mat1, (float) theta, axes[3]) ; 

	/*Attempting to scale the antenna.
	*/
	sgMultMat4(rot,rot_mat1,rot_mat);
	sgMultMat4(rot1,translate,rot);
	
	((ssgTransform*)m_Xfm[4])->setTransform(rot1);  
	glutPostRedisplay () ;
	

}