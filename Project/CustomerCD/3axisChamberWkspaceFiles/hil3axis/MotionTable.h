/*MotionTable.h<br>
*Written by J.West <br>
*11 Oct 03<br>
*Based on code written by Themie <br>
**/
#ifndef _MotionTable_H_
#define _MotionTable_H_

#include "sg.h"
#include "ssg.h"


/**
**\brief Class table builds a PLIB compatible scene graph of the 3 axis HIL table, and
* provides a mechanism to allow the axes to be animated. 
*
* This version includes manipulation of the seeker antenna and antenna offset. 
*This class also loads the background.<br>
*
*File names are	<br>
*motiontable0.3DS		-The base of Table and the floor<br>
*motiontable1.3DS		-The missile <br>
*motiontable2.3DS		-The inner ring of the table<br>
*motiontable3.3DS		-The outer ring of the table<br>
*motiontable4.3DS		-The seeker cone<br>
*motiontable5.3DS		-The walls and other fixed stuff.<br>
*
*If a different table needs to be implemented, such as the 5 axis table, this class
*should be replaced. The chamber class inherits from this class so all methods
*available in this class are available by creating an instance of the chamber.
*
*The antenna methods and member attributes have to be in any modification of this class.
* 
*
*Written by J.West <br>
*jwest@adfa.edu.au
*
*11 Oct 03<br>
*Based around code written by Themie Gouthas WSD
*<br>
**/
class table: public ssgBranch
{
	
public:

	//bool Initialised() const {return mInitialised;};

	//  Initialise
	/**
	**\brief Point to the directory where HIL component 3D meshes are found and load them
	*
	*Loads the .3ds data files<br><ul>
	*<li>motiontable0.3DS		-The base of Table and the floor<br>
	*<li>motiontable1.3DS		-The missile <br>
	*<li>motiontable2.3DS		-The inner ring of the table<br>
	*<li>motiontable3.3DS		-The outer ring of the table<br>
	*<li>motiontable4.3DS		-The seeker cone<br>
	*<li>motiontable5.3DS		-The walls and other fixed stuff.
	*</ul>
	*If these files are replaced notice should be taken of the orientation and scale
	*of the relacing part.<br>
	*
	*Here is where the antenna projection angle should also be changed if implemented.<br>
	*
	*/
	void  Initialise(const char *model_path);
	
	/**
	**\brief Required when extending ssgBranch<br>
	*/
	virtual ssgBase *clone ( int clone_flags = 0 );

	
	/**
	**\brief primary constructor.<br>
	*
	*Sets all moving parts to value of 0.<br>
	*The inital position of the moving parts are dependant on how they are drawn in the appropriate 
	*.3ds file.
	*/
	table (void) ;

	/**
	**\brief primary destructor.
	*/
	virtual ~table (void) ;

	/**
	**\brief Sets the angles of the table.
	*
	*Sets angle1, angle2 and angle3 of the table.<br>
	*angle1 is the inner ring.<br>
	*angle2 is the middle ring.<br>
	*and angle3 is the outer ring.<br>
	*<br>
	*The number corresponds to the position in the array ssgBranch *mXfm[6];<br>
	*Which in turn corresponds to the number in the file name, e.g. 
	*motiontable4.3DS		-The seeker cone<br>
	*The inital position of the moving parts are dependant on how they are drawn in the appropriate 
	*.3ds file.*
	*
	*/
	void setTableAngle(const double angle1, const double angle2,const double angle3);
	/**
	**\brief Sets the projection size of the seeker antenna.<br>
	*
	*Not implemented.<br>
	*Two methods were briefly tried to achieve this. One was creating a vsbCone and drawing the cone.<br>
	*This would be the preffered method as scale could be just a simple redraw.<br>
	*The other method is to load a know .3ds cone and rescale that cone.<br>
	*The initial position of the seeker antenna is determined by the file motiontable4.3DS<br>
	**/
	void setAntennaAngle(double angl);	

	/**
	**\brief Sets the offset of the antenna Cone.
	*
	*Initial position is 0.<br>
	*Negative is towards the wall, measured in metres.<br>
	**/
	void setAntennaOffset(double antennaOffset);
		

	
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual const char *getTypeName(void) ;
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual int load ( FILE *fd ) ;
	/**
	**\brief Override ssgBranch methods.
	*/
	virtual int save ( FILE *fd ) ;
	
	/**
	**\brief Sets the two axes of the antenna look movement.
	*/
	void table::setAntenna(const double psi,const double theta);
private:

	/**\brief true if the table has been initialised
	*/
	bool m_Initialised;
	/**
	**\brief Stores each element of the scene. The index is the same as the file number.
	*
	*These are used to store the original matrix so new transforms can be applied.
	*
	*mXfm[0] -Tables base and floor <br>
	*mXfm[1] -The missile<br>
	*mXfm[2] -The inner ring of the table<br>
	*mXfm[3] -The outer ring of the table<br>
	*mXfm[4] -The seeker cone<br>
	*mXfm[5] -The walls and other fixed stuff. This is the branch to be made 
	*transparent to turn the walls off.<br>
	*<br>
	*/
	ssgBranch *m_Xfm[6];
	
	/**
	**\brief the offset from the center of the table.
	*/
	double m_antennaOffset;
	/**
	**\brief Required when extending ssgBranch<br>
	*/
	virtual void copy_from ( table *src, int clone_flags ) ;

};


#endif