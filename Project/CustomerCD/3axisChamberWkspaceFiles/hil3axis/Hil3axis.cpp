
#include "3axischamber.h"
#include <stdio.h>
#include <string>
#include <iostream>

#include "GL\gl.h"
#include "GL\glut.h"

/**\brief The chamber objects. Currently only 1 instance is able to be drawn to he screen correctly.
*/
Chamber *c, *d;
/**\brief The values of the moving parts
*/
float inc1=0,inc2=0,inc3=0,incant1=0,incant2=0;
/**\brief The tables moving parts value
*/
double axis1_angle = 0,axis2_angle = 0 ,axis3_angle = 0; //outer ring back and forth (green one)
/**\brief The antenna movement values
*/
double m_antennaPsi=0, m_antennaTheta=0;
/**\brief The camera values. These can be retrieved by the i key.
*/
double camerax=-10,cameray=1,cameraz=2,pan=204, tilt=0;
/**\brief the phase of the RFHorns. 
*All horns are set the same, except some are given a negative value.
*/
double phase=0;			//Phase of the RF horns
/**\brief Whether all moving parts are animated
*/
bool an=false;			//whether to animate or not.
/**\brief Whether a particular moving part is animated
*/
bool anaxes1=false, anaxes2=false,anaxes3=false,anrf=false, ancamera=false, ancone=false;
/**\brief the offset of the antenna 
*/
double offset=0;

//int win_w = 640, win_h = 480;
/**\brief The context of the subwindow and mainWindow
*/
int subWinContext=0,mainWindow=0;
/**\brief Required to be supplied for the chamber class.
*Changine the scene occurs in here.
*/
void displayGUI () //this must be defined by the user of the chamber class
{	//std::cout<<"maindisplay";
	
	if(anaxes1)
	{	
		if(inc1==360)
			inc1=0;

		inc1 +=1;
		axis1_angle = inc1;
    
		
	
	}
	if(anaxes2)
	{	if(inc2==360)
		{inc2=0;
		}
		inc2 += .005;
		axis2_angle = sin(inc2)*45;
		
		
	}
	if(anaxes3)
	{	if(inc3==360)
			inc3=0;
		
		inc3 +=0.04;
		axis3_angle = sin(inc3)*45;
		
	}
	if(anrf)
	{
		if(phase==360)
			phase=0;
		
		++phase;
		
		
	}
	if(ancone)
	{
		if(incant1==360)
			incant1=0;
		
		if(incant2==360)
			incant2=0;
		incant1+= .05;
		incant2+= .06;
		m_antennaTheta=sin(incant1)*20;
		m_antennaPsi=sin(incant2)*20;

		
	}
	if(ancamera)
	{	if(pan==360)
			pan=0;
		
		pan+=.25;



		c->setCamera(camerax,cameray,cameraz,pan,tilt);
		
	}
	
	c->setTableAngle(axis1_angle,axis2_angle,axis3_angle);
	c->setAntenna(m_antennaTheta,m_antennaPsi);
	c->setRFHorn(0,phase,0);//if testing remove these.
	c->setRFHorn(1,-phase,0);
	c->setRFHorn(2,-phase,0);
	c->setRFHorn(3,phase,0);
	c->setRFHorn(4,-phase,0);
	c->setRFHorn(5,phase,0);
	c->setRFHorn(6,-phase,0);
	c->setRFHorn(7,phase,0);
	
	c->setAntennaOffset(offset);
	
	
}

/**\brief Called by GL on key event.
*/
void keyfn ( unsigned char key, int, int )
{
	switch (key)
	{	case '5':
			anaxes1=!anaxes1;
			tilt=-.25;
			pan=283.5;
			camerax=-10;
			cameray=4;
			cameraz=2;

			break;
		case '6':
			anaxes2=!anaxes2;
			tilt=-25.25;
			pan=441;
			camerax=-8;
			cameray=-1;
			cameraz=-2;
			break;
		case '7':
			anaxes3=!anaxes3;
			camerax=-10;
			cameray=1;
			cameraz=2;
			pan=204;
			tilt=0;
			break;
		case '8':
			anrf=!anrf;
			camerax=-10;
			cameray=1;
			cameraz=2;
			pan=204;
			tilt=0;
			break;
		case '9':
			ancone=!ancone;
			tilt=-16.5;
			pan=34.5;
			camerax=-10;
			cameray=1;
			cameraz=2;

			break;
		case '0':
			ancamera=!ancamera;
			break;
		
		case 'r':
			an=!an;
			if(an)
			{	ancamera=true;
				ancone=true;
				anrf=true;
				anaxes1=true;
				anaxes2=true;
				anaxes3=true;
						
	
			}
			else
			{
				ancamera=false;
				ancone=false;
				anrf=false;
				anaxes1=false;
				anaxes2=false;
				anaxes3=false;
			}

			break;
		case 'p':
			phase+=1;
			//std::cout<<phase;
			//std::cout<<"\n";
			break;
		case 'k':
			axis2_angle+=3;
			break;
		case ',': 
			axis2_angle-=3;
			break;
		
		case 'f':
		case 'F': m_antennaPsi+=2;
			break;
		case 'v':
		case 'V': m_antennaPsi-=2;
				break;
		case 'g':
		case 'G': m_antennaTheta+=2;
			break;
		case 'b':
		case 'B': m_antennaTheta-=2;
			break;
		case 'z':
			pan+=.5;
			break;
		case 'c':
			pan-=.5;
			break;
		case 'e':
			tilt+=.25;
			break;
		case 'q':
			tilt-=.25;
			break;
		case 'w':
			camerax+=.25;
			
			break;
		case 's':
		case '<':
			camerax-=.25;
			
			break;
		case 'd':
			cameray+=.25;
			
			break;
		case 'a':
			cameray-=.25;
			
			break;
		case 'i':
		case 'I':
			std::cout<<"pan:";
			std::cout<<pan;
			std::cout<<"\n";
			std::cout<<"tilt:";
			std::cout<<tilt;
			std::cout<<"\n";
			std::cout<<"camerax:";
			std::cout<<camerax;
			std::cout<<"\n";
			std::cout<<"camerazy:";
			std::cout<<cameray;
			std::cout<<"\n";
			std::cout<<"cameraz:";
			std::cout<<cameraz;
			std::cout<<"\n";
			break;
		
		
		case '1':
		case '!':
			camerax=-10;
			cameray=1;
			cameraz=2;
			pan=150;
			tilt=0;
			
			break;
		case '2':
		case '@':
			camerax=-10;
			cameray=1;
			cameraz=2;
			pan=204;
			tilt=0;
			
			break;

		case '=':
			offset-=.25;
			break;
		case '-':
			offset+=.25;
			break;

		
	}
	//Having this in diplayGUI overrides the mouse arcball movement
	//d->setCamera(camerax,cameray,cameraz,pan,tilt);
	c->setCamera(camerax,cameray,cameraz,pan,tilt);

	
}




/**
**\brief Demonstration of how to implement the Chamber class.
*
*This program creates one chamber object and "animates" all moving parts of the chamber.
*Multiple chamber objects can be created, however at this stage the two objects are placed in the 
*same part of the screen. 
*
*
*The following keys are used:
*<ul>
*<li>w -forward
*<li>s -back
*<li>e -tilt right
*<li>q -tilt left
*<li>z -pan left
*<li>c -pan right
*<li>i -get the camera information for that location
*<li>1 -camera view of table from left
*<li>2 -camera view of table from right
*<li>= - antenna offset toward the RF wall
*<li>- - antenna offset away from rf wall.
*<br>The following keys also modify the camera position.
*<li>5 - Toggle missile rotation
*<li>6 - Toggle inner ring
*<li>7 - Toggle outer ring
*<li>8 - Toggle RF horn phase
*<li>9 - Toggle seeker look movement
*<li>0 - Toggle rotate the camera
*
*</ul>
*
*
*
*Written by J.west<br>
*
*Creating instances doesn't work I believe it is due to the vsbviewport<br>
*The recalc Geometry places it at 0,0 <br>
void				ViewportShape(int w, int h)		<br>
{mView_W = w; <br>
 mView_H = h; <br>
 mView_Aspect = float(w)/float(h);<br>
 mArcBall.RecalcGeometry(0, 0, float(w), float(h));<br>
 };<br>
 
*/
int main(int argc, char* argv[])
{
	mainWindow=c->chamberInitialise(0,0,800,600);
	glutKeyboardFunc       ( keyfn     ) ;
	
	mainWindow=glutGetWindow();
	//this Chamber object creates a subwindow in mainWindow
	//d=new Chamber(20,0,mainWindow,400,0,800,600); //multiple instances doesn't work fully
	c=new Chamber(20,0,mainWindow,0,0,800,600);
	c->setCamera(camerax,cameray,cameraz,pan,tilt);
	subWinContext=c->getContext();

	//glutSetWindow(mainWindow);
	/*This many horns are referenced in displayGUI if you remove any make sure you remove the calls changing it*/
	c->addRFHorn	(2,		29,		10,		355,	42,		14);
	c->addRFHorn	(4,		59,		38,		0,		1,		34);
	c->addRFHorn	(9,		52,		42,		0,		1,		58);
	c->addRFHorn	(354,	59,		17,		0,		1,		55);
	c->addRFHorn	(357,	30,		2,		4,		22,		0);
	c->addRFHorn	(357,	29,		57,		355,	42,		14);
	c->addRFHorn	(2,		29,		17,		4,		22,		0);
	c->addRFHorn	(359,	59,		47,		0,		1,		55);
	
	//d=new Chamber(20,0,mainWindow,320,0,640,480);
	/*Walls for testing scale 
	c->addRFHorn(10,0,0,0,0,0);
	c->addRFHorn(-10,0,0,0,0,0);
	c->addRFHorn(0,0,0,10,0,0);
	c->addRFHorn(0,0,0,-10,0,0);
	*/
	
	c->startMainloop();//glutMainLoop();
	
	
	return 0;
}