// chunklod.h	-- tu@tulrich.com Copyright 2001 by Thatcher Ulrich

// Header declaring data structures for chunk LOD rendering.

#ifndef CHUNKLOD_H
#define CHUNKLOD_H


#include "chunklod_geometry.h"
#include "chunklod_viewstate.h"
#include "chunklod_container.h"


namespace chunklod {

class tqt;
struct lod_chunk;
class chunk_tree_loader;
struct lod_chunk_data;


struct render_options {
	bool	show_box;
	bool	show_geometry;
	bool	morph;

	render_options()
		:
		show_box(false),
		show_geometry(true),
		morph(true)
	{
	}
};


class lod_chunk_tree {
// Use this class as the UI to a chunked-LOD object.
// !!! turn this into an interface class and get the data into the .cpp file !!!

friend class tqt;
friend struct lod_chunk;
friend class chunk_tree_loader;
friend struct lod_chunk_data;

public:
	lod_chunk_tree(FILE* src, const tqt* texture_quadtree);
	~lod_chunk_tree();

	// External interface.
	void	set_parameters(float max_pixel_error, float max_texel_size, float screen_width, float horizontal_FOV_degrees);

	// update the reference point for view optimisation
	void	update(const vec3& viewpoint);

	// render the terrain
	int		render(const view_state& v);

	void	get_bounding_box(vec3* box_center, vec3* box_extent);

	// Internal interfaces, used by our contained chunks.
	unsigned short	compute_lod(const vec3& center, const vec3& extent, const vec3& viewpoint) const;

	// Internal interface,
	int	compute_texture_lod(const vec3& center, const vec3& extent, const vec3& viewpoint) const;

	// Call this to enable/disable loading in a background thread.
	void	set_use_loader_thread(bool use);

	// Set the underlying texture colour for time of day lighting
	void    set_color(float r, float g, float b);

	void    sync_loader_thread();

	// Chec if a point is above the terrain
	bool	is_above_terrain(vec3* point);

	// Get the terrain surface point corresponding to the provided point
	bool	get_terrain_surface(vec3* point);

	static  void	set_render_options(render_options opt){m_opt = opt;};

private:
//data:
	lod_chunk*	m_chunks;
	int	m_chunks_allocated;

	// data from chunk terrain file

	int						m_tree_depth;	// from chunk data.
	float					m_error_LODmax;	// from chunk data.
	float					m_vertical_scale;	// from chunk data; displayed_height = y_data * m_vertical_scale.


	float					m_distance_LODmax;	// computed from chunk data params and set_parameters() inputs --> controls displayed LOD level.
	float					m_texture_distance_LODmax;	// computed from texture quadtree params and set_parameters() --> controls when textures kick in.
	float					m_base_chunk_dimension;	// x/z size of highest LOD chunks.
	int						m_chunk_count;
	lod_chunk**				m_chunk_table;
	const tqt*				m_texture_quadtree;
	chunk_tree_loader*		m_loader;
	vec3					m_last_view_pt;
	static render_options	m_opt;
};

}; // namespace

#endif // CHUNKLOD_H


// Local Variables:
// mode: C++
// c-basic-offset: 8 
// tab-width: 8
// indent-tabs-mode: t
// End:
