// utility.cpp	-- by Thatcher Ulrich <tu@tulrich.com>

// This source code has been donated to the Public Domain.  Do
// whatever you want with it.

// Various little utility functions, macros & typedefs.


#include "chunklod_utility.h"

#ifdef _WIN32
#ifndef NDEBUG




int	tu_testbed_assert_break(const char* filename, int linenum, const char* expression)
{
	// @@ TODO output print error message
	__asm { int 3 }
	return 0;
}

#endif // not NDEBUG
namespace chunklod {
	
#define EPSILON 0.000001
#define CROSS(dest,v1,v2) \
	dest[0]=v1[1]*v2[2]-v1[2]*v2[1]; \
	dest[1]=v1[2]*v2[0]-v1[0]*v2[2]; \
	dest[2]=v1[0]*v2[1]-v1[1]*v2[0];
#define DOT(v1,v2) (v1[0]*v2[0]+v1[1]*v2[1]+v1[2]*v2[2])
#define SUB(dest,v1,v2) \
	dest[0]=v1[0]-v2[0]; \
	dest[1]=v1[1]-v2[1]; \
	dest[2]=v1[2]-v2[2]; 

bool
intersect_triangled(double orig[3], double dir[3],
					double vert0[3], double vert1[3], double vert2[3],
					double *t, double *u, double *v, bool cull)
{
	double edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
	double det,inv_det;
	
	/* find vectors for two edges sharing vert0 */
	SUB(edge1, vert1, vert0);
	SUB(edge2, vert2, vert0);
	
	/* begin calculating determinant - also used to calculate U parameter */
	CROSS(pvec, dir, edge2);
	
	/* if determinant is near zero, ray lies in plane of triangle */
	det = DOT(edge1, pvec);
	
	if (cull)           /* define TEST_CULL if culling is desired */
	{
		if (det < EPSILON)
			return 0;
		
		/* calculate distance from vert0 to ray origin */
		SUB(tvec, orig, vert0);
		
		/* calculate U parameter and test bounds */
		*u = DOT(tvec, pvec);
		if (*u < 0.0 || *u > det)
			return 0;
		
		/* prepare to test V parameter */
		CROSS(qvec, tvec, edge1);
		
		/* calculate V parameter and test bounds */
		*v = DOT(dir, qvec);
		if (*v < 0.0 || *u + *v > det)
			return 0;
		
		/* calculate t, scale parameters, ray intersects triangle */
		*t = DOT(edge2, qvec);
		inv_det = 1.0 / det;
		*t *= inv_det;
		*u *= inv_det;
		*v *= inv_det;
	} else
	{
		/* the non-culling branch */
		if (det > -EPSILON && det < EPSILON)
			return 0;
		inv_det = 1.0 / det;
		
		/* calculate distance from vert0 to ray origin */
		SUB(tvec, orig, vert0);
		
		/* calculate U parameter and test bounds */
		*u = DOT(tvec, pvec) * inv_det;
		if (*u < 0.0 || *u > 1.0)
			return 0;
		
		/* prepare to test V parameter */
		CROSS(qvec, tvec, edge1);
		
		/* calculate V parameter and test bounds */
		*v = DOT(dir, qvec) * inv_det;
		if (*v < 0.0 || *u + *v > 1.0)
			return 0;
		
		/* calculate t, ray intersects triangle */
		*t = DOT(edge2, qvec) * inv_det;
	}
	return 1;
}


bool
intersect_trianglef(float orig[3], float dir[3],
					float vert0[3], float vert1[3], float vert2[3],
					float *t, float *u, float *v, bool cull)
{
	float edge1[3], edge2[3], tvec[3], pvec[3], qvec[3];
	float det,inv_det;
	
	/* find vectors for two edges sharing vert0 */
	SUB(edge1, vert1, vert0);
	SUB(edge2, vert2, vert0);
	
	/* begin calculating determinant - also used to calculate U parameter */
	CROSS(pvec, dir, edge2);
	
	/* if determinant is near zero, ray lies in plane of triangle */
	det = DOT(edge1, pvec);
	
	if (cull)          /* define TEST_CULL if culling is desired */
	{
		if (det < EPSILON)
			return 0;
		
		/* calculate distance from vert0 to ray origin */
		SUB(tvec, orig, vert0);
		
		/* calculate U parameter and test bounds */
		*u = DOT(tvec, pvec);
		if (*u < 0.0 || *u > det)
			return 0;
		
		/* prepare to test V parameter */
		CROSS(qvec, tvec, edge1);
		
		/* calculate V parameter and test bounds */
		*v = DOT(dir, qvec);
		if (*v < 0.0 || *u + *v > det)
			return 0;
		
		/* calculate t, scale parameters, ray intersects triangle */
		*t = DOT(edge2, qvec);
		inv_det = (float) 1.0 / det;
		*t *= inv_det;
		*u *= inv_det;
		*v *= inv_det;
	} else {
		/* the non-culling branch */
		if (det > -EPSILON && det < EPSILON)
			return 0;
		inv_det = (float)1.0 / det;
		
		/* calculate distance from vert0 to ray origin */
		SUB(tvec, orig, vert0);
		
		/* calculate U parameter and test bounds */
		*u = DOT(tvec, pvec) * inv_det;
		if (*u < 0.0 || *u > 1.0)
			return 0;
		
		/* prepare to test V parameter */
		CROSS(qvec, tvec, edge1);
		
		/* calculate V parameter and test bounds */
		*v = DOT(dir, qvec) * inv_det;
		if (*v < 0.0 || *u + *v > 1.0)
			return 0;
		
		/* calculate t, ray intersects triangle */
		*t = DOT(edge2, qvec) * inv_det;
	}
	return 1;
}

#define MIN(x,y) (x<y?x:y)
#define MAX(x,y) (x>y?x:y)

// point in triangle

bool point_in_xz_triangle(float *triverts, float x, float z)
{
	 int N = 3;
	 int counter = 0;
	 int i;
	 float x1, z1, x2, z2, xinters;
	 
	 x1 = triverts[0];  // 
	 z1 = triverts[0+2];

	 for (i=0; i<N; i++)
	 {
		 x2 = triverts[i*3];
		 z2 = triverts[i*3+2];

		 if (z > MIN(z1, z2))
			if (z <= MAX(z1, z2))
				if (x <= MAX(x1, x2))
					if (z1 != z2)
					{
						xinters = (z-z1)*(x2-x1)/(z2-z1)+x1;
						if (x1 == x2 || x <= xinters)
							counter++;
					}	
		 x1 = x2;
		 z1 = z2;
	 }

	 return (counter % 2 != 0);

}

}; //namespace

#endif // _WIN32



