// view_state.h	-- Thatcher Ulrich <tu@tulrich.com>

// This source code has been donated to the Public Domain.  Do
// whatever you want with it.

// "view_state", a struct that holds various important rendering
// states during render traversal.


#ifndef CHUNKLOD_VIEWSTATE_H
#define CHUNKLOD_VIEWSTATE_H


#include "chunklod_geometry.h"
#include "chunklod_cull.h"

namespace chunklod {

struct view_state
// Description of basic rendering state.  Passed as an arg to
// visual::render().
{
	int	m_frame_number;
	matrix	m_matrix;
	plane_info	m_frustum[6];	// In local coordinates.

	vec3	get_viewpoint() const
	{
		vec3 v;
		m_matrix.ApplyInverseRotation(&v, -m_matrix.GetColumn(3));
		return v;
	}

	// @@ Should this contain the culling state?  Or keep that separate?

	// @@ Add transformation methods, perhaps lazy updating of frustum planes... ??
};

}
#endif // VIEW_STATE_H
