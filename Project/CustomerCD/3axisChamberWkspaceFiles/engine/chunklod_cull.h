// cull.h	-- by Thatcher Ulrich <tu@tulrich.com> 16 June 2001

// This source code has been donated to the Public Domain.  Do
// whatever you want with it.

// Routines for frustum culling.


#ifndef CHUNKLOD_CULL_H
#define CHUNKLOD_CULL_H


#include "chunklod_geometry.h"


namespace cull  {

	struct result_info {
		bool	culled;	// true when the volume is not visible
		unsigned char	active_planes;	// one bit per frustum plane
		
		result_info(bool c = false, unsigned char a = 0x3f) : culled(c), active_planes(a) {}
	};
	
	
	result_info	compute_box_visibility(const chunklod::vec3& center, const chunklod::vec3& extent,
					       const chunklod::plane_info frustum[6], result_info in);
};


#endif // CULL_H
