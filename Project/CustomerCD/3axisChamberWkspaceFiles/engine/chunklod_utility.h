// utility.h	-- by Thatcher Ulrich <tu@tulrich.com>

// This source code has been donated to the Public Domain.  Do
// whatever you want with it.

// Various little utility functions, macros & typedefs.


#ifndef CHUNKLOD_UTILITY_H
#define CHUNKLOD_UTILITY_H

#include <stdio.h>

#include <assert.h>
#include <math.h>



#ifdef _WIN32
#ifndef NDEBUG

// On windows, replace ANSI assert with our own, for a less annoying
// debugging experience.
int	tu_testbed_assert_break(const char* filename, int linenum, const char* expression);
#undef assert
#define assert(x)	((x) || tu_testbed_assert_break(__FILE__, __LINE__, #x))

#endif // not NDEBUG
#endif // _WIN32

namespace chunklod {

//
// new/delete wackiness -- if USE_DL_MALLOC is defined, we're going to
// try to use Doug Lea's malloc as much as possible by overriding the
// default operator new/delete.
//


#ifndef M_PI
#define M_PI 3.141592654
#endif // M_PI


//
// some misc handy math functions
//
inline int	iabs(int i) { if (i < 0) return -i; else return i; }
#ifdef __GNUC__
	// use the builtin (gcc) operator. ugly, but not my call.
	#define imax _max
	#define fmax _max
	#define _max(a,b) ((a)>?(b))
	#define imin _min
	#define fmin _min
	#define _min(a,b) ((a)<?(b))
#else // not GCC
	inline int	imax(int a, int b) { if (a < b) return b; else return a; }
	inline float	fmax(float a, float b) { if (a < b) return b; else return a; }
	inline int	imin(int a, int b) { if (a < b) return a; else return b; }
	inline float	fmin(float a, float b) { if (a < b) return a; else return b; }
#endif // not GCC

inline int	iclamp(int i, int min, int max) {
	assert( min <= max );
	return imax(min, imin(i, max));
}

inline float	fclamp(float f, float min, float max) {
	assert( min <= max );
	return fmax(min, fmin(f, max));
}

const float LN_2 = 0.693147180559945f;
inline float	log2(float f) { return logf(f) / LN_2; }

inline int	fchop( float f ) { return (int) f; }	// replace w/ inline asm if desired
inline int	frnd(float f) { return fchop(f + 0.5f); }	// replace with inline asm if desired

bool intersect_triangled(double orig[3], double dir[3],
                   double vert0[3], double vert1[3], double vert2[3],
                   double *t, double *u, double *v, bool cull = true);

bool intersect_trianglef(float orig[3], float dir[3],
                   float vert0[3], float vert1[3], float vert2[3],
                   float *t, float *u, float *v,bool cull = true);

bool point_in_xz_triangle(float *triverts, float x, float z);

//
// Read/write bytes to FILE streams.
//


inline void	WriteByte(FILE* dst, unsigned char b) {
	size_t count = fwrite(&b, sizeof(b), 1, dst);
}


inline unsigned char	ReadByte(FILE* src) {
	unsigned char	b;
	size_t count = fread(&b, sizeof(b), 1, src);
	return b;
}


//
// Read/write 32-bit little-endian floats, and 64-bit little-endian doubles.
//


inline void	WriteFloat32(FILE* dst, float value) 
{
	size_t count = fwrite(&value, sizeof(float), 1, dst);
}


inline float ReadFloat32(FILE* src) 
{
	float value;
	size_t count = fread(&value, sizeof(float), 1, src);
	return value;
}


inline void	WriteInt32(FILE* dst, unsigned int value) 
{
	size_t count = fwrite(&value, sizeof(unsigned int), 1, dst);
}


inline unsigned int ReadInt32(FILE* src) 
{
	unsigned int value;
	size_t count = fread(&value, sizeof(unsigned int), 1, src);
	return value;
}

inline void	WriteInt16(FILE* dst, unsigned short int value) 
{
	size_t count = fwrite(&value, sizeof(unsigned short int), 1, dst);
}


inline unsigned short int ReadInt16(FILE* src) 
{
	unsigned short int value;
	size_t count = fread(&value, sizeof(unsigned short int), 1, src);
	return value;
}


inline void	WriteDouble64(FILE* dst, double value) 
{
	size_t count = fwrite(&value, sizeof(double), 1, dst);
}


inline double ReadDouble64(FILE* src)
{
    double value;
	size_t count = fread(&value, sizeof(double), 1, src);
	return value;
}


inline unsigned short int Swap16(unsigned short int D)
{
	return((D<<8)|(D>>8));
}

inline unsigned int Swap32(unsigned int D)
{
	return((D<<24)|((D<<8)&0x00FF0000)|((D>>8)&0x0000FF00)|(D>>24));
}

}; //namespace

#endif // UTILITY_H
