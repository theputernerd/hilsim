/**
*\brief the 3axisChamber object source file.
*
*This file uses glut functions to create an easily displayable instance of a chamber.
*Written by J.West <br>
*jwest@adfa.edu.au
*
*/
#include "3axischamber.h"
#include "stdafx.h"

#ifdef _MSC_VER
#pragma warning( disable : 4786 ) // truncated symbol warning
#endif 


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <map>

#include "GL\gl.h"
#include "GL\glut.h"

#include <string>
#include <set>

#include "sg.h"
#include "ssg.h"
#include "pu.h"

#include "vsbSystemInit.h"
#include "vsbSysPath.h"
#include "vsbViewPortContext.h"
#include "vsbCone.h"
#include "plib\src\ssgaux\ssgaShapes.h"

#include "MotionTable.h"

/**
*\brief The attributes are for the functions of opengl and plib.
*/
bool left_button_down = false, right_button_down = false;
/**
*\brief used to replicate object orientation in the functions
*/
std::vector<Chamber*> instances; //used to replicate object orientation in the functions
	
/**\brief This function is called by displayfn when a window 
*needs to be refreshed or drawn.
*
*It needs to be supplied by the user of this class
*/
void displayGUI();
/**\brief Sets the default values of the chamber window.
*
*These are static so have to be defined here.
*/
int Chamber::m_mainh=640,Chamber::m_mainw=480,Chamber::m_mainx=0,Chamber::m_mainy=0;
/**\brief sets the default value for the m_mainWindowContext.
*
*/
int Chamber::m_mainWindowContext=1;//Because its static, must be defined
		
/**
**\brief Called by opengl on redraw.
*
*The first thing it does is calls the user supplied function
*displayGUI(); then marks the window for redrawing.
*/	
void displayfn ()
{
	displayGUI(); //call the user defined function
	for(int i=0;i<instances.size();i++)
	{//this loop is to go through each of the chamber objects and redraw appropriately.
		glutSetWindow(instances[i]->getContext());
		instances[i]->m_mainView->RedrawBuffer();
		glutSwapBuffers () ;
		glutPostRedisplay () ;

	}
	glutSetWindow(instances[0]->getMainContext());
	//instances[0]->m_mainWindowContext->RedrawBuffer();// for the main window
	//glutSwapBuffers () ;
	glutPostRedisplay () ;
	
}


/**
**\brief Called by opengl on resize.
*
*In order to make resize work for separate instances each window would 
*need to be drawn relative to the new size.
*There is a problem when two subwindows need to be rendered separately.
*Possibly this VSB method needs to be reviewed.
*mArcBall.RecalcGeometry draws from 0,0 not any specified values.
*
*ViewportShape(int w, int h)		
{			mView_W = w; 
			mView_H = h; 
			mView_Aspect = float(w)/float(h);
			mArcBall.RecalcGeometry(0, 0, float(w), float(h));
};
 
   
*/
void reshape ( int w, int h )
{	/*Another way of redrawing separate scenes would be to have this call
	*a redraw method from within instances. The glutCallback function cannot
	*call an objects method directly.
	*/
	for(int i=0;i<instances.size();i++)
	{//this loop is to go through each of the chamber objects and redraw appropriately.
		glutSetWindow(instances[i]->getContext());
		(instances[i])->m_mainView->ViewportShape(w, h); //maintains x-y scale
		instances[i]->m_ab.RecalcGeometry( instances[i]->m_xloc, instances[i]->m_yloc,
		(int)(w*instances[i]->m_wScale), (int)(h*instances[i]->m_hScale));
		
		glutPositionWindow((int)(w*instances[i]->getxScale()), 
			(int)(h*instances[i]->getyScale()));
		
		glutReshapeWindow((int)(w*instances[i]->getwScale()), 
			(int)(h*instances[i]->gethScale()));
		glutSwapBuffers () ;
		
	}
			
	glutPostRedisplay () ;
	
}

/**
**\brief This is called when mouse moves.
*
*Combined with mousefn this creates a mouse
*navigation tool, using the VSBarcball movements.
**/
void motionfn ( int x, int y )
{

	//std::cout<<"IN HERE";
	sgVec2 mouse_pos = {x, y};
	for(int i=0;i<instances.size();i++)
	{	glutSetWindow(instances[i]->getContext());
		vsbBaseCamera *cam = (instances[i])->m_mainView->CameraManager().ActiveCam();
		if (!cam) return;
		sgdMat4 &matrix = cam->RelativeMatrix();
		
		instances[i]->m_ab.Drag(mouse_pos);
	
		glutPostRedisplay () ;
	}
}

/**
**\brief This is called when button is clicked.
*
*Combined with mousefn this creates a mouse
*navigation tool, using the VSBarcball movements.
**/
void mousefn ( int button, int updown, int x, int y )
{
	vsbBaseCamera *cam;
	sgVec2 mouse_pos = {x, y};
	for(int i=0;i<instances.size();i++)
	{	glutSetWindow(instances[i]->getContext());
		cam = (instances[i])->m_mainView->CameraManager().ActiveCam();
		if (!cam) return;
		sgdMat4 &matrix = cam->RelativeMatrix();
	
		if (button == GLUT_LEFT_BUTTON)
		{
			if (updown == GLUT_DOWN)
			{
				left_button_down = true;
			}
			else if (updown == GLUT_UP)
			{
				left_button_down = false;
				right_button_down = false;
			}
		}
	
		if (button == GLUT_RIGHT_BUTTON)
		{
			if (updown == GLUT_DOWN)
			{
				right_button_down = true;
				
			}
			else if (updown == GLUT_UP)
			{
				left_button_down = false;
				right_button_down = false;
			}
		}
	
	
		if (right_button_down)
		{
			if (left_button_down)
			{
				instances[i]->m_ab.Click(ARCBALL_ZOOM, mouse_pos, &matrix);
			}
			else
			{
				instances[i]->m_ab.Click(ARCBALL_PAN, mouse_pos, &matrix);
			}
		} 
		else if (left_button_down)
		{
			instances[i]->m_ab.Click(ARCBALL_SPIN, mouse_pos, &matrix);
		
		}
		glutPostRedisplay () ;
	}//end for
	 	
}

vsbViewportContext *Chamber::m_mainwin;
	
//the chamber methods are commented in the header file.
	double Chamber::getwScale()
	{return m_wScale;
	};
	double Chamber::gethScale()
	{return m_hScale;
	};
	double Chamber::getyScale()
	{return m_yScale;
	};
	double Chamber::getxScale()
	{return m_xScale;
	};
	
	int Chamber::getMainContext()
	{return m_mainWindowContext;
	};
	
	
	int Chamber::chamberInitialise(int x,int y, int width, int height)
	{	
		int   fake_argc = 1 ;
		char *fake_argv[3] ;
		fake_argv[0] = "HIL 3AXIS " ;
		fake_argv[1] = "HIL 3AXIS table visualisation App." ;
		fake_argv[2] = NULL ;
	

		double count=0;
		//std::cout<<count;
	
		glutInitWindowPosition ( x, y ) ;
		glutInitWindowSize     ( width, height );//(160, 100); // ;
		glutInit               ( &fake_argc, fake_argv ) ;
		glutInitDisplayMode    ( GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH ) ;
		glutCreateWindow       ( fake_argv[1] ) ;
		vsbSystemInit();
		glutReshapeFunc        ( reshape  ) ;
		glutDisplayFunc        ( displayfn   ) ;
		m_mainh=height;
		m_mainw=width;
		m_mainx=x;
		m_mainy=y;
		m_mainWindowContext=glutGetWindow();
		m_mainwin= new vsbViewportContext;
		//delete(fake_argv);
		//fake_argv[0]=0;
		return m_mainWindowContext;

	}

	
	
	Chamber::Chamber(const double& antennaAngle, const double& antennaOffset,
		const int context,const int xloc,const int yloc,const int w, const int h)
	{	//instance=this;
		
		m_antennaAngle=antennaAngle;
		setAntennaOffset(antennaOffset);
		m_w=w;
		m_h=h;
		m_xloc=xloc;
		m_yloc=yloc;
		m_hScale=(double)(h)/m_mainh;
		m_wScale=(double)(w)/m_mainw;
		m_xScale=1-(double)(m_mainw-xloc)/m_mainw;
		m_yScale=1-(double)(m_mainh-yloc)/m_mainh;
		
		m_mainWindowContext=context;//so the non-OO functions can get to it.
		
		//m_axis3=0;  //the Angle of the inner Ring
		//m_axis2=0;  //the Angle of the outer ring
		//m_axis1=0;	//the rotational angle of the guidance system
		m_cameraX=4;	//camera x position
		m_cameraY=1;	//camera y position
		m_cameraZ=1;	//camera z position
		m_cameraPan=0;  //camera left-right plane
		m_cameraTilt=180; //camera up-down plane
		m_wallsOn=true;	//true if walls are on
		m_renderingOn=true; //true if wall rendering is on
		init_graphics () ;
		
		
		instances.push_back(this);
	};
	Chamber::~Chamber(void)
	{	delete(this);
		delete(&instances);
		delete(m_mainView);
		delete(&m_horns);
		delete(m_mainwin);
		//delete(hornB);
	};

/*
* returns the context if the window
*/
int Chamber::getContext()
{
	return m_context;
}


/*
*This is used when a reference to a window is given.
*It creates a subwindow in the window reference.
*This is called by the primary class
*/
void Chamber::init_graphics ()
{
	
	glutCreateSubWindow(m_mainWindowContext,m_xloc,m_yloc,m_w,m_h);
	m_context=glutGetWindow();
	glutDisplayFunc        ( displayfn   ) ;
	glutMouseFunc         ( mousefn   ) ;
	glutMotionFunc        ( motionfn  ) ;
	glutReshapeFunc        ( reshape  ) ;
	
	// create the main view
	m_mainView = new vsbViewportContext;
	m_mainView->EnvironmentOpts().SunOn(false);
	m_mainView->EnvironmentOpts().TimeOfDay(5);//this determines where the light originates from
	
	m_mainView->EnvironmentOpts().SkyDomeOn(false);
	m_mainView->EnvironmentOpts().GroundPlaneOn(false);
	
	// create a simple camera in the main view and activate it
	vsbBaseCamera *bc = m_mainView->CameraManager().Create("vsbSimpleCamera");
	bc->RelativeGeom(true);
	m_mainView->CameraManager().ActiveCam(bc);
	// adjust the initial position of the camera to get a good view
	setCamera( m_cameraX, m_cameraY, m_cameraZ, m_cameraPan, m_cameraTilt);
	// Dont want to translate entities to coords relative to camera origin -
	// default is on, and requires some repositioning of all models

	vsbViewportContext::RenderOpts().CameraCentricTransformsOn(false);

	std::cout << "Depth Buffer Size = " << glutGet(GLUT_WINDOW_DEPTH_SIZE) << std::endl;
	
	//Initialise the table
	this->Initialise(GetExecutableDir().c_str());
	m_mainView->EnvironmentManager().SceneRoot()->addKid(this);
	//m_mainView->EnvironmentManager().SceneRoot()->addKid(vc);

}
void Chamber::startMainloop()
{	glutMainLoop();
}

/****
*Turns the walls on or off
*/
void Chamber::setWalls(const bool val);

/****
*Turns the rendering of the walls on or off
*/
void Chamber::setRendering(const bool val);



/*
*Sets the x,y,z location of the camera as well as the direction.
*At this stage the rotations, pan and tilt are about the center of the table
*
*Only one camera object should be associated with this object.
*/
void Chamber::setCamera(const double& x,const double& y, const double& z, 
		const double& pan, const double& tilt)
{	
	m_cameraX=x;
	m_cameraY=y;
	m_cameraZ=z;
	m_cameraPan=pan;
	m_cameraTilt=tilt;
	vsbBaseCamera *bc = m_mainView->CameraManager().ActiveCam();
	sgdMat4 rel_matrix1;
	sgdMat4 rel_matrix2;
	sgdMat4 rel_matrix3;
	sgdMat4 rel_matrix4;
	
	sgdMat4 &rel_matrix = bc->RelativeMatrix();
	sgdMakeTransMat4(rel_matrix1, y, x,z);
	sgdVec3 axes[4] = {{0,0,0},{0,1,0},{0,0,1},{1,0,0}};
	sgdMakeRotMat4(rel_matrix2,pan,axes[2]);

	sgdMakeRotMat4(rel_matrix3,tilt,axes[3]);
	
	sgdMultMat4(rel_matrix4,rel_matrix3,rel_matrix2);
	//swap the commenting if you want rotations to be around current position
	//instead of origin. This will stuff up the initial position though.
	sgdMultMat4(rel_matrix,rel_matrix4,rel_matrix1);
	//sgdMultMat4(rel_matrix,rel_matrix1,rel_matrix4);
	glutPostRedisplay () ;
		
		
}
	

/*
*sets the attributes of the individual rf horn
*There is no way of changing the horns location once it is set.
*The order the horns are added determines the required hornNo.
*/
void Chamber::setRFHorn(const int hornNo,const short phase,const short power)
{

		
	sgMat4 rot_mat;
	sgVec3 axes[4] = {{0,0,0},{0,1,0},{0,0,1},{1,0,0}};
	sgMakeRotMat4   ( rot_mat, (float) phase, axes[1]) ;
	sgMat4 rel_matrix1,rot;
	sgMakeTransMat4(rel_matrix1, -m_horns[hornNo].x, -m_horns[hornNo].y,
	m_horns[hornNo].z);
	sgMultMat4(rot,rel_matrix1,rot_mat);
	((ssgTransform*)(m_horns[hornNo].hornB))->setTransform(rot);
	glutPostRedisplay () ;
};


double Chamber::wallx(short deg,short min,short sec)
{	double rads=deg+min/60.0+sec/3600.0;
	double loc=hornDist*tan(rads*3.142/180);
		
	return loc;
}
//This makes sure the cone is always hornDist away from the center.
double Chamber::wallz(short deg,short min,short sec)
{	double rads=deg+min/60.0+sec/3600.0;
	double loc=hornDist*tan(rads*3.142/180);
	
	return loc;
}
/**This makes sure the cone is always hornDist away from the center.
*/
double Chamber::wally(double x,double y)
{	double loc=sqrt(hornDist*hornDist-x*x-y*y);
	return loc;
}

	
void Chamber::addRFHorn(const short azimuthDeg, const short azimuthMin, const short azimuthSec,
		const short elevationDeg, const short elevationMin, const short elevationSec)
{
	//This adds the horn to the vector.
	//Create an rfHorn
	rfHorn oneHorn;
	oneHorn.phase=0;
	oneHorn.power=0;
	
	//add it to the vector
	//load the image from disk.
	//This could have been done once to be more efficient
	//however it only occurs at startup.
	std::string qualified_model_name;
	qualified_model_name = GetExecutableDir().c_str();
	qualified_model_name += '\\';
	qualified_model_name += "images";
	qualified_model_name += '\\';
	qualified_model_name += "hornon.3ds";
   
	ssgEntity *hornE = ssgLoad3ds(qualified_model_name.c_str());
    //now position the horn appropriately in the scene
	//Some of this is an attempt to change the colour of the horn.
	/*
	sgVec4 col={1,1,1,0};
	ssgColourArray *ca=new ssgColourArray();
	ca->add(col);
	int num=(int)(ca->getNum());
	ca->set(col,num);
	*/
	ssgLeaf *l=new ssgVtxArray ();
	ssgSimpleState *ss=new ssgSimpleState();
	//ss->setTextureFilename(qualified_model_name.c_str());
	//ss->setAlphaClamp(1);
	ss->setTranslucent();
	
	//std::cout<<hornE->getNumKids();
	//ss->setOpaque();
	//((ssgVtxArray*)hornE)->setColours(ca);
	(l)->setState(ss);
		
	//hornE->sets
	//ss->set
	//ss->setAlphaClamp(1);
	//(ssgLeaf*)hornE;
	//((ssgLeaf*)hornE)->setState(ss);
	//l->addParent(hornE);
	//((ssgTween*)l)->newBank(1,1,1,1 ) ;

	//((ssgTweenController*)hornE)->selectBank(0);
	std::cout<<hornE->getNumKids();
		
	//hornE->add
		
	ssgBranch *sb=new ssgTransform;
	//hornE->addParent(sb);
	//sb->addKid(l);
	sb->addKid(hornE);
		
	//sb->addKid(l);

	((ssgVtxArray*)l)->setState(ss);
	oneHorn.hornB=sb;

		
		
	//vsbCone *v=new vsbCone();   //attempting to draw a vsbcone of certain size
	//v->Draw(1,2);				  //If this worked this would help with changing colours
	//v->Create();				  //and changing scale
	//sb->addKid((ssgEntity*)v);
		
	//sgVec4 col={1,1,1,1};			//trying to add colour to the ssgentity
	//ca->add(col);
	//ca->set(rel_matrix1,1)
	//hornt->addKid((ssgEntity*)ca);
	

	//ssgTransform *myConeXfm = new ssgTransform;   //another attempt at drawing a cone
	//vsbCone *myCone = new vsbCone;
	//myCone->s	
	//myCone->Draw(1,2);				  //If this worked this would help with changing colours
	//myCone->Create();				  //and changing scale
	//myConeXfm->addKid((ssgTransform*)myCone);
	//sb->addKid(myConeXfm);
		

	//setMaterial(1,1,1,1);				//trying to add and mnipulate a simplestate
	//ssgLeaf sl;//=new ssgLeaf();
	//((ssgLeaf )hornt)//->setState(*ss);
	//ssgaCylinder();
	double xloc=wallx(azimuthDeg,azimuthMin,azimuthSec);
	double zloc=wallz(elevationDeg,elevationMin,elevationSec);
	double yloc=wally(xloc,zloc);

	oneHorn.x=xloc;
	oneHorn.y=yloc;
	oneHorn.z=zloc;
	
	sgMat4 rel_matrix1;
	sgMakeTransMat4(rel_matrix1, -xloc, -yloc,zloc);
			
	((ssgTransform*)oneHorn.hornB)->setTransform(rel_matrix1);  
    this->addKid(oneHorn.hornB);
	m_horns.push_back(oneHorn);

		
}

/**
**\brief Prints the attributes of the chamber.<br>
*Provides a output operator for any details reuqured to be printed.<br>
*
*/
std::ostream &operator<<(std::ostream &output,const Chamber &obj)
{	
	return output;
}

