/*
     PLIB - A Suite of Portable Game Libraries
     Copyright (C) 2001  Steve Baker
 
     This library is free software; you can redistribute it and/or
     modify it under the terms of the GNU Library General Public
     License as published by the Free Software Foundation; either
     version 2 of the License, or (at your option) any later version.
 
     This library is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     Library General Public License for more details.
 
     You should have received a copy of the GNU Library General Public
     License along with this library; if not, write to the Free Software
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 
     For further information visit http://plib.sourceforge.net

     $Id: ssgLoadJPG.cxx,v 1.1 2002/10/17 00:41:12 themsta Exp $
*/


#include "ssgLocal.h"

#ifdef SSG_LOAD_JPG_SUPPORTED

//#include <gl/glpng.h>

unsigned char * JpegFileToRGBbuffer(const char *fileName,
							   unsigned int *width,
							   unsigned int *height);




bool ssgLoadJPG ( const char *fname, ssgTextureInfo* info )
{
	unsigned int w, h;
    unsigned char *rgbBuff =  JpegFileToRGBbuffer(fname, &w, &h);
	if (!rgbBuff)
	{
		ulSetError ( UL_WARNING, "ssgLoadTexture: Can't load JPG file \"%s\".", fname ) ;
		return false;
	}
	if ( info != NULL )
  {
    info -> width = w ;
    info -> height = h ;
    info -> depth = 3 ;
    info -> alpha = 0 ;
  }
  return ssgMakeMipMaps ( rgbBuff, w, h, 3 ) ;	
}


#else

bool ssgLoadJPG ( const char *fname, ssgTextureInfo* info )
{
  ulSetError ( UL_WARNING, "ssgLoadTexture: '%s' - you need jpeglib for JPG format support",
        fname ) ;
  return false ;
}

#endif
