/*
     PLIB - A Suite of Portable Game Libraries
     Copyright (C) 2001  Steve Baker
 
     This library is free software; you can redistribute it and/or
     modify it under the terms of the GNU Library General Public
     License as published by the Free Software Foundation; either
     version 2 of the License, or (at your option) any later version.
 
     This library is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
     Library General Public License for more details.
 
     You should have received a copy of the GNU Library General Public
     License along with this library; if not, write to the Free
     Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 
     For further information visit http://plib.sourceforge.net

     $Id: ssgDList2.cxx,v 1.24 2002/06/28 01:13:38 themsta Exp $
*/


#include "ssgLocal.h"

static bool _ssgDepthSortDList = true; // depth sort off by default

// Global function to select whether the Dlist is to be sorted or not
// put a prototype of the following func in ssg.h

void ssgDListDepthSortEnable(bool trueFalse)
{
	_ssgDepthSortDList = trueFalse;
}
  

////////////////////////////////////////////////////////////////
//
//  Class _ssgMatrixStack
//  Interface
//  emulates the OpenGL matrix stack 
//

#define MAX_DLIST_MATRIX_STACK   1024

class _ssgMatrixStack
{

private:
  float *matStack [ MAX_DLIST_MATRIX_STACK ] ;
  float *currMat;
  int    stackTop;
  static sgMat4 defltMat;

protected:
  void   setDefault();

public:
	_ssgMatrixStack(); 
  const float *CurrMat() const; 	
  void   clear();
  void   push();	
  void   pop();	
  void   set(sgMat4 m);
  void   set(float *m);
}; 

////////////////////////////////////////////////////////////////
//
//  Class _ssgMatrixStack
//  Implementation
//  emulates the OpenGL matrix stack 
//


_ssgMatrixStack::_ssgMatrixStack() 	
{ 
	stackTop = 0; 
	sgMakeIdentMat4(defltMat); 
	currMat = (float *)defltMat; 
}

inline const float *_ssgMatrixStack::CurrMat() const 	
{ 
	return currMat;
}	
inline void  _ssgMatrixStack::clear()		
{ 
	stackTop = 0;
	setDefault();
}

inline void  _ssgMatrixStack::push()			
{ 
	if (stackTop >= MAX_DLIST_MATRIX_STACK )
		ulSetError ( UL_WARNING, "DList matrix stack overflow!" ) ;
	 matStack[stackTop++] = currMat;
}

inline void _ssgMatrixStack::pop()			
{ 
	if (stackTop == 0 )
		ulSetError ( UL_WARNING, "DList matrix stack underflow!" ) ;
	currMat = matStack[--stackTop];
}

inline void _ssgMatrixStack::set(sgMat4 m)	
{ 
	currMat = (float *)m; 
}

inline void _ssgMatrixStack::set(float *m)	
{ 
	currMat = m; 
}

inline void _ssgMatrixStack::setDefault()	
{ 
	currMat = (float *)defltMat; 
}


sgMat4 _ssgMatrixStack::defltMat;

// 
//
////////////////////////////////////////////////////////////////////////////////

static  _ssgMatrixStack _ssgMatStack; 
static sgMat4			_ssgTexMat; 

enum _ssgDListType
{
  SSG_DLIST_LEAF,
  SSG_DLIST_LOAD_MATRIX,
  SSG_DLIST_PUSH_MATRIX,
  SSG_DLIST_POP_MATRIX,
  SSG_DLIST_LOAD_TEX_MATRIX,
  SSG_DLIST_UNLOAD_TEX_MATRIX,
  SSG_DLIST_NOTHING
} ;

class _ssgDList
{
public:
  _ssgDListType type ;
  sgMat4        mat  ;
  
  ssgLeaf      *leaf ;

  sgMat4        textureMat; //Used by leafs in sorting mode only
  float         range;		//used by leafs in sorting mode only

  _ssgDList () { setEmpty () ; }

  void setPopMatrix ()
  {
    type = SSG_DLIST_POP_MATRIX ;
  }

  void setLoadTexMatrix ( sgMat4  m )
  {
    sgCopyMat4 ( mat, m ) ;
    type = SSG_DLIST_LOAD_TEX_MATRIX ;
  }

  void setUnloadTexMatrix ()
  {
    type = SSG_DLIST_UNLOAD_TEX_MATRIX ;
  }

  void setLoadMatrix ( sgMat4  m )
  {
    sgCopyMat4 ( mat, m ) ;
    type = SSG_DLIST_LOAD_MATRIX ;
  }

  void setPushMatrix ( sgMat4  m )
  {
    sgCopyMat4 ( mat, m ) ;
    type = SSG_DLIST_PUSH_MATRIX ;
  }

  void setDrawLeaf ( ssgLeaf *l )
  {
    leaf = l ;
    type = SSG_DLIST_LEAF ;
  }

  void setEmpty ()
  {
    type = SSG_DLIST_NOTHING ;
  }

  void draw ()
  {
    switch ( type )
    {
      case SSG_DLIST_LEAF :
        leaf -> draw () ;
        break ;

      case SSG_DLIST_POP_MATRIX :
        glPopMatrix () ;
        break ;

      case SSG_DLIST_LOAD_TEX_MATRIX :
        glMatrixMode  ( GL_TEXTURE ) ;
        glLoadMatrixf ( (float *) mat ) ;
        glMatrixMode  ( GL_MODELVIEW ) ;
        break ;

      case SSG_DLIST_UNLOAD_TEX_MATRIX :
        glMatrixMode   ( GL_TEXTURE ) ;
        glLoadIdentity () ;
        glMatrixMode   ( GL_MODELVIEW ) ;
        break ;

      case SSG_DLIST_LOAD_MATRIX :
        glLoadMatrixf ( (float *) mat ) ;
        break ;

      case SSG_DLIST_PUSH_MATRIX :
        glPushMatrix () ;
        glLoadMatrixf ( (float *) mat ) ;
        break ;

      default: break ;
    }

    setEmpty () ;
  }

  void sortedPreDraw ()
  {
	const float *bsphere_centre; 
    switch ( type )
    {
      case SSG_DLIST_LEAF :
		memcpy((void *)mat, _ssgMatStack.CurrMat(), sizeof(sgMat4));  
		sgCopyMat4(textureMat,_ssgTexMat);  
		
		bsphere_centre = leaf -> getBSphere() -> getCenter();

		//transform bsphere for eypoint relative z depth
		//and set the range to it,  and reducing it by bsphere radius
		//keeping in mind that range increases along negative Z
		
		range = bsphere_centre[0] * mat[ 0 ][ 2 ] +
				bsphere_centre[1] * mat[ 1 ][ 2 ] +
				bsphere_centre[2] * mat[ 2 ][ 2 ] + 
				mat[ 3 ][ 2 ] +
				leaf -> getBSphere() ->getRadius ();
        break ;

      case SSG_DLIST_POP_MATRIX :
        _ssgMatStack.pop() ;
        break ;

      case SSG_DLIST_LOAD_TEX_MATRIX :
		sgCopyMat4(_ssgTexMat, mat);
        break ;

      case SSG_DLIST_UNLOAD_TEX_MATRIX :
        sgMakeIdentMat4(_ssgTexMat) ;
        break ;

      case SSG_DLIST_LOAD_MATRIX :
		_ssgMatStack.set((float *) mat );
        break ;

      case SSG_DLIST_PUSH_MATRIX :
        _ssgMatStack.push() ;
        _ssgMatStack.set((float *) mat );
        break ;

      default: break ;
    }
  }

};


#define MAX_DLIST   8192

static int next_dlist = 0 ;
static _ssgDList dlist [ MAX_DLIST ] ;


// Compare function for sorting

static int _ssgDlistRangeCompareFunc(const void *elem1, const void *elem2 )
{
  const _ssgDList *dlist1 = *(const _ssgDList **)elem1;
  const _ssgDList *dlist2 = *(const _ssgDList **)elem2;

  // note we have reversed the signs to sort in decreasing order
  if (dlist1->range > dlist2->range) return 1; 
  if (dlist1->range < dlist2->range) return -1;
  return 0;
}

static void _ssgDrawSortedDList ()
{
   static _ssgDList *sortDlist[ MAX_DLIST ];
   int sortCount = 0;

   // Initialisation
   sortCount = 0;
   _ssgMatStack.clear();       // Cleare the matrix stack
   sgMakeIdentMat4(_ssgTexMat); // Clear the texture matrix
  
  for ( int i = 0 ; i < next_dlist ; i++ )
  {
    dlist [ i ] . sortedPreDraw () ;           // Call the predraw function   
    if (dlist [ i ].type == SSG_DLIST_LEAF)    // If leaf, stick it in the sorting list
		sortDlist[sortCount++] = &dlist [ i ];
	else 
		dlist [ i ].setEmpty () ;             // Otherwise, empty it for safety  
  }


  // Sort the leaf list

  qsort( sortDlist, sortCount, sizeof(_ssgDList *), _ssgDlistRangeCompareFunc );

  float *lastMat = 0;    // last view and texture matrices stored and used to
  float *lastTexMat = 0; // avoid unnecessary matrix loads where possible 
 
  for (i=0; i< sortCount; i++)
  {
    if ( !lastMat || memcmp((void *)lastMat,(void *)sortDlist[i]->mat,sizeof(sgMat4)))
	{	

		// either first leaf in the list or viewing matrix is different from the last
		// drawn entities viewing matrix - go on and set it

		lastMat = (float *)sortDlist[i]->mat;

		glLoadMatrixf ( lastMat ) ;
	}

	if (!lastTexMat || memcmp((void *)lastTexMat,(void *)sortDlist[i]->textureMat,sizeof(sgMat4)))
	{
		// either first leaf in the list or texture matrix is different from the last
		// drawn entities texture matrix - go on and set it 
		lastTexMat = (float *)sortDlist[i]->textureMat;

		glMatrixMode   ( GL_TEXTURE ) ;
 		glLoadMatrixf ( lastTexMat ) ;
		glMatrixMode   ( GL_MODELVIEW ) ;
	}

	// Now actually draw the leaf
	sortDlist[i]->leaf->draw();

	// Empty it for safety
	sortDlist[i]->setEmpty();	
  }		 
}


void _ssgDrawDList ()
{
  if (_ssgDepthSortDList)
     _ssgDrawSortedDList();
  else
    for ( int i = 0 ; i < next_dlist ; i++ )
      dlist [ i ] . draw () ;

  next_dlist = 0 ;
}




void _ssgPushMatrix ( sgMat4 m )
{
  /*
    There is no point in having a  pop/push sequence,
    so optimise it to a simple load.
  */

  if ( next_dlist > 0 &&
       dlist [ next_dlist - 1 ] . type == SSG_DLIST_POP_MATRIX )
  {
    next_dlist-- ;
    _ssgLoadMatrix ( m ) ;
  }
  else
  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setPushMatrix ( m ) ;
}


void _ssgPopMatrix ()
{
  /*
    There is no point in having a  push/pop sequence,
    so optimise it to a single load.
  */

  if ( next_dlist > 0 &&
       dlist [ next_dlist - 1 ] . type == SSG_DLIST_PUSH_MATRIX )
    next_dlist-- ;
  else
  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setPopMatrix () ;
}


void _ssgLoadTexMatrix ( sgMat4 m )
{
  /*
    There is no point in having a unload/load or a load/load sequence,
    so optimise it to a single load.
  */

  while ( next_dlist > 0 &&
          ( dlist [ next_dlist - 1 ] . type == SSG_DLIST_LOAD_TEX_MATRIX ||
            dlist [ next_dlist - 1 ] . type == SSG_DLIST_UNLOAD_TEX_MATRIX )
        )
    next_dlist-- ;

  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setLoadTexMatrix ( m ) ;
}



void _ssgUnloadTexMatrix ()
{
  /*
    There is no point in having a unload/unload or a load/unload sequence,
    so optimise it to a single unload.
  */

  while ( next_dlist > 0 &&
          ( dlist [ next_dlist - 1 ] . type == SSG_DLIST_LOAD_TEX_MATRIX ||
            dlist [ next_dlist - 1 ] . type == SSG_DLIST_UNLOAD_TEX_MATRIX )
        )
    next_dlist-- ;

  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setUnloadTexMatrix () ;
}



void _ssgLoadMatrix ( sgMat4 m )
{
  /*
    There is no point in having a  load/load sequence,
    so optimise it to a single load.
  */

  while ( next_dlist > 0 &&
          dlist [ next_dlist - 1 ] . type == SSG_DLIST_LOAD_MATRIX )
    next_dlist-- ;

  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setLoadMatrix ( m ) ;
}



void _ssgDrawLeaf ( ssgLeaf *l )
{
  if ( next_dlist >= MAX_DLIST )
    ulSetError ( UL_WARNING, "DList stack overflow!" ) ;
  else
    dlist [ next_dlist++ ] . setDrawLeaf ( l ) ;
}
