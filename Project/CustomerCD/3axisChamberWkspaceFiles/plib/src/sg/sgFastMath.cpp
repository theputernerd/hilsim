/*
     $Id: sgFastMath.cpp,v 1.8 2003/05/15 05:55:55 themsta Exp $
*/

#include <memory.h>
#include <math.h>
#include "sgFastMath.h"


__declspec(naked) float __fastcall FastAbs(float a)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fabs
		ret 4
	}
}


__declspec(naked) float __fastcall FastSin(float a)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fsin
		ret 4
	}
}


__declspec(naked) float __fastcall FastCos(float a)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fcos
		ret 4
	}
}



__declspec(naked) float __fastcall FastSqrt(float a)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fsqrt
		ret 4
	}
}


__declspec(naked) float __fastcall FastTan(float a)
{
	
	__asm {
		fld		DWORD PTR [esp+4] 
		fptan
		//fxch 
		fstp DWORD PTR [esp-4]
		ret 4
	}
}


__declspec(naked) float __fastcall FastAtan2(float y, float x)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fld		DWORD PTR [esp+8]
		fpatan

		ret 8
	}
}

__declspec(naked) float __fastcall InverseSqrt(float a)
{
	__asm {
		mov		eax, 0be6eb508h
		mov		DWORD PTR [esp-12],03fc00000h ;  1.5 on the stack
		sub		eax, DWORD PTR [esp+4]; a
		sub		DWORD PTR [esp+4], 800000h ; a/2 a=Y0
		shr		eax, 1     ; firs approx in eax=R0
		mov		DWORD PTR [esp-8], eax

		fld		DWORD PTR [esp-8] ;r
		fmul	st, st            ;r*r
		fld		DWORD PTR [esp-8] ;r
		fxch	st(1)
		fmul	DWORD PTR [esp+4];a ;r*r*y0
		fld		DWORD PTR [esp-12];load 1.5
		fld		st(0)
		fsub	st,st(2)			   ;r1 = 1.5 - y1
		;x1 = st(3)
		;y1 = st(2)
		;1.5 = st(1)
		;r1 = st(0)

		fld		st(1)
		fxch	st(1)
		fmul	st(3),st			; y2=y1*r1*...
		fmul	st(3),st			; y2=y1*r1*r1
		fmulp	st(4),st            ; x2=x1*r1
		fsub	st,st(2)               ; r2=1.5-y2
		;x2=st(3)
		;y2=st(2)
		;1.5=st(1)
		;r2 = st(0)

		fmul	st(2),st			;y3=y2*r2*...
		fmul	st(3),st			;x3=x2*r2
		fmulp	st(2),st			;y3=y2*r2*r2
		fxch	st(1)
		fsubp	st(1),st			;r3= 1.5 - y3
		;x3 = st(1)
		;r3 = st(0)
		fmulp	st(1), st
		ret 4
	}
}


#ifdef WIN32
void __fastcall FastSinCos( float theta, float *sin, float *cos)
{

	__asm {
		mov edx, dword ptr sin
		mov ecx, dword ptr cos
		fld		dword ptr theta
		fsincos
		fstp dword ptr [ecx]
		fstp dword ptr [edx]
	}
}
#else
void FastSinCos( float theta, float *sin, float *cos)
{
	*sin = FastSin(theta);
	*cos = FastCos(theta);
}
#endif

__declspec(naked) float __fastcall FastSqr(float a)
{
	__asm {
		fld		DWORD PTR [esp+4] 
		fld     st(0)
		fmul
		ret 4
	}
}

	


///////////////////////////////////////////////////////////////////////////////////////


void Generic_Mult_3x3_3x3( float *src1, float *src2, float *dst);
void Generic_Mult_3x3_3x1( float *src1, float *src2, float *dst);
void Generic_Mult_4x4_4x4( float *src1, float *src2, float *dst);
void Generic_Mult_4x4_4x1( float *src1, float *src2, float *dst);
void Generic_Mult_4x4_3x1( float *src1, float *src2, float *dst);
void Generic_Xfm_4x4_3x1( float *src1, float *src2, float *dst);


#ifdef WIN32 
// All subsequent code is PC x86 specific


void PII_Mult_3x3_3x3( float *src1, float *src2, float *dst);
void PII_Mult_3x3_3x1( float *src1, float *src2, float *dst);
void PII_Mult_4x4_4x4( float *src1, float *src2, float *dst);
void PII_Mult_4x4_4x1( float *src1, float *src2, float *dst);
void PII_Mult_4x4_3x1( float *src1, float *src2, float *dst);
void PII_Xfm_4x4_3x1( float *src1, float *src2, float *dst);


#endif // WIN32

#ifdef WIN32

Mult_3x3_3x3_func *Mult_3x3_3x3_ptr = Generic_Mult_3x3_3x3;
Mult_3x3_3x1_func *Mult_3x3_3x1_ptr = Generic_Mult_3x3_3x1;
Mult_4x4_4x4_func *Mult_4x4_4x4_ptr = PII_Mult_4x4_4x4;
Mult_4x4_4x1_func *Mult_4x4_4x1_ptr = PII_Mult_4x4_4x1;
Mult_4x4_3x1_func *Mult_4x4_3x1_ptr = PII_Mult_4x4_3x1;
Xfm_4x4_3x1_func  *Xfm_4x4_3x1_ptr  = PII_Xfm_4x4_3x1;

#else

Mult_3x3_3x3_func *Mult_3x3_3x3_ptr = Generic_Mult_3x3_3x3;
Mult_3x3_3x1_func *Mult_3x3_3x1_ptr = Generic_Mult_3x3_3x1;
Mult_4x4_4x4_func *Mult_4x4_4x4_ptr = Generic_Mult_4x4_4x4;
Mult_4x4_4x1_func *Mult_4x4_4x1_ptr = Generic_Mult_4x4_4x1;
Mult_4x4_3x1_func *Mult_4x4_3x1_ptr = Generic_Mult_4x4_3x1;
Xfm_4x4_3x1_func  *Xfm_4x4_3x1_ptr  = Generic_Xfm_4x4_3x1;

#endif


void Generic_Mult_3x3_3x3( float *src1, float *src2, float *dst)
{
    float tmp[9];
    int i, j;
 
    for (i = 0; i < 3; i++)
    {
        const int   idx = i * 3;
        const float *pa = &src1[idx];
        for(j = 0; j < 3; j++)
        {
            const float *pb = &src2[j];
            tmp[idx+j] = src1[idx+0] * src2[0+j]
                       + src1[idx+1] * src2[3+j]
                       + src1[idx+2] * src2[6+j];
        }
    }
    memcpy (dst, tmp, 3 * 3 * sizeof (float));
}

void Generic_Mult_3x3_3x1( float *src1, float *src2, float *dst)
{
  float t[3];

  memcpy(t, src2, 3 * sizeof(float));

  dst[0] = t[0] * src1[ 0 ] + t[1] * src1[3]  + t[2] * src1[6]  ;
  dst[1] = t[0] * src1[ 1 ] + t[1] * src1[4]  + t[2] * src1[7] ;
  dst[2] = t[0] * src1[ 2 ] + t[1] * src1[5]  + t[2] * src1[8] ;
}

void Generic_Mult_4x4_4x4( float *src1, float *src2, float *dst)
{
//	float tmp[16];

    dst[0]  = src2[0] * src1[0]  + src2[4] * src1[1]  + src2[8]  * src1[2]  + src2[12] * src1[3];
    dst[1]  = src2[1] * src1[0]  + src2[5] * src1[1]  + src2[9]  * src1[2]  + src2[13] * src1[3];
    dst[2]  = src2[2] * src1[0]  + src2[6] * src1[1]  + src2[10] * src1[2]  + src2[14] * src1[3];
    dst[3]  = src2[3] * src1[0]  + src2[7] * src1[1]  + src2[11] * src1[2]  + src2[15] * src1[3];
    dst[4]  = src2[0] * src1[4]  + src2[4] * src1[5]  + src2[8]  * src1[6]  + src2[12] * src1[7];
    dst[5]  = src2[1] * src1[4]  + src2[5] * src1[5]  + src2[9]  * src1[6]  + src2[13] * src1[7];
    dst[6]  = src2[2] * src1[4]  + src2[6] * src1[5]  + src2[10] * src1[6]  + src2[14] * src1[7];
    dst[7]  = src2[3] * src1[4]  + src2[7] * src1[5]  + src2[11] * src1[6]  + src2[15] * src1[7];
    dst[8]  = src2[0] * src1[8]  + src2[4] * src1[9]  + src2[8]  * src1[10] + src2[12] * src1[11];
    dst[9]  = src2[1] * src1[8]  + src2[5] * src1[9]  + src2[9]  * src1[10] + src2[13] * src1[11];
    dst[10] = src2[2] * src1[8]  + src2[6] * src1[9]  + src2[10] * src1[10] + src2[14] * src1[11];
    dst[11] = src2[3] * src1[8]  + src2[7] * src1[9]  + src2[11] * src1[10] + src2[15] * src1[11];
    dst[12] = src2[0] * src1[12] + src2[4] * src1[13] + src2[8]  * src1[14] + src2[12] * src1[15];
    dst[13] = src2[1] * src1[12] + src2[5] * src1[13] + src2[9]  * src1[14] + src2[13] * src1[15];
    dst[14] = src2[2] * src1[12] + src2[6] * src1[13] + src2[10] * src1[14] + src2[14] * src1[15];
    dst[15] = src2[3] * src1[12] + src2[7] * src1[13] + src2[11] * src1[14] + src2[15] * src1[15];
//	memcpy (dst, tmp, 4 * 4 * sizeof (float));
    
}

void Generic_Mult_4x4_4x1( float *src1, float *src2, float *dst)
{
  float t[4] = {src2[0], src2[1], src2[2], src2[3]};

  dst[0] = t[0] * src1[ 0 ] + t[1] * src1[4]  + t[2] * src1[8]  + t[3] * src1[ 12 ] ;
  dst[1] = t[0] * src1[ 1 ] + t[1] * src1[5]  + t[2] * src1[9]  + t[3] * src1[13] ;
  dst[2] = t[0] * src1[ 2 ] + t[1] * src1[6]  + t[2] * src1[10] + t[3] * src1[14] ;
  dst[3] = t[0] * src1[ 3 ] + t[1] * src1[7]  + t[2] * src1[11] + t[3] * src1[15] ;
}

void Generic_Mult_4x4_3x1( float *src1, float *src2, float *dst)
{
  float t[3] = {src2[0], src2[1], src2[2]};

  dst[0] = t[0] * src1[ 0 ] + t[1] * src1[4]  + t[2] * src1[8]  ;
  dst[1] = t[0] * src1[ 1 ] + t[1] * src1[5]  + t[2] * src1[9]  ;
  dst[2] = t[0] * src1[ 2 ] + t[1] * src1[6]  + t[2] * src1[10] ;
}

void Generic_Xfm_4x4_3x1( float *src1, float *src2, float *dst)
{
  float t[3] = {src2[0], src2[1], src2[2]};

  dst[0] = t[0] * src1[ 0 ] + t[1] * src1[4]  + t[2] * src1[8]  +  src1[12] ;
  dst[1] = t[0] * src1[ 1 ] + t[1] * src1[5]  + t[2] * src1[9]  +  src1[13] ;
  dst[2] = t[0] * src1[ 2 ] + t[1] * src1[6]  + t[2] * src1[10] +  src1[14] ;
}

#ifdef WIN32 

///////////////////////////////////////////////////////////////////////

long start = 0;
long end = 0;
long save_ebx;
#define RecordTime(var) \
	__asm cpuid \
	__asm rdtsc \
__asm mov var, eax

#define StartRecordTime \
__asm mov save_ebx, ebx \
RecordTime(start)

#define StopRecordTime \
	RecordTime(end) \
__asm mov ebx, save_ebx

#define mi(w, t, i, j) 4 * ((i * w + j) * (1-t) + (j * w + i) * t)
// Load & multiply.
#define flm(k, i, j, m, n, a, b) \
	__asm fld dword ptr [edx + mi(m, a, i, k)] \
    __asm fmul dword ptr [ecx + mi(n, b, k, j)]

// Load, multiply & add.
#define flma(k, i, j, m, n, a, b) flm(k, i, j, m, n, a, b) __asm faddp ST(1), ST(0)

#define e3(i, j, l, m, n, a, b) \
	flm (0, i, j, m, n, a, b) \
	flma(1, i, j, m, n, a, b) \
	flma(2, i, j, m, n, a, b) \
   __asm fstp dword ptr [eax + mi(l, 0, i, j)]


#define e4(i, j, l, m, n, a, b) \
	flm(0, i, j, m, n, a, b) \
	flm(1, i, j, m, n, a, b) \
	flm(2, i, j, m, n, a, b) \
	flm(3, i, j, m, n, a, b) \
	__asm faddp st(1), st(0) \
	__asm fxch st(2) \
	__asm faddp st(1), st(0) \
	__asm faddp st(1), st(0) \
	__asm fstp dword ptr [eax + mi(l, 0, i, j)]



void PII_Mult_3x3_3x3( float *src1, float *src2, float *dst)
{
	__asm mov edx, DWORD PTR src1
	__asm mov ecx, DWORD PTR src2
	__asm mov eax, DWORD PTR dst
	e3(0, 0, 3, 3, 3, 0, 0) 
	e3(0, 1, 3, 3, 3, 0, 0) 
	e3(0, 2, 3, 3, 3, 0, 0)
	e3(1, 0, 3, 3, 3, 0, 0) 
	e3(1, 1, 3, 3, 3, 0, 0) 
	e3(1, 2, 3, 3, 3, 0, 0)
	e3(2, 0, 3, 3, 3, 0, 0) 
	e3(2, 1, 3, 3, 3, 0, 0) 
	e3(2, 2, 3, 3, 3, 0, 0)
}


void PII_Mult_3x3_3x1( float *src1, float *src2, float *dst)
{
	
	__asm {
		mov edx, dword ptr src1
		mov ecx, dword ptr src2
		mov eax, dword ptr dst
		fld dword ptr [ecx]
		fmul dword ptr [edx+24]
		fld dword ptr [ecx]
		fmul dword ptr [edx+12]
		fld dword ptr [ecx]
		fmul dword ptr [edx]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+4]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+16]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+28]
		fxch ST(2)
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+8]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+20]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+32]
		fxch ST(2)
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fstp dword ptr [eax]
		fstp dword ptr [eax+4]
		fstp dword ptr [eax+8]
	}
	
}

void PII_Mult_4x4_4x4( float *src1, float *src2, float *dst)
{
	StartRecordTime;
	__asm mov edx, DWORD PTR src1
		__asm mov ecx, DWORD PTR src2
		__asm mov eax, DWORD PTR dst
		e4(0, 0, 4, 4, 4, 0, 0)
		e4(0, 1, 4, 4, 4, 0, 0)
		e4(0, 2, 4, 4, 4, 0, 0)
		e4(0, 3, 4, 4, 4, 0, 0)
		e4(1, 0, 4, 4, 4, 0, 0)
		e4(1, 1, 4, 4, 4, 0, 0)
		e4(1, 2, 4, 4, 4, 0, 0)
		e4(1, 3, 4, 4, 4, 0, 0)
		e4(2, 0, 4, 4, 4, 0, 0)
		e4(2, 1, 4, 4, 4, 0, 0)
		e4(2, 2, 4, 4, 4, 0, 0)
		e4(2, 3, 4, 4, 4, 0, 0)
		e4(3, 0, 4, 4, 4, 0, 0)
		e4(3, 1, 4, 4, 4, 0, 0)
		e4(3, 2, 4, 4, 4, 0, 0)
		e4(3, 3, 4, 4, 4, 0, 0)
		StopRecordTime;


}

void PII_Mult_4x4_3x1( float *src1, float *src2, float *dst)
{
	
	__asm {
		mov edx, dword ptr src1
		mov ecx, dword ptr src2
		mov eax, dword ptr dst
		fld dword ptr [ecx]
		fmul dword ptr [edx+8]
		fld dword ptr [ecx]
		fmul dword ptr [edx+4]
		fld dword ptr [ecx]
		fmul dword ptr [edx]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+24]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+20]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+16]
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+40]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+36]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+32]
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fstp dword ptr [eax]
		fstp dword ptr [eax+4]
		fstp dword ptr [eax+8]
	}
}


void PII_Xfm_4x4_3x1( float *src1, float *src2, float *dst)
{
	
	__asm {
		mov edx, dword ptr src1
		mov ecx, dword ptr src2
		mov eax, dword ptr dst
		fld dword ptr [ecx]
		fmul dword ptr [edx+8]
		fld dword ptr [ecx]
		fmul dword ptr [edx+4]
		fld dword ptr [ecx]
		fmul dword ptr [edx]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+24]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+20]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+16]
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+40]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+36]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+32]
		faddp ST(3),ST
		faddp ST(3),ST
		faddp ST(3),ST
		fadd dword ptr [edx+48]
		fstp dword ptr [eax]
		fadd dword ptr [edx+52]
		fstp dword ptr [eax+4]
		fadd dword ptr [edx+56]
		fstp dword ptr [eax+8]
	}
}






void PII_Mult_4x4_4x1( float *src1, float *src2, float *dst)
{
	
	__asm {
		mov edx, dword ptr src1
		mov ecx, dword ptr src2
		mov eax, dword ptr dst
		fld dword ptr [ecx]
		fmul dword ptr [edx+12]
		fld dword ptr [ecx]
		fmul dword ptr [edx+8]
		fld dword ptr [ecx]
		fmul dword ptr [edx+4]
		fld dword ptr [ecx]
		fmul dword ptr [edx]

		fld dword ptr [ecx+4]
		fmul dword ptr [edx+28]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+24]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+20]
		fld dword ptr [ecx+4]
		fmul dword ptr [edx+16]
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+44]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+40]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+36]
		fld dword ptr [ecx+8]
		fmul dword ptr [edx+32]
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST
		fld dword ptr [ecx+12]
		fmul dword ptr [edx+60]
		fld dword ptr [ecx+12]
		fmul dword ptr [edx+56]
		fld dword ptr [ecx+12]
		fmul dword ptr [edx+52]
		fld dword ptr [ecx+12]
		fmul dword ptr [edx+48]
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST
		faddp ST(4),ST


		fstp dword ptr [eax]
		fstp dword ptr [eax+4]
		fstp dword ptr [eax+8]
		fstp dword ptr [eax+12]
	}
}





#endif