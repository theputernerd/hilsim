/*
     $Id: sgFastMath.h,v 1.6 2002/06/11 00:07:07 themsta Exp $
*/


#ifndef SGFASTMATH_H
#define SGFASTMATH_H

#ifdef WIN32

float __fastcall FastAbs(float a);
float __fastcall FastSin(float a);
float __fastcall FastCos(float a);
float __fastcall InverseSqrt(float a);
float __fastcall FastSqrt(float a);
float __fastcall FastTan(float a);
void __fastcall FastSinCos( float theta, float *sin, float *cos);
float __fastcall FastAtan2(float y, float x);
float __fastcall Sqrt(float a);
float __fastcall FastSqr(float a);

#else

#define FastAbs(a) fabsf(a)
#define FastSin(a) sinf(a)
#define FastCos(a) cosf(a)
#define FastTan(a) tanf(a)
#define InverseSqrt(a) (sqrt(a)/a)
#define FastSqrt(a) sqrtf(a)
#define FastSqr(a) ((a)*(a))
#define FastAtan2(y,  x) atan2(y, x)
void    FastSinCos( float theta, float *sin, float *cos);

#endif


typedef void  Mult_3x3_3x3_func( float *src1, float *src2, float *dst);
typedef void  Mult_3x3_3x1_func( float *src1, float *src2, float *dst);
typedef void  Mult_4x4_4x4_func( float *src1, float *src2, float *dst);
typedef void  Mult_4x4_4x1_func( float *src1, float *src2, float *dst);
typedef void  Mult_4x4_3x1_func( float *src1, float *src2, float *dst);
typedef void  Xfm_4x4_3x1_func( float *src1, float *src2, float *dst);

extern Mult_3x3_3x3_func *Mult_3x3_3x3_ptr;
extern Mult_3x3_3x1_func *Mult_3x3_3x1_ptr;
extern Mult_4x4_4x4_func *Mult_4x4_4x4_ptr;
extern Mult_4x4_4x1_func *Mult_4x4_4x1_ptr;
extern Mult_4x4_3x1_func *Mult_4x4_3x1_ptr;
extern Xfm_4x4_3x1_func  *Xfm_4x4_3x1_ptr;


inline Mult_3x3_3x3( float *src1, float *src2, float *dst) {(*Mult_3x3_3x3_ptr)(src1, src2, dst);}
inline Mult_3x3_3x1( float *src1, float *src2, float *dst) {(*Mult_3x3_3x1_ptr)(src1, src2, dst);}
inline Mult_4x4_4x4( float *src1, float *src2, float *dst) {(*Mult_4x4_4x4_ptr)(src1, src2, dst);}
inline Mult_4x4_4x1( float *src1, float *src2, float *dst) {(*Mult_4x4_4x1_ptr)(src1, src2, dst);}
inline Mult_4x4_3x1( float *src1, float *src2, float *dst) {(*Mult_4x4_3x1_ptr)(src1, src2, dst);}
inline Xfm_4x4_3x1( float *src1, float *src2, float *dst) {(*Xfm_4x4_3x1_ptr)(src1, src2, dst);}


#endif

