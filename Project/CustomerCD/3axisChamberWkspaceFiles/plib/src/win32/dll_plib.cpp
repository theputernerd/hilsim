// dll_plib.cpp : Defines the entry point for the DLL application.
//

#include "dll_plib.h"

#if defined(DLL_PLIB_EXPORTS) 
#  ifdef _MSC_VER
#    define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#  endif
#  ifndef _WINDOWS_    // Dont open files unnecessarily
#     include <windows.h>
#  endif


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}

#endif

