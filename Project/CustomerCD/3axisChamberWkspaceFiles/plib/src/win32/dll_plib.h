#ifndef _DLL_PLIB_H
#define _DLL_PLIB_H
/*
// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DLL_PLIB_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DLL_PLIB_API functions as being imported from a DLL, wheras this DLL sees symbols
// defined with this macro as being exported.
*/

#if defined (DLL_PLIB_EXPORTS)
#define DLL_PLIB_API __declspec(dllexport)
#elif defined (DLL_PLIB_IMPORTS)
#define DLL_PLIB_API __declspec(dllimport)
#else
#define DLL_PLIB_API
#endif


#endif
