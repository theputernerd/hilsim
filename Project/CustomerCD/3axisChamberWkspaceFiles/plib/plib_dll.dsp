# Microsoft Developer Studio Project File - Name="plib_dll" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=plib_dll - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "plib_dll.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "plib_dll.mak" CFG="plib_dll - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "plib_dll - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "plib_dll - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "plib_dll - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "../../Bin/RELEASE"
# PROP Intermediate_Dir "RELEASE_DLL"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "PLIBDLL_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "../" /I "src/fnt" /I "src/pui" /I "src/sg" /I "src/ssg" /I "src/util" /I "src/js" /I "src/ssgAux" /I "src/Win32" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DLL_PLIB_EXPORTS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib glu32.lib OpenGl32.lib winmm.lib /nologo /dll /machine:I386 /out:"Release\plib_dll.dll"

!ELSEIF  "$(CFG)" == "plib_dll - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Plib_DLL___Win32_Debug"
# PROP BASE Intermediate_Dir "Plib_DLL___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "../../Bin/DEBUG"
# PROP Intermediate_Dir "DEBUG_DLL"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "PLIBDLL_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "../" /I "src/fnt" /I "src/pui" /I "src/sg" /I "src/ssg" /I "src/util" /I "src/js" /I "src/ssgAux" /I "src/Win32" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "DLL_PLIB_EXPORTS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib glu32.lib OpenGl32.lib winmm.lib /nologo /dll /debug /machine:I386 /out:"Debug\plib_dll.dll" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "plib_dll - Win32 Release"
# Name "plib_dll - Win32 Debug"
# Begin Group "fnt"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\fnt\fnt.cxx
# End Source File
# Begin Source File

SOURCE=.\src\fnt\fnt.h
# End Source File
# Begin Source File

SOURCE=.\src\fnt\fntLocal.h
# End Source File
# Begin Source File

SOURCE=.\src\fnt\fntTXF.cxx
# End Source File
# End Group
# Begin Group "pui"

# PROP Default_Filter ""
# Begin Group "pui headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\pui\pu.h
# End Source File
# Begin Source File

SOURCE=.\src\pui\puLocal.h
# End Source File
# End Group
# Begin Group "pui sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\pui\pu.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puArrowButton.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puBiSlider.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puBox.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puButton.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puButtonBox.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puDial.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puDialogBox.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puFilePicker.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puFont.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puFrame.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puGroup.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puInput.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puInterface.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puLargeInput.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puListBox.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puMenuBar.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puObject.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puOneShot.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puPopup.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puPopupMenu.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puSlider.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puText.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puTriSlider.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puValue.cxx
# End Source File
# Begin Source File

SOURCE=.\src\pui\puVerticalMenu.cxx
# End Source File
# End Group
# End Group
# Begin Group "sg"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\sg\sg.cxx
# End Source File
# Begin Source File

SOURCE=.\src\sg\sg.h
# End Source File
# Begin Source File

SOURCE=.\src\sg\sgd.cxx
# End Source File
# Begin Source File

SOURCE=.\src\sg\sgIsect.cxx
# End Source File
# End Group
# Begin Group "ssg"

# PROP Default_Filter ""
# Begin Group "ssg headers"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\ssg\ssg.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssg3ds.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgKeyFlier.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoaderWriterStuff.h
# End Source File
# End Group
# Begin Group "ssg sources"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\ssg\ssg.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgAnimation.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgAxisTransform.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgBase.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgBaseTransform.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgBranch.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgconf.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgContext.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgConvex.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgCutout.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgDList2.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgEntity.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgInvisible.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgIO.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgIsect.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLeaf.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgList.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoad.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoad3ds.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadAC.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadASE.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadATG.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadBMP.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadDXF.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoaderWriterStuff.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadFLT.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadM.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadMD2.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadMDL.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadMDL.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadOBJ.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadOFF.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadPNG.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadSGI.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadSSG.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadStrip.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadTexture.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadTGA.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadTRI.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadVRML.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLoadX.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgLocal.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgMSFSPalette.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgOptimiser.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgParser.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgParser.h
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgRangeSelector.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgRoot.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSave3ds.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveAC.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveASE.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveATG.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveDXF.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveM.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveOBJ.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveOFF.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveQHI.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveTRI.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSaveX.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSelector.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSimpleList.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgSimpleState.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgState.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgStateSelector.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgStateTables.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgStats.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgTexTrans.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgTexture.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgTransform.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgVTable.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgVtxArray.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssg\ssgVtxTable.cxx
# End Source File
# End Group
# End Group
# Begin Group "util"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\util\ul.cxx
# End Source File
# Begin Source File

SOURCE=.\src\util\ul.h
# End Source File
# Begin Source File

SOURCE=.\src\util\ulClock.cxx
# End Source File
# Begin Source File

SOURCE=.\src\util\ulError.cxx
# End Source File
# End Group
# Begin Group "js"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\js\js.h
# End Source File
# End Group
# Begin Group "ssgAux"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\ssgaux\ssgaShapes.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssgaux\ssgaShapes.h
# End Source File
# Begin Source File

SOURCE=.\src\ssgaux\ssgAux.cxx
# End Source File
# Begin Source File

SOURCE=.\src\ssgaux\ssgAux.h
# End Source File
# End Group
# Begin Group "Win32"

# PROP Default_Filter ""
# Begin Source File

SOURCE=.\src\win32\dll_plib.cpp
# End Source File
# Begin Source File

SOURCE=.\src\win32\dll_plib.h
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=..\GL\glut32.lib
# End Source File
# End Target
# End Project
