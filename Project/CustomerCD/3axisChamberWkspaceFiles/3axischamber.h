#ifndef _3axischamber_H_
#define _3axischamber_H_

#include "3axischamber.h"
#include <vector>
#include "MotionTable.h"
#include "vsbViewPortContext.h"

//#include <stdafx.h>
//#include<afxwin.h>

/**\brief This defines the distance the RFHorns are from the table origin
*/
#define hornDist 10

/**
* \brief Class chamber builds a 3d visualisation of an inherited motiontable
*and a generic anechoic chamber.
*
*Creating an instance of this class will create an OpenGL main window and draw the objects
*to that window. <br>
*To use this object: <br>
*<ul><li>the chamber must be initialised by calling the static method:<br>
*<code>static int Chamber::chamberInitialise(int x,int y, int width, int height);</code><br>
*Returned from this method is the context of the mainwindow.<br>
*This should be called only once as it calls glutInit()<br>
*<li>Any further glutfunctions should be defined, e.g.<br>
*<code>glutKeyboardFunc(keyfn) ;</code><BR>
*<li>A new instance can then be created <br>
*<code>c=new Chamber(8,20,0,mainWindow,0,0,320,480);</code><br>
*This requires the mainWindow context be given, which is not functioning completely.<br>
*<li>add the rf horns as needed<br>
eg. <code>c->addRFHorn(359,	59,		47,		0,		1,		55);</code>
*<li>Finally the mainloop is started off<br>
*<code>c->startMainloop();</code><br>
*this method calls glutmainloop and never returns, with the exception of the user defined function
*displayGUI() which is called on redisplay.<br>
*</ul>
*
*
*This object makes openGL calls and expects the user to provide a function <br>
*<code>void displayGUI(); </code><br>
*Control is returned to this function when opengl redisplays the scene, giving
*the creator of the object the opportunity to modify the scenes contents.<br>
*
* The object defines the following GL callbacks:<br>
*<code>	glutReshapeFunc(reshape  ) ;<br>
*	glutDisplayFunc(displayfn   ) ;<br>
*   glutMouseFunc(mousefn   ) ;<br>
*   glutMotionFunc(motionfn  ) ;<br></code>
*and provides appropriate functions to deal with them.	<BR><br>
*Further glut functions, such as <br>glutKeyboardFunc(keyfn);<br>
* can be defined in the users program after 
*<code>someinstance->chamberInitialise(...);</code>
* is called.
*
*A brief example of using this object.
*
*<pre><code><p>
*
*#include "3axischamber.h"
*#include <stdio.h>
*#include <string>
*#include <iostream>
*#include "GL\gl.h"
*#include "GL\glut.h"
*
*void displayGUI () //this must be defined by the user of the chamber class
*{  //Put your modifications to the chamber in here
*   c->setCamera(camerax,cameray,cameraz,pan,tilt);
*
*}
*
*void keyfn ( unsigned char key, int, int )
*{//Other glutfunction calls can still be made
*   std::cout<<key;
*
*}
*	Chamber c;
*	int mainWindow;
*	mainWindow=c->chamberInitialise(0,0,640,480); //create main window at 0,0 size 640,480
*	glutKeyboardFunc       ( keyfn     ) ;  //call this function on key press
*	c=new Chamber(20,0,mainWindow,0,0,320,480); //create a new window inside mainWindow
*	c->setCamera(-10,1,2,204,0);		//set the camer
*	c->addRFHorn	(2,	29,	10,	355,42,	14);	//add the horn to the scene
*	c->addRFHorn	(4,	59,	38,	0,	1,	34);
*	c->addRFHorn	(9,	52,	42,	0,	1,	58);
*	c->addRFHorn	(354,59,17,	0,	1,	55);
*	c->addRFHorn	(357,30,2,	4,	22,	0);
*	c->addRFHorn	(357,29,57,	355,42,	14);
*	c->addRFHorn	(2,	29,	17,	4,	22,	0);
*	c->addRFHorn	(359,59,47,	0,	1,	55);
*	c->startMainloop();							//start the mainloop
</code></pre>
*
*Written by J.West <br>
*jwest@adfa.edu.au<br>
*
*11 Oct 03<br>
*********************************/
class Chamber: public table
{	friend std::ostream &operator<<(std::ostream&,const Chamber &); //so the overload operate can
																	//access the member function
public:

	/**
	**\brief Primary Constructor for Chamber object.
	*
	* This constructor sets the inital settings of the chamber.<br>
	* <ul>
	*<li>antennaAngle - The angle of the antenna projection
	*<li>antennaOffset - The offset from center of the antenna origin. negative is towards the wall
	*<li>context - The window context to draw this instance into. (not implemented)
	*<li>xloc,yloc - The location of the subwindows context (not implemented)
	*<li>h,w - The height and width of the subwindow.
	*  
	*</ul>
	*
	*The constructor also sets up the callbacks for GLUT, therefore the static method<br>
	*<code>static int Chamber::chamberInitialise(int x,int y, int width, int height);</code><br>
	*needs to be called once prior to creating a new instance.<br>
	**/
	Chamber(const double& antennaAngle, const double& antennaOffset,
		const int context,const int xloc,const int yloc, const const int h,const int w);
	/**\brief Destructor for the object.<br>
	*
	*Deletes all available pointers.
	*/
	virtual ~Chamber(void);
	
	/**
	**\brief Turns the walls on or off.<br>
	*
	*Not implemented.
	*/
	void setWalls(const bool val);	
	
	/**
	**\brief Turns the rendering of the walls on or off.<br>
	*
	*Not implemented.
	*/
	void setRendering(const bool val);

	/**
	**\brief Sets the x,y,z location of the camera as well as the direction.<br>
	*
	*(0,0,0) is located and the central point of the table. <br>
	*Pan=0 and tilt=0 is facing directly away from the RF wall.<br>
	*At this stage the rotations, pan and tilt are about the center of the table.<br>
	*The rotations can easily be made about the current point by changing the line<br>
	* <code>sgdMultMat4(rel_matrix,rel_matrix4,rel_matrix1);</code><br>
	* to <br>
	* <code>sgdMultMat4(rel_matrix,rel_matrix1,rel_matrix4);</code><br>
	*However doing this will change the initial position and the camera co-ordiantes
	*of the system.
	*
	*For ease of keyboard navigation 
	*the camera operation is dependant on the direction that it is pointing.<br>
	*x is forward back, y is left right, z is up down.<br>
	*pan rotates around the central point of the table. Tilt tilts from side to side.
	*
	*/
	void setCamera(const double& x,const double& y, const double& z, 
			const double& pan, const double& tilt);
	

	/**
	**\brief Sets the attributes of the individual rf horn.<br>
	*
	*Horns are added with <code><br>
	*void addRFHorn(const short azimuthDeg, const short azimuthMin, const short azimuthSec, const short elevationDeg, const short elevationMin, const short elevationSec);
	<br></code>
	*
	*There is no way of changing the horns location once it is set.<br>
	*The order the horns are added determines the required hornNo.<br>
	*The first horn added is hornNo=0;<br>
	*Phase is indicated by rotating the hornOn.3ds image.<br>
	*Power is not implemented.<br>
	*The initial position is determined by the way it is drawn in the file hornon.3ds.
	*/
	void setRFHorn(const int hornNo,const short phase,const short power);

	/**
	**\brief Adds an RF horn to the scene<br>
	*
	*azimuth and elevation specify where the horn is located relative to the center of
	*the table.<br>
	*This will put the horn hornDist from the central point.<br>
	*In order to change this distance modify the #define hornDist in 3axischamber.h<br>
	*Care needs to be taken when creating the hornon.3ds graphic to ensure the appropriate
	*point is at the origin.<br>
	*
	*Loads hornOn.3ds. Merged with the image of the horn is a "clock hand".<br>
	*The phase of the horn is represented by rotating the horn image.<br>
	*
	*A different coloured image will be shown if the horn is completely off.(not Implemented)<br>
	*If the horn is in some power state in between maximum and 0 the horn image will change
	*colour accordingly not implemented)<br>
	*
	*<br><pre>
		*Currently the horns are at the following locs
		      Azimuth          Elevation 
	 	deg 	min 	sec 	deg 	min 	sec 
		354 	 59 	 17 	  0 	  1 	 55         %Horn 1
		357 	 30 	  2 	  4 	 22 	  0         %Horn 2
		357 	 29 	 57 	355 	 42 	 14         %Horn 3
		  2 	 29 	 17 	  4 	 22 	  0         %Horn 4 
		359 	 59 	 47 	  0 	  1 	 55         %Horn 5
		  2 	 29 	 10 	355 	 42 	 14         %Horn 6
		  4 	 59 	 38 	  0 	  1 	 34         %Horn 7
		  9 	 52 	 42 	  0 	  1 	 58         %Horn 8
	</pre>
	*/
	void addRFHorn(const short azimuthDeg, const short azimuthMin, const short azimuthSec,
			const short elevationDeg, const short elevationMin, const short elevationSec);


	/**\brief returns the context of the subwindow.
	*/
	int getContext();
	/**\brief returns the context of the mainWindow.
	*/
	static int getMainContext();

	/**\brief initialises the chamber scene, including the table, the GLUT window and the VSB camera.
	*
	*Returned is the context of the mainwindow.
	*/
	static int Chamber::chamberInitialise(int x,int y, int width, int height);

	/**
	**\brief commences glutmainloop and never returns<br>
	*
	*Control is passed to the user when redisplay is called by GLUT to the following function: <br>
	*<code>void displayGUI(); </code><br>
	*Redisplay is posted by this class when any moving part is modified.<br>
	*/
	static void Chamber::startMainloop();
	/**
	*\brief the mainWindow view context.<br>
	*
	*This is called by the functions so needs to be a public member.
	*/
	static vsbViewportContext *m_mainwin;
	/**
	*\brief the instance view context.<br>
	*
	*This is called by the functions so needs to be a public member.
	*/
	vsbViewportContext *m_mainView;
	/**
	*\brief the arcball for mouse movement.<br>
	*
	*This is called by the functions so needs to be a public member.
	*/
	vsbArcBall m_ab;
	/**
	*\brief The location of the subwindow<br>
	*
	*This is called by the functions so needs to be a public member.
	*/
	int m_xloc,m_yloc; //The location of the subwindow
	/**
	*\brief The sub window width and height.<br>
	*
	*This is called by the functions so needs to be a public member.
	*/
	int m_w,m_h;//sub window height
	
	/**
	*\brief returns the scale width of the subwindow relative to the main window.<br>
	*
	*This is used on resize. <br>
	*This is called by the functions so needs to be a public member.
	*
	*/
	double Chamber::getwScale();
	/**
	*\brief returns the scale height of the subwindow relative to the main window.<br>
	*
	*This is used on resize. <br>
	*This is called by the functions so needs to be a public member.
	*
	*/
	double Chamber::gethScale();
	/**
	*\brief returns the scale y of the subwindow relative to the main window.<br>
	*
	*This is used on resize. <br>
	*This is called by the functions so needs to be a public member.
	*
	*/
	double Chamber::getyScale();
	/**
	*\brief returns the scale x of the subwindow relative to the main window.<br>
	*
	*This is used on resize. <br>
	*This is called by the functions so needs to be a public member.
	*
	*/
	double Chamber::getxScale();
	/**
	*\brief the scale of the sub window relative to the main window. 
	*
	*The reason why 
	*the attributes are public is for
	*performance reasons. Accessing the member attributes by methods is slower. <br>
	*These attributes are needed at a critical performance time in redrawing the scene.<br>
	*This is called by the functions so needs to be a public member.
	*/
	double m_wScale, m_hScale,m_xScale, m_yScale;

	
	

private:
	/**\brief the size and position of the main window
	*/
	static int m_mainh,m_mainw, m_mainx,m_mainy;//the position of the window

	/**\brief return the x location of the horn on the RF wall. <br>
	*
	*uses #define hornDist 10*/
	double wallx(short deg,short min,short sec);
	/**\brief return the y location of the horn on the RF wall. <br>
	*
	*uses #define hornDist 10*/
	double wallz(short deg,short min,short sec);
	/**\brief return the z location of the horn on the RF wall. <br>
	*
	*uses #define hornDist 10*/
	double wally(double x,double y);
	
	/**
	**\brief Called by the primary constructor to initialise glut.
	*/
	void init_graphics ();
	/**
	**\brief The GL Context for this object.
	*/
	int m_context; 
	/**\brief The projection angle of the antenna
	*/
	double m_antennaAngle;   
	/**\brief The camera position and angle.
	*/
	double m_cameraX, m_cameraY,m_cameraZ,m_cameraPan, m_cameraTilt; //camera up-down plane
	/**\brief true if the walls are on.
	*
	*Not implemented in this version
	*/
	bool m_wallsOn;	//true if walls are on
	/**\brief true if rendering is on. 
	*
	*Not implemented in this version
	*/
	bool m_renderingOn; //true if wall rendering is on
	/**\brief the location of the rf horn on the wall and its power and phase.
	*/
	struct rfHorn
	{//Horn Structjre
		/**\brief the phase and power of this horn
		*/
		short phase,power; // for each horns power
		
		//in final implementation don't need the azimuth and elev as the data is used
		//when it is implemented.
		/**\brief the location of the horn on the rf wall.
		*
		*The distance the horn is from the central axis is determined by 
		*#define hornDist 10
		*/
		double x,y,z;
		/**\brief the branch representing the inital horn. 
		*
		*This is positioned at 0,0,0 so to rotate you only have to translate
		*back to the wall after the rotations, not translate, rot, translate.
		*It saves one translation.
		*/
		ssgBranch *hornB;//The branch description of the initial horn.
						 //this is positioned at 0,0,0 so to rotate you only have to translate
						 //back to the wall after rotation, not translate, rot, translate.
	};
	/**\brief Container for all of the horns in the scene
	*/
	std::vector<rfHorn> m_horns; //A vector of RFhorns.

	/**
	**\brief this is the context for the mainWindow
	*/
	static int m_mainWindowContext;//this is the context for the mainWindow



};
/**
*\brief Overload of the << operator so can print out.
*
*This is added so any data can be printed as neccesary..
*/
std::ostream &operator<<(std::string&,const Chamber&);

#endif
