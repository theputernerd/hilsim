% DATA FILE CONTAINING RFSG HORN ARRAY PARAMETERS.
%
% FUNCTIONS REQUIRED 
%   dms2deg - converts angles in (degrees, minutes, seconds) into degrees.
%   wrap - wraps angles in degrees to the range -180 to 180 degrees.
%
% VERSION INFO:
%   1.0 - Baseline for the RFSG model(which included only AoA synthesis 
%         capability).
%
%   2.0 - Modifications made to accomodate proposal for complete
%         virtual RFSG modelling capability. 
%       - Computation for RFSG array radius from horn phase centre
%         to focus added.
%       - Computation for RFSG horn positions in azimuth and elevation
%         added (in degrees).
%       - Computation for RFSG horn positions in cartesian coordinates
%         added (in metres). Left hand coordinate system is used.
%
%   Author: Ninh Duong (WSD-DSTO)
%   Project: RFSG simulation model
%   Version: 2.0
%   Date: 13/09/2001
%
%   Matlab Build: Version 6.1.0.450 Release 12.1
%#######################################################################

% Radial distance of mount points in spherical array 
% to point of focus (m)
RadialDist = 10.655000; 

% Total length of the RFSG horns (m)
HornLength = 0.333440; 

% Total length of mounting brackets (m)
MountLength = 0.100000; 

% Distance between phase centre to front face of RFSG horn (m)
PhaseDist = 0.043000; 

% Compute radial distance from phase centre of RFSG horn to array focus (m).
Radius = RadialDist - HornLength - MountLength + PhaseDist;

% From the seeker
% Azimuth and elevation of each of the horns. Angles are
% in (degrees, minutes, seconds) relative to negative x axis.
AzEl = [ 
%      Azimuth          Elevation 
% 	deg 	min 	sec 	deg 	min 	sec 
	354 	 59 	 17 	  0 	  1 	 55         %Horn 1
	357 	 30 	  2 	  4 	 22 	  0         %Horn 2
	357 	 29 	 57 	355 	 42 	 14         %Horn 3
	  2 	 29 	 17 	  4 	 22 	  0         %Horn 4 
	359 	 59 	 47 	  0 	  1 	 55         %Horn 5
	  2 	 29 	 10 	355 	 42 	 14         %Horn 6
	  4 	 59 	 38 	  0 	  1 	 34         %Horn 7
	  9 	 52 	 42 	  0 	  1 	 58         %Horn 8
]; 

% Determine cartesian coordinates of RFSG horns.
[hx,hy,hz] = sph2cart(dms2deg(AzEl(:,1:3))*pi/180, ...
                      dms2deg(AzEl(:,4:6))*pi/180, Radius);
hornxyz = [hx,hy,hz];

% Determine spherical coordinates of RFSG horns.
hornAzEl = [wrap(dms2deg(AzEl(:,1:3))) wrap(dms2deg(AzEl(:,4:6)))];

% Definition of triads in RFSG array. Horn numbering is 
% in relation to the row index of the AzEl array, above.
triadsets = [
  1  2  5 
  1  3  5 
  2  4  5 
  3  5  6 
  4  5  7 
  5  6  7 
  4  7  8 
  6  7  8 
]; 


% Clear non-essential variables
clear RadialDist HornLength MountLength PhaseDist AzEl hx hy hz
